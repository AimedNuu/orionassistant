// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** Macro.cpp
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "macro.h"
#include "../OrionAssistant/orionassistant.h"
#include "../Managers/spellmanager.h"
#include "../CommonItems/skill.h"
//----------------------------------------------------------------------------------
QString CMacro::GetText()
{
	switch (m_Type)
	{
		case MT_USE:
		{
			CMacroWithObject *withObject = (CMacroWithObject*)this;

			if (m_ConvertType == MCT_BY_TYPE)
				return "UseType: " + COrionAssistant::GraphicToText(withObject->GetGraphic());
			else if (m_ConvertType == MCT_BY_TYPE_COLOR)
				return "UseTypeColor: " + COrionAssistant::GraphicToText(withObject->GetGraphic()) + ", " + COrionAssistant::GraphicToText(withObject->GetColor());
			else if (m_ConvertType == MCT_BY_TYPE_GROUND)
				return "UseTypeGround: " + COrionAssistant::GraphicToText(withObject->GetGraphic());
			else if (m_ConvertType == MCT_BY_TYPE_COLOR_GROUND)
				return "UseTypeColorGround: " + COrionAssistant::GraphicToText(withObject->GetGraphic()) + ", " + COrionAssistant::GraphicToText(withObject->GetColor());
			else if (m_ConvertType == MCT_LAST)
				return "UseObject: last object";
			else if (m_ConvertType == MCT_SELF)
				return "UseObject: self";

			return "UseObject: " + COrionAssistant::SerialToText(withObject->GetSerial());
		}
		case MT_ATTACK:
		{
			if (m_ConvertType == MCT_LAST || m_ConvertType == MCT_LASTATTACK)
				return "Attack: last attack";
			else if (m_ConvertType == MCT_LASTTARGET)
				return "Attack: last target";
			//else if (m_ConvertType == MCT_FRIEND)
			//	return "Attack: friend";
			else if (m_ConvertType == MCT_ENEMY)
				return "Attack: enemy";

			return "Attack: " + COrionAssistant::SerialToText(((CMacroWithObject*)this)->GetSerial());
		}
		case MT_WAIT_FOR_TARGET_OBJECT:
		{
			CMacroWithObject *withObject = (CMacroWithObject*)this;
			QString text;

			if (m_ConvertType == MCT_BY_TYPE)
				text.sprintf("WaitForTarget: type 0x%04X (delay=%i ms)", withObject->GetGraphic(), m_WaitingTime);
			else if (m_ConvertType == MCT_BY_TYPE_COLOR)
				text.sprintf("WaitForTarget: type color 0x%04X, 0x%04X (delay=%i ms)", withObject->GetGraphic(), withObject->GetColor(), m_WaitingTime);
			else if (m_ConvertType == MCT_BY_TYPE_GROUND)
				text.sprintf("WaitForTargetGround: type 0x%04X (delay=%i ms)", withObject->GetGraphic(), m_WaitingTime);
			else if (m_ConvertType == MCT_BY_TYPE_COLOR_GROUND)
				text.sprintf("WaitForTargetGround: type color 0x%04X, 0x%04X (delay=%i ms)", withObject->GetGraphic(), withObject->GetColor(), m_WaitingTime);
			else if (m_ConvertType == MCT_LASTATTACK)
				text.sprintf("WaitForTarget: last attack (delay=%i ms)", m_WaitingTime);
			else if (m_ConvertType == MCT_LASTTARGET)
				text.sprintf("WaitForTarget: last target (delay=%i ms)", m_WaitingTime);
			else if (m_ConvertType == MCT_SELF)
				text.sprintf("WaitForTarget: self (delay=%i ms)", m_WaitingTime);
			else if (m_ConvertType == MCT_ENEMY)
				text.sprintf("WaitForTarget: enemy (delay=%i ms)", m_WaitingTime);
			else if (m_ConvertType == MCT_FRIEND)
				text.sprintf("WaitForTarget: friend (delay=%i ms)", m_WaitingTime);
			else
				text.sprintf("WaitForTarget: object 0x%08X (delay=%i ms)", withObject->GetSerial(), m_WaitingTime);

			return text;
		}
		case MT_WAIT_FOR_TARGET_TILE:
		{
			QString text;

			if (m_ConvertType == MCT_LAST)
				text.sprintf("WaitForTarget: last tile (delay=%i ms)", m_WaitingTime);
			else if (m_ConvertType == MCT_BY_TARGET_RELATIVE)
				text.sprintf("WaitForTarget: relative tile (delay=%i ms)", m_WaitingTime);
			else
				text.sprintf("WaitForTarget: tile (delay=%i ms)", m_WaitingTime);

			return text;
		}
		case MT_USE_SKILL:
		{
			if (m_ConvertType == MCT_LAST)
				return "UseSkill: last";

			return "UseSkill: " + g_Skills[((CMacroWithIntValue*)this)->GetValue()].GetName();
		}
		case MT_CAST:
		case MT_CAST_NEW:
		case MT_CAST_FROM_BOOK:
		{
			if (m_ConvertType == MCT_LAST)
				return "CastSpell: last";

			int spell = ((CMacroWithIntValue*)this)->GetValue();
			int spellbook = spell / 100;
			int spellbooksCount = qMin(g_SpellManager.m_SpellbookSpellNames.size(), 7);

			if (spellbook < spellbooksCount)
			{
				QStringList &list = g_SpellManager.m_SpellbookSpellNames[spellbook];
				spell %= 100;

				if (spell > 0 && spell <= list.size())
					return "CastSpell: " + list[spell - 1];
			}

			break;
		}
		case MT_SAY:
		{
			if (m_ConvertType == MCT_SAY_YELL)
				return "Yell: " + ((CMacroWithStringValue*)this)->GetValue();
			else if (m_ConvertType == MCT_SAY_WHISPER)
				return "Whisper: " + ((CMacroWithStringValue*)this)->GetValue();
			else if (m_ConvertType == MCT_SAY_EMOTE)
				return "Emote: " + ((CMacroWithStringValue*)this)->GetValue();
			else if (m_ConvertType == MCT_SAY_BROADCAST)
				return "Broadcast: " + ((CMacroWithStringValue*)this)->GetValue();
			else if (m_ConvertType == MCT_SAY_PARTY)
				return "Party chat: " + ((CMacroWithStringValue*)this)->GetValue();
			else if (m_ConvertType == MCT_SAY_GUILD)
				return "Guild chat: " + ((CMacroWithStringValue*)this)->GetValue();
			else if (m_ConvertType == MCT_SAY_ALLIANCE)
				return "Alliance chat: " + ((CMacroWithStringValue*)this)->GetValue();

			return "Say: " + ((CMacroWithStringValue*)this)->GetValue();
		}
		case MT_PICK_UP_ITEM:
		{
			return "PickUp: ";
		}
		case MT_DROP_ITEM:
		{
			return "Drop: ";
		}
		case MT_EQUIP_ITEM:
		{
			return "Equip: ";
		}
		case MT_WAIT_FOR_PROMPT:
			return "WaitForPrompt: " + ((CMacroWithStringValue*)this)->GetValue();
		case MT_WAIT_FOR_MENU:
		{
			QString text;

			if (m_ConvertType == MCT_ANY)
				text.sprintf("WaitForMenu: any (delay=%i ms)", m_WaitingTime);
			else
				text.sprintf("WaitForMenu: %s (delay=%i ms)", ((CMacroWithMenu*)this)->GetName().toStdString().c_str(), m_WaitingTime);

			return text;
		}
		case MT_WAIT_FOR_GUMP:
		{
			QString text;
			uint button = ((CMacroWithGump*)this)->GetButton();

			if (m_ConvertType == MCT_ANY)
				text.sprintf("WaitForGump: any, button:%i (delay=%i ms)", button, m_WaitingTime);
			else
				text.sprintf("WaitForGump: 0x%08X, button:%i (delay=%i ms)", ((CMacroWithGump*)this)->GetID(), button, m_WaitingTime);

			return text;
		}
		case MT_REQUEST_CONTEXT_MENU:
		{
			if (m_ConvertType == MCT_SELF)
				return "RequestContextMenu: self";
			else if (m_ConvertType == MCT_LASTATTACK)
				return "RequestContextMenu: last attack";
			else if (m_ConvertType == MCT_LASTTARGET)
				return "RequestContextMenu: last target";
			else if (m_ConvertType == MCT_ENEMY)
				return "RequestContextMenu: enemy";
			else if (m_ConvertType == MCT_FRIEND)
				return "RequestContextMenu: friend";

			return "RequestContextMenu: object " + COrionAssistant::SerialToText(((CMacroWithObject*)this)->GetSerial());
		}
		case MT_WAIT_FOR_CONTEXT_MENU:
		{
			QString text;

			if (m_ConvertType == MCT_ANY)
				text.sprintf("WaitForContext menu: any (delay=%i ms)", m_WaitingTime);
			else
				text.sprintf("WaitForContext menu: object 0x%08X (delay=%i ms)", ((CMacroWithObject*)this)->GetSerial(), m_WaitingTime);

			return text;
		}
		case MT_SELECT_TRADE:
		{
			return "WaitForTrade: ";
		}
		case MT_WAR_MODE:
		{
			if (m_ConvertType == MCT_TOGGLE)
				return "WarMode: toggle";

			return QString("War mode: ") + (((CMacroWithIntValue*)this)->GetValue() ? "on" : "off");
		}
		case MT_WALK:
		{
			int val = ((CMacroWithIntValue*)this)->GetValue();

			QString text;

			const QString directionName[8] = { "North", "North East", "East", "South East", "South", "South West", "West", "North West" };

			if (val < 0)
			{
				val = -val;
				text = "Turn: " + directionName[val & 7];
			}
			else if (val & 0x80)
				text = "Run: " + directionName[val & 7];
			else
				text = "Walk: " + directionName[val & 7];

			return text;
		}
		case MT_HELP_MENU_REQUEST:
			return "Help menu request";
		case MT_QUEST_MENU_REQUEST:
			return "Quest menu request";
		case MT_GUILD_MENU_REQUEST:
			return "Guild menu request";
		case MT_WRESTLING_STUN:
			return "Wrestling: stun";
		case MT_WRESTLING_DISARM:
			return "Wrestling: disarm";
		case MT_COMBAT_ABILITY:
		{
			if (m_ConvertType == MCT_PRIMARY)
				return "Use ability: Primary";
			else
				return "Use ability: Secondary";
		}
		case MT_TOGGLE_GARGOYLE_FLYING:
			return "Toggle gargoyle flying";
		case MT_WAIT:
			return "Wait: " + QString::number(m_WaitingTime) + " ms";
		case MT_SET_ARM:
			return "SetArm";
		case MT_DISARM:
			return "Disarm, delay: " + QString::number(m_WaitingTime) + " ms";
		case MT_ARM:
			return "Arm, delay: " + QString::number(m_WaitingTime) + " ms";
		case MT_SET_DRESS:
			return "SetDress";
		case MT_UNDRESS:
			return "Undress, delay: " + QString::number(m_WaitingTime) + " ms";
		case MT_DRESS:
			return "Dress, delay: " + QString::number(m_WaitingTime) + " ms";
		default:
			break;
	}

	return "unknown action";
}
//----------------------------------------------------------------------------------
bool CMacro::HaveConvertTable()
{
	switch (m_Type)
	{
		case MT_USE:
		case MT_ATTACK:
		case MT_WAIT_FOR_TARGET_OBJECT:
		case MT_WAIT_FOR_TARGET_TILE:
		case MT_USE_SKILL:
		case MT_CAST:
		case MT_CAST_NEW:
		case MT_CAST_FROM_BOOK:
		case MT_SAY:
		case MT_PICK_UP_ITEM:
		case MT_DROP_ITEM:
		case MT_WAIT_FOR_MENU:
		case MT_WAIT_FOR_GUMP:
		case MT_REQUEST_CONTEXT_MENU:
		case MT_WAIT_FOR_CONTEXT_MENU:
		//case MT_SELECT_TRADE:
		case MT_WAR_MODE:
		case MT_WALK:
		case MT_DISARM:
		case MT_ARM:
		case MT_UNDRESS:
		case MT_DRESS:
		case MT_WAIT:
		case MT_COMBAT_ABILITY:
			return true;
		default:
			break;
	}

	return false;
}
//----------------------------------------------------------------------------------
CMacro *CMacro::Copy()
{
	return new CMacro(this);
}
//----------------------------------------------------------------------------------
CMacro *CMacroWithIntValue::Copy()
{
	return new CMacroWithIntValue(this);
}
//----------------------------------------------------------------------------------
CMacro *CMacroWithStringValue::Copy()
{
	return new CMacroWithStringValue(this);
}
//----------------------------------------------------------------------------------
CMacro *CMacroWithObject::Copy()
{
	return new CMacroWithObject(this);
}
//----------------------------------------------------------------------------------
CMacro *CMacroWithGump::Copy()
{
	return new CMacroWithGump(this);
}
//----------------------------------------------------------------------------------
CMacro *CMacroWithMenu::Copy()
{
	return new CMacroWithMenu(this);
}
//----------------------------------------------------------------------------------
CMacro *CMacroWithTargetTile::Copy()
{
	return new CMacroWithTargetTile(this);
}
//----------------------------------------------------------------------------------
CMacro *CMacro::Load(QXmlStreamAttributes &attributes)
{
	CMacro *result = nullptr;

	if (attributes.hasAttribute("type") && attributes.hasAttribute("convert_type"))
	{
		MACRO_TYPE type = (MACRO_TYPE)attributes.value("type").toInt();

		switch (type)
		{
			case MT_USE:
			case MT_ATTACK:
			case MT_WAIT_FOR_TARGET_OBJECT:
			case MT_REQUEST_CONTEXT_MENU:
			case MT_WAIT_FOR_CONTEXT_MENU:
			{
				if (attributes.hasAttribute("serial") && attributes.hasAttribute("graphic") && attributes.hasAttribute("color"))
				{
					result = new CMacroWithObject
							(
								type,
								COrionAssistant::TextToSerial(attributes.value("serial").toString()),
								COrionAssistant::TextToGraphic(attributes.value("graphic").toString()),
								COrionAssistant::TextToGraphic(attributes.value("color").toString())
							);
				}

				break;
			}
			case MT_USE_SKILL:
			case MT_CAST:
			case MT_CAST_FROM_BOOK:
			case MT_CAST_NEW:
			case MT_WAR_MODE:
			case MT_WALK:
			{
				if (attributes.hasAttribute("value"))
					result = new CMacroWithIntValue(type, attributes.value("value").toInt());

				break;
			}
			case MT_SAY:
			case MT_WAIT_FOR_PROMPT:
			{
				if (attributes.hasAttribute("value"))
					result = new CMacroWithStringValue(type, attributes.value("value").toString());

				break;
			}
			case MT_WAIT_FOR_GUMP:
			{
				if (attributes.hasAttribute("serial") && attributes.hasAttribute("id") && attributes.hasAttribute("button"))
				{
					result = new CMacroWithGump
							(
								type,
								COrionAssistant::TextToSerial(attributes.value("serial").toString()),
								COrionAssistant::TextToSerial(attributes.value("id").toString()),
								attributes.value("button").toInt()
							);
				}

				break;
			}
			case MT_WAIT_FOR_MENU:
			{
				if (attributes.hasAttribute("name") && attributes.hasAttribute("item_name") && attributes.hasAttribute("graphic") && attributes.hasAttribute("color"))
				{
					result = new CMacroWithMenu
							(
								type,
								attributes.value("name").toString(),
								attributes.value("item_name").toString(),
								COrionAssistant::TextToGraphic(attributes.value("graphic").toString()),
								COrionAssistant::TextToGraphic(attributes.value("color").toString())
							);
				}

				break;
			}
			case MT_WAIT_FOR_TARGET_TILE:
			{
				if (attributes.hasAttribute("graphic") && attributes.hasAttribute("x") && attributes.hasAttribute("y") && attributes.hasAttribute("z") && attributes.hasAttribute("relative_x") && attributes.hasAttribute("relative_y") && attributes.hasAttribute("relative_z"))
				{
					result = new CMacroWithTargetTile
							(
								type,
								COrionAssistant::TextToGraphic(attributes.value("graphic").toString()),
								attributes.value("x").toInt(),
								attributes.value("y").toInt(),
								attributes.value("z").toInt(),
								attributes.value("relative_x").toInt(),
								attributes.value("relative_y").toInt(),
								attributes.value("relative_z").toInt()
							);
				}

				break;
			}
			case MT_WAIT:
			case MT_HELP_MENU_REQUEST:
			case MT_QUEST_MENU_REQUEST:
			case MT_GUILD_MENU_REQUEST:
			case MT_SET_ARM:
			case MT_DISARM:
			case MT_ARM:
			case MT_SET_DRESS:
			case MT_UNDRESS:
			case MT_DRESS:
			case MT_WRESTLING_STUN:
			case MT_WRESTLING_DISARM:
			case MT_COMBAT_ABILITY:
			case MT_TOGGLE_GARGOYLE_FLYING:
			{
				result = new CMacro(type);
				break;
			}
			default:
				break;
		}

		if (result != nullptr)
		{
			result->SetConvertType((MACRO_CONVERT_TYPE)attributes.value("convert_type").toInt());

			if (attributes.hasAttribute("waiting_time"))
				result->SetWaitingTime(attributes.value("waiting_time").toInt());
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
void CMacro::Save(QXmlStreamWriter &writter)
{
	writter.writeAttribute("type", QString::number((int)m_Type));
	writter.writeAttribute("convert_type", QString::number((int)m_ConvertType));
	writter.writeAttribute("waiting_time", QString::number(m_WaitingTime));
}
//----------------------------------------------------------------------------------
void CMacroWithIntValue::Save(QXmlStreamWriter &writter)
{
	CMacro::Save(writter);

	writter.writeAttribute("value", QString::number(m_Value));
}
//----------------------------------------------------------------------------------
void CMacroWithStringValue::Save(QXmlStreamWriter &writter)
{
	CMacro::Save(writter);

	writter.writeAttribute("value", m_Value);
}
//----------------------------------------------------------------------------------
void CMacroWithObject::Save(QXmlStreamWriter &writter)
{
	CMacro::Save(writter);

	writter.writeAttribute("serial", COrionAssistant::SerialToText(m_Serial));
	writter.writeAttribute("graphic", COrionAssistant::GraphicToText(m_Graphic));
	writter.writeAttribute("color", COrionAssistant::GraphicToText(m_Color));
}
//----------------------------------------------------------------------------------
void CMacroWithGump::Save(QXmlStreamWriter &writter)
{
	CMacro::Save(writter);

	writter.writeAttribute("serial", COrionAssistant::SerialToText(m_Serial));
	writter.writeAttribute("id", COrionAssistant::SerialToText(m_ID));
	writter.writeAttribute("button", QString::number(m_Button));
}
//----------------------------------------------------------------------------------
void CMacroWithMenu::Save(QXmlStreamWriter &writter)
{
	CMacro::Save(writter);

	writter.writeAttribute("name", m_Name);
	writter.writeAttribute("item_name", m_ItemName);
	writter.writeAttribute("graphic", COrionAssistant::GraphicToText(m_Graphic));
	writter.writeAttribute("color", COrionAssistant::GraphicToText(m_Color));
}
//----------------------------------------------------------------------------------
void CMacroWithTargetTile::Save(QXmlStreamWriter &writter)
{
	CMacro::Save(writter);

	writter.writeAttribute("graphic", COrionAssistant::GraphicToText(m_Graphic));
	writter.writeAttribute("x", QString::number(m_X));
	writter.writeAttribute("y", QString::number(m_Y));
	writter.writeAttribute("z", QString::number(m_Z));
}
//----------------------------------------------------------------------------------
