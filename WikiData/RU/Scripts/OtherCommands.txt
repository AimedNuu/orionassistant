# ������ ������ �� �������� � ��������� (�������������� � �����) ��� Orion Assistant 2.0.

������ ������:

**NameSpace**.**name**(_**requiredParameters**_, [_optionalParameters=defaultValue_]);

***

- void Orion.SaveConfig();

��������� ������������.

***

- void Orion.SetLight(state, [value=currentLight]);

���������� ������� ������������.

state - ��������� ��������/���������.

value - �������� ������������ �� 0 �� 31.

***

- void Orion.SetWeather(state, [index=currentWeather], [effectsCount=currentCount], [temperature=currentTemperature]);

���������� �������� �������.

state - ��������� ��������/���������.

index - ���������� ����� ��������� �������.

effectsCount - ���������� �������� �� ������.

temperature - �� ������������.

***

- void Orion.SetSeason(state, [index=currentIndex], [musicIndex=currentMusic]);

���������� ����� � �������.

state - ��������� ��������/���������.

index - ���������� ����� ������.

musicIndex - ����� ������ ��� ���������������.

***

- void Orion.Track([state=false], [x=-1, y=-1]);

������/���������� ��������� ������� � ��������� �����������. -1 � ����������� ���������� ������� �������������� � ����.

state - ��������� ����������/������.

x - ���������� X � ����, ���� ��������� �������

y - ���������� Y � ����, ���� ��������� �������

***

- bool Orion.SaveHotkeys('fileName');

��������� ������� ����� ������� ��� ��������� fileName (� ����� Hotkeys � ����� �������).

���������: true ��� �������� ���������� �����.

***

- bool Orion.LoadHotkeys('fileName');

��������� ����� ������� c ��������� fileName (�� ����� Hotkeys � ����� �������).

���������: true ��� �������� �������� �����.

***

- void Orion.Cast(spellIndex/'spellName', ['targetSerial']);

���������� ���������� spellName (��� �� ����� ���������� �� ����������� ������).

���� ������ targetSerial - ������������� ����������� ������ ������� ������� �� ���� ������, � ���� ������ - ������ �������� ������ ����� �� ������.

***

- void Orion.UseSkill(skillIndex/'skillName', ['targetSerial']);

������������ ����� skillName (��� �� ����� ���������� �� ����������� ������).

���� ������ targetSerial - ������������� ����������� ������ ������� ������� �� ���� ������, � ���� ������ - ������ �������� ������ ������������� ������ �� ������.

***

- int Orion.SkillValue(skillIndex/'skillName', ['type'=real]);

�������� �������� ������ skillName (��� �� ����� ���������� �� ����������� ������).

type - ��� ��������: real, base, cap, lock.

���������: ������������� �������� ������ (��������: 3.0 ����� 30, 10.3 - 103, 50.8 - 508, 100.0 - 1000).

***

- void Orion.CloseUO();

������� ������ ����.

***

- void Orion.WarMode([state=switch]);

����������� ������ �����.

state - ������/��������� ����� �����. ���� �� ������ - ����������� ������� ���������.

***

- void Orion.Morph(['graphic'=0]);

�������� ���� ��������� �� graphic.

��� ���������� ��� 0 - ���������� ���� � ��������������� ���������.

***

- void Orion.Resend();

������������� � ��������. ����� ������������ ��� � ��������� ������.

***

- void Orion.Sound(index);

��������� ���� index.

***

- void Orion.EmoteAction('actionName');

������ ��������������� ������ actionName.

***

- void Orion.BuffExists('name');

�������� ������� ������ �� ����� ��� Graphic.

���������: true ���� ���� ����.

***

# Introduced in 2.0.7.0

- void Orion.UseAbility('abilityName');

������������� ����������� abilityName.

abilityName ����� ���� 'Primary', 'Secondary', �������� �������� ����������� (� ����������� ���� ���������) ��� �� ���������� ����� �� '0' �� '30'.

***

- void Orion.UseWrestlingDisarm();

������������� ����������� Wrestling Disarm.

***

- void Orion.UseWrestlingStun();

������������� ����������� Wrestling Stun.

***

# Introduced in 2.0.8.0

- void Orion.InvokeVirture('name');

...

***

# Introduced in 2.0.9.0

- void Orion.PlayWav('filePath');

��������� *.WAV ���� �� ���������� ����.

***

# Introduced in 2.0.12.0

- void Orion.Screenshot();

������� �������� ������.

***

# Introduced in 2.0.13.0

- void Orion.LogOut();

����� �� ���� (������� ������ ����������).

***

- MacroObject Orion.CreateClientMacro(['action'], ['subAction']);

������� ����� ������ �������.

���������: ������ ������ MacroObject ��� null, ���� �� ������� ������.

***

# Introduced in 2.0.14.0

- void Orion.OpenPaperdoll('serial');

������� ��������� ��������� serial.

***

- void Orion.ClosePaperdoll('serial');

������� ��������� ��������� serial.

***

- void Orion.MovePaperdoll('serial', x, y);

����������� ��������� ��������� serial � �������� ���������� x,y.

***

# Introduced in 2.0.15.0

- void Orion.ClientViewRange(range);

���������� View Range ������� ������ range (������� 5, �������� 24).

***

- int Orion.ClientViewRange();

�������� ������� �������� View Range �������.

���������: ViewRange �������.

***

- void Orion.LoadProfile('name');

��������� ������� � ������ name. ���� ������� �� ������ - ������ �� ����������.
