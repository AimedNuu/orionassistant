# ������ ������ ��������� ������� ��� Orion Assistant 2.0.

������ ������:

**NameSpace**.**name**(_**requiredParameters**_, [_optionalParameters=defaultValue_]);

***

flags - ������� ������:

- fast - ����� ��������������� �� ������ ��������� �������;

- near - ����� ���������� ������� � ���������;

- mobile - ����� ������ ����� �������;

- item - ����� ������ �������������� ���������;

- human - ����� ������ ����������� ��������, ����: 0x0190-0x0193, 0x03DB, 0x03DF � 0x03E2;

- live - ����� ������ ����� �������;

- dead - ����� ������ ������� �������, ����: 0x0192 � 0x0193.

- injured - ����� ������� � ���������� ����������� �� (�������� ������ ��� FindFriend/FindEnemy).

- next - ����� ���������� ������� (�������� ������ ��� FindFriend/FindEnemy).

- ignorefriends - ������������ ������ (Introduced in OA 2.0.8.0).

- ignoreenemies - ������������ ������ (Introduced in OA 2.0.8.0).

_��� ������������� human, live ��� dead - ���� mobile ������������ �������������_

_��� ������������� next - ���� fast ������������ �������������_

***

notoriety - ��������� �������� ���������.

- innocent/blue

- friendly/green

- gray

- criminal

- enemy/orange

- murderer/red

- invulnerable/yellow

***

distance - ��������� ������.

������ �������� �������� ����� ��������� ��������� ���������: finddistance, usedistance, opencorpsedistance.

***

- bool Orion.UseType('graphic', ['color'=0xFFFF], ['container'=self], [recurse=true]);

����� ������� �� ���� � ����� � ����������.

- - graphic - ��� ��� ������ ����� ��� ������. 0xFFFF ������������.

- - color - ���� ��� ������ ������ ��� ������. 0xFFFF ������������.

- - container - ���������, � ������� ������������ �����.

- - recurse - ����������� ����� �� ��������������.

(Introduced in OA 2.0.8.2) ���������: true � ������ ��������� �������������.

***

- bool Orion.UseFromGround('graphic', ['color'=0xFFFF], ['distance'=useObjectsDistance], ['flags']);

����� ������� �� ���� � ����� �� �����.

- - graphic - ��� ��� ������ ����� ��� ������. 0xFFFF ������������.

- - color - ���� ��� ������ ������ ��� ������. 0xFFFF ������������.

- - distance - ��������� ������.

- - flags - ����� �������� ������.

(Introduced in OA 2.0.8.2) ���������: true � ������ ��������� �������������.

***

- bool Orion.UseTypeList('listName', ['container'=self], [recurse=true]);

����� ������� �� ������ ������ � ����������.

- - listName - ��� ������ ������.

- - container - ���������, � ������� ������������ �����.

- - recurse - ����������� ����� �� ��������������.

���������: true ���� ������ ������� � ��� �����������.

***

- bool Orion.UseFromGroundList('listName', ['distance'=useObjectsDistance], ['flags']);

����� ������� �� ������ ������ �� �����.

- - listName - ��� ������ ������.

- - distance - ��������� ������.

- - flags - ����� �������� ������.

���������: true ���� ������ ������� � ��� �����������.

***

- StringList Orion.FindType('graphic', ['color'=0xFFFF], ['container'=backpack], ['flags'], ['distance'=searchObjectsDistance], ['notoriety'], [recurse]);

����� ������� �� ���� � �����.

- - graphic - ��� ��� ������ ����� ��� ������. 0xFFFF ������������.

- - color - ���� ��� ������ ������ ��� ������. 0xFFFF ������������.

- - container - ���������, � ������� ������������ �����.

- - flags - ����� �������� ������.

- - distance - ��������� ������.

- - notoriety - ��������� �������� ���������.

- - recurse - ����������� ����� �� ��������������.

���������: ������ ��������� ����������.

***

- void Orion.Ignore('serial', [state=true]);

����������/����� ���� ������������� �� ������ serial.

***

- void Orion.IgnoreReset();

����� ���� ������������� �� ���� ��������.

***

- GameObject Orion.FindObject('serial');

���������: ������ ���� GameObject ��� null.

***

- int Orion.Count('graphic', ['color'=0xFFFF], ['container'=self], ['distance'=searchObjectsDistance], [recurse=true]);

���������� ����� ���������� ��������� (�� ���������� ��������� ��������, � ���������� ���������).

- - graphic - ��� ��� ������ ����� ��� ������. 0xFFFF ������������.

- - color - ���� ��� ������ ������ ��� ������. 0xFFFF ������������.

- - container - ���������, � ������� ������������ �����.

- - distance - ��������� ������.

- - recurse - ����������� ����� �� ��������������.

���������: ���������� ���������.

***

- void Orion.ResetIgnoreList();

�������� ������������� ������ �������������.

***

- void Orion.UseIgnoreList('listName');

������������ ������ ������������� listName.

***

- StringList Orion.FindList('listName', ['container'=backpack], ['flags'], ['distance'=searchObjectsDistance], ['notoriety'], [recurse]);

����� ������� �� ������ ������.

- - listName - ��� ������ ������.

- - container - ���������, � ������� ������������ �����.

- - flags - ����� �������� ������.

- - distance - ��������� ������.

- - notoriety - ��������� �������� ���������.

- - recurse - ����������� ����� �� ��������������.

***

- GameObject Orion.ObjAtLayer('layerName', ['serial'=self]);

���������: ������ ���� GameObject � ��������� ���� � ������� serial ��� null.

***

- String Orion.FindFriend(['flags'=fast], ['distance'=searchObjectsDistance]);

����� ������� �� ������ ������.

- - flags - ����� �������� ������.

- - distance - ��������� ������.

***

- String Orion.FindEnemy(['flags'=fast], ['distance'=searchObjectsDistance]);

����� ������� �� ������ ������.

- - flags - ����� �������� ������.

- - distance - ��������� ������.
