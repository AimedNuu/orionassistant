# ����� �������� ���� ��� Orion Assistant 2.0.3.

������ ������:

**NameSpace**.**name**(_**requiredParameters**_, [_optionalParameters=defaultValue_]);

***

- String menu.Serial();

���������: �������� ����.

***

- String menu.ID();

���������: ������������� ����.

***

- String menu.Name();

���������: �������� ����.

***

- bool menu.IsGrayMenu();

���������: ����� ���� (�������) ��� �������.

***

- void menu.Select(index);

���������� ����� �������� ���� �� ������� �������� �� ����� ����.

***

- void menu.Select('name');

���������� ����� �������� ���� �� ����� �������� �� ����� ����.

***

- void menu.Close();

������� ������� ���� ������ �� �������.

***

- int menu.ItemsCount();

���������: ���������� ��������� � ����.

***

- int menu.ItemID(index);

���������: ID �������� ���� ��� ��������� ��������.

***

- String menu.ItemGraphic(index);

���������: Graphic �������� ���� ��� ��������� ��������.

***

- String menu.ItemColor(index);

���������: Color �������� ���� ��� ��������� ��������.

***

- String menu.ItemName(index);

���������: Name �������� ���� ��� ��������� ��������.
