# �������� Agents -> Party

![agents_party](http://www.imageup.ru/img290/2713079/tab_agents_party.png)

1) ������ ���������� ������� �� ������� ������������� ��������� ����������� � ������.

2) ������ ���������� ������� �� ������� ������������� ��������� ����������� � ������.

3) Add player to accept list - �������� ������ ������ � ���������� ������� � ������ ����������.

4) Edit selected player in accept list - ��������� ��������� � ��������� �������� ������ ����������.

5) Remove selected player from accept list - ������� ��������� ������� �� ������ ����������.

6) Add player to decline list - �������� ������ ������ � ���������� ������� � ������ ��������������.

7) Edit selected player in decline list - ��������� ��������� � ��������� �������� ������ ��������������.

8) Remove selected player from decline list - ������� ��������� ������� �� ������ ��������������.

9) Serial - �������� ����� �������.

10) Name - ��� ������� (��� �� ����� ���� �������� ��������� ���������).

11) Select object from target - ������� ������ � ������� ���� ��������.

12) Auto accept all invites - ������������� ��������� ��� �����������.

13) Auto accept invites from friends - ������������� ��������� ����������� �� ������� � ������ ������.

14) Auto decline all invites - ������������� ��������� ��� �����������.

15) Auto decline invites from enemies - ������������� ��������� ����������� �� ������� � ������ ������.
