#-------------------------------------------------
#
# Project created by QtCreator 2016-10-04T12:53:13
#
#-------------------------------------------------
VERSION = 2.0.16.1

DEFINES += APP_VERSION=\\\"$$VERSION\\\"

QT       += core gui widgets winextras qml

LIBS += -lgdi32 -lMsimg32 -lWinmm -lUser32 -lShell32

TARGET = OrionAssistant
TARGET_EXT = .dll
TEMPLATE = lib
CONFIG += dll precompile_header

include("$$PWD/OrionAssistant/TextOperationDialog/TextOperationDialog.pri")

DESTDIR = $$PWD/../OrionUO/OrionUO/OA

DEFINES += ORIONASSISTANT_LIBRARY

PRECOMPILED_HEADER = orionassistant_global.h

precompile_header:!isEmpty(PRECOMPILED_HEADER) {
DEFINES += USING_PCH
}

SOURCES += \
        orionassistant_global.cpp \
        orionassistantlogger.cpp \
        OrionAssistant/orionassistantform.cpp \
        OrionAssistant/mainscript.cpp \
        Managers/datareader.cpp \
        Managers/datawritter.cpp \
        Managers/packetmanager.cpp \
    OrionAssistant/orionassistant.cpp \
    OrionAssistant/Packets.cpp \
    basequeue.cpp \
    GameObjects/gameobject.cpp \
    GameObjects/gamecharacter.cpp \
    GameObjects/gameitem.cpp \
    GameObjects/gameplayer.cpp \
    GameObjects/gameworld.cpp \
    CommonItems/skill.cpp \
    GUI/objectlistitem.cpp \
    Managers/displaymanager.cpp \
    GUI/displayaction.cpp \
    GUI/typelistitem.cpp \
    Managers/TextParser.cpp \
    CommonItems/displayitem.cpp \
    GUI/displaygrouplistitem.cpp \
    GUI/displaygroupaction.cpp \
    GUI/displayitemlistitem.cpp \
    GUI/coloredpushbutton.cpp \
    Script/command.cpp \
    OrionAssistant/textdialog.cpp \
    GUI/dresslistitem.cpp \
    GUI/dressitemlistitem.cpp \
    OrionAssistant/Target.cpp \
    Managers/spellmanager.cpp \
    Managers/skillmanager.cpp \
    GUI/hotkeyaction.cpp \
    CommonItems/binaryfile.cpp \
    GUI/hotkeybox.cpp \
    Managers/menumanager.cpp \
    CommonItems/menuitem.cpp \
    CommonItems/journalmessage.cpp \
    Managers/journalmanager.cpp \
    Managers/corpsemanager.cpp \
    GUI/extendedplaintextedit.cpp \
    Managers/buffmanager.cpp \
    CommonItems/buffitem.cpp \
    OrionAssistant/scripteditordialog.cpp \
    Script/oascriptengine.cpp \
    GUI/runningscriptlistitem.cpp \
    Script/oascripthandle.cpp \
    Managers/textcommandmanager.cpp \
    Managers/commandmanager.cpp \
    Script/oagameobjectengine.cpp \
    Script/oajournalengine.cpp \
    Script/oatextwindowengine.cpp \
    Script/oascriptenginebase.cpp \
    Script/oaplayerengine.cpp \
    GUI/findlistitem.cpp \
    GUI/findlistitemlistitem.cpp \
    CommonItems/finditem.cpp \
    GUI/ignorelistitem.cpp \
    GUI/ignorelistitemlistitem.cpp \
    CommonItems/ignoreitem.cpp \
    Script/oafileengine.cpp \
    GUI/worldobjectlistitem.cpp \
    Script/oamenuengine.cpp \
    CommonItems/menu.cpp \
    GUI/comboboxdelegate.cpp \
    CommonItems/tradewindow.cpp \
    Managers/trademanager.cpp \
    CommonItems/objectnamereceiver.cpp \
    Managers/UOMapManager.cpp \
    Managers/MappedFile.cpp \
    Managers/filemanager.cpp \
    Managers/gumpmanager.cpp \
    CommonItems/gump.cpp \
    CommonItems/gumpitem.cpp \
    GUI/imagedpushbutton.cpp \
    GUI/filterspeechlistitem.cpp \
    GUI/filtersoundlistitem.cpp \
    GUI/filterreplacelistitem.cpp \
    Script/oagumpengine.cpp \
    Script/oagumphookengine.cpp \
    CommonItems/gumphook.cpp \
    Managers/ClilocManager.cpp \
    OrionAssistant/tabmain.cpp \
    OrionAssistant/tabdisplay.cpp \
    OrionAssistant/tabliststypes.cpp \
    OrionAssistant/tablistsobjects.cpp \
    OrionAssistant/tablistsfind.cpp \
    OrionAssistant/tablistsignore.cpp \
    OrionAssistant/tablistsfriends.cpp \
    OrionAssistant/tablistsenemies.cpp \
    OrionAssistant/tabscripts.cpp \
    OrionAssistant/tabagentsdress.cpp \
    OrionAssistant/tabagentsparty.cpp \
    OrionAssistant/tabskills.cpp \
    OrionAssistant/tabfiltersspeech.cpp \
    OrionAssistant/tabfiltersreplaces.cpp \
    OrionAssistant/tabfilterssound.cpp \
    OrionAssistant/objectinspectorform.cpp \
    GUI/objectinfotreeitem.cpp \
    OrionAssistant/tabobjectinspector.cpp \
    GUI/extendedlistwidget.cpp \
    OrionAssistant/shipcontrolform.cpp \
    Managers/promptmanager.cpp \
    CommonItems/prompt.cpp \
    Managers/partymanager.cpp \
    OrionAssistant/tabagentsshop.cpp \
    GUI/shoplistitem.cpp \
    OrionAssistant/shopprogressform.cpp \
    OrionAssistant/shoplisteditorform.cpp \
    GUI/shopitemlistitem.cpp \
    CommonItems/shopitem.cpp \
    CommonItems/vendorshopitem.cpp \
    Script/oaclientselectedtile.cpp \
    OrionAssistant/housecontrolform.cpp \
    OrionAssistant/animalandvendorcontrolform.cpp \
    Managers/ObjectPropertiesManager.cpp \
    Script/oaclientmacroengine.cpp \
    Managers/clientmacromanager.cpp \
    Managers/contextmenumanager.cpp \
    OrionAssistant/tababout.cpp \
    OrionAssistant/orionmap.cpp \
    GUI/mapaction.cpp \
    GUI/mapimagewidget.cpp \
    OrionAssistant/mapsettingsform.cpp \
	OrionAssistant/tabmacros.cpp \
    Macros/macro.cpp \
    GUI/macroplaintextedit.cpp \
    GUI/syntaxhighlighter.cpp \
    GUI/scriptplaintextedit.cpp \
    GUI/editablelistitem.cpp \
    GUI/macroactionlistitem.cpp \
    GUI/macrolistitem.cpp \
    GUI/macroactionmenuaction.cpp \
    OrionAssistant/hotkeysform.cpp \
    GUI/hotkeytableitem.cpp \
    OrionAssistant/tabhotkeys.cpp
HEADERS +=\
        orionassistant_global.h \
        orionassistantlogger.h \
        OrionAssistant/orionassistantform.h \
        OrionAssistant/mainscript.h \
        Managers/datareader.h \
        Managers/datawritter.h \
        Managers/packetmanager.h \
    OrionAssistant/orionassistant.h \
    OrionAssistant/Packets.h \
    basequeue.h \
    GameObjects/gameobject.h \
    GameObjects/gamecharacter.h \
    GameObjects/gameitem.h \
    GameObjects/gameplayer.h \
    GameObjects/gameworld.h \
    CommonItems/skill.h \
    GUI/objectlistitem.h \
    Managers/displaymanager.h \
    GUI/displayaction.h \
    GUI/typelistitem.h \
    Managers/TextParser.h \
    CommonItems/displayitem.h \
    GUI/displaygrouplistitem.h \
    GUI/displaygroupaction.h \
    GUI/displayitemlistitem.h \
    GUI/coloredpushbutton.h \
    Script/command.h \
    OrionAssistant/textdialog.h \
    GUI/dresslistitem.h \
    GUI/dressitemlistitem.h \
    OrionAssistant/Target.h \
    Managers/spellmanager.h \
    Managers/skillmanager.h \
    GUI/hotkeyaction.h \
    CommonItems/binaryfile.h \
    GUI/hotkeybox.h \
    Managers/menumanager.h \
    CommonItems/menuitem.h \
    CommonItems/journalmessage.h \
    Managers/journalmanager.h \
    Managers/corpsemanager.h \
    GUI/extendedplaintextedit.h \
    Managers/buffmanager.h \
    CommonItems/buffitem.h \
    OrionAssistant/scripteditordialog.h \
    Script/oascriptengine.h \
    GUI/runningscriptlistitem.h \
    Script/oascripthandle.h \
    Managers/textcommandmanager.h \
    Managers/commandmanager.h \
    Script/oagameobjectengine.h \
    Script/oajournalengine.h \
    Script/oatextwindowengine.h \
    Script/oascriptenginebase.h \
    Script/oaplayerengine.h \
    GUI/findlistitem.h \
    GUI/findlistitemlistitem.h \
    CommonItems/finditem.h \
    GUI/ignorelistitem.h \
    GUI/ignorelistitemlistitem.h \
    CommonItems/ignoreitem.h \
    Script/oafileengine.h \
    GUI/worldobjectlistitem.h \
    Script/oamenuengine.h \
    CommonItems/menu.h \
    GUI/comboboxdelegate.h \
    CommonItems/tradewindow.h \
    Managers/trademanager.h \
    CommonItems/objectnamereceiver.h \
    Managers/UOMapManager.h \
    Managers/MappedFile.h \
    OrionAssistant/MulStruct.h \
    Managers/filemanager.h \
    Managers/gumpmanager.h \
    CommonItems/gump.h \
    CommonItems/gumpitem.h \
    GUI/imagedpushbutton.h \
    GUI/filterspeechlistitem.h \
    GUI/filtersoundlistitem.h \
    GUI/filterreplacelistitem.h \
    Script/oagumpengine.h \
    Script/oagumphookengine.h \
    CommonItems/gumphook.h \
    Managers/ClilocManager.h \
    CommonInterfaces.h \
    MulStruct.h \
    PluginInterface.h \
    EnumList.h \
    OrionAssistant/tabmain.h \
    OrionAssistant/tabdisplay.h \
    OrionAssistant/tabliststypes.h \
    OrionAssistant/tablistsobjects.h \
    OrionAssistant/tablistsfind.h \
    OrionAssistant/tablistsignore.h \
    OrionAssistant/tablistsfriends.h \
    OrionAssistant/tablistsenemies.h \
    OrionAssistant/tabscripts.h \
    OrionAssistant/tabagentsdress.h \
    OrionAssistant/tabagentsparty.h \
    OrionAssistant/tabskills.h \
    OrionAssistant/tabfiltersspeech.h \
    OrionAssistant/tabfiltersreplaces.h \
    OrionAssistant/tabfilterssound.h \
    OrionAssistant/objectinspectorform.h \
    GUI/objectinfotreeitem.h \
    OrionAssistant/tabobjectinspector.h \
    GUI/extendedlistwidget.h \
    OrionAssistant/shipcontrolform.h \
    Managers/promptmanager.h \
    CommonItems/prompt.h \
    Managers/partymanager.h \
    OrionAssistant/tabagentsshop.h \
    GUI/shoplistitem.h \
    OrionAssistant/shopprogressform.h \
    OrionAssistant/shoplisteditorform.h \
    GUI/shopitemlistitem.h \
    CommonItems/shopitem.h \
    CommonItems/vendorshopitem.h \
    Script/oaclientselectedtile.h \
    OrionAssistant/housecontrolform.h \
    OrionAssistant/animalandvendorcontrolform.h \
    Managers/ObjectPropertiesManager.h \
    Script/oaclientmacroengine.h \
    Managers/clientmacromanager.h \
    Managers/contextmenumanager.h \
    OrionAssistant/tababout.h \
    OrionAssistant/orionmap.h \
    GUI/mapaction.h \
    GUI/mapimagewidget.h \
    OrionAssistant/mapsettingsform.h \
	OrionAssistant/tabmacros.h \
    Macros/macro.h \
    GUI/macroplaintextedit.h \
    GUI/syntaxhighlighter.h \
    GUI/scriptplaintextedit.h \
    GUI/editablelistitem.h \
    GUI/macroactionlistitem.h \
    GUI/macrolistitem.h \
    GUI/macroactionmenuaction.h \
    OrionAssistant/hotkeysform.h \
    GUI/hotkeytableitem.h \
    OrionAssistant/tabhotkeys.h

unix {
        target.path = /usr/lib
        INSTALLS += target
}

FORMS += \
        OrionAssistant/orionassistantform.ui \
    OrionAssistant/textdialog.ui \
    OrionAssistant/scripteditordialog.ui \
    OrionAssistant/tabmain.ui \
    OrionAssistant/tabdisplay.ui \
    OrionAssistant/tabliststypes.ui \
    OrionAssistant/tablistsobjects.ui \
    OrionAssistant/tablistsfind.ui \
    OrionAssistant/tablistsignore.ui \
    OrionAssistant/tablistsfriends.ui \
    OrionAssistant/tablistsenemies.ui \
    OrionAssistant/tabscripts.ui \
    OrionAssistant/tabagentsdress.ui \
    OrionAssistant/tabagentsparty.ui \
    OrionAssistant/tabskills.ui \
    OrionAssistant/tabfiltersspeech.ui \
    OrionAssistant/tabfiltersreplaces.ui \
    OrionAssistant/tabfilterssound.ui \
    OrionAssistant/objectinspectorform.ui \
    OrionAssistant/tabobjectinspector.ui \
    OrionAssistant/shipcontrolform.ui \
    OrionAssistant/tabagentsshop.ui \
    OrionAssistant/shopprogressform.ui \
    OrionAssistant/shoplisteditorform.ui \
    OrionAssistant/housecontrolform.ui \
    OrionAssistant/animalandvendorcontrolform.ui \
    OrionAssistant/tababout.ui \
    OrionAssistant/orionmap.ui \
    OrionAssistant/mapsettingsform.ui \
    OrionAssistant/tabmacros.ui \
    OrionAssistant/hotkeysform.ui \
    OrionAssistant/tabhotkeys.ui

RESOURCES += \
    resource.qrc
