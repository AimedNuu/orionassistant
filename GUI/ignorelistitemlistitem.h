/***********************************************************************************
**
** IgnoreListItemListItem.h
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef IGNORELISTITEMLISTITEM_H
#define IGNORELISTITEMLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
class CIgnoreListItemListItem : public QListWidgetItem
{
	SETGET(QString, Serial, "")
	SETGET(QString, Graphic, "")
	SETGET(QString, Color, "")
	SETGET(QString, Comment, "")

public:
	CIgnoreListItemListItem(QListWidget *parent);
	virtual ~CIgnoreListItemListItem() {}

	void UpdateText();
};
//----------------------------------------------------------------------------------
#endif // IGNORELISTITEMLISTITEM_H
//----------------------------------------------------------------------------------
