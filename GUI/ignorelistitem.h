/***********************************************************************************
**
** IgnoreListItem.h
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef IGNORELISTITEM_H
#define IGNORELISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
#include <QList>
#include "../CommonItems/ignoreitem.h"
//----------------------------------------------------------------------------------
class CIgnoreListItem : public QListWidgetItem
{
public:
	CIgnoreListItem(const QString &name, QListWidget *parent);
	virtual ~CIgnoreListItem() {}

	QList<CIgnoreItem> m_Items;
};
//----------------------------------------------------------------------------------
#endif // IGNORELISTITEM_H
//----------------------------------------------------------------------------------
