// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** MacroActionMenuAction.cpp
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "macroactionmenuaction.h"
#include "../OrionAssistant/tabmacros.h"
//----------------------------------------------------------------------------------
CMacroActionMenuAction::CMacroActionMenuAction(const QString &text, const int &command, const int &action, const bool &checkedState, QObject *parent)
: QAction(text, parent), m_Command(command), m_Action(action)
{
	if (checkedState)
	{
		setCheckable(true);
		setChecked(true);
	}

	connect(this, SIGNAL(triggered()), SLOT(Clicked()));
}
//----------------------------------------------------------------------------------
void CMacroActionMenuAction::Clicked()
{
	if (g_TabMacros != nullptr)
		g_TabMacros->AddActionFromMenu(m_Command, m_Action);
}
//----------------------------------------------------------------------------------
