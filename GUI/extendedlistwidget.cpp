// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ExtendedListWidget.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "extendedlistwidget.h"
#include <QKeyEvent>
#include <QApplication>
//----------------------------------------------------------------------------------
CExtendedListWidget::CExtendedListWidget(QWidget *parent)
: QListWidget(parent)
{
	OAFUN_DEBUG("");
	this->setDragDropMode(DragDropMode::DragDrop);
	this->setDefaultDropAction(Qt::DropAction::MoveAction);
}
//----------------------------------------------------------------------------------
void CExtendedListWidget::dropEvent(QDropEvent *e)
{
	OAFUN_DEBUG("");
	QListWidget::dropEvent(e);

	emit itemDropped();
}
//----------------------------------------------------------------------------------
void CExtendedListWidget::keyPressEvent(QKeyEvent *event)
{
	OAFUN_DEBUG("");
	QListWidget::keyPressEvent(event);

	if (event->isAutoRepeat())
		return;

	if (event->key() == Qt::Key_Delete && QApplication::focusWidget() == this)
		RemoveCurrentItem();

	event->accept();
}
//----------------------------------------------------------------------------------
void CExtendedListWidget::RemoveCurrentItem()
{
	QListWidgetItem *obj = takeItem(currentRow());

	if (obj != nullptr)
	{
		delete obj;
		emit itemDeleted(this);
	}
}
//----------------------------------------------------------------------------------
QString CExtendedListWidget::GetFreeName(QListWidgetItem *item, const QString &name)
{
	QString currentName = name;
	int currentOffset = 1;

	while (true)
	{
		bool exit = true;

		DFOR(i, count() - 1, 0)
		{
			QListWidgetItem *obj = this->item(i);

			if (obj != nullptr && obj != item)
			{
				if (obj->text().toLower() == currentName)
				{
					exit = false;
					break;
				}
			}
		}

		if (exit)
			return currentName;

		currentName = name + "_" + QString::number(currentOffset++);
	}

	return name;
}
//----------------------------------------------------------------------------------
void CExtendedListWidget::on_itemChanged(QListWidgetItem *item)
{
	QString name = item->text().trimmed();

	if (!name.length())
	{
		QMessageBox::warning(parentWidget(), "Name is empty", "You can't enter a empty name!");
		item->setText(GetFreeName(item, "name"));
		emit itemRenamed(item);
		return;
	}

	QString lowerName = name.toLower();

	DFOR(i, count() - 1, 0)
	{
		QListWidgetItem *obj = this->item(i);

		if (obj != nullptr && obj != item)
		{
			if (obj->text().toLower() == lowerName)
			{
				QMessageBox::warning(parentWidget(), "Name is already exists", "This name is already exists!");
				item->setText(GetFreeName(item, name));
			}
		}
	}

	emit itemRenamed(item);
}
//----------------------------------------------------------------------------------
