// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ExtendedPlainTextEdit.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "extendedplaintextedit.h"
#include "../orionassistant_global.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../GUI/hotkeyaction.h"
#include "../CommonItems/skill.h"
#include <QtWidgets>
//#include "../Managers/textcommandmanager.h"
#include "../Managers/clientmacromanager.h"
#include "../Managers/spellmanager.h"
//----------------------------------------------------------------------------------
CExtendedPlainTextEdit::CExtendedPlainTextEdit(QWidget *parent)
: QPlainTextEdit(parent)
{
	OAFUN_DEBUG("c14_f1");
	setTabStopWidth(24);
	m_LineNumberArea = new CLineNumberArea(this);

	m_SyntaxHightlighter = new CSyntaxHighlighter(this->document());

	connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
	connect(this, SIGNAL(updateRequest(QRect, int)), this, SLOT(updateLineNumberArea(QRect, int)));
	connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightCurrentLine()));

	updateLineNumberAreaWidth(0);
	highlightCurrentLine();

	m_Completer = new QCompleter(this);
	m_Completer->setCompletionMode(QCompleter::PopupCompletion);
	m_Completer->setCaseSensitivity(Qt::CaseInsensitive);
	m_Completer->setWrapAround(false);
	m_Completer->setWidget(this);

	QObject::connect(m_Completer, SIGNAL(activated(QString)), this, SLOT(insertCompletion(QString)));

	setLineWrapMode(QPlainTextEdit::NoWrap);
	setContextMenuPolicy(Qt::CustomContextMenu);
}
//----------------------------------------------------------------------------------
CExtendedPlainTextEdit::~CExtendedPlainTextEdit()
{
	OAFUN_DEBUG("c14_f2");
	RELEASE_POINTER(m_SyntaxHightlighter);
}
//----------------------------------------------------------------------------------
int CExtendedPlainTextEdit::lineNumberAreaWidth()
{
	OAFUN_DEBUG("c14_f3");
	return qMax(30, 10 + fontMetrics().width(QLatin1Char('9')) * QString::number(qMax(1, blockCount())).length());
}
//----------------------------------------------------------------------------------
void CExtendedPlainTextEdit::updateLineNumberAreaWidth(int /* newBlockCount */)
{
	OAFUN_DEBUG("c14_f4");
	setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}
//----------------------------------------------------------------------------------
void CExtendedPlainTextEdit::updateLineNumberArea(const QRect &rect, int dy)
{
	OAFUN_DEBUG("c14_f5");
	if (dy)
		m_LineNumberArea->scroll(0, dy);
	else
		m_LineNumberArea->update(0, rect.y(), m_LineNumberArea->width(), rect.height());

	if (rect.contains(viewport()->rect()))
		updateLineNumberAreaWidth(0);
}
//----------------------------------------------------------------------------------
void CExtendedPlainTextEdit::resizeEvent(QResizeEvent *e)
{
	OAFUN_DEBUG("c14_f6");
	QPlainTextEdit::resizeEvent(e);

	QRect cr = contentsRect();
	m_LineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}
//----------------------------------------------------------------------------------
void CExtendedPlainTextEdit::highlightCurrentLine()
{
	OAFUN_DEBUG("c14_f7");
	QList<QTextEdit::ExtraSelection> extraSelections;

	if (!isReadOnly())
	{
		QTextEdit::ExtraSelection selection;

		static const QColor lineColor = QColor(Qt::yellow).lighter(190);

		selection.format.setBackground(lineColor);
		selection.format.setProperty(QTextFormat::FullWidthSelection, true);
		selection.cursor = textCursor();
		selection.cursor.clearSelection();
		extraSelections.append(selection);
	}

	setExtraSelections(extraSelections);
}
//----------------------------------------------------------------------------------
void CExtendedPlainTextEdit::lineNumberAreaPaintEvent(QPaintEvent *event)
{
	OAFUN_DEBUG("c14_f8");
	QPainter painter(m_LineNumberArea);
	static const QColor grayColor = QColor(Qt::lightGray).lighter(110);
	painter.fillRect(event->rect(), grayColor);

	QTextBlock block = firstVisibleBlock();
	int blockNumber = block.blockNumber();
	int top = (int)blockBoundingGeometry(block).translated(contentOffset()).top();
	int bottom = top + (int)blockBoundingRect(block).height();
	painter.setPen(Qt::black);

	while (block.isValid() && top <= event->rect().bottom())
	{
		if (block.isVisible() && bottom >= event->rect().top())
		{
			QString number = QString::number(blockNumber + 1);
			painter.drawText(-2, top, m_LineNumberArea->width(), fontMetrics().height(), Qt::AlignRight, number);
		}

		block = block.next();
		top = bottom;
		bottom = top + (int)blockBoundingRect(block).height();
		blockNumber++;
	}
}
//----------------------------------------------------------------------------------
//-----------------------------------Completer--------------------------------------
//----------------------------------------------------------------------------------
void CExtendedPlainTextEdit::insertCompletion(const QString& completion)
{
	OAFUN_DEBUG("c14_f9");
	if (m_Completer->widget() != this)
		return;

	QTextCursor tc = textCursor();
	tc.select(QTextCursor::WordUnderCursor);
	tc.removeSelectedText();
	tc.insertText(completion);
	setTextCursor(tc);
}
//----------------------------------------------------------------------------------
QString CExtendedPlainTextEdit::textUnderCursor() const
{
	OAFUN_DEBUG("c14_f10");
	QTextCursor tc = textCursor();
	tc.select(QTextCursor::WordUnderCursor);
	return tc.selectedText();
}
//----------------------------------------------------------------------------------
void CExtendedPlainTextEdit::focusInEvent(QFocusEvent *e)
{
	OAFUN_DEBUG("c14_f11");
	if (m_Completer)
		m_Completer->setWidget(this);

	QPlainTextEdit::focusInEvent(e);
}
//----------------------------------------------------------------------------------
void CExtendedPlainTextEdit::keyPressEvent(QKeyEvent *e)
{
	OAFUN_DEBUG("c14_f12");

	if ((e->modifiers() & Qt::ControlModifier) && e->key() == Qt::Key_Q)
		emit signal_CtrlQPressed();

	if (m_Completer)
	{
		QAbstractItemView *popup = m_Completer->popup();

		if (popup->isVisible())
		{
			switch (e->key())
			{
				case Qt::Key_Enter:
				case Qt::Key_Return:
				case Qt::Key_Escape:
				case Qt::Key_Tab:
				case Qt::Key_Backtab:
				{
					e->ignore();
					return;
				}
				case Qt::Key_Space:
				{
					m_Completer->popup()->hide();
					QPlainTextEdit::keyPressEvent(e);
					return;
				}
				default:
					break;
			}
		}

		const bool shiftPressed = e->modifiers() & Qt::ShiftModifier;

		bool isShortcut = ((e->modifiers() & Qt::ControlModifier) && e->key() == Qt::Key_E); // CTRL+E

		if (!isShortcut) // do not process the shortcut when we have a completer
			QPlainTextEdit::keyPressEvent(e);

		const bool ctrlOrShift = ((e->modifiers() & Qt::ControlModifier) || shiftPressed);

		if (ctrlOrShift && e->text().isEmpty())
			return;

		static QString eow("~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-="); // end of word
		bool hasModifier = (e->modifiers() != Qt::NoModifier) && !ctrlOrShift;
		QString completionPrefix = textUnderCursor();

		if (e->key() == Qt::Key_Enter || e->key() == Qt::Key_Return)
		{
			QString prevBlockText = document()->findBlockByLineNumber(textCursor().blockNumber() - 1).text();

			for (int i = 0; i < prevBlockText.length(); i++)
			{
				char ch = (char)prevBlockText.at(i).cell();

				if (ch != ' ' && ch != '\t')
				{
					if (i)
						prevBlockText.resize(i);
					else
						prevBlockText = "";
				}
			}

			if (prevBlockText.length())
			{
				QTextCursor tc = textCursor();
				tc.insertText(prevBlockText);
				setTextCursor(tc);
			}
		}

		if (!isShortcut && (hasModifier || e->text().isEmpty() || completionPrefix.length() < 1 || eow.contains(e->text().right(1))))
		{
			popup->hide();
			return;
		}

		if (e->key() >= Qt::Key_E && e->key() <= Qt::Key_Z)
		{
			if (completionPrefix != m_Completer->completionPrefix())
			{
				m_Completer->setCompletionPrefix(completionPrefix);
				popup->setCurrentIndex(m_Completer->completionModel()->index(0, 0));
			}

			QRect cr = cursorRect();
			cr.setWidth(popup->sizeHintForColumn(0) + popup->verticalScrollBar()->sizeHint().width());
			m_Completer->complete(cr); // popup it up!
		}
	}
	else
		QPlainTextEdit::keyPressEvent(e);
}
//----------------------------------------------------------------------------------
