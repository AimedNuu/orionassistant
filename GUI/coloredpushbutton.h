/***********************************************************************************
**
** ColoredPushButton.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef COLOREDPUSHBUTTON_H
#define COLOREDPUSHBUTTON_H
//----------------------------------------------------------------------------------
#include <QPushButton>
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class QColoredPushButton : public QPushButton
{
	Q_OBJECT

	SETGET(QColor, Color, QColor())

public:
	QColoredPushButton(QWidget *parent = nullptr);

	virtual ~QColoredPushButton() {}

	void ResetColor();

	void ChangeColor(const QColor &color);

	void ChangeColor(const uint &color);

	uint GetUIntColor();
};
//----------------------------------------------------------------------------------
#endif // COLOREDPUSHBUTTON_H
//----------------------------------------------------------------------------------
