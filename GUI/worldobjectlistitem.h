/***********************************************************************************
**
** WorldObjectListItem.h
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef WORLDOBJECTLISTITEM_H
#define WORLDOBJECTLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
class CWorldObjectListItem : public QListWidgetItem
{
	SETGET(uint, Serial, 0)
	SETGET(QString, Name, "")

public:
	CWorldObjectListItem(const uint &serial, const QString &name, const bool &checked, QListWidget *parent);
	virtual ~CWorldObjectListItem() {}

	void UpdateText();
};
//----------------------------------------------------------------------------------
#endif // WORLDOBJECTLISTITEM_H
//----------------------------------------------------------------------------------
