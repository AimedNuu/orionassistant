// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ComboboxDelegate.cpp
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "ComboboxDelegate.h"
//----------------------------------------------------------------------------------
void CComboDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	OAFUN_DEBUG("");
	QStyleOptionViewItem newOption(option);

	if (index.data().toString().at(0).cell() == '#')
		newOption.palette.setColor(QPalette::Text, Qt::darkGray);

	drawBackground(painter, newOption, index);
	QItemDelegate::paint(painter, newOption, index);
}
//----------------------------------------------------------------------------------
void CComboDelegate::drawBackground(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	OAFUN_DEBUG("");
	QString text = index.data().toString();

	if (text.at(0).cell() == '#')
		painter->fillRect(option.rect, Qt::lightGray);
	else
		painter->fillRect(option.rect, Qt::white);
}
//----------------------------------------------------------------------------------
