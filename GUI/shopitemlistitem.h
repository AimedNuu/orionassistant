/***********************************************************************************
**
** ShopItemListItem.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SHOPITEMLISTITEM_H
#define SHOPITEMLISTITEM_H
//----------------------------------------------------------------------------------
#include <QListWidget>
#include <QListWidgetItem>
#include "../orionassistant_global.h"
#include "../CommonItems/shopitem.h"
//----------------------------------------------------------------------------------
class CShopItemListItem : public QListWidgetItem
{
public:
	CShopItemListItem(const CShopItem &shopItem, QListWidget *parent);

	CShopItem m_ShopItem;

	void UpdateText();
};
//----------------------------------------------------------------------------------
#endif // SHOPITEMLISTITEM_H
//----------------------------------------------------------------------------------
