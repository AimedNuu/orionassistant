// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ObjectListItem.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "objectlistitem.h"
#include "../OrionAssistant/orionassistant.h"
//----------------------------------------------------------------------------------
CObjectListItem::CObjectListItem(const QString &name, const QString &serial, QListWidget *parent)
	: QListWidgetItem(name, parent)
{
	OAFUN_DEBUG("");
	m_Serial = COrionAssistant::TextToSerial(serial);
}
//----------------------------------------------------------------------------------
