/***********************************************************************************
**
** MacroPlainTextEdit.h
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef MACROPLAINTEXTEDIT_H
#define MACROPLAINTEXTEDIT_H
//----------------------------------------------------------------------------------
#include "extendedplaintextedit.h"
//----------------------------------------------------------------------------------
class CMacroPlainTextEdit : public CExtendedPlainTextEdit
{
	Q_OBJECT

private slots:
	void onCustomContextMenuRequested(const QPoint &pos);

public:
	CMacroPlainTextEdit(QWidget *parent = nullptr);

	virtual ~CMacroPlainTextEdit() {}
};
//----------------------------------------------------------------------------------
#endif // MACROPLAINTEXTEDIT_H
//----------------------------------------------------------------------------------
