/***********************************************************************************
**
** ImagedPushButton.h
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef IMAGEDPUSHBUTTON_H
#define IMAGEDPUSHBUTTON_H
//----------------------------------------------------------------------------------
#include <QPushButton>
//----------------------------------------------------------------------------------
class QImagedPushButton : public QPushButton
{
	Q_OBJECT

public:
	QImagedPushButton(QWidget *parent = nullptr);
};
//----------------------------------------------------------------------------------
#endif // IMAGEDPUSHBUTTON_H
//----------------------------------------------------------------------------------
