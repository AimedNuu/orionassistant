// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** HotkeyTableItem.cpp
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "hotkeytableitem.h"
#include "hotkeybox.h"
//----------------------------------------------------------------------------------
CHotkeyTableItem::CHotkeyTableItem()
: QTableWidgetItem()
{
	setCheckState(Qt::Unchecked);
	setText(QHotkeyBox::GetHotkeyText(m_Key, m_Modifiers));
}
//----------------------------------------------------------------------------------
CHotkeyTableItem::CHotkeyTableItem(const uint &key, const uchar &modifiers, const uint &type, const uint &action, const QString &scriptOrMacroName, const QString &externalCode, const bool &runningMode)
: QTableWidgetItem(), m_Key(key), m_Modifiers(modifiers), m_Type(type), m_Action(action),
m_ScriptOrMacroName(scriptOrMacroName), m_ExternalCode(externalCode), m_RunningMode(runningMode)
{
	if (key != 0)
		setCheckState(Qt::Checked);
	else
		setCheckState(Qt::Unchecked);

	setText(QHotkeyBox::GetHotkeyText(m_Key, m_Modifiers));
}
//----------------------------------------------------------------------------------
