// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** MapImageWidget.cpp
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "mapimagewidget.h"
#include "../OrionAssistant/orionmap.h"
#include <QPainter>
#include <QPaintEvent>
#include <QMouseEvent>
//----------------------------------------------------------------------------------
void CMapImageWidget::paintEvent(QPaintEvent *event)
{
	event->accept();

	if (g_OrionMap != nullptr)
	{
		QPainter painter(this);
		g_OrionMap->mapPaintEvent(painter);
	}
}
//----------------------------------------------------------------------------------
void CMapImageWidget::mousePressEvent(QMouseEvent *event)
{
	event->accept();
	if (g_OrionMap != nullptr)
		g_OrionMap->mapMousePressEvent(event);
}
//----------------------------------------------------------------------------------
void CMapImageWidget::mouseReleaseEvent(QMouseEvent *event)
{
	event->accept();
	if (g_OrionMap != nullptr)
		g_OrionMap->mapMouseReleaseEvent(event);
}
//----------------------------------------------------------------------------------
void CMapImageWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
	event->accept();
	if (g_OrionMap != nullptr)
		g_OrionMap->mapMouseDoubleClickEvent(event);
}
//----------------------------------------------------------------------------------
void CMapImageWidget::mouseMoveEvent(QMouseEvent *event)
{
	event->accept();
	if (g_OrionMap != nullptr)
		g_OrionMap->mapMouseMoveEvent(event);
}
//----------------------------------------------------------------------------------
void CMapImageWidget::wheelEvent(QWheelEvent *event)
{
	event->accept();
	if (g_OrionMap != nullptr)
		g_OrionMap->mapWheelEvent(event);
}
//----------------------------------------------------------------------------------
