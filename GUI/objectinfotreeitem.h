/***********************************************************************************
**
** ObjectInfoTreeItem.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OBJECTINFOTREEITEM_H
#define OBJECTINFOTREEITEM_H
//----------------------------------------------------------------------------------
#include <QTreeWidgetItem>
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
enum OBJECT_INFO_KEY
{
	OIK_GAME_OBJECT = -1,
	OIK_GAME_ITEM = -2,
	OIK_GAME_CHARACTER = -3,
	OIK_GAME_PLAYER = -4,
	OIK_SERIAL = 1,
	OIK_CONTAINER,
	OIK_TOP_OBJECT,
	OIK_NAME,
	OIK_GRAPHIC,
	OIK_COLOR,
	OIK_COUNT,
	OIK_X,
	OIK_Y,
	OIK_Z,
	OIK_MAP,
	OIK_FLAGS,
	OIK_FLAGS_FROZEN,
	OIK_FLAGS_POISONED,
	OIK_FLAGS_YELLOW_HITS,
	OIK_FLAGS_IGNORE_CHARACTERS,
	OIK_FLAGS_LOCKED,
	OIK_FLAGS_IN_WAR_MODE,
	OIK_FLAGS_HIDDEN,
	OIK_FLAGS_FLYING,
	OIK_IGNORED,
	OIK_LAYER,
	OIK_HITS,
	OIK_MAX_HITS,
	OIK_MANA,
	OIK_MAX_MANA,
	OIK_STAM,
	OIK_MAX_STAM,
	OIK_SEX,
	OIK_RACE,
	OIK_DIRECTION,
	OIK_RUNNING,
	OIK_NOTORIETY,
	OIK_CAN_CHANGE_NAME,
	OIK_STR,
	OIK_INT,
	OIK_DEX,
	OIK_STATS_CAP,
	OIK_LOCK_STR,
	OIK_LOCK_INT,
	OIK_LOCK_DEX,
	OIK_WEIGHT,
	OIK_MAX_WEIGHT,
	OIK_ARMOR,
	OIK_GOLD,
	OIK_WARMODE,
	OIK_FOLLOWERS,
	OIK_MAX_FOLLOWERS,
	OIK_FIRE_RESISTANCE,
	OIK_COLD_RESISTANCE,
	OIK_POISON_RESISTANCE,
	OIK_ENERGY_RESISTANCE,
	OIK_LUCK,
	OIK_MIN_DAMAGE,
	OIK_MAX_DAMAGE,
	OIK_TITHING_POINTS,
	OIK_STEALTH_STEPS
};
//----------------------------------------------------------------------------------
class CObjectInfoTreeItem : public QTreeWidgetItem
{
	SETGET(bool, Group, false)
	SETGETE(QString, Name, "", OnNameChange)
	SETGETE(QString, Value, "", OnValueChange)
	SETGET(int, Key, 0)

public:
	CObjectInfoTreeItem(const bool &group, const QString &name, const QString &value, const int &key);
};
//----------------------------------------------------------------------------------
#endif // OBJECTINFOTREEITEM_H
//----------------------------------------------------------------------------------
