// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ShopListItem.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "shoplistitem.h"
#include "../OrionAssistant/shoplisteditorform.h"
//----------------------------------------------------------------------------------
CShopListItem::CShopListItem(const QString &name, QListWidget *parent)
: QListWidgetItem(name, parent)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CShopListItem::~CShopListItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
