/***********************************************************************************
**
** SyntaxHighlighter.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SYNTAXHIGHLIGHTER_H
#define SYNTAXHIGHLIGHTER_H
//----------------------------------------------------------------------------------
#include <QSyntaxHighlighter>
#include <QTextCharFormat>
//----------------------------------------------------------------------------------
QT_BEGIN_NAMESPACE
class QTextDocument;
QT_END_NAMESPACE
//----------------------------------------------------------------------------------
struct CHighlightingRule
{
	QRegExp Pattern;
	QTextCharFormat Format;
};
//----------------------------------------------------------------------------------
class CSyntaxHighlighter : public QSyntaxHighlighter
{
	Q_OBJECT

public:
	CSyntaxHighlighter(QTextDocument *parent = 0) : QSyntaxHighlighter(parent) {}

	QList<CHighlightingRule> m_HighlightingRules;

protected:
	void highlightBlock(const QString &text) Q_DECL_OVERRIDE;
};
//----------------------------------------------------------------------------------
#endif // SYNTAXHIGHLIGHTER_H
//----------------------------------------------------------------------------------
