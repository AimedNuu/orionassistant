/***********************************************************************************
**
** MacroActionMenuAction.h
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef MACROACTIONMENUACTION_H
#define MACROACTIONMENUACTION_H
//----------------------------------------------------------------------------------
#include <QAction>
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CMacroActionMenuAction : public QAction
{
	Q_OBJECT

	SETGET(int, Command, 0)
	SETGET(int, Action, 0)

public:
	CMacroActionMenuAction(const QString &text, const int &command, const int &action, const bool &checkedState, QObject *parent);
	virtual ~CMacroActionMenuAction() {}

public slots:
	void Clicked();
};
//----------------------------------------------------------------------------------
#endif // MACROACTIONMENUACTION_H
//----------------------------------------------------------------------------------
