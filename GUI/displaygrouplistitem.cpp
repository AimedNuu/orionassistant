// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** DisplayGroupListItem.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "DisplayGroupListItem.h"
//----------------------------------------------------------------------------------
CDisplayGroupListItem::CDisplayGroupListItem(const QString &name, const QString &titleData, QListWidget *parent)
: QListWidgetItem(name, parent), m_TitleData(titleData)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
