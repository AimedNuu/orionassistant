/***********************************************************************************
**
** MapAction.h
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef MAPACTION_H
#define MAPACTION_H
//----------------------------------------------------------------------------------
#include <QAction>
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CMapAction : public QAction
{
	Q_OBJECT

	SETGET(uint, Type, 0)
	SETGET(uint, Extra, 0)

public:
	CMapAction(const QString &text, const uint &type, const uint &extra, const bool &checked, QObject *parent);
	virtual ~CMapAction() {}

public slots:
	void ClickedAction();
};
//----------------------------------------------------------------------------------
#endif // MAPACTION_H
//----------------------------------------------------------------------------------
