// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ObjectInfoTreeItem.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "objectinfotreeitem.h"
//----------------------------------------------------------------------------------
CObjectInfoTreeItem::CObjectInfoTreeItem(const bool &group, const QString &name, const QString &value, const int &key)
: QTreeWidgetItem(), m_Group(group), m_Name(name), m_Value(value), m_Key(key)
{
	OAFUN_DEBUG("");
	setText(0, name + value);
}
//----------------------------------------------------------------------------------
void CObjectInfoTreeItem::OnNameChange(const QString &val)
{
	OAFUN_DEBUG("");
	setText(0, val + m_Value);
}
//----------------------------------------------------------------------------------
void CObjectInfoTreeItem::OnValueChange(const QString &val)
{
	OAFUN_DEBUG("");
	setText(0, m_Name + val);
}
//----------------------------------------------------------------------------------
