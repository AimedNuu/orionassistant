// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** FindListItemListItem.cpp
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "findlistitemlistitem.h"
//----------------------------------------------------------------------------------
CFindListItemListItem::CFindListItemListItem(QListWidget *parent)
: QListWidgetItem("item", parent)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
void CFindListItemListItem::UpdateText()
{
	OAFUN_DEBUG("");
	QString text = m_Graphic + " : " + m_Color;

	if (m_Comment.length())
		text += " //" + m_Comment;

	setText(text);
}
//----------------------------------------------------------------------------------
