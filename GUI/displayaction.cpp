// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** DisplayAction.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "displayaction.h"
#include "../OrionAssistant/tabdisplay.h"
//----------------------------------------------------------------------------------
CDisplayAction::CDisplayAction(const QString &text, QObject *parent)
: QAction(text, parent)
{
	OAFUN_DEBUG("");
	connect(this, SIGNAL(triggered()), SLOT(ClickedDisplayAction()));
}
//----------------------------------------------------------------------------------
void CDisplayAction::ClickedDisplayAction()
{
	OAFUN_DEBUG("");
	g_TabDisplay->InsertDisplayAction(text());
}
//----------------------------------------------------------------------------------
