/***********************************************************************************
**
** FilterSpeechListItem.h
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef FILTERSPEECHLISTITEM_H
#define FILTERSPEECHLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
class CFilterSpeechListItem : public QListWidgetItem
{
	SETGET(QString, Text, "")
	SETGET(QString, Color, "")
	SETGET(bool, FullMatch, false)

public:
	CFilterSpeechListItem(const QString &text, const QString &color, const bool &fullMatch, QListWidget *parent);
	virtual ~CFilterSpeechListItem() {}

	void UpdateText();
};
//----------------------------------------------------------------------------------
#endif // FILTERSPEECHLISTITEM_H
//----------------------------------------------------------------------------------
