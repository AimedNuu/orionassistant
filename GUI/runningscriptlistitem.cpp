// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** RunningScriptListItem.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "runningscriptlistitem.h"
//----------------------------------------------------------------------------------
CRunningScriptListItem::CRunningScriptListItem()
: QListWidgetItem("CRunningScriptListItem", nullptr)
{
	OAFUN_DEBUG("c15_f0");
	setIcon(QIcon(QPixmap(":/resource/images/macro_start.png")));
}
//----------------------------------------------------------------------------------
CRunningScriptListItem::CRunningScriptListItem(const QString &name, const QStringList &args, const uint &key, const uchar &modifiers, QListWidget *parent)
: QListWidgetItem(name, parent), m_Key(key), m_Modifiers(modifiers), m_Args(args)
{
	OAFUN_DEBUG("c15_f1");
	setIcon(QIcon(QPixmap(":/resource/images/macro_start.png")));
}
//----------------------------------------------------------------------------------
CRunningScriptListItem::~CRunningScriptListItem()
{
	OAFUN_DEBUG("c15_f2");
	if (m_ScriptHandle != nullptr)
	{
		//delete m_ScriptHandle;
		m_ScriptHandle = nullptr;
	}
}
//----------------------------------------------------------------------------------
void CRunningScriptListItem::Terminate()
{
	OAFUN_DEBUG("c15_f3");
	if (m_ScriptHandle != nullptr)
		m_ScriptHandle->TerminateScript();
}
//----------------------------------------------------------------------------------
bool CRunningScriptListItem::GetPaused()
{
	OAFUN_DEBUG("c15_f4");
	if (m_ScriptHandle != nullptr)
		return m_ScriptHandle->GetPaused();

	return false;
}
//----------------------------------------------------------------------------------
void CRunningScriptListItem::SetPaused(const bool &value)
{
	OAFUN_DEBUG("c15_f5");
	if (m_ScriptHandle != nullptr)
		m_ScriptHandle->SetPaused(value);
}
//----------------------------------------------------------------------------------
void CRunningScriptListItem::Start(const QString &scriptBody)
{
	OAFUN_DEBUG("c15_f6");
	m_ScriptHandle = new COAScriptHandle(this, text(), m_Args, scriptBody);
}
//----------------------------------------------------------------------------------
