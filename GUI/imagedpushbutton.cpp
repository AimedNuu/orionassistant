// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ImagedPushButton.cpp
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "imagedpushbutton.h"
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
QImagedPushButton::QImagedPushButton(QWidget *parent)
: QPushButton(parent)
{
	OAFUN_DEBUG("");
	setStyleSheet
	(
		"QPushButton\r\n"
		"{\r\n"
		"	background: none;\r\n"
		"	border-radius: 0px;\r\n"
		"}\r\n"
		"QPushButton:hover\r\n"
		"{\r\n"
		"	background: solid rgb(192, 192, 192);\r\n"
		"}\r\n"
		"QPushButton:pressed\r\n"
		"{\r\n"
		"	background: solid rgb(160, 160, 160);\r\n"
		"	margin-top: 2px;\r\n"
		"	margin-left: 2px;\r\n"
		"}"
	);
}
//----------------------------------------------------------------------------------
