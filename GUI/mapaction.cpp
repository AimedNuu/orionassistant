// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** MapAction.cpp
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "mapaction.h"
#include "../OrionAssistant/orionmap.h"
//----------------------------------------------------------------------------------
CMapAction::CMapAction(const QString &text, const uint &type, const uint &extra, const bool &checked, QObject *parent)
: QAction(text, parent), m_Type(type), m_Extra(extra)
{
	OAFUN_DEBUG("");
	connect(this, SIGNAL(triggered()), this, SLOT(ClickedAction()));
	setCheckable(true);
	setChecked(checked);
}
//----------------------------------------------------------------------------------
void CMapAction::ClickedAction()
{
	OAFUN_DEBUG("");
	if (parent() != nullptr)
		g_OrionMap->OnActionSelect(m_Type, m_Extra);
}
//----------------------------------------------------------------------------------
