/***********************************************************************************
**
** FilterReplaceListItem.h
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef FILTERREPLACELISTITEM_H
#define FILTERREPLACELISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
enum REPLACE_TEXT_TYPE
{
	RTT_ALL = 0,
	RTT_ASCII,
	RTT_UNICODE
};
//----------------------------------------------------------------------------------
enum REPLACE_TEXT_MODE
{
	RTM_ALL = 0,
	RTM_FRIENDS_AND_YOU,
	RTM_ENEMIES,
	RTM_OTHERS
};
//----------------------------------------------------------------------------------
class CFilterReplaceListItem : public QListWidgetItem
{
	SETGET(QString, OriginalText, "")
	SETGET(QString, OriginalColor, "")
	SETGET(QString, ReplacedText, "")
	SETGET(QString, ReplacedColor, "")
	SETGET(int, Type, 0)
	SETGET(int, Mode, 0)
	SETGET(bool, FullMatch, false)

public:
	CFilterReplaceListItem(const QString &originalText, const QString &originalColor, const QString &replacedText, const QString &replacedColor, const int &type, const int &mode, const bool &fullMatch, QListWidget *parent);
	virtual ~CFilterReplaceListItem() {}

	void UpdateText();
};
//----------------------------------------------------------------------------------
#endif // FILTERREPLACELISTITEM_H
//----------------------------------------------------------------------------------
