// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** DisplayGroupAction.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "displaygroupaction.h"
#include "../OrionAssistant/tabdisplay.h"
//----------------------------------------------------------------------------------
CDisplayGroupAction::CDisplayGroupAction(const QString &text, QObject *parent)
: QAction(text, parent)
{
	OAFUN_DEBUG("");
	connect(this, SIGNAL(triggered()), SLOT(ClickedDisplayGroupAction()));
}
//----------------------------------------------------------------------------------
void CDisplayGroupAction::ClickedDisplayGroupAction()
{
	OAFUN_DEBUG("");
	g_TabDisplay->InsertDisplayGroupAction(text());
}
//----------------------------------------------------------------------------------
