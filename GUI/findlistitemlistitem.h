/***********************************************************************************
**
** FindListItemListItem.h
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef FINDLISTITEMLISTITEM_H
#define FINDLISTITEMLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
class CFindListItemListItem : public QListWidgetItem
{
	SETGET(QString, Graphic, "")
	SETGET(QString, Color, "")
	SETGET(QString, Comment, "")

public:
	CFindListItemListItem(QListWidget *parent);
	virtual ~CFindListItemListItem() {}

	void UpdateText();
};
//----------------------------------------------------------------------------------
#endif // FINDLISTITEMLISTITEM_H
//----------------------------------------------------------------------------------
