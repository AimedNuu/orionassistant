/***********************************************************************************
**
** DressListItem.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef DRESSLISTITEM_H
#define DRESSLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
class CDressListItem : public QListWidgetItem
{
public:
	CDressListItem(const QString &name, QListWidget *parent);
	virtual ~CDressListItem() {}

	uint m_Items[25];
};
//----------------------------------------------------------------------------------
#endif // DRESSLISTITEM_H
//----------------------------------------------------------------------------------
