// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** MacroActionListItem.cpp
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "macroactionlistitem.h"
//----------------------------------------------------------------------------------
CMacroActionListItem::CMacroActionListItem(CMacro *macro)
: QListWidgetItem(macro->GetText(), nullptr), m_Macro(macro->Copy())
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CMacroActionListItem::~CMacroActionListItem()
{
	OAFUN_DEBUG("");
	RELEASE_POINTER(m_Macro);
}
//----------------------------------------------------------------------------------
