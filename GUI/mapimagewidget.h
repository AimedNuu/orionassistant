/***********************************************************************************
**
** MapImageWidget.h
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef MAPIMAGEWIDGET_H
#define MAPIMAGEWIDGET_H
//----------------------------------------------------------------------------------
#include <QWidget>
//----------------------------------------------------------------------------------
class CMapImageWidget : public QWidget
{
protected:
	virtual void paintEvent(QPaintEvent *event);
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);
	virtual void mouseDoubleClickEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void wheelEvent(QWheelEvent *event);

public:
	CMapImageWidget(QWidget *parent = nullptr) : QWidget(parent) {}
};
//----------------------------------------------------------------------------------
#endif // MAPIMAGEWIDGET_H
//----------------------------------------------------------------------------------
