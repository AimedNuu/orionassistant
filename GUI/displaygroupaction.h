/***********************************************************************************
**
** DisplayGroupAction.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef DISPLAYGROUPACTION_H
#define DISPLAYGROUPACTION_H
//----------------------------------------------------------------------------------
#include <QAction>
//----------------------------------------------------------------------------------
class CDisplayGroupAction : public QAction
{
	Q_OBJECT

public:
	CDisplayGroupAction(const QString &text, QObject *parent);
	virtual ~CDisplayGroupAction() {}

public slots:
	void ClickedDisplayGroupAction();
};
//----------------------------------------------------------------------------------
#endif // DISPLAYGROUPACTION_H
//----------------------------------------------------------------------------------
