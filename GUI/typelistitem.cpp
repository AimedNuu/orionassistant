// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TypeListItem.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "typelistitem.h"
#include "../OrionAssistant/orionassistant.h"
//----------------------------------------------------------------------------------
CTypeListItem::CTypeListItem(const QString &name, const QString &type, QListWidget *parent)
: QListWidgetItem(name, parent)
{
	OAFUN_DEBUG("");
	m_Type = COrionAssistant::TextToGraphic(type);
}
//----------------------------------------------------------------------------------
