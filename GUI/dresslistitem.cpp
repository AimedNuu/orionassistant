// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** DressListItem.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "dresslistitem.h"
//----------------------------------------------------------------------------------
CDressListItem::CDressListItem(const QString &name, QListWidget *parent)
: QListWidgetItem(name, parent)
{
	OAFUN_DEBUG("");
	memset(&m_Items[0], 0, sizeof(m_Items));
}
//----------------------------------------------------------------------------------
