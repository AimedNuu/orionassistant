/***********************************************************************************
**
** TypeListItem.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TYPELISTITEM_H
#define TYPELISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
class CTypeListItem : public QListWidgetItem
{
	SETGET(ushort, Type, 0)

public:
	CTypeListItem(const QString &name, const QString &type, QListWidget *parent);
	virtual ~CTypeListItem() {}
};
//----------------------------------------------------------------------------------
#endif // TYPELISTITEM_H
//----------------------------------------------------------------------------------
