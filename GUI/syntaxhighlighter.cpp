// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** SyntaxHighlighter.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "SyntaxHighlighter.h"
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
void CSyntaxHighlighter::highlightBlock(const QString &textIn)
{
	OAFUN_DEBUG("c30_f1");
	foreach (const CHighlightingRule &rule, m_HighlightingRules)
	{
		QRegExp expression(rule.Pattern);

		int index = expression.indexIn(textIn);

		while (index >= 0)
		{
			int length = expression.matchedLength();
			setFormat(index, length, rule.Format);
			index = expression.indexIn(textIn, index + length);
		}
	}

	setCurrentBlockState(0);
}
//----------------------------------------------------------------------------------
