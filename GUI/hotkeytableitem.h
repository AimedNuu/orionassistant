/***********************************************************************************
**
** HotkeyTableItem.h
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef HOTKEYTABLEITEM_H
#define HOTKEYTABLEITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CHotkeyTableItem : public QTableWidgetItem
{
	SETGET(uint, Key, 0)
	SETGET(uchar, Modifiers, 0)
	SETGET(uint, Type, 0)
	SETGET(uint, Action, 0)
	SETGET(QString, ScriptOrMacroName, "")
	SETGET(QString, ExternalCode, "")
	SETGET(bool, RunningMode, false)
	SETGET(QString, ExternalName, "")

public:
	CHotkeyTableItem();
	CHotkeyTableItem(const uint &key, const uchar &modifiers, const uint &type, const uint &action, const QString &scriptOrMacroName, const QString &externalCode, const bool &runningMode);
	virtual ~CHotkeyTableItem() {}
};
//----------------------------------------------------------------------------------
#endif // HOTKEYTABLEITEM_H
//----------------------------------------------------------------------------------
