/***********************************************************************************
**
** DressItemListItem.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef DRESSITEMLISTITEM_H
#define DRESSITEMLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
class CDressItemListItem : public QListWidgetItem
{
	SETGET(uint, Serial, 0)
	SETGET(int, Layer, 0)

public:
	CDressItemListItem(const QString &name, QListWidget *parent);
	virtual ~CDressItemListItem() {}

	void UpdateText();
};
//----------------------------------------------------------------------------------
#endif // DRESSITEMLISTITEM_H
//----------------------------------------------------------------------------------
