/***********************************************************************************
**
** MacroActionListItem.h
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef MACROACTIONLISTITEM_H
#define MACROACTIONLISTITEM_H
//----------------------------------------------------------------------------------
#include "../Macros/macro.h"
#include <QListWidgetItem>
//----------------------------------------------------------------------------------
class CMacroActionListItem : public QListWidgetItem
{
	SETGET(CMacro*, Macro, nullptr)

public:
	CMacroActionListItem(CMacro *macro);
	virtual ~CMacroActionListItem();
};
//----------------------------------------------------------------------------------
#endif // MACROACTIONLISTITEM_H
//----------------------------------------------------------------------------------
