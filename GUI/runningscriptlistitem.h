/***********************************************************************************
**
** RunningScriptListItem.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef RUNNINGSCRIPTLISTITEM_H
#define RUNNINGSCRIPTLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
#include "../Script/oascripthandle.h"
//----------------------------------------------------------------------------------
class CRunningScriptListItem : public QListWidgetItem
{
	SETGET(uint, Key, 0)
	SETGET(uchar, Modifiers, 0)

private:
	QStringList m_Args;

public:
	CRunningScriptListItem();
	CRunningScriptListItem(const QString &name, const QStringList &args, const uint &key, const uchar &modifiers, QListWidget *parent);
	virtual ~CRunningScriptListItem();

	class COAScriptHandle *m_ScriptHandle{nullptr};

	void Terminate();

	bool GetPaused();
	void SetPaused(const bool &value);

	void Start(const QString &scriptBody);
};
//----------------------------------------------------------------------------------
#endif // RUNNINGSCRIPTLISTITEM_H
//----------------------------------------------------------------------------------
