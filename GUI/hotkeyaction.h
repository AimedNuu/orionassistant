/***********************************************************************************
**
** HotkeyAction.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef HOTKEYACTION_H
#define HOTKEYACTION_H
//----------------------------------------------------------------------------------
#include <QAction>
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CHotkeyAction : public QAction
{
	Q_OBJECT

	SETGET(QString, Command, "")

public:
	CHotkeyAction(const QString &text, const QString &command, QObject *parent);
	virtual ~CHotkeyAction() {}

public slots:
	void ClickedHotkeyAction();
};
//----------------------------------------------------------------------------------
#endif // HOTKEYACTION_H
//----------------------------------------------------------------------------------
