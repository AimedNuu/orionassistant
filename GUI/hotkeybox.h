/***********************************************************************************
**
** HotkeyBox.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef HOTKEYBOX_H
#define HOTKEYBOX_H
//----------------------------------------------------------------------------------
#include <QLineEdit>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QKeyEvent>
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
static const uint MOUSE_WHELL_SCROLL_UP = 0x00000001;
static const uint MOUSE_WHELL_SCROLL_DOWN = 0x00000002;
static const uint MOUSE_WHELL_PRESS = 0x00000004;
static const uint MOUSE_X_UP = 0x00000008;
static const uint MOUSE_X_DOWN = 0x00000010;
//----------------------------------------------------------------------------------
enum HOTKEY_MODIFIERS
{
	HM_NONE = 0,
	HM_MOUSE_WHELL = 0x01,
	HM_ALT = 0x02,
	HM_CTRL = 0x04,
	HM_SHIFT = 0x08,
	HM_KEYPAD = 0x10,
	HM_META = 0x20,
	HM_MOUSE_X = 0x40
};
//----------------------------------------------------------------------------------
class QHotkeyEventFilter : public QObject
{
	Q_OBJECT

protected:
	bool eventFilter(QObject *obj, QEvent *event);

	class QHotkeyBox *m_Owner;

public:
	QHotkeyEventFilter(class QHotkeyBox *owner) :QObject(), m_Owner(owner) {}
	virtual ~QHotkeyEventFilter() {}

};
//----------------------------------------------------------------------------------
class QHotkeyBox : public QLineEdit
{
	Q_OBJECT

protected:
	uint m_Key;
	uchar m_Modifiers;

	QHotkeyEventFilter m_Filter;

	static const QString m_HotkeyText[0x100];

public:
	QHotkeyBox(QWidget *parent = nullptr);

	virtual ~QHotkeyBox() {}

	uint GetKey() const {return m_Key;}
	uchar GetModifiers() const {return m_Modifiers;}

	uchar AddModifiers(uchar modifiers);

	void CreateKey(const uint &key, const uchar &modifiers);

	static QString GetHotkeyText(const uint &key, const uchar &modifiers);
};
//----------------------------------------------------------------------------------
#endif // HOTKEYBOX_H
//----------------------------------------------------------------------------------
