// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** DisplayItemListItem.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "DisplayItemListItem.h"
//----------------------------------------------------------------------------------
CDisplayItemListItem::CDisplayItemListItem(const QString &name, QListWidget *parent)
: QListWidgetItem(name, parent), m_Item()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
