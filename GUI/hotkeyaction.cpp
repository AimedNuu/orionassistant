// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** HotkeyAction.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "hotkeyaction.h"
#include "extendedplaintextedit.h"
//----------------------------------------------------------------------------------
CHotkeyAction::CHotkeyAction(const QString &text, const QString &command, QObject *parent)
: QAction(text, parent), m_Command(command)
{
	OAFUN_DEBUG("");
	connect(this, SIGNAL(triggered()), SLOT(ClickedHotkeyAction()));
}
//----------------------------------------------------------------------------------
void CHotkeyAction::ClickedHotkeyAction()
{
	OAFUN_DEBUG("");
	if (parent() != nullptr)
		((CExtendedPlainTextEdit*)parent())->insertPlainText("\n" + m_Command);
}
//----------------------------------------------------------------------------------
