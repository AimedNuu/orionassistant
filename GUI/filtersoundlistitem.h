/***********************************************************************************
**
** FilterSoundListItem.h
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef FILTERSOUNDLISTITEM_H
#define FILTERSOUNDLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
class CFilterSoundListItem : public QListWidgetItem
{
	SETGET(QString, Index, "")
	SETGET(QString, Name, "")

public:
	CFilterSoundListItem(const QString &index, const QString &name, QListWidget *parent);
	virtual ~CFilterSoundListItem() {}

	void UpdateText();
};
//----------------------------------------------------------------------------------
#endif // FILTERSOUNDLISTITEM_H
//----------------------------------------------------------------------------------
