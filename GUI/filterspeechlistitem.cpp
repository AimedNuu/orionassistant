// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** FilterSpeechListItem.cpp
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "filterspeechlistitem.h"
//----------------------------------------------------------------------------------
CFilterSpeechListItem::CFilterSpeechListItem(const QString &text, const QString &color, const bool &fullMatch, QListWidget *parent)
: QListWidgetItem("item", parent), m_Text(text), m_Color(color), m_FullMatch(fullMatch)
{
	OAFUN_DEBUG("");
	UpdateText();
}
//----------------------------------------------------------------------------------
void CFilterSpeechListItem::UpdateText()
{
	OAFUN_DEBUG("");
	if (!m_Color.length())
		m_Color = "0xFFFF";

	QString text = (!m_FullMatch ? "*PART*: " : "") + m_Color + " : " + m_Text;

	setText(text);
}
//----------------------------------------------------------------------------------
