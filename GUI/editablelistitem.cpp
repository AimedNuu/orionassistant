// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** EditableListItem.cpp
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "editablelistitem.h"
//----------------------------------------------------------------------------------
CEditableListItem::CEditableListItem(const QString &name)
: QListWidgetItem(name)
{
	OAFUN_DEBUG("");
	setFlags(flags() | Qt::ItemIsEditable);
}
//----------------------------------------------------------------------------------
