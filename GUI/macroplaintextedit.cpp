// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** MacroPlainTextEdit.cpp
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "macroplaintextedit.h"
#include "../orionassistant_global.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../GUI/hotkeyaction.h"
#include "../CommonItems/skill.h"
#include <QtWidgets>
//#include "../Managers/textcommandmanager.h"
#include "../Managers/clientmacromanager.h"
#include "../Managers/spellmanager.h"
//----------------------------------------------------------------------------------
CMacroPlainTextEdit::CMacroPlainTextEdit(QWidget *parent)
: CExtendedPlainTextEdit(parent)
{
	OAFUN_DEBUG("");
	connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(onCustomContextMenuRequested(const QPoint&)));
}
//----------------------------------------------------------------------------------
void CMacroPlainTextEdit::onCustomContextMenuRequested(const QPoint &pos)
{
	OAFUN_DEBUG("");
	Q_UNUSED(pos);
}
//----------------------------------------------------------------------------------
