/***********************************************************************************
**
** DisplayItemListItem.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef DISPLAYITEMLISTITEM_H
#define DISPLAYITEMLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
#include "../CommonItems/displayitem.h"
//----------------------------------------------------------------------------------
class CDisplayItemListItem : public QListWidgetItem
{
public:
	CDisplayItemListItem(const QString &name, QListWidget *parent);
	virtual ~CDisplayItemListItem() {}

	CDisplayItem m_Item;
};
//----------------------------------------------------------------------------------
#endif // DISPLAYITEMLISTITEM_H
//----------------------------------------------------------------------------------
