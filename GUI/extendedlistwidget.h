/***********************************************************************************
**
** ExtendedListWidget.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef EXTENDEDLISTWIDGET_H
#define EXTENDEDLISTWIDGET_H
//----------------------------------------------------------------------------------
#include <QListWidget>
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CExtendedListWidget : public QListWidget
{
	Q_OBJECT

Q_SIGNALS:
	void itemDropped();
	void itemDeleted(QListWidget*);
	void itemRenamed(QListWidgetItem*);

protected:
	void dropEvent(QDropEvent *e) Q_DECL_OVERRIDE;
	virtual void keyPressEvent(QKeyEvent *event);

public slots:
	void on_itemChanged(QListWidgetItem *item);

public:
	CExtendedListWidget(QWidget *parent = nullptr);

	virtual ~CExtendedListWidget() {}

	void RemoveCurrentItem();

	QString GetFreeName(QListWidgetItem *item, const QString &name);
};
//----------------------------------------------------------------------------------
#endif // EXTENDEDLISTWIDGET_H
//----------------------------------------------------------------------------------
