/***********************************************************************************
**
** FindListItem.h
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef FINDLISTITEM_H
#define FINDLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
#include <QList>
#include "../CommonItems/finditem.h"
//----------------------------------------------------------------------------------
class CFindListItem : public QListWidgetItem
{
public:
	CFindListItem(const QString &name, QListWidget *parent);
	virtual ~CFindListItem() {}

	QList<CFindItem> m_Items;
};
//----------------------------------------------------------------------------------
#endif // FINDLISTITEM_H
//----------------------------------------------------------------------------------
