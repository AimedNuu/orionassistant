/***********************************************************************************
**
** ExtendedPlainTextEdit.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef EXTENDEDPLAINTEXTEDIT_H
#define EXTENDEDPLAINTEXTEDIT_H
//----------------------------------------------------------------------------------
#include <QPlainTextEdit>
#include <QObject>
#include <QCompleter>
#include <QKeyEvent>
#include "SyntaxHighlighter.h"
//----------------------------------------------------------------------------------
class CExtendedPlainTextEdit : public QPlainTextEdit
{
	Q_OBJECT

protected:
	QWidget *m_LineNumberArea;

	QCompleter *m_Completer{ nullptr };

	CSyntaxHighlighter *m_SyntaxHightlighter{nullptr};

	QString textUnderCursor() const;

signals:
	void signal_CtrlQPressed();

protected:
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void keyPressEvent(QKeyEvent *e) Q_DECL_OVERRIDE;
	void focusInEvent(QFocusEvent *e) Q_DECL_OVERRIDE;

private slots:
	void updateLineNumberAreaWidth(int newBlockCount);
	void highlightCurrentLine();
	void updateLineNumberArea(const QRect &, int);
	void insertCompletion(const QString &completion);

public:
	CExtendedPlainTextEdit(QWidget *parent = nullptr);

	virtual ~CExtendedPlainTextEdit();

	void lineNumberAreaPaintEvent(QPaintEvent *event);
	int lineNumberAreaWidth();
};
//----------------------------------------------------------------------------------
class CLineNumberArea : public QWidget
{
private:
	CExtendedPlainTextEdit *m_Editor;

protected:
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE { m_Editor->lineNumberAreaPaintEvent(event); }

public:
	CLineNumberArea(CExtendedPlainTextEdit *editor) : QWidget(editor), m_Editor(editor) {}

	virtual ~CLineNumberArea() {}

	QSize sizeHint() const Q_DECL_OVERRIDE { return QSize(m_Editor->lineNumberAreaWidth(), 0); }
};
//----------------------------------------------------------------------------------
#endif // EXTENDEDPLAINTEXTEDIT_H
//----------------------------------------------------------------------------------
