// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** MacroListItem.cpp
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "macrolistitem.h"
//----------------------------------------------------------------------------------
CMacroListItem::CMacroListItem(const QString &name)
: CEditableListItem(name)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CMacroListItem::~CMacroListItem()
{
	Clear();
}
//----------------------------------------------------------------------------------
void CMacroListItem::Clear()
{
	for (CMacro *macro : m_Items)
		delete macro;

	m_Items.clear();
}
//----------------------------------------------------------------------------------
