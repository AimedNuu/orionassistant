/***********************************************************************************
**
** ScriptPlainTextEdit.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SCRIPTPLAINTEXTEDIT_H
#define SCRIPTPLAINTEXTEDIT_H
//----------------------------------------------------------------------------------
#include "extendedplaintextedit.h"
//----------------------------------------------------------------------------------
class CScriptPlainTextEdit : public CExtendedPlainTextEdit
{
	Q_OBJECT

private slots:
	void onCustomContextMenuRequested(const QPoint &pos);

public:
	CScriptPlainTextEdit(QWidget *parent = nullptr);

	virtual ~CScriptPlainTextEdit() {}
};
//----------------------------------------------------------------------------------
#endif // SCRIPTPLAINTEXTEDIT_H
//----------------------------------------------------------------------------------
