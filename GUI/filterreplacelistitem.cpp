// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** FilterReplaceListItem.cpp
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "filterreplacelistitem.h"
//----------------------------------------------------------------------------------
CFilterReplaceListItem::CFilterReplaceListItem(const QString &originalText, const QString &originalColor, const QString &replacedText, const QString &replacedColor, const int &type, const int &mode, const bool &fullMatch, QListWidget *parent)
: QListWidgetItem("item", parent), m_OriginalText(originalText), m_OriginalColor(originalColor),
m_ReplacedText(replacedText), m_ReplacedColor(replacedColor), m_Type(type), m_Mode(mode),
m_FullMatch(fullMatch)
{
	OAFUN_DEBUG("");
	UpdateText();
}
//----------------------------------------------------------------------------------
void CFilterReplaceListItem::UpdateText()
{
	OAFUN_DEBUG("");
	if (!m_OriginalColor.length())
		m_OriginalColor = "0xFFFF";

	if (!m_ReplacedColor.length())
		m_ReplacedColor = "0xFFFF";

	QString text = (!m_FullMatch ? "*PART*: " : "") + m_OriginalText + " => " + m_ReplacedText;

	setText(text);
}
//----------------------------------------------------------------------------------
