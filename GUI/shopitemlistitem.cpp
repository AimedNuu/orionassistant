// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ShopItemListItem.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "shopitemlistitem.h"
//----------------------------------------------------------------------------------
CShopItemListItem::CShopItemListItem(const CShopItem &shopItem, QListWidget *parent)
: QListWidgetItem("", parent), m_ShopItem(shopItem)
{
	OAFUN_DEBUG("");
	UpdateText();
}
//----------------------------------------------------------------------------------
void CShopItemListItem::UpdateText()
{
	OAFUN_DEBUG("");
	QString count = (m_ShopItem.GetCount() ? QString::number(m_ShopItem.GetCount()) : "all");
	QString text = "[" + m_ShopItem.GetGraphic() + ", " + m_ShopItem.GetColor() + "] " + count + " '" + m_ShopItem.GetName() + "'";

	if (m_ShopItem.GetPrice())
		text += " at " + QString::number(m_ShopItem.GetPrice()) + " gp";

	if (m_ShopItem.GetComment().length())
		text += " //" + m_ShopItem.GetComment();

	setText(text);
}
//----------------------------------------------------------------------------------
