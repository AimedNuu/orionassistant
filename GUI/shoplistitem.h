/***********************************************************************************
**
** ShopListItem.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SHOPLISTITEM_H
#define SHOPLISTITEM_H
//----------------------------------------------------------------------------------
#include <QListWidget>
#include <QListWidgetItem>
#include "../orionassistant_global.h"
#include "shopitemlistitem.h"
#include "../CommonItems/shopitem.h"
//----------------------------------------------------------------------------------
class CShopListItem : public QListWidgetItem
{
public:
	CShopListItem(const QString &name, QListWidget *parent);
	virtual ~CShopListItem();

	QVector<CShopItem> m_Items;
};
//----------------------------------------------------------------------------------
#endif // SHOPLISTITEM_H
//----------------------------------------------------------------------------------
