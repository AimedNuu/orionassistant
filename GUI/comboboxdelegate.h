/***********************************************************************************
**
** ComboboxDelegate.h
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef COMBOBOXDELEGATE_H
#define COMBOBOXDELEGATE_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QItemDelegate>
#include <QPainter>
//----------------------------------------------------------------------------------
class CComboDelegate : public QItemDelegate
{
public:
	CComboDelegate() : QItemDelegate() {}
	virtual ~CComboDelegate() {}

	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual void drawBackground(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};
//----------------------------------------------------------------------------------
#endif // COMBOBOXDELEGATE_H
//----------------------------------------------------------------------------------
