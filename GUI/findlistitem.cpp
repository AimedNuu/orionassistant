// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** FindListItem.cpp
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "findlistitem.h"
//----------------------------------------------------------------------------------
CFindListItem::CFindListItem(const QString &name, QListWidget *parent)
: QListWidgetItem(name, parent)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
