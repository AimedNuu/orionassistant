// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** DressItemListItem.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "dressitemlistitem.h"
#include "../OrionAssistant/orionassistant.h"
//----------------------------------------------------------------------------------
CDressItemListItem::CDressItemListItem(const QString &name, QListWidget *parent)
: QListWidgetItem(name, parent)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
void CDressItemListItem::UpdateText()
{
	OAFUN_DEBUG("");
	setText(g_LayerName[m_Layer] + " = " + COrionAssistant::SerialToText(m_Serial));
}
//----------------------------------------------------------------------------------
