// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** FilterSoundListItem.cpp
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "filtersoundlistitem.h"
//----------------------------------------------------------------------------------
CFilterSoundListItem::CFilterSoundListItem(const QString &index, const QString &name, QListWidget *parent)
: QListWidgetItem("item", parent), m_Index(index), m_Name(name)
{
	OAFUN_DEBUG("");
	UpdateText();
}
//----------------------------------------------------------------------------------
void CFilterSoundListItem::UpdateText()
{
	OAFUN_DEBUG("");
	setText(m_Index + " // " + m_Name);
}
//----------------------------------------------------------------------------------
