// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** WorldObjectListItem.cpp
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "WorldObjectListItem.h"
#include "../OrionAssistant/orionassistant.h"
//----------------------------------------------------------------------------------
CWorldObjectListItem::CWorldObjectListItem(const uint &serial, const QString &name, const bool &checked, QListWidget *parent)
: QListWidgetItem(name, parent), m_Serial(serial), m_Name(name)
{
	OAFUN_DEBUG("");
	setCheckState(checked ? Qt::Checked : Qt::Unchecked);
	UpdateText();
}
//----------------------------------------------------------------------------------
void CWorldObjectListItem::UpdateText()
{
	OAFUN_DEBUG("");
	setText(COrionAssistant::SerialToText(m_Serial) + " " + m_Name);
}
//----------------------------------------------------------------------------------
