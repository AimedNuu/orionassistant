// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ColoredPushButton.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "coloredpushbutton.h"
//----------------------------------------------------------------------------------
QColoredPushButton::QColoredPushButton(QWidget *parent)
: QPushButton(parent)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
void QColoredPushButton::ResetColor()
{
	OAFUN_DEBUG("");

	if (styleSheet().length())
		setStyleSheet("");
}
//----------------------------------------------------------------------------------
void QColoredPushButton::ChangeColor(const QColor &color)
{
	OAFUN_DEBUG("");
	m_Color = color;
	QString newStyle;
	//newStyle.sprintf("QPushButton{background:#%02X%02X%02X;}", color.red(), color.green(), color.blue());
	newStyle.sprintf("background-color: rgb(%i, %i, %i);", color.red(), color.green(), color.blue());

	if (styleSheet() != newStyle)
		setStyleSheet(newStyle);
}
//----------------------------------------------------------------------------------
void QColoredPushButton::ChangeColor(const uint &color)
{
	OAFUN_DEBUG("");
	ChangeColor(QColor(color & 0xFF, ((color >> 8) & 0xFF), ((color >> 16) & 0xFF)));
}
//----------------------------------------------------------------------------------
uint QColoredPushButton::GetUIntColor()
{
	OAFUN_DEBUG("");
	return (m_Color.blue() << 16) | (m_Color.green() << 8) | m_Color.red();
}
//----------------------------------------------------------------------------------
