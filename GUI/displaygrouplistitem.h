/***********************************************************************************
**
** DisplayGroupListItem.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef DISPLAYGROUPLISTITEM_H
#define DISPLAYGROUPLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
class CDisplayGroupListItem : public QListWidgetItem
{
	SETGET(QString, TitleData, "")

public:
	CDisplayGroupListItem(const QString &name, const QString &titleData, QListWidget *parent);
	virtual ~CDisplayGroupListItem() {}
};
//----------------------------------------------------------------------------------
#endif // DISPLAYGROUPLISTITEM_H
//----------------------------------------------------------------------------------
