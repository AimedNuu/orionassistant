// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ScriptPlainTextEdit.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "scriptplaintextedit.h"
#include "../orionassistant_global.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../GUI/hotkeyaction.h"
#include "../CommonItems/skill.h"
#include <QtWidgets>
//#include "../Managers/textcommandmanager.h"
#include "../Managers/clientmacromanager.h"
#include "../Managers/spellmanager.h"
//----------------------------------------------------------------------------------
CScriptPlainTextEdit::CScriptPlainTextEdit(QWidget *parent)
: CExtendedPlainTextEdit(parent)
{
	OAFUN_DEBUG("c14_f1");
	connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(onCustomContextMenuRequested(const QPoint&)));

	CHighlightingRule rule;

	QTextCharFormat keywordFormat;
	//keywordFormat.setForeground(Qt::darkBlue);
	//keywordFormat.setFontWeight(QFont::Bold);
	keywordFormat.setForeground(Qt::darkMagenta);
	rule.Format = keywordFormat;

	QStringList keywordPatterns;
	QStringList words;

	keywordPatterns << "Wait" << "Info" << "InfoTile" << "InfoMenu" << "SaveConfig" << "Click"
					<< "UseObject" << "GetStatus" << "Attack" << "SetLight" << "SetWeather"
					<< "SetSeason" << "SetTrack" << "SaveHotkeys" << "LoadHotkeys" << "Cast"
					<< "UseSkill" << "SkillValue" << "HelpGump" << "CloseUO"
					<< "Morph" << "Resend" << "Sound" << "EmoteAction" << "Hide" << "BlockMoving"
					<< "UseType" << "UseFromGround" << "Print" << "CharPrint" << "Say"
					<< "RenameMount" << "FindType" << "Ignore" << "IgnoreReset" << "Drop"
					<< "DropHere" << "MoveItem" << "ShowJournal" << "ClearJournal"
					<< "JournalIgnoreCase" << "SetDressBag" << "UnsetDressBag" << "SetArm"
					<< "UnsetArm" << "SetDress" << "UnsetDress" << "Arm" << "Disarm"
					<< "Dress" << "Undress" << "Unequip" << "Equip" << "EquipT" << "Terminate"
					<< "FindObject" << "Open" << "Close" << "Clear" << "Serial" << "Timer"
					<< "Color" << "Text" << "Flags" << "Now" << "CancelWaitMenu" << "WaitMenu"
					<< "CancelWaitTarget" << "WaitTargetTileRelative" << "WaitTargetTile"
					<< "WaitTargetGround" << "WaitTargetType" << "WaitTargetObject"
					<< "HaveTarget" << "InJournal" << "WaitJournal" << "Graphic"
					<< "X" << "Y" << "Z" << "Container" << "Map" << "Count" << "Name"
					<< "Mobile" << "Ignored" << "Frozen" << "Poisoned" << "Flying" << "YellowHits"
					<< "IgnoreCharacters" << "Locked" << "WarMode" << "Hidden" << "IsHuman"
					<< "IsPlayer" << "IsCorpse" << "Layer" << "IsMulti" << "EquipLayer"
					<< "Hits" << "MaxHits" << "Mana" << "MaxMana" << "Stam" << "MaxStam"
					<< "Female" << "Race" << "Direction" << "Notoriety" << "CanChangeName"
					<< "Dead" << "Str" << "Int" << "Dex" << "LockStrState" << "LockIntState"
					<< "LockDexState" << "Weight" << "MaxWeight" << "Armor" << "Gold"
					<< "StatsCap" << "Followers" << "MaxFollowers" << "FireResistance"
					<< "ColdResistance" << "PoisonResistance" << "EnergyResistance"
					<< "Luck" << "MinDamage" << "MaxDamage" << "TithingPoints" << "StealthSteps"
					<< "FindTextID" << "ResetIgnoreList" << "UseIgnoreList" << "FindList"
					<< "TargetObject" << "TargetType" << "TargetGround" << "TargetTile"
					<< "TargetTileRelative" << "Exists" << "GetDistance" << "CanWalk" << "Step"
					<< "WalkTo" << "StopWalking" << "IsWalking" << "NewFile" << "Debug" << "AddType"
					<< "RemoveType" << "AddObject" << "RemoveObject" << "ObjAtLayer" << "LoadScript"
					<< "Exec" << "ScriptRunning" << "Append" << "Opened" << "ReadLine" << "Read"
					<< "WriteLine" << "Write" << "OptionSound" << "OptionSoundVolume" << "OptionMusic"
					<< "OptionMusicVolume" << "OptionUseTooltips" << "OptionAlwaysRun"
					<< "OptionNewTargetSystem" << "OptionObjectHandles" << "OptionScaleSpeech"
					<< "OptionScaleSpeechDelay" << "OptionIgnoreGuildMessages"
					<< "OptionIgnoreAllianceMessages" << "OptionDarkNights" << "OptionColoredLighting"
					<< "OptionCriminalActionsQuery" << "OptionCircleOfTransparency"
					<< "OptionCircleOfTransparencyValue" << "OptionLockResizingGameWindow"
					<< "OptionFPSValue" << "OptionUseScalingGameWindow" << "OptionDrawStatusState"
					<< "OptionDrawStumps" << "OptionMarkingCaves" << "OptionNoVegetation"
					<< "OptionNoFieldsAnimation" << "OptionStandardCharactesFrameRate"
					<< "OptionStandardItemsFrameRate" << "OptionLockGumpsMoving" << "OptionEnterChat"
					<< "OptionHiddenCharacters" << "OptionHiddenCharactersAlpha"
					<< "OptionHiddenCharactersModeOnlyForSelf" << "OptionTransparentSpellIcons"
					<< "OptionSpellIconsAlpha" << "OptionFastRotation" << "ClientLastTarget"
					<< "ClientLastAttack" << "TargetSystemSerial" << "GetSerial" << "BandageSelf"
					<< "AddFindList" << "ClearFindList" << "AddIgnoreList" << "ClearIgnoreList"
					<< "MenuCount" << "GetMenu" << "SelectMenu" << "CloseMenu" << "BuffExists"
					<< "ID" << "IsGrayMenu" << "Select" << "ItemsCount" << "ItemID" << "ItemGraphic"
					<< "ItemColor" << "ItemName" << "Paralyzed" << "TradeCount" << "TradeContainer"
					<< "TradeOpponent" << "TradeName" << "TradeCheckState" << "TradeCheck"
					<< "TradeClose" << "UseTypeList" << "UseFromGroundList" << "WaitTargetTypeList"
					<< "WaitTargetGroundList" << "TargetTypeList" << "TargetGroundList" << "GetGraphic"
					<< "GetContainer" << "FindFriend" << "FindEnemy" << "GetFriendList"
					<< "GetFriendsStatus" << "GetEnemyList" << "GetEnemiesStatus" << "SetFontColor"
					<< "GetFontColor" << "GetFontColorValue" << "SetCharactersFontColor"
					<< "GetCharactersFontColor" << "GetCharactersFontColorValue" << "AddFriend"
					<< "RemoveFriend" << "ClearFriendList" << "AddEnemy" << "RemoveEnemy"
					<< "ClearEnemyList" << "SetGlobal" << "GetGlobal" << "ClearGlobals" << "InfoGump"
					<< "ValidateTargetTile" << "ValidateTargetTileRelative" << "Launch" << "UseAbility"
					<< "Contains" << "Split" << "LastJournalMessage" << "GetLastTargetPosition"
					<< "GetLastAttackPosition" << "UseWrestlingDisarm" << "UseWrestlingStun"
					<< "OpenContainer" << "ObjectExists" << "OAVersion" << "Connected" << "Time"
					<< "Date" << "Random" << "RequestName" << "InvokeVirture" << "JournalCount"
					<< "JournalLine" << "SetText" << "CreateGumpHook" << "WaitGump" << "CancelWaitGump"
					<< "GumpCount" << "GetLastGump" << "GetGump" << "Replayed" << "ReplyID"
					<< "ButtonList" << "CheckboxList" << "RadioList" << "TilepicList" << "GumppicList"
					<< "EntriesList" << "CommandList" << "TextList" << "Command" << "GetHook"
					<< "Index" << "AddEntry" << "AddCheck" << "PlayWav" << "Properties"
					<< "ProfileReceived" << "Profile" << "Screenshot" << "WaitPrompt"
					<< "CancelWaitPrompt" << "Title" << "Buy" << "Sell" << "IsLandTile"
					<< "IsStaticTile" << "IsGameObject" << "LandGraphic" << "LandX" << "LandY"
					<< "LandZ" << "ShowStatusbar" << "CloseStatusbar" << "LogOut" << "SetPos"
					<< "SetSize" << "CreateClientMacro" << "AddAction" << "Play" << "TimerExists"
					<< "SetTimer" << "RemoveTimer" << "ClearTimers" << "CancelTarget" << "PromptExists"
					<< "PromptSerial" << "PromptID" << "SendPrompt" << "OpenPaperdoll" << "ClosePaperdoll"
					<< "MovePaperdoll" << "AddIgnoreListObject" << "RequestContextMenu" << "WaitContextMenu"
					<< "CancelContextMenu" << "SayYell" << "SayWhisper" << "SayEmote" << "SayBroadcast"
					<< "SayParty" << "SayGuild" << "SayAlliance" << "BandageTarget" << "CastTarget"
					<< "UseSkillTarget" << "ClientViewRange" << "OpenOrionMap" << "MoveOrionMap"
					<< "CloseOrionMap" << "OpenEnhancedMap" << "WaitForTarget" << "WaitForMenu"
					<< "WaitForGump" << "WaitForPrompt" << "WaitForShop" << "WaitForTrade"
					<< "WaitForContextMenu" << "LoadProfile" << "ActivateClient" << "ShutdownWindows"
					<< "OnOffHotkeys" << "GetTargetType" << "Remove" << "SaveToFile";

	foreach (const QString &pattern, keywordPatterns)
	{
		rule.Pattern = QRegExp("\\.\\b" + pattern + "\\b");
		m_SyntaxHightlighter->m_HighlightingRules.append(rule);
	}

	words << keywordPatterns;
	keywordPatterns.clear();

	//keywordFormat.setForeground(Qt::red);
	keywordFormat.setForeground(Qt::darkBlue);
	keywordFormat.setFontWeight(QFont::Bold);
	rule.Format = keywordFormat;

	keywordPatterns << "Orion" << "if" << "else" << "for" << "var" << "TextWindow"
					<< "while" << "Player" << "function" << "return" << "break" << "switch"
					<< "case" << "continue" << "SelectedTile";

	foreach (const QString &pattern, keywordPatterns)
	{
		rule.Pattern = QRegExp("\\b" + pattern + "\\b");
		m_SyntaxHightlighter->m_HighlightingRules.append(rule);
	}

	words << keywordPatterns;
	keywordPatterns.clear();

	keywordFormat.setForeground(Qt::darkYellow);
	keywordFormat.setFontWeight(QFont::Normal);
	rule.Format = keywordFormat;

	keywordPatterns << "self" << "lastcorpse" << "lasttarget" << "lasttile" << "backpack"
					<< "ground" << "lastattack" << "lastcontainer" << "true" << "false"
					<< "null" << "laststatus" << "lastobject" << "ScriptName" << "undefined";

	words << keywordPatterns;

	foreach (const QString &pattern, keywordPatterns)
	{
		rule.Pattern = QRegExp("\\b" + pattern + "\\b");
		m_SyntaxHightlighter->m_HighlightingRules.append(rule);
	}

	QTextCharFormat numbersFormat;
	numbersFormat.setForeground(Qt::blue);
	rule.Pattern = QRegExp("\\b[\\d]+\\b");
	rule.Format = numbersFormat;
	rule.Pattern.setMinimal(true);
	m_SyntaxHightlighter->m_HighlightingRules.append(rule);
	rule.Pattern = QRegExp("\\b0x([\\d]|[abcdef]|[ABCDEF])+\\b");
	m_SyntaxHightlighter->m_HighlightingRules.append(rule);

	QTextCharFormat quotationFormat;
	quotationFormat.setForeground(Qt::darkGreen);
	rule.Pattern = QRegExp("\".*\"");
	rule.Pattern.setMinimal(true);
	rule.Format = quotationFormat;
	m_SyntaxHightlighter->m_HighlightingRules.append(rule);
	rule.Pattern = QRegExp("'.*'");
	rule.Pattern.setMinimal(true);
	m_SyntaxHightlighter->m_HighlightingRules.append(rule);

	QTextCharFormat comment;
	comment.setForeground(Qt::gray);
	rule.Pattern = QRegExp("//[^\n]*");
	rule.Format = comment;
	m_SyntaxHightlighter->m_HighlightingRules.append(rule);



	qSort(words.begin(), words.end());

	m_Completer->setModel(new QStringListModel(words, m_Completer));
	//m_Completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
}
//----------------------------------------------------------------------------------
void CScriptPlainTextEdit::onCustomContextMenuRequested(const QPoint &pos)
{
	OAFUN_DEBUG("c14_f13");
	Q_UNUSED(pos);

	QMenu *menu = new QMenu(this);

	QMenu *menuObjects = menu->addMenu("Objects");
	menuObjects->addAction(new CHotkeyAction("Info", "Orion.Info(['serial']);", this));
	menuObjects->addAction(new CHotkeyAction("GetSerial", "Orion.GetSerial(['serial']);", this));
	menuObjects->addAction(new CHotkeyAction("GetGraphic", "Orion.GetGraphic(['graphic']);", this));
	menuObjects->addAction(new CHotkeyAction("GetContainer", "Orion.GetContainer(['serial']);", this));
	menuObjects->addAction(new CHotkeyAction("Click", "Orion.Click(['serial']);", this));
	menuObjects->addAction(new CHotkeyAction("Use object", "Orion.UseObject(['serial']);", this));
	menuObjects->addAction(new CHotkeyAction("Attack", "Orion.Attack(['serial']);", this));
	menuObjects->addAction(new CHotkeyAction("Request status", "Orion.GetStatus(['serial']);", this));
	menuObjects->addAction(new CHotkeyAction("Hide", "Orion.Hide(['serial']);", this));
	menuObjects->addAction(new CHotkeyAction("Set dress bag", "Orion.SetDressBag(['serial']);", this));
	menuObjects->addAction(new CHotkeyAction("Unset dress bag", "Orion.UnsetDressBag();", this));
	menuObjects->addAction(new CHotkeyAction("Drop", "Orion.Drop(['serial'], [count], [x, y], [z]);", this));
	menuObjects->addAction(new CHotkeyAction("DropHere", "Orion.DropHere(['serial'], [count]);", this));
	menuObjects->addAction(new CHotkeyAction("MoveItem", "Orion.MoveItem(['serial'], [count], ['container'], [x, y], [z]);", this));
	menuObjects->addAction(new CHotkeyAction("GetDistance", "Orion.GetDistance('serial')", this));
	menuObjects->addAction(new CHotkeyAction("GetDistance", "Orion.GetDistance(x, y)", this));
	menuObjects->addAction(new CHotkeyAction("Get friends status", "Orion.GetFriendsStatus();", this));
	menuObjects->addAction(new CHotkeyAction("Get enemies status", "Orion.GetEnemiesStatus();", this));
	menuObjects->addAction(new CHotkeyAction("Get last target (remove) position", "Orion.GetLastTargetPosition();", this));
	menuObjects->addAction(new CHotkeyAction("Get last attack (remove) position", "Orion.GetLastAttackPosition();", this));
	menuObjects->addAction(new CHotkeyAction("Open container", "Orion.OpenContainer('serial', ['delay'=600], ['errorTextPattern']);", this));
	menuObjects->addAction(new CHotkeyAction("Exists", "Orion.ObjectExists('serial');", this));
	menuObjects->addAction(new CHotkeyAction("Request name", "Orion.RequestName('serial', ['delay'=200]);", this));
	menuObjects->addAction(new CHotkeyAction("Display/Move statusbar gump", "Orion.ShowStatusbar('serial', x, y, [minimized]);", this));
	menuObjects->addAction(new CHotkeyAction("Close statusbar gump", "Orion.CloseStatusbar('serial');", this));
	menuObjects->addAction(new CHotkeyAction("Open paperdoll", "Orion.OpenPaperdoll('serial');", this));
	menuObjects->addAction(new CHotkeyAction("Close paperdoll", "Orion.ClosePaperdoll('serial');", this));
	menuObjects->addAction(new CHotkeyAction("Move paperdoll", "Orion.MovePaperdoll('serial', x, y);", this));

	QMenu *menuFindUse = menu->addMenu("Find or Use");
	menuFindUse->addAction(new CHotkeyAction("UseType", "Orion.UseType('graphic', ['color'], ['container'], [recurseSearch]);", this));
	menuFindUse->addAction(new CHotkeyAction("UseFromGround", "Orion.UseFromGround('graphic', ['color'], ['distance'], ['flags']);", this));
	menuFindUse->addAction(new CHotkeyAction("UseTypeList", "Orion.UseTypeList('findListName', ['container'], [recurseSearch]);", this));
	menuFindUse->addAction(new CHotkeyAction("UseFromGroundList", "Orion.UseFromGroundList('findListName', ['distance'], ['flags']);", this));
	menuFindUse->addAction(new CHotkeyAction("Count", "Orion.Count('graphic', ['color'], ['container'], ['distance'], [recurseSearch]);", this));
	menuFindUse->addAction(new CHotkeyAction("FindType", "Orion.FindType('graphic', ['color'], ['container'], ['flags'], ['distance'], ['notoriety'], [recurseSearch]);", this));
	menuFindUse->addAction(new CHotkeyAction("FindList", "Orion.FindList('findListName', ['container'], ['flags'], ['distance'], ['notoriety'], [recurseSearch]);", this));
	menuFindUse->addAction(new CHotkeyAction("Use ignore list", "Orion.UseIgnoreList('ignoreListName');", this));
	menuFindUse->addAction(new CHotkeyAction("Reset ignore list", "Orion.ResetIgnoreList();", this));
	menuFindUse->addAction(new CHotkeyAction("Find object", "Orion.FindObject('serial');", this));
	menuFindUse->addAction(new CHotkeyAction("Ignore object", "Orion.Ignore('serial', [true/false]);", this));
	menuFindUse->addAction(new CHotkeyAction("Reset ignored objects", "Orion.IgnoreReset();", this));
	menuFindUse->addAction(new CHotkeyAction("Find object at layer", "Orion.ObjAtLayer('layerName', ['serial']);", this));
	menuFindUse->addAction(new CHotkeyAction("Find friend", "Orion.FindFriend(['flags'], ['distance']);", this));
	menuFindUse->addAction(new CHotkeyAction("Find enemy", "Orion.FindEnemy(['flags'], ['distance']);", this));

	QMenu *menuTargets = menu->addMenu("Targets");
	menuTargets->addAction(new CHotkeyAction("HaveTarget", "Orion.HaveTarget();", this));
	menuTargets->addAction(new CHotkeyAction("WaitTargetType", "Orion.WaitTargetType('graphic', ['color'], ['container'], ['flags'], [recurseSearch]);", this));
	menuTargets->addAction(new CHotkeyAction("WaitTargetGround", "Orion.WaitTargetGround('graphic', ['color'], ['distance'], ['flags']);", this));
	menuTargets->addAction(new CHotkeyAction("WaitTargetTypeList", "Orion.WaitTargetTypeList('findListName', ['container'], ['flags'], [recurseSearch]);", this));
	menuTargets->addAction(new CHotkeyAction("WaitTargetGroundList", "Orion.WaitTargetGroundList('findListName', ['distance'], ['flags']);", this));
	menuTargets->addAction(new CHotkeyAction("WaitTarget on object", "Orion.WaitTargetObject('serial');", this));
	menuTargets->addAction(new CHotkeyAction("WaitTarget on lasttarget", "Orion.WaitTargetObject(lasttarget);", this));
	menuTargets->addAction(new CHotkeyAction("WaitTarget on self", "Orion.WaitTargetObject(self);", this));
	menuTargets->addAction(new CHotkeyAction("WaitTarget on tile", "Orion.WaitTargetTile('graphic', x, y, z);", this));
	menuTargets->addAction(new CHotkeyAction("WaitTarget on lasttile", "Orion.WaitTargetTile(lasttile);", this));
	menuTargets->addAction(new CHotkeyAction("WaitTarget on tile (relative)", "Orion.WaitTargetTileRelative('graphic', x, y, z);", this));
	menuTargets->addAction(new CHotkeyAction("Cancel wait target", "Orion.CancelWaitTarget();", this));
	menuTargets->addAction(new CHotkeyAction("TargetType", "Orion.TargetType('graphic', ['color'], ['container'], ['flags'], [recurseSearch]);", this));
	menuTargets->addAction(new CHotkeyAction("TargetGround", "Orion.TargetGround('graphic', ['color'], ['distance'], ['flags']);", this));
	menuTargets->addAction(new CHotkeyAction("TargetTypeList", "Orion.TargetTypeList('findListName', ['container'], ['flags'], [recurseSearch]);", this));
	menuTargets->addAction(new CHotkeyAction("TargetGroundList", "Orion.TargetGroundList('findListName', ['distance'], ['flags']);", this));
	menuTargets->addAction(new CHotkeyAction("Target on object", "Orion.TargetObject('serial');", this));
	menuTargets->addAction(new CHotkeyAction("Target on lasttarget", "Orion.TargetObject(lasttarget);", this));
	menuTargets->addAction(new CHotkeyAction("Target on self", "Orion.TargetObject(self);", this));
	menuTargets->addAction(new CHotkeyAction("Target on tile", "Orion.TargetTile('graphic', x, y, z);", this));
	menuTargets->addAction(new CHotkeyAction("Target on lasttile", "Orion.TargetTile(lasttile);", this));
	menuTargets->addAction(new CHotkeyAction("Target on tile (relative)", "Orion.TargetTileRelative('graphic', x, y, z);", this));
	menuTargets->addAction(new CHotkeyAction("Cancel current client target", "Orion.CancelTarget();", this));
	menuTargets->addAction(new CHotkeyAction("Validate target on tile", "Orion.ValidateTargetTile('graphic', x, y);", this));
	menuTargets->addAction(new CHotkeyAction("Validate target on tile (relative)", "Orion.ValidateTargetTileRelative('graphic', x, y);", this));
	menuTargets->addAction(new CHotkeyAction("Waiting for target", "Orion.WaitForTarget([maxDelay=1000]);", this));
	menuTargets->addAction(new CHotkeyAction("Get target type", "Orion.GetTargetType();", this));

	QMenu *menuActions = menu->addMenu("Actions");
	menuActions->addAction(new CHotkeyAction("Resend", "Orion.Resend();", this));
	menuActions->addAction(new CHotkeyAction("WarMode", "Orion.WarMode([true/false]);", this));
	menuActions->addAction(new CHotkeyAction("RenameMount", "Orion.RenameMount('serial', 'new name');", this));
	menuActions->addAction(new CHotkeyAction("EmoteAction", "Orion.EmoteAction('action name');", this));
	menuActions->addAction(new CHotkeyAction("Morph", "Orion.Morph(['graphic']);", this));
	menuActions->addAction(new CHotkeyAction("Bandage Self", "Orion.BandageSelf();", this));

	QMenu *menuNewTargetSystemActions = menu->addMenu("New target system actions");
	menuNewTargetSystemActions->addAction(new CHotkeyAction("Bandage self", "Orion.BandageTarget(self);", this));
	menuNewTargetSystemActions->addAction(new CHotkeyAction("Bandage target", "Orion.BandageTarget('serial');", this));
	menuNewTargetSystemActions->addAction(new CHotkeyAction("Cast spell on target", "Orion.CastTarget('nameOrIndex', 'serial');", this));
	menuNewTargetSystemActions->addAction(new CHotkeyAction("Use skill on target", "Orion.UseSkillTarget('nameOrIndex', 'serial');", this));

	QMenu *menuSpeechAndPrint = menu->addMenu("Speech and print");
	menuSpeechAndPrint->addAction(new CHotkeyAction("Print", "Orion.Print('some text');", this));
	menuSpeechAndPrint->addAction(new CHotkeyAction("Colored Print", "Orion.Print('color', 'some text');", this));
	menuSpeechAndPrint->addAction(new CHotkeyAction("CharPrint", "Orion.CharPrint('serial', 'color', 'some text');", this));
	menuSpeechAndPrint->addSeparator();
	menuSpeechAndPrint->addAction(new CHotkeyAction("Say", "Orion.Say('some text');", this));
	menuSpeechAndPrint->addAction(new CHotkeyAction("Yell", "Orion.SayYell('some text');", this));
	menuSpeechAndPrint->addAction(new CHotkeyAction("Whisper", "Orion.SayWhisper('some text');", this));
	menuSpeechAndPrint->addAction(new CHotkeyAction("Emote", "Orion.SayEmote('some text');", this));
	menuSpeechAndPrint->addAction(new CHotkeyAction("Broadcast", "Orion.SayBroadcast('some text');", this));
	menuSpeechAndPrint->addAction(new CHotkeyAction("Party", "Orion.SayParty('some text');", this));
	menuSpeechAndPrint->addAction(new CHotkeyAction("Guild", "Orion.SayGuild('some text');", this));
	menuSpeechAndPrint->addAction(new CHotkeyAction("Alliance", "Orion.SayAlliance('some text');", this));

	QMenu *mapUOActions = menu->addMenu("MapUO");

	mapUOActions->addAction(new CHotkeyAction("Open Orion Map", "Orion.OpenOrionMap([x, y]);", this));
	mapUOActions->addAction(new CHotkeyAction("Move Orion Map", "Orion.MoveOrionMap(x, y);", this));
	mapUOActions->addAction(new CHotkeyAction("Close Orion Map", "Orion.CloseOrionMap());", this));

	mapUOActions->addSeparator();

	mapUOActions->addAction(new CHotkeyAction("Open Enhanced Map", "Orion.OpenEnhancedMap(['filePath']);", this));

	QMenu *specialAbilitiesActions = menu->addMenu("Special abilities");

	specialAbilitiesActions->addAction(new CHotkeyAction("Primary", "Orion.UseAbility('Primary');", this));
	specialAbilitiesActions->addAction(new CHotkeyAction("Secondary", "Orion.UseAbility('Secondary');", this));
	specialAbilitiesActions->addSeparator();

	specialAbilitiesActions->addAction(new CHotkeyAction("Wrestling Disarm", "Orion.UseWrestlingDisarm();", this));
	specialAbilitiesActions->addAction(new CHotkeyAction("Wrestling Stun", "Orion.UseWrestlingStun();", this));
	specialAbilitiesActions->addSeparator();

	IFOR(i, 0, MAX_ABILITIES_COUNT)
	{
		const QString &name = g_AbilityName[i];

		specialAbilitiesActions->addAction(new CHotkeyAction(name, "Orion.UseAbility('" + name + "');", this));
	}

	QMenu *invokeVirtureActions = menu->addMenu("Invoke virture");

	IFOR(i, 0, MAX_INVOKE_VIRTURE_COUNT)
	{
		const QString &name = g_InvokeVirtureName[i];

		invokeVirtureActions->addAction(new CHotkeyAction(name, "Orion.InvokeVirture('" + name + "');", this));
	}

	QMenu *menuGlobals = menu->addMenu("Globals");
	menuGlobals->addAction(new CHotkeyAction("Self", "self", this));
	menuGlobals->addAction(new CHotkeyAction("Last target", "lasttarget", this));
	menuGlobals->addAction(new CHotkeyAction("Last attack", "lastattack", this));
	menuGlobals->addAction(new CHotkeyAction("Last status", "laststatus", this));
	menuGlobals->addAction(new CHotkeyAction("Last corpse", "lastcorpse", this));
	menuGlobals->addAction(new CHotkeyAction("Last object", "lastobject", this));
	menuGlobals->addAction(new CHotkeyAction("Last container", "lastcontainer", this));
	menuGlobals->addAction(new CHotkeyAction("Backpack", "backpack", this));
	menuGlobals->addAction(new CHotkeyAction("Ground", "ground", this));
	menuGlobals->addAction(new CHotkeyAction("Ground", "ScriptName", this));

	QMenu *menuClientGlobalVariables = menu->addMenu("Client global variables");

	QMenu *menuClientGlobalVariablesGet = menuClientGlobalVariables->addMenu("Get");
	menuClientGlobalVariablesGet->addAction(new CHotkeyAction("Last target", "Orion.ClientLastTarget();", this));
	menuClientGlobalVariablesGet->addAction(new CHotkeyAction("Last attack", "Orion.ClientLastAttack();", this));
	menuClientGlobalVariablesGet->addAction(new CHotkeyAction("New target system (serial)", "Orion.TargetSystemSerial();", this));

	QMenu *menuClientGlobalVariablesSet = menuClientGlobalVariables->addMenu("Set");
	menuClientGlobalVariablesSet->addAction(new CHotkeyAction("Last target", "Orion.ClientLastTarget('serial');", this));
	menuClientGlobalVariablesSet->addAction(new CHotkeyAction("Last attack", "Orion.ClientLastAttack('serial');", this));
	menuClientGlobalVariablesSet->addAction(new CHotkeyAction("New target system (serial)", "Orion.TargetSystemSerial('serial');", this));

	QMenu *menuScriptsControl = menu->addMenu("Scripts control");
	menuScriptsControl->addAction(new CHotkeyAction("Execute script", "Orion.Exec('scriptName', [oneScriptCanBeRunned], [arguments]);", this));
	menuScriptsControl->addAction(new CHotkeyAction("Terminate script", "Orion.Terminate('scriptName', ['saveScriptName']);", this));
	menuScriptsControl->addAction(new CHotkeyAction("Load script", "Orion.LoadScript('filePath');", this));
	menuScriptsControl->addAction(new CHotkeyAction("Script running", "Orion.ScriptRunning('scriptName');", this));
	menuScriptsControl->addSeparator();
	menuScriptsControl->addAction(new CHotkeyAction("Activate client window", "Orion.ActivateClient();", this));
	menuScriptsControl->addAction(new CHotkeyAction("Turn off window", "Orion.ShutdownWindows(['mode']);", this));
	menuScriptsControl->addSeparator();
	menuScriptsControl->addAction(new CHotkeyAction("Get current hotkeys state (on/off)", "Orion.OnOffHotkeys();", this));
	menuScriptsControl->addAction(new CHotkeyAction("Set current hotkeys state (on/off)", "Orion.OnOffHotkeys(state);", this));

	QMenu *menuScript = menu->addMenu("Script");
	menuScript->addAction(new CHotkeyAction("Wait (ms)", "Orion.Wait(100);", this));
	menuScript->addAction(new CHotkeyAction("Return", "return;", this));
	menuScript->addAction(new CHotkeyAction("Break", "break;", this));
	menuScript->addAction(new CHotkeyAction("Continue", "continue;", this));

	menuScript->addAction(new CHotkeyAction("If", "if (condition)", this));
	menuScript->addAction(new CHotkeyAction("Else", "else", this));

	menuScript->addAction(new CHotkeyAction("For cycle", "for (var i = 0; i < ...; i++)", this));
	menuScript->addAction(new CHotkeyAction("While cycle", "while (condition)", this));

	menuScript->addSeparator();

	menuScript->addAction(new CHotkeyAction("Set global valiable", "Orion.SetGlobal(name, value);", this));
	menuScript->addAction(new CHotkeyAction("Get global valiable", "Orion.GetGlobal(name);", this));
	menuScript->addAction(new CHotkeyAction("Clear all global valiables", "Orion.ClearGlobals();", this));

	QMenu *menuSkills = menu->addMenu("Skills");
	QMenu *menuSkillValue = menu->addMenu("SkillValue");
	QMenu *menuSkillValueReal = menuSkillValue->addMenu("real");
	QMenu *menuSkillValueBase = menuSkillValue->addMenu("base");
	QMenu *menuSkillValueCap = menuSkillValue->addMenu("cap");
	QMenu *menuSkillValueLock = menuSkillValue->addMenu("lock");

	IFOR(i, 0, g_SkillsCount)
	{
		QString name = g_Skills[i].GetName();

		if (g_Skills[i].GetButton() && name.length())
			menuSkills->addAction(new CHotkeyAction(name, "Orion.UseSkill('" + name + "', ['targetSerial']);", this));

		menuSkillValueReal->addAction(new CHotkeyAction(name, "Orion.SkillValue('" + name + "', 'real')", this));
		menuSkillValueBase->addAction(new CHotkeyAction(name, "Orion.SkillValue('" + name + "', 'base')", this));
		menuSkillValueCap->addAction(new CHotkeyAction(name, "Orion.SkillValue('" + name + "', 'cap')", this));
		menuSkillValueLock->addAction(new CHotkeyAction(name, "Orion.SkillValue('" + name + "', 'lock')", this));
	}

	QMenu *menuSpells = menu->addMenu("Spells");

	static const QString spellbookName[7] =
	{
		"Mage",
		"Necromancer",
		"Paladin",
		"Bushido",
		"Ninjitsu",
		"Spellweaving",
		"Mysticism"
	};

	int spellbooksCount = qMin(g_SpellManager.m_SpellbookSpellNames.size(), 7);

	IFOR(i, 0, spellbooksCount)
	{
		QStringList &list = g_SpellManager.m_SpellbookSpellNames[i];

		QMenu *spellsBlock = menuSpells->addMenu(spellbookName[i]);

		foreach (const QString &spellName, list)
		{
			if (spellName.length())
				spellsBlock->addAction(new CHotkeyAction(spellName, "Orion.Cast('" + spellName + "', ['targetSerial']);", this));
		}
	}

	QMenu *menuWalker = menu->addMenu("Walker");
	menuWalker->addAction(new CHotkeyAction("BlockMoving", "Orion.BlockMoving(true/false);", this));
	menuWalker->addAction(new CHotkeyAction("CanWalk", "Orion.CanWalk(direction, x, y, z);", this));
	menuWalker->addAction(new CHotkeyAction("Step", "Orion.Step(direction, [run]);", this));
	menuWalker->addAction(new CHotkeyAction("WalkTo", "Orion.WalkTo( x, y, z, distance);", this));
	menuWalker->addAction(new CHotkeyAction("StopWalking", "Orion.StopWalking();", this));
	menuWalker->addAction(new CHotkeyAction("IsWalking", "Orion.IsWalking();", this));

	QMenu *menuDressing = menu->addMenu("Dressing");
	menuDressing->addAction(new CHotkeyAction("SetArm", "Orion.SetArm('setName');", this));
	menuDressing->addAction(new CHotkeyAction("UnsetArm", "Orion.UnsetArm('setName');", this));
	menuDressing->addAction(new CHotkeyAction("Arm", "Orion.Arm('setName');", this));
	menuDressing->addAction(new CHotkeyAction("Disarm", "Orion.Disarm();", this));
	menuDressing->addAction(new CHotkeyAction("SetDress", "Orion.SetDress('setName');", this));
	menuDressing->addAction(new CHotkeyAction("UnsetDress", "Orion.UnsetDress('setName');", this));
	menuDressing->addAction(new CHotkeyAction("Dress", "Orion.Dress('setName');", this));
	menuDressing->addAction(new CHotkeyAction("Undress", "Orion.Undress();", this));
	menuDressing->addAction(new CHotkeyAction("Equip", "Orion.Equip('serial');", this));

	QMenu *menuUnequip = menuDressing->addMenu("Unequip");

	IFOR(layer, 1, 25)
	{
		if (g_LayerSafe[layer])
			menuUnequip->addAction(new CHotkeyAction(g_LayerName[layer], "Orion.Unequip('" + g_LayerName[layer] + "');", this));
	}

	menuDressing->addAction(new CHotkeyAction("EquipT", "Orion.EquipT('graphic', ['color']);", this));

	QMenu *menuJournal = menu->addMenu("Journal");
	menuJournal->addAction(new CHotkeyAction("ShowJournal", "Orion.ShowJournal([linesCount]);", this));
	menuJournal->addAction(new CHotkeyAction("ClearJournal", "Orion.ClearJournal(['pattern'], ['flags'], ['serial'], ['color']);", this));
	menuJournal->addAction(new CHotkeyAction("JournalIgnoreCase", "Orion.JournalIgnoreCase(true/false);", this));
	menuJournal->addAction(new CHotkeyAction("InJournal", "Orion.InJournal('pattern', ['flags'], ['serial'], ['color'], [startTime], [endTime]);", this));
	menuJournal->addAction(new CHotkeyAction("WaitJournal", "Orion.WaitJournal('pattern', startTime, endTime, ['flags'], ['serial'], ['color']);", this));
	menuJournal->addAction(new CHotkeyAction("Last journal message", "Orion.LastJournalMessage();", this));
	menuJournal->addAction(new CHotkeyAction("Get journal lines count", "Orion.JournalCount();", this));
	menuJournal->addAction(new CHotkeyAction("Get journal line", "Orion.JournalLine(index);", this));

	QMenu *menuMenu = menu->addMenu("Menu");
	menuMenu->addAction(new CHotkeyAction("WaitMenu", "Orion.WaitMenu(prompt, choice);", this));
	menuMenu->addAction(new CHotkeyAction("CancelWaitMenu", "Orion.CancelWaitMenu();", this));
	menuMenu->addAction(new CHotkeyAction("InfoMenu", "Orion.InfoMenu();", this));
	menuMenu->addAction(new CHotkeyAction("MenuCount", "Orion.MenuCount();", this));
	menuMenu->addAction(new CHotkeyAction("GetMenu", "Orion.GetMenu(name);", this));
	menuMenu->addAction(new CHotkeyAction("SelectMenu", "Orion.SelectMenu(name, itemName);", this));
	menuMenu->addAction(new CHotkeyAction("CloseMenu", "Orion.CloseMenu(name);", this));
	menuMenu->addAction(new CHotkeyAction("Waiting for menu", "Orion.WaitForMenu([maxDelay=1000]);", this));

	QMenu *menuGump = menu->addMenu("Gump");
	menuGump->addAction(new CHotkeyAction("Help gump", "Orion.HelpGump();", this));
	menuGump->addAction(new CHotkeyAction("Info gump", "Orion.InfoGump([index=-1]);", this));
	menuGump->addAction(new CHotkeyAction("Create gump hook", "Orion.CreateGumpHook(index);", this));
	menuGump->addAction(new CHotkeyAction("Wait gump", "Orion.WaitGump(hook);", this));
	menuGump->addAction(new CHotkeyAction("Cancel wait gump", "Orion.CancelWaitGump();", this));
	menuGump->addAction(new CHotkeyAction("Get gumps count", "Orion.GumpCount();", this));
	menuGump->addAction(new CHotkeyAction("Get last gump", "Orion.GetLastGump();", this));
	menuGump->addAction(new CHotkeyAction("Get gump by index", "Orion.GetGump(index);", this));
	menuGump->addAction(new CHotkeyAction("Get gump by serial & id", "Orion.GetGump(serial, id);", this));
	menuGump->addAction(new CHotkeyAction("Waiting for gump", "Orion.WaitForGump([maxDelay=1000]);", this));

	QMenu *menuTimers = menu->addMenu("Timers");
	menuTimers->addAction(new CHotkeyAction("Exists", "Orion.TimerExists(name);", this));
	menuTimers->addAction(new CHotkeyAction("Add/Set value", "Orion.SetTimer(name, [value=0]);", this));
	menuTimers->addAction(new CHotkeyAction("Get current value", "Orion.Timer(name);", this));
	menuTimers->addAction(new CHotkeyAction("Remove", "Orion.RemoveTimer(name);", this));
	menuTimers->addAction(new CHotkeyAction("Clear all timers", "Orion.ClearTimers();", this));

	QMenu *menuShop = menu->addMenu("Shop");
	menuShop->addAction(new CHotkeyAction("Buy", "Orion.Buy(shopListName, [vendorName=''], [shopDelay=0]);", this));
	menuShop->addAction(new CHotkeyAction("Sell", "Orion.Sell(shopListName, [vendorName=''], [shopDelay=0]);", this));
	menuShop->addAction(new CHotkeyAction("Waiting for shop", "Orion.WaitForShop([maxDelay=1000]);", this));

	QMenu *menuTrade = menu->addMenu("Trade");
	menuTrade->addAction(new CHotkeyAction("Count", "Orion.TradeCount();", this));
	menuTrade->addAction(new CHotkeyAction("Container", "Orion.TradeContainer('index', 'container');", this));
	menuTrade->addAction(new CHotkeyAction("Opponent", "Orion.TradeOpponent('index');", this));
	menuTrade->addAction(new CHotkeyAction("Name", "Orion.TradeName('index');", this));
	menuTrade->addAction(new CHotkeyAction("CheckState", "Orion.TradeCheckState('index', 'container');", this));
	menuTrade->addAction(new CHotkeyAction("Check", "Orion.TradeCheck('index', state);", this));
	menuTrade->addAction(new CHotkeyAction("Close", "Orion.TradeClose('index');", this));
	menuTrade->addAction(new CHotkeyAction("Waiting for trade", "Orion.WaitForTrade([maxDelay=1000]);", this));

	QMenu *menuContextMenu = menu->addMenu("Context menu");
	menuContextMenu->addAction(new CHotkeyAction("Request", "Orion.RequestContextMenu('serial');", this));
	menuContextMenu->addAction(new CHotkeyAction("Wait context menu", "Orion.WaitContextMenu('serial', index);", this));
	menuContextMenu->addAction(new CHotkeyAction("Cancel context menu waiting", "Orion.CancelContextMenu();", this));
	menuContextMenu->addAction(new CHotkeyAction("Waiting for contect menu", "Orion.WaitForContextMenu([maxDelay=1000]);", this));

	QMenu *menuFilters = menu->addMenu("Filters");
	menuFilters->addAction(new CHotkeyAction("SetLight", "Orion.SetLight(true/false, [amount]);", this));
	menuFilters->addAction(new CHotkeyAction("SetWeather", "Orion.SetWeather(true/false, [index], [effectsCount], [temperature]);", this));
	menuFilters->addAction(new CHotkeyAction("SetSeason", "Orion.SetSeason(true/false, [index], [musicIndex]);", this));

	QMenu *menuTextWindow = menu->addMenu("Text window");
	menuTextWindow->addAction(new CHotkeyAction("Open", "TextWindow.Open();", this));
	menuTextWindow->addAction(new CHotkeyAction("Close", "TextWindow.Close();", this));
	menuTextWindow->addAction(new CHotkeyAction("Clear", "TextWindow.Clear();", this));
	menuTextWindow->addAction(new CHotkeyAction("Print", "TextWindow.Print('some text');", this));
	menuTextWindow->addAction(new CHotkeyAction("Set window position", "TextWindow.SetPos(x, y);", this));
	menuTextWindow->addAction(new CHotkeyAction("Set window size", "TextWindow.SetSize(width, height);", this));
	menuTextWindow->addAction(new CHotkeyAction("Save content to file", "TextWindow.SaveToFile('filePath');", this));

	QMenu *menuOther = menu->addMenu("Other");
	menuOther->addAction(new CHotkeyAction("Info tile", "Orion.InfoTile([lasttile]);", this));
	menuOther->addAction(new CHotkeyAction("Save config", "Orion.SaveConfig();", this));
	menuOther->addAction(new CHotkeyAction("CloseUO", "Orion.CloseUO();", this));
	menuOther->addAction(new CHotkeyAction("Sound", "Orion.Sound(index);", this));
	menuOther->addAction(new CHotkeyAction("SaveHotkeys", "Orion.SaveHotkeys('fileName');", this));
	menuOther->addAction(new CHotkeyAction("LoadHotkeys", "Orion.LoadHotkeys('fileName');", this));
	menuOther->addAction(new CHotkeyAction("Track", "Orion.Track(true/false, [x, y]);", this));
	menuOther->addAction(new CHotkeyAction("Now time (in ms)", "Orion.Now();", this));
	menuOther->addAction(new CHotkeyAction("BuffExists", "Orion.BuffExists(nameOrGraphic);", this));
	menuOther->addAction(new CHotkeyAction("Launch", "Orion.Launch(filePath, [arguments]);", this));
	menuOther->addAction(new CHotkeyAction("Contains text", "Orion.Contains(text, pattern, [ignoreCase=false]);", this));
	menuOther->addAction(new CHotkeyAction("Split text", "Orion.Contains(text, [separator], [skipEmptyWords=true]);", this));
	menuOther->addAction(new CHotkeyAction("Time", "Orion.Time([format]);", this));
	menuOther->addAction(new CHotkeyAction("Date", "Orion.Date([format]);", this));
	menuOther->addAction(new CHotkeyAction("Assistant version", "Orion.OAVersion();", this));
	menuOther->addAction(new CHotkeyAction("Connected", "Orion.Connected();", this));
	menuOther->addAction(new CHotkeyAction("Random", "Orion.Random([value]);", this));
	menuOther->addAction(new CHotkeyAction("Random min max", "Orion.Random(minValue, maxValue);", this));
	menuOther->addAction(new CHotkeyAction("Play WAV file", "Orion.PlayWav('fileName');", this));
	menuOther->addAction(new CHotkeyAction("Save screenshot", "Orion.Screenshot();", this));
	menuOther->addAction(new CHotkeyAction("LogOut", "Orion.LogOut();", this));
	menuOther->addAction(new CHotkeyAction("Set view range", "Orion.ClientViewRange(range);", this));
	menuOther->addAction(new CHotkeyAction("Get view range", "Orion.ClientViewRange();", this));
	menuOther->addAction(new CHotkeyAction("Load profile", "Orion.LoadProfile('name');", this));

	QMenu *menuPrompts = menu->addMenu("Prompts");
	menuPrompts->addAction(new CHotkeyAction("Exists", "Orion.PromptExists();", this));
	menuPrompts->addAction(new CHotkeyAction("Serial", "Orion.PromptSerial();", this));
	menuPrompts->addAction(new CHotkeyAction("ID", "Orion.PromptID();", this));
	menuPrompts->addAction(new CHotkeyAction("Wait prompt", "Orion.WaitPrompt(text, [serial='0'], [type='all']);", this));
	menuPrompts->addAction(new CHotkeyAction("Cancel wait prompt", "Orion.CancelWaitPrompt();", this));
	menuPrompts->addAction(new CHotkeyAction("Send current prompt", "Orion.SendPrompt('text');", this));
	menuPrompts->addAction(new CHotkeyAction("Waiting for prompt", "Orion.WaitForPrompt([maxDelay=1000]);", this));

	QMenu *menuFontColor = menu->addMenu("Font color");
	menuFontColor->addAction(new CHotkeyAction("Enable and set your font color", "Orion.SetFontColor(state, ['color']);", this));
	menuFontColor->addAction(new CHotkeyAction("Get your font color state", "Orion.GetFontColor();", this));
	menuFontColor->addAction(new CHotkeyAction("Get your font color value", "Orion.GetFontColorValue();", this));
	menuFontColor->addSeparator();
	menuFontColor->addAction(new CHotkeyAction("Enable and set characters font color", "Orion.SetCharactersFontColor(state, ['color']);", this));
	menuFontColor->addAction(new CHotkeyAction("Get characters font color state", "Orion.GetCharactersFontColor();", this));
	menuFontColor->addAction(new CHotkeyAction("Get characters font color value", "Orion.GetCharactersFontColorValue();", this));

	QMenu *menuFile = menu->addMenu("Work with files");
	menuFile->addAction(new CHotkeyAction("Create file object", "Orion.NewFile();", this));
	menuFile->addAction(new CHotkeyAction("Open", ".Open('filePath', [checkExists]);", this));
	menuFile->addAction(new CHotkeyAction("Append", ".Append('filePath', [checkExists]);", this));
	menuFile->addAction(new CHotkeyAction("Opened", ".Opened();", this));
	menuFile->addAction(new CHotkeyAction("Close", ".Close();", this));
	menuFile->addAction(new CHotkeyAction("Remove", ".Remove(['filePath']);", this));
	menuFile->addAction(new CHotkeyAction("ReadLine", ".ReadLine();", this));
	menuFile->addAction(new CHotkeyAction("Read", ".Read();", this));
	menuFile->addAction(new CHotkeyAction("WriteLine", ".WriteLine('some text');", this));
	menuFile->addAction(new CHotkeyAction("Write", ".Write('some text');", this));

	QMenu *menuLists = menu->addMenu("Work with lists");
	menuLists->addAction(new CHotkeyAction("Add (or save) type", "Orion.AddType('typeName', ['typeValue']);", this));
	menuLists->addAction(new CHotkeyAction("Remove type", "Orion.RemoveType('typeName');", this));
	menuLists->addSeparator();
	menuLists->addAction(new CHotkeyAction("Add (or save) object", "Orion.AddObject('objectName', ['objectValue']);", this));
	menuLists->addAction(new CHotkeyAction("Remove object", "Orion.RemoveObject('objectName');", this));
	menuLists->addSeparator();
	menuLists->addAction(new CHotkeyAction("Add find list item", "Orion.AddFindList(['listName'=targetRequest], ['graphic', 'color'], ['comment']);", this));
	menuLists->addAction(new CHotkeyAction("Clear find list", "Orion.ClearFindList('listName');", this));
	menuLists->addSeparator();
	menuLists->addAction(new CHotkeyAction("Add ignore list item", "Orion.AddIgnoreList(['listName'=targetRequest], ['graphic', 'color'], ['comment']);", this));
	menuLists->addAction(new CHotkeyAction("Add ignore list object", "Orion.AddIgnoreListObject(['listName'=targetRequest], ['serial'], ['comment']);", this));
	menuLists->addAction(new CHotkeyAction("Clear ignore list", "Orion.ClearIgnoreList('listName');", this));
	menuLists->addSeparator();
	menuLists->addAction(new CHotkeyAction("Add (or save) friend", "Orion.AddFriend('friendName', ['serial']);", this));
	menuLists->addAction(new CHotkeyAction("Remove friend", "Orion.RemoveFriend('friendName');", this));
	menuLists->addAction(new CHotkeyAction("Get friend list", "Orion.GetFriendList();", this));
	menuLists->addAction(new CHotkeyAction("Clear friend list", "Orion.ClearFriendList();", this));
	menuLists->addSeparator();
	menuLists->addAction(new CHotkeyAction("Add (or save) enemy", "Orion.AddEnemy('enemyName', ['serial']);", this));
	menuLists->addAction(new CHotkeyAction("Remove enemy", "Orion.RemoveEnemy('enemyName');", this));
	menuLists->addAction(new CHotkeyAction("Get enemy list", "Orion.GetEnemyList();", this));
	menuLists->addAction(new CHotkeyAction("Clear enemy list", "Orion.ClearEnemyList();", this));

	QMenu *menuClientOptions = menu->addMenu("Client options");

	QMenu *menuClientOptionsGet = menuClientOptions->addMenu("Get");
	menuClientOptionsGet->addAction(new CHotkeyAction("Sound", "Orion.OptionSound()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("SoundVolume", "Orion.OptionSoundVolume()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("Music", "Orion.OptionMusic()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("MusicVolume", "Orion.OptionMusicVolume()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("UseTooltips", "Orion.OptionUseTooltips()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("AlwaysRun", "Orion.OptionAlwaysRun()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("NewTargetSystem", "Orion.OptionNewTargetSystem()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("ObjectHandles", "Orion.OptionObjectHandles()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("ScaleSpeech", "Orion.OptionScaleSpeech()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("ScaleSpeechDelay", "Orion.OptionScaleSpeechDelay()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("IgnoreGuildMessages", "Orion.OptionIgnoreGuildMessages()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("IgnoreAllianceMessages", "Orion.OptionIgnoreAllianceMessages()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("DarkNights", "Orion.OptionDarkNights()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("ColoredLighting", "Orion.OptionColoredLighting()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("CriminalActionsQuery", "Orion.OptionCriminalActionsQuery()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("CircleOfTransparency", "Orion.OptionCircleOfTransparency()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("CircleOfTransparencyValue", "Orion.OptionCircleOfTransparencyValue()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("LockResizingGameWindow", "Orion.OptionLockResizingGameWindow()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("FPSValue", "Orion.OptionFPSValue()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("UseScalingGameWindow", "Orion.OptionUseScalingGameWindow()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("DrawStatusState", "Orion.OptionDrawStatusState()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("DrawStumps", "Orion.OptionDrawStumps()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("MarkingCaves", "Orion.OptionMarkingCaves()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("NoVegetation", "Orion.OptionNoVegetation()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("NoFieldsAnimation", "Orion.OptionNoFieldsAnimation()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("StandardCharactesFrameRate", "Orion.OptionStandardCharactesFrameRate()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("StandardItemsFrameRate", "Orion.OptionStandardItemsFrameRate()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("LockGumpsMoving", "Orion.OptionLockGumpsMoving()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("EnterChat", "Orion.OptionEnterChat()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("HiddenCharacters", "Orion.OptionHiddenCharacters()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("HiddenCharactersAlpha", "Orion.OptionHiddenCharactersAlpha()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("HiddenCharactersModeOnlyForSelf", "Orion.OptionHiddenCharactersModeOnlyForSelf()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("TransparentSpellIcons", "Orion.OptionTransparentSpellIcons()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("SpellIconsAlpha", "Orion.OptionSpellIconsAlpha()", this));
	menuClientOptionsGet->addAction(new CHotkeyAction("FastRotation", "Orion.OptionFastRotation()", this));

	QMenu *menuClientOptionsSet = menuClientOptions->addMenu("Set");
	menuClientOptionsSet->addAction(new CHotkeyAction("Sound", "Orion.OptionSound(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("SoundVolume", "Orion.OptionSoundVolume(value);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("Music", "Orion.OptionMusic(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("MusicVolume", "Orion.OptionMusicVolume(value);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("UseTooltips", "Orion.OptionUseTooltips(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("AlwaysRun", "Orion.OptionAlwaysRun(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("NewTargetSystem", "Orion.OptionNewTargetSystem(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("ObjectHandles", "Orion.OptionObjectHandles(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("ScaleSpeech", "Orion.OptionScaleSpeech(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("ScaleSpeechDelay", "Orion.OptionScaleSpeechDelay(value);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("IgnoreGuildMessages", "Orion.OptionIgnoreGuildMessages(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("IgnoreAllianceMessages", "Orion.OptionIgnoreAllianceMessages(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("DarkNights", "Orion.OptionDarkNights(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("ColoredLighting", "Orion.OptionColoredLighting(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("CriminalActionsQuery", "Orion.OptionCriminalActionsQuery(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("CircleOfTransparency", "Orion.OptionCircleOfTransparency(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("CircleOfTransparencyValue", "Orion.OptionCircleOfTransparencyValue(value);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("LockResizingGameWindow", "Orion.OptionLockResizingGameWindow(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("FPSValue", "Orion.OptionFPSValue(value);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("UseScalingGameWindow", "Orion.OptionUseScalingGameWindow(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("DrawStatusState", "Orion.OptionDrawStatusState(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("DrawStumps", "Orion.OptionDrawStumps(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("MarkingCaves", "Orion.OptionMarkingCaves(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("NoVegetation", "Orion.OptionNoVegetation(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("NoFieldsAnimation", "Orion.OptionNoFieldsAnimation(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("StandardCharactesFrameRate", "Orion.OptionStandardCharactesFrameRate(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("StandardItemsFrameRate", "Orion.OptionStandardItemsFrameRate(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("LockGumpsMoving", "Orion.OptionLockGumpsMoving(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("EnterChat", "Orion.OptionEnterChat(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("HiddenCharacters", "Orion.OptionHiddenCharacters(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("HiddenCharactersAlpha", "Orion.OptionHiddenCharactersAlpha(value);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("HiddenCharactersModeOnlyForSelf", "Orion.OptionHiddenCharactersModeOnlyForSelf(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("TransparentSpellIcons", "Orion.OptionTransparentSpellIcons(state);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("SpellIconsAlpha", "Orion.OptionSpellIconsAlpha(value);", this));
	menuClientOptionsSet->addAction(new CHotkeyAction("FastRotation", "Orion.OptionFastRotation(state);", this));

	QMenu *menuObjectProperties = menu->addMenu("Object properties");

	QMenu *menuPropJournalObject = menuObjectProperties->addMenu("Journal object");
	menuPropJournalObject->addAction(new CHotkeyAction("Serial", ".Serial()", this));
	menuPropJournalObject->addAction(new CHotkeyAction("Timer", ".Timer()", this));
	menuPropJournalObject->addAction(new CHotkeyAction("Color", ".Color()", this));
	menuPropJournalObject->addAction(new CHotkeyAction("Text", ".Text()", this));
	menuPropJournalObject->addAction(new CHotkeyAction("FindTextID", ".FindTextID()", this));
	menuPropJournalObject->addAction(new CHotkeyAction("SetText", ".SetText('newText')", this));

	QMenu *menuPropGameObject = menuObjectProperties->addMenu("Game object");
	menuPropGameObject->addAction(new CHotkeyAction("Serial", ".Serial()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Graphic", ".Graphic()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Color", ".Color()", this));
	menuPropGameObject->addAction(new CHotkeyAction("X", ".X()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Y", ".Y()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Z", ".Z()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Container", ".Container()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Map", ".Map()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Count", ".Count()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Flags", ".Flags()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Name", ".Name()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Mobile", ".Mobile()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Ignored", ".Ignored()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Frozen", ".Frozen()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Poisoned", ".Poisoned()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Flying", ".Flying()", this));
	menuPropGameObject->addAction(new CHotkeyAction("YellowHits", ".YellowHits()", this));
	menuPropGameObject->addAction(new CHotkeyAction("IgnoreCharacters", ".IgnoreCharacters()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Locked", ".Locked()", this));
	menuPropGameObject->addAction(new CHotkeyAction("WarMode", ".WarMode()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Hidden", ".Hidden()", this));
	menuPropGameObject->addAction(new CHotkeyAction("IsHuman", ".IsHuman()", this));
	menuPropGameObject->addAction(new CHotkeyAction("IsPlayer", ".IsPlayer()", this));
	menuPropGameObject->addAction(new CHotkeyAction("IsCorpse", ".IsCorpse()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Layer", ".Layer()", this));
	menuPropGameObject->addAction(new CHotkeyAction("IsMulti", ".IsMulti()", this));
	menuPropGameObject->addAction(new CHotkeyAction("EquipLayer", ".EquipLayer()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Hits", ".Hits()", this));
	menuPropGameObject->addAction(new CHotkeyAction("MaxHits", ".MaxHits()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Mana", ".Mana()", this));
	menuPropGameObject->addAction(new CHotkeyAction("MaxMana", ".MaxMana()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Stam", ".Stam()", this));
	menuPropGameObject->addAction(new CHotkeyAction("MaxStam", ".MaxStam()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Female", ".Female()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Race", ".Race()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Direction", ".Direction()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Notoriety", ".Notoriety()", this));
	menuPropGameObject->addAction(new CHotkeyAction("CanChangeName", ".CanChangeName()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Dead", ".Dead()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Exists", ".Exists()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Properties", ".Properties()", this));
	menuPropGameObject->addAction(new CHotkeyAction("ProfileReceived", ".ProfileReceived()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Profile", ".Profile()", this));
	menuPropGameObject->addAction(new CHotkeyAction("Title", ".Title()", this));

	QMenu *menuPropPlayer = menuObjectProperties->addMenu("Player");
	menuPropPlayer->addAction(new CHotkeyAction("Serial", "Player.Serial()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Graphic", "Player.Graphic()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Color", "Player.Color()", this));
	menuPropPlayer->addAction(new CHotkeyAction("X", "Player.X()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Y", "Player.Y()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Z", "Player.Z()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Container", "Player.Container()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Map", "Player.Map()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Count", "Player.Count()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Flags", "Player.Flags()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Name", "Player.Name()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Mobile", "Player.Mobile()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Ignored", "Player.Ignored()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Frozen", "Player.Frozen()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Poisoned", "Player.Poisoned()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Paralyzed", "Player.Paralyzed()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Flying", "Player.Flying()", this));
	menuPropPlayer->addAction(new CHotkeyAction("YellowHits", "Player.YellowHits()", this));
	menuPropPlayer->addAction(new CHotkeyAction("IgnoreCharacters", "Player.IgnoreCharacters()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Locked", "Player.Locked()", this));
	menuPropPlayer->addAction(new CHotkeyAction("WarMode", "Player.WarMode()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Hidden", "Player.Hidden()", this));
	menuPropPlayer->addAction(new CHotkeyAction("IsHuman", "Player.IsHuman()", this));
	menuPropPlayer->addAction(new CHotkeyAction("IsPlayer", "Player.IsPlayer()", this));
	menuPropPlayer->addAction(new CHotkeyAction("IsCorpse", "Player.IsCorpse()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Layer", "Player.Layer()", this));
	menuPropPlayer->addAction(new CHotkeyAction("IsMulti", "Player.IsMulti()", this));
	menuPropPlayer->addAction(new CHotkeyAction("EquipLayer", "Player.EquipLayer()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Hits", "Player.Hits()", this));
	menuPropPlayer->addAction(new CHotkeyAction("MaxHits", "Player.MaxHits()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Mana", "Player.Mana()", this));
	menuPropPlayer->addAction(new CHotkeyAction("MaxMana", "Player.MaxMana()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Stam", "Player.Stam()", this));
	menuPropPlayer->addAction(new CHotkeyAction("MaxStam", "Player.MaxStam()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Female", "Player.Female()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Race", "Player.Race()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Direction", "Player.Direction()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Notoriety", "Player.Notoriety()", this));
	menuPropPlayer->addAction(new CHotkeyAction("CanChangeName", "Player.CanChangeName()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Dead", "Player.Dead()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Str", "Player.Str()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Int", "Player.Int()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Dex", "Player.Dex()", this));
	menuPropPlayer->addAction(new CHotkeyAction("LockStrState", "Player.LockStrState()", this));
	menuPropPlayer->addAction(new CHotkeyAction("LockIntState", "Player.LockIntState()", this));
	menuPropPlayer->addAction(new CHotkeyAction("LockDexState", "Player.LockDexState()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Weight", "Player.Weight()", this));
	menuPropPlayer->addAction(new CHotkeyAction("MaxWeight", "Player.MaxWeight()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Armor", "Player.Armor()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Gold", "Player.Gold()", this));
	menuPropPlayer->addAction(new CHotkeyAction("StatsCap", "Player.StatsCap()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Followers", "Player.Followers()", this));
	menuPropPlayer->addAction(new CHotkeyAction("MaxFollowers", "Player.MaxFollowers()", this));
	menuPropPlayer->addAction(new CHotkeyAction("FireResistance", "Player.FireResistance()", this));
	menuPropPlayer->addAction(new CHotkeyAction("ColdResistance", "Player.ColdResistance()", this));
	menuPropPlayer->addAction(new CHotkeyAction("PoisonResistance", "Player.PoisonResistance()", this));
	menuPropPlayer->addAction(new CHotkeyAction("EnergyResistance", "Player.EnergyResistance()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Luck", "Player.Luck()", this));
	menuPropPlayer->addAction(new CHotkeyAction("MinDamage", "Player.MinDamage()", this));
	menuPropPlayer->addAction(new CHotkeyAction("MaxDamage", "Player.MaxDamage()", this));
	menuPropPlayer->addAction(new CHotkeyAction("TithingPoints", "Player.TithingPoints()", this));
	menuPropPlayer->addAction(new CHotkeyAction("StealthSteps", "Player.StealthSteps()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Properties", "Player.Properties()", this));
	menuPropPlayer->addAction(new CHotkeyAction("ProfileReceived", "Player.ProfileReceived()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Profile", "Player.Profile()", this));
	menuPropPlayer->addAction(new CHotkeyAction("Title", "Player.Title()", this));

	QMenu *menuPropMenu = menuObjectProperties->addMenu("Menu");
	menuPropMenu->addAction(new CHotkeyAction("Serial", ".Serial()", this));
	menuPropMenu->addAction(new CHotkeyAction("ID", ".ID()", this));
	menuPropMenu->addAction(new CHotkeyAction("Name", ".Name()", this));
	menuPropMenu->addAction(new CHotkeyAction("IsGrayMenu", ".IsGrayMenu()", this));
	menuPropMenu->addAction(new CHotkeyAction("Select by index", ".Select(index)", this));
	menuPropMenu->addAction(new CHotkeyAction("Select by name", ".Select(name)", this));
	menuPropMenu->addAction(new CHotkeyAction("Close", ".Close()", this));
	menuPropMenu->addAction(new CHotkeyAction("ItemsCount", ".ItemsCount()", this));
	menuPropMenu->addAction(new CHotkeyAction("Item ID by index", ".ItemID(index)", this));
	menuPropMenu->addAction(new CHotkeyAction("Item Graphic by index", ".ItemGraphic(index)", this));
	menuPropMenu->addAction(new CHotkeyAction("Item Color by index", ".ItemColor(index)", this));
	menuPropMenu->addAction(new CHotkeyAction("Item Name by index", ".ItemName(index)", this));

	QMenu *menuPropCoordinates = menuObjectProperties->addMenu("Coordinates");
	menuPropCoordinates->addAction(new CHotkeyAction("X", ".X()", this));
	menuPropCoordinates->addAction(new CHotkeyAction("Y", ".Y()", this));

	QMenu *menuPropGump = menuObjectProperties->addMenu("Gump");
	menuPropGump->addAction(new CHotkeyAction("Serial", ".Serial()", this));
	menuPropGump->addAction(new CHotkeyAction("ID", ".ID()", this));
	menuPropGump->addAction(new CHotkeyAction("X", ".X()", this));
	menuPropGump->addAction(new CHotkeyAction("Y", ".Y()", this));
	menuPropGump->addAction(new CHotkeyAction("Replayed", ".Replayed()", this));
	menuPropGump->addAction(new CHotkeyAction("ReplyID", ".ReplyID()", this));
	menuPropGump->addAction(new CHotkeyAction("ButtonList", ".ButtonList()", this));
	menuPropGump->addAction(new CHotkeyAction("CheckboxList", ".CheckboxList()", this));
	menuPropGump->addAction(new CHotkeyAction("RadioList", ".RadioList()", this));
	menuPropGump->addAction(new CHotkeyAction("TilepicList", ".TilepicList()", this));
	menuPropGump->addAction(new CHotkeyAction("GumppicList", ".GumppicList()", this));
	menuPropGump->addAction(new CHotkeyAction("EntriesList", ".EntriesList()", this));
	menuPropGump->addAction(new CHotkeyAction("CommandList", ".CommandList()", this));
	menuPropGump->addAction(new CHotkeyAction("TextList", ".TextList()", this));
	menuPropGump->addAction(new CHotkeyAction("Command", ".Command(index)", this));
	menuPropGump->addAction(new CHotkeyAction("Text", ".Text(index)", this));
	menuPropGump->addAction(new CHotkeyAction("Select", ".Select(hook)", this));
	menuPropGump->addAction(new CHotkeyAction("Close", ".Close()", this));

	QMenu *menuPropGumpHook = menuObjectProperties->addMenu("Gump hook");
	menuPropGumpHook->addAction(new CHotkeyAction("Index", ".Index()", this));
	menuPropGumpHook->addAction(new CHotkeyAction("AddEntry", ".AddEntry(index, 'text')", this));
	menuPropGumpHook->addAction(new CHotkeyAction("AddCheck", ".AddCheck(index, state)", this));

	QMenu *menuPropSelectedClientTile = menuObjectProperties->addMenu("Selected slient tile");
	menuPropSelectedClientTile->addAction(new CHotkeyAction("Serial", "SelectedTile.Serial()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("Graphic", "SelectedTile.Graphic()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("Color", "SelectedTile.Color()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("X", "SelectedTile.X()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("Y", "SelectedTile.Y()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("Z", "SelectedTile.Z()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("Land tile graphic", "SelectedTile.LandGraphic()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("Land tile x", "SelectedTile.LandX()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("Land tile y", "SelectedTile.LandY()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("Land tile z", "SelectedTile.LandZ()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("IsLandTile", "SelectedTile.IsLandTile()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("IsStaticTile", "SelectedTile.IsStaticTile()", this));
	menuPropSelectedClientTile->addAction(new CHotkeyAction("IsGameObject", "SelectedTile.IsGameObject()", this));

	QMenu *menuPropClientMacro = menuObjectProperties->addMenu("Client macro");

	menuObjectProperties->addAction(new CHotkeyAction("Create client macro", "Orion.CreateClientMacro(['action'], ['subAction'])", this));

	if (!g_ClientMacro.empty())
	{
		QMenu *menuClientMacro = menuPropClientMacro->addMenu("Add macro action to queue");

		for (const CClientMacro &macro : g_ClientMacro)
		{
			const QStringList &macroActions = macro.m_Actions;
			const QString &macroName = macro.GetName();

			if (macroActions.empty())
				menuClientMacro->addAction(new CHotkeyAction(macroName, "macro.AddAction('" + macroName + "');", this));
			else
			{
				QMenu *menuClientMacroActions = menuClientMacro->addMenu(macroName);

				for (const QString &str : macroActions)
				{
					menuClientMacroActions->addAction(new CHotkeyAction(str, "macro.AddAction('" + macroName + "', '" + str + "');", this));
				}
			}
		}
	}
	else
		menuPropClientMacro->addAction(new CHotkeyAction("Add macro action to queue", "macro.AddAction('action', ['subAction'])", this));

	menuPropClientMacro->addAction(new CHotkeyAction("Play macro queue", "macro.Play([waitForEndOfPlaying=false], [maxDelay=10050000])", this));

	menu->popup(QCursor::pos());
}
//----------------------------------------------------------------------------------
