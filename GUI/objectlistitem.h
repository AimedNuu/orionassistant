/***********************************************************************************
**
** ObjectListItem.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OBJECTLISTITEM_H
#define OBJECTLISTITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QListWidgetItem>
#include <QListWidget>
//----------------------------------------------------------------------------------
class CObjectListItem : public QListWidgetItem
{
	SETGET(uint, Serial, 0)

public:
	CObjectListItem(const QString &name, const QString &serial, QListWidget *parent);
	virtual ~CObjectListItem() {}
};
//----------------------------------------------------------------------------------
#endif // OBJECTLISTITEM_H
//----------------------------------------------------------------------------------
