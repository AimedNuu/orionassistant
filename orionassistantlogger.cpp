﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
//----------------------------------------------------------------------------------
#include "orionassistantlogger.h"
#include <stdarg.h>
#include <locale>
#include "orionassistant_global.h"
//----------------------------------------------------------------------------------
CLogger g_Logger;
//----------------------------------------------------------------------------------
CLogger::CLogger()
: m_File(NULL)
{
	OAFUN_DEBUG("c3_f1");
}
//----------------------------------------------------------------------------------
CLogger::~CLogger()
{
	OAFUN_DEBUG("c3_f2");
	if (m_File != NULL)
	{
		LOG("Log closed.\n");
		fclose(m_File);
	}
}
//----------------------------------------------------------------------------------
void CLogger::Init(const string &filePath)
{
	OAFUN_DEBUG("c3_f3");
	if (m_File != NULL)
		fclose(m_File);

	fopen_s(&m_File, filePath.c_str(), "w");
	LOG("Log opened.\n");
}
//----------------------------------------------------------------------------------
void CLogger::Init(const wstring &filePath)
{
	OAFUN_DEBUG("c3_f4");
	if (m_File != NULL)
		fclose(m_File);

	_wfopen_s(&m_File, filePath.c_str(), L"w");
	LOG("Log opened.\n");
}
//----------------------------------------------------------------------------------
void CLogger::Print(const char *format, ...)
{
	OAFUN_DEBUG("c3_f5");
	if (m_File == NULL)
		return;

	va_list arg;
	va_start(arg, format);
	vfprintf(m_File, format, arg);
	va_end(arg);
	fflush(m_File);
}
//----------------------------------------------------------------------------------
void CLogger::VPrint(const char *format, va_list ap)
{
	OAFUN_DEBUG("c3_f6");
	if (m_File == NULL)
		return;

	vfprintf(m_File, format, ap);
	fflush(m_File);
}
//----------------------------------------------------------------------------------
void CLogger::Print(const wchar_t *format, ...)
{
	OAFUN_DEBUG("c3_f7");
	if (m_File == NULL)
		return;

	va_list arg;
	va_start(arg, format);
	vfwprintf(m_File, format, arg);
	va_end(arg);
	fflush(m_File);
}
//----------------------------------------------------------------------------------
void CLogger::VPrint(const wchar_t *format, va_list ap)
{
	OAFUN_DEBUG("c3_f8");
	if (m_File == NULL)
		return;

	vfwprintf(m_File, format, ap);
	fflush(m_File);
}
//----------------------------------------------------------------------------------
void CLogger::Dump(uchar *buf, int size)
{
	OAFUN_DEBUG("c3_f9");
	if (m_File == NULL)
		return;

	int num_lines = size / 16;

	if (size % 16 != 0)
		num_lines++;

	for (int line = 0; line < num_lines; line++)
	{
		int row = 0;
		fprintf(m_File, "%04X: ", line * 16);

		for (row = 0; row < 16; row++)
		{
			if (line * 16 + row < size)
				fprintf(m_File, "%02X ", buf[line * 16 + row]);
			else
				fprintf(m_File, "-- ");
		}

		fprintf(m_File, ": ");

		for (row = 0; row < 16; row++)
		{
			if (line * 16 + row < size)
				fputc(isprint(buf[line * 16 + row]) ? buf[line * 16 + row] : '.', m_File);
		}

		fputc('\n', m_File);
	}

	fflush(m_File);
}
//----------------------------------------------------------------------------------
