/***********************************************************************************
**
** OAScriptHandle.h
**
** Copyright (C) January 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OASCRIPTHANDLE_H
#define OASCRIPTHANDLE_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "oascriptengine.h"
#include "oatextwindowengine.h"
#include "oaplayerengine.h"
#include "oaclientselectedtile.h"
#include "../GUI/runningscriptlistitem.h"
#include <QThread>
//----------------------------------------------------------------------------------
class COAScriptHandle : public QObject
{
	Q_OBJECT

public slots:
	void process();

signals:
	void finished();

private:
	void RunEngine();

private:
	class CRunningScriptListItem *m_Parent{nullptr};

	COAScriptEngine *m_ScriptEngine{nullptr};

	COAScriptEngineTextWindow *m_ScriptEngineTextWindow{nullptr};

	COAPlayerObject *m_ScriptEnginePlayer{nullptr};

	QString m_FunctionName{""};

	QStringList m_Args;

	QString m_Text{""};

public:
	COAScriptHandle(class CRunningScriptListItem *parent, const QString &functionName, const QStringList &args, const QString &scriptBody);
	virtual ~COAScriptHandle();

	bool GetPaused();

	void SetPaused(bool value);

	void TerminateScript();
};
//----------------------------------------------------------------------------------
#endif // OASCRIPTHANDLE_H
//----------------------------------------------------------------------------------
