/***********************************************************************************
**
** OAFileEngine.h
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OAFILEENGINE_H
#define OAFILEENGINE_H
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
#include <QFile>
#include <QTextStream>
//----------------------------------------------------------------------------------
class COAScriptFileObject : public QObject
{
	Q_OBJECT

private:
	QFile m_File;
	QTextStream m_Stream;

public:
	COAScriptFileObject();
	virtual ~COAScriptFileObject();

	Q_INVOKABLE bool Open(const QString &filePath);

	Q_INVOKABLE bool Open(const QString &filePath, const bool &checkExists);

	Q_INVOKABLE bool Append(const QString &filePath);

	Q_INVOKABLE bool Append(const QString &filePath, const bool &checkExists);

	Q_INVOKABLE bool Opened();

	Q_INVOKABLE void Close();

	Q_INVOKABLE QString ReadLine();

	Q_INVOKABLE QString Read();

	Q_INVOKABLE void WriteLine(const QString &str);

	Q_INVOKABLE void Write(const QString &str);

	Q_INVOKABLE void Remove();

	Q_INVOKABLE void Remove(const QString &filePath);
};
//----------------------------------------------------------------------------------
#endif // OAFILEENGINE_H
//----------------------------------------------------------------------------------
