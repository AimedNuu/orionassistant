// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAGumpEngine.cpp
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oagumpengine.h"
#include "../OrionAssistant/orionassistant.h"
#include "oagumphookengine.h"
#include "../Managers/commandmanager.h"
//----------------------------------------------------------------------------------
COAScriptGumpObject::COAScriptGumpObject(CGump *parent)
: QObject(), m_Parent(parent)
{
	OAFUN_DEBUG("c35_f3");

	if (parent != nullptr)
	{
		m_Gump.SetSerial(parent->GetSerial());
		m_Gump.SetID(parent->GetID());
		m_Gump.SetX(parent->GetX());
		m_Gump.SetY(parent->GetY());
		m_Gump.SetReplayed(parent->GetReplayed());
		m_Gump.SetReplyID(parent->GetReplyID());

		if (!parent->m_Items.empty())
		{
			for (const CGumpItem &item : parent->m_Items)
				m_Gump.m_Items.push_back(item);
		}

		m_Gump.m_Text << parent->m_Text;
	}
}
//----------------------------------------------------------------------------------
int COAScriptGumpObject::Serial()
{
	OAFUN_DEBUG("c35_f2");
	return m_Gump.GetSerial();
}
//----------------------------------------------------------------------------------
int COAScriptGumpObject::ID()
{
	OAFUN_DEBUG("c35_f3");
	return m_Gump.GetID();
}
//----------------------------------------------------------------------------------
int COAScriptGumpObject::X()
{
	OAFUN_DEBUG("c35_f4");
	return m_Gump.GetX();
}
//----------------------------------------------------------------------------------
int COAScriptGumpObject::Y()
{
	OAFUN_DEBUG("c35_f5");
	return m_Gump.GetY();
}
//----------------------------------------------------------------------------------
bool COAScriptGumpObject::Replayed()
{
	OAFUN_DEBUG("c35_f6");
	return m_Gump.GetReplayed();
}
//----------------------------------------------------------------------------------
int COAScriptGumpObject::ReplyID()
{
	OAFUN_DEBUG("c35_f7");
	return m_Gump.GetReplyID();
}
//----------------------------------------------------------------------------------
QStringList COAScriptGumpObject::ButtonList()
{
	OAFUN_DEBUG("c35_f8");

	QStringList result;

	if (!m_Gump.m_Items.empty())
	{
		for (const CGumpItem &item : m_Gump.m_Items)
		{
			if (item.GetType() == GIT_BUTTON || item.GetType() == GIT_BUTTONTILEART)
				result << item.GetCommand();
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
QStringList COAScriptGumpObject::CheckboxList()
{
	OAFUN_DEBUG("c35_f9");

	QStringList result;

	if (!m_Gump.m_Items.empty())
	{
		for (const CGumpItem &item : m_Gump.m_Items)
		{
			if (item.GetType() == GIT_CHECKBOX)
				result << item.GetCommand();
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
QStringList COAScriptGumpObject::RadioList()
{
	OAFUN_DEBUG("c35_f10");

	QStringList result;

	if (!m_Gump.m_Items.empty())
	{
		for (const CGumpItem &item : m_Gump.m_Items)
		{
			if (item.GetType() == GIT_RADIO)
				result << item.GetCommand();
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
QStringList COAScriptGumpObject::TilepicList()
{
	OAFUN_DEBUG("c35_f11");

	QStringList result;

	if (!m_Gump.m_Items.empty())
	{
		for (const CGumpItem &item : m_Gump.m_Items)
		{
			if (item.GetType() == GIT_TILEPIC || item.GetType() == GIT_TILEPICHUE)
				result << item.GetCommand();
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
QStringList COAScriptGumpObject::GumppicList()
{
	OAFUN_DEBUG("c35_f12");

	QStringList result;

	if (!m_Gump.m_Items.empty())
	{
		for (const CGumpItem &item : m_Gump.m_Items)
		{
			if (item.GetType() == GIT_GUMPPIC || item.GetType() == GIT_GUMPPICTILED)
				result << item.GetCommand();
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
QStringList COAScriptGumpObject::EntriesList()
{
	OAFUN_DEBUG("c35_f13");

	QStringList result;

	if (!m_Gump.m_Items.empty())
	{
		for (const CGumpItem &item : m_Gump.m_Items)
		{
			if (item.GetType() == GIT_TEXTENTRY || item.GetType() == GIT_TEXTENTRYLIMITED)
				result << item.GetCommand();
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
QStringList COAScriptGumpObject::CommandList()
{
	OAFUN_DEBUG("c35_f14");

	QStringList result;

	if (!m_Gump.m_Items.empty())
	{
		for (const CGumpItem &item : m_Gump.m_Items)
			result << item.GetCommand();
	}

	return result;
}
//----------------------------------------------------------------------------------
QStringList COAScriptGumpObject::TextList()
{
	OAFUN_DEBUG("c35_f15");
	return m_Gump.m_Text;
}
//----------------------------------------------------------------------------------
QString COAScriptGumpObject::Command(const int &index)
{
	OAFUN_DEBUG("c35_f16");

	if (index >= 0 && index < m_Gump.m_Items.size())
		return m_Gump.m_Items[index].GetCommand();

	return "";
}
//----------------------------------------------------------------------------------
QString COAScriptGumpObject::Text(const int &index)
{
	OAFUN_DEBUG("c35_f17");

	if (index >= 0 && index < m_Gump.m_Text.size())
		return m_Gump.m_Text[index];

	return "";
}
//----------------------------------------------------------------------------------
bool COAScriptGumpObject::Select(QObject *hook)
{
	OAFUN_DEBUG("c35_f18");
	bool result = false;
	emit g_CommandManager.CommandSelectGump(m_Parent, ((COAScriptGumpHookObject*)hook)->GetHook(), &result);

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptGumpObject::Close()
{
	OAFUN_DEBUG("c35_f18");
	bool result = false;

	CGumpHook hook(nullptr);
	hook.SetIndex(0);

	emit g_CommandManager.CommandSelectGump(m_Parent, &hook, &result);
}
//----------------------------------------------------------------------------------
