// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAMenuEngine.cpp
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oamenuengine.h"
#include "../OrionAssistant/orionassistant.h"
#include "../Managers/commandmanager.h"
//----------------------------------------------------------------------------------
COAScriptMenuObject::COAScriptMenuObject(CMenu *menu)
: QObject()
{
	OAFUN_DEBUG("c36_f1");
	if (menu != nullptr)
	{
		m_Menu.SetSerial(menu->GetSerial());
		m_Menu.SetID(menu->GetID());
		m_Menu.SetGrayMenu(menu->GetGrayMenu());
		m_Menu.SetName(menu->GetName());

		if (!menu->m_Items.empty())
		{
			for (const CMenuItem &item : menu->m_Items)
				m_Menu.m_Items.push_back(item);
		}
	}
}
//----------------------------------------------------------------------------------
QString COAScriptMenuObject::Serial()
{
	OAFUN_DEBUG("c36_f2");
	return COrionAssistant::SerialToText(m_Menu.GetSerial());
}
//----------------------------------------------------------------------------------
QString COAScriptMenuObject::ID()
{
	OAFUN_DEBUG("c36_f3");
	return COrionAssistant::GraphicToText(m_Menu.GetID());
}
//----------------------------------------------------------------------------------
QString COAScriptMenuObject::Name()
{
	OAFUN_DEBUG("c36_f4");
	return m_Menu.GetName();
}
//----------------------------------------------------------------------------------
bool COAScriptMenuObject::IsGrayMenu()
{
	OAFUN_DEBUG("c36_f5");
	return m_Menu.GetGrayMenu();
}
//----------------------------------------------------------------------------------
bool COAScriptMenuObject::Select(const int &index)
{
	OAFUN_DEBUG("c36_f6");
	bool result = false;
	if (index >= 0 && index < m_Menu.m_Items.size())
		emit g_CommandManager.CommandSelectMenu(m_Menu.GetName(), m_Menu.m_Items[index].GetName(), &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptMenuObject::Select(const QString &name)
{
	OAFUN_DEBUG("c36_f7");
	bool result = false;
	emit g_CommandManager.CommandSelectMenu(m_Menu.GetName(), name, &result);

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptMenuObject::Close()
{
	OAFUN_DEBUG("c36_f8");
	emit g_CommandManager.CommandCloseMenu(m_Menu.GetName());
}
//----------------------------------------------------------------------------------
int COAScriptMenuObject::ItemsCount()
{
	OAFUN_DEBUG("c36_f9");
	return m_Menu.m_Items.size();
}
//----------------------------------------------------------------------------------
int COAScriptMenuObject::ItemID(const int &index)
{
	OAFUN_DEBUG("c36_f10");
	if (index >= 0 && index < m_Menu.m_Items.size())
		return m_Menu.m_Items[index].GetID();

	return 0;
}
//----------------------------------------------------------------------------------
QString COAScriptMenuObject::ItemGraphic(const int &index)
{
	OAFUN_DEBUG("c36_f11");
	if (index >= 0 && index < m_Menu.m_Items.size())
		return COrionAssistant::GraphicToText(m_Menu.m_Items[index].GetGraphic());

	return "";
}
//----------------------------------------------------------------------------------
QString COAScriptMenuObject::ItemColor(const int &index)
{
	OAFUN_DEBUG("c36_f12");
	if (index >= 0 && index < m_Menu.m_Items.size())
		return COrionAssistant::GraphicToText(m_Menu.m_Items[index].GetColor());

	return "";
}
//----------------------------------------------------------------------------------
QString COAScriptMenuObject::ItemName(const int &index)
{
	OAFUN_DEBUG("c36_f13");
	if (index >= 0 && index < m_Menu.m_Items.size())
		return m_Menu.m_Items[index].GetName();

	return "";
}
//----------------------------------------------------------------------------------
