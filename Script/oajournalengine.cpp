// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAJournalEngine.cpp
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oajournalengine.h"
#include "../OrionAssistant/orionassistant.h"
#include "../Managers/journalmanager.h"
//----------------------------------------------------------------------------------
COAScriptJournalObject::COAScriptJournalObject(CJournalMessage *msg)
: QObject(), m_Parent(msg)
{
	OAFUN_DEBUG("c35_f1");
	if (msg != nullptr)
		m_Message = *msg;
}
//----------------------------------------------------------------------------------
QString COAScriptJournalObject::Serial()
{
	OAFUN_DEBUG("c35_f2");
	return COrionAssistant::SerialToText(m_Message.GetSerial());
}
//----------------------------------------------------------------------------------
int COAScriptJournalObject::Timer()
{
	OAFUN_DEBUG("c35_f3");
	return (int)m_Message.GetTimer();
}
//----------------------------------------------------------------------------------
QString COAScriptJournalObject::Color()
{
	OAFUN_DEBUG("c35_f4");
	return COrionAssistant::GraphicToText(m_Message.GetColor());
}
//----------------------------------------------------------------------------------
QString COAScriptJournalObject::Text()
{
	OAFUN_DEBUG("c35_f5");
	return m_Message.GetText();
}
//----------------------------------------------------------------------------------
uchar COAScriptJournalObject::Flags()
{
	OAFUN_DEBUG("c35_f6");
	return m_Message.GetFlags();
}
//----------------------------------------------------------------------------------
int COAScriptJournalObject::FindTextID()
{
	OAFUN_DEBUG("c35_f7");
	return m_Message.GetFindTextID();
}
//----------------------------------------------------------------------------------
void COAScriptJournalObject::SetText(const QString &newText)
{
	OAFUN_DEBUG("c35_f8");
	g_JournalManager.SetMessageText(m_Parent, newText);
}
//----------------------------------------------------------------------------------
