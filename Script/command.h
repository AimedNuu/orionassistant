/***********************************************************************************
**
** Command.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef CCOMMAND_H
#define CCOMMAND_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QList>
//----------------------------------------------------------------------------------
typedef QList<QString> COMMAND_ARGS;
class CTextCommandManager;
typedef void (CTextCommandManager::*CMD_HANDLER)(const COMMAND_ARGS &, const class CCommand &);
//----------------------------------------------------------------------------------
class CCommand
{
	SETGET(bool, Enabled, false)
	SETGET(QString, Description, "")
	SETGET(uint64, Flags, 0)

public:
	CCommand();
	CCommand(const bool &enabled, const uint64 &flags, const CMD_HANDLER &handler, const QString &description);
	~CCommand();

	CMD_HANDLER Handler{nullptr};
};
//----------------------------------------------------------------------------------
#endif // CCOMMAND_H
//----------------------------------------------------------------------------------
