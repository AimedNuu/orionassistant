/***********************************************************************************
**
** OAGameObjectEngine.h
**
** Copyright (C) Fabruary 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OAGAMEOBJECTENGINE_H
#define OAGAMEOBJECTENGINE_H
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
//----------------------------------------------------------------------------------
class COAScriptPositionObject : public QObject
{
	Q_OBJECT

private:
	int m_X{ 0 };
	int m_Y{ 0 };

public:
	COAScriptPositionObject(const int &x, const int &y) : m_X(x), m_Y(y) {}
	virtual ~COAScriptPositionObject() {}

	Q_INVOKABLE int X();

	Q_INVOKABLE int Y();
};
//----------------------------------------------------------------------------------
class COAScriptGameObject : public QObject
{
	Q_OBJECT

private:
	uint m_Serial{ 0 };

public:
	COAScriptGameObject(const uint &serial);
	virtual ~COAScriptGameObject() {}

	Q_INVOKABLE QString Serial();

	Q_INVOKABLE QString Graphic();

	Q_INVOKABLE QString Color();

	Q_INVOKABLE int X();

	Q_INVOKABLE int Y();

	Q_INVOKABLE int Z();

	Q_INVOKABLE QString Container();

	Q_INVOKABLE int Map();

	Q_INVOKABLE int Count();

	Q_INVOKABLE int Flags();

	Q_INVOKABLE QString Name();

	Q_INVOKABLE bool Mobile();

	Q_INVOKABLE bool Ignored();

	Q_INVOKABLE bool Frozen();

	Q_INVOKABLE bool Poisoned();

	Q_INVOKABLE bool Flying();

	Q_INVOKABLE bool YellowHits();

	Q_INVOKABLE bool IgnoreCharacters();

	Q_INVOKABLE bool Locked();

	Q_INVOKABLE bool WarMode();

	Q_INVOKABLE bool Hidden();

	Q_INVOKABLE bool IsHuman();

	Q_INVOKABLE bool IsPlayer();

	Q_INVOKABLE bool IsCorpse();

	Q_INVOKABLE int Layer();

	Q_INVOKABLE bool IsMulti();

	Q_INVOKABLE int EquipLayer();

	Q_INVOKABLE int Hits();

	Q_INVOKABLE int Hits(const QString &args);

	Q_INVOKABLE int MaxHits();

	Q_INVOKABLE int Mana();

	Q_INVOKABLE int Mana(const QString &args);

	Q_INVOKABLE int MaxMana();

	Q_INVOKABLE int Stam();

	Q_INVOKABLE int Stam(const QString &args);

	Q_INVOKABLE int MaxStam();

	Q_INVOKABLE bool Female();

	Q_INVOKABLE int Race();

	Q_INVOKABLE int Direction();

	Q_INVOKABLE int Notoriety();

	Q_INVOKABLE bool CanChangeName();

	Q_INVOKABLE bool Dead();

	Q_INVOKABLE bool Exists();

	Q_INVOKABLE QString Properties();

	Q_INVOKABLE bool ProfileReceived();

	Q_INVOKABLE QString Profile();

	Q_INVOKABLE QString Title();
};
//----------------------------------------------------------------------------------
#endif // OAGAMEOBJECTENGINE_H
//----------------------------------------------------------------------------------
