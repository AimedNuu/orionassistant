// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAGumpHookEngine.cpp
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oagumphookengine.h"
#include "../OrionAssistant/orionassistant.h"
//----------------------------------------------------------------------------------
COAScriptGumpHookObject::COAScriptGumpHookObject(const int &index)
: QObject(), m_Hook(nullptr)
{
	OAFUN_DEBUG("c35_f1");
	m_Hook.SetIndex(index);
}
//----------------------------------------------------------------------------------
int COAScriptGumpHookObject::Index()
{
	OAFUN_DEBUG("c35_f2");
	return m_Hook.GetIndex();
}
//----------------------------------------------------------------------------------
void COAScriptGumpHookObject::AddEntry(const int &index, const QString &text)
{
	OAFUN_DEBUG("c35_f3");
	m_Hook.m_Entries.push_back(CGumpHookEntry(index, text));
}
//----------------------------------------------------------------------------------
void COAScriptGumpHookObject::AddCheck(const int &index, const bool &state)
{
	OAFUN_DEBUG("c35_f4");
	m_Hook.m_Checks.push_back(CGumpHookCheck(index, state));
}
//----------------------------------------------------------------------------------
