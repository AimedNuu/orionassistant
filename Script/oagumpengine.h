/***********************************************************************************
**
** OAGumpEngine.h
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OAGUMPENGINE_H
#define OAGUMPENGINE_H
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
#include "../Managers/gumpmanager.h"
//----------------------------------------------------------------------------------
class COAScriptGumpObject : public QObject
{
	Q_OBJECT

private:
	CGump m_Gump;
	CGump *m_Parent{ nullptr };

public:
	COAScriptGumpObject(CGump *parent);
	virtual ~COAScriptGumpObject() {}

	Q_INVOKABLE int Serial();

	Q_INVOKABLE int ID();

	Q_INVOKABLE int X();

	Q_INVOKABLE int Y();

	Q_INVOKABLE bool Replayed();

	Q_INVOKABLE int ReplyID();

	Q_INVOKABLE QStringList ButtonList();

	Q_INVOKABLE QStringList CheckboxList();

	Q_INVOKABLE QStringList RadioList();

	Q_INVOKABLE QStringList TilepicList();

	Q_INVOKABLE QStringList GumppicList();

	Q_INVOKABLE QStringList EntriesList();

	Q_INVOKABLE QStringList CommandList();

	Q_INVOKABLE QStringList TextList();

	Q_INVOKABLE QString Command(const int &index);

	Q_INVOKABLE QString Text(const int &index);

	Q_INVOKABLE bool Select(QObject *hook);

	Q_INVOKABLE void Close();
};
//----------------------------------------------------------------------------------
#endif // OAGUMPENGINE_H
//----------------------------------------------------------------------------------
