// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAScriptHandle.cpp
**
** Copyright (C) January 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oascripthandle.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../OrionAssistant/orionassistant.h"
#include "../OrionAssistant/tabscripts.h"
#include <QJSEngine>
//----------------------------------------------------------------------------------
COAScriptHandle::COAScriptHandle(CRunningScriptListItem *parent, const QString &functionName, const QStringList &args, const QString &scriptBody)
: QObject(nullptr), m_Parent(parent), m_FunctionName(functionName), m_Args(args), m_Text(scriptBody)
{
	OAFUN_DEBUG("c40_f1");

	QThread *thread = new QThread();
	this->moveToThread(thread);

	connect(thread, SIGNAL(started()), this, SLOT(process()));
	connect(this, SIGNAL(finished()), thread, SLOT(quit()));
	connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
	connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

	thread->start();
}
//----------------------------------------------------------------------------------
COAScriptHandle::~COAScriptHandle()
{
	OAFUN_DEBUG("c40_f2");
}
//----------------------------------------------------------------------------------
void COAScriptHandle::TerminateScript()
{
	OAFUN_DEBUG("c40_f3");
	if (m_ScriptEngine != nullptr)
	{
		SetPaused(false);

		m_ScriptEngine->SetTerminated(true);

		if (m_ScriptEngineTextWindow != nullptr)
			m_ScriptEngineTextWindow->SetTerminated(true);

		delete m_ScriptEngine;
		m_ScriptEngine = nullptr;

		delete m_ScriptEnginePlayer;
		m_ScriptEnginePlayer = nullptr;

		if (m_Parent != nullptr)
		{
			emit g_TabScripts->signal_RemoveRunningScriptItem(m_Parent);
			m_Parent = nullptr;
		}
	}
}
//----------------------------------------------------------------------------------
bool COAScriptHandle::GetPaused()
{
	OAFUN_DEBUG("c40_f4");
	if (m_ScriptEngine != nullptr)
		return m_ScriptEngine->GetPaused();

	return false;
}
//----------------------------------------------------------------------------------
void COAScriptHandle::SetPaused(bool value)
{
	OAFUN_DEBUG("c40_f5");
	if (m_ScriptEngine != nullptr)
		m_ScriptEngine->SetPaused(value);

	if (m_ScriptEngineTextWindow != nullptr)
		m_ScriptEngineTextWindow->SetPaused(value);
}
//----------------------------------------------------------------------------------
void COAScriptHandle::process()
{
	OAFUN_DEBUG("c40_f6");
	qsrand(GetTickCount());

	try
	{
		RunEngine();
	}
	catch (std::exception &e)
	{
		LOG("Script engine catch error: %s\n", e.what());
	}

	if (m_Parent != nullptr)
	{
		emit g_TabScripts->signal_RemoveRunningScriptItem(m_Parent);
		m_Parent = nullptr;
	}

	emit finished();
}
//----------------------------------------------------------------------------------
void COAScriptHandle::RunEngine()
{
	OAFUN_DEBUG("c40_f7");
        QJSEngine jsEngine;

	m_ScriptEngine = new COAScriptEngine();
        QJSValue orion = jsEngine.newQObject(m_ScriptEngine);
	jsEngine.globalObject().setProperty("Orion", orion);

	m_ScriptEngineTextWindow = new COAScriptEngineTextWindow();
        QJSValue textWindow =jsEngine.newQObject(m_ScriptEngineTextWindow);
	jsEngine.globalObject().setProperty("TextWindow", textWindow);

	m_ScriptEnginePlayer = new COAPlayerObject();
        QJSValue player = jsEngine.newQObject(m_ScriptEnginePlayer);
	jsEngine.globalObject().setProperty("Player", player);

	COAClientSelectedTile *selectedClientTileObject = new COAClientSelectedTile();
        QJSValue selectedClientTile = jsEngine.newQObject(selectedClientTileObject);
	jsEngine.globalObject().setProperty("SelectedTile", selectedClientTile);

	jsEngine.globalObject().setProperty("self", "self");
	jsEngine.globalObject().setProperty("ground", "ground");
	jsEngine.globalObject().setProperty("lastcorpse", "lastcorpse");
	jsEngine.globalObject().setProperty("lasttarget", "lasttarget");
	jsEngine.globalObject().setProperty("lasttile", "lasttile");
	jsEngine.globalObject().setProperty("backpack", "backpack");
	jsEngine.globalObject().setProperty("lastcontainer", "lastcontainer");
	jsEngine.globalObject().setProperty("lastattack", "lastattack");
	jsEngine.globalObject().setProperty("laststatus", "laststatus");
	jsEngine.globalObject().setProperty("lastobject", "lastobject");
	jsEngine.globalObject().setProperty("ScriptName", m_FunctionName);

	//qDebug() << "Evaluating script" << m_FunctionName << "...";
        QJSValue result = jsEngine.evaluate(m_Text);

	if (result.isError())
	{
		//qDebug() << "Evaluate error! function and line:" << result.property("stack").toString() << "description:" << result.toString();
		emit g_OrionAssistant.signal_ScriptError("Evaluate script error!", "Line: " + result.property("lineNumber").toString() + "\n" + result.toString());
	}
	else
	{
		//qDebug() << "Collecting arguments...";
                QJSValueList arguments;

		if (!m_Args.empty())
		{
			for (const QString &param : m_Args)
				arguments << param;
		}

		//qDebug() << "args size:" << arguments.size();

		//qDebug() << "Calling script...";
                result = jsEngine.globalObject().property(m_FunctionName).call(arguments);
		//qDebug() << "End of calling script...";

		if (result.isError())
		{
			//qDebug() << "Running error! function and line:" << result.property("stack").toString() << "description:" << result.toString();

			if (m_ScriptEngine != nullptr)
				emit g_OrionAssistant.signal_ScriptError("Running script error!", "Line: " + result.property("lineNumber").toString() + "\n" + result.toString());
		}
	}

	jsEngine.collectGarbage();

}
//----------------------------------------------------------------------------------
