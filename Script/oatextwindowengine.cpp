// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OATextWindowEngine.cpp
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oatextwindowengine.h"
#include "../Managers/commandmanager.h"
//----------------------------------------------------------------------------------
COAScriptEngineTextWindow::COAScriptEngineTextWindow()
: COAScriptEngineBase()
{
}
//----------------------------------------------------------------------------------
void COAScriptEngineTextWindow::Open()
{
	OAFUN_DEBUG("c39_f1");
	emit g_CommandManager.CommandTextWindowOpen();

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngineTextWindow::Close()
{
	OAFUN_DEBUG("c39_f2");
	emit g_CommandManager.CommandTextWindowClose();

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngineTextWindow::Clear()
{
	OAFUN_DEBUG("c39_f3");
	emit g_CommandManager.CommandTextWindowClear();

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngineTextWindow::Print(const QString &text)
{
	OAFUN_DEBUG("c39_f4");
	emit g_CommandManager.CommandTextWindowPrint(text);

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngineTextWindow::SetPos(const int &x, const int &y)
{
	OAFUN_DEBUG("c39_f4");
	emit g_CommandManager.CommandTextWindowSetPos(x, y);

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngineTextWindow::SetSize(const int &width, const int &height)
{
	OAFUN_DEBUG("c39_f4");
	emit g_CommandManager.CommandTextWindowSetSize(width, height);

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngineTextWindow::SaveToFile(const QString &filePath)
{
	OAFUN_DEBUG("c39_f4");
	emit g_CommandManager.CommandTextWindowSaveToFile(filePath);

	PauseWaiting();
}
//----------------------------------------------------------------------------------
