// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAPlayerEngine.cpp
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oaplayerengine.h"
#include "../OrionAssistant/orionassistant.h"
#include "../Managers/commandmanager.h"
//----------------------------------------------------------------------------------
COAPlayerObject::COAPlayerObject()
: QObject()
{
	OAFUN_DEBUG("c37_f1");
}
//----------------------------------------------------------------------------------
QString COAPlayerObject::Serial()
{
	OAFUN_DEBUG("c37_f2");
	if (g_Player != nullptr)
		return COrionAssistant::SerialToText(g_PlayerSerial);

	return "0x00000000";
}
//----------------------------------------------------------------------------------
QString COAPlayerObject::Graphic()
{
	OAFUN_DEBUG("c37_f3");
	QString result = "0";
	emit g_CommandManager.CommandObjectGetGraphic(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAPlayerObject::Color()
{
	OAFUN_DEBUG("c37_f4");
	QString result = "0";
	emit g_CommandManager.CommandObjectGetColor(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::X()
{
	OAFUN_DEBUG("c37_f5");
	int result = 0;
	emit g_CommandManager.CommandObjectGetX(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Y()
{
	OAFUN_DEBUG("c37_f6");
	int result = 0;
	emit g_CommandManager.CommandObjectGetY(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Z()
{
	OAFUN_DEBUG("c37_f7");
	int result = 0;
	emit g_CommandManager.CommandObjectGetZ(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAPlayerObject::Container()
{
	OAFUN_DEBUG("c37_f8");
	QString result = "0";
	emit g_CommandManager.CommandObjectGetContainer(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Map()
{
	OAFUN_DEBUG("c37_f9");
	int result = 0;
	emit g_CommandManager.CommandObjectGetMap(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Count()
{
	OAFUN_DEBUG("c37_f10");
	int result = 0;
	emit g_CommandManager.CommandObjectGetCount(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Flags()
{
	OAFUN_DEBUG("c37_f11");
	int result = 0;
	emit g_CommandManager.CommandObjectGetFlags(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAPlayerObject::Name()
{
	OAFUN_DEBUG("c37_f12");
	QString result = "";
	emit g_CommandManager.CommandObjectGetName(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::Mobile()
{
	OAFUN_DEBUG("c37_f13");
	bool result = false;
	emit g_CommandManager.CommandObjectGetMobile(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::Ignored()
{
	OAFUN_DEBUG("c37_f14");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIgnored(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::Frozen()
{
	OAFUN_DEBUG("c37_f15");
	bool result = false;
	emit g_CommandManager.CommandObjectGetFrozen(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::Poisoned()
{
	OAFUN_DEBUG("c37_f16");
	bool result = false;
	emit g_CommandManager.CommandObjectGetPoisoned(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::Paralyzed()
{
	OAFUN_DEBUG("c37_f17");
	bool result = false;
	emit g_CommandManager.CommandPlayerGetParalyzed(&result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::Flying()
{
	OAFUN_DEBUG("c37_f18");
	bool result = false;
	emit g_CommandManager.CommandObjectGetFlying(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::YellowHits()
{
	OAFUN_DEBUG("c37_f19");
	bool result = false;
	emit g_CommandManager.CommandObjectGetYellowHits(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::IgnoreCharacters()
{
	OAFUN_DEBUG("c37_f20");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIgnoreCharacters(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::Locked()
{
	OAFUN_DEBUG("c37_f21");
	bool result = false;
	emit g_CommandManager.CommandObjectGetLocked(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::WarMode()
{
	OAFUN_DEBUG("c37_f22");
	bool result = false;
	emit g_CommandManager.CommandObjectGetWarMode(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::Hidden()
{
	OAFUN_DEBUG("c37_f23");
	bool result = false;
	emit g_CommandManager.CommandObjectGetHidden(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::IsHuman()
{
	OAFUN_DEBUG("c37_f24");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIsHuman(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::IsPlayer()
{
	OAFUN_DEBUG("c37_f25");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIsPlayer(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::IsCorpse()
{
	OAFUN_DEBUG("c37_f26");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIsCorpse(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Layer()
{
	OAFUN_DEBUG("c37_f27");
	int result = 0;
	emit g_CommandManager.CommandObjectGetLayer(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::IsMulti()
{
	OAFUN_DEBUG("c37_f28");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIsMulti(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::EquipLayer()
{
	OAFUN_DEBUG("c37_f29");
	int result = 0;
	emit g_CommandManager.CommandObjectGetEquipLayer(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Hits()
{
	OAFUN_DEBUG("c37_f30");
	return Hits("");
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Hits(const QString &args)
{
	OAFUN_DEBUG("c37_f31");
	int value = 0;
	emit g_CommandManager.CommandObjectGetHits(g_PlayerSerial, &value);

	if (args == "%")
	{
		int maxValue = 0;
		emit g_CommandManager.CommandObjectGetMaxHits(g_PlayerSerial, &maxValue);

		if (!maxValue)
			return 0;

		return (value * 100) / maxValue;
	}

	return value;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::MaxHits()
{
	OAFUN_DEBUG("c37_f32");
	int result = 0;
	emit g_CommandManager.CommandObjectGetMaxHits(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Mana()
{
	OAFUN_DEBUG("c37_f33");
	return Mana("");
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Mana(const QString &args)
{
	OAFUN_DEBUG("c37_f34");
	int value = 0;
	emit g_CommandManager.CommandObjectGetMana(g_PlayerSerial, &value);

	if (args == "%")
	{
		int maxValue = 0;
		emit g_CommandManager.CommandObjectGetMaxMana(g_PlayerSerial, &maxValue);

		if (!maxValue)
			return 0;

		return (value * 100) / maxValue;
	}

	return value;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::MaxMana()
{
	OAFUN_DEBUG("c37_f35");
	int result = 0;
	emit g_CommandManager.CommandObjectGetMaxMana(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Stam()
{
	OAFUN_DEBUG("c37_f36");
	return Stam("");
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Stam(const QString &args)
{
	OAFUN_DEBUG("c37_f37");
	int value = 0;
	emit g_CommandManager.CommandObjectGetStam(g_PlayerSerial, &value);

	if (args == "%")
	{
		int maxValue = 0;
		emit g_CommandManager.CommandObjectGetMaxStam(g_PlayerSerial, &maxValue);

		if (!maxValue)
			return 0;

		return (value * 100) / maxValue;
	}

	return value;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::MaxStam()
{
	OAFUN_DEBUG("c37_f38");
	int result = 0;
	emit g_CommandManager.CommandObjectGetMaxStam(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::Female()
{
	OAFUN_DEBUG("c37_f39");
	bool result = false;
	emit g_CommandManager.CommandObjectGetFemale(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Race()
{
	OAFUN_DEBUG("c37_f40");
	int result = 0;
	emit g_CommandManager.CommandObjectGetRace(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Direction()
{
	OAFUN_DEBUG("c37_f41");
	int result = 0;
	emit g_CommandManager.CommandObjectGetDirection(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Notoriety()
{
	OAFUN_DEBUG("c37_f42");
	int result = 0;
	emit g_CommandManager.CommandObjectGetNotoriety(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::CanChangeName()
{
	OAFUN_DEBUG("c37_f43");
	bool result = false;
	emit g_CommandManager.CommandObjectGetCanChangeName(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::Dead()
{
	OAFUN_DEBUG("c37_f44");
	bool result = false;
	emit g_CommandManager.CommandObjectGetDead(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Str()
{
	OAFUN_DEBUG("c37_f45");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetStr(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Int()
{
	OAFUN_DEBUG("c37_f46");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetInt(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Dex()
{
	OAFUN_DEBUG("c37_f47");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetDex(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::LockStrState()
{
	OAFUN_DEBUG("c37_f48");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetLockStrState(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::LockIntState()
{
	OAFUN_DEBUG("c37_f49");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetLockIntState(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::LockDexState()
{
	OAFUN_DEBUG("c37_f50");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetLockDexState(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Weight()
{
	OAFUN_DEBUG("c37_f51");
	return Weight("");
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Weight(const QString &args)
{
	OAFUN_DEBUG("c37_f52");
	int value = 0;
	emit g_CommandManager.CommandPlayerGetWeight(&value);

	if (args == "%")
	{
		int maxValue = 0;
		emit g_CommandManager.CommandPlayerGetMaxWeight(&maxValue);

		return (value * 100) / maxValue;
	}

	return value;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::MaxWeight()
{
	OAFUN_DEBUG("c37_f53");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetMaxWeight(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Armor()
{
	OAFUN_DEBUG("c37_f54");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetArmor(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Gold()
{
	OAFUN_DEBUG("c37_f55");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetGold(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::StatsCap()
{
	OAFUN_DEBUG("c37_f56");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetStatsCap(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Followers()
{
	OAFUN_DEBUG("c37_f57");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetFollowers(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::MaxFollowers()
{
	OAFUN_DEBUG("c37_f58");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetMaxFollowers(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::FireResistance()
{
	OAFUN_DEBUG("c37_f59");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetFireResistance(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::ColdResistance()
{
	OAFUN_DEBUG("c37_f60");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetColdResistance(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::PoisonResistance()
{
	OAFUN_DEBUG("c37_f61");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetPoisonResistance(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::EnergyResistance()
{
	OAFUN_DEBUG("c37_f62");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetEnergyResistance(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::Luck()
{
	OAFUN_DEBUG("c37_f63");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetLuck(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::MinDamage()
{
	OAFUN_DEBUG("c37_f64");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetMinDamage(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::MaxDamage()
{
	OAFUN_DEBUG("c37_f65");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetMaxDamage(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::TithingPoints()
{
	OAFUN_DEBUG("c37_f66");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetTithingPoints(&result);

	return result;
}
//----------------------------------------------------------------------------------
int COAPlayerObject::StealthSteps()
{
	OAFUN_DEBUG("c37_f67");
	int result = 0;
	emit g_CommandManager.CommandPlayerGetStealthSteps(&result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAPlayerObject::Properties()
{
	OAFUN_DEBUG("c37_f68");
	QString result = "";
	emit g_CommandManager.CommandObjectGetProperties(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAPlayerObject::ProfileReceived()
{
	OAFUN_DEBUG("c37_f69");
	bool result = false;
	emit g_CommandManager.CommandObjectGetProfileReceived(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAPlayerObject::Profile()
{
	OAFUN_DEBUG("c37_f70");
	QString result = "";
	emit g_CommandManager.CommandObjectGetProfile(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAPlayerObject::Title()
{
	OAFUN_DEBUG("c34_f45");
	QString result = "";
	emit g_CommandManager.CommandObjectGetTitle(g_PlayerSerial, &result);

	return result;
}
//----------------------------------------------------------------------------------
