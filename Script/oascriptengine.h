/***********************************************************************************
**
** OAScriptEngine.h
**
** Copyright (C) January 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OASCRIPTENGINE_H
#define OASCRIPTENGINE_H
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
//----------------------------------------------------------------------------------
class COAScriptEngine : public COAScriptEngineBase
{
	Q_OBJECT

public:
	COAScriptEngine();
	virtual ~COAScriptEngine() {}

	Q_INVOKABLE void Wait(QString text);

	Q_INVOKABLE void Info();

	Q_INVOKABLE void Info(const QString &serial);

	Q_INVOKABLE void InfoTile();

	Q_INVOKABLE void InfoTile(const QString &lasttile);

	Q_INVOKABLE void InfoMenu();

	Q_INVOKABLE void InfoMenu(const QString &index);

	Q_INVOKABLE void SaveConfig();

	Q_INVOKABLE void Click();

	Q_INVOKABLE void Click(const QString &serial);

	Q_INVOKABLE void UseObject();

	Q_INVOKABLE void UseObject(const QString &serial);

	Q_INVOKABLE void GetStatus();

	Q_INVOKABLE void GetStatus(const QString &serial);

	Q_INVOKABLE void Attack(const QString &serial);

	Q_INVOKABLE void SetLight(const bool &on);

	Q_INVOKABLE void SetLight(const bool &on, const int &value);

	Q_INVOKABLE void SetWeather(const bool &on);

	Q_INVOKABLE void SetWeather(const bool &on, const int &index);

	Q_INVOKABLE void SetWeather(const bool &on, const int &index, const int &count);

	Q_INVOKABLE void SetWeather(const bool &on, const int &index, const int &count, const int &temperature);

	Q_INVOKABLE void SetSeason(const bool &on);

	Q_INVOKABLE void SetSeason(const bool &on, const int &index);

	Q_INVOKABLE void SetSeason(const bool &on, const int &index, const int &music);

	Q_INVOKABLE void SetTrack(const bool &on);

	Q_INVOKABLE void SetTrack(const bool &on, const int &x, const int &y);

	Q_INVOKABLE bool SaveHotkeys(const QString &fileName);

	Q_INVOKABLE bool LoadHotkeys(const QString &fileName);

	Q_INVOKABLE void Cast(const QString &name);

	Q_INVOKABLE void Cast(const int &index);

	Q_INVOKABLE void Cast(const QString &name, const QString &target);

	Q_INVOKABLE void Cast(const int &index, const QString &target);

	Q_INVOKABLE void UseSkill(const QString &name);

	Q_INVOKABLE void UseSkill(const int &index);

	Q_INVOKABLE void UseSkill(const QString &name, const QString &target);

	Q_INVOKABLE void UseSkill(const int &index, const QString &target);

	Q_INVOKABLE int SkillValue(const QString &name);

	Q_INVOKABLE int SkillValue(const int &index);

	Q_INVOKABLE int SkillValue(const QString &name, const QString &type);

	Q_INVOKABLE int SkillValue(const int &index, const QString &type);

	Q_INVOKABLE void HelpGump();

	Q_INVOKABLE void CloseUO();

	Q_INVOKABLE void WarMode();

	Q_INVOKABLE void WarMode(const uint &state);

	Q_INVOKABLE void Morph();

	Q_INVOKABLE void Morph(const QString &graphic);

	Q_INVOKABLE void Resend();

	Q_INVOKABLE void Sound(const uint &sound);

	Q_INVOKABLE void EmoteAction(const QString &name);

	Q_INVOKABLE void Hide();

	Q_INVOKABLE void Hide(const QString &serial);

	Q_INVOKABLE bool UseType(const QStringList &graphic);

	Q_INVOKABLE bool UseType(const QStringList &graphic, const QStringList &color);

	Q_INVOKABLE bool UseType(const QStringList &graphic, const QStringList &color, const QString &container);

	Q_INVOKABLE bool UseType(const QStringList &graphic, const QStringList &color, const QString &container, const bool &recurse);

	Q_INVOKABLE bool UseType(const QString &graphic);

	Q_INVOKABLE bool UseType(const QString &graphic, const QString &color);

	Q_INVOKABLE bool UseType(const QString &graphic, const QString &color, const QString &container);

	Q_INVOKABLE bool UseType(const QString &graphic, const QString &color, const QString &container, const bool &recurse);

	Q_INVOKABLE bool UseFromGround(const QStringList &graphic);

	Q_INVOKABLE bool UseFromGround(const QStringList &graphic, const QStringList &color);

	Q_INVOKABLE bool UseFromGround(const QStringList &graphic, const QStringList &color, const QString &distance);

	Q_INVOKABLE bool UseFromGround(const QStringList &graphic, const QStringList &color, const QString &distance, const QString &flags);

	Q_INVOKABLE bool UseFromGround(const QString &graphic);

	Q_INVOKABLE bool UseFromGround(const QString &graphic, const QString &color);

	Q_INVOKABLE bool UseFromGround(const QString &graphic, const QString &color, const QString &distance);

	Q_INVOKABLE bool UseFromGround(const QString &graphic, const QString &color, const QString &distance, const QString &flags);

	Q_INVOKABLE bool UseTypeList(const QString &findListName);

	Q_INVOKABLE bool UseTypeList(const QString &findListName, const QString &container);

	Q_INVOKABLE bool UseTypeList(const QString &findListName, const QString &container, const bool &recurse);

	Q_INVOKABLE bool UseFromGroundList(const QString &findListName);

	Q_INVOKABLE bool UseFromGroundList(const QString &findListName, const QString &distance);

	Q_INVOKABLE bool UseFromGroundList(const QString &findListName, const QString &distance, const QString &flags);

	Q_INVOKABLE void Print(const QString &text);

	Q_INVOKABLE void Print(const QString &color, const QString &text);

	Q_INVOKABLE void CharPrint(const QString &serial, const QString &color, const QString &text);

	Q_INVOKABLE void Say(const QString &text);

	Q_INVOKABLE void RenameMount(const QString &serial, const QString &name);

	Q_INVOKABLE QStringList FindType(const QStringList &graphic);

	Q_INVOKABLE QStringList FindType(const QStringList &graphic, const QStringList &color);

	Q_INVOKABLE QStringList FindType(const QStringList &graphic, const QStringList &color, const QString &container);

	Q_INVOKABLE QStringList FindType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags);

	Q_INVOKABLE QStringList FindType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags, const QString &distance);

	Q_INVOKABLE QStringList FindType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags, const QString &distance, const QString &notoriety);

	Q_INVOKABLE QStringList FindType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags, const QString &distance, const QString &notoriety, const bool &recurse);

	Q_INVOKABLE QStringList FindType(const QString &graphic);

	Q_INVOKABLE QStringList FindType(const QString &graphic, const QString &color);

	Q_INVOKABLE QStringList FindType(const QString &graphic, const QString &color, const QString &container);

	Q_INVOKABLE QStringList FindType(const QString &graphic, const QString &color, const QString &container, const QString &flags);

	Q_INVOKABLE QStringList FindType(const QString &graphic, const QString &color, const QString &container, const QString &flags, const QString &distance);

	Q_INVOKABLE QStringList FindType(const QString &graphic, const QString &color, const QString &container, const QString &flags, const QString &distance, const QString &notoriety);

	Q_INVOKABLE QStringList FindType(const QString &graphic, const QString &color, const QString &container, const QString &flags, const QString &distance, const QString &notoriety, const bool &recurse);

	Q_INVOKABLE void Ignore(const QString &serial);

	Q_INVOKABLE void Ignore(const QString &serial, const bool &on);

	Q_INVOKABLE void IgnoreReset();

	Q_INVOKABLE void Drop();

	Q_INVOKABLE void Drop(const QString &serial);

	Q_INVOKABLE void Drop(const QString &serial, const int &count);

	Q_INVOKABLE void Drop(const QString &serial, const int &count, const int &x, const int &y, const int &z);

	Q_INVOKABLE void DropHere();

	Q_INVOKABLE void DropHere(const QString &serial);

	Q_INVOKABLE void DropHere(const QString &serial, const int &count);

	Q_INVOKABLE void MoveItem();

	Q_INVOKABLE void MoveItem(const QString &serial);

	Q_INVOKABLE void MoveItem(const QString &serial, const int &count);

	Q_INVOKABLE void MoveItem(const QString &serial, const int &count, const QString &container);

	Q_INVOKABLE void MoveItem(const QString &serial, const int &count, const QString &container, const int &x, const int &y);

	Q_INVOKABLE void MoveItem(const QString &serial, const int &count, const QString &container, const int &x, const int &y, const int &z);

	Q_INVOKABLE void ShowJournal();

	Q_INVOKABLE void ShowJournal(const int &lines);

	Q_INVOKABLE void ClearJournal();

	Q_INVOKABLE void ClearJournal(const QString &pattern);

	Q_INVOKABLE void ClearJournal(const QString &pattern, const QString &flags);

	Q_INVOKABLE void ClearJournal(const QString &pattern, const QString &flags, const QString &serial);

	Q_INVOKABLE void ClearJournal(const QString &pattern, const QString &flags, const QString &serial, const QString &color);

	Q_INVOKABLE void JournalIgnoreCase(const bool &on);

	Q_INVOKABLE QObject *InJournal(const QString &pattern);

	Q_INVOKABLE QObject *InJournal(const QString &pattern, const QString &flags);

	Q_INVOKABLE QObject *InJournal(const QString &pattern, const QString &flags, const QString &serial);

	Q_INVOKABLE QObject *InJournal(const QString &pattern, const QString &flags, const QString &serial, const QString &color);

	Q_INVOKABLE QObject *InJournal(const QString &pattern, const QString &flags, const QString &serial, const QString &color, const uint &startTime, const uint &endTime);

	Q_INVOKABLE QObject *WaitJournal(const QString &pattern, const uint &startTime, const uint &endTime);

	Q_INVOKABLE QObject *WaitJournal(const QString &pattern, const uint &startTime, const uint &endTime, const QString &flags);

	Q_INVOKABLE QObject *WaitJournal(const QString &pattern, const uint &startTime, const uint &endTime, const QString &flags, const QString &serial);

	Q_INVOKABLE QObject *WaitJournal(const QString &pattern, const uint &startTime, const uint &endTime, const QString &flags, const QString &serial, const QString &colorText);

	Q_INVOKABLE void SetDressBag(const QString &serial);

	Q_INVOKABLE void UnsetDressBag();

	Q_INVOKABLE void SetArm(const QString &name);

	Q_INVOKABLE void UnsetArm(const QString &name);

	Q_INVOKABLE void SetDress(const QString &name);

	Q_INVOKABLE void UnsetDress(const QString &name);

	Q_INVOKABLE void Arm(const QString &name);

	Q_INVOKABLE void Disarm();

	Q_INVOKABLE void Dress(const QString &name);

	Q_INVOKABLE void Undress();

	Q_INVOKABLE void Unequip(const QString &layerName);

	Q_INVOKABLE void Equip(const QString &serial);

	Q_INVOKABLE void EquipT(const QString &graphic);

	Q_INVOKABLE void EquipT(const QString &graphic, const QString &color);

	Q_INVOKABLE bool HaveTarget();

	Q_INVOKABLE void WaitTargetObject(const QStringList &objects);

	Q_INVOKABLE void WaitTargetObject(const QString &objects);

	Q_INVOKABLE void WaitTargetType(const QStringList &graphic);

	Q_INVOKABLE void WaitTargetType(const QStringList &graphic, const QStringList &color);

	Q_INVOKABLE void WaitTargetType(const QStringList &graphic, const QStringList &color, const QString &container);

	Q_INVOKABLE void WaitTargetType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags);

	Q_INVOKABLE void WaitTargetType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags, const bool &recurse);

	Q_INVOKABLE void WaitTargetType(const QString &graphic);

	Q_INVOKABLE void WaitTargetType(const QString &graphic, const QString &color);

	Q_INVOKABLE void WaitTargetType(const QString &graphic, const QString &color, const QString &container);

	Q_INVOKABLE void WaitTargetType(const QString &graphic, const QString &color, const QString &container, const QString &flags);

	Q_INVOKABLE void WaitTargetType(const QString &graphic, const QString &color, const QString &container, const QString &flags, const bool &recurse);

	Q_INVOKABLE void WaitTargetGround(const QStringList &graphic);

	Q_INVOKABLE void WaitTargetGround(const QStringList &graphic, const QStringList &color);

	Q_INVOKABLE void WaitTargetGround(const QStringList &graphic, const QStringList &color, const QString &distance);

	Q_INVOKABLE void WaitTargetGround(const QStringList &graphic, const QStringList &color, const QString &distance, const QString &flags);

	Q_INVOKABLE void WaitTargetGround(const QString &graphic);

	Q_INVOKABLE void WaitTargetGround(const QString &graphic, const QString &color);

	Q_INVOKABLE void WaitTargetGround(const QString &graphic, const QString &color, const QString &distance);

	Q_INVOKABLE void WaitTargetGround(const QString &graphic, const QString &color, const QString &distance, const QString &flags);

	Q_INVOKABLE bool WaitTargetTypeList(const QString &findListName);

	Q_INVOKABLE bool WaitTargetTypeList(const QString &findListName, const QString &container);

	Q_INVOKABLE bool WaitTargetTypeList(const QString &findListName, const QString &container, const QString &flags);

	Q_INVOKABLE bool WaitTargetTypeList(const QString &findListName, const QString &container, const QString &flags, const bool &recurse);

	Q_INVOKABLE bool WaitTargetGroundList(const QString &findListName);

	Q_INVOKABLE bool WaitTargetGroundList(const QString &findListName, const QString &distance);

	Q_INVOKABLE bool WaitTargetGroundList(const QString &findListName, const QString &distance, const QString &flags);

	Q_INVOKABLE void WaitTargetTile(const QString &tile);

	Q_INVOKABLE void WaitTargetTile(const QString &tile, const int &x, const int &y, const int &z);

	Q_INVOKABLE void WaitTargetTileRelative(const QString &tile, const int &x, const int &y, const int &z);

	Q_INVOKABLE void CancelWaitTarget();

	Q_INVOKABLE void WaitMenu(const QString &prompt, const QString &choice);

	Q_INVOKABLE void CancelWaitMenu();

	Q_INVOKABLE int Now();

	Q_INVOKABLE QObject *FindObject(const QString &serial);

	Q_INVOKABLE int GetDistance(const QString &serial);

	Q_INVOKABLE int GetDistance(const int &x, const int &y);

	Q_INVOKABLE int Count(const QStringList &graphic);

	Q_INVOKABLE int Count(const QStringList &graphic, const QStringList &color);

	Q_INVOKABLE int Count(const QStringList &graphic, const QStringList &color, const QString &container);

	Q_INVOKABLE int Count(const QStringList &graphic, const QStringList &color, const QString &container, const QString &distance);

	Q_INVOKABLE int Count(const QStringList &graphic, const QStringList &color, const QString &container,const QString &distance, const bool &recurse);

	Q_INVOKABLE int Count(const QString &graphic);

	Q_INVOKABLE int Count(const QString &graphic, const QString &color);

	Q_INVOKABLE int Count(const QString &graphic, const QString &color, const QString &container);

	Q_INVOKABLE int Count(const QString &graphic, const QString &color, const QString &container, const QString &distance);

	Q_INVOKABLE int Count(const QString &graphic, const QString &color, const QString &container,const QString &distance, const bool &recurse);

	Q_INVOKABLE void ResetIgnoreList();

	Q_INVOKABLE void UseIgnoreList(const QString &listName);

	Q_INVOKABLE QStringList FindList(const QString &findListName);

	Q_INVOKABLE QStringList FindList(const QString &findListName, const QString &container);

	Q_INVOKABLE QStringList FindList(const QString &findListName, const QString &container, const QString &flags);

	Q_INVOKABLE QStringList FindList(const QString &findListName, const QString &container, const QString &flags, const QString &distance);

	Q_INVOKABLE QStringList FindList(const QString &findListName, const QString &container, const QString &flags, const QString &distance, const QString &notoriety);

	Q_INVOKABLE QStringList FindList(const QString &findListName, const QString &container, const QString &flags, const QString &distance, const QString &notoriety, const bool &recurse);

	Q_INVOKABLE void TargetObject(const QStringList &objects);

	Q_INVOKABLE void TargetObject(const QString &objects);

	Q_INVOKABLE void TargetType(const QStringList &graphic);

	Q_INVOKABLE void TargetType(const QStringList &graphic, const QStringList &color);

	Q_INVOKABLE void TargetType(const QStringList &graphic, const QStringList &color, const QString &container);

	Q_INVOKABLE void TargetType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags);

	Q_INVOKABLE void TargetType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags, const bool &recurse);

	Q_INVOKABLE void TargetType(const QString &graphic);

	Q_INVOKABLE void TargetType(const QString &graphic, const QString &color);

	Q_INVOKABLE void TargetType(const QString &graphic, const QString &color, const QString &container);

	Q_INVOKABLE void TargetType(const QString &graphic, const QString &color, const QString &container, const QString &flags);

	Q_INVOKABLE void TargetType(const QString &graphic, const QString &color, const QString &container, const QString &flags, const bool &recurse);

	Q_INVOKABLE void TargetGround(const QStringList &graphic);

	Q_INVOKABLE void TargetGround(const QStringList &graphic, const QStringList &color);

	Q_INVOKABLE void TargetGround(const QStringList &graphic, const QStringList &color, const QString &distance);

	Q_INVOKABLE void TargetGround(const QStringList &graphic, const QStringList &color, const QString &distance, const QString &flags);

	Q_INVOKABLE void TargetGround(const QString &graphic);

	Q_INVOKABLE void TargetGround(const QString &graphic, const QString &color);

	Q_INVOKABLE void TargetGround(const QString &graphic, const QString &color, const QString &distance);

	Q_INVOKABLE void TargetGround(const QString &graphic, const QString &color, const QString &distance, const QString &flags);

	Q_INVOKABLE bool TargetTypeList(const QString &findListName);

	Q_INVOKABLE bool TargetTypeList(const QString &findListName, const QString &container);

	Q_INVOKABLE bool TargetTypeList(const QString &findListName, const QString &container, const QString &flags);

	Q_INVOKABLE bool TargetTypeList(const QString &findListName, const QString &container, const QString &flags, const bool &recurse);

	Q_INVOKABLE bool TargetGroundList(const QString &findListName);

	Q_INVOKABLE bool TargetGroundList(const QString &findListName, const QString &distance);

	Q_INVOKABLE bool TargetGroundList(const QString &findListName, const QString &distance, const QString &flags);

	Q_INVOKABLE void TargetTile(const QString &tile);

	Q_INVOKABLE void TargetTile(const QString &tile, const int &x, const int &y, const int &z);

	Q_INVOKABLE void TargetTileRelative(const QString &tile, const int &x, const int &y, const int &z);

	Q_INVOKABLE void BlockMoving(const bool &on);

	Q_INVOKABLE bool CanWalk(const int &direction, const int &x, const int &y, const int &z);

	Q_INVOKABLE bool Step(const int &direction);

	Q_INVOKABLE bool Step(const int &direction, const bool &run);

	Q_INVOKABLE bool WalkTo(const int &x, const int &y, const int &z);

	Q_INVOKABLE bool WalkTo(const int &x, const int &y, const int &z, const int &distance);

	Q_INVOKABLE void StopWalking();

	Q_INVOKABLE bool IsWalking();

	Q_INVOKABLE QObject *NewFile();

	Q_INVOKABLE void AddType(const QString &name);

	Q_INVOKABLE void AddType(const QString &name, const QString &type);

	Q_INVOKABLE void RemoveType(const QString &name);

	Q_INVOKABLE void AddObject(const QString &name);

	Q_INVOKABLE void AddObject(const QString &name, const QString &object);

	Q_INVOKABLE void RemoveObject(const QString &name);

	Q_INVOKABLE QObject *ObjAtLayer(const QString &layer);

	Q_INVOKABLE QObject *ObjAtLayer(const QString &layer, const QString &serial);

	Q_INVOKABLE void LoadScript(const QString &filePath);

	Q_INVOKABLE void Exec(const QString &name);

	Q_INVOKABLE void Exec(const QString &name, const bool &once);

	Q_INVOKABLE void Exec(const QString &name, const bool &once, const QStringList &args);

	Q_INVOKABLE void Exec(const QString &name, const bool &once, const QString &args);

	Q_INVOKABLE void Terminate(const QString &functionName);

	Q_INVOKABLE void Terminate(const QString &functionName, const QString &functionsSave);

	Q_INVOKABLE bool ScriptRunning(const QString &name);

	Q_INVOKABLE void BandageSelf();

	Q_INVOKABLE bool OptionSound();

	Q_INVOKABLE void OptionSound(const bool &state);

	Q_INVOKABLE int OptionSoundVolume();

	Q_INVOKABLE void OptionSoundVolume(const int &value);

	Q_INVOKABLE bool OptionMusic();

	Q_INVOKABLE void OptionMusic(const bool &state);

	Q_INVOKABLE int OptionMusicVolume();

	Q_INVOKABLE void OptionMusicVolume(const int &value);

	Q_INVOKABLE bool OptionUseTooltips();

	Q_INVOKABLE void OptionUseTooltips(const bool &state);

	Q_INVOKABLE bool OptionAlwaysRun();

	Q_INVOKABLE void OptionAlwaysRun(const bool &state);

	Q_INVOKABLE bool OptionNewTargetSystem();

	Q_INVOKABLE void OptionNewTargetSystem(const bool &state);

	Q_INVOKABLE bool OptionObjectHandles();

	Q_INVOKABLE void OptionObjectHandles(const bool &state);

	Q_INVOKABLE bool OptionScaleSpeech();

	Q_INVOKABLE void OptionScaleSpeech(const bool &state);

	Q_INVOKABLE int OptionScaleSpeechDelay();

	Q_INVOKABLE void OptionScaleSpeechDelay(const int &value);

	Q_INVOKABLE bool OptionIgnoreGuildMessages();

	Q_INVOKABLE void OptionIgnoreGuildMessages(const bool &state);

	Q_INVOKABLE bool OptionIgnoreAllianceMessages();

	Q_INVOKABLE void OptionIgnoreAllianceMessages(const bool &state);

	Q_INVOKABLE bool OptionDarkNights();

	Q_INVOKABLE void OptionDarkNights(const bool &state);

	Q_INVOKABLE bool OptionColoredLighting();

	Q_INVOKABLE void OptionColoredLighting(const bool &state);

	Q_INVOKABLE bool OptionCriminalActionsQuery();

	Q_INVOKABLE void OptionCriminalActionsQuery(const bool &state);

	Q_INVOKABLE bool OptionCircleOfTransparency();

	Q_INVOKABLE void OptionCircleOfTransparency(const bool &state);

	Q_INVOKABLE int OptionCircleOfTransparencyValue();

	Q_INVOKABLE void OptionCircleOfTransparencyValue(const int &value);

	Q_INVOKABLE bool OptionLockResizingGameWindow();

	Q_INVOKABLE void OptionLockResizingGameWindow(const bool &state);

	Q_INVOKABLE int OptionFPSValue();

	Q_INVOKABLE void OptionFPSValue(const int &value);

	Q_INVOKABLE bool OptionUseScalingGameWindow();

	Q_INVOKABLE void OptionUseScalingGameWindow(const bool &state);

	Q_INVOKABLE int OptionDrawStatusState();

	Q_INVOKABLE void OptionDrawStatusState(const int &state);

	Q_INVOKABLE bool OptionDrawStumps();

	Q_INVOKABLE void OptionDrawStumps(const bool &state);

	Q_INVOKABLE bool OptionMarkingCaves();

	Q_INVOKABLE void OptionMarkingCaves(const bool &state);

	Q_INVOKABLE bool OptionNoVegetation();

	Q_INVOKABLE void OptionNoVegetation(const bool &state);

	Q_INVOKABLE bool OptionNoFieldsAnimation();

	Q_INVOKABLE void OptionNoFieldsAnimation(const bool &state);

	Q_INVOKABLE bool OptionStandardCharactesFrameRate();

	Q_INVOKABLE void OptionStandardCharactesFrameRate(const bool &state);

	Q_INVOKABLE bool OptionStandardItemsFrameRate();

	Q_INVOKABLE void OptionStandardItemsFrameRate(const bool &state);

	Q_INVOKABLE bool OptionLockGumpsMoving();

	Q_INVOKABLE void OptionLockGumpsMoving(const bool &state);

	Q_INVOKABLE bool OptionEnterChat();

	Q_INVOKABLE void OptionEnterChat(const bool &state);

	Q_INVOKABLE int OptionHiddenCharacters();

	Q_INVOKABLE void OptionHiddenCharacters(const int &state);

	Q_INVOKABLE int OptionHiddenCharactersAlpha();

	Q_INVOKABLE void OptionHiddenCharactersAlpha(const int &value);

	Q_INVOKABLE bool OptionHiddenCharactersModeOnlyForSelf();

	Q_INVOKABLE void OptionHiddenCharactersModeOnlyForSelf(const bool &state);

	Q_INVOKABLE bool OptionTransparentSpellIcons();

	Q_INVOKABLE void OptionTransparentSpellIcons(const bool &state);

	Q_INVOKABLE int OptionSpellIconsAlpha();

	Q_INVOKABLE void OptionSpellIconsAlpha(const int &value);

	Q_INVOKABLE bool OptionFastRotation();

	Q_INVOKABLE void OptionFastRotation(const bool &state);

	Q_INVOKABLE QString ClientLastTarget();

	Q_INVOKABLE void ClientLastTarget(const QString &value);

	Q_INVOKABLE QString ClientLastAttack();

	Q_INVOKABLE void ClientLastAttack(const QString &value);

	Q_INVOKABLE QString TargetSystemSerial();

	Q_INVOKABLE void TargetSystemSerial(const QString &value);

	Q_INVOKABLE QString GetSerial(const QString &value);

	Q_INVOKABLE void AddFindList();

	Q_INVOKABLE void AddFindList(const QString &listName, const QString &graphic, const QString &color);

	Q_INVOKABLE void AddFindList(const QString &listName, const QString &graphic, const QString &color, const QString &comment);

	Q_INVOKABLE void ClearFindList(const QString &listName);

	Q_INVOKABLE void AddIgnoreList();

	Q_INVOKABLE void AddIgnoreListObject(const QString &listName, const QString &serial);

	Q_INVOKABLE void AddIgnoreListObject(const QString &listName, const QString &serial, const QString &comment);

	Q_INVOKABLE void AddIgnoreList(const QString &listName, const QString &graphic, const QString &color);

	Q_INVOKABLE void AddIgnoreList(const QString &listName, const QString &graphic, const QString &color, const QString &comment);

	Q_INVOKABLE void ClearIgnoreList(const QString &listName);

	Q_INVOKABLE int MenuCount();

	Q_INVOKABLE QObject *GetMenu(const QString &name);

	Q_INVOKABLE bool SelectMenu(const QString &name, const QString &itemName);

	Q_INVOKABLE void CloseMenu(const QString &name);

	Q_INVOKABLE bool BuffExists(const QString &name);

	Q_INVOKABLE int TradeCount();

	Q_INVOKABLE QString TradeContainer(const QString &index, const QString &container);

	Q_INVOKABLE QString TradeOpponent(const QString &index);

	Q_INVOKABLE QString TradeName(const QString &index);

	Q_INVOKABLE bool TradeCheckState(const QString &index, const QString &container);

	Q_INVOKABLE void TradeCheck(const QString &index, const bool &state);

	Q_INVOKABLE void TradeClose(const QString &index);

	Q_INVOKABLE QString GetGraphic(const QString &value);

	Q_INVOKABLE QString GetContainer(const QString &value);

	Q_INVOKABLE QString FindFriend();

	Q_INVOKABLE QString FindFriend(const QString &flags);

	Q_INVOKABLE QString FindFriend(const QString &flags, const QString &distance);

	Q_INVOKABLE QString FindEnemy();

	Q_INVOKABLE QString FindEnemy(const QString &flags);

	Q_INVOKABLE QString FindEnemy(const QString &flags, const QString &distance);

	Q_INVOKABLE QStringList GetFriendList();

	Q_INVOKABLE void GetFriendsStatus();

	Q_INVOKABLE QStringList GetEnemyList();

	Q_INVOKABLE void GetEnemiesStatus();

	Q_INVOKABLE void SetFontColor(const bool &state);

	Q_INVOKABLE void SetFontColor(const bool &state, const QString &color);

	Q_INVOKABLE bool GetFontColor();

	Q_INVOKABLE QString GetFontColorValue();

	Q_INVOKABLE void SetCharactersFontColor(const bool &state);

	Q_INVOKABLE void SetCharactersFontColor(const bool &state, const QString &color);

	Q_INVOKABLE bool GetCharactersFontColor();

	Q_INVOKABLE QString GetCharactersFontColorValue();

	Q_INVOKABLE void AddFriend(const QString &name);

	Q_INVOKABLE void AddFriend(const QString &name, const QString &object);

	Q_INVOKABLE void RemoveFriend(const QString &name);

	Q_INVOKABLE void ClearFriendList();

	Q_INVOKABLE void AddEnemy(const QString &name);

	Q_INVOKABLE void AddEnemy(const QString &name, const QString &object);

	Q_INVOKABLE void RemoveEnemy(const QString &name);

	Q_INVOKABLE void ClearEnemyList();

	Q_INVOKABLE void SetGlobal(const QString &name, const QString &value);

	Q_INVOKABLE QString GetGlobal(const QString &name);

	Q_INVOKABLE void ClearGlobals();

	Q_INVOKABLE void InfoGump();

	Q_INVOKABLE void InfoGump(const QString &index);

	Q_INVOKABLE bool ValidateTargetTile(const QString &tile, const int &x, const int &y);

	Q_INVOKABLE bool ValidateTargetTileRelative(const QString &tile, const int &x, const int &y);

	Q_INVOKABLE void Launch(const QString &filePath);

	Q_INVOKABLE void Launch(const QString &filePath, const QStringList &args);

	Q_INVOKABLE void Launch(const QString &filePath, const QString &args);

	Q_INVOKABLE void UseAbility(const int &index);

	Q_INVOKABLE void UseAbility(const QString &name);

	Q_INVOKABLE bool Contains(const QString &text, const QString &pattern);

	Q_INVOKABLE bool Contains(QString text, QString pattern, const bool &ignoreCase);

	Q_INVOKABLE QStringList Split(const QString &text);

	Q_INVOKABLE QStringList Split(const QString &text, const QString &separator);

	Q_INVOKABLE QStringList Split(const QString &text, const QString &separator, const bool &skipEmptyWorld);

	Q_INVOKABLE QObject *LastJournalMessage();

	Q_INVOKABLE QObject *GetLastTargetPosition();

	Q_INVOKABLE QObject *GetLastAttackPosition();

	Q_INVOKABLE void UseWrestlingDisarm();

	Q_INVOKABLE void UseWrestlingStun();

	Q_INVOKABLE bool OpenContainer(const QString &serial);

	Q_INVOKABLE bool OpenContainer(const QString &serial, const QString &delayText);

	Q_INVOKABLE bool OpenContainer(QString serial, const QString &delayText, const QString &errorPattern);

	Q_INVOKABLE bool ObjectExists(const QString &serial);

	Q_INVOKABLE QString OAVersion();

	Q_INVOKABLE bool Connected();

	Q_INVOKABLE QString Time();

	Q_INVOKABLE QString Time(const QString &format);

	Q_INVOKABLE QString Date();

	Q_INVOKABLE QString Date(const QString &format);

	Q_INVOKABLE int Random();

	Q_INVOKABLE int Random(const int &value);

	Q_INVOKABLE int Random(const int &minValue, const int &maxValue);

	Q_INVOKABLE QString RequestName(const QString &serial);

	Q_INVOKABLE QString RequestName(QString serial, const QString &delayText);

	Q_INVOKABLE void InvokeVirture(const QString &name);

	Q_INVOKABLE int JournalCount();

	Q_INVOKABLE QObject *JournalLine(const int &index);

	Q_INVOKABLE QObject *CreateGumpHook(QString name);

	Q_INVOKABLE QObject *CreateGumpHook(const int &index);

	Q_INVOKABLE void WaitGump(QObject *hook);

	Q_INVOKABLE void CancelWaitGump();

	Q_INVOKABLE int GumpCount();

	Q_INVOKABLE QObject *GetLastGump();

	Q_INVOKABLE QObject *GetGump(const int &index);

	Q_INVOKABLE QObject *GetGump(const int &serial, const int &id);

	Q_INVOKABLE void PlayWav(const QString &fileName);

	Q_INVOKABLE bool GetProfile(const QString &serial);

	Q_INVOKABLE bool GetProfile(const QString &serial, const QString &delayText);

	Q_INVOKABLE bool GetProfile(QString serial, const QString &delayText, const QString &errorPattern);

	Q_INVOKABLE void Screenshot();

	Q_INVOKABLE void WaitPrompt(const QString &text);

	Q_INVOKABLE void WaitPrompt(const QString &text, const QString &serial);

	Q_INVOKABLE void WaitPrompt(const QString &text, const QString &serial, const QString &type);

	Q_INVOKABLE void CancelWaitPrompt();

	Q_INVOKABLE void Buy(const QString &shopListName);

	Q_INVOKABLE void Buy(const QString &shopListName, const QString &vendorName);

	Q_INVOKABLE void Buy(const QString &shopListName, const QString &vendorName, const int &shopDelay);

	Q_INVOKABLE void Sell(const QString &shopListName);

	Q_INVOKABLE void Sell(const QString &shopListName, const QString &vendorName);

	Q_INVOKABLE void Sell(const QString &shopListName, const QString &vendorName, const int &shopDelay);

	Q_INVOKABLE void ShowStatusbar(const QString &serial, const int &x, const int &y);

	Q_INVOKABLE void ShowStatusbar(const QString &serial, const int &x, const int &y, const bool &minimized);

	Q_INVOKABLE void CloseStatusbar(const QString &serial);

	Q_INVOKABLE void LogOut();

	Q_INVOKABLE QObject *CreateClientMacro();

	Q_INVOKABLE QObject *CreateClientMacro(const QString &action);

	Q_INVOKABLE QObject *CreateClientMacro(const QString &action, const QString &subAction);

	Q_INVOKABLE bool TimerExists(const QString &name);

	Q_INVOKABLE void SetTimer(const QString &name);

	Q_INVOKABLE void SetTimer(const QString &name, const int &value);

	Q_INVOKABLE int Timer(const QString &name);

	Q_INVOKABLE void RemoveTimer(const QString &name);

	Q_INVOKABLE void ClearTimers();

	Q_INVOKABLE void CancelTarget();

	Q_INVOKABLE bool PromptExists();

	Q_INVOKABLE QString PromptSerial();

	Q_INVOKABLE QString PromptID();

	Q_INVOKABLE void SendPrompt(const QString &text);

	Q_INVOKABLE void OpenPaperdoll(const QString &serial);

	Q_INVOKABLE void ClosePaperdoll(const QString &serial);

	Q_INVOKABLE void MovePaperdoll(const QString &serial, const int &x, const int &y);

	Q_INVOKABLE void RequestContextMenu(const QString &serial);

	Q_INVOKABLE void WaitContextMenu(const QString &serial, const int &index);

	Q_INVOKABLE void CancelContextMenu();

	Q_INVOKABLE void SayYell(const QString &text);

	Q_INVOKABLE void SayWhisper(const QString &text);

	Q_INVOKABLE void SayEmote(const QString &text);

	Q_INVOKABLE void SayBroadcast(const QString &text);

	Q_INVOKABLE void SayParty(const QString &text);

	Q_INVOKABLE void SayGuild(const QString &text);

	Q_INVOKABLE void SayAlliance(const QString &text);

	Q_INVOKABLE void BandageTarget(const QString &serial);

	Q_INVOKABLE void CastTarget(const QString &name,const QString &serial);

	Q_INVOKABLE void CastTarget(const int &index, const QString &serial);

	Q_INVOKABLE void UseSkillTarget(const QString &name,const QString &serial);

	Q_INVOKABLE void UseSkillTarget(const int &index, const QString &serial);

	Q_INVOKABLE int ClientViewRange();

	Q_INVOKABLE void ClientViewRange(const int &range);

	Q_INVOKABLE void OpenOrionMap();

	Q_INVOKABLE void OpenOrionMap(const int &x, const int &y);

	Q_INVOKABLE void MoveOrionMap(const int &x, const int &y);

	Q_INVOKABLE void CloseOrionMap();

	Q_INVOKABLE void OpenEnhancedMap();

	Q_INVOKABLE void OpenEnhancedMap(const QString &filePath);

	Q_INVOKABLE bool WaitForTarget();

	Q_INVOKABLE bool WaitForTarget(const int &maxDelay);

	Q_INVOKABLE bool WaitForMenu();

	Q_INVOKABLE bool WaitForMenu(const int &maxDelay);

	Q_INVOKABLE bool WaitForGump();

	Q_INVOKABLE bool WaitForGump(const int &maxDelay);

	Q_INVOKABLE bool WaitForPrompt();

	Q_INVOKABLE bool WaitForPrompt(const int &maxDelay);

	Q_INVOKABLE bool WaitForShop();

	Q_INVOKABLE bool WaitForShop(const int &maxDelay);

	Q_INVOKABLE bool WaitForTrade();

	Q_INVOKABLE bool WaitForTrade(const int &maxDelay);

	Q_INVOKABLE bool WaitForContextMenu();

	Q_INVOKABLE bool WaitForContextMenu(const int &maxDelay);

	Q_INVOKABLE void LoadProfile(const QString &name);

	Q_INVOKABLE void ActivateClient();

	Q_INVOKABLE void ShutdownWindows();

	Q_INVOKABLE void ShutdownWindows(const QString &mode);

	Q_INVOKABLE bool OnOffHotkeys();

	Q_INVOKABLE void OnOffHotkeys(const bool &state);

	Q_INVOKABLE int GetTargetType();
};
//----------------------------------------------------------------------------------
#endif // OASCRIPTENGINE_H
//----------------------------------------------------------------------------------
