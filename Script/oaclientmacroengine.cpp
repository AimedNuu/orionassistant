// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAClientMacroEngine.cpp
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oaclientmacroengine.h"
#include "../Managers/commandmanager.h"
//----------------------------------------------------------------------------------
COAClientMacroEngine::COAClientMacroEngine()
: QObject()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
COAClientMacroEngine::~COAClientMacroEngine()
{
	OAFUN_DEBUG("");
	m_ActionList.clear();
	m_SubActionList.clear();
}
//----------------------------------------------------------------------------------
void COAClientMacroEngine::AddAction(const QString &action)
{
	OAFUN_DEBUG("");
	m_ActionList.push_back(action);
	m_SubActionList.push_back("");
}
//----------------------------------------------------------------------------------
void COAClientMacroEngine::AddAction(const QString &action, const QString &subAction)
{
	OAFUN_DEBUG("");
	m_ActionList.push_back(action);
	m_SubActionList.push_back(subAction);
}
//----------------------------------------------------------------------------------
void COAClientMacroEngine::Play()
{
	OAFUN_DEBUG("");
	Play(false, 100500);
}
//----------------------------------------------------------------------------------
void COAClientMacroEngine::Play(const bool &waitWhileMacroPlaying)
{
	OAFUN_DEBUG("");
	Play(waitWhileMacroPlaying, 100500);
}
//----------------------------------------------------------------------------------
void COAClientMacroEngine::Play(const bool &waitWhileMacroPlaying, const int &delay)
{
	OAFUN_DEBUG("");
	if (!m_ActionList.empty())
	{
		g_ClientMacroPlayed = waitWhileMacroPlaying;
		emit g_CommandManager.CommandPlayClientMacro(m_ActionList, m_SubActionList);

		uint timer = GetTickCount() + delay;

		while (g_ClientMacroPlayed && timer > GetTickCount())
		{
			Sleep(10);
		}
	}
}
//----------------------------------------------------------------------------------
