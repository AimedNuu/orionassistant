/***********************************************************************************
**
** OAClientMacroEngine.h
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OACLIENTMACROENGINE_H
#define OACLIENTMACROENGINE_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class COAClientMacroEngine : public QObject
{
	Q_OBJECT

private:
	QStringList m_ActionList;
	QStringList m_SubActionList;

public:
	COAClientMacroEngine();

	virtual ~COAClientMacroEngine();

	Q_INVOKABLE void AddAction(const QString &name);

	Q_INVOKABLE void AddAction(const QString &name, const QString &param);

	Q_INVOKABLE void Play();

	Q_INVOKABLE void Play(const bool &waitWhileMacroPlaying);

	Q_INVOKABLE void Play(const bool &waitWhileMacroPlaying, const int &delay);
};
//----------------------------------------------------------------------------------
#endif // OACLIENTMACROENGINE_H
//----------------------------------------------------------------------------------
