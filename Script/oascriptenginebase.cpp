// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAScriptEngineBase.cpp
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
//----------------------------------------------------------------------------------
COAScriptEngineBase::COAScriptEngineBase()
: QObject(nullptr)
{
}
//----------------------------------------------------------------------------------
void COAScriptEngineBase::PauseWaiting()
{
	OAFUN_DEBUG("c38_f1");
	while (m_Paused && !m_Terminated)
		Sleep(10);
}
//----------------------------------------------------------------------------------
