/***********************************************************************************
**
** OAPlayerEngine.h
**
** Copyright (C) Fabruary 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OAPLAYERENGINE_H
#define OAPLAYERENGINE_H
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
#include "../GameObjects/GamePlayer.h"
//----------------------------------------------------------------------------------
class COAPlayerObject : public QObject
{
	Q_OBJECT

public:
	COAPlayerObject();
	virtual ~COAPlayerObject() {}

	Q_INVOKABLE QString Serial();

	Q_INVOKABLE QString Graphic();

	Q_INVOKABLE QString Color();

	Q_INVOKABLE int X();

	Q_INVOKABLE int Y();

	Q_INVOKABLE int Z();

	Q_INVOKABLE QString Container();

	Q_INVOKABLE int Map();

	Q_INVOKABLE int Count();

	Q_INVOKABLE int Flags();

	Q_INVOKABLE QString Name();

	Q_INVOKABLE bool Mobile();

	Q_INVOKABLE bool Ignored();

	Q_INVOKABLE bool Frozen();

	Q_INVOKABLE bool Poisoned();

	Q_INVOKABLE bool Paralyzed();

	Q_INVOKABLE bool Flying();

	Q_INVOKABLE bool YellowHits();

	Q_INVOKABLE bool IgnoreCharacters();

	Q_INVOKABLE bool Locked();

	Q_INVOKABLE bool WarMode();

	Q_INVOKABLE bool Hidden();

	Q_INVOKABLE bool IsHuman();

	Q_INVOKABLE bool IsPlayer();

	Q_INVOKABLE bool IsCorpse();

	Q_INVOKABLE int Layer();

	Q_INVOKABLE bool IsMulti();

	Q_INVOKABLE int EquipLayer();

	Q_INVOKABLE int Hits();

	Q_INVOKABLE int Hits(const QString &args);

	Q_INVOKABLE int MaxHits();

	Q_INVOKABLE int Mana();

	Q_INVOKABLE int Mana(const QString &args);

	Q_INVOKABLE int MaxMana();

	Q_INVOKABLE int Stam();

	Q_INVOKABLE int Stam(const QString &args);

	Q_INVOKABLE int MaxStam();

	Q_INVOKABLE bool Female();

	Q_INVOKABLE int Race();

	Q_INVOKABLE int Direction();

	Q_INVOKABLE int Notoriety();

	Q_INVOKABLE bool CanChangeName();

	Q_INVOKABLE bool Dead();





	Q_INVOKABLE int Str();

	Q_INVOKABLE int Int();

	Q_INVOKABLE int Dex();

	Q_INVOKABLE int LockStrState();

	Q_INVOKABLE int LockIntState();

	Q_INVOKABLE int LockDexState();

	Q_INVOKABLE int Weight();

	Q_INVOKABLE int Weight(const QString &args);

	Q_INVOKABLE int MaxWeight();

	Q_INVOKABLE int Armor();

	Q_INVOKABLE int Gold();

	Q_INVOKABLE int StatsCap();

	Q_INVOKABLE int Followers();

	Q_INVOKABLE int MaxFollowers();

	Q_INVOKABLE int FireResistance();

	Q_INVOKABLE int ColdResistance();

	Q_INVOKABLE int PoisonResistance();

	Q_INVOKABLE int EnergyResistance();

	Q_INVOKABLE int Luck();

	Q_INVOKABLE int MinDamage();

	Q_INVOKABLE int MaxDamage();

	Q_INVOKABLE int TithingPoints();

	Q_INVOKABLE int StealthSteps();

	Q_INVOKABLE QString Properties();

	Q_INVOKABLE bool ProfileReceived();

	Q_INVOKABLE QString Profile();

	Q_INVOKABLE QString Title();
};
//----------------------------------------------------------------------------------
#endif // OAPLAYERENGINE_H
//----------------------------------------------------------------------------------
