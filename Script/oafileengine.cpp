// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAFileEngine.cpp
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oafileengine.h"
#include "../OrionAssistant/orionassistant.h"
//----------------------------------------------------------------------------------
COAScriptFileObject::COAScriptFileObject()
: QObject(), m_Stream(&m_File)
{
	OAFUN_DEBUG("c33_f1");
}
//----------------------------------------------------------------------------------
COAScriptFileObject::~COAScriptFileObject()
{
	OAFUN_DEBUG("c33_f2");
}
//----------------------------------------------------------------------------------
bool COAScriptFileObject::Open(const QString &filePath)
{
	OAFUN_DEBUG("c33_f3");
	return Open(filePath, false);
}
//----------------------------------------------------------------------------------
bool COAScriptFileObject::Append(const QString &filePath)
{
	OAFUN_DEBUG("c33_f4");
	return Append(filePath, false);
}
//----------------------------------------------------------------------------------
bool COAScriptFileObject::Opened()
{
	OAFUN_DEBUG("c33_f5");
	return m_File.isOpen();
}
//----------------------------------------------------------------------------------
void COAScriptFileObject::Close()
{
	OAFUN_DEBUG("c33_f6");
	m_File.close();
}
//----------------------------------------------------------------------------------
QString COAScriptFileObject::ReadLine()
{
	OAFUN_DEBUG("c33_f7");
	return m_Stream.readLine();
}
//----------------------------------------------------------------------------------
QString COAScriptFileObject::Read()
{
	OAFUN_DEBUG("c33_f8");
	QString str = "";
	m_Stream >> str;
	return str;
}
//----------------------------------------------------------------------------------
void COAScriptFileObject::WriteLine(const QString &str)
{
	OAFUN_DEBUG("c33_f9");
	m_Stream << str << "\r\n";
}
//----------------------------------------------------------------------------------
void COAScriptFileObject::Write(const QString &str)
{
	OAFUN_DEBUG("c33_f10");
	m_Stream << str;
}
//----------------------------------------------------------------------------------
bool COAScriptFileObject::Open(const QString &filePath, const bool &checkExists)
{
	OAFUN_DEBUG("c33_f11");
	if (m_File.isOpen())
		m_File.close();

	m_File.setFileName(filePath);

	if (checkExists && !m_File.exists())
		return false;

	return m_File.open(QIODevice::ReadWrite | QIODevice::Text);
}
//----------------------------------------------------------------------------------
bool COAScriptFileObject::Append(const QString &filePath, const bool &checkExists)
{
	OAFUN_DEBUG("c33_f12");
	if (m_File.isOpen())
		m_File.close();

	m_File.setFileName(filePath);

	if (checkExists && !m_File.exists())
		return false;

	return m_File.open(QIODevice::Append | QIODevice::Text);
}
//----------------------------------------------------------------------------------
void COAScriptFileObject::Remove()
{
	OAFUN_DEBUG("c33_f13");
	if (m_File.isOpen())
		m_File.close();

	Remove(m_File.fileName());
}
//----------------------------------------------------------------------------------
void COAScriptFileObject::Remove(const QString &filePath)
{
	OAFUN_DEBUG("c33_f14");

	if (QFile::exists(filePath))
		QFile::remove(filePath);
}
//----------------------------------------------------------------------------------
