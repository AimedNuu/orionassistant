// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAScriptEngine.cpp
**
** Copyright (C) January 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oascriptengine.h"
#include "oajournalengine.h"
#include "oagameobjectengine.h"
#include "../OrionAssistant/orionassistant.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../GameObjects/GamePlayer.h"
#include "../GameObjects/GameWorld.h"
#include "../Managers/commandmanager.h"
#include "../OrionAssistant/Target.h"
#include "../Managers/menumanager.h"
#include "oafileengine.h"
#include "oamenuengine.h"
#include "oagumphookengine.h"
#include "oagumpengine.h"
#include "../Managers/gumpmanager.h"
#include <QProcess>
#include "../OrionAssistant/tabmain.h"
#include "oaclientmacroengine.h"
//----------------------------------------------------------------------------------
COAScriptEngine::COAScriptEngine()
: COAScriptEngineBase()
{
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Wait(QString text)
{
	OAFUN_DEBUG("c41_f1");
	int delay = 0;

	emit g_CommandManager.CommandWait(text, &delay);

	if (delay > 0)
		Sleep(delay);

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Info()
{
	OAFUN_DEBUG("c41_f2");
	Info("");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Info(const QString &serial)
{
	OAFUN_DEBUG("c41_f3");
	emit g_CommandManager.CommandInfo(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::InfoTile()
{
	OAFUN_DEBUG("c41_f4");
	InfoTile("");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::InfoTile(const QString &lasttile)
{
	OAFUN_DEBUG("c41_f5");
	emit g_CommandManager.CommandInfoTile(lasttile);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::InfoMenu()
{
	OAFUN_DEBUG("c41_f6");
	InfoMenu("lastmenu");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SaveConfig()
{
	OAFUN_DEBUG("c41_f7");
	emit g_CommandManager.CommandSaveConfig();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Click()
{
	OAFUN_DEBUG("c41_f8");
	Click("");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Click(const QString &serial)
{
	OAFUN_DEBUG("c41_f9");
	emit g_CommandManager.CommandClick(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseObject()
{
	OAFUN_DEBUG("c41_f10");
	UseObject("");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseObject(const QString &serial)
{
	OAFUN_DEBUG("c41_f11");
	emit g_CommandManager.CommandUseObject(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::GetStatus()
{
	OAFUN_DEBUG("c41_f12");
	GetStatus("");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::GetStatus(const QString &serial)
{
	OAFUN_DEBUG("c41_f13");
	emit g_CommandManager.CommandGetStatus(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Attack(const QString &serial)
{
	OAFUN_DEBUG("c41_f14");
	emit g_CommandManager.CommandAttack(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetLight(const bool &on)
{
	OAFUN_DEBUG("c41_f15");
	SetLight(on, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetLight(const bool &on, const int &value)
{
	OAFUN_DEBUG("c41_f16");
	emit g_CommandManager.CommandSetLight(on, value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetWeather(const bool &on)
{
	OAFUN_DEBUG("c41_f17");
	SetWeather(on, 0, 0, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetWeather(const bool &on, const int &index)
{
	OAFUN_DEBUG("c41_f18");
	SetWeather(on, index, 0, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetWeather(const bool &on, const int &index, const int &count)
{
	OAFUN_DEBUG("c41_f19");
	SetWeather(on, index, count, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetWeather(const bool &on, const int &index, const int &count, const int &temperature)
{
	OAFUN_DEBUG("c41_f20");
	emit g_CommandManager.CommandSetWeather(on, index, count, temperature);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetSeason(const bool &on)
{
	OAFUN_DEBUG("c41_f21");
	SetSeason(on, 0, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetSeason(const bool &on, const int &index)
{
	OAFUN_DEBUG("c41_f22");
	SetSeason(on, index, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetSeason(const bool &on, const int &index, const int &music)
{
	OAFUN_DEBUG("c41_f23");
	emit g_CommandManager.CommandSetSeason(on, index, music);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetTrack(const bool &on)
{
	OAFUN_DEBUG("c41_f24");
	SetTrack(on, -1, -1);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetTrack(const bool &on, const int &x, const int &y)
{
	OAFUN_DEBUG("c41_f25");
	emit g_CommandManager.CommandTrack(on, x, y);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::SaveHotkeys(const QString &fileName)
{
	OAFUN_DEBUG("c41_f26");
	bool result = false;
	emit g_CommandManager.CommandSaveHotkeys(fileName, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::LoadHotkeys(const QString &fileName)
{
	OAFUN_DEBUG("c41_f27");
	bool result = false;
	emit g_CommandManager.CommandLoadHotkeys(fileName, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Cast(const QString &name)
{
	OAFUN_DEBUG("c41_f28");
	emit g_CommandManager.CommandCast(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Cast(const int &index)
{
	OAFUN_DEBUG("c41_f29");
	Cast(QString::number(index));
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Cast(const QString &name, const QString &target)
{
	OAFUN_DEBUG("c41_f30");
	emit g_CommandManager.CommandWaitTargetObject(QStringList() << target);
	emit g_CommandManager.CommandCast(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Cast(const int &index, const QString &target)
{
	OAFUN_DEBUG("c41_f31");
	Cast(QString::number(index), target);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseSkill(const QString &name)
{
	OAFUN_DEBUG("c41_f32");
	emit g_CommandManager.CommandUseSkill(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseSkill(const int &index)
{
	OAFUN_DEBUG("c41_f33");
	UseSkill(QString::number(index));
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseSkill(const QString &name, const QString &target)
{
	OAFUN_DEBUG("c41_f34");
	emit g_CommandManager.CommandWaitTargetObject(QStringList() << target);
	emit g_CommandManager.CommandUseSkill(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseSkill(const int &index, const QString &target)
{
	OAFUN_DEBUG("c41_f35");
	UseSkill(QString::number(index), target);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::SkillValue(const QString &name)
{
	OAFUN_DEBUG("c41_f36");
	return SkillValue(name, "real");
}
//----------------------------------------------------------------------------------
int COAScriptEngine::SkillValue(const int &index)
{
	OAFUN_DEBUG("c41_f37");
	return SkillValue(QString::number(index), "real");
}
//----------------------------------------------------------------------------------
int COAScriptEngine::SkillValue(const QString &name, const QString &type)
{
	OAFUN_DEBUG("c41_f38");
	int result = 0;
	emit g_CommandManager.CommandSkillValue(name, type, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptEngine::SkillValue(const int &index, const QString &type)
{
	OAFUN_DEBUG("c41_f39");
	return SkillValue(QString::number(index), type);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::HelpGump()
{
	OAFUN_DEBUG("c41_f40");
	emit g_CommandManager.CommandHelpGump();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CloseUO()
{
	OAFUN_DEBUG("c41_f41");
	emit g_CommandManager.CommandCloseUO();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WarMode()
{
	OAFUN_DEBUG("c41_f42");
	WarMode(2);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WarMode(const uint &state)
{
	OAFUN_DEBUG("c41_f43");
	emit g_CommandManager.CommandWarmode(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Morph()
{
	OAFUN_DEBUG("c41_f44");
	Morph("");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Morph(const QString &graphic)
{
	OAFUN_DEBUG("c41_f45");
	emit g_CommandManager.CommandMorph(COrionAssistant::TextToGraphic(graphic));
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Resend()
{
	OAFUN_DEBUG("c41_f46");
	emit g_CommandManager.CommandResend();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Sound(const uint &sound)
{
	OAFUN_DEBUG("c41_f47");
	emit g_CommandManager.CommandSound(sound);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::EmoteAction(const QString &name)
{
	OAFUN_DEBUG("c41_f48");
	emit g_CommandManager.CommandEmoteAction(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Hide()
{
	OAFUN_DEBUG("c41_f49");
	Hide("");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Hide(const QString &serial)
{
	OAFUN_DEBUG("c41_f50");
	emit g_CommandManager.CommandHide(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseType(const QStringList &graphic)
{
	OAFUN_DEBUG("c41_f51");
	return UseType(graphic, QStringList() << "0xFFFF", "self", true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseType(const QStringList &graphic, const QStringList &color)
{
	OAFUN_DEBUG("c41_f52");
	return UseType(graphic, color, "self", true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseType(const QStringList &graphic, const QStringList &color, const QString &container)
{
	OAFUN_DEBUG("c41_f53");
	return UseType(graphic, color, container, true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseType(const QStringList &graphic, const QStringList &color, const QString &container, const bool &recurse)
{
	OAFUN_DEBUG("c41_f54");
	bool result = false;
	emit g_CommandManager.CommandUseType(graphic, color, container, recurse, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseType(const QString &graphic)
{
	OAFUN_DEBUG("c41_f51");
	return UseType(QStringList() << graphic, QStringList() << "0xFFFF", "self", true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseType(const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f52");
	return UseType(QStringList() << graphic, QStringList() << color, "self", true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseType(const QString &graphic, const QString &color, const QString &container)
{
	OAFUN_DEBUG("c41_f53");
	return UseType(QStringList() << graphic, QStringList() << color, container, true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseType(const QString &graphic, const QString &color, const QString &container, const bool &recurse)
{
	OAFUN_DEBUG("c41_f53");
	return UseType(QStringList() << graphic, QStringList() << color, container, recurse);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGround(const QStringList &graphic)
{
	OAFUN_DEBUG("c41_f55");
	return UseFromGround(graphic, QStringList() << "0xFFFF", "usedistance", "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGround(const QStringList &graphic, const QStringList &color)
{
	OAFUN_DEBUG("c41_f56");
	return UseFromGround(graphic, color, "usedistance", "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGround(const QStringList &graphic, const QStringList &color, const QString &distance)
{
	OAFUN_DEBUG("c41_f57");
	return UseFromGround(graphic, color, distance, "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGround(const QStringList &graphic, const QStringList &color, const QString &distance, const QString &flags)
{
	OAFUN_DEBUG("c41_f58");
	bool result = false;
	emit g_CommandManager.CommandUseFromGround(graphic, color, distance, flags, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGround(const QString &graphic)
{
	OAFUN_DEBUG("c41_f55");
	return UseFromGround(QStringList() << graphic, QStringList() << "0xFFFF", "usedistance", "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGround(const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f56");
	return UseFromGround(QStringList() << graphic, QStringList() << color, "usedistance", "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGround(const QString &graphic, const QString &color, const QString &distance)
{
	OAFUN_DEBUG("c41_f57");
	return UseFromGround(QStringList() << graphic, QStringList() << color, distance, "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGround(const QString &graphic, const QString &color, const QString &distance, const QString &flags)
{
	OAFUN_DEBUG("c41_f57");
	return UseFromGround(QStringList() << graphic, QStringList() << color, distance, flags);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseTypeList(const QString &findListName)
{
	OAFUN_DEBUG("c41_f59");
	return UseTypeList(findListName, "self", true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseTypeList(const QString &findListName, const QString &container)
{
	OAFUN_DEBUG("c41_f60");
	return UseTypeList(findListName, container, true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseTypeList(const QString &findListName, const QString &container, const bool &recurse)
{
	OAFUN_DEBUG("c41_f61");
	bool result = false;
	emit g_CommandManager.CommandUseTypeList(findListName, container, recurse, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGroundList(const QString &findListName)
{
	OAFUN_DEBUG("c41_f62");
	return UseFromGroundList(findListName, "usedistance", "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGroundList(const QString &findListName, const QString &distance)
{
	OAFUN_DEBUG("c41_f63");
	return UseFromGroundList(findListName, distance, "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::UseFromGroundList(const QString &findListName, const QString &distance, const QString &flags)
{
	OAFUN_DEBUG("c41_f64");
	bool result = false;
	emit g_CommandManager.CommandUseFromGroundList(findListName, distance, flags, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Print(const QString &text)
{
	OAFUN_DEBUG("c41_f65");
	Print("", text);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Print(const QString &color, const QString &text)
{
	OAFUN_DEBUG("c41_f66");
	ushort textColor = 0xFFFF;

	if (color.length())
		textColor = COrionAssistant::TextToGraphic(color);

	emit g_CommandManager.CommandPrint(textColor, text);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CharPrint(const QString &serial, const QString &color, const QString &text)
{
	OAFUN_DEBUG("c41_f67");
	emit g_CommandManager.CommandCharPrint(serial, COrionAssistant::TextToGraphic(color), text);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Say(const QString &text)
{
	OAFUN_DEBUG("c41_f68");
	emit g_CommandManager.CommandSay(text);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::RenameMount(const QString &serial, const QString &name)
{
	OAFUN_DEBUG("c41_f69");
	emit g_CommandManager.CommandRenameMount(serial, name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QStringList &graphic)
{
	OAFUN_DEBUG("c41_f70");
	return FindType(graphic, QStringList() << "0xFFFF", "backpack", "", "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QStringList &graphic, const QStringList &color)
{
	OAFUN_DEBUG("c41_f71");
	return FindType(graphic, color, "backpack", "", "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QStringList &graphic, const QStringList &color, const QString &container)
{
	OAFUN_DEBUG("c41_f72");
	return FindType(graphic, color, container, "", "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags)
{
	OAFUN_DEBUG("c41_f73");
	return FindType(graphic, color, container, flags, "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags, const QString &distance)
{
	OAFUN_DEBUG("c41_f74");
	return FindType(graphic, color, container, flags, distance, "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags, const QString &distance, const QString &notoriety)
{
	OAFUN_DEBUG("c41_f75");
	return FindType(graphic, color, container, flags, distance, notoriety, g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags, const QString &distance, const QString &notoriety, const bool &recurse)
{
	OAFUN_DEBUG("c41_f76");
	QStringList result;
	emit g_CommandManager.CommandFindType(graphic, color, container, distance, flags, notoriety, recurse, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QString &graphic)
{
	OAFUN_DEBUG("c41_f70");
	return FindType(QStringList() << graphic, QStringList() << "0xFFFF", "backpack", "", "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f71");
	return FindType(QStringList() << graphic, QStringList() << color, "backpack", "", "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QString &graphic, const QString &color, const QString &container)
{
	OAFUN_DEBUG("c41_f72");
	return FindType(QStringList() << graphic, QStringList() << color, container, "", "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QString &graphic, const QString &color, const QString &container, const QString &flags)
{
	OAFUN_DEBUG("c41_f73");
	return FindType(QStringList() << graphic, QStringList() << color, container, flags, "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QString &graphic, const QString &color, const QString &container, const QString &flags, const QString &distance)
{
	OAFUN_DEBUG("c41_f74");
	return FindType(QStringList() << graphic, QStringList() << color, container, flags, distance, "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QString &graphic, const QString &color, const QString &container, const QString &flags, const QString &distance, const QString &notoriety)
{
	OAFUN_DEBUG("c41_f75");
	return FindType(QStringList() << graphic, QStringList() << color, container, flags, distance, notoriety, g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindType(const QString &graphic, const QString &color, const QString &container, const QString &flags, const QString &distance, const QString &notoriety, const bool &recurse)
{
	OAFUN_DEBUG("c41_f75");
	return FindType(QStringList() << graphic, QStringList() << color, container, flags, distance, notoriety, recurse);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Ignore(const QString &serial)
{
	OAFUN_DEBUG("c41_f77");
	Ignore(serial, true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Ignore(const QString &serial, const bool &on)
{
	OAFUN_DEBUG("c41_f78");
	emit g_CommandManager.CommandIgnore(serial, on);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::IgnoreReset()
{
	OAFUN_DEBUG("c41_f79");
	emit g_CommandManager.CommandIgnoreReset();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Drop()
{
	OAFUN_DEBUG("c41_f80");
	Drop(0, 0, 0, 0, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Drop(const QString &serial)
{
	OAFUN_DEBUG("c41_f81");
	Drop(serial, 0, g_Player->GetX(), g_Player->GetY(), g_Player->GetZ());
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Drop(const QString &serial, const int &count)
{
	OAFUN_DEBUG("c41_f82");
	Drop(serial, count, g_Player->GetX(), g_Player->GetY(), g_Player->GetZ());
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Drop(const QString &serial, const int &count, const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c41_f83");
	emit g_CommandManager.CommandDrop(serial, count, x, y, z);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::DropHere()
{
	OAFUN_DEBUG("c41_f84");
	DropHere(0, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::DropHere(const QString &serial)
{
	OAFUN_DEBUG("c41_f85");
	DropHere(serial, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::DropHere(const QString &serial, const int &count)
{
	OAFUN_DEBUG("c41_f86");
	emit g_CommandManager.CommandDropHere(serial, count);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::MoveItem()
{
	OAFUN_DEBUG("c41_f87");
	MoveItem(0, 0, 0, 0, 0, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::MoveItem(const QString &serial)
{
	OAFUN_DEBUG("c41_f88");
	MoveItem(serial, 0, "backpack", -1, -1, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::MoveItem(const QString &serial, const int &count)
{
	OAFUN_DEBUG("c41_f89");
	MoveItem(serial, count, "backpack", -1, -1, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::MoveItem(const QString &serial, const int &count, const QString &container)
{
	OAFUN_DEBUG("c41_f90");
	MoveItem(serial, count, container, -1, -1, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::MoveItem(const QString &serial, const int &count, const QString &container, const int &x, const int &y)
{
	OAFUN_DEBUG("c41_f91");
	MoveItem(serial, count, container, x, y, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::MoveItem(const QString &serial, const int &count, const QString &container, const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c41_f92");
	emit g_CommandManager.CommandMoveItem(serial, count, container, x, y, z);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ShowJournal()
{
	OAFUN_DEBUG("c41_f93");
	ShowJournal(-1);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ShowJournal(const int &lines)
{
	OAFUN_DEBUG("c41_f94");
	emit g_CommandManager.CommandShowJournal(lines);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearJournal()
{
	OAFUN_DEBUG("c41_f95");
	ClearJournal("", "", "0", "0xFFFF");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearJournal(const QString &pattern)
{
	OAFUN_DEBUG("c41_f96");
	ClearJournal(pattern, "", "0", "0xFFFF");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearJournal(const QString &pattern, const QString &flags)
{
	OAFUN_DEBUG("c41_f97");
	ClearJournal(pattern, flags, "0", "0xFFFF");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearJournal(const QString &pattern, const QString &flags, const QString &serial)
{
	OAFUN_DEBUG("c41_f98");
	ClearJournal(pattern, flags, serial, "0xFFFF");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearJournal(const QString &pattern, const QString &flags, const QString &serial, const QString &color)
{
	OAFUN_DEBUG("c41_f99");
	emit g_CommandManager.CommandClearJournal(pattern, serial, COrionAssistant::TextToGraphic(color), flags);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::JournalIgnoreCase(const bool &on)
{
	OAFUN_DEBUG("c41_f100");
	emit g_CommandManager.CommandJournalIgnoreCase(on);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::InJournal(const QString &pattern)
{
	OAFUN_DEBUG("c41_f101");
	return InJournal(pattern, "", "0", "0xFFFF", 0, 0);
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::InJournal(const QString &pattern, const QString &flags)
{
	OAFUN_DEBUG("c41_f102");
	return InJournal(pattern, flags, "0", "0xFFFF", 0, 0);
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::InJournal(const QString &pattern, const QString &flags, const QString &serial)
{
	OAFUN_DEBUG("c41_f103");
	return InJournal(pattern, flags, serial, "0xFFFF", 0, 0);
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::InJournal(const QString &pattern, const QString &flags, const QString &serial, const QString &color)
{
	OAFUN_DEBUG("c41_f104");
	return InJournal(pattern, flags, serial, color, 0, 0);
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::InJournal(const QString &pattern, const QString &flags, const QString &serial, const QString &color, const uint &startTime, const uint &endTime)
{
	OAFUN_DEBUG("c41_f105");
	CJournalMessage *result = nullptr;
	emit g_CommandManager.CommandInJournal(pattern, serial, COrionAssistant::TextToGraphic(color), flags, startTime, endTime, &result);
	PauseWaiting();

	if (result != nullptr)
		return new COAScriptJournalObject(result);

	return nullptr;
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::WaitJournal(const QString &pattern, const uint &startTime, const uint &endTime)
{
	OAFUN_DEBUG("c41_f106");
	return WaitJournal(pattern, startTime, endTime, "", "0", "0xFFFF");
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::WaitJournal(const QString &pattern, const uint &startTime, const uint &endTime, const QString &flags)
{
	OAFUN_DEBUG("c41_f107");
	return WaitJournal(pattern, startTime, endTime, flags, "0", "0xFFFF");
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::WaitJournal(const QString &pattern, const uint &startTime, const uint &endTime, const QString &flags, const QString &serial)
{
	OAFUN_DEBUG("c41_f108");
	return WaitJournal(pattern, startTime, endTime, flags, serial, "0xFFFF");
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::WaitJournal(const QString &pattern, const uint &startTime, const uint &endTime, const QString &flags, const QString &serial, const QString &colorText)
{
	OAFUN_DEBUG("c41_f109");
	CJournalMessage *result = nullptr;
	ushort color = COrionAssistant::TextToGraphic(colorText);

	while (!endTime || GetTickCount() < endTime)
	{
		emit g_CommandManager.CommandInJournal(pattern, serial, color, flags, startTime, endTime, &result);

		if (result != nullptr)
			return new COAScriptJournalObject(result);

		Sleep(10);
		PauseWaiting();
	}

	return nullptr;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetDressBag(const QString &serial)
{
	OAFUN_DEBUG("c41_f110");
	emit g_CommandManager.CommandSetDressBag(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UnsetDressBag()
{
	OAFUN_DEBUG("c41_f111");
	emit g_CommandManager.CommandUnsetDressBag();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetArm(const QString &name)
{
	OAFUN_DEBUG("c41_f112");
	emit g_CommandManager.CommandSetArm(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UnsetArm(const QString &name)
{
	OAFUN_DEBUG("c41_f113");
	emit g_CommandManager.CommandUnsetArm(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetDress(const QString &name)
{
	OAFUN_DEBUG("c41_f114");
	emit g_CommandManager.CommandSetDress(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UnsetDress(const QString &name)
{
	OAFUN_DEBUG("c41_f115");
	emit g_CommandManager.CommandUnsetDress(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Arm(const QString &name)
{
	OAFUN_DEBUG("c41_f116");
	emit g_CommandManager.CommandArm(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Disarm()
{
	OAFUN_DEBUG("c41_f117");
	emit g_CommandManager.CommandDisarm();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Dress(const QString &name)
{
	OAFUN_DEBUG("c41_f118");
	emit g_CommandManager.CommandDress(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Undress()
{
	OAFUN_DEBUG("c41_f119");
	emit g_CommandManager.CommandUndress();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Unequip(const QString &layerName)
{
	OAFUN_DEBUG("c41_f120");
	emit g_CommandManager.CommandUnequip(layerName);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Equip(const QString &serial)
{
	OAFUN_DEBUG("c41_f121");
	emit g_CommandManager.CommandEquip(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::EquipT(const QString &graphic)
{
	OAFUN_DEBUG("c41_f122");
	EquipT(graphic, "0xFFFF");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::EquipT(const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f123");
	emit g_CommandManager.CommandEquipT(graphic, color);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::HaveTarget()
{
	OAFUN_DEBUG("c41_f124");
	int result = 0;
	emit g_CommandManager.CommandHaveTarget(&result);

	return (result != 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetObject(const QStringList &objects)
{
	OAFUN_DEBUG("c41_f125");
	emit g_CommandManager.CommandWaitTargetObject(objects);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetObject(const QString &objects)
{
	OAFUN_DEBUG("c41_f125");
	emit g_CommandManager.CommandWaitTargetObject(QStringList() << objects);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetType(const QStringList &graphic)
{
	OAFUN_DEBUG("c41_f126");
	WaitTargetType(graphic, QStringList() << "0xFFFF", "self", "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetType(const QStringList &graphic, const QStringList &color)
{
	OAFUN_DEBUG("c41_f127");
	WaitTargetType(graphic, color, "self", "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetType(const QStringList &graphic, const QStringList &color, const QString &container)
{
	OAFUN_DEBUG("c41_f128");
	WaitTargetType(graphic, color, container, "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags)
{
	OAFUN_DEBUG("c41_f129");
	WaitTargetType(graphic, color, container, flags, true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags, const bool &recurse)
{
	OAFUN_DEBUG("c41_f130");
	QStringList result;
	emit g_CommandManager.CommandFindType(graphic, color, container, "1", flags, "", recurse, &result);

	if (result.size())
		emit g_CommandManager.CommandWaitTargetObject(QStringList() << result[0]);
	PauseWaiting();
}

//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetType(const QString &graphic)
{
	OAFUN_DEBUG("c41_f126");
	WaitTargetType(QStringList() << graphic, QStringList() << "0xFFFF", "self", "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetType(const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f127");
	WaitTargetType(QStringList() << graphic, QStringList() << color, "self", "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetType(const QString &graphic, const QString &color, const QString &container)
{
	OAFUN_DEBUG("c41_f128");
	WaitTargetType(QStringList() << graphic, QStringList() << color, container, "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetType(const QString &graphic, const QString &color, const QString &container, const QString &flags)
{
	OAFUN_DEBUG("c41_f129");
	WaitTargetType(QStringList() << graphic, QStringList() << color, container, flags, true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetType(const QString &graphic, const QString &color, const QString &container, const QString &flags, const bool &recurse)
{
	OAFUN_DEBUG("c41_f129");
	WaitTargetType(QStringList() << graphic, QStringList() << color, container, flags, recurse);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetGround(const QStringList &graphic)
{
	OAFUN_DEBUG("c41_f131");
	WaitTargetGround(graphic, QStringList() << "0xFFFF", "finddistance", "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetGround(const QStringList &graphic, const QStringList &color)
{
	OAFUN_DEBUG("c41_f132");
	WaitTargetGround(graphic, color, "finddistance", "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetGround(const QStringList &graphic, const QStringList &color, const QString &distance)
{
	OAFUN_DEBUG("c41_f133");
	WaitTargetGround(graphic, color, distance, "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetGround(const QStringList &graphic, const QStringList &color, const QString &distance, const QString &flags)
{
	OAFUN_DEBUG("c41_f134");
	QStringList result;
	emit g_CommandManager.CommandFindType(graphic, color, "ground", distance, flags, "", false, &result);

	if (result.size())
		emit g_CommandManager.CommandWaitTargetObject(QStringList() << result[0]);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetGround(const QString &graphic)
{
	OAFUN_DEBUG("c41_f131");
	WaitTargetGround(QStringList() << graphic, QStringList() << "0xFFFF", "finddistance", "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetGround(const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f132");
	WaitTargetGround(QStringList() << graphic, QStringList() << color, "finddistance", "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetGround(const QString &graphic, const QString &color, const QString &distance)
{
	OAFUN_DEBUG("c41_f133");
	WaitTargetGround(QStringList() << graphic, QStringList() << color, distance, "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetGround(const QString &graphic, const QString &color, const QString &distance, const QString &flags)
{
	OAFUN_DEBUG("c41_f133");
	WaitTargetGround(QStringList() << graphic, QStringList() << color, distance, flags);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitTargetTypeList(const QString &findListName)
{
	OAFUN_DEBUG("c41_f135");
	return WaitTargetTypeList(findListName, "self", "fast", true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitTargetTypeList(const QString &findListName, const QString &container)
{
	OAFUN_DEBUG("c41_f136");
	return WaitTargetTypeList(findListName, container, "fast", true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitTargetTypeList(const QString &findListName, const QString &container, const QString &flags)
{
	OAFUN_DEBUG("c41_f137");
	return WaitTargetTypeList(findListName, container, flags, true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitTargetTypeList(const QString &findListName, const QString &container, const QString &flags, const bool &recurse)
{
	OAFUN_DEBUG("c41_f138");
	QStringList list;
	emit g_CommandManager.CommandFindList(findListName, container, "1", flags, "", recurse, &list);

	if (list.size())
		emit g_CommandManager.CommandWaitTargetObject(QStringList() << list[0]);
	PauseWaiting();

	return (list.size() != 0);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitTargetGroundList(const QString &findListName)
{
	OAFUN_DEBUG("c41_f139");
	return WaitTargetGroundList(findListName, "finddistance", "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitTargetGroundList(const QString &findListName, const QString &distance)
{
	OAFUN_DEBUG("c41_f140");
	return WaitTargetGroundList(findListName, distance, "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitTargetGroundList(const QString &findListName, const QString &distance, const QString &flags)
{
	OAFUN_DEBUG("c41_f141");
	QStringList list;
	emit g_CommandManager.CommandFindList(findListName, "ground", distance, flags, "", false, &list);

	if (list.size())
		emit g_CommandManager.CommandWaitTargetObject(QStringList() << list[0]);
	PauseWaiting();

	return (list.size() != 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetTile(const QString &tile)
{
	OAFUN_DEBUG("c41_f142");
	WaitTargetTile(tile, 0, 0, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetTile(const QString &tile, const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c41_f143");
	emit g_CommandManager.CommandWaitTargetTile(tile, x, y, z, false);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitTargetTileRelative(const QString &tile, const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c41_f144");
	emit g_CommandManager.CommandWaitTargetTile(tile, x, y, z, true);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CancelWaitTarget()
{
	OAFUN_DEBUG("c41_f145");
	emit g_CommandManager.CommandCancelWaitTarget();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitMenu(const QString &prompt, const QString &choice)
{
	OAFUN_DEBUG("c41_f146");
	emit g_CommandManager.CommandWaitMenu(prompt, choice);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CancelWaitMenu()
{
	OAFUN_DEBUG("c41_f147");
	emit g_CommandManager.CommandCancelWaitMenu();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Now()
{
	OAFUN_DEBUG("c41_f148");
	return GetTickCount();
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::FindObject(const QString &serial)
{
	OAFUN_DEBUG("c41_f149");
	int objSerial = 0;
	emit g_CommandManager.CommandFindObject(serial, &objSerial);

	if (objSerial)
		return new COAScriptGameObject((uint)objSerial);

	return nullptr;
}
//----------------------------------------------------------------------------------
int COAScriptEngine::GetDistance(const QString &serial)
{
	OAFUN_DEBUG("c41_f150");
	int result = 100500;
	emit g_CommandManager.CommandGetDistance(serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptEngine::GetDistance(const int &x, const int &y)
{
	OAFUN_DEBUG("c41_f151");
	int result = 100500;
	emit g_CommandManager.CommandGetDistance(x, y, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Count(const QStringList &graphic)
{
	OAFUN_DEBUG("c41_f152");
	return Count(graphic, QStringList() << "0xFFFF", "self", "finddistance", true);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Count(const QStringList &graphic, const QStringList &color)
{
	OAFUN_DEBUG("c41_f153");
	return Count(graphic, color, "self", "finddistance", true);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Count(const QStringList &graphic, const QStringList &color, const QString &container)
{
	OAFUN_DEBUG("c41_f154");
	return Count(graphic, color, container, "finddistance", true);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Count(const QStringList &graphic, const QStringList &color, const QString &container, const QString &distance)
{
	OAFUN_DEBUG("c41_f155");
	return Count(graphic, color, container, distance, true);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Count(const QStringList &graphic, const QStringList &color, const QString &container, const QString &distance, const bool &recurse)
{
	OAFUN_DEBUG("c41_f156");
	int result = 0;
	emit g_CommandManager.CommandCountType(graphic, color, container, distance, recurse, &result);
	PauseWaiting();

	return result;
}

//----------------------------------------------------------------------------------
int COAScriptEngine::Count(const QString &graphic)
{
	OAFUN_DEBUG("c41_f152");
	return Count(QStringList() << graphic, QStringList() << "0xFFFF", "self", "finddistance", true);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Count(const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f153");
	return Count(QStringList() << graphic, QStringList() << color, "self", "finddistance", true);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Count(const QString &graphic, const QString &color, const QString &container)
{
	OAFUN_DEBUG("c41_f154");
	return Count(QStringList() << graphic, QStringList() << color, container, "finddistance", true);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Count(const QString &graphic, const QString &color, const QString &container, const QString &distance)
{
	OAFUN_DEBUG("c41_f155");
	return Count(QStringList() << graphic, QStringList() << color, container, distance, true);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Count(const QString &graphic, const QString &color, const QString &container, const QString &distance, const bool &recurse)
{
	OAFUN_DEBUG("c41_f155");
	return Count(QStringList() << graphic, QStringList() << color, container, distance, recurse);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ResetIgnoreList()
{
	OAFUN_DEBUG("c41_f157");
	emit g_CommandManager.CommandSetUsedIgnoreList("");
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseIgnoreList(const QString &listName)
{
	OAFUN_DEBUG("c41_f158");
	emit g_CommandManager.CommandSetUsedIgnoreList(listName);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindList(const QString &findListName)
{
	OAFUN_DEBUG("c41_f159");
	return FindList(findListName, "backpack", "", "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindList(const QString &findListName, const QString &container)
{
	OAFUN_DEBUG("c41_f160");
	return FindList(findListName, container, "", "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindList(const QString &findListName, const QString &container, const QString &flags)
{
	OAFUN_DEBUG("c41_f161");
	return FindList(findListName, container, flags, "finddistance", "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindList(const QString &findListName, const QString &container, const QString &flags, const QString &distance)
{
	OAFUN_DEBUG("c41_f162");
	return FindList(findListName, container, flags, distance, "", g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindList(const QString &findListName, const QString &container, const QString &flags, const QString &distance, const QString &notoriety)
{
	OAFUN_DEBUG("c41_f163");
	return FindList(findListName, container, flags, distance, notoriety, g_TabMain->RecurseContainersSearch());
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::FindList(const QString &findListName, const QString &container, const QString &flags, const QString &distance, const QString &notoriety, const bool &recurse)
{
	OAFUN_DEBUG("c41_f164");
	QStringList result;
	emit g_CommandManager.CommandFindList(findListName, container, distance, flags, notoriety, recurse, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetObject(const QStringList &objects)
{
	OAFUN_DEBUG("c41_f165");
	emit g_CommandManager.CommandTargetObject(objects);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetObject(const QString &objects)
{
	OAFUN_DEBUG("c41_f165");
	emit g_CommandManager.CommandTargetObject(QStringList() << objects);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetType(const QStringList &graphic)
{
	OAFUN_DEBUG("c41_f166");
	TargetType(graphic, QStringList() << "0xFFFF", "self", "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetType(const QStringList &graphic, const QStringList &color)
{
	OAFUN_DEBUG("c41_f167");
	TargetType(graphic, color, "self", "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetType(const QStringList &graphic, const QStringList &color, const QString &container)
{
	OAFUN_DEBUG("c41_f168");
	TargetType(graphic, color, container, "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags)
{
	OAFUN_DEBUG("c41_f169");
	TargetType(graphic, color, container, flags, true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &flags, const bool &recurse)
{
	OAFUN_DEBUG("c41_f170");
	QStringList result;
	emit g_CommandManager.CommandFindType(graphic, color, container, "1", flags, "", recurse, &result);

	if (result.size())
		emit g_CommandManager.CommandTargetObject(QStringList() << result[0]);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetType(const QString &graphic)
{
	OAFUN_DEBUG("c41_f166");
	TargetType(QStringList() << graphic, QStringList() << "0xFFFF", "self", "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetType(const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f167");
	TargetType(QStringList() << graphic, QStringList() << color, "self", "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetType(const QString &graphic, const QString &color, const QString &container)
{
	OAFUN_DEBUG("c41_f168");
	TargetType(QStringList() << graphic, QStringList() << color, container, "fast", true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetType(const QString &graphic, const QString &color, const QString &container, const QString &flags)
{
	OAFUN_DEBUG("c41_f169");
	TargetType(QStringList() << graphic, QStringList() << color, container, flags, true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetType(const QString &graphic, const QString &color, const QString &container, const QString &flags, const bool &recurse)
{
	OAFUN_DEBUG("c41_f169");
	TargetType(QStringList() << graphic, QStringList() << color, container, flags, recurse);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetGround(const QStringList &graphic)
{
	OAFUN_DEBUG("c41_f171");
	TargetGround(graphic, QStringList() << "0xFFFF", "finddistance", "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetGround(const QStringList &graphic, const QStringList &color)
{
	OAFUN_DEBUG("c41_f172");
	TargetGround(graphic, color, "finddistance", "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetGround(const QStringList &graphic, const QStringList &color, const QString &distance)
{
	OAFUN_DEBUG("c41_f173");
	TargetGround(graphic, color, distance, "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetGround(const QStringList &graphic, const QStringList &color, const QString &distance, const QString &flags)
{
	OAFUN_DEBUG("c41_f174");
	QStringList result;
	emit g_CommandManager.CommandFindType(graphic, color, "ground", distance, flags, "", false, &result);

	if (result.size())
		emit g_CommandManager.CommandTargetObject(QStringList() << result[0]);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetGround(const QString &graphic)
{
	OAFUN_DEBUG("c41_f171");
	TargetGround(QStringList() << graphic, QStringList() << "0xFFFF", "finddistance", "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetGround(const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f172");
	TargetGround(QStringList() << graphic, QStringList() << color, "finddistance", "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetGround(const QString &graphic, const QString &color, const QString &distance)
{
	OAFUN_DEBUG("c41_f173");
	TargetGround(QStringList() << graphic, QStringList() << color, distance, "fast");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetGround(const QString &graphic, const QString &color, const QString &distance, const QString &flags)
{
	OAFUN_DEBUG("c41_f173");
	TargetGround(QStringList() << graphic, QStringList() << color, distance, flags);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::TargetTypeList(const QString &findListName)
{
	OAFUN_DEBUG("c41_f175");
	return TargetTypeList(findListName, "self", "fast", true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::TargetTypeList(const QString &findListName, const QString &container)
{
	OAFUN_DEBUG("c41_f176");
	return TargetTypeList(findListName, container, "fast", true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::TargetTypeList(const QString &findListName, const QString &container, const QString &flags)
{
	OAFUN_DEBUG("c41_f177");
	return TargetTypeList(findListName, container, flags, true);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::TargetTypeList(const QString &findListName, const QString &container, const QString &flags, const bool &recurse)
{
	OAFUN_DEBUG("c41_f178");
	QStringList list;
	emit g_CommandManager.CommandFindList(findListName, container, "1", flags, "", recurse, &list);

	if (list.size())
		emit g_CommandManager.CommandTargetObject(QStringList() << list[0]);
	PauseWaiting();

	return (list.size() != 0);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::TargetGroundList(const QString &findListName)
{
	OAFUN_DEBUG("c41_f179");
	return TargetGroundList(findListName, "finddistance", "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::TargetGroundList(const QString &findListName, const QString &distance)
{
	OAFUN_DEBUG("c41_f180");
	return TargetGroundList(findListName, distance, "fast");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::TargetGroundList(const QString &findListName, const QString &distance, const QString &flags)
{
	OAFUN_DEBUG("c41_f181");
	QStringList list;
	emit g_CommandManager.CommandFindList(findListName, "ground", distance, flags, "", false, &list);

	if (list.size())
		emit g_CommandManager.CommandTargetObject(QStringList() << list[0]);
	PauseWaiting();

	return (list.size() != 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetTile(const QString &tile)
{
	OAFUN_DEBUG("c41_f182");
	TargetTile(tile, 0, 0, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetTile(const QString &tile, const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c41_f183");
	emit g_CommandManager.CommandTargetTile(tile, x, y, z, false);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetTileRelative(const QString &tile, const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c41_f184");
	emit g_CommandManager.CommandTargetTile(tile, x, y, z, true);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::BlockMoving(const bool &on)
{
	OAFUN_DEBUG("c41_f185");
	emit g_CommandManager.CommandBlockMoving(on);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::CanWalk(const int &direction, const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c41_f186");
	bool result = false;
	emit g_CommandManager.CommandCanWalk(direction, x, y, z, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::Step(const int &direction)
{
	OAFUN_DEBUG("c41_f187");
	return Step(direction, false);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::Step(const int &direction, const bool &run)
{
	OAFUN_DEBUG("c41_f188");
	bool result = false;
	emit g_CommandManager.CommandStep(direction, run, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WalkTo(const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c41_f189");
	return WalkTo(x, y, z, 1);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WalkTo(const int &x, const int &y, const int &z, const int &distance)
{
	OAFUN_DEBUG("c41_f190");
	bool result = false;
	emit g_CommandManager.CommandWalkTo(x, y, z, distance, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::StopWalking()
{
	OAFUN_DEBUG("c41_f191");
	emit g_CommandManager.CommandStopWalking();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::IsWalking()
{
	OAFUN_DEBUG("c41_f192");
	bool result = false;
	emit g_CommandManager.CommandIsWalking(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::NewFile()
{
	OAFUN_DEBUG("c41_f193");
	return new COAScriptFileObject();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddType(const QString &name)
{
	OAFUN_DEBUG("c41_f194");
	AddType(name, "");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddType(const QString &name, const QString &type)
{
	OAFUN_DEBUG("c41_f195");
	emit g_CommandManager.CommandAddType(name, type);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::RemoveType(const QString &name)
{
	OAFUN_DEBUG("c41_f196");
	emit g_CommandManager.CommandRemoveType(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddObject(const QString &name)
{
	OAFUN_DEBUG("c41_f197");
	AddObject(name, "");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddObject(const QString &name, const QString &object)
{
	OAFUN_DEBUG("c41_f198");
	emit g_CommandManager.CommandAddObject(name, object);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::RemoveObject(const QString &name)
{
	OAFUN_DEBUG("c41_f199");
	emit g_CommandManager.CommandRemoveObject(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::ObjAtLayer(const QString &layer)
{
	OAFUN_DEBUG("c41_f200");
	return ObjAtLayer(layer, "");
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::ObjAtLayer(const QString &layer, const QString &serial)
{
	OAFUN_DEBUG("c41_f201");
	int objSerial = 0;
	emit g_CommandManager.CommandObjAtLayer(layer, serial, &objSerial);

	if (objSerial)
		return new COAScriptGameObject((uint)objSerial);

	return nullptr;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::LoadScript(const QString &filePath)
{
	OAFUN_DEBUG("c41_f202");
	emit g_CommandManager.CommandLoadScript(filePath);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Exec(const QString &name)
{
	OAFUN_DEBUG("c41_f203");
	Exec(name, false, QStringList());
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Exec(const QString &name, const bool &once)
{
	OAFUN_DEBUG("c41_f204");
	Exec(name, once, QStringList());
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Exec(const QString &name, const bool &once, const QString &args)
{
	OAFUN_DEBUG("c41_f204");
	Exec(name, once, QStringList() << args);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Exec(const QString &name, const bool &once, const QStringList &args)
{
	OAFUN_DEBUG("c41_f205");
	emit g_CommandManager.CommandExec(name, once, args);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Terminate(const QString &functionName)
{
	OAFUN_DEBUG("c41_f206");
	Terminate(functionName, "");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Terminate(const QString &functionName, const QString &functionsSave)
{
	OAFUN_DEBUG("c41_f207");
	emit g_CommandManager.CommandTerminate(functionName, functionsSave);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::ScriptRunning(const QString &name)
{
	OAFUN_DEBUG("c41_f208");
	bool result = false;
	emit g_CommandManager.CommandScriptRunning(name, &result);

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::BandageSelf()
{
	OAFUN_DEBUG("c41_f209");
	emit g_CommandManager.CommandWaitTargetObject(QStringList() << "self");
	bool result = false;
	emit g_CommandManager.CommandUseType(QStringList() << "0x0E21", QStringList() << "0", "backpack", true, &result);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionSound()
{
	OAFUN_DEBUG("c41_f210");
	bool result = false;
	emit g_CommandManager.CommandOptionSound(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionSound(const bool &state)
{
	OAFUN_DEBUG("c41_f211");
	emit g_CommandManager.CommandOptionSound(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::OptionSoundVolume()
{
	OAFUN_DEBUG("c41_f212");
	int result = 0;
	emit g_CommandManager.CommandOptionSoundVolume(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionSoundVolume(const int &value)
{
	OAFUN_DEBUG("c41_f213");
	emit g_CommandManager.CommandOptionSoundVolume(value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionMusic()
{
	OAFUN_DEBUG("c41_f214");
	bool result = false;
	emit g_CommandManager.CommandOptionMusic(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionMusic(const bool &state)
{
	OAFUN_DEBUG("c41_f215");
	emit g_CommandManager.CommandOptionMusic(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::OptionMusicVolume()
{
	OAFUN_DEBUG("c41_f216");
	int result = 0;
	emit g_CommandManager.CommandOptionMusicVolume(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionMusicVolume(const int &value)
{
	OAFUN_DEBUG("c41_f217");
	emit g_CommandManager.CommandOptionMusicVolume(value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionUseTooltips()
{
	OAFUN_DEBUG("c41_f218");
	bool result = false;
	emit g_CommandManager.CommandOptionUseTooltips(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionUseTooltips(const bool &state)
{
	OAFUN_DEBUG("c41_f219");
	emit g_CommandManager.CommandOptionUseTooltips(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionAlwaysRun()
{
	OAFUN_DEBUG("c41_f220");
	bool result = false;
	emit g_CommandManager.CommandOptionAlwaysRun(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionAlwaysRun(const bool &state)
{
	OAFUN_DEBUG("c41_f221");
	emit g_CommandManager.CommandOptionAlwaysRun(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionNewTargetSystem()
{
	OAFUN_DEBUG("c41_f222");
	bool result = false;
	emit g_CommandManager.CommandOptionNewTargetSystem(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionNewTargetSystem(const bool &state)
{
	OAFUN_DEBUG("c41_f223");
	emit g_CommandManager.CommandOptionNewTargetSystem(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionObjectHandles()
{
	OAFUN_DEBUG("c41_f224");
	bool result = false;
	emit g_CommandManager.CommandOptionObjectHandles(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionObjectHandles(const bool &state)
{
	OAFUN_DEBUG("c41_f225");
	emit g_CommandManager.CommandOptionObjectHandles(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionScaleSpeech()
{
	OAFUN_DEBUG("c41_f226");
	bool result = false;
	emit g_CommandManager.CommandOptionScaleSpeech(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionScaleSpeech(const bool &state)
{
	OAFUN_DEBUG("c41_f227");
	emit g_CommandManager.CommandOptionScaleSpeech(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::OptionScaleSpeechDelay()
{
	OAFUN_DEBUG("c41_f228");
	int result = 0;
	emit g_CommandManager.CommandOptionScaleSpeechDelay(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionScaleSpeechDelay(const int &value)
{
	OAFUN_DEBUG("c41_f229");
	emit g_CommandManager.CommandOptionScaleSpeechDelay(value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionIgnoreGuildMessages()
{
	OAFUN_DEBUG("c41_f230");
	bool result = false;
	emit g_CommandManager.CommandOptionIgnoreGuildMessages(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionIgnoreGuildMessages(const bool &state)
{
	OAFUN_DEBUG("c41_f231");
	emit g_CommandManager.CommandOptionIgnoreGuildMessages(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionIgnoreAllianceMessages()
{
	OAFUN_DEBUG("c41_f232");
	bool result = false;
	emit g_CommandManager.CommandOptionIgnoreAllianceMessages(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionIgnoreAllianceMessages(const bool &state)
{
	OAFUN_DEBUG("c41_f233");
	emit g_CommandManager.CommandOptionIgnoreAllianceMessages(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionDarkNights()
{
	OAFUN_DEBUG("c41_f234");
	bool result = false;
	emit g_CommandManager.CommandOptionDarkNights(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionDarkNights(const bool &state)
{
	OAFUN_DEBUG("c41_f235");
	emit g_CommandManager.CommandOptionDarkNights(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionColoredLighting()
{
	OAFUN_DEBUG("c41_f236");
	bool result = false;
	emit g_CommandManager.CommandOptionColoredLighting(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionColoredLighting(const bool &state)
{
	OAFUN_DEBUG("c41_f237");
	emit g_CommandManager.CommandOptionColoredLighting(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionCriminalActionsQuery()
{
	OAFUN_DEBUG("c41_f238");
	bool result = false;
	emit g_CommandManager.CommandOptionCriminalActionsQuery(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionCriminalActionsQuery(const bool &state)
{
	OAFUN_DEBUG("c41_f239");
	emit g_CommandManager.CommandOptionCriminalActionsQuery(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionCircleOfTransparency()
{
	OAFUN_DEBUG("c41_f240");
	bool result = false;
	emit g_CommandManager.CommandOptionCircleOfTransparency(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionCircleOfTransparency(const bool &state)
{
	OAFUN_DEBUG("c41_f241");
	emit g_CommandManager.CommandOptionCircleOfTransparency(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::OptionCircleOfTransparencyValue()
{
	OAFUN_DEBUG("c41_f242");
	int result = 0;
	emit g_CommandManager.CommandOptionCircleOfTransparencyValue(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionCircleOfTransparencyValue(const int &value)
{
	OAFUN_DEBUG("c41_f243");
	emit g_CommandManager.CommandOptionCircleOfTransparencyValue(value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionLockResizingGameWindow()
{
	OAFUN_DEBUG("c41_f244");
	bool result = false;
	emit g_CommandManager.CommandOptionLockResizingGameWindow(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionLockResizingGameWindow(const bool &state)
{
	OAFUN_DEBUG("c41_f245");
	emit g_CommandManager.CommandOptionLockResizingGameWindow(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::OptionFPSValue()
{
	OAFUN_DEBUG("c41_f246");
	int result = 0;
	emit g_CommandManager.CommandOptionFPSValue(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionFPSValue(const int &value)
{
	OAFUN_DEBUG("c41_f247");
	emit g_CommandManager.CommandOptionFPSValue(value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionUseScalingGameWindow()
{
	OAFUN_DEBUG("c41_f248");
	bool result = false;
	emit g_CommandManager.CommandOptionUseScalingGameWindow(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionUseScalingGameWindow(const bool &state)
{
	OAFUN_DEBUG("c41_f249");
	emit g_CommandManager.CommandOptionUseScalingGameWindow(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::OptionDrawStatusState()
{
	OAFUN_DEBUG("c41_f250");
	int result = 0;
	emit g_CommandManager.CommandOptionDrawStatusState(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionDrawStatusState(const int &state)
{
	OAFUN_DEBUG("c41_f251");
	emit g_CommandManager.CommandOptionDrawStatusState(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionDrawStumps()
{
	OAFUN_DEBUG("c41_f252");
	bool result = false;
	emit g_CommandManager.CommandOptionDrawStumps(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionDrawStumps(const bool &state)
{
	OAFUN_DEBUG("c41_f253");
	emit g_CommandManager.CommandOptionDrawStumps(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionMarkingCaves()
{
	OAFUN_DEBUG("c41_f254");
	bool result = false;
	emit g_CommandManager.CommandOptionMarkingCaves(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionMarkingCaves(const bool &state)
{
	OAFUN_DEBUG("c41_f255");
	emit g_CommandManager.CommandOptionMarkingCaves(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionNoVegetation()
{
	OAFUN_DEBUG("c41_f256");
	bool result = false;
	emit g_CommandManager.CommandOptionNoVegetation(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionNoVegetation(const bool &state)
{
	OAFUN_DEBUG("c41_f257");
	emit g_CommandManager.CommandOptionNoVegetation(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionNoFieldsAnimation()
{
	OAFUN_DEBUG("c41_f258");
	bool result = false;
	emit g_CommandManager.CommandOptionNoFieldsAnimation(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionNoFieldsAnimation(const bool &state)
{
	OAFUN_DEBUG("c41_f259");
	emit g_CommandManager.CommandOptionNoFieldsAnimation(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionStandardCharactesFrameRate()
{
	OAFUN_DEBUG("c41_f260");
	bool result = false;
	emit g_CommandManager.CommandOptionStandardCharactesFrameRate(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionStandardCharactesFrameRate(const bool &state)
{
	OAFUN_DEBUG("c41_f261");
	emit g_CommandManager.CommandOptionStandardCharactesFrameRate(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionStandardItemsFrameRate()
{
	OAFUN_DEBUG("c41_f262");
	bool result = false;
	emit g_CommandManager.CommandOptionStandardItemsFrameRate(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionStandardItemsFrameRate(const bool &state)
{
	OAFUN_DEBUG("c41_f263");
	emit g_CommandManager.CommandOptionStandardItemsFrameRate(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionLockGumpsMoving()
{
	OAFUN_DEBUG("c41_f264");
	bool result = false;
	emit g_CommandManager.CommandOptionLockGumpsMoving(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionLockGumpsMoving(const bool &state)
{
	OAFUN_DEBUG("c41_f265");
	emit g_CommandManager.CommandOptionLockGumpsMoving(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionEnterChat()
{
	OAFUN_DEBUG("c41_f266");
	bool result = false;
	emit g_CommandManager.CommandOptionEnterChat(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionEnterChat(const bool &state)
{
	OAFUN_DEBUG("c41_f267");
	emit g_CommandManager.CommandOptionEnterChat(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::OptionHiddenCharacters()
{
	OAFUN_DEBUG("c41_f268");
	int result = 0;
	emit g_CommandManager.CommandOptionHiddenCharacters(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionHiddenCharacters(const int &state)
{
	OAFUN_DEBUG("c41_f269");
	emit g_CommandManager.CommandOptionHiddenCharacters(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::OptionHiddenCharactersAlpha()
{
	OAFUN_DEBUG("c41_f270");
	int result = 0;
	emit g_CommandManager.CommandOptionHiddenCharactersAlpha(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionHiddenCharactersAlpha(const int &value)
{
	OAFUN_DEBUG("c41_f271");
	emit g_CommandManager.CommandOptionHiddenCharactersAlpha(value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionHiddenCharactersModeOnlyForSelf()
{
	OAFUN_DEBUG("c41_f272");
	bool result = false;
	emit g_CommandManager.CommandOptionHiddenCharactersModeOnlyForSelf(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionHiddenCharactersModeOnlyForSelf(const bool &state)
{
	OAFUN_DEBUG("c41_f273");
	emit g_CommandManager.CommandOptionHiddenCharactersModeOnlyForSelf(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionTransparentSpellIcons()
{
	OAFUN_DEBUG("c41_f274");
	bool result = false;
	emit g_CommandManager.CommandOptionTransparentSpellIcons(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionTransparentSpellIcons(const bool &state)
{
	OAFUN_DEBUG("c41_f275");
	emit g_CommandManager.CommandOptionTransparentSpellIcons(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::OptionSpellIconsAlpha()
{
	OAFUN_DEBUG("c41_f276");
	int result = 0;
	emit g_CommandManager.CommandOptionSpellIconsAlpha(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionSpellIconsAlpha(const int &value)
{
	OAFUN_DEBUG("c41_f277");
	emit g_CommandManager.CommandOptionSpellIconsAlpha(value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OptionFastRotation()
{
	OAFUN_DEBUG("c41_f278");
	bool result = false;
	emit g_CommandManager.CommandOptionFastRotation(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OptionFastRotation(const bool &state)
{
	OAFUN_DEBUG("c41_f279");
	emit g_CommandManager.CommandOptionFastRotation(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::ClientLastTarget()
{
	OAFUN_DEBUG("c41_f280");
	QString result = "0";
	emit g_CommandManager.CommandClientLastTarget(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClientLastTarget(const QString &value)
{
	OAFUN_DEBUG("c41_f281");
	emit g_CommandManager.CommandClientLastTarget(value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::ClientLastAttack()
{
	OAFUN_DEBUG("c41_f282");
	QString result = "0";
	emit g_CommandManager.CommandClientLastAttack(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClientLastAttack(const QString &value)
{
	OAFUN_DEBUG("c41_f283");
	emit g_CommandManager.CommandClientLastAttack(value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::TargetSystemSerial()
{
	OAFUN_DEBUG("c41_f284");
	QString result = "0";
	emit g_CommandManager.CommandTargetSystemSerial(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TargetSystemSerial(const QString &value)
{
	OAFUN_DEBUG("c41_f285");
	emit g_CommandManager.CommandTargetSystemSerial(value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::GetSerial(const QString &value)
{
	OAFUN_DEBUG("c41_f286");
	QString result = value;
	emit g_CommandManager.CommandGetSerial(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddFindList()
{
	OAFUN_DEBUG("c41_f287");
	AddFindList("", "0", "0", "");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddFindList(const QString &listName, const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f288");
	AddFindList(listName, graphic, color, "");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddFindList(const QString &listName, const QString &graphic, const QString &color, const QString &comment)
{
	OAFUN_DEBUG("c41_f289");
	emit g_CommandManager.CommandAddFindList(listName, graphic, color, comment);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearFindList(const QString &listName)
{
	OAFUN_DEBUG("c41_f290");
	emit g_CommandManager.CommandClearFindList(listName);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddIgnoreList()
{
	OAFUN_DEBUG("c41_f291");
	AddIgnoreList("", "0", "0", "");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddIgnoreListObject(const QString &listName, const QString &serial)
{
	OAFUN_DEBUG("c41_f292");
	AddIgnoreListObject(listName, serial, "");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddIgnoreListObject(const QString &listName, const QString &serial, const QString &comment)
{
	OAFUN_DEBUG("c41_f293");
	emit g_CommandManager.CommandAddIgnoreList(listName, serial, comment);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddIgnoreList(const QString &listName, const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c41_f294");
	AddIgnoreList(listName, graphic, color, "");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddIgnoreList(const QString &listName, const QString &graphic, const QString &color, const QString &comment)
{
	OAFUN_DEBUG("c41_f295");
	emit g_CommandManager.CommandAddIgnoreList(listName, graphic, color, comment);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearIgnoreList(const QString &listName)
{
	OAFUN_DEBUG("c41_f296");
	emit g_CommandManager.CommandClearIgnoreList(listName);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::MenuCount()
{
	OAFUN_DEBUG("c41_f297");
	int result = 0;
	emit g_CommandManager.CommandMenuCount(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::GetMenu(const QString &name)
{
	OAFUN_DEBUG("c41_f298");
	CMenu *menu = nullptr;
	emit g_CommandManager.CommandGetMenu(name, &menu);
	PauseWaiting();

	return new COAScriptMenuObject(menu);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::SelectMenu(const QString &name, const QString &itemName)
{
	OAFUN_DEBUG("c41_f299");
	bool result = false;
	emit g_CommandManager.CommandSelectMenu(name, itemName, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CloseMenu(const QString &name)
{
	OAFUN_DEBUG("c41_f300");
	emit g_CommandManager.CommandCloseMenu(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::BuffExists(const QString &name)
{
	OAFUN_DEBUG("c41_f301");
	bool result = false;
	emit g_CommandManager.CommandBuffExists(name, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptEngine::TradeCount()
{
	OAFUN_DEBUG("c41_f302");
	int result = 0;
	emit g_CommandManager.CommandTradeCount(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::TradeContainer(const QString &index, const QString &container)
{
	OAFUN_DEBUG("c41_f303");
	QString result = "";
	int opponent = 0;

	if (container.trimmed().toLower() == "right")
		opponent = 1;
	else
		opponent = container.toInt();

	emit g_CommandManager.CommandTradeContainer(index, opponent, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::TradeOpponent(const QString &index)
{
	OAFUN_DEBUG("c41_f304");
	QString result = "";
	emit g_CommandManager.CommandTradeOpponent(index, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::TradeName(const QString &index)
{
	OAFUN_DEBUG("c41_f305");
	QString result = "";
	emit g_CommandManager.CommandTradeName(index, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::TradeCheckState(const QString &index, const QString &container)
{
	OAFUN_DEBUG("c41_f306");
	bool result = false;
	int opponent = 0;

	if (container.trimmed().toLower() == "right")
		opponent = 1;
	else
		opponent = container.toInt();

	emit g_CommandManager.CommandTradeCheckState(index, opponent, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TradeCheck(const QString &index, const bool &state)
{
	OAFUN_DEBUG("c41_f307");
	emit g_CommandManager.CommandTradeCheck(index, state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::TradeClose(const QString &index)
{
	OAFUN_DEBUG("c41_f308");
	emit g_CommandManager.CommandTradeClose(index);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::GetGraphic(const QString &value)
{
	OAFUN_DEBUG("c41_f309");
	QString result = value;
	emit g_CommandManager.CommandGetGraphic(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::GetContainer(const QString &value)
{
	OAFUN_DEBUG("c41_f310");
	QString result = value;
	emit g_CommandManager.CommandGetContainer(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::FindFriend()
{
	OAFUN_DEBUG("c41_f311");
	return FindFriend("fast", "finddistance");
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::FindFriend(const QString &flags)
{
	OAFUN_DEBUG("c41_f312");
	return FindFriend(flags, "finddistance");
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::FindFriend(const QString &flags, const QString &distance)
{
	OAFUN_DEBUG("c41_f313");
	QString result = "";
	emit g_CommandManager.CommandFindFriend(flags, distance, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::FindEnemy()
{
	OAFUN_DEBUG("c41_f314");
	return FindEnemy("fast", "finddistance");
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::FindEnemy(const QString &flags)
{
	OAFUN_DEBUG("c41_f315");
	return FindEnemy(flags, "finddistance");
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::FindEnemy(const QString &flags, const QString &distance)
{
	OAFUN_DEBUG("c41_f316");
	QString result = "";
	emit g_CommandManager.CommandFindEnemy(flags, distance, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::GetFriendList()
{
	OAFUN_DEBUG("c41_f317");
	QStringList result;
	emit g_CommandManager.CommandGetFriendList(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::GetFriendsStatus()
{
	OAFUN_DEBUG("c41_f318");
	emit g_CommandManager.CommandGetFriendsStatus();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::GetEnemyList()
{
	OAFUN_DEBUG("c41_f319");
	QStringList result;
	emit g_CommandManager.CommandGetEnemyList(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::GetEnemiesStatus()
{
	OAFUN_DEBUG("c41_f320");
	emit g_CommandManager.CommandGetEnemiesStatus();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetFontColor(const bool &state)
{
	OAFUN_DEBUG("c41_f321");
	SetFontColor(state, "0x02B2");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetFontColor(const bool &state, const QString &color)
{
	OAFUN_DEBUG("c41_f322");
	emit g_CommandManager.CommandSetFontColor(state, color);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::GetFontColor()
{
	OAFUN_DEBUG("c41_f323");
	bool result = false;
	emit g_CommandManager.CommandGetFontColor(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::GetFontColorValue()
{
	OAFUN_DEBUG("c41_f324");
	QString result = "0x02B2";
	emit g_CommandManager.CommandGetFontColorValue(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetCharactersFontColor(const bool &state)
{
	OAFUN_DEBUG("c41_f325");
	SetCharactersFontColor(state, "0x02B2");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetCharactersFontColor(const bool &state, const QString &color)
{
	OAFUN_DEBUG("c41_f326");
	emit g_CommandManager.CommandSetCharactersFontColor(state, color);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::GetCharactersFontColor()
{
	OAFUN_DEBUG("c41_f327");
	bool result = false;
	emit g_CommandManager.CommandGetCharactersFontColor(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::GetCharactersFontColorValue()
{
	OAFUN_DEBUG("c41_f328");
	QString result = "0x02B2";
	emit g_CommandManager.CommandGetCharactersFontColorValue(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddFriend(const QString &name)
{
	OAFUN_DEBUG("c41_f329");
	AddFriend(name, "");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddFriend(const QString &name, const QString &object)
{
	OAFUN_DEBUG("c41_f330");
	emit g_CommandManager.CommandAddFriend(name, object);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::RemoveFriend(const QString &name)
{
	OAFUN_DEBUG("c41_f331");
	emit g_CommandManager.CommandRemoveFriend(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearFriendList()
{
	OAFUN_DEBUG("c41_f332");
	emit g_CommandManager.CommandClearFriendList();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddEnemy(const QString &name)
{
	OAFUN_DEBUG("c41_f329");
	AddEnemy(name, "");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::AddEnemy(const QString &name, const QString &object)
{
	OAFUN_DEBUG("c41_f330");
	emit g_CommandManager.CommandAddEnemy(name, object);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::RemoveEnemy(const QString &name)
{
	OAFUN_DEBUG("c41_f331");
	emit g_CommandManager.CommandRemoveEnemy(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearEnemyList()
{
	OAFUN_DEBUG("c41_f332");
	emit g_CommandManager.CommandClearEnemyList();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetGlobal(const QString &name, const QString &value)
{
	OAFUN_DEBUG("c41_f333");
	emit g_CommandManager.CommandSetGlobal(name, value);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::GetGlobal(const QString &name)
{
	OAFUN_DEBUG("c41_f334");
	QString result = "";
	emit g_CommandManager.CommandGetGlobal(name, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearGlobals()
{
	OAFUN_DEBUG("c41_f335");
	emit g_CommandManager.CommandClearGlobals();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::InfoGump()
{
	OAFUN_DEBUG("c41_f336");
	InfoGump("lastgump");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::InfoGump(const QString &index)
{
	OAFUN_DEBUG("c41_f337");
	emit g_CommandManager.CommandInfoGump(index);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::ValidateTargetTile(const QString &tile, const int &x, const int &y)
{
	OAFUN_DEBUG("c41_f338");
	bool result = false;
	emit g_CommandManager.CommandValidateTargetTile(tile, x, y, false, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::ValidateTargetTileRelative(const QString &tile, const int &x, const int &y)
{
	OAFUN_DEBUG("c41_f339");
	bool result = false;
	emit g_CommandManager.CommandValidateTargetTile(tile, x, y, true, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Launch(const QString &filePath)
{
	OAFUN_DEBUG("c41_f340");
	Launch(filePath, QStringList());
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Launch(const QString &filePath, const QString &args)
{
	OAFUN_DEBUG("c41_f340");
	Launch(filePath, QStringList() << args);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Launch(const QString &filePath, const QStringList &args)
{
	OAFUN_DEBUG("c41_f341");

	if (!EnabledCommandLaunch())
		return;

	QProcess *proc = new QProcess();
	proc->start(filePath, args);
	proc->waitForStarted(5000);

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseAbility(const int &index)
{
	OAFUN_DEBUG("c41_f342");
	UseAbility(QString::number(index));
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseAbility(const QString &name)
{
	OAFUN_DEBUG("c41_f343");
	emit g_CommandManager.CommandUseAbility(name.toLower());
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::Contains(const QString &text, const QString &pattern)
{
	OAFUN_DEBUG("c41_f344");
	return Contains(text, pattern, false);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::Contains(QString text, QString pattern, const bool &ignoreCase)
{
	OAFUN_DEBUG("c41_f345");
	if (!text.length() || !pattern.length())
		return false;

	if (ignoreCase)
	{
		pattern = pattern.toLower();
		text = text.toLower();
	}

	QStringList patterns = pattern.split("|");

	foreach (const QString &p, patterns)
	{
		if (text.contains(p))
			return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::Split(const QString &text)
{
	OAFUN_DEBUG("c41_f346");
	return Split(text, " ", true);
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::Split(const QString &text, const QString &separator)
{
	OAFUN_DEBUG("c41_f347");
	return Split(text, separator, true);
}
//----------------------------------------------------------------------------------
QStringList COAScriptEngine::Split(const QString &text, const QString &separator, const bool &skipEmptyWorld)
{
	OAFUN_DEBUG("c41_f348");
	return text.split(separator, (skipEmptyWorld ? QString::SkipEmptyParts : QString::KeepEmptyParts));
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::LastJournalMessage()
{
	OAFUN_DEBUG("c41_f349");
	CJournalMessage *result = nullptr;
	emit g_CommandManager.CommandLastJournalMessage(&result);
	PauseWaiting();

	if (result != nullptr)
		return new COAScriptJournalObject(result);

	return nullptr;
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::GetLastTargetPosition()
{
	OAFUN_DEBUG("c41_f350");
	int x = 0;
	int y = 0;

	emit g_CommandManager.CommandGetLastTargetPosition(&x, &y);
	PauseWaiting();

	return new COAScriptPositionObject(x, y);
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::GetLastAttackPosition()
{
	OAFUN_DEBUG("c41_f351");
	int x = 0;
	int y = 0;

	emit g_CommandManager.CommandGetLastTargetPosition(&x, &y);
	PauseWaiting();

	return new COAScriptPositionObject(x, y);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseWrestlingDisarm()
{
	OAFUN_DEBUG("c41_f352");
	emit g_CommandManager.CommandUseWrestlingDisarm();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseWrestlingStun()
{
	OAFUN_DEBUG("c41_f353");
	emit g_CommandManager.CommandUseWrestlingStun();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OpenContainer(const QString &serial)
{
	OAFUN_DEBUG("c41_f354");
	return OpenContainer(serial, "600", "reach that|too away");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OpenContainer(const QString &serial, const QString &delayText)
{
	OAFUN_DEBUG("c41_f355");
	return OpenContainer(serial, delayText, "reach that|too away");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OpenContainer(QString serial, const QString &delayText, const QString &errorPattern)
{
	OAFUN_DEBUG("c41_f356");
	int delay = 0;

	emit g_CommandManager.CommandWait(delayText, &delay);

	uint startTime = GetTickCount();
	uint endTime = startTime + delay;

	emit g_CommandManager.CommandGetSerial(&serial);
	emit g_CommandManager.CommandUseObject(serial);

	uint serialID = COrionAssistant::TextToUInt(serial);

	while (GetTickCount() < endTime)
	{
		CJournalMessage *result = nullptr;
		emit g_CommandManager.CommandInJournal(errorPattern, "0", 0xFFFF, "my|sys", startTime, 0, &result);

		if (result != nullptr)
			break;

		if (g_LastContainerObject == serialID && g_World != nullptr)
		{
			CGameObject *obj = g_World->FindWorldObject(serialID);

			if (obj != nullptr && obj->m_Items != nullptr)
				break;
		}

		Sleep(10);
		PauseWaiting();
	}

	return (g_LastContainerObject == serialID);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::ObjectExists(const QString &serial)
{
	OAFUN_DEBUG("c41_f357");
	bool result = false;
	emit g_CommandManager.CommandObjectGetExists(serial, &result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::InfoMenu(const QString &index)
{
	OAFUN_DEBUG("c41_f358");
	emit g_CommandManager.CommandInfoMenu(index);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::OAVersion()
{
	OAFUN_DEBUG("c41_f359");
	return APP_VERSION;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::Connected()
{
	OAFUN_DEBUG("c41_f360");
	return g_Connected;
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::Time()
{
	OAFUN_DEBUG("c41_f361");
	return Time("hh:mm:ss.zzz");
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::Time(const QString &format)
{
	OAFUN_DEBUG("c41_f362");

	return QDateTime(QDate::currentDate(), QTime::currentTime()).toString(format);
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::Date()
{
	OAFUN_DEBUG("c41_f363");

	return Date("dd.MM.yyyy");
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::Date(const QString &format)
{
	OAFUN_DEBUG("c41_f364");

	return QDateTime(QDate::currentDate(), QTime::currentTime()).toString(format);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Random()
{
	OAFUN_DEBUG("c41_f365");
	return Random(INT_MAX);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Random(const int &value)
{
	OAFUN_DEBUG("c41_f366");
	return (qrand() % value);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Random(const int &minValue, const int &maxValue)
{
	OAFUN_DEBUG("c41_f367");
	PauseWaiting();
	int value = maxValue - minValue;

	if (!value)
		return 0;

	return minValue + (qrand() % value);
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::RequestName(const QString &serial)
{
	OAFUN_DEBUG("c41_f368");
	return RequestName(serial, "200");
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::RequestName(QString serial, const QString &delayText)
{
	OAFUN_DEBUG("c41_f369");

	emit g_CommandManager.CommandGetSerial(&serial);
	uint serialID = COrionAssistant::TextToUInt(serial);

	QString result = "";
	emit g_CommandManager.CommandObjectGetName(serialID, &result);

	if (result.length())
		return result;

	int delay = 0;

	emit g_CommandManager.CommandWait(delayText, &delay);

	uint startTime = GetTickCount();
	uint endTime = startTime + delay;

	emit g_CommandManager.CommandClick(serial);

	while (GetTickCount() < endTime && !result.length())
	{
		emit g_CommandManager.CommandObjectGetName(serialID, &result);

		Sleep(10);
		PauseWaiting();
	}

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::InvokeVirture(const QString &name)
{
	OAFUN_DEBUG("c41_f370");
	emit g_CommandManager.CommandInvokeVirture(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::JournalCount()
{
	OAFUN_DEBUG("c41_f371");
	int result = 0;
	emit g_CommandManager.CommandJournalCount(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::JournalLine(const int &index)
{
	OAFUN_DEBUG("c41_f372");
	CJournalMessage *result = nullptr;
	emit g_CommandManager.CommandJournalLine(index, &result);
	PauseWaiting();

	if (result != nullptr)
		return new COAScriptJournalObject(result);

	return nullptr;
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::CreateGumpHook(QString name)
{
	OAFUN_DEBUG("c41_f373");

	name = name.trimmed().toLower();

	if (name == "cancel")
		name = "0";

	return CreateGumpHook(name.toInt());
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::CreateGumpHook(const int &index)
{
	OAFUN_DEBUG("c41_f373.1");
	PauseWaiting();
	return new COAScriptGumpHookObject(index);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitGump(QObject *hook)
{
	OAFUN_DEBUG("c41_f374");

	if (hook != nullptr)
		emit g_CommandManager.CommandWaitGump(((COAScriptGumpHookObject*)hook)->GetHook());

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CancelWaitGump()
{
	OAFUN_DEBUG("c41_f375");
	PauseWaiting();
	emit g_CommandManager.CommandCancelWaitGump();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::GumpCount()
{
	OAFUN_DEBUG("c41_f376");
	int result = 0;
	emit g_CommandManager.CommandGumpCount(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::GetLastGump()
{
	OAFUN_DEBUG("c41_f377");
	return GetGump(-1);
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::GetGump(const int &index)
{
	OAFUN_DEBUG("c41_f378");
	CGump *result = nullptr;
	emit g_CommandManager.CommandGetGump(index, &result);
	PauseWaiting();

	if (result != nullptr)
		return new COAScriptGumpObject(result);

	return nullptr;
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::GetGump(const int &serial, const int &id)
{
	OAFUN_DEBUG("c41_f379");
	CGump *result = nullptr;
	emit g_CommandManager.CommandGetGump(serial, id, &result);
	PauseWaiting();

	if (result != nullptr)
		return new COAScriptGumpObject(result);

	return nullptr;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::PlayWav(const QString &fileName)
{
	OAFUN_DEBUG("c41_f380");

	sndPlaySound(fileName.toStdWString().c_str(), SND_ASYNC);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::GetProfile(const QString &serial)
{
	OAFUN_DEBUG("c41_f381");
	return GetProfile(serial, "300", "reach that|too away");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::GetProfile(const QString &serial, const QString &delayText)
{
	OAFUN_DEBUG("c41_f382");
	return GetProfile(serial, delayText, "reach that|too away");
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::GetProfile(QString serial, const QString &delayText, const QString &errorPattern)
{
	OAFUN_DEBUG("c41_f383");

	int delay = 0;

	emit g_CommandManager.CommandWait(delayText, &delay);

	uint startTime = GetTickCount();
	uint endTime = startTime + delay;

	emit g_CommandManager.CommandGetSerial(&serial);
	uint serialID = COrionAssistant::TextToUInt(serial);

	emit g_CommandManager.CommandRequestProfile(serialID);

	while (GetTickCount() < endTime)
	{
		CJournalMessage *result = nullptr;
		emit g_CommandManager.CommandInJournal(errorPattern, "0", 0xFFFF, "my|sys", startTime, 0, &result);

		if (result != nullptr)
			break;

		if (g_World != nullptr)
		{
			CGameCharacter *obj = g_World->FindWorldCharacter(serialID);

			if (obj != nullptr && obj->GetProfileReceived())
				break;
		}

		Sleep(10);
		PauseWaiting();
	}

	return false;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Screenshot()
{
	OAFUN_DEBUG("c41_f384");

	PostMessageA(g_ClientHandle, WM_KEYUP, 0x2C, 0);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitPrompt(const QString &text)
{
	OAFUN_DEBUG("c41_f385");
	WaitPrompt(text, "0", "all");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitPrompt(const QString &text, const QString &serial)
{
	OAFUN_DEBUG("c41_f386");
	WaitPrompt(text, serial, "all");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitPrompt(const QString &text, const QString &serial, const QString &type)
{
	OAFUN_DEBUG("c41_f387");

	emit g_CommandManager.CommandWaitPrompt(text, serial, type.toLower());

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CancelWaitPrompt()
{
	OAFUN_DEBUG("c41_f388");

	emit g_CommandManager.CommandCancelWaitPrompt();

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Buy(const QString &shopListName)
{
	OAFUN_DEBUG("c41_f389");
	Buy(shopListName, "", 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Buy(const QString &shopListName, const QString &vendorName)
{
	OAFUN_DEBUG("c41_f390");
	Buy(shopListName, vendorName, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Buy(const QString &shopListName, const QString &vendorName, const int &shopDelay)
{
	OAFUN_DEBUG("c41_f391");

	emit g_CommandManager.CommandShop(shopListName, true, vendorName, shopDelay);

	bool shopping = true;

	while (shopping)
	{
		Sleep(10);

		emit g_CommandManager.CommandIsShopping(&shopping);

		PauseWaiting();
	}
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Sell(const QString &shopListName)
{
	OAFUN_DEBUG("c41_f392");
	Sell(shopListName, "", 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Sell(const QString &shopListName, const QString &vendorName)
{
	OAFUN_DEBUG("c41_f393");
	Sell(shopListName, vendorName, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::Sell(const QString &shopListName, const QString &vendorName, const int &shopDelay)
{
	OAFUN_DEBUG("c41_f394");

	emit g_CommandManager.CommandShop(shopListName, false, vendorName, shopDelay);

	bool shopping = true;

	while (shopping)
	{
		Sleep(10);

		emit g_CommandManager.CommandIsShopping(&shopping);

		PauseWaiting();
	}
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ShowStatusbar(const QString &serial, const int &x, const int &y)
{
	OAFUN_DEBUG("c41_f395");
	ShowStatusbar(serial, x, y, true);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ShowStatusbar(const QString &serial, const int &x, const int &y, const bool &minimized)
{
	OAFUN_DEBUG("c41_f396");

	emit g_CommandManager.CommandShowStatusbar(serial, x, y, minimized);

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CloseStatusbar(const QString &serial)
{
	OAFUN_DEBUG("c41_f397");

	emit g_CommandManager.CommandCloseStatusbar(serial);

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::LogOut()
{
	OAFUN_DEBUG("c41_f398");

	emit g_CommandManager.CommandLogOut();

	PauseWaiting();
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::CreateClientMacro()
{
	OAFUN_DEBUG("c41_f399");
	PauseWaiting();
	return new COAClientMacroEngine();
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::CreateClientMacro(const QString &action)
{
	OAFUN_DEBUG("c41_f400");
	return CreateClientMacro(action, "");
}
//----------------------------------------------------------------------------------
QObject *COAScriptEngine::CreateClientMacro(const QString &action, const QString &subAction)
{
	OAFUN_DEBUG("c41_f401");

	COAClientMacroEngine *macro = new COAClientMacroEngine();
	macro->AddAction(action, subAction);

	PauseWaiting();

	return macro;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::TimerExists(const QString &name)
{
	OAFUN_DEBUG("c41_f402");
	bool result = false;

	emit g_CommandManager.CommandTimerExists(name.toLower(), &result);

	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetTimer(const QString &name)
{
	OAFUN_DEBUG("c41_f403");
	SetTimer(name, 0);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SetTimer(const QString &name, const int &value)
{
	OAFUN_DEBUG("c41_f404");

	emit g_CommandManager.CommandSetTimer(name.toLower(), value);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::Timer(const QString &name)
{
	OAFUN_DEBUG("c41_f405");
	int result = 0;

	emit g_CommandManager.CommandTimer(name.toLower(), &result);

	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::RemoveTimer(const QString &name)
{
	OAFUN_DEBUG("c41_f406");

	emit g_CommandManager.CommandRemoveTimer(name.toLower());

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClearTimers()
{
	OAFUN_DEBUG("c41_f407");

	emit g_CommandManager.CommandClearTimers();

	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CancelTarget()
{
	OAFUN_DEBUG("c41_f408");

	emit g_CommandManager.CommandCancelTarget();

	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::PromptExists()
{
	OAFUN_DEBUG("c41_f409");
	bool result = false;

	emit g_CommandManager.CommandPromptExists(&result);

	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::PromptSerial()
{
	OAFUN_DEBUG("c41_f410");

	int result = 0;

	emit g_CommandManager.CommandPromptSerial(&result);

	PauseWaiting();

	return COrionAssistant::SerialToText(result);
}
//----------------------------------------------------------------------------------
QString COAScriptEngine::PromptID()
{
	OAFUN_DEBUG("c41_f411");

	int result = 0;

	emit g_CommandManager.CommandPromptID(&result);

	PauseWaiting();

	return COrionAssistant::SerialToText(result);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SendPrompt(const QString &text)
{
	OAFUN_DEBUG("c41_f412");

	emit g_CommandManager.CommandSendPrompt(text);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OpenPaperdoll(const QString &serial)
{
	OAFUN_DEBUG("c41_f413");

	emit g_CommandManager.CommandOpenPaperdoll(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClosePaperdoll(const QString &serial)
{
	OAFUN_DEBUG("c41_f414");

	emit g_CommandManager.CommandClosePaperdoll(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::MovePaperdoll(const QString &serial, const int &x, const int &y)
{
	OAFUN_DEBUG("c41_f415");

	emit g_CommandManager.CommandMovePaperdoll(serial, x, y);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::RequestContextMenu(const QString &serial)
{
	OAFUN_DEBUG("c41_f416");

	emit g_CommandManager.CommandRequestContextMenu(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::WaitContextMenu(const QString &serial, const int &index)
{
	OAFUN_DEBUG("c41_f417");

	emit g_CommandManager.CommandWaitContextMenu(serial, index);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CancelContextMenu()
{
	OAFUN_DEBUG("c41_f418");

	emit g_CommandManager.CommandCancelContextMenu();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SayYell(const QString &text)
{
	OAFUN_DEBUG("c41_f419");
	if (text.length())
		Say("! " + text);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SayWhisper(const QString &text)
{
	OAFUN_DEBUG("c41_f420");
	if (text.length())
		Say("; " + text);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SayEmote(const QString &text)
{
	OAFUN_DEBUG("c41_f421");
	if (text.length())
		Say(": " + text);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SayBroadcast(const QString &text)
{
	OAFUN_DEBUG("c41_f422");
	if (text.length())
		Say("? " + text);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SayParty(const QString &text)
{
	OAFUN_DEBUG("c41_f423");
	if (text.length())
		Say("/ " + text);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SayGuild(const QString &text)
{
	OAFUN_DEBUG("c41_f424");
	if (text.length())
		Say("\\ " + text);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::SayAlliance(const QString &text)
{
	OAFUN_DEBUG("c41_f425");
	if (text.length())
		Say("| " + text);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::BandageTarget(const QString &serial)
{
	OAFUN_DEBUG("c41_f426");

	emit g_CommandManager.CommandBandageTarget(serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CastTarget(const QString &name,const QString &serial)
{
	OAFUN_DEBUG("c41_f427");

	emit g_CommandManager.CommandCastTarget(name, serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CastTarget(const int &index, const QString &serial)
{
	OAFUN_DEBUG("c41_f428");
	CastTarget(QString::number(index), serial);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseSkillTarget(const QString &name,const QString &serial)
{
	OAFUN_DEBUG("c41_f429");

	emit g_CommandManager.CommandUseSkillTarget(name, serial);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::UseSkillTarget(const int &index, const QString &serial)
{
	OAFUN_DEBUG("c41_f430");
	UseSkillTarget(QString::number(index), serial);
}
//----------------------------------------------------------------------------------
int COAScriptEngine::ClientViewRange()
{
	OAFUN_DEBUG("c41_f431");
	int range = 0;
	emit g_CommandManager.CommandClientViewRange(&range);
	PauseWaiting();

	return range;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ClientViewRange(const int &range)
{
	OAFUN_DEBUG("c41_f432");
	emit g_CommandManager.CommandClientViewRange(range);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OpenOrionMap()
{
	OAFUN_DEBUG("c41_f433");
	emit g_CommandManager.CommandOpenOrionMap();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OpenOrionMap(const int &x, const int &y)
{
	OAFUN_DEBUG("c41_f434");
	OpenOrionMap();
	MoveOrionMap(x, y);
}
//----------------------------------------------------------------------------------
void COAScriptEngine::MoveOrionMap(const int &x, const int &y)
{
	OAFUN_DEBUG("c41_f435");
	emit g_CommandManager.CommandMoveOrionMap(x, y);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::CloseOrionMap()
{
	OAFUN_DEBUG("c41_f436");
	emit g_CommandManager.CommandCloseOrionMap();
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OpenEnhancedMap()
{
	OAFUN_DEBUG("c41_f437");
	OpenEnhancedMap(g_ClientPath + "/Map/EnhancedMap.exe");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OpenEnhancedMap(const QString &filePath)
{
	OAFUN_DEBUG("c41_f438");

	emit g_CommandManager.CommandPrint(0xFFFF, "Opening Enhanced map from: " + filePath);

	QProcess *proc = new QProcess();
	proc->start(filePath);
	proc->waitForStarted(5000);

	PauseWaiting();
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForTarget()
{
	OAFUN_DEBUG("c41_f439");
	return WaitForTarget(1000);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForTarget(const int &maxDelay)
{
	OAFUN_DEBUG("c41_f440");
	if (g_Target.IsTargeting())
		return true;

	g_TargetReceived = false;
	uint timer = GetTickCount() + maxDelay;

	while (!g_TargetReceived && timer > GetTickCount())
		Sleep(10);

	PauseWaiting();

	return g_TargetReceived;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForMenu()
{
	OAFUN_DEBUG("c41_f441");
	return WaitForMenu(1000);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForMenu(const int &maxDelay)
{
	OAFUN_DEBUG("c41_f442");
	g_MenuReceived = false;
	uint timer = GetTickCount() + maxDelay;

	while (!g_MenuReceived && timer > GetTickCount())
		Sleep(10);

	PauseWaiting();

	return g_MenuReceived;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForGump()
{
	OAFUN_DEBUG("c41_f443");
	return WaitForGump(1000);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForGump(const int &maxDelay)
{
	OAFUN_DEBUG("c41_f443");
	g_GumpReceived = false;
	uint timer = GetTickCount() + maxDelay;

	while (!g_GumpReceived && timer > GetTickCount())
		Sleep(10);

	PauseWaiting();

	return g_GumpReceived;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForPrompt()
{
	OAFUN_DEBUG("c41_f444");
	return WaitForPrompt(1000);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForPrompt(const int &maxDelay)
{
	OAFUN_DEBUG("c41_f445");
	g_PromptReceived = false;
	uint timer = GetTickCount() + maxDelay;

	while (!g_PromptReceived && timer > GetTickCount())
		Sleep(10);

	PauseWaiting();

	return g_PromptReceived;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForShop()
{
	OAFUN_DEBUG("c41_f446");
	return WaitForShop(1000);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForShop(const int &maxDelay)
{
	OAFUN_DEBUG("c41_f447");
	g_ShopReceived = false;
	uint timer = GetTickCount() + maxDelay;

	while (!g_ShopReceived && timer > GetTickCount())
		Sleep(10);

	PauseWaiting();

	return g_ShopReceived;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForTrade()
{
	OAFUN_DEBUG("c41_f448");
	return WaitForTrade(1000);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForTrade(const int &maxDelay)
{
	OAFUN_DEBUG("c41_f449");
	g_TradeReceived = false;
	uint timer = GetTickCount() + maxDelay;

	while (!g_TradeReceived && timer > GetTickCount())
		Sleep(10);

	PauseWaiting();

	return g_TradeReceived;
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForContextMenu()
{
	OAFUN_DEBUG("c41_f450");
	return WaitForContextMenu(1000);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::WaitForContextMenu(const int &maxDelay)
{
	OAFUN_DEBUG("c41_f451");
	g_ContextMenuReceived = false;
	uint timer = GetTickCount() + maxDelay;

	while (!g_ContextMenuReceived && timer > GetTickCount())
		Sleep(10);

	PauseWaiting();

	return g_ContextMenuReceived;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::LoadProfile(const QString &name)
{
	OAFUN_DEBUG("c41_f452");
	emit g_CommandManager.CommandLoadProfile(name);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ActivateClient()
{
	OAFUN_DEBUG("c41_f453");
	SetForegroundWindow(g_ClientHandle);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ShutdownWindows()
{
	OAFUN_DEBUG("c41_f454");
	ShutdownWindows("");
}
//----------------------------------------------------------------------------------
void COAScriptEngine::ShutdownWindows(const QString &mode)
{
	OAFUN_DEBUG("c41_f455");

	HANDLE hProc = GetCurrentProcess();
	HANDLE hToken = NULL;
	TOKEN_PRIVILEGES priv;
	TOKEN_PRIVILEGES newPriv;

	if (OpenProcessToken(hProc, TOKEN_ADJUST_PRIVILEGES + TOKEN_QUERY, &hToken) && LookupPrivilegeValue(NULL, L"SeShutdownPrivilege", &priv.Privileges[0].Luid))
	{
		DWORD zero = 0;
		priv.PrivilegeCount = 1;
		priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		AdjustTokenPrivileges(hToken, false, &priv, sizeof(TOKEN_PRIVILEGES), &newPriv, &zero);
	}

	uint flags = EWX_SHUTDOWN;

	if (mode.toLower() == "forced")
		flags |= EWX_FORCE;

	ExitWindowsEx(flags, 0xffffffff);
}
//----------------------------------------------------------------------------------
bool COAScriptEngine::OnOffHotkeys()
{
	OAFUN_DEBUG("c41_f456");
	bool result = false;
	emit g_CommandManager.CommandOnOffHotkeys(&result);
	PauseWaiting();

	return result;
}
//----------------------------------------------------------------------------------
void COAScriptEngine::OnOffHotkeys(const bool &state)
{
	OAFUN_DEBUG("c41_f457");
	emit g_CommandManager.CommandOnOffHotkeys(state);
	PauseWaiting();
}
//----------------------------------------------------------------------------------
int COAScriptEngine::GetTargetType()
{
	OAFUN_DEBUG("c41_f458");
	int result = 0;
	emit g_CommandManager.CommandHaveTarget(&result);

	return result;
}
//----------------------------------------------------------------------------------
