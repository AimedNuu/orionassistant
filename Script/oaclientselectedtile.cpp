// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAClientSelectedTile.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oaclientselectedtile.h"
#include "../OrionAssistant/orionassistant.h"
//----------------------------------------------------------------------------------
COAClientSelectedTile::COAClientSelectedTile()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
QString COAClientSelectedTile::Serial()
{
	OAFUN_DEBUG("coacst_f1");
	return COrionAssistant::SerialToText(g_SelectedClientWorldObject.Serial);
}
//----------------------------------------------------------------------------------
QString COAClientSelectedTile::Graphic()
{
	OAFUN_DEBUG("coacst_f2");
	return COrionAssistant::GraphicToText(g_SelectedClientWorldObject.Graphic);
}
//----------------------------------------------------------------------------------
QString COAClientSelectedTile::Color()
{
	OAFUN_DEBUG("coacst_f3");
	return COrionAssistant::GraphicToText(g_SelectedClientWorldObject.Color);
}
//----------------------------------------------------------------------------------
int COAClientSelectedTile::X()
{
	OAFUN_DEBUG("coacst_f4");
	return g_SelectedClientWorldObject.X;
}
//----------------------------------------------------------------------------------
int COAClientSelectedTile::Y()
{
	OAFUN_DEBUG("coacst_f5");
	return g_SelectedClientWorldObject.Y;
}
//----------------------------------------------------------------------------------
int COAClientSelectedTile::Z()
{
	OAFUN_DEBUG("coacst_f6");
	return g_SelectedClientWorldObject.Z;
}
//----------------------------------------------------------------------------------
QString COAClientSelectedTile::LandGraphic()
{
	OAFUN_DEBUG("coacst_f7");
	return COrionAssistant::GraphicToText(g_SelectedClientWorldObject.LandGraphic);
}
//----------------------------------------------------------------------------------
int COAClientSelectedTile::LandX()
{
	OAFUN_DEBUG("coacst_f8");
	return g_SelectedClientWorldObject.LandX;
}
//----------------------------------------------------------------------------------
int COAClientSelectedTile::LandY()
{
	OAFUN_DEBUG("coacst_f9");
	return g_SelectedClientWorldObject.LandY;
}
//----------------------------------------------------------------------------------
int COAClientSelectedTile::LandZ()
{
	OAFUN_DEBUG("coacst_f10");
	return g_SelectedClientWorldObject.LandZ;
}
//----------------------------------------------------------------------------------
bool COAClientSelectedTile::IsLandTile()
{
	OAFUN_DEBUG("coacst_f11");
	return (g_SelectedClientWorldObject.Serial == 0xFFFFFFFF);
}
//----------------------------------------------------------------------------------
bool COAClientSelectedTile::IsStaticTile()
{
	OAFUN_DEBUG("coacst_f12");
	return (g_SelectedClientWorldObject.Serial == 0);
}
//----------------------------------------------------------------------------------
bool COAClientSelectedTile::IsGameObject()
{
	OAFUN_DEBUG("coacst_f13");
	return (g_SelectedClientWorldObject.Serial && g_SelectedClientWorldObject.Serial != 0xFFFFFFFF);
}
//----------------------------------------------------------------------------------
