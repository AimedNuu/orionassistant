/***********************************************************************************
**
** OAMenuEngine.h
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OAMENUENGINE_H
#define OAMENUENGINE_H
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
#include "../Managers/menumanager.h"
//----------------------------------------------------------------------------------
class COAScriptMenuObject : public QObject
{
	Q_OBJECT

private:
	CMenu m_Menu;

public:
	COAScriptMenuObject(CMenu *menu);
	virtual ~COAScriptMenuObject() {}

	Q_INVOKABLE QString Serial();

	Q_INVOKABLE QString ID();

	Q_INVOKABLE QString Name();

	Q_INVOKABLE bool IsGrayMenu();

	Q_INVOKABLE bool Select(const int &index);

	Q_INVOKABLE bool Select(const QString &name);

	Q_INVOKABLE void Close();

	Q_INVOKABLE int ItemsCount();

	Q_INVOKABLE int ItemID(const int &index);

	Q_INVOKABLE QString ItemGraphic(const int &index);

	Q_INVOKABLE QString ItemColor(const int &index);

	Q_INVOKABLE QString ItemName(const int &index);
};
//----------------------------------------------------------------------------------
#endif // OAMENUENGINE_H
//----------------------------------------------------------------------------------
