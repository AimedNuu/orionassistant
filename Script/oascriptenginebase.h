/***********************************************************************************
**
** OAScriptEngineBase.h
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OASCRIPTENGINEBASE_H
#define OASCRIPTENGINEBASE_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class COAScriptEngineBase : public QObject
{
	Q_OBJECT

	SETGET(bool, Paused, false)
	SETGET(bool, Terminated, false)

protected:
	void PauseWaiting();

public:
	COAScriptEngineBase();
	virtual ~COAScriptEngineBase() {}

};
//----------------------------------------------------------------------------------
#endif // OASCRIPTENGINEBASE_H
//----------------------------------------------------------------------------------
