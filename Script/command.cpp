// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** Command.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "command.h"
//----------------------------------------------------------------------------------
CCommand::CCommand()
{
}
//----------------------------------------------------------------------------------
CCommand::CCommand(const bool &enabled, const uint64 &flags, const CMD_HANDLER &handler, const QString &description)
: m_Enabled(enabled), m_Description(description), m_Flags(flags), Handler(handler)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CCommand::~CCommand()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
