/***********************************************************************************
**
** OAGumpHookEngine.h
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OAGUMPHOOKENGINE_H
#define OAGUMPHOOKENGINE_H
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
#include "../Managers/gumpmanager.h"
//----------------------------------------------------------------------------------
class COAScriptGumpHookObject : public QObject
{
	Q_OBJECT

private:
	CGumpHook m_Hook;

public:
	COAScriptGumpHookObject(const int &index);
	virtual ~COAScriptGumpHookObject() {}

	CGumpHook *GetHook() { return &m_Hook; }

	Q_INVOKABLE int Index();

	Q_INVOKABLE void AddEntry(const int &index, const QString &text);

	Q_INVOKABLE void AddCheck(const int &index, const bool &state);
};
//----------------------------------------------------------------------------------
#endif // OAGUMPHOOKENGINE_H
//----------------------------------------------------------------------------------
