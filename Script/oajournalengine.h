/***********************************************************************************
**
** OAJournalEngine.h
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OAJOURNALENGINE_H
#define OAJOURNALENGINE_H
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
#include "../CommonItems/journalmessage.h"
//----------------------------------------------------------------------------------
class COAScriptJournalObject : public QObject
{
	Q_OBJECT

private:
	CJournalMessage *m_Parent{ nullptr };
	CJournalMessage m_Message{ CJournalMessage() };

public:
	COAScriptJournalObject(CJournalMessage *msg);
	virtual ~COAScriptJournalObject() {}

	Q_INVOKABLE QString Serial();

	Q_INVOKABLE int Timer();

	Q_INVOKABLE QString Color();

	Q_INVOKABLE QString Text();

	Q_INVOKABLE uchar Flags();

	Q_INVOKABLE int FindTextID();

	Q_INVOKABLE void SetText(const QString &newText);
};
//----------------------------------------------------------------------------------
#endif // OAJOURNALENGINE_H
//----------------------------------------------------------------------------------
