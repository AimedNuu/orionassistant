/***********************************************************************************
**
** OAClientSelectedTile.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OACLIENTSELECTEDTILE_H
#define OACLIENTSELECTEDTILE_H
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class COAClientSelectedTile : public QObject
{
	Q_OBJECT

public:
	COAClientSelectedTile();

	Q_INVOKABLE QString Serial();

	Q_INVOKABLE QString Graphic();

	Q_INVOKABLE QString Color();

	Q_INVOKABLE int X();

	Q_INVOKABLE int Y();

	Q_INVOKABLE int Z();

	Q_INVOKABLE QString LandGraphic();

	Q_INVOKABLE int LandX();

	Q_INVOKABLE int LandY();

	Q_INVOKABLE int LandZ();

	Q_INVOKABLE bool IsLandTile();

	Q_INVOKABLE bool IsStaticTile();

	Q_INVOKABLE bool IsGameObject();
};
//----------------------------------------------------------------------------------
#endif // OACLIENTSELECTEDTILE_H
//----------------------------------------------------------------------------------
