// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OAGameObjectEngine.cpp
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "oagameobjectengine.h"
#include "../GameObjects/GameWorld.h"
#include "../OrionAssistant/orionassistant.h"
#include "../Managers/commandmanager.h"
//----------------------------------------------------------------------------------
int COAScriptPositionObject::X()
{
	return m_X;
}
//----------------------------------------------------------------------------------
int COAScriptPositionObject::Y()
{
	return m_Y;
}
//----------------------------------------------------------------------------------
COAScriptGameObject::COAScriptGameObject(const uint &serial)
: QObject(), m_Serial(serial)
{
	OAFUN_DEBUG("c34_f1");
}
//----------------------------------------------------------------------------------
QString COAScriptGameObject::Serial()
{
	OAFUN_DEBUG("c34_f2");
	return COrionAssistant::SerialToText(m_Serial);
}
//----------------------------------------------------------------------------------
QString COAScriptGameObject::Graphic()
{
	OAFUN_DEBUG("c34_f3");
	QString result = "0";
	emit g_CommandManager.CommandObjectGetGraphic(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptGameObject::Color()
{
	OAFUN_DEBUG("c34_f4");
	QString result = "0";
	emit g_CommandManager.CommandObjectGetColor(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::X()
{
	OAFUN_DEBUG("c34_f5");
	int result = 0;
	emit g_CommandManager.CommandObjectGetX(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Y()
{
	OAFUN_DEBUG("c34_f6");
	int result = 0;
	emit g_CommandManager.CommandObjectGetY(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Z()
{
	OAFUN_DEBUG("c34_f7");
	int result = 0;
	emit g_CommandManager.CommandObjectGetZ(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptGameObject::Container()
{
	OAFUN_DEBUG("c34_f8");
	QString result = "0";
	emit g_CommandManager.CommandObjectGetContainer(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Map()
{
	OAFUN_DEBUG("c34_f9");
	int result = 0;
	emit g_CommandManager.CommandObjectGetMap(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Count()
{
	OAFUN_DEBUG("c34_f10");
	int result = 0;
	emit g_CommandManager.CommandObjectGetCount(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Flags()
{
	OAFUN_DEBUG("c34_f11");
	int result = 0;
	emit g_CommandManager.CommandObjectGetFlags(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptGameObject::Name()
{
	OAFUN_DEBUG("c34_f12");
	QString result = "";
	emit g_CommandManager.CommandObjectGetName(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::Mobile()
{
	OAFUN_DEBUG("c34_f13");
	bool result = false;
	emit g_CommandManager.CommandObjectGetMobile(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::Ignored()
{
	OAFUN_DEBUG("c34_f14");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIgnored(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::Frozen()
{
	OAFUN_DEBUG("c34_f15");
	bool result = false;
	emit g_CommandManager.CommandObjectGetFrozen(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::Poisoned()
{
	OAFUN_DEBUG("c34_f16");
	bool result = false;
	emit g_CommandManager.CommandObjectGetPoisoned(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::Flying()
{
	OAFUN_DEBUG("c34_f17");
	bool result = false;
	emit g_CommandManager.CommandObjectGetFlying(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::YellowHits()
{
	OAFUN_DEBUG("c34_f18");
	bool result = false;
	emit g_CommandManager.CommandObjectGetYellowHits(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::IgnoreCharacters()
{
	OAFUN_DEBUG("c34_f19");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIgnoreCharacters(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::Locked()
{
	OAFUN_DEBUG("c34_f20");
	bool result = false;
	emit g_CommandManager.CommandObjectGetLocked(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::WarMode()
{
	OAFUN_DEBUG("c34_f21");
	bool result = false;
	emit g_CommandManager.CommandObjectGetWarMode(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::Hidden()
{
	OAFUN_DEBUG("c34_f22");
	bool result = false;
	emit g_CommandManager.CommandObjectGetHidden(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::IsHuman()
{
	OAFUN_DEBUG("c34_f23");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIsHuman(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::IsPlayer()
{
	OAFUN_DEBUG("c34_f24");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIsPlayer(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::IsCorpse()
{
	OAFUN_DEBUG("c34_f25");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIsCorpse(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Layer()
{
	OAFUN_DEBUG("c34_f26");
	int result = 0;
	emit g_CommandManager.CommandObjectGetLayer(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::IsMulti()
{
	OAFUN_DEBUG("c34_f27");
	bool result = false;
	emit g_CommandManager.CommandObjectGetIsMulti(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::EquipLayer()
{
	OAFUN_DEBUG("c34_f28");
	int result = 0;
	emit g_CommandManager.CommandObjectGetEquipLayer(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Hits()
{
	OAFUN_DEBUG("c34_f29");
	return Hits("");
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Hits(const QString &args)
{
	OAFUN_DEBUG("c34_f30");
	int value = 0;
	emit g_CommandManager.CommandObjectGetHits(m_Serial, &value);

	if (args == "%")
	{
		int maxValue = 0;
		emit g_CommandManager.CommandObjectGetMaxHits(m_Serial, &maxValue);

		if (!maxValue)
			return 0;

		return (value * 100) / maxValue;
	}

	return value;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::MaxHits()
{
	OAFUN_DEBUG("c34_f31");
	int result = 0;
	emit g_CommandManager.CommandObjectGetMaxHits(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Mana()
{
	OAFUN_DEBUG("c34_f32");
	return Mana("");
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Mana(const QString &args)
{
	OAFUN_DEBUG("c34_f33");
	int value = 0;
	emit g_CommandManager.CommandObjectGetMana(m_Serial, &value);

	if (args == "%")
	{
		int maxValue = 0;
		emit g_CommandManager.CommandObjectGetMaxMana(m_Serial, &maxValue);

		if (!maxValue)
			return 0;

		return (value * 100) / maxValue;
	}

	return value;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::MaxMana()
{
	OAFUN_DEBUG("c34_f34");
	int result = 0;
	emit g_CommandManager.CommandObjectGetMaxMana(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Stam()
{
	OAFUN_DEBUG("c34_f35");
	return Stam("");
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Stam(const QString &args)
{
	OAFUN_DEBUG("c34_f36");
	int value = 0;
	emit g_CommandManager.CommandObjectGetStam(m_Serial, &value);

	if (args == "%")
	{
		int maxValue = 0;
		emit g_CommandManager.CommandObjectGetMaxStam(m_Serial, &maxValue);

		if (!maxValue)
			return 0;

		return (value * 100) / maxValue;
	}

	return value;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::MaxStam()
{
	OAFUN_DEBUG("c34_f37");
	int result = 0;
	emit g_CommandManager.CommandObjectGetMaxStam(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::Female()
{
	OAFUN_DEBUG("c34_f38");
	bool result = false;
	emit g_CommandManager.CommandObjectGetFemale(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Race()
{
	OAFUN_DEBUG("c34_f39");
	int result = 0;
	emit g_CommandManager.CommandObjectGetRace(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Direction()
{
	OAFUN_DEBUG("c34_f40");
	int result = 0;
	emit g_CommandManager.CommandObjectGetDirection(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
int COAScriptGameObject::Notoriety()
{
	OAFUN_DEBUG("c34_f41");
	int result = 0;
	emit g_CommandManager.CommandObjectGetNotoriety(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::CanChangeName()
{
	OAFUN_DEBUG("c34_f42");
	bool result = false;
	emit g_CommandManager.CommandObjectGetCanChangeName(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::Dead()
{
	OAFUN_DEBUG("c34_f43");
	bool result = false;
	emit g_CommandManager.CommandObjectGetDead(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::Exists()
{
	OAFUN_DEBUG("c34_f44");
	bool result = false;
	emit g_CommandManager.CommandObjectGetExists(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptGameObject::Properties()
{
	OAFUN_DEBUG("c34_f45");
	QString result = "";
	emit g_CommandManager.CommandObjectGetProperties(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
bool COAScriptGameObject::ProfileReceived()
{
	OAFUN_DEBUG("c34_f44");
	bool result = false;
	emit g_CommandManager.CommandObjectGetProfileReceived(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptGameObject::Profile()
{
	OAFUN_DEBUG("c34_f45");
	QString result = "";
	emit g_CommandManager.CommandObjectGetProfile(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
QString COAScriptGameObject::Title()
{
	OAFUN_DEBUG("c34_f45");
	QString result = "";
	emit g_CommandManager.CommandObjectGetTitle(m_Serial, &result);

	return result;
}
//----------------------------------------------------------------------------------
