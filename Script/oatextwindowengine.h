/***********************************************************************************
**
** OATextWindowEngine.h
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OATEXTWINDOWENGINE_H
#define OATEXTWINDOWENGINE_H
//----------------------------------------------------------------------------------
#include "oascriptenginebase.h"
//----------------------------------------------------------------------------------
class COAScriptEngineTextWindow : public COAScriptEngineBase
{
	Q_OBJECT

public:
	COAScriptEngineTextWindow();
	virtual ~COAScriptEngineTextWindow() {}

	Q_INVOKABLE void Open();

	Q_INVOKABLE void Close();

	Q_INVOKABLE void Clear();

	Q_INVOKABLE void Print(const QString &text);

	Q_INVOKABLE void SetPos(const int &x, const int &y);

	Q_INVOKABLE void SetSize(const int &width, const int &height);

	Q_INVOKABLE void SaveToFile(const QString &filePath);
};
//----------------------------------------------------------------------------------
#endif // OATEXTWINDOWENGINE_H
//----------------------------------------------------------------------------------
