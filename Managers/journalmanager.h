/***********************************************************************************
**
** JournalManager.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef JOURNALMANAGER_H
#define JOURNALMANAGER_H
//----------------------------------------------------------------------------------
#include "../CommonItems/journalmessage.h"
//----------------------------------------------------------------------------------
typedef QVector<CJournalMessage> JOURNAL_LIST;
//----------------------------------------------------------------------------------
enum JOURNAL_MESSAGE_TYPE
{
	JMT_NORMAL	= 0x00,
	JMT_MY		= 0x01,
	JMT_SYSTEM	= 0x02
};
//----------------------------------------------------------------------------------
class CJournalManager
{
	SETGET(bool, IgnoreCase, false)
	SETGET(int, MaxLines, 100)

private:
	JOURNAL_LIST m_List;

	int FindLine(QString pattern, const uint &serial, const ushort &color, const uchar &type, const uint &startTime, const uint &endTime, const bool &remove);

public:
	CJournalManager();
	virtual ~CJournalManager();

	void Clear(QString pattern, const uint &serial, const ushort &color, const uchar &type);

	CJournalMessage *Find(QString pattern, const uint &serial, const ushort &color, const uchar &type, const uint &startTime, const uint &endTime);

	void Add(const CJournalMessage &message);
	CJournalMessage *Get(const uint &index);
	void Remove(const uint &index);

	int Count() { return m_List.size(); }
	void SetMessageText(const CJournalMessage *message, const QString &newText);

	CJournalMessage *LastMessage();

	void ShowLines(int count);
};
//----------------------------------------------------------------------------------
extern CJournalManager g_JournalManager;
//----------------------------------------------------------------------------------
#endif // JOURNALMANAGER_H
//----------------------------------------------------------------------------------
