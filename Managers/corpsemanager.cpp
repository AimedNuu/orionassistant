// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** CorpseManager.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "corpsemanager.h"
#include "../GameObjects/GameWorld.h"
#include "../GameObjects/GamePlayer.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../OrionAssistant/orionassistant.h"
#include "../OrionAssistant/tabmain.h"

CCorpseManager g_CorpseManager;
//----------------------------------------------------------------------------------
CCorpseManager::CCorpseManager()
{
}
//----------------------------------------------------------------------------------
CCorpseManager::~CCorpseManager()
{
}
//----------------------------------------------------------------------------------
void CCorpseManager::Add(const CCorpseInfo &info)
{
	OAFUN_DEBUG("c17_f1");
	for (CORPSE_INFO_LIST::iterator i = m_List.begin(); i != m_List.end(); ++i)
	{
		if (i->GetSerial() == info.GetSerial())
		{
			i->SetTimer(info.GetTimer());
			return;
		}
	}

	m_List.push_back(info);

	g_OrionAssistantForm->OnNewCorpse();
}
//----------------------------------------------------------------------------------
void CCorpseManager::Clear()
{
	OAFUN_DEBUG("c17_f2");
	m_List.clear();
}
//----------------------------------------------------------------------------------
void CCorpseManager::CheckCorpses()
{
	OAFUN_DEBUG("c17_f3");
	if (g_World == nullptr || g_Player == nullptr)
		return;

	int useTicks = g_LastUseObjectTimer.elapsed();

	if (useTicks < g_TabMain->UseItemsDelay())
		return;

	int globalTicks = g_StartTimer.elapsed();
	int useDistance = g_TabMain->OpenCorpsesDistance();

	for (CORPSE_INFO_LIST::iterator i = m_List.begin(); i != m_List.end();)
	{
		CGameItem *item = g_World->FindWorldItem(i->GetSerial());

		if (item == nullptr)
		{
			++i;
			continue;
		}
		else if (item->GetContainer() != 0xFFFFFFFF)
		{
			i = m_List.erase(i);
			continue;
		}

		if (GetDistance(g_Player, item) <= useDistance)
		{
			g_OrionAssistant.DoubleClick(item->GetSerial());
			g_LastUseObjectTimer.restart();
			m_List.erase(i);
			return;
		}

		if (i->GetTimer() < globalTicks)
			i = m_List.erase(i);
		else
			++i;
	}
}
//----------------------------------------------------------------------------------
