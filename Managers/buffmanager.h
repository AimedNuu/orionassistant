/***********************************************************************************
**
** BuffManager.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef BUFFMANAGER_H
#define BUFFMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../CommonItems/buffitem.h"
//----------------------------------------------------------------------------------
typedef QMap<ushort, CBuffItem> BUFF_MAP;
//----------------------------------------------------------------------------------
class CBuffManager
{
private:
	BUFF_MAP m_Map;

public:
	CBuffManager();
	virtual ~CBuffManager();

	void Clear();

	bool Exists(const ushort &graphic);

	bool Exists(const QString &name);

	void Add(const ushort &graphic, const uint &timer, const QString &name);

	void Remove(const ushort &graphic);
};
//----------------------------------------------------------------------------------
extern CBuffManager g_BuffManager;
//----------------------------------------------------------------------------------
#endif // BUFFMANAGER_H
//----------------------------------------------------------------------------------
