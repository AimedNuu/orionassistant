// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TradeManager.cpp
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "trademanager.h"
#include "../GameObjects/GameWorld.h"
#include "../OrionAssistant/orionassistant.h"

CTradeManager g_TradeManager;
//----------------------------------------------------------------------------------
CTradeManager::CTradeManager()
{
}
//----------------------------------------------------------------------------------
CTradeManager::~CTradeManager()
{
	m_List.clear();
}
//----------------------------------------------------------------------------------
bool CTradeManager::OnPacketSend(CDataReader &reader)
{
	OAFUN_DEBUG("c23_f1");
	if (g_World == nullptr)
		return true;

	int action = reader.ReadUInt8();
	uint id = reader.ReadUInt32BE();

	switch ((TRADE_ACTIONS)action)
	{
		case TA_CLOSE:
		{
			IFOR(i, 0, m_List.size())
			{
				CTradeWindow &trade = m_List[i];

				if (trade.GetID1() == id)
				{
					CGameObject *obj = g_World->FindWorldObject(trade.GetID1());

					if (obj != nullptr)
						g_World->RemoveObject(obj);

					obj = g_World->FindWorldObject(trade.GetID2());

					if (obj != nullptr)
						g_World->RemoveObject(obj);

					m_List.removeAt(i);

					return true;
				}
			}

			LOG("Warning!!! Unknown trade closed\n");

			break;
		}
		case TA_CHECK:
		{
			for (CTradeWindow &trade : m_List)
			{
				if (trade.GetID1() == id)
				{
					trade.SetCheck1((bool)reader.ReadUInt8());
					return true;
				}
			}

			LOG("Warning!!! Unknown trade changed\n");

			break;
		}
		default:
		{
			LOG("Warning!!! Unknown trade action\n");
			break;
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
bool CTradeManager::OnPacketReceived(CDataReader &reader)
{
	OAFUN_DEBUG("c23_f2");
	if (g_World == nullptr)
		return true;

	int action = reader.ReadUInt8();
	uint serial = reader.ReadUInt32BE();
	uint id1 = reader.ReadUInt32BE();
	uint id2 = reader.ReadUInt32BE();

	switch ((TRADE_ACTIONS)action)
	{
		case TA_OPEN:
		{
			QString name = "";

			if (reader.ReadUInt8() != 0)
				name = reader.ReadString().c_str();

			m_List.push_back(CTradeWindow(serial, id1, id2, name));

			break;
		}
		case TA_CLOSE:
		{
			IFOR(i, 0, m_List.size())
			{
				CTradeWindow &trade = m_List[i];

				if (trade.GetID1() == serial)
				{
					CGameObject *obj = g_World->FindWorldObject(trade.GetID1());

					if (obj != nullptr)
						g_World->RemoveObject(obj);

					obj = g_World->FindWorldObject(trade.GetID2());

					if (obj != nullptr)
						g_World->RemoveObject(obj);

					m_List.removeAt(i);

					return true;
				}
			}

			LOG("Warning!!! Unknown trade closed\n");

			break;
		}
		case TA_CHECK:
		{
			for (CTradeWindow &trade : m_List)
			{
				if (trade.GetID1() == serial)
				{
					trade.SetCheck1((bool)id1);
					trade.SetCheck2((bool)id2);
					return true;
				}
			}

			LOG("Warning!!! Unknown trade changed\n");

			break;
		}
		default:
		{
			LOG("Warning!!! Unknown trade action\n");
			break;
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
void CTradeManager::Clear()
{
	OAFUN_DEBUG("c23_f3");
	m_List.clear();
}
//----------------------------------------------------------------------------------
int CTradeManager::Count()
{
	OAFUN_DEBUG("c23_f4");
	return m_List.size();
}
//----------------------------------------------------------------------------------
CTradeWindow *CTradeManager::GetTrade(const QString &serial)
{
	OAFUN_DEBUG("c23_f5");
	uint index = 0xFFFFFFFF;

	if (serial.indexOf("0x", 0, Qt::CaseInsensitive) == 0)
	{
		index = COrionAssistant::TextToSerial(serial);

		IFOR(i, 0, m_List.size())
		{
			if (m_List[i].GetSerial() == index)
				return &m_List[i];
		}
	}
	else
	{
		index = serial.toInt();

		if (index < (uint)m_List.size())
			return &m_List[index];
	}

	return nullptr;
}
//----------------------------------------------------------------------------------
