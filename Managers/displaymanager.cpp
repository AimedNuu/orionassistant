// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** DisplayManager.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "displaymanager.h"
#include "TextParser.h"
#include "../OrionAssistant/orionassistant.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../GameObjects/gameworld.h"
#include "../GameObjects/gameplayer.h"
#include <QBuffer>
#include <QDebug>
#include <QtWin>
#include "../OrionAssistant/tabdisplay.h"
#include "filemanager.h"

CDisplayManager g_DisplayManager;
//----------------------------------------------------------------------------------
const CDisplayItem CDisplayManager::m_DefaultItems[26] =
{
	//ABGR colors, A=00
	CDisplayItem(), //empty
	//														text min	mid			norm		bg		bg min		mid			norm		min/max val		percents
	CDisplayItem(DIT_NAME,				"",			0, 0,	0x00000000, 0x00000000, 0x00000001, false,	0x0000007F, 0x007F7F7F, 0x007F0000, 0,		0,		false), //Player name
	CDisplayItem(DIT_SERVER,			"",			0, 0,	0x00000000, 0x00000000, 0x00000001, false,	0x00000000, 0x00000000, 0x00000000, 0,		0,		false), //Server name
	CDisplayItem(DIT_STATUSBAR_SMALL,	"",			0, 0,	0x00007F00, 0x00007F00, 0x00007F00, false,	0x00000000, 0x00000000, 0x00000000, 33,		66,		false), //Statusbar small
	CDisplayItem(DIT_STATUSBAR_MIDDLE,	"",			0, 0,	0x00007F00, 0x00007F00, 0x00007F00, false,	0x00000000, 0x00000000, 0x00000000, 33,		66,		false), //Statusbar middle
	CDisplayItem(DIT_STATUSBAR_LARGE,	"",			0, 0,	0x00007F00, 0x00007F00, 0x00007F00, false,	0x00000000, 0x00000000, 0x00000000, 33,		66,		false), //Statusbar large
	CDisplayItem(DIT_HITS,				"hp:",		0, 0,	0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 33,		66,		true), //Hitpoints
	CDisplayItem(DIT_MAX_HITS,			"",			0, 0,	0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 0,		0,		false), //Max hitpoints
	CDisplayItem(DIT_HITS_MAX_HITS,		"hp:",		0, 0,	0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 33,		66,		true), //Hitpoints / [Max hitpoints]
	CDisplayItem(DIT_MANA,				"mp:",		0, 0,	0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 33,		66,		true), //Mana
	CDisplayItem(DIT_MAX_MANA,			"",			0, 0,	0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 0,		0,		false), //Max mana
	CDisplayItem(DIT_MANA_MAX_MANA,		"mp:",		0, 0,	0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 33,		66,		true), //Mana / [Max mana]
	CDisplayItem(DIT_STAM,				"sp:",		0, 0,	0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 33,		66,		true), //Stamina
	CDisplayItem(DIT_MAX_STAM,			"",			0, 0,	0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 0,		0,		false), //Max stamina
	CDisplayItem(DIT_STAM_MAX_STAM,		"sp:",		0, 0,	0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 33,		66,		true), //Stamina / [Max stamina]
	CDisplayItem(DIT_STR,				"str:",		0, 0,	0x00000001, 0x00000001, 0x00000001, false,	0x00000000, 0x00000000, 0x00000000, 10,		30,		false), //Stringth
	CDisplayItem(DIT_INT,				"int:",		0, 0,	0x00000001, 0x00000001, 0x00000001, false,	0x00000000, 0x00000000, 0x00000000, 10,		30,		false), //Intelligence
	CDisplayItem(DIT_DEX,				"dex:",		0, 0,	0x00000001, 0x00000001, 0x00000001, false,	0x00000000, 0x00000000, 0x00000000, 10,		30,		false), //Dexterity
	CDisplayItem(DIT_WEIGHT,			"w:",		0, 0,	0x00000001, 0x00000001, 0x00FFFFFF, true,	0x00000000, 0x0000FFFF, 0x0000007F, 33,		66,		true), //Weight
	CDisplayItem(DIT_MAX_WEIGHT,		"",			0, 0,	0x00000001, 0x00000001, 0x00000001, false,	0x00000000, 0x00000000, 0x00000000, 0,		0,		false), //Max weight
	CDisplayItem(DIT_WEIGHT_MAX_WEIGHT,	"w:",		0, 0,	0x00000001, 0x00000001, 0x00FFFFFF, true,	0x00000000, 0x0000FFFF, 0x0000007F, 33,		66,		true), //Weight / [Max weight]
	CDisplayItem(DIT_ARMOR,				"ar:",		0, 0,	0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 20,		40,		false), //Armor rating
	CDisplayItem(DIT_GOLD,				"g:",		0, 0,	0x00000001, 0x00000001, 0x00000001, false,	0x00000000, 0x00000000, 0x00000000, 0,		0,		false), //Gold
	CDisplayItem(DIT_X,					"x:",		0, 0,	0x00000001, 0x00000001, 0x00000001, false,	0x00000000, 0x00000000, 0x00000000, 0,		0,		false), //X
	CDisplayItem(DIT_Y,					"y:",		0, 0,	0x00000001, 0x00000001, 0x00000001, false,	0x00000000, 0x00000000, 0x00000000, 0,		0,		false), //Y
	CDisplayItem(DIT_Z,					"z:",		0, 0,	0x00000001, 0x00000001, 0x00000001, false,	0x00000000, 0x00000000, 0x00000000, 0,		0,		false) //Z
};
//----------------------------------------------------------------------------------
const DISPLAY_KEY_ITEM CDisplayManager::m_KeysList[DKI_COUNT] =
{
	{"wtf key?", DKI_NONE},
	{"text", DKI_TEXT},
	{"id", DKI_ID},
	{"hue", DKI_HUE},
	{"mincolor", DKI_COLOR_MIN},
	{"midcolor", DKI_COLOR_MID},
	{"color", DKI_COLOR},
	{"background", DKI_BACKGROUND},
	{"bgmincolor", DKI_BG_COLOR_MIN},
	{"bgmidcolor", DKI_BG_COLOR_MID},
	{"bgcolor", DKI_BG_COLOR},
	{"minval", DKI_MIN_VAULE},
	{"midval", DKI_MID_VALUE},
	{"%", DKI_PERCENTS},
};
//----------------------------------------------------------------------------------
CDisplayIconData::~CDisplayIconData()
{
	if (Bitmap != nullptr)
	{
		DeleteObject(Bitmap);
		Bitmap = nullptr;
	}
}
//----------------------------------------------------------------------------------
CDisplayManager::CDisplayManager()
: m_InfoCommands()
{
	OAFUN_DEBUG("c18_f1");
	m_InfoCommands.push_back(CDisplayInfo("", "", DIT_NONE));
	m_InfoCommands.push_back(CDisplayInfo("Player name", "name", DIT_NAME));
	m_InfoCommands.push_back(CDisplayInfo("Server name", "server", DIT_SERVER));
	m_InfoCommands.push_back(CDisplayInfo("Statusbar small", "barsmall", DIT_STATUSBAR_SMALL));
	m_InfoCommands.push_back(CDisplayInfo("Statusbar middle", "barmiddle", DIT_STATUSBAR_MIDDLE));
	m_InfoCommands.push_back(CDisplayInfo("Statusbar large", "barlarge", DIT_STATUSBAR_LARGE));
	m_InfoCommands.push_back(CDisplayInfo("Hitpoints", "hp", DIT_HITS));
	m_InfoCommands.push_back(CDisplayInfo("Max hitpoints", "maxhp", DIT_MAX_HITS));
	m_InfoCommands.push_back(CDisplayInfo("Hitpoints / [Max hitpoints]", "hp_maxhp", DIT_HITS_MAX_HITS));
	m_InfoCommands.push_back(CDisplayInfo("Mana", "mp", DIT_MANA));
	m_InfoCommands.push_back(CDisplayInfo("Max mana", "maxmp", DIT_MAX_MANA));
	m_InfoCommands.push_back(CDisplayInfo("Mana / [Max mana]", "mp_maxmp", DIT_MANA_MAX_MANA));
	m_InfoCommands.push_back(CDisplayInfo("Stamina", "sp", DIT_STAM));
	m_InfoCommands.push_back(CDisplayInfo("Max stamina", "maxsp", DIT_MAX_STAM));
	m_InfoCommands.push_back(CDisplayInfo("Stamina / [Max stamina]", "sp_maxsp", DIT_STAM_MAX_STAM));
	m_InfoCommands.push_back(CDisplayInfo("Stringth", "str", DIT_STR));
	m_InfoCommands.push_back(CDisplayInfo("Intelligence", "int", DIT_INT));
	m_InfoCommands.push_back(CDisplayInfo("Dexterity", "dex", DIT_DEX));
	m_InfoCommands.push_back(CDisplayInfo("Weight", "weight", DIT_WEIGHT));
	m_InfoCommands.push_back(CDisplayInfo("Max weight", "maxweight", DIT_MAX_WEIGHT));
	m_InfoCommands.push_back(CDisplayInfo("Weight / [Max weight]", "weight_maxweight", DIT_WEIGHT_MAX_WEIGHT));
	m_InfoCommands.push_back(CDisplayInfo("Armor rating", "armor", DIT_ARMOR));
	m_InfoCommands.push_back(CDisplayInfo("Gold", "gold", DIT_GOLD));
	m_InfoCommands.push_back(CDisplayInfo("X", "x", DIT_X));
	m_InfoCommands.push_back(CDisplayInfo("Y", "y", DIT_Y));
	m_InfoCommands.push_back(CDisplayInfo("Z", "z", DIT_Z));
	m_InfoCommands.push_back(CDisplayInfo("Any item counter", "item", DIT_OBJECT));
}
//----------------------------------------------------------------------------------
CDisplayManager::~CDisplayManager()
{
}
//----------------------------------------------------------------------------------
void CDisplayManager::ClearIcons()
{
	OAFUN_DEBUG("c18_f2");
	for (DISPLAY_ICON_MAP::iterator i = m_Icons.begin(); i != m_Icons.end(); ++i)
		delete i.value();

	m_Icons.clear();
}
//----------------------------------------------------------------------------------
CDisplayIconData *CDisplayManager::GetIcon(const CDisplayItem &item)
{
	OAFUN_DEBUG("c18_f3");
	if (!item.GetGraphic())
		return nullptr;

	uint serial = (item.GetGraphic() << 16) | item.GetHue();

	DISPLAY_ICON_MAP::iterator i = m_Icons.find(serial);

	if (i == m_Icons.end() || i.value() == nullptr)
	{
		size_t address = g_FileManager.GetStaticArtInfo(item.GetGraphic()).GetAddress();

		if (address == 0)
			return nullptr;

		CDisplayIconData *obj = new CDisplayIconData();

		m_Icons.insert(serial, obj);

		LoadDisplayIcon(obj, item.GetGraphic(), item.GetHue(), address);

		return obj;
	}

	return i.value();
}
//----------------------------------------------------------------------------------
void CDisplayManager::LoadDisplayIcon(CDisplayIconData *item, ushort graphic, const ushort &color, const size_t &address)
{
	OAFUN_DEBUG("c18_f4");
	pushort sizes = (pushort)(address + 4);

	int dataWidth = *sizes;
	sizes++;

	if (!dataWidth || dataWidth >= 1024)
		return;

	int dataHeight = *sizes;
	sizes++;

	if (!dataHeight || (dataHeight * 2) > 5120)
		return;

	if (graphic == 0x0EED || graphic == 0x0EEE)
		graphic = 0x0EEF; //Fix gold display

	uint64 tileFlags = g_GetStaticFlags(graphic);

	int stackOffset = 0;
	UINT_LIST pix;

	if ((tileFlags & 0x00000800) && graphic != 0x0EEF && g_TabDisplay->DoubleImage())
	{
		stackOffset = 5;
		pix.resize((dataWidth + 5) * (dataHeight + 5), 0);
	}
	else
		pix.resize(dataWidth * dataHeight, 0);

	bool partialHue = (tileFlags & 0x00040000);

	pushort lineOffsets = sizes;
	PVOID dataStart = (PVOID)((uint)sizes + (dataHeight * 2));

	int X = 0;
	int Y = 0;
	ushort XOffs = 0;
	ushort Run = 0;

	FUNCDEF_GET_COLOR *funGetColor = (partialHue ? g_ClientInterface->Client->ColorManager->GetPartialHueColor : (FUNCDEF_GET_COLOR*)g_ClientInterface->Client->ColorManager->GetColor);
	FUNCDEF_GET_COLOR16TO32 *funColor16TO32 = g_ClientInterface->Client->ColorManager->Color16To32;

	pushort ptr = (pushort)((uint)dataStart + (*lineOffsets));

	int imageWidth = dataWidth + stackOffset;
	int imageHeight = dataHeight + stackOffset;

	int minX = 999;
	int minY = 999;
	int maxX = 0;
	int maxY = 0;

	while (Y < dataHeight)
	{
		XOffs = *ptr++;
		Run = *ptr++;

		if (XOffs + Run >= 2048)
			return;
		else if (XOffs + Run != 0)
		{
			X += XOffs;
			int pos = (Y * imageWidth) + X;

			IFOR(j, 0, Run)
			{
				if (*ptr)
				{
					if (X + j < minX)
						minX = X + j;

					if (X + j > maxX)
						maxX = X + j;

					if (Y < minY)
						minY = Y;

					if (Y > maxY)
						maxY = Y;

					uint col = 0;

					if (color)
						col = funGetColor(*ptr, color);
					else
						col = funColor16TO32(*ptr);

					puchar pcol = (puchar)&col;
					uchar bcol = pcol[0];
					pcol[0] = pcol[2];
					pcol[2] = bcol;

					pix[pos + j] = col;
				}

				ptr++;
			}

			X += Run;
		}
		else
		{
			X = 0;
			Y++;
			ptr = (pushort)((uint)dataStart + (lineOffsets[Y] * 2));
		}
	}

	if (stackOffset)
	{
		DFOR(j, dataHeight - 1, 0)
		{
			int srcPosY = (j * imageWidth);
			int dstPosY = ((j + stackOffset) * imageWidth);

			DFOR(i, dataWidth - 1, 0)
			{
				int srcPos = srcPosY + i;
				uint pixel = pix[srcPos];

				if (pixel)
				{
					int dstPos = dstPosY + i + stackOffset;
					pix[dstPos] = pixel;
				}
			}
		}
	}

	int realWidth = (maxX - minX) + stackOffset;

	if (maxX < imageWidth)
		realWidth++;

	int realHeight = (maxY - minY) + stackOffset;

	if (maxY < imageHeight)
		realHeight++;

	UINT_LIST realPixels(realWidth * realHeight);

	IFOR(i, 0, realHeight)
	{
		int srcY = (i + minY) * imageWidth;
		int dstY = i * realWidth;

		IFOR(j, 0, realWidth)
			realPixels[dstY + j] = pix[srcY + j + minX];
	}

	item->Bitmap = CreateBitmap(realWidth, realHeight, 1, 32, (uchar*)&realPixels[0]);

	/*QImage img((puchar)&pix[0], imageWidth, imageHeight, QImage::Format_RGB32);

	if (imageHeight > 22)
	{
		int per = (22 * 100) / imageHeight;

		imageWidth = (int)((imageWidth * per) / 100) + 1;
		imageHeight = (int)((imageHeight * per) / 100) + 1;

		img = img.scaled(imageWidth, imageHeight);
	}

	item->Image = img;*/

	/*QPixmap pm;
	pm.fromImage(item->Image);
	item->Bitmap = QtWin::toHBITMAP(pm, QtWin::HBitmapAlpha);*/


	/*QByteArray bytes;
	QBuffer buffer(&bytes);
	buffer.open(QIODevice::WriteOnly);
	item->Image.save(&buffer, "BMP");

	//item->Bitmap = CreateBitmap(dataWidth, dataHeight, 1, 32, (uchar*)bytes.data());*/
}
//----------------------------------------------------------------------------------
void CDisplayManager::GetValues(const CDisplayItem &item, QString &value, QString &maxValue, uint &textColor, uint &backgroundColor, const bool &useNotoriety)
{
	OAFUN_DEBUG("c18_f5");
	int iVal = 0;
	int iMaxVal = 0;
	bool inPercents = false;

	switch (item.GetType())
	{
		case DIT_NAME:
		{
			value = g_CharacterName;
			backgroundColor = 0;

			if (useNotoriety)
			{
				if (g_Player->GetNotoriety() == 3 || g_Player->GetNotoriety() == 4)
					textColor = item.GetBackgroundMidColor(); //gray
				else if (g_Player->GetNotoriety() == 5 || g_Player->GetNotoriety() == 6)
					textColor = item.GetBackgroundMinColor(); //red
				else
					textColor = item.GetBackgroundColor(); //blue
			}
			else
				textColor = item.GetColor();

			return;
		}
		case DIT_SERVER:
		{
			value = g_ServerName;
			textColor = item.GetColor();
			backgroundColor = item.GetBackgroundColor();
			return;
		}
		case DIT_HITS:
		case DIT_MAX_HITS:
		case DIT_HITS_MAX_HITS:
		{
			iVal = g_Player->GetHits();
			iMaxVal = g_Player->GetMaxHits();
			inPercents = (item.GetPercents() && item.GetType() != DIT_MAX_HITS);
			break;
		}
		case DIT_MANA:
		case DIT_MAX_MANA:
		case DIT_MANA_MAX_MANA:
		{
			iVal = g_Player->GetMana();
			iMaxVal = g_Player->GetMaxMana();
			inPercents = (item.GetPercents() && item.GetType() != DIT_MAX_MANA);
			break;
		}
		case DIT_STAM:
		case DIT_MAX_STAM:
		case DIT_STAM_MAX_STAM:
		{
			iVal = g_Player->GetStam();
			iMaxVal = g_Player->GetMaxStam();
			inPercents = (item.GetPercents() && item.GetType() != DIT_MAX_STAM);
			break;
		}
		case DIT_STR:
		{
			iVal = g_Player->GetStr();
			break;
		}
		case DIT_INT:
		{
			iVal = g_Player->GetInt();
			break;
		}
		case DIT_DEX:
		{
			iVal = g_Player->GetDex();
			break;
		}
		case DIT_WEIGHT:
		case DIT_MAX_WEIGHT:
		case DIT_WEIGHT_MAX_WEIGHT:
		{
			iVal = g_Player->GetWeight();
			iMaxVal = g_Player->GetMaxWeight();
			inPercents = (item.GetPercents() && item.GetType() != DIT_MAX_WEIGHT);
			break;
		}
		case DIT_ARMOR:
		{
			iVal = g_Player->GetArmor();
			break;
		}
		case DIT_GOLD:
		{
			iVal = g_Player->GetGold();
			break;
		}
		case DIT_X:
		{
			value = QString::number(g_Player->GetX());
			textColor = item.GetColor();
			backgroundColor = item.GetBackgroundColor();
			return;
		}
		case DIT_Y:
		{
			value = QString::number(g_Player->GetY());
			textColor = item.GetColor();
			backgroundColor = item.GetBackgroundColor();
			return;
		}
		case DIT_Z:
		{
			value = QString::number(g_Player->GetZ());
			textColor = item.GetColor();
			backgroundColor = item.GetBackgroundColor();
			return;
		}
		case DIT_OBJECT:
		{
			iVal = g_Player->CountItems(item.GetGraphic(), item.GetHue(), true);

			break;
		}
		case DIT_NONE:
			textColor = item.GetColor();
		default:
			return;
	}

	value = QString::number(iVal);
	maxValue = QString::number(iMaxVal);

	if (inPercents && iMaxVal)
		iVal = (int)((iVal * 100) / (float)iMaxVal);

	if (iVal <= item.GetMinValue())
	{
		textColor = item.GetMinColor();
		backgroundColor = item.GetBackgroundMinColor();
	}
	else if (iVal <= item.GetMidValue())
	{
		textColor = item.GetMidColor();
		backgroundColor = item.GetBackgroundMidColor();
	}
	else
	{
		textColor = item.GetColor();
		backgroundColor = item.GetBackgroundColor();
	}
}
//----------------------------------------------------------------------------------
bool CDisplayManager::CanRedraw(const ushort &graphic, const ushort &color)
{
	OAFUN_DEBUG("c18_f6");
	for (DISPLAY_LIST::iterator it = m_Commands.begin(); it != m_Commands.end(); ++it)
	{
		CDisplayItem &obj = *it;

		if (obj.GetType() == DIT_OBJECT && obj.GetGraphic() == graphic && (obj.GetHue() == 0xFFFF || obj.GetHue() == color))
			return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
void CDisplayManager::Redraw(const REDRAW_DISPLAY_MODE &mode, const bool &useNotoriety, const bool &hightFont, bool highlightReagents, const bool &displayAppIcon, const uint &backgroundColor)
{
	OAFUN_DEBUG("c18_f7");
	QString title = "";

	if (mode == RDM_DISABLED || IsIconic(g_ClientHandle) || g_World == nullptr || g_Player == nullptr)
	{
		title = "Ultima Online";

		if (g_World == nullptr)
		{
			if (g_ServerName.length())
				title += " - " + g_ServerName;
		}
		else
			title += " - " + g_CharacterName + " (" + g_ServerName + ")";
	}
	else if (mode == RDM_TEXTUAL)
	{
		for (DISPLAY_LIST::iterator it = m_Commands.begin(); it != m_Commands.end(); ++it)
		{
			CDisplayItem &obj = *it;

			QString value = "";
			QString maxValue = "";
			uint textColor = 0;
			uint backgroundColor = 0;

			GetValues(obj, value, maxValue, textColor, backgroundColor, useNotoriety);

			title += obj.GetText();

			switch (obj.GetType())
			{
				case DIT_MAX_HITS:
				case DIT_MAX_MANA:
				case DIT_MAX_STAM:
				case DIT_MAX_WEIGHT:
				{
					title += maxValue;
					break;
				}
				case DIT_HITS_MAX_HITS:
				case DIT_MANA_MAX_MANA:
				case DIT_STAM_MAX_STAM:
				case DIT_WEIGHT_MAX_WEIGHT:
				{
					if (value != maxValue)
						title += value + "/" + maxValue;
					else
						title += value;

					break;
				}
				default:
				{
					title += value;
					break;
				}
			}
		}

		if (!title.length())
			title = "Ultima Online - " + g_CharacterName + " (" + g_ServerName + ")";
	}
	else
	{
		bool useImages = (mode == RDM_COLORED_GRAPHICAL);

		PAINTSTRUCT ps;
		RECT rect1;
		GetWindowRect(g_ClientHandle, &rect1);

		int X_offs = GetSystemMetrics(SM_CXSIZE) * 3 - 2 + GetSystemMetrics(SM_CXSIZEFRAME);
		int nowx = 4;
		int yStart = GetSystemMetrics(SM_CYSIZEFRAME);
		int yEnd = GetSystemMetrics(SM_CYSIZEFRAME) + GetSystemMetrics(SM_CYCAPTION);

		int iconoffs = 2;

		if (displayAppIcon)
			iconoffs = 28;
		else
			nowx = 15;

		int x1 = rect1.right - rect1.left - X_offs - iconoffs;

		BeginPaint(g_ClientHandle, &ps);
		HDC hDC = GetWindowDC(g_ClientHandle);

		HDC hBufDC = CreateCompatibleDC(hDC);
		HBITMAP hBufBMP = CreateCompatibleBitmap(hDC, x1, yEnd);
		SelectObject(hBufDC, hBufBMP);

		HBRUSH hActiveBrush = CreateSolidBrush(GetSysColor(COLOR_ACTIVECAPTION));
		HBRUSH hInactiveBrush = CreateSolidBrush(GetSysColor(COLOR_INACTIVECAPTION));
		HBRUSH hUserBrush = CreateSolidBrush(backgroundColor);

		HPEN hActivePen = CreatePen(PS_SOLID, 1, GetSysColor(COLOR_ACTIVECAPTION));
		HPEN hInactivePen = CreatePen(PS_SOLID, 1, GetSysColor(COLOR_INACTIVECAPTION));
		HPEN hUserPen = CreatePen(PS_SOLID, 1, backgroundColor);

		HPEN hPenWhite = CreatePen(PS_SOLID, 1, RGB(0xFF, 0xFF, 0xFF));
		HPEN hPenBlack = CreatePen(PS_SOLID, 1, 0);
		HPEN hPenRed = CreatePen(PS_SOLID, 1, RGB(0xFF, 0, 0));

		wstring sFontName = L"System";
		int iFontH = yEnd - yStart;

		if (!g_LinuxOS)
		{
			NONCLIENTMETRICS ncm; // = {sizeof(NONCLIENTMETRICS), 0};
			ncm.cbSize = sizeof(NONCLIENTMETRICS);

#if (WINVER >= 0x0600)
			OSVERSIONINFO osvi = {sizeof(OSVERSIONINFO), 0};
			GetVersionEx(&osvi);
			if (osvi.dwMajorVersion < 6)
				ncm.cbSize -= sizeof(int);
#endif

			SystemParametersInfo(SPI_GETNONCLIENTMETRICS, ncm.cbSize, &ncm, 0);
			sFontName = ncm.lfCaptionFont.lfFaceName;
			iFontH = ncm.lfCaptionFont.lfHeight;
		}

		int hightFontOffsetY = 0;

		if (hightFont)
		{
			hightFontOffsetY = 2;
			iFontH *= 1.2;
		}

		HFONT hFont = CreateFont(iFontH, 0, 0, 0, FW_BOLD, 0, 0, 0,
				ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
				DEFAULT_PITCH | FF_SWISS, sFontName.c_str());

		HGDIOBJ hOldBrush;
		HGDIOBJ hOldPen;

		if (GetForegroundWindow() == g_ClientHandle && !backgroundColor)
		{
			SetBkColor(hBufDC, GetSysColor(COLOR_ACTIVECAPTION));
			hOldBrush = SelectObject(hBufDC, hActiveBrush);
			hOldPen = SelectObject(hBufDC, hActivePen);
		}
		else
		{
			if (backgroundColor)
			{
				SetBkColor(hBufDC, backgroundColor);
				hOldBrush = SelectObject(hBufDC, hUserBrush);
				hOldPen = SelectObject(hBufDC, hUserPen);
			}
			else
			{
				SetBkColor(hBufDC, GetSysColor(COLOR_INACTIVECAPTION));
				hOldBrush = SelectObject(hBufDC, hInactiveBrush);
				hOldPen = SelectObject(hBufDC, hInactivePen);
			}
		}

		SetBkMode(hBufDC, OPAQUE);
		SetTextAlign(hBufDC, TA_LEFT | TA_TOP);

		Rectangle(hBufDC, 0, 0, x1, yEnd);

		SelectObject(hBufDC, hPenBlack);
		HGDIOBJ hOldFont = SelectObject(hBufDC, hFont);

		int spell = g_LastSpellIndex - 1;
		highlightReagents = (highlightReagents && spell >= 0 && spell < 64);

		ushort reagents[8] =
		{
			0x0F7A, 0x0F7B, 0x0F84, 0x0F85,
			0x0F86, 0x0F88, 0x0F8C, 0x0F8D
		};

		int reagCount = (highlightReagents ? g_SpellReagents[spell][0] : 0);

		for (DISPLAY_LIST::iterator it = m_Commands.begin(); it != m_Commands.end(); it++)
		{
			CDisplayItem &obj = *it;

			QString value = "";
			QString maxValue = "";
			uint textColor = 0;
			uint backgroundColor = 0;

			GetValues(obj, value, maxValue, textColor, backgroundColor, useNotoriety);

			bool imaged = false;

			if (obj.GetBackground() && backgroundColor)
			{
				SetBkMode(hBufDC, OPAQUE);
				SetBkColor(hBufDC, backgroundColor);
			}
			else
			{
				SetBkMode(hBufDC, TRANSPARENT);
				SetBkColor(hBufDC, 0);
			}

			SetTextColor(hBufDC, textColor);

			int startDrawX = nowx + 2;

			bool wantToHighlightReagent = false;

			if (obj.GetType() != DIT_NONE && obj.GetGraphic())
			{
				if (highlightReagents && !obj.GetHue())
				{
					IFOR(r, 0, 8)
					{
						const ushort &reag = reagents[r];

						if (reag != obj.GetGraphic())
							continue;

						DFOR(ri, reagCount, 1)
						{
							if (reag == g_SpellReagents[spell][ri])
							{
								wantToHighlightReagent = true;
								r = 8;
								break;
							}
						}
					}
				}

				if (useImages)
				{
					CDisplayIconData *icon = GetIcon(obj);

					if (icon != nullptr)
					{
						HBITMAP hBitmap = icon->Bitmap;

						if (hBitmap)
						{
							imaged = true;

							nowx += 2;
							BITMAP bm;
							GetObject(hBitmap, sizeof(bm), &bm);
							int cx = bm.bmWidth;
							int cy = bm.bmHeight;
							HDC memdc = CreateCompatibleDC(hBufDC);
							SelectObject(hBufDC, hPenBlack);
							HGDIOBJ oldbmp = SelectObject(memdc, hBitmap);
							TransparentBlt(hBufDC, nowx, yStart + ((yEnd - yStart) - cy) / 2, cx, cy, memdc, 0, 0, cx, cy, 0);
							nowx += (cx + 2);
							SelectObject(memdc, oldbmp);
							DeleteDC(memdc);
						}
					}
				}
			}

			wstring str = L"";

			if (!imaged)
				str = obj.GetText().toStdWString();

			switch (obj.GetType())
			{
				case DIT_MAX_HITS:
				case DIT_MAX_MANA:
				case DIT_MAX_STAM:
				case DIT_MAX_WEIGHT:
				{
					str += maxValue.toStdWString();

					break;
				}
				case DIT_HITS_MAX_HITS:
				case DIT_MANA_MAX_MANA:
				case DIT_STAM_MAX_STAM:
				case DIT_WEIGHT_MAX_WEIGHT:
				{
					if (value != maxValue)
						str += (value + "/" + maxValue).toStdWString();
					else
						str += value.toStdWString();

					break;
				}
				case DIT_STATUSBAR_SMALL:
				case DIT_STATUSBAR_MIDDLE:
				case DIT_STATUSBAR_LARGE:
				{
					if (g_Player->GetMaxHits() > 0 && g_Player->GetMaxMana() > 0 && g_Player->GetMaxStam() > 0)
					{
						int sbData[3][4] =
						{
							//Hits,					MaxHits,				StartY,								EndY
							{ g_Player->GetHits(),	g_Player->GetMaxHits(), yStart,								yStart + (yEnd - yStart) / 3 },
							{ g_Player->GetMana(),	g_Player->GetMaxMana(), yStart + (yEnd - yStart) / 3,		yStart + (yEnd - yStart) * 2 / 3 },
							{ g_Player->GetStam(),	g_Player->GetMaxStam(), yStart + (yEnd - yStart) * 2 / 3,	yEnd }
						};

						int bar_len = 100; //large && reserved

						if (obj.GetType() == DIT_STATUSBAR_SMALL)
							bar_len = 33; //small
						else if (obj.GetType() == DIT_STATUSBAR_MIDDLE)
							bar_len = 66; //medium

						SetTextColor(hBufDC, 0);
						SetBkMode(hBufDC, TRANSPARENT);
						SetBkColor(hBufDC, 0);

						HPEN sb_penN = CreatePen(PS_NULL, 0, 0);
						HBRUSH sb_brushB = CreateSolidBrush(0);
						HGDIOBJ sb_oldpen = SelectObject(hBufDC, sb_penN);
						HGDIOBJ sb_oldbrush = SelectObject(hBufDC, sb_brushB);

						Rectangle(hBufDC, nowx, yStart, nowx + bar_len, yEnd);

						IFOR(j, 0, 3)
						{
							uint clr = 0;

							int len = (bar_len * sbData[j][0]) / sbData[j][1];

							if (len < 0)
								len = 0;
							else if (len > bar_len)
								len = bar_len;

							if (!j && g_Player->Poisoned())
								clr = RGB(0xFF, 0x7F, 0);
							else
							{
								int percents = (100 * sbData[j][0]) / sbData[j][1];

								if (percents <= obj.GetMinValue())
									clr = obj.GetMinColor(); //RGB(0xFF, 0, 0);
								else if (percents <= obj.GetMidValue())
									clr = obj.GetMidColor(); //RGB(0xFF, 0xFF, 0);
								else
									clr = obj.GetColor(); //RGB(0, 0xFF, 0);
							}

							HBRUSH sb_brush = CreateSolidBrush(clr);
							SelectObject(hBufDC, sb_brush);

							Rectangle(hBufDC, nowx, sbData[j][2], nowx + len, sbData[j][3]);
							DeleteObject(sb_brush);
						}

						SelectObject(hBufDC, sb_oldbrush);
						SelectObject(hBufDC, sb_oldpen);
						DeleteObject(sb_penN);
						DeleteObject(sb_brushB);
						nowx += bar_len;
					}
					break;
				}
				default:
				{
					str += value.toStdWString();

					break;
				}
			}

			if (str.length())
			{
				TextOut(hBufDC, nowx, yStart + 3 - hightFontOffsetY, str.c_str(), str.length());
				SIZE ext;
				GetTextExtentPoint(hBufDC, str.c_str(), str.length(), &ext);
				nowx += ext.cx;

				if (wantToHighlightReagent)
				{
					HGDIOBJ hr_oldpen = SelectObject(hBufDC, hPenRed);
					MoveToEx(hBufDC, startDrawX, yEnd - 4, 0);
					LineTo(hBufDC, nowx, yEnd - 4);
					SelectObject(hBufDC, hr_oldpen);
				}
			}
		}

		BitBlt(hDC, displayAppIcon ? 26 : 0, 0, x1, yEnd, hBufDC, 0, 0, SRCCOPY);
		SelectObject(hBufDC, hOldPen);
		SelectObject(hBufDC, hOldBrush);
		SelectObject(hBufDC, hOldFont);
		DeleteObject(hActiveBrush);
		DeleteObject(hInactiveBrush);
		DeleteObject(hUserBrush);
		DeleteObject(hActivePen);
		DeleteObject(hInactivePen);
		DeleteObject(hUserPen);
		DeleteObject(hPenWhite);
		DeleteObject(hPenBlack);
		DeleteObject(hPenRed);
		DeleteObject(hFont);
		DeleteObject(hBufBMP);
		DeleteDC(hBufDC);
		ReleaseDC(g_ClientHandle, hDC);
		EndPaint(g_ClientHandle, &ps);

		return;
	}

	if (IsWindowUnicode(g_ClientHandle))
		SetWindowTextW(g_ClientHandle, title.toStdWString().c_str());
	else
		SetWindowTextA(g_ClientHandle, title.toStdString().c_str());
}
//----------------------------------------------------------------------------------
CDisplayItem CDisplayManager::GetDisplayItemByName(const QString &name)
{
	OAFUN_DEBUG("c18_f8");
	CDisplayItem item = g_TabDisplay->GetCustomDisplayItemByName(name);

	if (item.GetType() == DIT_NONE)
	{
		for (DISPLAY_INFO_LIST::iterator it = m_InfoCommands.begin(); it != m_InfoCommands.end(); ++it)
		{
			if (name == it->Command)
			{
				if (it->Type != DIT_OBJECT)
					return m_DefaultItems[it->Type];
					//return CDisplayItem(m_DefaultItems[it->Type]);

				break;
			}
		}
	}

	return item;
}
//----------------------------------------------------------------------------------
void CDisplayManager::ApplyAttributeToDisplayItem(CDisplayItem &obj, const QString &name, const QString &value)
{
	OAFUN_DEBUG("c18_f9");
	IFOR(i, 0, DKI_COUNT)
	{
		if (name == m_KeysList[i].Name)
		{
			switch (m_KeysList[i].Identifier)
			{
				case DKI_TEXT:
				{
					obj.SetText(value);
					break;
				}
				case DKI_ID:
				{
					obj.SetGraphic((ushort)COrionAssistant::TextToGraphic(value.toLower()));
					break;
				}
				case DKI_HUE:
				{
					obj.SetHue((ushort)COrionAssistant::TextToGraphic(value.toLower()));
					break;
				}
				case DKI_COLOR_MIN:
				{
					obj.SetMinColor(COrionAssistant::RawStringToUInt(value.toLower()));
					break;
				}
				case DKI_COLOR_MID:
				{
					obj.SetMidColor(COrionAssistant::RawStringToUInt(value.toLower()));
					break;
				}
				case DKI_COLOR:
				{
					obj.SetColor(COrionAssistant::RawStringToUInt(value.toLower()));
					break;
				}
				case DKI_BACKGROUND:
				{
					obj.SetBackground(COrionAssistant::RawStringToBool(value.toLower()));
					break;
				}
				case DKI_BG_COLOR_MIN:
				{
					obj.SetBackgroundMinColor(COrionAssistant::RawStringToUInt(value.toLower()));
					break;
				}
				case DKI_BG_COLOR_MID:
				{
					obj.SetBackgroundMidColor(COrionAssistant::RawStringToUInt(value.toLower()));
					break;
				}
				case DKI_BG_COLOR:
				{
					obj.SetBackgroundColor(COrionAssistant::RawStringToUInt(value.toLower()));
					break;
				}
				case DKI_MIN_VAULE:
				{
					obj.SetMinValue((int)COrionAssistant::RawStringToUInt(value.toLower()));
					break;
				}
				case DKI_MID_VALUE:
				{
					obj.SetMidValue((int)COrionAssistant::RawStringToUInt(value.toLower()));
					break;
				}
				case DKI_PERCENTS:
				{
					obj.SetPercents(COrionAssistant::RawStringToBool(value.toLower()));
					break;
				}
				default:
					break;
			}

			break;
		}
	}
}
//----------------------------------------------------------------------------------
void CDisplayManager::RecompilleData(const QString &text, const uint &textColor)
{
	OAFUN_DEBUG("c18_f10");
	g_UpdateDisplayOnStep = false;
	m_Commands.clear();

	CompilleData(text, textColor);
}
//----------------------------------------------------------------------------------
void CDisplayManager::CompilleData(const QString &text, const uint &textColor)
{
	OAFUN_DEBUG("c18_f11");
	CTextParser textParset(text.toStdString(), "", "", "{}", true);

	while (!textParset.IsEOF())
	{
		QStringList list = textParset.ReadTokens(false);

		for (QStringList::iterator it = list.begin(); it != list.end(); ++it)
		{
			if (!it->length())
				continue;

			QString str = *it;

			if (str.at(0) == '{')
			{
				CTextParser tagParset(str.toStdString(), " \t{}=", "", "\"\"''", false);

				QStringList tags = tagParset.ReadTokens();

				if (tags.size())
				{
					QString lowerName = (*tags.begin()).toLower();

					if (lowerName == "group")
					{
						if (tags.size() > 1)
							CompilleData(g_TabDisplay->GetGroupContent((*(tags.begin() + 1)).toLower()), textColor);

						continue;
					}

					CDisplayItem tagData = GetDisplayItemByName(lowerName);

					for (QStringList::iterator t = tags.begin() + 1; t != tags.end(); ++t)
					{
						QString tagName = *t;

						++t;

						if (t != tags.end())
							ApplyAttributeToDisplayItem(tagData, tagName.toLower(), *t);
						else
							break;
					}

					if (tagData.GetType() >= DIT_X && tagData.GetType() <= DIT_Z)
						g_UpdateDisplayOnStep = true;

					m_Commands.push_back(tagData);
				}
			}
			else
			{
				m_Commands.push_back(CDisplayItem(DIT_NONE, str, 0, 0, 0, 0, textColor, false, 0, 0, 0, 0, 0, false));
			}
		}
	}
}
//----------------------------------------------------------------------------------
