// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ContextMenuManager.cpp
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "contextmenumanager.h"
#include "../OrionAssistant/Packets.h"
#include "../OrionAssistant/tabmacros.h"
//----------------------------------------------------------------------------------
CContextMenuManager g_ContextMenuManager;
//----------------------------------------------------------------------------------
CContextMenuManager::CContextMenuManager()
{
}
//----------------------------------------------------------------------------------
void CContextMenuManager::SetHook(const uint &id, const uint &index)
{
	LOG("CContextMenuManager::SetHook: 0x%08X %i\n", id, index);
	m_HookEnabled = true;
	m_HookID = id;
	m_HookIndex = index;
}
//----------------------------------------------------------------------------------
void CContextMenuManager::RemoveHook()
{
	m_HookEnabled = false;
}
//----------------------------------------------------------------------------------
bool CContextMenuManager::OnPacketReceived(CDataReader &reader)
{
	ushort mode = reader.ReadUInt16BE();
	bool isNewClilocs = (mode >= 2);
	m_Serial = reader.ReadUInt32BE();

	if (/*!m_HookEnabled && */g_TabMacros->GetPlaying() && g_TabMacros->PlayWaiting(QList<MACRO_TYPE>() << MT_WAIT_FOR_CONTEXT_MENU))
		return false;

	if (m_HookEnabled)
	{
		if (m_HookID && m_Serial != m_HookID)
			return true;

		uchar count = reader.ReadUInt8();

		uint index = m_HookIndex;

		IFOR(i, 0, count)
		{
			if (isNewClilocs)
			{
				reader.Move(4);

				if (!index)
				{
					index = reader.ReadUInt16BE();
					m_HookEnabled = false;
					CPacketContextMenuSelection(m_Serial, index).SendServer();
					return false;
				}

				index--;

				reader.Move(2);
				reader.Move(2);
			}
			else
			{
				if (!index)
				{
					index = reader.ReadUInt16BE();
					m_HookEnabled = false;
					CPacketContextMenuSelection(m_Serial, index).SendServer();
					return false;
				}

				index--;

				reader.Move(2);
				ushort flags = reader.ReadUInt16BE();

				if (flags & 0x84)
					reader.Move(2);

				if (flags & 0x40)
					reader.Move(2);

				if (flags & 0x20)
					reader.Move(2);
			}
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
bool CContextMenuManager::OnPacketSend(CDataReader &reader)
{
	Q_UNUSED(reader);
	return true;
}
//----------------------------------------------------------------------------------
