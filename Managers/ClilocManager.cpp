﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ClilocManager.cpp
**
** Copyright (C) June 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "ClilocManager.h"
//----------------------------------------------------------------------------------
CClilocManager g_ClilocManager;
//----------------------------------------------------------------------------------
CClilocManager::CClilocManager()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CClilocManager::~CClilocManager()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
QString CClilocManager::Load(uint &id)
{
	OAFUN_DEBUG("");
	QString result = "";

	m_ClilocEnu.ResetPtr();
	m_ClilocEnu.Move(6);

	while (!m_ClilocEnu.IsEOF())
	{
		uint currentID = m_ClilocEnu.ReadUInt32LE();

		m_ClilocEnu.Move(1);

		short len = m_ClilocEnu.ReadUInt16LE();

		if (currentID == id)
		{
			result = m_ClilocEnu.ReadString(len).c_str();

			if (id >= 3000000)
				m_ClilocSupport[currentID] = result;
			else if (id >= 1000000)
				m_ClilocRegular[currentID] = result;
			else
				m_ClilocSystem[currentID] = result;

			break;
		}
		else
			m_ClilocEnu.Move(len);
	}

	id = 0;

	return result;
}
//----------------------------------------------------------------------------------
QString CClilocManager::CamelCaseTest(const bool &toCamelCase, QString result)
{
	OAFUN_DEBUG("");
	if (toCamelCase)
	{
		int offset = 'a' - 'A';
		bool lastSpace = true;

		for (QChar &c : result)
		{
			if (c.cell() == ' ')
				lastSpace = true;
			else if (lastSpace)
			{
				lastSpace = false;

				if (c.cell() >= 'a' && c.cell() <= 'z')
					c.setCell(c.cell() - offset);
			}
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
QString CClilocManager::Get(const uint &id, const bool &toCamelCase, QString result)
{
	OAFUN_DEBUG("");
	if (id >= 3000000)
	{
		CLILOC_MAP::iterator i = m_ClilocSupport.find(id);
		if (i != m_ClilocSupport.end() && i.value().length())
			return CamelCaseTest(toCamelCase, i.value());
	}
	else if (id >= 1000000)
	{
		CLILOC_MAP::iterator i = m_ClilocRegular.find(id);
		if (i != m_ClilocRegular.end() && i.value().length())
			return CamelCaseTest(toCamelCase, i.value());
	}
	else
	{
		CLILOC_MAP::iterator i = m_ClilocSystem.find(id);
		if (i != m_ClilocSystem.end() && i.value().length())
			return CamelCaseTest(toCamelCase, i.value());
	}

	uint tmpID = id;
	QString loadStr = Load(tmpID);

	if (!tmpID && loadStr.length())
		return CamelCaseTest(toCamelCase, loadStr);
	else if (!result.length())
		result = "Unknown Cliloc #" + QString::number(id);

	return CamelCaseTest(toCamelCase, result);
}
//----------------------------------------------------------------------------------
QString CClilocManager::ParseArgumentsToClilocString(const uint &cliloc, const bool &toCamelCase, wstring args)
{
	OAFUN_DEBUG("");
	while (args.length() && args[0] == L'\t')
		args.erase(args.begin());

	vector<wstring> arguments;

	while (true)
	{
		size_t pos = args.find(L"\t");

		if (pos != string::npos)
		{
			arguments.push_back(args.substr(0, pos));
			args = args.substr(pos + 1);
		}
		else
		{
			arguments.push_back(args);
			break;
		}
	}

	wstring message = Get(cliloc, toCamelCase).toStdWString();

	IFOR(i, 0, (int)arguments.size())
	{
		size_t pos1 = message.find(L"~");

		if (pos1 == string::npos)
			break;

		size_t pos2 = message.find(L"~", pos1 + 1);

		if (pos2 == string::npos)
			break;

		if (arguments[i].length() > 1 && *arguments[i].c_str() == L'#')
		{
			uint id = _wtoi(arguments[i].c_str() + 1);
			arguments[i] = Get(id, toCamelCase).toStdWString();
		}

		message.replace(pos1, pos2 - pos1 + 1, arguments[i]);
	}

	return CamelCaseTest(toCamelCase, QString::fromStdWString(message));
}
//----------------------------------------------------------------------------------
