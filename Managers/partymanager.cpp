// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** PartyManager.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "partymanager.h"
#include "../OrionAssistant/Packets.h"
#include "journalmanager.h"
#include "../OrionAssistant/tabagentsparty.h"
#include "../OrionAssistant/orionmap.h"
//----------------------------------------------------------------------------------
CPartyManager g_PartyManager;
//----------------------------------------------------------------------------------
CPartyManager::CPartyManager()
{
	OAFUN_DEBUG("");
	memset(&Member[0], 0, sizeof(Member));
}
//----------------------------------------------------------------------------------
bool CPartyManager::Contains(const uint &serial)
{
	OAFUN_DEBUG("");
	bool result = false;

	if (m_Leader != 0)
	{
		IFOR(i, 0, 10)
		{
			if (Member[i] == serial)
			{
				result = true;

				break;
			}
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
void CPartyManager::SaveMemberInfo(CGameCharacter *obj)
{
	IFOR(i, 0, 10)
	{
		if (Member[i] == obj->GetSerial())
		{
			m_MemberInfo[i].X = obj->GetX();
			m_MemberInfo[i].Y = obj->GetY();
			m_MemberInfo[i].Name = obj->GetName();

			if (g_OrionMap != nullptr && g_OrionMap->isVisible())
				g_OrionMap->CheckForUpdate(obj->GetSerial());

			break;
		}
	}
}
//----------------------------------------------------------------------------------
void CPartyManager::SetMemberInfo(const PARTY_MEMBER_INFO &info)
{
	IFOR(i, 0, 10)
	{
		if (Member[i] == info.Serial)
		{
			m_MemberInfo[i].Serial = info.Serial;
			m_MemberInfo[i].X = info.X;
			m_MemberInfo[i].Y = info.Y;
			m_MemberInfo[i].Map = info.Map;

			if (g_OrionMap != nullptr && g_OrionMap->isVisible())
				g_OrionMap->CheckForUpdate(info.Serial);

			break;
		}
	}
}
//----------------------------------------------------------------------------------
void CPartyManager::Clear()
{
	OAFUN_DEBUG("");
	IFOR(i, 0, 10)
		Member[i] = 0;
}
//----------------------------------------------------------------------------------
bool CPartyManager::OnPacketReceived(CDataReader &reader)
{
	OAFUN_DEBUG("");
	if (!EnabledFeaturePartyAgent())
		return true;

	uchar subCmd = reader.ReadUInt8();

	switch (subCmd)
	{
		case 1: //Add member
		case 2: //Remove member
		{
			Clear();

			uchar count = reader.ReadUInt8();

			if (subCmd == 1)
			{
				if (m_Leader == 0)
				{
					m_Leader = g_PlayerSerial;
					m_Inviter = 0;
				}
			}
			else if (count <= 1)
			{
				m_Leader = 0;
				m_Inviter = 0;

				break;
			}

			if (count > 10)
				count = 10;

			IFOR(i, 0, count)
				Member[i] = reader.ReadUInt32BE();

			break;
		}
		case 3: //Private party message
		case 4: //Party message
		{
			uint serial = reader.ReadUInt32BE();
			wstring str = reader.ReadWString(0, true);

			g_JournalManager.Add(CJournalMessage(serial, 0x0044, QString().fromStdWString(str)));

			break;
		}
		case 7:
		{
			uint serial = reader.ReadUInt32BE();
			m_Inviter = reader.ReadUInt32BE();

			if (g_TabAgentsParty->PartyInviteCanBeAutoAccepted(serial))
				CPacketPartyAccept(serial).SendServer();
			else if (g_TabAgentsParty->PartyInviteCanBeAutoDeclined(serial))
				CPacketPartyDecline(serial).SendServer();

			break;
		}
		default:
			break;
	}

	return true;
}
//----------------------------------------------------------------------------------
