//----------------------------------------------------------------------------------
#ifndef DATAREADER_H
#define DATAREADER_H
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CDataReader
{
	SETGET(puchar, Start, nullptr)
	SETGET(int, Size, 0)
	SETGET(puchar, End, nullptr)
	SETGET(puchar, Ptr, nullptr)
	SETGET(uint, Hash, 0)

public:
	CDataReader();
	CDataReader(puchar start, const int &size);

	virtual ~CDataReader();

	void SetData(puchar start, const int &size, const int &offset = 0);
	void ResetPtr() { m_Ptr = m_Start; }

	bool IsEOF() { return m_Ptr >= m_End; }

	void Move(const int &offset) { m_Ptr += offset; }

	void ReadDataBE(puchar data, const int &size, const int offset = 0);
	void ReadDataLE(puchar data, const int &size, const int offset = 0);

	uchar ReadUInt8(const int &offset = 0) { uchar val = 0; ReadDataBE((puchar)&val, sizeof(uchar), offset); return val; }

	ushort ReadUInt16BE(const int &offset = 0) { ushort val = 0; ReadDataBE((puchar)&val, sizeof(ushort), offset); return val; }
	ushort ReadUInt16LE(const int &offset = 0) { ushort val = 0; ReadDataLE((puchar)&val, sizeof(ushort), offset); return val; }

	uint ReadUInt32BE(const int &offset = 0) { uint val = 0; ReadDataBE((puchar)&val, sizeof(uint), offset); return val; }
	uint ReadUInt32LE(const int &offset = 0) { uint val = 0; ReadDataLE((puchar)&val, sizeof(uint), offset); return val; }

	char ReadInt8(const int &offset = 0) { char val = 0; ReadDataBE((puchar)&val, sizeof(char), offset); return val; }

	short ReadInt16BE(const int &offset = 0) { short val = 0; ReadDataBE((puchar)&val, sizeof(short), offset); return val; }
	short ReadInt16LE(const int &offset = 0) { short val = 0; ReadDataLE((puchar)&val, sizeof(short), offset); return val; }

	int ReadInt32BE(const int &offset = 0) { int val = 0; ReadDataBE((puchar)&val, sizeof(int), offset); return val; }
	int ReadInt32LE(const int &offset = 0) { int val = 0; ReadDataLE((puchar)&val, sizeof(int), offset); return val; }

	__int64 ReadInt64BE(const int &offset = 0) { __int64 val = 0; ReadDataBE((puchar)&val, sizeof(__int64), offset); return val; }
	__int64 ReadInt64LE(const int &offset = 0) { __int64 val = 0; ReadDataLE((puchar)&val, sizeof(__int64), offset); return val; }

	float ReadFloatBE(const int &offset = 0) { float val = 0.0f; ReadDataBE((puchar)&val, sizeof(float), offset); return val; }
	float ReadFloatLE(const int &offset = 0) { float val = 0.0f; ReadDataLE((puchar)&val, sizeof(float), offset); return val; }

	double ReadDoubleBE(const int &offset = 0) { double val = 0.0; ReadDataBE((puchar)&val, sizeof(double), offset); return val; }
	double ReadDoubleLE(const int &offset = 0) { double val = 0.0; ReadDataLE((puchar)&val, sizeof(double), offset); return val; }

	string ReadString(int size = 0, const int &offset = 0);
	wstring ReadWString(int size = 0, const bool &bigEndian = true, const int &offset = 0);
};
//----------------------------------------------------------------------------------
#endif

