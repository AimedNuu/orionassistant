/***********************************************************************************
**
** SpellManager.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SPELLMANAGER_H
#define SPELLMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
typedef QMap<QString, int> SPELLS_MAP;
//----------------------------------------------------------------------------------
class CSpellManager
{
private:
	SPELLS_MAP m_Spells;

public:
	CSpellManager();
	~CSpellManager();

	QList<QStringList> m_SpellbookSpellNames;

	void Insert(const QString &name, const int &index);

	int Find(QString name);

	void Cast(const QString &name);

	void TargetCast(const QString &name, const uint &serial);
};
//----------------------------------------------------------------------------------
extern CSpellManager g_SpellManager;
//----------------------------------------------------------------------------------
#endif // SPELLMANAGER_H
//----------------------------------------------------------------------------------
