/***********************************************************************************
**
** TextCommandManager.h
**
** Copyright (C) January 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TEXTCOMMANDMANAGER_H
#define TEXTCOMMANDMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../Script/command.h"
#include "TextParser.h"
#include <QMap>
//----------------------------------------------------------------------------------
typedef QMap<QString, CCommand> COMMANDS_MAP;
#define HANDLER_COMMAND(name) void Command ##name (const COMMAND_ARGS &args, const CCommand &command)
//----------------------------------------------------------------------------------
class CTextCommandManager
{
private:
	static COMMANDS_MAP m_Commands;

	void CommandTok(COMMAND_ARGS &args, const QString &in);

public:
	CTextCommandManager();
	~CTextCommandManager();

	static void InitCommands();

	void DoCommand(const QString &text);

	void UpdateCommands();

	HANDLER_COMMAND(info);
	HANDLER_COMMAND(infotile);
	HANDLER_COMMAND(saveconfig);
	HANDLER_COMMAND(setdressbag);
	HANDLER_COMMAND(unsetdressbag);
	HANDLER_COMMAND(getstatus);
	HANDLER_COMMAND(click);
	HANDLER_COMMAND(useobject);
	HANDLER_COMMAND(attack);
	HANDLER_COMMAND(usetype);
	HANDLER_COMMAND(usefromground);
	HANDLER_COMMAND(textopen);
	HANDLER_COMMAND(textclose);
	HANDLER_COMMAND(textclear);
	HANDLER_COMMAND(textprint);
	HANDLER_COMMAND(terminate);
	HANDLER_COMMAND(setlight);
	HANDLER_COMMAND(setweather);
	HANDLER_COMMAND(setseason);
	HANDLER_COMMAND(cast);
	HANDLER_COMMAND(useskill);
	HANDLER_COMMAND(helpgump);
	HANDLER_COMMAND(closeuo);
	HANDLER_COMMAND(warmode);
	HANDLER_COMMAND(morph);
	HANDLER_COMMAND(resend);
	HANDLER_COMMAND(sound);
	HANDLER_COMMAND(emoteaction);
	HANDLER_COMMAND(hide);
	HANDLER_COMMAND(blockmoving);
	HANDLER_COMMAND(drop);
	HANDLER_COMMAND(drophere);
	HANDLER_COMMAND(moveitem);
	HANDLER_COMMAND(setarm);
	HANDLER_COMMAND(unsetarm);
	HANDLER_COMMAND(setdress);
	HANDLER_COMMAND(unsetdress);
	HANDLER_COMMAND(savehotkeys);
	HANDLER_COMMAND(loadhotkeys);
	HANDLER_COMMAND(track);
	HANDLER_COMMAND(infomenu);
	HANDLER_COMMAND(showjournal);
	HANDLER_COMMAND(clearjournal);
	HANDLER_COMMAND(journalignorecase);
	HANDLER_COMMAND(ignorereset);
	HANDLER_COMMAND(infogump);
};
//----------------------------------------------------------------------------------
extern CTextCommandManager g_TextCommandManager;
//----------------------------------------------------------------------------------
#endif // TEXTCOMMANDMANAGER_H
//----------------------------------------------------------------------------------
