﻿/***********************************************************************************
**
** PacketManager.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef PACKETMANAGER_H
#define PACKETMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "DataReader.h"
//----------------------------------------------------------------------------------
class CPacketManager;
typedef bool (CPacketManager::*PACKET_FUNCTION)();
//----------------------------------------------------------------------------------
//!Направление пакета
enum PACKET_DIRECTION
{
	DIR_SEND = 0,	//!От клиента серверу
	DIR_RECV,		//!От сервера клиенту
	DIR_BOTH		//!В обе стороны
};
//----------------------------------------------------------------------------------
//!Класс для хранения информации о пакетах
class CPacketInfo
{
public:
	//!Название пакета
	const char *Name;

	//!Размер пакета
	int Size;

	//!Направление пакета
	PACKET_DIRECTION Direction;

	//!Обработчик отправленного пакета
	PACKET_FUNCTION SendHandler;

	//!Обработчик принятого пакета
	PACKET_FUNCTION RecvHandler;
};
//----------------------------------------------------------------------------------
#define HANDLER_PACKET(name)bool Handle##name()
//----------------------------------------------------------------------------------
class CPacketManager : public CDataReader
{
	SETGET(CLIENT_VERSION, ClientVersion, CV_OLD)

private:
	static CPacketInfo m_Packets[0x100];

protected:
	//!Обработчики пакетов
	HANDLER_PACKET(SecondLogin);
	HANDLER_PACKET(LightLevel);
	HANDLER_PACKET(PersonalLightLevel);
	HANDLER_PACKET(LoginComplete);
	HANDLER_PACKET(EnterWorld);
	HANDLER_PACKET(UpdateHitpoints);
	HANDLER_PACKET(UpdateMana);
	HANDLER_PACKET(UpdateStamina);
	HANDLER_PACKET(MobileAttributes);
	HANDLER_PACKET(NewHealthbarUpdate);
	HANDLER_PACKET(UpdatePlayer);
	HANDLER_PACKET(CharacterStatus);
	HANDLER_PACKET(UpdateItem);
	HANDLER_PACKET(UpdateItemSA);
	HANDLER_PACKET(UpdateObject);
	HANDLER_PACKET(EquipItem);
	HANDLER_PACKET(UpdateContainedItem);
	HANDLER_PACKET(UpdateContainedItems);
	HANDLER_PACKET(DeleteObject);
	HANDLER_PACKET(UpdateCharacter);
	HANDLER_PACKET(WarmodeS);
	HANDLER_PACKET(WarmodeR);
	HANDLER_PACKET(UpdateSkillsS);
	HANDLER_PACKET(UpdateSkillsR);
	HANDLER_PACKET(TargetS);
	HANDLER_PACKET(TargetR);
	HANDLER_PACKET(ClientTalkS);
	HANDLER_PACKET(ClientTalkR);
	HANDLER_PACKET(UnicodeClientTalk);
	HANDLER_PACKET(Talk);
	HANDLER_PACKET(UnicodeTalk);
	HANDLER_PACKET(SetWeather);
	HANDLER_PACKET(Season);
	HANDLER_PACKET(DisplayDeath);
	HANDLER_PACKET(MultiPlacement);
	HANDLER_PACKET(OpenMenu);
	HANDLER_PACKET(MenuChoice);
	HANDLER_PACKET(DeathScreenS);
	HANDLER_PACKET(DeathScreenR);
	HANDLER_PACKET(OrionMessagesS);
	HANDLER_PACKET(OrionMessagesR);
	HANDLER_PACKET(OpenContainer);
	HANDLER_PACKET(BuffDebuff);
	HANDLER_PACKET(AttackCharacter);
	HANDLER_PACKET(ExtendedCommandS);
	HANDLER_PACKET(ExtendedCommandR);
	HANDLER_PACKET(SecureTradingS);
	HANDLER_PACKET(SecureTradingR);
	HANDLER_PACKET(DyeDataS);
	HANDLER_PACKET(DyeDataR);
	HANDLER_PACKET(OpenGump);
	HANDLER_PACKET(ChoiceGump);
	HANDLER_PACKET(PlaySoundEffect);
	HANDLER_PACKET(GraphicEffect);
	HANDLER_PACKET(CharacterAnimation);
	HANDLER_PACKET(DisplayMap);
	HANDLER_PACKET(DisplayClilocString);
	HANDLER_PACKET(MegaClilocS);
	HANDLER_PACKET(MegaClilocR);
	HANDLER_PACKET(Resend);
	HANDLER_PACKET(WalkRequest);
	HANDLER_PACKET(ConfirmWalk);
	HANDLER_PACKET(ASCIIPromptS);
	HANDLER_PACKET(ASCIIPromptR);
	HANDLER_PACKET(UnicodePromptS);
	HANDLER_PACKET(UnicodePromptR);
	HANDLER_PACKET(CharacterProfileS);
	HANDLER_PACKET(CharacterProfileR);
	HANDLER_PACKET(OpenPaperdoll);
	HANDLER_PACKET(BuyList);
	HANDLER_PACKET(BuyReplyS);
	HANDLER_PACKET(BuyReplyR);
	HANDLER_PACKET(SellList);
	HANDLER_PACKET(SellReply);
	HANDLER_PACKET(KrriosClientSpecialS);
	HANDLER_PACKET(KrriosClientSpecialR);
	HANDLER_PACKET(PerformAction);
	HANDLER_PACKET(DoubleClick);
	HANDLER_PACKET(AttackRequest);
	HANDLER_PACKET(HelpRequest);
	HANDLER_PACKET(AOSCommandsS);
	HANDLER_PACKET(AOSCommandsR);

	//Не обработаны
	/*
	HANDLER_PACKET(EnableLockedFeatures);
	HANDLER_PACKET(DenyWalk);
	HANDLER_PACKET(OpenUrl);
	HANDLER_PACKET(CorpseEquipment);
	HANDLER_PACKET(DisplayQuestArrow);
	HANDLER_PACKET(AssistVersion);
	HANDLER_PACKET(CharacterListNotification);
	HANDLER_PACKET(ErrorCode);
	HANDLER_PACKET(OpenChat);
	HANDLER_PACKET(TextEntryDialog);

	0x15 BMSG("Follow", 0x09),
	0x2D RMSG("Mob Attributes", 0x11),
	0x2F RMSG("Combat Notification", 0x0a),
	0x97 RMSG("Move Player", 0x02),
	0xB2 BMSG("Chat Data", SIZE_VARIABLE),
	0xB3 RMSG("Chat Text ?", SIZE_VARIABLE),
	0xB5 BMSG("Open Chat Window", 0x40),
	0xB7 RMSG("Popup Help Data", SIZE_VARIABLE),
	0xD7 BMSG("+AoS command",SIZE_VARIABLE),
	0xD8 RMSG("+Custom house",SIZE_VARIABLE),
	0xDC RMSG("OPL Info Packet", 9),
	0xE2 RMSG("New Character Animation", 0xa),
	*/

public:
	CPacketManager();
	virtual ~CPacketManager();

	CPacketInfo GetInfo(const uchar &buf) const { return m_Packets[buf]; }

	bool PacketRecv(uchar*, const int&);
	bool PacketSend(uchar*, const int&);
};
//---------------------------------------------------------------------------
extern CPacketManager g_PacketManager;
//---------------------------------------------------------------------------
#endif
