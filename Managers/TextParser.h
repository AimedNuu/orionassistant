﻿//----------------------------------------------------------------------------------
#ifndef TEXTPARSER_H
#define TEXTPARSER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "DataReader.h"
#include <QStringList>
//----------------------------------------------------------------------------------
class CTextParser
{
	SETGET(string, RawLine, "")
	SETGET(bool, SaveQuotes, false)

private:
	//Указатель на адрес в памяти, куда загружен файл
	string m_Text;

	//Указатель на текущий обрабатываемый символ
	puchar m_Ptr;

	//Указатель на начало предыдущей обрабатываемой строки
	puchar m_StartPtr;

	//Адрес конца строка
	puchar m_EOL;

	//Адрес конца файла
	puchar m_End;

	CDataReader m_File;

	//Разделители
	char m_Delimiters[50];

	//Количество разделителей
	int m_DelimitersSize;

	//Комментарии
	char m_Comentaries[50];

	//Количество коментариев
	int m_ComentariesSize;

	//Набор кавычек, формат кавычек в наборе: openQuote, closeQuote (могут быть одинаковыми)
	char m_Quotes[50];

	//Количество элементов в наборе кавычек
	int m_QuotesSize;

	//Обрезать строки на выходе
	bool m_Trim{ true };

	//Получить конец строки
	void GetEOL();

	//Проверка на разделитель
	bool IsDelimiter();

	//Пропустить все до данных
	void SkipToData();

	//Проверка на комментарий
	bool IsComment();

	//Проверка на кавычку
	bool IsQuote();

	//Проверка на закрывающую кавычку
	bool IsSecondQuote();

	//Получить следующий токен
	QString ObtainData();

	//Получить следующий фрагмент строки или токен (если кавычки не обнаружены)
	QString ObtainQuotedData();

	void SaveRawLine();

public:
	CTextParser(const string &text, const char *delimiters = "", const char *comentaries = "", const char *quotes = "", const bool &saveQuotes = false);
	~CTextParser();

	//Сбросить указатель на старт
	void Restart();

	//Проверка на конец файла
	bool IsEOF();

	void SavePosition();

	void LoadPosition();

	//Прочитать токены из файла
	QStringList ReadTokens(bool trim = true);

	//Прочитать токены из строки
	QStringList GetTokens(const char *str, bool trim = true);
};
//----------------------------------------------------------------------------------
#endif

