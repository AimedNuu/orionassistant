﻿//----------------------------------------------------------------------------------
#ifndef MAPPEDFILE_H
#define MAPPEDFILE_H
#include "DataReader.h"
//----------------------------------------------------------------------------------
class CMappedFile : public CDataReader
{
	HANDLE m_File;
	HANDLE m_Map;

	bool Load();

public:
	CMappedFile();

	virtual ~CMappedFile();

	bool Load(const QString &path);

	void Unload();
};
//----------------------------------------------------------------------------------
extern QString g_MappedFileError;
//----------------------------------------------------------------------------------
#endif

