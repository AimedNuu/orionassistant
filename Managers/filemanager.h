/***********************************************************************************
**
** FileManager.h
**
** Copyright (C) April 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef FILEMANAGER_H
#define FILEMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "MappedFile.h"
//----------------------------------------------------------------------------------
class CIndexMap
{
	SETGET(uint, OriginalMapAddress, 0)
	SETGET(uint, OriginalStaticAddress, 0)
	SETGET(uint, OriginalStaticCount, 0)

	SETGET(uint, MapAddress, 0)
	SETGET(uint, StaticAddress, 0)
	SETGET(uint, StaticCount, 0)

public:
	CIndexMap() {}
	virtual ~CIndexMap() {}
};
//----------------------------------------------------------------------------------
class CMapPatchInfo
{
	SETGET(uint, MapPatchesCount, 0)
	SETGET(uint, StaticPatchesCount, 0)

public:
	CMapPatchInfo() {}
	virtual ~CMapPatchInfo() {}

	void Reset()
	{
		m_MapPatchesCount = 0;
		m_StaticPatchesCount = 0;
	}
};
//----------------------------------------------------------------------------------
class CSearchTileInfo
{
	SETGET(ushort, Start, 0)
	SETGET(ushort, End, 0)
	SETGET(bool, Land, false)

public:
	CSearchTileInfo() {}
	CSearchTileInfo(const ushort &start, const ushort &end, const bool &land)
		: m_Start(start), m_End(end), m_Land(land) {}
	~CSearchTileInfo() {}
};
//----------------------------------------------------------------------------------
class CTileInfo
{
	SETGET(ushort, Graphic, 0)
	SETGET(short, X, 0)
	SETGET(short, Y, 0)
	SETGET(char, Z, 0)
	SETGET(bool, Land, true)

private:
	bool TestTile(const QList<CSearchTileInfo> &list);

public:
	CTileInfo() {}
	CTileInfo(const ushort &graphic, const short &x, const short &y, const char &z, const bool &land)
		: m_Graphic(graphic), m_X(x), m_Y(y), m_Z(z), m_Land(land) {}
	~CTileInfo() {}

	bool CanBeAdded(const ushort &graphic, const ushort &flags);
};
//----------------------------------------------------------------------------------
enum TILE_NAME_FLAGS
{
	TNF_BY_GRAPHIC	= 0x00,
	TNF_MINE		= 0x01,
	TNF_TREE		= 0x02,
	TNF_WATER		= 0x04,
	TNF_LAND		= 0x08,
	TNF_ANY			= 0x10
};
//----------------------------------------------------------------------------------
class CRawDataInfo
{
	SETGET(bool, Loaded, false)
	SETGET(size_t, Address, 0)
	SETGET(size_t, Size, 0)
	SETGET(size_t, CompressedSize, 0)

public:
	CRawDataInfo() {}
	virtual ~CRawDataInfo() {}
};
//----------------------------------------------------------------------------------
class CGumpDataInfo : public CRawDataInfo
{
	SETGET(ushort, Width, 0)
	SETGET(ushort, Height, 0)

public:
	CGumpDataInfo() : CRawDataInfo() {}
	virtual ~CGumpDataInfo() {}
};
//----------------------------------------------------------------------------------
typedef QVector<CIndexMap> MAP_INDEX_LIST;
//----------------------------------------------------------------------------------
class CFileManager
{
	SETGET(uint, MaxBlockIndex, 0)

private:
	uint m_CRC_Table[256];

	CIndexMap *m_BlockData{ nullptr };

	MAP_INDEX_LIST m_BlockDataV[6];

	void LoadTiledata();

	void LoadPatches(const bool &mapOnly);

	void ResetMapPatches();

	void ReplacePatchedAddresses();

	ushort TileNameToFlags(QString tileName, ushort &graphic);

	USHORT_LIST m_Radarcol;

	CRawDataInfo m_StaticArtIndex[MAX_STATIC_DATA_INDEX_COUNT];
	CGumpDataInfo m_GumpArtIndex[MAX_GUMP_DATA_INDEX_COUNT];

public:
	CFileManager();
	virtual ~CFileManager();

	uint GetFileHashCode(puchar ptr, size_t size);

	QPoint m_MapSize[6];
	QPoint m_MapBlockSize[6];

	CMapPatchInfo m_PatchInfo[6];

	const CRawDataInfo &GetStaticArtInfo(const ushort &index);

	CDataReader m_File[OFI_FILES_COUNT];

	uint Color16To32(const ushort &color);

	ushort GetRadarcolData(const uint &graphic);

	CIndexMap *GetIndex(const uint &map, const uint &x, const uint &y);

	void LoadFiles();
	void UnloadFiles();

	QList<LAND_GROUP> m_LandTiledata;
	QList<STATIC_GROUP> m_StaticTiledata;

	QList<CSearchTileInfo> m_MineTiles;
	QList<CSearchTileInfo> m_TreeTiles;
	QList<CSearchTileInfo> m_WaterTiles;

	int m_LandDataCount{ 0 };
	int m_StaticDataCount{ 0 };

	void OnMapChange();

	void ApplyPatches(CDataReader &stream);

	int GetActualMap();

	QList<CTileInfo> GetTile(const int &x, const int &y, const int &z, QString tileName);

	unsigned long long CreateHash(string s);
};
//----------------------------------------------------------------------------------
extern CFileManager g_FileManager;
//----------------------------------------------------------------------------------
#endif // FILEMANAGER_H
//----------------------------------------------------------------------------------
