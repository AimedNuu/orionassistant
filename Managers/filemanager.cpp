﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** FileManager.cpp
**
** Copyright (C) April 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "filemanager.h"
#include "MappedFile.h"
#include "PacketManager.h"
#include "TextParser.h"
#include "../OrionAssistant/orionassistant.h"
#include "../OrionAssistant/orionmap.h"
#include "ClilocManager.h"
#include <unordered_map>

CFileManager g_FileManager;
//----------------------------------------------------------------------------------
uint Reflect(uint source, int c)
{
	uint value = 0;

	IFOR(i, 1, c + 1)
	{
		if (source & 0x1)
			value |= (1 << (c - i));

		source >>= 1;
	}

	return value;
}
//----------------------------------------------------------------------------------
CFileManager::CFileManager()
{
	OAFUN_DEBUG("");
	IFOR(i, 0, 256)
	{
		m_CRC_Table[i] = Reflect((int)i, 8) << 24;

		IFOR(j, 0, 8)
			m_CRC_Table[i] = (m_CRC_Table[i] << 1) ^ ((m_CRC_Table[i] & (1 << 31)) ? 0x04C11DB7 : 0);

		m_CRC_Table[i] = Reflect(m_CRC_Table[i], 32);
	}
}
//----------------------------------------------------------------------------------
CFileManager::~CFileManager()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
uint CFileManager::GetFileHashCode(puchar ptr, size_t size)
{
	uint crc = 0xFFFFFFFF;

	while (size > 0)
	{
		crc = (crc >> 8) ^ m_CRC_Table[(crc & 0xFF) ^ *ptr];

		ptr++;
		size--;
	}

	return (crc & 0xFFFFFFFF);
}
//----------------------------------------------------------------------------------
uint CFileManager::Color16To32(const ushort &color)
{
	return
	(
		((((color >> 10) & 0x1F) * 0xFF / 0x1F) << 16) |
		((((color >> 5) & 0x1F) * 0xFF / 0x1F) << 8) |
		((color & 0x1F) * 0xFF / 0x1F)
	);
}
//----------------------------------------------------------------------------------
const CRawDataInfo &CFileManager::GetStaticArtInfo(const ushort &index)
{
	CRawDataInfo &staticInfo = m_StaticArtIndex[index];

	if (!staticInfo.GetLoaded())
	{
		ORION_RAW_ART_INFO info;
		g_ClientInterface->Client->FileManager->GetStaticArtInfo(index, info);

		staticInfo.SetLoaded(true);
		staticInfo.SetAddress(info.Address);
		staticInfo.SetSize(info.Size);
		staticInfo.SetCompressedSize(info.CompressedSize);
	}

	return m_StaticArtIndex[index];
}
//----------------------------------------------------------------------------------
ushort CFileManager::GetRadarcolData(const uint &graphic)
{
	if (graphic < (uint)m_Radarcol.size())
		return m_Radarcol[graphic];

	return 0;
}
//----------------------------------------------------------------------------------
CIndexMap *CFileManager::GetIndex(const uint &map, const uint &x, const uint &y)
{
	uint index = (x * m_MapBlockSize[map].y()) + y;

	if (map < 6 && index < m_MaxBlockIndex)
		return &m_BlockData[index];

	return nullptr;
}
//----------------------------------------------------------------------------------
void CFileManager::LoadFiles()
{
	OAFUN_DEBUG("");

	IFileManager *intr = g_ClientInterface->Client->FileManager;

	if (intr == nullptr)
		return;

	IFOR(i, OFI_MAP_0_MUL, OFI_FILES_COUNT)
	{
		ORION_RAW_FILE_INFO info;

		intr->GetFileInfo(i, info);

		if (i == OFI_CLILOC_MUL && info.Extra == 'ENU')
			g_ClilocManager.m_ClilocEnu.SetData((puchar)info.Address, info.Size);
		else
		{
			m_File[i].SetData((puchar)info.Address, info.Size);
			m_File[i].SetHash(GetFileHashCode((puchar)info.Address, info.Size));
		}
	}

	LoadTiledata();

	if (m_File[OFI_RADARCOL_MUL].GetSize())
	{
		m_Radarcol.resize(m_File[OFI_RADARCOL_MUL].GetSize() / 2);
		memcpy(&m_Radarcol[0], (PVOID)m_File[OFI_RADARCOL_MUL].GetStart(), m_File[OFI_RADARCOL_MUL].GetSize());
	}

	IFOR(i, 0, 6)
	{
		uint value = i;
		value = GetInt(VKI_GET_MAP_SIZE, value);
		m_MapSize[i].setX((value >> 16) & 0xFFFF);
		m_MapSize[i].setY(value & 0xFFFF);

		value = i;
		value = GetInt(VKI_GET_MAP_BLOCK_SIZE, value);
		m_MapBlockSize[i].setX((value >> 16) & 0xFFFF);
		m_MapBlockSize[i].setY(value & 0xFFFF);
	}

	OnMapChange();

	LoadPatches(false);
}
//----------------------------------------------------------------------------------
void CFileManager::UnloadFiles()
{
	OAFUN_DEBUG("");
	if (m_BlockData != nullptr)
	{
		delete []m_BlockData;
		m_BlockData = nullptr;
	}

	m_MaxBlockIndex = 0;
}
//----------------------------------------------------------------------------------
void CFileManager::LoadTiledata()
{
	OAFUN_DEBUG("");
	CDataReader &tiledata = m_File[OFI_TILEDATA_MUL];

	if (tiledata.GetStart())
	{
		tiledata.ResetPtr();

		bool isOldTiledataVersion = (g_PacketManager.GetClientVersion() < CV_7090);
		int staticsCount = 512;

		if (isOldTiledataVersion)
			staticsCount = (tiledata.GetSize() - (512 * sizeof(LAND_GROUP_OLD))) / sizeof(STATIC_GROUP_OLD);
		else
			staticsCount = (tiledata.GetSize() - (512 * sizeof(LAND_GROUP_NEW))) / sizeof(STATIC_GROUP_NEW);

		m_LandDataCount = 512 * 32;
		m_StaticDataCount = staticsCount * 32;

		IFOR(i, 0, 512)
		{
			LAND_GROUP group;
			group.Unknown = tiledata.ReadUInt32LE();

			IFOR(j, 0, 32)
			{
				LAND_TILES &tile = group.Tiles[j];

				if (isOldTiledataVersion)
					tile.Flags = tiledata.ReadUInt32LE();
				else
					tile.Flags = tiledata.ReadInt64LE();

				tile.TexID = tiledata.ReadUInt16LE();
				tile.Name = tiledata.ReadString(20);
			}

			m_LandTiledata.push_back(group);
		}

		IFOR(i, 0, staticsCount)
		{
			STATIC_GROUP group;
			group.Unk = tiledata.ReadUInt32LE();

			IFOR(j, 0, 32)
			{
				STATIC_TILES &tile = group.Tiles[j];

				if (isOldTiledataVersion)
					tile.Flags = tiledata.ReadUInt32LE();
				else
					tile.Flags = tiledata.ReadInt64LE();

				tile.Weight = tiledata.ReadInt8();
				tile.Quality = tiledata.ReadInt8();
				tile.Unknown = tiledata.ReadInt16LE();
				tile.Unknown1 = tiledata.ReadInt8();
				tile.Quality1 = tiledata.ReadInt8();
				tile.AnimID = tiledata.ReadInt16LE();
				tile.Unknown2 = tiledata.ReadInt8();
				tile.Hue = tiledata.ReadInt8();
				tile.SittingOffset = tiledata.ReadInt8();
				tile.Unknown3 = tiledata.ReadInt8();
				tile.Height = tiledata.ReadInt8();
				tile.Name = tiledata.ReadString(20);
			}

			m_StaticTiledata.push_back(group);
		}
	}
}
//----------------------------------------------------------------------------------
void CFileManager::LoadPatches(const bool &mapOnly)
{
	OAFUN_DEBUG("");
	CDataReader &verdata = m_File[OFI_VERDATA_MUL];

	if (g_ClientInterface->UseVerdata && verdata.GetStart())
	{
		int dataCount = verdata.ReadInt32LE();

		uint vAddr = (uint)verdata.GetStart();

		IFOR(i, 0, dataCount)
		{
			PVERDATA_HEADER vh = (PVERDATA_HEADER)(vAddr + 4 + (i * sizeof(VERDATA_HEADER)));

			if (vh->FileID == 0 && m_MaxBlockIndex) //Map0
			{
				if (vh->BlockID >= m_MaxBlockIndex)
					continue;

				m_BlockData[vh->BlockID].SetOriginalMapAddress(vAddr + vh->Position);
				m_BlockData[vh->BlockID].SetMapAddress(vAddr + vh->Position);

			}
			else if (mapOnly)
				continue;
			else if (vh->FileID == 30) //Tiledata
			{
				verdata.ResetPtr();
				verdata.Move(vh->Position);

				if (vh->Size == 836)
				{
					if (vh->BlockID >= (uint)m_LandTiledata.size())
						continue;

					LAND_GROUP &group = m_LandTiledata[vh->BlockID];
					group.Unknown = verdata.ReadUInt32LE();

					IFOR(j, 0, 32)
					{
						LAND_TILES &tile = group.Tiles[j];

						tile.Flags = verdata.ReadUInt32LE();
						tile.TexID = verdata.ReadUInt16LE();
						tile.Name = verdata.ReadString(20);
					}
				}
				else if (vh->Size == 1188)
				{
					uint bID = vh->BlockID - 0x0200;

					if (bID >= (uint)m_StaticTiledata.size())
						continue;

					STATIC_GROUP &group = m_StaticTiledata[bID];
					group.Unk = verdata.ReadUInt32LE();

					IFOR(j, 0, 32)
					{
						STATIC_TILES &tile = group.Tiles[j];

						tile.Flags = verdata.ReadUInt32LE();
						tile.Weight = verdata.ReadInt8();
						tile.Quality = verdata.ReadInt8();
						tile.Unknown = verdata.ReadInt16LE();
						tile.Unknown1 = verdata.ReadInt8();
						tile.Quality1 = verdata.ReadInt8();
						tile.AnimID = verdata.ReadInt16LE();
						tile.Unknown2 = verdata.ReadInt8();
						tile.Hue = verdata.ReadInt8();
						tile.SittingOffset = verdata.ReadInt8();
						tile.Unknown3 = verdata.ReadInt8();
						tile.Height = verdata.ReadInt8();
						tile.Name = verdata.ReadString(20);
					}
				}
			}
		}
	}

	ReplacePatchedAddresses();
}
//----------------------------------------------------------------------------------
void CFileManager::OnMapChange()
{
	OAFUN_DEBUG("");
	uint map = g_CurrentMap;

	if (map >= 6)
		return;

	m_MaxBlockIndex = m_MapBlockSize[map].x() * m_MapBlockSize[map].y();

	if (m_BlockData != nullptr)
	{
		delete[] m_BlockData;
		m_BlockData = nullptr;
	}

	//Return and error notification?
	if (m_MaxBlockIndex < 1)
		return;

	m_BlockData = new CIndexMap[m_MaxBlockIndex];

	uint mapAddress = (uint)g_FileManager.m_File[OFI_MAP_0_UOP + map].GetStart();
	uint endMapAddress = mapAddress + g_FileManager.m_File[OFI_MAP_0_UOP + map].GetSize();

	bool useUOP = (mapAddress && endMapAddress > mapAddress && g_PacketManager.GetClientVersion() >= CV_7000);

	struct UOPMapaData
	{
		int offset;
		int length;
	};

	std::unordered_map<unsigned long long, UOPMapaData> hashes;

	if (!useUOP)
	{
		mapAddress = (uint)g_FileManager.m_File[OFI_MAP_0_MUL + map].GetStart();
		endMapAddress = mapAddress + g_FileManager.m_File[OFI_MAP_0_MUL + map].GetSize();
	}
	else
	{
		CDataReader &uopFile = g_FileManager.m_File[OFI_MAP_0_UOP + map];

		uopFile.ResetPtr();

		//Начинаем читать УОП
		if (uopFile.ReadInt32LE() != 0x50594D)
		{
			LOG("Bad MapN.uop file\n");
			return;
		}

		uopFile.ReadInt64LE(); // version + signature
		long long nextBlock = uopFile.ReadInt64LE();

		uopFile.ResetPtr();
		uopFile.Move(static_cast<int>(nextBlock));

		do
		{
			int fileCount = uopFile.ReadInt32LE();
			nextBlock = uopFile.ReadInt64LE();
			IFOR(i, 0, fileCount)
			{
				auto offset = uopFile.ReadInt64LE();
				auto headerLength = uopFile.ReadInt32LE();
				auto compressedLength = uopFile.ReadInt32LE();
				uopFile.Move(4);
				auto hash = uopFile.ReadInt64LE();
				uopFile.Move(6);

				if (offset == 0)
				{
					continue;
				}

				UOPMapaData dataStruct;
				dataStruct.offset = static_cast<int>(offset + headerLength);
				dataStruct.length = compressedLength;
				hashes[hash] = dataStruct;
			}

			uopFile.ResetPtr();
			uopFile.Move(static_cast<int>(nextBlock));
		} while (nextBlock != 0);
	}

	uint staticIdxAddress = (uint)g_FileManager.m_File[OFI_STAIDX_0_MUL + map].GetStart();
	uint endStaticIdxAddress = staticIdxAddress + g_FileManager.m_File[OFI_STAIDX_0_MUL + map].GetSize();

	uint staticAddress = (uint)g_FileManager.m_File[OFI_STATICS_0_MUL + map].GetStart();
	uint endStaticAddress = staticAddress + g_FileManager.m_File[OFI_STATICS_0_MUL + map].GetSize();

	if (!mapAddress || !staticIdxAddress || !staticAddress)
		return;

	int fileNumber = -1;
	uint uopOffset = 0;

	IFOR(block, 0, (int)m_MaxBlockIndex)
	{
		uint realMapAddress = 0;
		uint realStaticAddress = 0;
		int realStaticCount = 0;

		if (mapAddress != 0)
		{
			int blockNumber = block;

			if (useUOP)
			{
				blockNumber &= 4095;
				int shifted = (int)block >> 12;

				if (fileNumber != shifted)
				{
					fileNumber = shifted;
					char mapFilePath[200] = { 0 };
					sprintf_s(mapFilePath, "build/map%ilegacymul/%08i.dat", map, shifted);

					unsigned long long hash = CreateHash(mapFilePath);

					if (hashes.find(hash) != hashes.end())
						uopOffset = (uint)hashes.at(hash).offset;
					else
					{
						LOG("Hash not found in uop map %i file.\n", map);
					}
				}
			}

			uint address = mapAddress + uopOffset + (blockNumber * sizeof(MAP_BLOCK));

			if (address < endMapAddress)
				realMapAddress = address;
		}

		uint staticIdxBlockAddress = staticIdxAddress + block * sizeof(STAIDX_BLOCK);

		if (staticIdxBlockAddress < endStaticIdxAddress)
		{
			PSTAIDX_BLOCK sidx = (PSTAIDX_BLOCK)staticIdxBlockAddress;

			if (sidx->Size > 0 && sidx->Position != 0xFFFFFFFF)
			{
				uint address = staticAddress + sidx->Position;

				if (address < endStaticAddress)
				{
					realStaticAddress = address;
					realStaticCount = sidx->Size / sizeof(STATICS_BLOCK);

					if (realStaticCount > 1024)
						realStaticCount = 1024;
				}
			}
		}

		CIndexMap &index = m_BlockData[block];

		index.SetOriginalMapAddress(realMapAddress);
		index.SetOriginalStaticAddress(realStaticAddress);
		index.SetOriginalStaticCount(realStaticCount);

		index.SetMapAddress(realMapAddress);
		index.SetStaticAddress(realStaticAddress);
		index.SetStaticCount(realStaticCount);
	}

	if (g_OrionMap != nullptr && g_OrionMap->isVisible())
	{
		g_OrionMap->SetWantUpdateImage(true);
		g_OrionMap->SetWantRedraw(true);
	}
}
//----------------------------------------------------------------------------------
void CFileManager::ResetMapPatches()
{
	OAFUN_DEBUG("");
	int map = g_CurrentMap;

	if (map >= 6)
		map = 0;

	if ((m_File[OFI_MAP_0_MUL].GetStart() == 0 && m_File[OFI_MAP_0_UOP].GetStart() == 0) || m_File[OFI_STAIDX_0_MUL].GetStart() == 0 || m_File[OFI_STATICS_0_MUL].GetStart() == 0)
		return;

	IFOR(block, 0, (int)m_MaxBlockIndex)
	{
		CIndexMap &index = m_BlockData[block];

		index.SetMapAddress(index.GetOriginalMapAddress());
		index.SetStaticAddress(index.GetOriginalStaticAddress());
		index.SetStaticCount(index.GetOriginalStaticCount());
	}
}
//----------------------------------------------------------------------------------
void CFileManager::ApplyPatches(CDataReader &stream)
{
	OAFUN_DEBUG("");
	ResetMapPatches();

	IFOR(i, 0, 6)
		m_PatchInfo[i].Reset();

	int count = stream.ReadUInt32BE();

	if (count < 0)
		count = 0;

	if (count > 6)
		count = 6;

	IFOR(i, 0, count)
	{
		m_PatchInfo[i].SetMapPatchesCount(stream.ReadUInt32BE());
		m_PatchInfo[i].SetStaticPatchesCount(stream.ReadUInt32BE());
	}

	ReplacePatchedAddresses();

	//UpdatePatched();
}
//----------------------------------------------------------------------------------
void CFileManager::ReplacePatchedAddresses()
{
	OAFUN_DEBUG("");
	int i = g_CurrentMap;

	if (i < 0 || i >= 6)
		i = 0;

	int mapPatchesCount = m_PatchInfo[i].GetMapPatchesCount();
	int staticsPatchesCount = m_PatchInfo[i].GetStaticPatchesCount();

	if (mapPatchesCount)
	{
		CDataReader &difl = m_File[OFI_MAP_DIFL_0_MUL + i];
		CDataReader &dif = m_File[OFI_MAP_DIF_0_MUL + i];

		mapPatchesCount = qMin(mapPatchesCount, difl.GetSize() / 4);

		difl.ResetPtr();
		dif.ResetPtr();

		IFOR(j, 0, mapPatchesCount)
		{
			uint blockIndex = difl.ReadUInt32LE();

			if (blockIndex < m_MaxBlockIndex)
				m_BlockData[blockIndex].SetMapAddress((uint)dif.GetPtr());

			dif.Move(sizeof(MAP_BLOCK));
		}
	}

	if (staticsPatchesCount)
	{
		CDataReader &difl = m_File[OFI_STA_DIFL_0_MUL + i];
		CDataReader &difi = m_File[OFI_STA_DIFI_0_MUL + i];
		uint startAddress = (uint)m_File[OFI_STA_DIF_0_MUL + i].GetStart();

		staticsPatchesCount = qMin(staticsPatchesCount, difl.GetSize() / 4);

		difl.ResetPtr();
		difi.ResetPtr();

		IFOR(j, 0, staticsPatchesCount)
		{
			uint blockIndex = difl.ReadUInt32LE();

			PSTAIDX_BLOCK sidx = (PSTAIDX_BLOCK)difi.GetPtr();

			difi.Move(sizeof(STAIDX_BLOCK));

			if (blockIndex < m_MaxBlockIndex)
			{
				uint realStaticAddress = 0;
				int realStaticCount = 0;

				if (sidx->Size > 0 && sidx->Position != 0xFFFFFFFF)
				{
					realStaticAddress = startAddress + sidx->Position;
					realStaticCount = sidx->Size / sizeof(STATICS_BLOCK);

					if (realStaticCount > 0)
					{
						if (realStaticCount > 1024)
							realStaticCount = 1024;
					}
				}

				if (realStaticAddress && realStaticCount)
				{
					m_BlockData[blockIndex].SetStaticAddress(realStaticAddress);
					m_BlockData[blockIndex].SetStaticCount(realStaticCount);
				}
			}
		}
	}

	//UpdatePatched();
}
//----------------------------------------------------------------------------------
int CFileManager::GetActualMap()
{
	OAFUN_DEBUG("");
	if ((g_CurrentMap == 1 && ((!m_File[OFI_MAP_1_MUL].GetStart() && m_File[OFI_MAP_1_UOP].GetStart()) || !m_File[OFI_STAIDX_1_MUL].GetStart() || !m_File[OFI_STATICS_1_MUL].GetStart())) || g_CurrentMap >= 6)
		return 0;

	return g_CurrentMap;
}
//----------------------------------------------------------------------------------
ushort CFileManager::TileNameToFlags(QString tileName, ushort &graphic)
{
	OAFUN_DEBUG("");
	ushort flags = TNF_BY_GRAPHIC;

	CTextParser parser(tileName.toStdString(), "|", "", "");

	QStringList list = parser.ReadTokens();

	foreach (const QString &str, list)
	{
		if (str == "mine")
			flags |= TNF_MINE;
		else if (str == "tree")
			flags |= TNF_TREE;
		else if (str == "water")
			flags |= TNF_WATER;
		else if (str == "land")
			flags |= TNF_LAND;
		else if (str == "any")
			flags |= TNF_ANY;
		else
			graphic = COrionAssistant::TextToGraphic(str);
	}

	return flags;
}
//----------------------------------------------------------------------------------
bool CTileInfo::TestTile(const QList<CSearchTileInfo> &list)
{
	OAFUN_DEBUG("");
	if (!list.empty())
	{
		for (const CSearchTileInfo &info : list)
		{
			if (info.GetLand() != m_Land)
				continue;

			if (info.GetEnd())
			{
				if (m_Graphic >= info.GetStart() && m_Graphic <= info.GetEnd())
					return true;
			}
			else if (info.GetStart() == m_Graphic)
				return true;
		}
	}

	return false;
}
//----------------------------------------------------------------------------------
bool CTileInfo::CanBeAdded(const ushort &graphic, const ushort &flags)
{
	OAFUN_DEBUG("");
	if (flags == TNF_BY_GRAPHIC && m_Graphic == graphic)
		return true;
	else if (flags & TNF_MINE)
		return TestTile(g_FileManager.m_MineTiles);
	else if (flags & TNF_TREE)
		return TestTile(g_FileManager.m_TreeTiles);
	else if (flags & TNF_WATER)
		return TestTile(g_FileManager.m_WaterTiles);

	return false;
}
//----------------------------------------------------------------------------------
QList<CTileInfo> CFileManager::GetTile(const int &x, const int &y, const int &z, QString tileName)
{
	OAFUN_DEBUG("");
	int map = GetActualMap();
	QList<CTileInfo> result;

	uint block = ((x / 8) * m_MapBlockSize[map].y()) + (y / 8);

	ushort tileID = 0;
	ushort flags = TileNameToFlags(tileName, tileID);

	if (EnabledCommandExtendedTargetTile() && block < m_MaxBlockIndex)
	{
		CIndexMap &index = m_BlockData[block];

		int tileX = x % 8;
		int tileY = y % 8;

		CTileInfo land(tileID, x, y, z, false);

		if (index.GetMapAddress() != 0)
		{
			int pos = tileY * 8 + tileX;

			PMAP_BLOCK pmb = (PMAP_BLOCK)index.GetMapAddress();

			land = CTileInfo(pmb->Cells[pos].TileID & 0x3FFF, x, y, pmb->Cells[pos].Z, true);

			if (flags & TNF_LAND)
			{
				if (!tileID || land.CanBeAdded(tileID, flags))
					result.push_back(land);

				return result;
			}
		}

		PSTATICS_BLOCK sb = (PSTATICS_BLOCK)index.GetStaticAddress();

		if (sb != nullptr)
		{
			int count = index.GetStaticCount();

			for (int c = 0; c < count; c++, sb++)
			{
				if (sb->Color && sb->Color != 0xFFFF && tileX == sb->X && tileY == sb->Y)
				{
					int pos = (sb->Y * 8) + sb->X;

					if (pos >= 64)
						continue;

					CTileInfo statics(sb->Color, x, y, sb->Z, false);

					if (statics.CanBeAdded(tileID, flags) || (flags & TNF_ANY))
						result.push_back(statics);
				}
			}
		}

		if ((flags & TNF_ANY) && !result.size())
			result.push_back(land);
	}

	if (!result.size())
		result.push_back(CTileInfo(tileID, x, y, z, false));

	return result;
}
//----------------------------------------------------------------------------------
unsigned long long CFileManager::CreateHash(string s)
{
	OAFUN_DEBUG("");
	unsigned long eax, ecx, edx, ebx, esi, edi;

	eax = ecx = edx = ebx = esi = edi = 0;
	ebx = edi = esi = s.length() + 0xDEADBEEF;

	unsigned long i = 0;

	for (i = 0; i + 12 < s.length(); i += 12)
	{
		edi = (uint)((s[i + 7] << 24) | (s[i + 6] << 16) | (s[i + 5] << 8) | s[i + 4]) + edi;
		esi = (uint)((s[i + 11] << 24) | (s[i + 10] << 16) | (s[i + 9] << 8) | s[i + 8]) + esi;
		edx = (uint)((s[i + 3] << 24) | (s[i + 2] << 16) | (s[i + 1] << 8) | s[i]) - esi;

		edx = (edx + ebx) ^ (esi >> 28) ^ (esi << 4);
		esi += edi;
		edi = (edi - edx) ^ (edx >> 26) ^ (edx << 6);
		edx += esi;
		esi = (esi - edi) ^ (edi >> 24) ^ (edi << 8);
		edi += edx;
		ebx = (edx - esi) ^ (esi >> 16) ^ (esi << 16);
		esi += edi;
		edi = (edi - ebx) ^ (ebx >> 13) ^ (ebx << 19);
		ebx += esi;
		esi = (esi - edi) ^ (edi >> 28) ^ (edi << 4);
		edi += ebx;
	}

	if (s.length() - i > 0)
	{
		switch (s.length() - i)
		{
		case 12:
			esi += static_cast<unsigned long>(s[i + 11]) << 24;
			goto  case_11;
			break;
		case 11:
		case_11 :
			esi += static_cast<unsigned long>(s[i + 10]) << 16;
				goto  case_10;
				break;
		case 10:
		case_10 :
			esi += static_cast<unsigned long>(s[i + 9]) << 8;
				goto  case_9;
				break;
		case 9:
		case_9 :
			esi += s[i + 8];
			   goto  case_8;
			   break;
		case 8:
		case_8 :
			edi += static_cast<unsigned long>(s[i + 7]) << 24;
			   goto case_7;
			   break;
		case 7:
		case_7 :
			edi += static_cast<unsigned long>(s[i + 6]) << 16;
			   goto case_6;
			   break;
		case 6:
		case_6 :
			edi += static_cast<unsigned long>(s[i + 5]) << 8;
			   goto case_5;
			   break;
		case 5:
		case_5 :
			edi += s[i + 4];
			   goto case_4;
			   break;
		case 4:
		case_4 :
			ebx += static_cast<unsigned long>(s[i + 3]) << 24;
			   goto case_3;
			   break;
		case 3:
		case_3 :
			ebx += static_cast<unsigned long>(s[i + 2]) << 16;
			   goto case_2;
			   break;
		case 2:
		case_2 :
			ebx += static_cast<unsigned long>(s[i + 1]) << 8;
			   goto case_1;
		case 1:
		case_1 :
			ebx += s[i];
			   break;
		}

		esi = (esi ^ edi) - ((edi >> 18) ^ (edi << 14));
		ecx = (esi ^ ebx) - ((esi >> 21) ^ (esi << 11));
		edi = (edi ^ ecx) - ((ecx >> 7) ^ (ecx << 25));
		esi = (esi ^ edi) - ((edi >> 16) ^ (edi << 16));
		edx = (esi ^ ecx) - ((esi >> 28) ^ (esi << 4));
		edi = (edi ^ edx) - ((edx >> 18) ^ (edx << 14));
		eax = (esi ^ edi) - ((edi >> 8) ^ (edi << 24));

		return (static_cast<unsigned long long>(edi) << 32) | eax;
	}

	return (static_cast<unsigned long long>(esi) << 32) | eax;
}
//----------------------------------------------------------------------------------
