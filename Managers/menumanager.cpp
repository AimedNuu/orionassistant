// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** MenuManager.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "menumanager.h"
#include "../GameObjects/GameWorld.h"
#include "../OrionAssistant/textdialog.h"
#include "../OrionAssistant/Packets.h"
#include "../OrionAssistant/tabmacros.h"

CMenuManager g_MenuManager;
//----------------------------------------------------------------------------------
CMenuManager::CMenuManager()
{
}
//----------------------------------------------------------------------------------
CMenuManager::~CMenuManager()
{
}
//----------------------------------------------------------------------------------
int CMenuManager::GetMenuCount()
{
	OAFUN_DEBUG("c20_f1");
	return m_Items.size();
}
//----------------------------------------------------------------------------------
CMenu *CMenuManager::GetLastMenu()
{
	OAFUN_DEBUG("c20_f2");
	return m_LastMenu;
}
//----------------------------------------------------------------------------------
CMenu *CMenuManager::GetMenu(const QString &name)
{
	OAFUN_DEBUG("c20_f3");
	IFOR(i, 0, m_Items.size())
	{
		if (m_Items[i]->GetName().indexOf(name, 0, Qt::CaseInsensitive) == 0)
			return m_Items[i];
	}

	uint index = name.toInt();

	if (index < (uint)m_Items.size())
		return m_Items[index];

	return nullptr;
}
//----------------------------------------------------------------------------------
void CMenuManager::RemoveLastMenu()
{
	OAFUN_DEBUG("c20_f4");
	IFOR(i, 0, m_Items.size())
	{
		if (m_Items[i] == m_LastMenu)
		{
			m_LastMenu = nullptr;
			break;
		}
	}

	delete m_LastMenu;
	m_LastMenu = nullptr;
}
//----------------------------------------------------------------------------------
void CMenuManager::ClearMenus()
{
	OAFUN_DEBUG("c20_f5");
	IFOR(i, 0, m_Items.size())
		delete m_Items[i];

	m_Items.clear();
	m_LastMenu = nullptr;
}
//----------------------------------------------------------------------------------
void CMenuManager::SetHook(const QString &name, const QString &itemName)
{
	OAFUN_DEBUG("c20_f6");
	m_Hooks.push_back(CMenuHook(name, itemName));
}
//----------------------------------------------------------------------------------
void CMenuManager::ClearHooks()
{
	OAFUN_DEBUG("c20_f7");
	m_Hooks.clear();
}
//----------------------------------------------------------------------------------
bool CMenuManager::OnPacketReceived(CDataReader &reader)
{
	OAFUN_DEBUG("c20_f8");
	RemoveLastMenu();

	if (g_World == nullptr)
		return true;

	uint serial = reader.ReadUInt32BE();
	ushort id = reader.ReadUInt16BE();

	uchar nameLen = reader.ReadUInt8();
	QString name = reader.ReadString(nameLen).c_str();

	uchar count = reader.ReadUInt8();

	bool grayMenu = (unpack16(reader.GetPtr()) == 0); //menu/gray menu

	m_LastMenu = new CMenu(serial, id, name, grayMenu);
	m_Items.push_back(m_LastMenu);

	IFOR(i, 0, count)
	{
		ushort graphic = reader.ReadUInt16BE();
		ushort color = reader.ReadUInt16BE();

		nameLen = reader.ReadUInt8();
		name = reader.ReadString(nameLen).c_str();

		m_LastMenu->m_Items.push_back(CMenuItem(i + 1, graphic, color, name));
	}

	if (/*!m_Hooks.empty() &&*/ g_TabMacros->GetPlaying() && g_TabMacros->PlayWaiting(QList<MACRO_TYPE>() << MT_WAIT_FOR_MENU))
		return false;

	if (!m_Hooks.empty())
	{
		const CMenuHook &hook = m_Hooks.first();

		if (SendChoiceIfMenuExists(hook.GetName(), hook.GetItemName(), true))
		{
			m_Hooks.pop_front();
			return false;
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
bool CMenuManager::OnPacketSend(CDataReader &reader)
{
	OAFUN_DEBUG("c20_f9");
	uint serial = reader.ReadUInt32BE();
	uint id = reader.ReadUInt16BE();

	IFOR(i, 0, m_Items.size())
	{
		CMenu *menu = m_Items[i];

		if (!menu->GetReplayed() && menu->GetSerial() == serial && menu->GetID() == id)
		{
			uint index = reader.ReadUInt16BE();

			menu->SetReplayed(true);
			menu->SetReplyID(index);

			if (g_World != nullptr && g_TabMacros->GetRecording())
			{
				for (const CMenuItem &item : menu->m_Items)
				{
					if (item.GetID() == index)
					{
						g_TabMacros->AddAction(new CMacroWithMenu(MT_WAIT_FOR_MENU, menu->GetName(), item.GetName(), item.GetGraphic(), item.GetColor()), false);
						break;
					}
				}
			}

			if (menu != m_LastMenu)
				delete menu;

			m_Items.removeAt(i);

			break;
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
bool CMenuManager::SendChoiceIfMenuExists(const QString &name, const QString &itemName, const bool &sendPacket)
{
	OAFUN_DEBUG("c20_f10");
	IFOR(i, 0, m_Items.size())
	{
		CMenu *menu = m_Items[i];

		if (!menu->GetReplayed() && menu->GetName().indexOf(name, 0, Qt::CaseInsensitive) == 0)
		{
			if (SendMenuChoice(menu, itemName, sendPacket))
			{
				if (menu != m_LastMenu)
					delete menu;

				m_Items.removeAt(i);

				return true;
			}
		}
	}

	return false;
}
//----------------------------------------------------------------------------------
bool CMenuManager::SendMenuChoice(CMenu *menu, const QString &itemName, const bool &sendPacket)
{
	OAFUN_DEBUG("c20_f11");
	if (!itemName.length() || itemName.toLower() == "cancel")
	{
		if (sendPacket)
		{
			CPacketMenuResponse(menu->GetSerial(), menu->GetID(), 0, 0, 0).SendServer();

			menu->SetReplayed(true);
			menu->SetReplyID(0);
		}
		else
			CPacketSelectMenuInClient(menu->GetSerial(), menu->GetID(), 0).SendClient();

		return true;
	}

	foreach (const CMenuItem &item, menu->m_Items)
	{
		if (item.GetName().indexOf(itemName) == 0)
		{
			if (sendPacket)
			{
				if (menu->GetGrayMenu())
					CPacketGrayMenuResponse(menu->GetSerial(), menu->GetID(), item.GetID()).SendServer();
				else
					CPacketMenuResponse(menu->GetSerial(), menu->GetID(), item.GetID(), item.GetGraphic(), item.GetColor()).SendServer();

				menu->SetReplayed(true);
				menu->SetReplyID(item.GetID());
			}
			else
				CPacketSelectMenuInClient(menu->GetSerial(), menu->GetID(), item.GetID()).SendClient();

			return true;
		}
	}

	bool ok = false;
	uint index = 0;

	if (itemName.toLower() == "random" && menu->m_Items.size())
	{
		ok = true;
		index = qrand() % menu->m_Items.size();
	}
	else
		index = itemName.toInt(&ok);

	if (ok && index < (uint)menu->m_Items.size())
	{
		CMenuItem &item = menu->m_Items[index];

		if (sendPacket)
		{
			if (menu->GetGrayMenu())
				CPacketGrayMenuResponse(menu->GetSerial(), menu->GetID(), item.GetID()).SendServer();
			else
				CPacketMenuResponse(menu->GetSerial(), menu->GetID(), item.GetID(), item.GetGraphic(), item.GetColor()).SendServer();

			menu->SetReplayed(true);
			menu->SetReplyID(item.GetID());
		}
		else
			CPacketSelectMenuInClient(menu->GetSerial(), menu->GetID(), item.GetID()).SendClient();

		return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
void CMenuManager::InfoMenu(const QString &menuIndex)
{
	OAFUN_DEBUG("c20_f12");
	int index = -1;

	if (menuIndex.toLower() != "lastmenu" || menuIndex.toLower() != "last")
	{
		bool ok = false;
		index = menuIndex.toInt(&ok);

		if (!ok)
			index= -1;
	}

	CMenu *menu = nullptr;

	if (index == -1)
		menu = m_LastMenu;
	else if (index >= 0 && index < m_Items.size())
		menu = m_Items[index];

	if (menu == nullptr)
		return;

	g_TextDialog->ClearText();

	if (menu->GetGrayMenu())
		g_TextDialog->AddText("Gray menu information:\nName: " + menu->GetName());
	else
		g_TextDialog->AddText("Menu information:\nName: " + menu->GetName());

	if (menu->GetReplayed())
		g_TextDialog->AddText("Menu replayed with id: " + QString::number(menu->GetReplyID()));

	foreach (const CMenuItem &item, menu->m_Items)
	{
		QString text = "";
		text.sprintf("Item: ID=%i Graphic=0x%04X Color=0x%04X Name=%s", item.GetID(), item.GetGraphic(), item.GetColor(), item.GetName().toStdString().c_str());

		g_TextDialog->AddText(text);
	}

	if (g_TextDialog->isVisible())
		g_TextDialog->activateWindow();
	else
		g_TextDialog->show();
}
//----------------------------------------------------------------------------------
