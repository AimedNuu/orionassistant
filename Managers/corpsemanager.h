/***********************************************************************************
**
** CorpseManager.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef CORPSEMANAGER_H
#define CORPSEMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CCorpseInfo
{
	SETGET(uint, Serial, 0)
	SETGET(int, Timer, 0)

public:
	CCorpseInfo() {}
	CCorpseInfo(const uint &serial, const int &timer) : m_Serial(serial), m_Timer(timer) {}
	~CCorpseInfo() {}
};
//----------------------------------------------------------------------------------
typedef QVector<CCorpseInfo> CORPSE_INFO_LIST;
//----------------------------------------------------------------------------------
class CCorpseManager
{
private:
	CORPSE_INFO_LIST m_List;

public:
	CCorpseManager();
	virtual ~CCorpseManager();

	void Add(const CCorpseInfo &info);

	void Clear();

	void CheckCorpses();

	bool Empty() { return m_List.empty();}
};
//----------------------------------------------------------------------------------
extern CCorpseManager g_CorpseManager;
//----------------------------------------------------------------------------------
#endif // CORPSEMANAGER_H
//----------------------------------------------------------------------------------
