﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** PacketManager.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "PacketManager.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../OrionAssistant/orionassistant.h"
#include "../GameObjects/gameworld.h"
#include "../GameObjects/gameplayer.h"
#include "textcommandmanager.h"
#include "../OrionAssistant/Target.h"
#include "menumanager.h"
#include "journalmanager.h"
#include "corpsemanager.h"
#include "buffmanager.h"
#include "trademanager.h"
#include "../OrionAssistant/Packets.h"
#include "UOMapManager.h"
#include "FileManager.h"
#include "gumpmanager.h"
#include "ClilocManager.h"
#include "promptmanager.h"
#include "partymanager.h"
#include "../OrionAssistant/tabmain.h"
#include "../OrionAssistant/tabdisplay.h"
#include "../OrionAssistant/tabscripts.h"
#include "../OrionAssistant/tabagentsparty.h"
#include "../OrionAssistant/tabskills.h"
#include "../OrionAssistant/tabfiltersspeech.h"
#include "../OrionAssistant/tabfiltersreplaces.h"
#include "../OrionAssistant/tabfilterssound.h"
#include "../OrionAssistant/objectinspectorform.h"
#include "../OrionAssistant/tabagentsshop.h"
#include "ObjectPropertiesManager.h"
#include "clientmacromanager.h"
#include "spellmanager.h"
#include "contextmenumanager.h"
#include "../OrionAssistant/orionmap.h"
#include "../OrionAssistant/tabmacros.h"
#include "../Macros/macro.h"
#include "../OrionAssistant/hotkeysform.h"
#include "../OrionAssistant/tabhotkeys.h"
//----------------------------------------------------------------------------------
CPacketManager g_PacketManager;
//----------------------------------------------------------------------------------
const int PACKET_VARIABLE_SIZE = 0;
//----------------------------------------------------------------------------------
//Карта пакетов УО для анализа
#define UMSG(size) { "?", size, DIR_BOTH, 0, 0 }
#define SMSG(name, size) { name, size, DIR_SEND, 0, 0 }
#define RMSG(name, size) { name, size, DIR_RECV, 0, 0 }
#define BMSG(name, size) { name, size, DIR_BOTH, 0, 0 }
#define RMSGH(name, size, rmethod) \
{ name, size, DIR_RECV, 0, &CPacketManager::Handle ##rmethod }
#define SMSGH(name, size, smethod) \
{ name, size, DIR_SEND, &CPacketManager::Handle ##smethod, 0 }
#define BMSGH(name, size, smethod, rmethod) \
{ name, size, DIR_BOTH, &CPacketManager::Handle ##smethod, &CPacketManager::Handle ##rmethod }
//----------------------------------------------------------------------------------
CPacketInfo CPacketManager::m_Packets[0x100] =
{
    /*0x00*/ SMSG("Create Character", 0x68),
    /*0x01*/ SMSG("Disconnect", 0x05),
    /*0x02*/ SMSGH("Walk Request", 0x07, WalkRequest),
    /*0x03*/ BMSGH("Client Talk", PACKET_VARIABLE_SIZE, ClientTalkS, ClientTalkR),
    /*0x04*/ SMSG("Request God mode (God client)", 0x02),
	/*0x05*/ SMSGH("Attack", 0x05, AttackRequest),
	/*0x06*/ SMSGH("Double Click", 0x05, DoubleClick),
    /*0x07*/ SMSG("Pick Up Item", 0x07),
    /*0x08*/ SMSG("Drop Item", 0x0e),
    /*0x09*/ SMSG("Single Click", 0x05),
    /*0x0A*/ SMSG("Edit (God client)", 0x0b),
    /*0x0B*/ RMSG("Damage Visualization", 0x07),
    /*0x0C*/ BMSG("Edit tiledata (God client)", PACKET_VARIABLE_SIZE),
    /*0x0D*/ UMSG(0x03),
    /*0x0E*/ UMSG(0x01),
    /*0x0F*/ UMSG(0x3d),
    /*0x10*/ UMSG(0xd7),
    /*0x11*/ RMSGH("Character Status", PACKET_VARIABLE_SIZE, CharacterStatus),
    /*0x12*/ SMSGH("Perform Action", PACKET_VARIABLE_SIZE, PerformAction),
    /*0x13*/ SMSG("Client Equip Item", 0x0a),
    /*0x14*/ SMSG("Send elevation (God client)", 0x06),
    /*0x15*/ BMSG("Follow", 0x09),
    /*0x16*/ UMSG(0x01),
    /*0x17*/ RMSGH("Health status bar update (KR)", PACKET_VARIABLE_SIZE, NewHealthbarUpdate),
    /*0x18*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x19*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x1A*/ RMSGH("Update Item", PACKET_VARIABLE_SIZE, UpdateItem),
    /*0x1B*/ RMSGH("Enter World", 0x25, EnterWorld),
    /*0x1C*/ RMSGH("Server Talk", PACKET_VARIABLE_SIZE, Talk),
    /*0x1D*/ RMSGH("Delete Object", 0x05, DeleteObject),
    /*0x1E*/ UMSG(0x04),
    /*0x1F*/ UMSG(0x08),
    /*0x20*/ RMSGH("Update Player", 0x13, UpdatePlayer),
    /*0x21*/ RMSG("Deny Walk", 0x08),
	/*0x22*/ BMSGH("Confirm Walk", 0x03, Resend, ConfirmWalk),
    /*0x23*/ RMSG("Drag Animation", 0x1a),
    /*0x24*/ RMSGH("Open Container", 0x07, OpenContainer),
    /*0x25*/ RMSGH("Update Contained Item", 0x14, UpdateContainedItem),
    /*0x26*/ UMSG(0x05),
    /*0x27*/ RMSG("Deny Move Item", 0x02),
    /*0x28*/ UMSG(0x05),
    /*0x29*/ RMSG("Drop Item Approved", 0x01),
    /*0x2A*/ UMSG(0x05),
    /*0x2B*/ UMSG(0x02),
    /*0x2C*/ BMSGH("Death Screen", 0x02, DeathScreenS, DeathScreenR),
    /*0x2D*/ RMSGH("Mobile Attributes", 0x11, MobileAttributes),
    /*0x2E*/ RMSGH("Server Equip Item", 0x0f, EquipItem),
    /*0x2F*/ RMSG("Combat Notification", 0x0a),
    /*0x30*/ RMSG("Attack ok", 0x05),
    /*0x31*/ RMSG("Attack end", 0x01),
    /*0x32*/ UMSG(0x02),
    /*0x33*/ RMSG("Pause Control", 0x02),
    /*0x34*/ SMSG("Status Request", 0x0a),
    /*0x35*/ UMSG(0x28d),
    /*0x36*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x37*/ UMSG(0x08),
    /*0x38*/ BMSG("Pathfinding in Client", 0x07),
    /*0x39*/ RMSG("Remove (Group)", 0x09),
    /*0x3A*/ BMSGH("Update Skills", PACKET_VARIABLE_SIZE, UpdateSkillsS, UpdateSkillsR),
	/*0x3B*/ BMSGH("Vendor Buy Reply", PACKET_VARIABLE_SIZE, BuyReplyS, BuyReplyR),
    /*0x3C*/ RMSGH("Update Contained Items", PACKET_VARIABLE_SIZE, UpdateContainedItems),
    /*0x3D*/ UMSG(0x02),
    /*0x3E*/ UMSG(0x25),
    /*0x3F*/ RMSG("Update Statics (God Client)", PACKET_VARIABLE_SIZE),
    /*0x40*/ UMSG(0xc9),
    /*0x41*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x42*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x43*/ UMSG(0x229),
    /*0x44*/ UMSG(0x2c9),
    /*0x45*/ BMSG("Version OK", 0x05),
    /*0x46*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x47*/ UMSG(0x0b),
    /*0x48*/ UMSG(0x49),
    /*0x49*/ UMSG(0x5d),
    /*0x4A*/ UMSG(0x05),
    /*0x4B*/ UMSG(0x09),
    /*0x4C*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x4D*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x4E*/ RMSGH("Personal Light Level", 0x06, PersonalLightLevel),
    /*0x4F*/ RMSGH("Global Light Level", 0x02, LightLevel),
    /*0x50*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x51*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x52*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x53*/ RMSG("Error Code", 0x02),
    /*0x54*/ RMSGH("Sound Effect", 0x0c, PlaySoundEffect),
    /*0x55*/ RMSGH("Login Complete", 0x01, LoginComplete),
    /*0x56*/ BMSG("Map Data", 0x0b),
    /*0x57*/ BMSG("Update Regions", 0x6e),
    /*0x58*/ UMSG(0x6a),
    /*0x59*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x5A*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x5B*/ RMSG("Set Time", 0x04),
    /*0x5C*/ BMSG("Restart Version", 0x02),
    /*0x5D*/ SMSG("Select Character", 0x49),
    /*0x5E*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x5F*/ UMSG(0x31),
    /*0x60*/ UMSG(0x05),
    /*0x61*/ UMSG(0x09),
    /*0x62*/ UMSG(0x0f),
    /*0x63*/ UMSG(0x0d),
    /*0x64*/ UMSG(0x01),
    /*0x65*/ RMSGH("Set Weather", 0x04, SetWeather),
    /*0x66*/ BMSG("Book Page Data", PACKET_VARIABLE_SIZE),
    /*0x67*/ UMSG(0x15),
    /*0x68*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x69*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x6A*/ UMSG(0x03),
    /*0x6B*/ UMSG(0x09),
    /*0x6C*/ BMSGH("Target Data", 0x13, TargetS, TargetR),
    /*0x6D*/ RMSG("Play Music", 0x03),
    /*0x6E*/ RMSGH("Character Animation", 0x0e, CharacterAnimation),
    /*0x6F*/ BMSGH("Secure Trading", PACKET_VARIABLE_SIZE, SecureTradingS, SecureTradingR),
    /*0x70*/ RMSGH("Graphic Effect", 0x1c, GraphicEffect),
    /*0x71*/ BMSG("Bulletin Board Data", PACKET_VARIABLE_SIZE),
    /*0x72*/ BMSGH("War Mode", 0x05, WarmodeS, WarmodeR),
    /*0x73*/ BMSG("Ping", 0x02),
	/*0x74*/ RMSGH("Vendor Buy List", PACKET_VARIABLE_SIZE, BuyList),
    /*0x75*/ SMSG("Rename Character", 0x23),
    /*0x76*/ RMSG("New Subserver", 0x10),
    /*0x77*/ RMSGH("Update Character", 0x11, UpdateCharacter),
    /*0x78*/ RMSGH("Update Object", PACKET_VARIABLE_SIZE, UpdateObject),
    /*0x79*/ UMSG(0x09),
    /*0x7A*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x7B*/ UMSG(0x02),
    /*0x7C*/ RMSGH("Open Menu", PACKET_VARIABLE_SIZE, OpenMenu),
    /*0x7D*/ SMSGH("Menu Choice", 0x0d, MenuChoice),
    /*0x7E*/ UMSG(0x02),
    /*0x7F*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x80*/ SMSG("First Login", 0x3e),
    /*0x81*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x82*/ RMSG("Login Error", 0x02),
    /*0x83*/ SMSG("Delete Character", 0x27),
    /*0x84*/ UMSG(0x45),
    /*0x85*/ RMSG("Character List Notification", 0x02),
    /*0x86*/ RMSG("Resend Character List", PACKET_VARIABLE_SIZE),
    /*0x87*/ UMSG(PACKET_VARIABLE_SIZE),
	/*0x88*/ RMSGH("Open Paperdoll", 0x42, OpenPaperdoll),
    /*0x89*/ RMSG("Corpse Equipment", PACKET_VARIABLE_SIZE),
    /*0x8A*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x8B*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x8C*/ RMSG("Relay Server", 0x0b),
    /*0x8D*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x8E*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x8F*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x90*/ RMSGH("Display Map", 0x13, DisplayMap),
    /*0x91*/ SMSGH("Second Login", 0x41, SecondLogin),
    /*0x92*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x93*/ RMSG("Open Book", 0x63),
    /*0x94*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x95*/ BMSGH("Dye Data", 0x09, DyeDataS, DyeDataR),
    /*0x96*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0x97*/ RMSG("Move Player", 0x02),
    /*0x98*/ BMSG("All Names (3D Client Only)", PACKET_VARIABLE_SIZE),
    /*0x99*/ RMSGH("Multi Placement", 0x1a, MultiPlacement),
    /*0x9A*/ BMSGH("ASCII Prompt", PACKET_VARIABLE_SIZE, ASCIIPromptS, ASCIIPromptR),
	/*0x9B*/ SMSGH("Help Request", 0x102, HelpRequest),
    /*0x9C*/ UMSG(0x135),
    /*0x9D*/ UMSG(0x33),
	/*0x9E*/ RMSGH("Vendor Sell List", PACKET_VARIABLE_SIZE, SellList),
	/*0x9F*/ SMSGH("Vendor Sell Reply", PACKET_VARIABLE_SIZE, SellReply),
    /*0xA0*/ SMSG("Select Server", 0x03),
    /*0xA1*/ RMSGH("Update Hitpoints", 0x09, UpdateHitpoints),
    /*0xA2*/ RMSGH("Update Mana", 0x09, UpdateMana),
    /*0xA3*/ RMSGH("Update Stamina", 0x09, UpdateStamina),
    /*0xA4*/ SMSG("System Information", 0x95),
    /*0xA5*/ RMSG("Open URL", PACKET_VARIABLE_SIZE),
    /*0xA6*/ RMSG("Tip Window", PACKET_VARIABLE_SIZE),
    /*0xA7*/ SMSG("Request Tip", 0x04),
    /*0xA8*/ RMSG("Server List", PACKET_VARIABLE_SIZE),
    /*0xA9*/ RMSG("Character List", PACKET_VARIABLE_SIZE),
    /*0xAA*/ RMSGH("Attack Reply", 0x05, AttackCharacter),
    /*0xAB*/ RMSG("Text Entry Dialog", PACKET_VARIABLE_SIZE),
    /*0xAC*/ SMSG("Text Entry Dialog Reply", PACKET_VARIABLE_SIZE),
    /*0xAD*/ SMSGH("Unicode Client Talk", PACKET_VARIABLE_SIZE, UnicodeClientTalk),
    /*0xAE*/ RMSGH("Unicode Server Talk", PACKET_VARIABLE_SIZE, UnicodeTalk),
    /*0xAF*/ RMSGH("Display Death", 0x0d, DisplayDeath),
	/*0xB0*/ RMSGH("Open Gump", PACKET_VARIABLE_SIZE, OpenGump),
	/*0xB1*/ SMSGH("Gump Choice", PACKET_VARIABLE_SIZE, ChoiceGump),
    /*0xB2*/ BMSG("Chat Data", PACKET_VARIABLE_SIZE),
    /*0xB3*/ RMSG("Chat Text ?", PACKET_VARIABLE_SIZE),
    /*0xB4*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xB5*/ BMSG("Open Chat Window", 0x40),
    /*0xB6*/ SMSG("Popup Help Request", 0x09),
    /*0xB7*/ RMSG("Popup Help Data", PACKET_VARIABLE_SIZE),
	/*0xB8*/ BMSGH("Character Profile", PACKET_VARIABLE_SIZE, CharacterProfileS, CharacterProfileR),
    /*0xB9*/ RMSG("Enable locked client features", 0x03),
    /*0xBA*/ RMSG("Display Quest Arrow", 0x06),
    /*0xBB*/ SMSG("Account ID ?", 0x09),
    /*0xBC*/ RMSGH("Season", 0x03, Season),
    /*0xBD*/ BMSG("Client Version", PACKET_VARIABLE_SIZE),
    /*0xBE*/ BMSG("Assist Version", PACKET_VARIABLE_SIZE),
    /*0xBF*/ BMSGH("Extended Command", PACKET_VARIABLE_SIZE, ExtendedCommandS, ExtendedCommandR),
    /*0xC0*/ RMSGH("Graphical Effect", 0x24, GraphicEffect),
    /*0xC1*/ RMSGH("Display cliloc String", PACKET_VARIABLE_SIZE, DisplayClilocString),
    /*0xC2*/ BMSGH("Unicode prompt", PACKET_VARIABLE_SIZE, UnicodePromptS, UnicodePromptR),
    /*0xC3*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xC4*/ UMSG(0x06),
    /*0xC5*/ UMSG(0xcb),
    /*0xC6*/ UMSG(0x01),
    /*0xC7*/ RMSGH("Graphical Effect", 0x31, GraphicEffect),
    /*0xC8*/ BMSG("Client View Range", 0x02),
    /*0xC9*/ UMSG(0x06),
    /*0xCA*/ UMSG(0x06),
    /*0xCB*/ UMSG(0x07),
    /*0xCC*/ RMSGH("Localized Text Plus String", PACKET_VARIABLE_SIZE, DisplayClilocString),
    /*0xCD*/ UMSG(0x01),
    /*0xCE*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xCF*/ UMSG(0x4e),
    /*0xD0*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xD1*/ UMSG(0x02),
    /*0xD2*/ RMSGH("Update Character (New)", 0x19, UpdateCharacter),
    /*0xD3*/ RMSGH("Update Object (New)", PACKET_VARIABLE_SIZE, UpdateObject),
    /*0xD4*/ BMSG("Open Book (New)", PACKET_VARIABLE_SIZE),
    /*0xD5*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xD6*/ BMSGH("Mega cliloc", PACKET_VARIABLE_SIZE, MegaClilocS, MegaClilocR),
	/*0xD7*/ BMSGH("+AoS command", PACKET_VARIABLE_SIZE, AOSCommandsS, AOSCommandsR),
    /*0xD8*/ RMSG("+Custom house", PACKET_VARIABLE_SIZE),
    /*0xD9*/ SMSG("+Metrics", 0x10c),
    /*0xDA*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xDB*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xDC*/ RMSG("OPL Info Packet", 9),
    /*0xDD*/ RMSG("Compressed Gump", PACKET_VARIABLE_SIZE),
    /*0xDE*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xDF*/ RMSGH("Buff/Debuff", PACKET_VARIABLE_SIZE, BuffDebuff),
    /*0xE0*/ SMSG("Bug Report KR", PACKET_VARIABLE_SIZE),
    /*0xE1*/ SMSG("Client Type KR/SA", 0x09),
    /*0xE2*/ RMSG("New Character Animation", 0xa),
    /*0xE3*/ RMSG("KR Encryption Responce", 0x4d),
    /*0xE4*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xE5*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xE6*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xE7*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xE8*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xE9*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xEA*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xEB*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xEC*/ SMSG("Equip Macro", PACKET_VARIABLE_SIZE),
    /*0xED*/ SMSG("Unequip item macro", PACKET_VARIABLE_SIZE),
    /*0xEE*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xEF*/ SMSG("KR/2D Client Login/Seed", 0x15),
	/*0xF0*/ BMSGH("Krrios client special", PACKET_VARIABLE_SIZE, KrriosClientSpecialS, KrriosClientSpecialR),
    /*0xF1*/ BMSG("Freeshard List", PACKET_VARIABLE_SIZE),
    /*0xF2*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xF3*/ RMSGH("Update Item (SA)", 0x18, UpdateItemSA),
    /*0xF4*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xF5*/ RMSGH("Display New Map", 0x15, DisplayMap),
    /*0xF6*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xF7*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xF8*/ SMSG("Character Creation (7.0.16.0)", 0x6a),
    /*0xF9*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xFA*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xFB*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xFC*/ BMSGH("Orion messages", PACKET_VARIABLE_SIZE, OrionMessagesS, OrionMessagesR),
    /*0xFE*/ UMSG(PACKET_VARIABLE_SIZE),
    /*0xFD*/ RMSG("Razor Handshake", 0x8),
    /*0xFF*/ UMSG(PACKET_VARIABLE_SIZE)
};
//----------------------------------------------------------------------------------
CPacketManager::CPacketManager()
: CDataReader()
{
}
//----------------------------------------------------------------------------------
CPacketManager::~CPacketManager()
{
}
//----------------------------------------------------------------------------------
bool CPacketManager::PacketRecv(uchar *buf, const int &size)
{
	OAFUN_DEBUG("c42_f1");
	uint ticks = GetTickCount();
	CPacketInfo &packet = m_Packets[*buf];
	//LOG("- %04d (%d) --- server %s\n", ticks - g_LastPacketTime, size, packet.Name);
	g_LastPacketTime = ticks;
	g_TotalRecvSize += size;

	//LOG_DUMP(buf, size);

	if (packet.Direction != DIR_RECV && packet.Direction != DIR_BOTH)
		LOG("Warning!!! message direction invalid: 0x%02X\n", *buf);
	else if (packet.RecvHandler != nullptr)
	{
		SetData(buf, size);

		if (!packet.Size)
			Move(3);
		else
			Move(1);

		return (this->*(packet.RecvHandler))();
	}

	return true;
}
//----------------------------------------------------------------------------------
bool CPacketManager::PacketSend(uchar *buf, const int &size)
{
	OAFUN_DEBUG("c42_f2");
	uint ticks = GetTickCount();
	CPacketInfo &packet = m_Packets[*buf];
	//LOG("- %04d (%d) --- client %s\n", ticks - g_LastPacketTime, size, packet.Name);
	g_LastPacketTime = ticks;
	g_TotalSendSize += size;

	/*if (*buf == 0x80 || *buf == 0x91)
	{
		LOG_DUMP(buf, 1);
		LOG("**** ACCOUNT AND PASSWORD CENSORED ****\n");
	}
	else
	{
		LOG_DUMP(buf, size);
	}*/

	if (packet.Direction != DIR_SEND && packet.Direction != DIR_BOTH)
		LOG("Warning!!! message direction invalid: 0x%02X\n", *buf);
	else if (packet.SendHandler != nullptr)
	{
		SetData(buf, size);

		if (!packet.Size)
			Move(3);
		else
			Move(1);

		return (this->*(packet.SendHandler))();
	}

	return true;
}
//----------------------------------------------------------------------------------
#define PACKET_HANDLER(name) bool CPacketManager::Handle ##name ()
//----------------------------------------------------------------------------------
PACKET_HANDLER(SecondLogin)
{
	OAFUN_DEBUG("c42_f3");
	g_Connected = false;
	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(LightLevel)
{
	OAFUN_DEBUG("c42_f4");
	uchar level = ReadUInt8();

	if (level > 0x1F)
		level = 0x1F;

	g_LightLevel = level;

	return !g_TabMain->LightFilter();
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(PersonalLightLevel)
{
	OAFUN_DEBUG("c42_f5");
	uint serial = ReadUInt32BE();

	if (serial == g_PlayerSerial)
	{
		uchar level = ReadUInt8();

		if (level > 0x1F)
			level = 0x1F;

		g_PersonalLightLevel = level;
	}

	return !g_TabMain->LightFilter();
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(LoginComplete)
{
	OAFUN_DEBUG("c42_f6");
	g_Connected = true;

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(EnterWorld)
{
	OAFUN_DEBUG("c42_f7");
	uint serial = ReadUInt32BE();
	bool runAutostart = false;

	if (g_World != nullptr)
	{
		LOG("Error!!! Duplicate enter world message\n");
	}
	else
		runAutostart = true;

	g_LastPlayerCorpsePosition.setX(-1);
	g_LastPlayerCorpsePosition.setY(-1);
	g_LastPlayerDeadTimer = 0;

	g_LastTargetObject = 0;
	g_LastAttackObject = 0;
	g_MorphGraphic = 0;
	g_OriginalGraphic = 0;
	g_LastCorpseObject = 0;
	g_LastContainerObject = 0;
	g_LastStatusRequest = 0;
	g_LastStatusObject = 0;
	g_LastUseObject = 0;
	g_TargetReceived = false;
	g_MenuReceived = false;
	g_GumpReceived = false;
	g_PromptReceived = false;
	g_Target.ClearHooks();
	g_TabMacros->Reset();

	RELEASE_POINTER(g_World);

	g_World = new CGameWorld(serial);

	Move(4);

	g_Player->SetGraphic(ReadUInt16BE());
	g_OriginalGraphic = g_Player->GetGraphic();

	g_Player->SetX(ReadUInt16BE());
	g_Player->SetY(ReadUInt16BE());
	Move(1);
	g_Player->SetZ(ReadUInt8());
	g_Player->SetDirection(ReadUInt8());

	LOG("Player 0x%08lX entered the world.\n", serial);

	g_Connected = true;

	g_OrionAssistantForm->LoadProfileBySerial(serial);

	if (g_Player->Dead() && g_TabMain->NoGrayScreen())
	{
		pack16(m_Start + 9, 0x03DB);
		g_OriginalGraphic = 0x03DB;
	}

	if (runAutostart)
		g_OrionAssistantForm->RunAutostart();

	g_UOMapManager.UOAMEnterWorld();

	if (g_OrionMapSettings.GetShowOnLogin())
	{
		g_TabMain->OpenMap();
	}

	if (g_OrionMap != nullptr && g_OrionMap->isVisible())
		g_OrionMap->SetWantRedraw(true);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateHitpoints)
{
	OAFUN_DEBUG("c42_f8");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();

	CGameCharacter *obj = g_World->FindWorldCharacter(serial);

	if (obj == nullptr)
		return true;

	obj->SetMaxHits(ReadInt16BE());
	obj->SetHits(ReadInt16BE());

	if (obj->IsPlayer())
	{
		g_TabDisplay->RedrawTitle();
		g_UOMapManager.UpdateHits();
	}

	if (g_OrionMap != nullptr && g_OrionMap->isVisible())
		g_OrionMap->CheckForUpdate(obj->GetSerial());

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateMana)
{
	OAFUN_DEBUG("c42_f9");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();

	CGameCharacter *obj = g_World->FindWorldCharacter(serial);

	if (obj == nullptr)
		return true;

	obj->SetMaxMana(ReadInt16BE());
	obj->SetMana(ReadInt16BE());

	if (obj->IsPlayer())
	{
		g_TabDisplay->RedrawTitle();
		g_UOMapManager.UpdateMana();
	}

	if (g_OrionMap != nullptr && g_OrionMap->isVisible())
		g_OrionMap->CheckForUpdate(obj->GetSerial());

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateStamina)
{
	OAFUN_DEBUG("c42_f10");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();

	CGameCharacter *obj = g_World->FindWorldCharacter(serial);

	if (obj == nullptr)
		return true;

	obj->SetMaxStam(ReadInt16BE());
	obj->SetStam(ReadInt16BE());

	if (obj->IsPlayer())
	{
		g_TabDisplay->RedrawTitle();
		g_UOMapManager.UpdateStam();
	}

	if (g_OrionMap != nullptr && g_OrionMap->isVisible())
		g_OrionMap->CheckForUpdate(obj->GetSerial());

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(MobileAttributes)
{
	OAFUN_DEBUG("c42_f11");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();

	CGameCharacter *obj = g_World->FindWorldCharacter(serial);

	if (obj == nullptr)
		return true;

	obj->SetMaxHits(ReadInt16BE());
	obj->SetHits(ReadInt16BE());

	if (obj->IsPlayer())
	{
		obj->SetMaxMana(ReadInt16BE());
		obj->SetMana(ReadInt16BE());

		obj->SetMaxStam(ReadInt16BE());
		obj->SetStam(ReadInt16BE());

		g_TabDisplay->RedrawTitle();

		g_UOMapManager.UpdateHits();
		g_UOMapManager.UpdateMana();
		g_UOMapManager.UpdateStam();
	}

	if (g_OrionMap != nullptr && g_OrionMap->isVisible())
		g_OrionMap->CheckForUpdate(obj->GetSerial());

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(NewHealthbarUpdate)
{
	OAFUN_DEBUG("c42_f12");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();

	CGameCharacter *obj = g_World->FindWorldCharacter(serial);

	if (obj == nullptr)
		return true;

	ushort count = ReadUInt16BE();

	IFOR(i, 0, count)
	{
		ushort type = ReadUInt16BE();
		uchar enable = ReadUInt8(); //enable/disable

		uchar flags = obj->GetFlags();

		if (type == 1) //Poison, enable as poisonlevel + 1
		{
			if (enable)
			{
				if (m_ClientVersion >= CV_7000)
					obj->SetSA_Poisoned(true);
				else
					flags |= 0x04;
			}
			else
			{
				if (m_ClientVersion >= CV_7000)
					obj->SetSA_Poisoned(false);
				else
					flags &= ~0x04;
			}
		}
		else if (type == 2) //Yellow hits
		{
			if (enable)
				flags |= 0x08;
			else
				flags &= ~0x08;
		}
		else if (type == 3) //Red?
		{
		}

		obj->SetFlags(flags);
	}

	if (obj->IsPlayer())
		g_TabDisplay->RedrawTitle();

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdatePlayer)
{
	OAFUN_DEBUG("c42_f13");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();
	ushort graphic = ReadUInt16BE();
	uchar graphicIncrement = ReadUInt8();
	ushort color = ReadUInt16BE();
	uchar flags = ReadUInt8();
	ushort x = ReadUInt16BE();
	ushort y = ReadUInt16BE();
	ushort serverID = ReadUInt16BE();
	uchar direction = ReadUInt8();
	char z = ReadUInt8();

	bool oldDead = g_Player->Dead();

	//LOG("0x%08X 0x%04X %i 0x%04X 0x%02X %i %i %i %i %i\n", serial, graphic, graphicIncrement, color, flags, x, y, serverID, direction, z);
	g_World->UpdatePlayer(serial, graphic, graphicIncrement, color, flags, x, y, serverID, direction, z);

	if (g_MorphGraphic)
		pack16(m_Start + 5, g_MorphGraphic);

	if (g_Player->Dead())
	{
		if (g_TabMain->NoGrayScreen())
		{
			pack16(m_Start + 5, 0x03DB);
			m_Start[7] = 0;
		}

		if (oldDead != g_Player->Dead())
		{
			g_LastPlayerCorpsePosition.setX(g_Player->GetX());
			g_LastPlayerCorpsePosition.setY(g_Player->GetY());
			g_LastPlayerDeadTimer = GetTickCount() + 300000;
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(CharacterStatus)
{
	OAFUN_DEBUG("c42_f14");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();

	CGameCharacter *obj = g_World->FindWorldCharacter(serial);

	if (obj == nullptr)
		return true;

	obj->SetName(ReadString(30).c_str());

	obj->SetHits(ReadInt16BE());
	obj->SetMaxHits(ReadInt16BE());

	if (g_OrionMap != nullptr && g_OrionMap->isVisible())
		g_OrionMap->CheckForUpdate(obj->GetSerial());

	obj->SetCanChangeName(ReadUInt8() != 0);

	uchar flag = ReadUInt8();

	if (flag > 0)
	{
		obj->SetSex(ReadUInt8());

		if (serial == g_PlayerSerial)
		{
			g_Player->SetStr(ReadInt16BE());
			g_Player->SetDex(ReadInt16BE());
			g_Player->SetInt(ReadInt16BE());

			g_Player->SetStam(ReadInt16BE());
			g_Player->SetMaxStam(ReadInt16BE());
			g_Player->SetMana(ReadInt16BE());
			g_Player->SetMaxMana(ReadInt16BE());
			g_Player->SetGold(ReadUInt32BE());
			g_Player->SetArmor(ReadInt16BE());
			g_Player->SetWeight(ReadInt16BE());

			g_UOMapManager.UpdateHits();
			g_UOMapManager.UpdateMana();
			g_UOMapManager.UpdateStam();

			if (flag >= 5)
			{
				g_Player->SetMaxWeight(ReadInt16BE());
				g_Player->SetRace((RACE_TYPE)ReadUInt8());
			}
			else
			{
				if (m_ClientVersion >= CV_500A)
					g_Player->SetMaxWeight(7 * (g_Player->GetStr() / 2) + 40);
				else
					g_Player->SetMaxWeight((g_Player->GetStr() * 4) + 25);
			}

			if (flag >= 3)
			{
				g_Player->SetStatsCap(ReadInt16BE());
				g_Player->SetFollowers(ReadUInt8());
				g_Player->SetMaxFollowers(ReadUInt8());
			}

			if (flag >= 4)
			{
				g_Player->SetFireResistance(ReadInt16BE());
				g_Player->SetColdResistance(ReadInt16BE());
				g_Player->SetPoisonResistance(ReadInt16BE());
				g_Player->SetEnergyResistance(ReadInt16BE());
				g_Player->SetLuck(ReadInt16BE());
				g_Player->SetMinDamage(ReadInt16BE());
				g_Player->SetMaxDamage(ReadInt16BE());
				g_Player->SetTithingPoints(ReadUInt32BE());
			}

			g_TabSkills->UpdateStatsAndSkillsInfo();

			g_TabDisplay->RedrawTitle();
		}
	}
	else
	{
		if (serial == g_LastStatusRequest)
		{
			g_LastStatusRequest = 0;
			g_LastStatusObject = serial;
		}
	}

	g_ObjectInspectorForm->CheckUpdate(obj);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateItem)
{
	OAFUN_DEBUG("c42_f15");
	if (g_World == nullptr)
		return true;

	UPDATE_GAME_OBJECT_TYPE updateType = UGOT_ITEM;
	uint serial = ReadUInt32BE();
	ushort count = 0;
	uchar graphicIncrement = 0;
	uchar direction = 0;
	ushort color = 0;
	uchar flags = 0;

	if (serial & 0x80000000)
	{
		serial &= 0x7FFFFFFF;
		count = 1;
	}

	ushort graphic = ReadUInt16BE();

	//if (g_TheAbyss && (graphic & 0x7FFF) == 0x0E5C)
	//	return;

	if (graphic & 0x8000)
	{
		graphic &= 0x7FFF;
		graphicIncrement = ReadUInt8();
	}

	if (count)
		count = ReadUInt16BE();
	else
		count++;

	ushort x = ReadUInt16BE();

	if (x & 0x8000)
	{
		x &= 0x7FFF;
		direction = 1;
	}

	ushort y = ReadUInt16BE();

	if (y & 0x8000)
	{
		y &= 0x7FFF;
		color = 1;
	}

	if (y & 0x4000)
	{
		y &= 0x3FFF;
		flags = 1;
	}

	if (direction)
		direction = ReadUInt8();

	char z = ReadUInt8();

	if (color)
		color = ReadUInt16BE();

	if (flags)
		flags = ReadUInt8();

	if (graphic >= 0x4000)
	{
		//graphic += 0xC000;
		//updateType = UGOT_NEW_ITEM;
		updateType = UGOT_MULTI;
	}

	if (serial != g_PlayerSerial)
		g_World->UpdateGameObject(serial, graphic, graphicIncrement, count, x, y, z, direction, color, flags, count, updateType, 1);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateItemSA)
{
	OAFUN_DEBUG("c42_f16");
	if (g_World == nullptr)
		return true;

	Move(2);
	UPDATE_GAME_OBJECT_TYPE updateType = (UPDATE_GAME_OBJECT_TYPE)ReadUInt8();
	uint serial = ReadUInt32BE();
	ushort graphic = ReadUInt16BE();
	uchar graphicIncrement = ReadUInt8();
	ushort count = ReadUInt16BE();
	ushort unknown = ReadUInt16BE();
	ushort x = ReadUInt16BE();
	ushort y = ReadUInt16BE();
	uchar z = ReadUInt8();
	uchar direction = ReadUInt8();
	ushort color = ReadUInt16BE();
	uchar flags = ReadUInt8();
	ushort unknown2 = ReadUInt16BE();

	g_World->UpdateGameObject(serial, graphic, graphicIncrement, count, x, y, z, direction, color, flags, unknown, updateType, unknown2);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateObject)
{
	OAFUN_DEBUG("c42_f17");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();
	ushort graphic = ReadUInt16BE();
	ushort x = ReadUInt16BE();
	ushort y = ReadUInt16BE();
	uchar z = ReadUInt8();
	uchar direction = ReadUInt8();
	ushort color = ReadUInt16BE();
	uchar flags = ReadUInt8();
	uchar notoriety = ReadUInt8();

	if (serial == g_PlayerSerial)
	{
		if (g_Player != nullptr)
		{
			bool oldDead = g_Player->Dead();

			g_Player->SetGraphic(graphic);
			//g_Player->OnGraphicChange(1000);
			g_Player->SetColor(g_World->FixColor(color, 0));
			g_Player->SetFlags(flags);

			g_OriginalGraphic = graphic;

			if (g_MorphGraphic)
				pack16(m_Start + 7, g_MorphGraphic);

			if (g_Player->Dead())
			{
				if (g_TabMain->NoGrayScreen())
				{
					g_OriginalGraphic = 0x03DB;
					pack16(m_Start + 7, 0x03DB);
				}

				if (oldDead != g_Player->Dead())
				{
					g_LastPlayerCorpsePosition.setX(g_Player->GetX());
					g_LastPlayerCorpsePosition.setY(g_Player->GetY());
					g_LastPlayerDeadTimer = GetTickCount() + 300000;
				}
			}
		}
	}
	else
	{
		g_World->UpdateGameObject(serial, graphic, 0, 0, x, y, z, direction, color, flags, 0, UGOT_ITEM, 1);
	}

	CGameObject *obj = g_World->FindWorldObject(serial);

	if (obj == nullptr)
		return true;

	if (obj->GetNPC())
	{
		((CGameCharacter*)obj)->SetNotoriety(notoriety);
		g_ObjectInspectorForm->CheckUpdate(obj);

		if (!((CGameCharacter*)obj)->GetMaxHits())
			g_OrionAssistant.GetStatus(obj->GetSerial());
	}

	if (*m_Start != 0x78)
		Move(6);

	uint itemSerial = ReadUInt32BE();

	while (itemSerial != 0 && !IsEOF())
	{
		ushort itemGraphic = ReadUInt16BE();
		uchar layer = ReadUInt8();
		ushort itemColor = 0;

		if (m_ClientVersion >= CV_70331)
			itemColor = ReadUInt16BE();
		else if (itemGraphic & 0x8000)
		{
			itemGraphic &= 0x7FFF;
			itemColor = ReadUInt16BE();
		}

		CGameItem *item = g_World->GetWorldItem(itemSerial);

		item->SetMapIndex(g_CurrentMap);

		item->SetGraphic(itemGraphic);
		item->SetTiledataFlags(g_GetStaticFlags(itemGraphic));
		item->SetColor(g_World->FixColor(itemColor, 0));

		if (layer == OL_BACKPACK && obj->IsPlayer())
			g_Backpack = itemSerial;

		g_ObjectInspectorForm->CheckUpdate(item);
		g_World->PutEquipment(item, obj, layer);
		//item->OnGraphicChange();

		//LOG("\t0x%08X:%04X [%d] %04X\n", item->GetSerial(), item->GetGraphic(), layer, item->GetColor());

		g_World->MoveToTop(item);

		itemSerial = ReadUInt32BE();
	}

	if (obj->IsPlayer())
		g_Player->UpdateAbilities();

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(EquipItem)
{
	OAFUN_DEBUG("c42_f18");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();

	CGameItem *obj = g_World->GetWorldItem(serial);
	obj->SetMapIndex(g_CurrentMap);
	ushort graphic = ReadUInt16BE();
	obj->SetGraphic(graphic);
	obj->SetTiledataFlags(g_GetStaticFlags(graphic));
	Move(1);
	int layer = ReadUInt8();
	uint cserial = ReadUInt32BE();
	obj->SetColor(g_World->FixColor(ReadUInt16BE(), 0));

	g_World->PutEquipment(obj, cserial, layer);

	if (layer >= OL_BUY_RESTOCK && layer <= OL_SELL)
		obj->Clear();
	else if (layer == OL_BACKPACK && cserial == g_PlayerSerial)
		g_Backpack = serial;

	if (cserial == g_PlayerSerial && (layer == OL_1_HAND || layer == OL_2_HAND))
		g_Player->UpdateAbilities();

	g_ObjectInspectorForm->CheckUpdate(obj);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateContainedItem)
{
	OAFUN_DEBUG("c42_f19");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();
	ushort graphic = ReadUInt16BE();
	uchar graphicIncrement = ReadUInt8();
	ushort count = ReadUInt16BE();
	ushort x = ReadUInt16BE();
	ushort y = ReadUInt16BE();

	if (m_ClientVersion >= CV_6017)
		Move(1);

	uint containerSerial = ReadUInt32BE();
	ushort color = ReadUInt16BE();

	CGameItem *contobj = g_World->FindWorldItem(containerSerial);

	// do not update items inside spellbook. Bug with oldstyle spellbooks.
	if (contobj != nullptr && contobj->GetGraphic() == 0x0EFA)
		return true;

	g_World->UpdateContainedItem(serial, graphic, graphicIncrement, count, x, y, containerSerial, color);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateContainedItems)
{
	OAFUN_DEBUG("c42_f20");
	if (g_World == nullptr)
		return true;

	ushort itemsCount = ReadUInt16BE();

	IFOR(i, 0, itemsCount)
	{
		uint serial = ReadUInt32BE();
		ushort graphic = ReadUInt16BE();
		uchar graphicIncrement = ReadUInt8();
		ushort count = ReadUInt16BE();
		ushort x = ReadUInt16BE();
		ushort y = ReadUInt16BE();

		if (m_ClientVersion >= CV_6017)
			Move(1);

		uint containerSerial = ReadUInt32BE();
		ushort color = ReadUInt16BE();

		if (!i)
		{
			CGameObject *container = g_World->FindWorldObject(containerSerial);

			if (container != NULL)
			{
				// do not update items inside spellbook. Bug with oldstyle spellbooks.
				if (container->GetGraphic() == 0x0EFA)
					break;

				//LOG("Making %08X empty...\n", containerSerial);

				if (container->IsCorpse())
					container->ClearUnequipped();
				else
					container->Clear();
			}
		}

		g_World->UpdateContainedItem(serial, graphic, graphicIncrement, count, x, y, containerSerial, color);
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(DeleteObject)
{
	OAFUN_DEBUG("c42_f21");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();

	if (serial == g_PlayerSerial)
		return true;

	CGameObject *obj = g_World->FindWorldObject(serial);

	if (obj != NULL)
	{
		bool updateAbilities = false;
		CGameObject *top = obj->GetTopObject();

		if (top != NULL && top->IsPlayer())
		{
			g_TabDisplay->CheckRedrawTitle(obj);

			CGameItem *item = (CGameItem*)obj;
			updateAbilities = (item->GetLayer() == OL_1_HAND || item->GetLayer() == OL_2_HAND);
		}
		else if (EnabledCommandRemovedPtrPosition())
		{
			if (obj->GetSerial() == g_LastTargetObject)
				g_LastTargetPosition = QPoint(obj->GetX(), obj->GetY());
			else if (obj->GetSerial() == g_LastAttackObject)
				g_LastAttackPosition = QPoint(obj->GetX(), obj->GetY());
		}

		if (g_PartyManager.Contains(obj->GetSerial()))
			g_PartyManager.SaveMemberInfo((CGameCharacter*)obj);

		g_World->RemoveObject(obj);

		if (updateAbilities)
			g_Player->UpdateAbilities();
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateCharacter)
{
	OAFUN_DEBUG("c42_f22");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();
	CGameCharacter *obj = g_World->FindWorldCharacter(serial);

	if (obj == nullptr)
		return true;

	ushort graphic = ReadUInt16BE();
	ushort x = ReadUInt16BE();
	ushort y = ReadUInt16BE();
	char z = ReadUInt8();
	uchar direction = ReadUInt8();
	ushort color = ReadUInt16BE();
	uchar flags = ReadUInt8();
	uchar notoriety = ReadUInt8();

	obj->SetNotoriety(notoriety);

	if (obj->IsPlayer())
	{
		obj->SetFlags(flags);
	}
	else
	{
		/*if (!obj->m_Steps.empty() && obj->Direction == obj->m_Steps.back().Direction)
		{
			CWalkData &wd = obj->m_Steps.back();

			obj->X = wd.X;
			obj->Y = wd.Y;
			obj->Z = wd.Z;
			obj->Direction = wd.Direction;

			obj->m_Steps.clear();
		}*/

		if (!obj->GetMaxHits())
			g_OrionAssistant.GetStatus(serial);

		g_World->UpdateGameObject(serial, graphic, 0, 0, x, y, z, direction, color, flags, 0, UGOT_ITEM, 1);
	}

	g_World->MoveToTop(obj);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(WarmodeS)
{
	OAFUN_DEBUG("c42_f23");

	if (g_World != nullptr && g_TabMacros->GetRecording())
		g_TabMacros->AddAction(new CMacroWithIntValue(MT_WAR_MODE, ReadUInt8()), false);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(WarmodeR)
{
	OAFUN_DEBUG("c42_f24");
	if (g_World != nullptr)
	{
		g_Player->SetWarmode(ReadUInt8());
		g_World->MoveToTop(g_Player);
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateSkillsS)
{
	OAFUN_DEBUG("c42_f25");
	ushort id = ReadUInt16BE();

	if (id < g_SkillsCount)
	{
		g_Skills[id].SetStatus(ReadInt8());
		g_TabSkills->UpdateSkill(id);
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UpdateSkillsR)
{
	OAFUN_DEBUG("c42_f26");
	int type = ReadUInt8();
	bool haveCap = (type == 0x02 || type == 0xDF);
	//bool isSingleUpdate = (type == 0xFF || type == 0xDF);
	//LOG("Skill update type %i (Cap=%d)\n", type, haveCap);

	if (type == 0xFE)
		return true;

	while (m_Ptr < m_End)
	{
		ushort id = ReadUInt16BE();

		if (!id && !type)
			break;
		else if (!type || type == 0x02)
			id--;

		ushort baseVal = ReadUInt16BE();
		ushort realVal = ReadUInt16BE();
		char lock = ReadInt8();
		ushort cap = 0;

		if (haveCap)
			cap = ReadUInt16BE();

		if (id < g_SkillsCount)
		{
			CSkill &skill = g_Skills[id];
			skill.SetBaseValue((float)(baseVal / 10.0f));
			skill.SetValue((float)(realVal / 10.0f));
			if (!(int)skill.GetOldValue())
				skill.SetOldValue(skill.GetValue());
			skill.SetCap((float)(cap / 10.0f));
			skill.SetStatus(lock);

			g_TabSkills->UpdateSkill(id);

			/*if (haveCap)
				LOG("Skill %i is %i|%i|%i\n", id, baseVal, realVal, cap);
			else
				LOG("Skill %i is %i|%i\n", id, baseVal, realVal);*/
		}
		/*else
		{
			LOG("Unknown skill update %d\n", id);
		}*/
	}

	g_TabSkills->UpdateStatsAndSkillsInfo();

	//LOG("Skill updated!\n");

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(TargetS)
{
	OAFUN_DEBUG("c42_f27");
	if (g_World == nullptr)
		return true;

	if (g_TargetHandler != nullptr)
	{
		(g_OrionAssistant.*(g_TargetHandler))(m_Start);
		g_TargetHandler = nullptr;
		g_Target.Reset();
		g_Target.RestoreTarget();

		return false;
	}

	return g_Target.SetSendData(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(TargetR)
{
	OAFUN_DEBUG("c42_f28");
	g_TargetReceived = true;

	return g_Target.SetRecvData(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(MultiPlacement)
{
	OAFUN_DEBUG("c42_f29");
	g_TargetReceived = true;

	return g_Target.SetMultiData(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(ClientTalkS)
{
	OAFUN_DEBUG("c42_f30");

	Move(1);
	ushort fontColor = ReadUInt16BE();
	Move(2);

	QString str = ReadString(0).c_str();
	int prefixLen = g_TabHotkeys->IsCommandPrefix(str);

	if (prefixLen)
	{
		str.remove(0, prefixLen);
		g_TextCommandManager.DoCommand(str);
		return false;
	}
	else if (str.length() && str.at(0) == '-')
		return !g_UOMapManager.UOAnotifyCmd(qPrintable(str));

	if (g_World != nullptr && g_TabMacros->GetRecording())
	{
		g_TabMacros->AddAction(new CMacroWithStringValue(MT_SAY, str), false);
	}

	ushort replaceFontColorValue = 0;
	bool replaceFontColor = g_TabMain->GetFontColor(replaceFontColorValue);

	if (!replaceFontColor && replaceFontColorValue != fontColor)
	{
		replaceFontColorValue = fontColor;
		g_TabMain->SetFontColor(false, fontColor);
	}

	if (replaceFontColor)
		pack16(m_Start + 4, replaceFontColorValue);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(ClientTalkR)
{
	OAFUN_DEBUG("c42_f31");
	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UnicodeClientTalk)
{
	OAFUN_DEBUG("c42_f32");
	SPEECH_TYPE type = (SPEECH_TYPE)ReadUInt8();

	ushort fontColor = ReadUInt16BE();

	Move(6);
	QString str = "";

	if (type & ST_ENCODED_COMMAND)
	{
		ushort word = ReadUInt16BE();

		int bits = (word >> 4) * 12 - 4;

		Move(bits / 8);

		if (bits % 8)
			Move(1);

		str = str.fromStdString(ReadString(0));
	}
	else
		str = str.fromStdWString(ReadWString());

	int prefixLen = g_TabHotkeys->IsCommandPrefix(str);

	if (prefixLen)
	{
		str.remove(0, prefixLen);
		g_TextCommandManager.DoCommand(str);
		return false;
	}
	else if (str.length() && str.at(0) == '-')
		return !g_UOMapManager.UOAnotifyCmd(qPrintable(str));

	if (g_World != nullptr && g_TabMacros->GetRecording())
	{
		g_TabMacros->AddAction(new CMacroWithStringValue(MT_SAY, str), false);
	}

	ushort replaceFontColorValue = 0;
	bool replaceFontColor = g_TabMain->GetFontColor(replaceFontColorValue);

	if (!replaceFontColor && replaceFontColorValue != fontColor)
	{
		replaceFontColorValue = fontColor;
		g_TabMain->SetFontColor(false, fontColor);
	}

	if (replaceFontColor)
		pack16(m_Start + 4, replaceFontColorValue);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(Talk)
{
	OAFUN_DEBUG("c42_f33");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();
	ushort graphic = ReadUInt16BE();
	SPEECH_TYPE type = (SPEECH_TYPE)ReadUInt8();
	ushort textColor = ReadUInt16BE();
	ushort font = ReadUInt16BE();

	Q_UNUSED(graphic);

	if (!serial && font == 0xFFFF && textColor == 0xFFFF)
		return true;
	else if (font >= 10)
		return true;

	QString name(ReadString(0).c_str());
	QString str = "";

	if (m_Size > 44)
	{
		m_Ptr = m_Start + 44;
		str = ReadString(0).c_str();
	}

	CJournalMessage message(serial, textColor, str);

	CGameObject *obj = g_World->FindWorldObject(serial);

	if (type == ST_BROADCAST || serial == 0xFFFFFFFF || !serial || (name.toLower() == "system" && obj == nullptr))
		message.SetFlags(JMT_SYSTEM);
	else
	{
		//if (type == ST_EMOTE)
		//	str = "*" + str + "*";

		if (obj != nullptr)
		{
			if (!obj->GetName().length())
			{
				obj->SetName(name);
				g_ObjectInspectorForm->CheckUpdate(obj);
			}

			if (serial == g_PlayerSerial)
				message.SetFlags(JMT_MY);
			else if (obj->GetName() == name && str != name)
			{
				ushort replaceFontColorValue = 0;
				bool replaceFontColor = g_TabMain->GetCharactersFontColor(replaceFontColorValue);

				if (replaceFontColor)
					pack16(m_Start + 10, replaceFontColorValue);
			}
		}
	}

	g_JournalManager.Add(message);

	if (g_TabFiltersReplaces->CheckReplaceText(str, serial, graphic, type, textColor, font, 0, false))
		return false;
	else if (g_TabFiltersSpeech->CheckSpeechInFilter(str, textColor))
		return false;

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UnicodeTalk)
{
	OAFUN_DEBUG("c42_f34");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();
	ushort graphic = ReadUInt16BE();
	SPEECH_TYPE type = (SPEECH_TYPE)ReadUInt8();
	ushort textColor = ReadUInt16BE();
	ushort font = ReadUInt16BE();
	uint language = ReadUInt32BE();

	Q_UNUSED(graphic);
	Q_UNUSED(language);

	if (!serial && font == 0xFFFF && textColor == 0xFFFF)
		return true;

	QString name(ReadString(0).c_str());
	QString str = "";

	if (m_Size > 48)
	{
		m_Ptr = m_Start + 48;
		int strLen = (m_Size - 48) / 2;
		str = str.fromWCharArray(ReadWString(strLen).c_str());
	}

	CJournalMessage message(serial, textColor, str);

	CGameObject *obj = g_World->FindWorldObject(serial);

	if (type == ST_BROADCAST || serial == 0xFFFFFFFF || !serial || (name.toLower() == "system" && obj == nullptr))
		message.SetFlags(JMT_SYSTEM);
	else
	{
		//if (type == ST_EMOTE)
		//	str = "*" + str + "*";

		if (obj != nullptr)
		{
			if (!obj->GetName().length())
			{
				obj->SetName(name);
				g_ObjectInspectorForm->CheckUpdate(obj);
			}

			if (serial == g_PlayerSerial)
				message.SetFlags(JMT_MY);
			else if (obj->GetName() == name && str != name)
			{
				ushort replaceFontColorValue = 0;
				bool replaceFontColor = g_TabMain->GetCharactersFontColor(replaceFontColorValue);

				if (replaceFontColor)
					pack16(m_Start + 10, replaceFontColorValue);
			}
		}
	}

	g_JournalManager.Add(message);

	if (g_TabFiltersReplaces->CheckReplaceText(str, serial, graphic, type, textColor, font, language, true))
		return false;
	else if (g_TabFiltersSpeech->CheckSpeechInFilter(str, textColor))
		return false;

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(SetWeather)
{
	OAFUN_DEBUG("c42_f35");
	g_Weather.Type = ReadUInt8();
	g_Weather.Count = ReadUInt8();
	g_Weather.Temperature = ReadUInt8();

	return !g_TabMain->WeatherFilter();
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(Season)
{
	OAFUN_DEBUG("c42_f36");
	g_Season.Index = ReadUInt8();
	g_Season.Music = ReadUInt8();

	return !g_TabMain->SeasonFilter();
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(DisplayDeath)
{
	OAFUN_DEBUG("c42_f37");
	if (g_World == nullptr)
		return true;

	g_LastCorpseObject = ReadUInt32BE(4);

	if (g_TabMain->CorpsesAutoopen())
		g_CorpseManager.Add(CCorpseInfo(g_LastCorpseObject, g_StartTimer.elapsed() + g_TabMain->CorpseKeepDelay()));

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(OpenMenu)
{
	OAFUN_DEBUG("c42_f38");
	g_MenuReceived = true;

	return g_MenuManager.OnPacketReceived(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(MenuChoice)
{
	OAFUN_DEBUG("c42_f39");
	return g_MenuManager.OnPacketSend(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(DeathScreenS)
{
	OAFUN_DEBUG("c42_f40");
	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(DeathScreenR)
{
	OAFUN_DEBUG("c42_f41");
	if (g_World != nullptr /*&& !ReadUInt8()*/)
	{
		if (g_TabMain->Tracker())
		{
			//g_OrionAssistant.Track(false, -1, -1);
			g_OrionAssistant.Track(true, -1, -1);
		}

		g_LastPlayerCorpsePosition.setX(g_Player->GetX());
		g_LastPlayerCorpsePosition.setY(g_Player->GetY());
		g_LastPlayerDeadTimer = GetTickCount() + 300000;

		if (g_TabMain->NoDeathScreen())
		{
			g_OrionAssistant.ClientPrint("You are DEAD!");
			return false;
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(OpenContainer)
{
	OAFUN_DEBUG("c42_f44");
	uint serial = ReadUInt32BE();
	ushort graphic = ReadUInt16BE();

	if (graphic == 0x0030)
		return g_TabAgentsShop->OnPacketReceivedOpenContaier(serial);

	g_LastContainerObject = serial;

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(BuffDebuff)
{
	OAFUN_DEBUG("c42_f45");
	if (g_World == nullptr)
		return true;

	const int tableCount = 126;

	static const ushort table[tableCount] =
	{
	    0x754C, 0x754A, 0x0000, 0x0000, 0x755E, 0x7549, 0x7551, 0x7556, 0x753A, 0x754D,
	    0x754E, 0x7565, 0x753B, 0x7543, 0x7544, 0x7546, 0x755C, 0x755F, 0x7566, 0x7554,
	    0x7540, 0x7568, 0x754F, 0x7550, 0x7553, 0x753E, 0x755D, 0x7563, 0x7562, 0x753F,
	    0x7559, 0x7557, 0x754B, 0x753D, 0x7561, 0x7558, 0x755B, 0x7560, 0x7541, 0x7545,
	    0x7552, 0x7569, 0x7548, 0x755A, 0x753C, 0x7547, 0x7567, 0x7542, 0x758A, 0x758B,
	    0x758C, 0x758D, 0x0000, 0x758E, 0x094B, 0x094C, 0x094D, 0x094E, 0x094F, 0x0950,
	    0x753E, 0x5011, 0x7590, 0x7591, 0x7592, 0x7593, 0x7594, 0x7595, 0x7596, 0x7598,
	    0x7599, 0x759B, 0x759C, 0x759E, 0x759F, 0x75A0, 0x75A1, 0x75A3, 0x75A4, 0x75A5,
	    0x75A6, 0x75A7, 0x75C0, 0x75C1, 0x75C2, 0x75C3, 0x75C4, 0x75F2, 0x75F3, 0x75F4,
	    0x75F5, 0x75F6, 0x75F7, 0x75F8, 0x75F9, 0x75FA, 0x75FB, 0x75FC, 0x75FD, 0x75FE,
	    0x75FF, 0x7600, 0x7601, 0x7602, 0x7603, 0x7604, 0x7605, 0x7606, 0x7607, 0x7608,
	    0x7609, 0x760A, 0x760B, 0x760C, 0x760D, 0x760E, 0x760F, 0x7610, 0x7611, 0x7612,
	    0x7613, 0x7614, 0x7615, 0x75C5, 0x75F6, 0x761B
	};

	static const QString nameTable[tableCount] =
	{
	    "dismount", "disarm", "0x0000", "0x0000", "night sight", "death strike", "evil omen", "0x7556", "regeneration", "divine fury",
	    "enemy of one", "hiding", "meditation", "blood oath caster", "blood oath", "corpse skin", "mind rot", "pain spike", "strangle", "gift of renewal",
	    "attune weapon", "thunderstorm", "essence of wind", "ethereal voyage", "gift of life", "arcane empowerment", "mortal strike", "reactive armor", "protection", "arch protection",
	    "magic reflection", "incognito", "disguised", "animal form", "polymorph", "invisibility", "paralyze", "poison", "bleed", "clumsy",
	    "feeblemind", "weaken", "curse", "mass curse", "agility", "cunning", "strength", "bless", "sleep", "0x758B",
	    "spell plague", "0x758D", "0x0000", "0x758E", "0x094B", "0x094C", "0x094D", "0x094E", "0x094F", "0x0950",
	    "0x753E", "0x5011", "0x7590", "0x7591", "0x7592", "0x7593", "0x7594", "0x7595", "0x7596", "0x7598",
	    "0x7599", "0x759B", "0x759C", "0x759E", "0x759F", "0x75A0", "0x75A1", "0x75A3", "0x75A4", "0x75A5",
	    "0x75A6", "0x75A7", "0x75C0", "0x75C1", "0x75C2", "0x75C3", "0x75C4", "0x75F2", "0x75F3", "0x75F4",
		"0x75F5", "0x75F6", "0x75F7", "0x75F8", "counter attack", "0x75FA", "0x75FB", "0x75FC", "0x75FD", "0x75FE",
	    "0x75FF", "0x7600", "0x7601", "0x7602", "0x7603", "0x7604", "0x7605", "0x7606", "0x7607", "0x7608",
	    "0x7609", "0x760A", "0x760B", "0x760C", "0x760D", "0x760E", "0x760F", "0x7610", "0x7611", "0x7612",
	    "0x7613", "0x7614", "0x7615", "0x75C5", "0x75F6", "0x761B"
	};

	const ushort buffIconStart = 0x03E9;

	uint serial = ReadUInt32BE();

	ushort iconID = ReadUInt16BE() - buffIconStart;

	if (iconID < tableCount && serial == g_PlayerSerial) //buff
	{
		ushort mode = ReadUInt16BE();

		if (mode)
		{
			Move(12);

			ushort timer = ReadUInt16BE();

			Move(3);

			/*uint titleCliloc =*/ ReadUInt32BE();
			uint descriptionCliloc = ReadUInt32BE();
			/*uint wtfCliloc =*/ ReadUInt32BE();

			Move(4);

			if (descriptionCliloc)
				ReadWString(0, false);

			g_BuffManager.Add(table[iconID], timer, nameTable[iconID]);
		}
		else
			g_BuffManager.Remove(table[iconID]);
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(AttackCharacter)
{
	OAFUN_DEBUG("c42_f46");
	uint serial = ReadUInt32BE();

	if (serial)
		g_LastAttackObject = serial;

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(ExtendedCommandS)
{
	OAFUN_DEBUG("c42_f47");

	ushort cmd = ReadUInt16BE();

	if (cmd == 0x0009) //Wrestling stun ability
	{
		if (g_World != nullptr && g_TabMacros->GetRecording())
			g_TabMacros->AddAction(new CMacro(MT_WRESTLING_STUN), false);
	}
	else if (cmd == 0x000A) //Wrestling disarm ability
	{
		if (g_World != nullptr && g_TabMacros->GetRecording())
			g_TabMacros->AddAction(new CMacro(MT_WRESTLING_DISARM), false);
	}
	else if (cmd == 0x001C) //Cast spell
	{
		bool lastSpell = g_LastSpellIndex;

		Move(2);
		g_LastSpellIndex = ReadInt16BE();

		if (lastSpell != g_LastSpellIndex)
			g_TabDisplay->RedrawTitle();

		if (g_World != nullptr && g_TabMacros->GetRecording())
		{
			CMacro *macro = new CMacroWithIntValue(MT_CAST_NEW, g_LastSpellIndex);

			if (lastSpell == g_LastSpellIndex)
				macro->SetConvertType(MCT_LAST);

			g_TabMacros->AddAction(macro, false);
		}
	}
	else if (cmd == 0x0013) //Context menu request
	{
		uint serial = ReadUInt32BE();

		if (g_World != nullptr && g_TabMacros->GetRecording())
			g_TabMacros->AddAction(new CMacroWithObject(MT_REQUEST_CONTEXT_MENU, serial, 0, 0), false);
	}
	else if (cmd == 0x0015) //Context menu selection
	{
		uint serial = ReadUInt32BE();
		ushort menuID = ReadInt16BE();

		if (g_World != nullptr && g_TabMacros->GetRecording())
			g_TabMacros->AddAction(new CMacroWithObject(MT_WAIT_FOR_CONTEXT_MENU, serial, menuID, 0), false);
	}
	else if (cmd == 0x0032) //Toggle gargoyle flying
	{
		if (g_World != nullptr && g_TabMacros->GetRecording())
			g_TabMacros->AddAction(new CMacro(MT_TOGGLE_GARGOYLE_FLYING), false);
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(ExtendedCommandR)
{
	OAFUN_DEBUG("c42_f48");
	ushort cmd = ReadUInt16BE();

	switch (cmd)
	{
		case 6: //party
			return g_PartyManager.OnPacketReceived(*this);
		case 8: //
		{
			uint oldMap = g_CurrentMap;
			g_CurrentMap = ReadUInt8();

			if (g_CurrentMap > 5)
				g_CurrentMap = 0;

			g_UOMapManager.UOAMnotifyLand();

			if (oldMap != g_CurrentMap)
				g_FileManager.OnMapChange();

			break;
		}
		case 0x14: //Display Popup/context menu (2D and KR)
		{
			g_ContextMenuReceived = true;
			return g_ContextMenuManager.OnPacketReceived(*this);

			break;
		}
		case 0x18: //Enable map (diff) patches
		{
			g_FileManager.ApplyPatches(*this);

			break;
		}
		default:
			break;
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(SecureTradingS)
{
	OAFUN_DEBUG("c42_f49");
	if (g_World == nullptr)
		return true;

	return g_TradeManager.OnPacketSend(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(SecureTradingR)
{
	OAFUN_DEBUG("c42_f50");
	if (g_World == nullptr)
		return true;

	g_TradeReceived = true;
	return g_TradeManager.OnPacketReceived(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(DyeDataS)
{
	OAFUN_DEBUG("c42_f51");

	uint serial = ReadUInt32BE();
	Move(2);

	if (serial == 0xFFFFFFFF)
		g_TabMain->SetFontColor(true, ReadUInt16BE());
	else if (serial == 0xFFFFFFFE)
		g_TabMain->SetCharactersFontColor(true, ReadUInt16BE());

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(DyeDataR)
{
	OAFUN_DEBUG("c42_f52");
	return true;
}
//---------------------------------------------------------------------------
PACKET_HANDLER(OpenGump)
{
	OAFUN_DEBUG("c42_f53");
	if (g_World == nullptr)
		return true;

	/*if (g_ProfileParam.Requested && g_ProfileParam.Timer > GetTickCount())
	{
		g_ProfileParam.Requested = false;
		TGameObject *obj = World->FindWorldObject(g_ProfileParam.Serial);

		if (obj != NULL)
			obj->SetProfile(true);
		if (g_ProfileParam.NoShow)
			return false;
	}
	else
		g_ProfileParam.Requested = false;*/

	g_GumpReceived = true;

	return g_GumpManager.OnPacketReceived(*this);
}
//---------------------------------------------------------------------------
PACKET_HANDLER(ChoiceGump)
{
	OAFUN_DEBUG("c42_f54");
	if (g_World == nullptr)
		return true;

	return g_GumpManager.OnPacketSend(*this);
}
//---------------------------------------------------------------------------
PACKET_HANDLER(PlaySoundEffect)
{
	OAFUN_DEBUG("c42_f55");
	if (g_World == nullptr)
		return true;

	Move(1);
	ushort index = ReadUInt16BE();
	ushort volume = ReadUInt16BE();
	ushort x = ReadUInt16BE();
	ushort y = ReadUInt16BE();

	if (g_TabMain->SoundEcho())
	{
		QString text = "";
		text.sprintf("soundecho: id=0x%04X volume=%i x=%i y=%i", index, volume, x, y);

		CJournalMessage message(0xFFFFFFFF, 0x0000, text);
		message.SetFlags(JMT_SYSTEM);

		g_JournalManager.Add(message);
	}

	if (g_TabFiltersSound->CheckSoundInFilter(index))
		return false;

	return true;
}
//---------------------------------------------------------------------------
PACKET_HANDLER(GraphicEffect)
{
	OAFUN_DEBUG("c42_f56");
	if (g_World == nullptr)
		return true;

	if (g_TabMain->EffectEcho())
	{
		uchar type = ReadUInt8();

		QString text = "";

		if (type > 3)
		{
			if (type == 4 && *m_Start == 0x70)
			{
				Move(8);
				ushort val = ReadInt16BE();

				text.sprintf("effectecho: type=%i mode=%i", type, val);
			}
			else
				text.sprintf("effectecho: type=%i", type);

			CJournalMessage message(0xFFFFFFFF, 0x0000, text);
			message.SetFlags(JMT_SYSTEM);

			g_JournalManager.Add(message);

			return true;
		}

		uint sourceSerial = ReadUInt32BE();
		uint destSerial = ReadUInt32BE();
		ushort graphic = ReadUInt16BE();
		short sourceX = ReadInt16BE();
		short sourceY = ReadInt16BE();
		char sourceZ = ReadInt8();
		short destX = ReadInt16BE();
		short destY = ReadInt16BE();
		char destZ = ReadInt8();
		uchar speed = ReadUInt8();
		short duration = (short)ReadUInt8() * 50;
		//what is in 24-25 bytes?
		Move(2);
		uchar fixedDirection = ReadUInt8();
		uchar explode = ReadUInt8();

		uint color = 0;
		uint renderMode = 0;

		if (*m_Start != 0x70)
		{
			//0xC0
			color = ReadUInt32BE();
			renderMode = ReadUInt32BE() % 7;

			if (*m_Start == 0xC7)
			{
				/*
				0000: c7 03 00 13 82 2f 00 00 00 00 37 6a 05 d6 06 47 : ...../....7j...G
				0010: 15 05 d6 06 47 15 09 20 00 00 01 00 00 00 00 00 : ....G.. ........
				0020: 00 00 00 00 13 8d 00 01 00 00 00 13 82 2f 03 00 : ............./..
				0030: 00 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- : .
				*/
			}
		}

		text.sprintf("effectecho: type=%i src_serial=0x%08X src_x=%i src_y=%i src_z=%i dest_serial=0x%08X dest_x=%i dest_y=%i dest_z=%i graphic=0x%04X speed=%i duration=%i fixed=%i explode=%i color=%i render_mode=%i",
		             type, sourceSerial, sourceX, sourceY, sourceZ, destSerial, destX, destY, destZ, graphic, speed, duration, fixedDirection, explode, color, renderMode);

		CJournalMessage message(0xFFFFFFFF, 0x0000, text);
		message.SetFlags(JMT_SYSTEM);

		g_JournalManager.Add(message);
	}

	return true;
}
//---------------------------------------------------------------------------
PACKET_HANDLER(CharacterAnimation)
{
	OAFUN_DEBUG("c42_f57");
	if (g_World == nullptr)
		return true;

	if (g_TabMain->AnimationEcho())
	{
		uint serial = ReadUInt32BE();
		ushort action = ReadUInt16BE();
		ushort frameCount = ReadUInt16BE();
		frameCount = 0;
		ushort repeatMode = ReadUInt16BE();
		bool frameDirection = (ReadUInt8() == 0); //true - forward, false - backward
		bool repeat = (ReadUInt8() != 0);
		uchar delay = ReadUInt8();

		QString text = "";
		text.sprintf("animationecho: serial=0x%08X action=%i count=%i repeat_mode=%i forward=%i repeat=%i delay=%i",
		             serial, action, frameCount, repeatMode, frameDirection, repeat, delay);

		CJournalMessage message(0xFFFFFFFF, 0x0000, text);
		message.SetFlags(JMT_SYSTEM);

		g_JournalManager.Add(message);
	}

	return true;
}
//---------------------------------------------------------------------------
PACKET_HANDLER(DisplayMap)
{
	OAFUN_DEBUG("c42_f58");
	if (g_World == nullptr)
		return true;

	if (EnabledFeatureMapEcho())
	{
		Move(6);

		ushort startX = ReadUInt16BE();
		ushort startY = ReadUInt16BE();
		ushort endX = ReadUInt16BE();
		ushort endY = ReadUInt16BE();

		int x = (endX + startX);
		int y = (endY + startY);

		if (*m_Start != 0xF5)
		{
			x /= 2;
			y /= 2;
		}

		QString text = "";
		text.sprintf("Map opened to: %i:%i", x, y);

		g_OrionAssistant.ClientPrint(text);

		text.sprintf("mapecho: x=%i y=%i", x, y);
		CJournalMessage message(0xFFFFFFFF, 0x0000, text);
		message.SetFlags(JMT_SYSTEM);

		g_JournalManager.Add(message);

		if (g_TabMain->Tracker())
			g_OrionAssistant.Track(true, x, y);
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(DisplayClilocString)
{
	OAFUN_DEBUG("c42_f59");
	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();
	ushort graphic = ReadUInt16BE();
	uchar type = ReadUInt8();
	ushort color = ReadUInt16BE();
	ushort font = (uchar)ReadUInt16BE();
	uint cliloc = ReadUInt32BE();

	uchar flags = 0;

	if (*m_Start == 0xCC)
		flags = ReadUInt8();

	Q_UNUSED(flags);

	QString name = ReadString(30).c_str();

	string affix = "";
	if (*m_Start == 0xCC)
		affix = ReadString(0);

	QString str = g_ClilocManager.ParseArgumentsToClilocString(cliloc, false, (wchar_t*)m_Ptr);

	//LOG("str:%s\n", str.toStdString().c_str());
	CJournalMessage message(serial, color, str);

	CGameObject *obj = g_World->FindWorldObject(serial);

	if (/*type == ST_BROADCAST ||*/ serial == 0xFFFFFFFF || !serial || (name.toLower() == "system" && obj == nullptr))
		message.SetFlags(JMT_SYSTEM);
	else
	{
		//if (type == ST_EMOTE)
		//	str = "*" + str + "*";

		if (obj != nullptr)
		{
			if (!obj->GetName().length())
				obj->SetName(name);

			if (serial == g_PlayerSerial)
				message.SetFlags(JMT_MY);
			else if (obj->GetName() == name && str != name)
			{
				ushort replaceFontColorValue = 0;
				bool replaceFontColor = g_TabMain->GetCharactersFontColor(replaceFontColorValue);

				if (replaceFontColor)
					pack16(m_Start + 10, replaceFontColorValue);
			}
		}
	}

	g_JournalManager.Add(message);

	if (g_TabFiltersReplaces->CheckReplaceText(str, serial, graphic, type, color, font, 0, true))
		return false;
	else if (g_TabFiltersSpeech->CheckSpeechInFilter(str, color))
		return false;

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(MegaClilocS)
{
	OAFUN_DEBUG("c42_f60");

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(MegaClilocR)
{
	OAFUN_DEBUG("c42_f61");
	if (g_World == nullptr)
		return true;

	ushort unknown = ReadUInt16BE();

	if (unknown > 1)
		return true;

	uint serial = ReadUInt32BE();

	Move(2);
	uint clilocRevision = ReadUInt32BE();

	QString name = "";
	QString message = "";

	puchar end = m_Start + m_Size;
	QStringList list;

	while (m_Ptr < end)
	{
		uint cliloc = ReadUInt32BE();

		if (!cliloc)
			break;

		short len = ReadInt16BE();

		wstring argument = L"";

		if (len > 0)
		{
			argument = wstring((wchar_t*)m_Ptr, len / 2);
			m_Ptr += len;
		}

		QString str = g_ClilocManager.ParseArgumentsToClilocString(cliloc, true, argument);

		bool found = false;

		if (!list.empty())
		{
			for (const QString &tempStr : list)
			{
				if (tempStr == str)
				{
					found = true;
					break;
				}
			}
		}

		if (!found)
			list.push_back(str);

		//LOG("Cliloc: argstr=%s\n", ToString(str).c_str());

		//LOG("Cliloc: 0x%08X len=%i arg=%s\n", cliloc, len, ToString(argument).c_str());
	}

	bool first = true;

	if (!list.empty())
	{
		for (const QString &str : list)
		{
			if (first)
			{
				name = str;
				first = false;
			}

			if (message.length())
				message += "\n";

			message += str;
		}
	}

	CGameObject *obj = g_World->FindWorldObject(serial);

	if (obj != nullptr && !obj->GetNPC())
		obj->SetName(name);

	g_ObjectPropertiesManager.Add(serial, CObjectProperty(clilocRevision, message));

	//LOG_DUMP((PBYTE)message.c_str(), message.length() * 2);

	//LOG("message=%s\n", ToString(message).c_str());

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(Resend)
{
	OAFUN_DEBUG("c42_f62");

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(WalkRequest)
{
	OAFUN_DEBUG("c42_f62_1");
	int direction = ReadUInt8();

	auto getNewXY = [](const int &direction, int &x, int &y)
	{
		switch (direction & 7)
		{
			case 0:
			{
				y--;
				break;
			}
			case 1:
			{
				x++;
				y--;
				break;
			}
			case 2:
			{
				x++;
				break;
			}
			case 3:
			{
				x++;
				y++;
				break;
			}
			case 4:
			{
				y++;
				break;
			}
			case 5:
			{
				x--;
				y++;
				break;
			}
			case 6:
			{
				x--;
				break;
			}
			case 7:
			{
				x--;
				y--;
				break;
			}
			default:
				break;
		}
	};

	if (g_Player != nullptr && g_TabMain->AutoOpenDoors() && !g_Player->Dead() && !g_Player->IsGM() && !(direction % 2))
	{
		int x = g_Player->GetX();
		int y = g_Player->GetY();

		getNewXY(direction, x, y);

		if ((direction & 7) == (g_Player->GetDirection() & 7))
			getNewXY(direction, x, y);

		g_OrionAssistantForm->AutoOpenDoor(x, y, g_Player->GetZ());
	}

	if (g_World != nullptr && g_TabMacros->GetRecording())
	{
		if ((direction & 7) != (g_Player->GetDirection() & 7))
			direction = -(direction | 0x80);

		g_TabMacros->AddAction(new CMacroWithIntValue(MT_WALK, direction), false);
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(ConfirmWalk)
{
	OAFUN_DEBUG("c42_f63");

	if (g_World == nullptr)
		return true;

	if (g_TabDisplay->ApplyNotorietyColorOnName())
	{
		Move(1);
		uchar noto = ReadUInt8() & 0x0F;

		if (noto != g_Player->GetNotoriety())
		{
			g_Player->SetNotoriety(noto);

			g_TabDisplay->RedrawTitle();
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(ASCIIPromptS)
{
	OAFUN_DEBUG("c42_f64");

	return g_PromptManager.OnPacketSend(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(ASCIIPromptR)
{
	OAFUN_DEBUG("c42_f65");

	g_PromptReceived = true;

	return g_PromptManager.OnPacketReceived(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UnicodePromptS)
{
	OAFUN_DEBUG("c42_f66");

	return g_PromptManager.OnPacketSend(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(UnicodePromptR)
{
	OAFUN_DEBUG("c42_f67");

	g_PromptReceived = true;

	return g_PromptManager.OnPacketReceived(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(CharacterProfileS)
{
	OAFUN_DEBUG("c42_f68");

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(CharacterProfileR)
{
	OAFUN_DEBUG("c42_f69");

	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();

	CGameCharacter *obj = g_World->FindWorldCharacter(serial);

	if (obj != nullptr)
	{
		string topText = ReadString();

		wstring bottomText = ReadWString();
		wstring dataText = ReadWString() + L"\n" + bottomText;

		obj->SetProfileReceived(true);
		obj->SetProfile(QString(topText.c_str()) + "\n" + QString::fromStdWString(dataText));
		bool block = obj->GetBlockProfile();
		obj->SetBlockProfile(false);

		if (block)
			return false;
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(OpenPaperdoll)
{
	OAFUN_DEBUG("c42_f70");

	if (g_World == nullptr)
		return true;

	uint serial = ReadUInt32BE();

	CGameCharacter *obj = g_World->FindWorldCharacter(serial);

	if (obj != nullptr)
		obj->SetTitle(ReadString(60).c_str());

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(BuyList)
{
	OAFUN_DEBUG("c42_f71");
	g_ShopReceived = true;
	return g_TabAgentsShop->OnPacketReceivedBuyList(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(BuyReplyS)
{
	OAFUN_DEBUG("c42_f72");
	return g_TabAgentsShop->OnPacketSendBuyReply();
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(BuyReplyR)
{
	OAFUN_DEBUG("c42_f73");
	return g_TabAgentsShop->OnPacketReceivedBuyReply(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(SellList)
{
	OAFUN_DEBUG("c42_f74");
	g_ShopReceived = true;
	return g_TabAgentsShop->OnPacketReceivedSellList(*this);
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(SellReply)
{
	OAFUN_DEBUG("c42_f75");
	return g_TabAgentsShop->OnPacketSendSellReply();
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(PerformAction)
{
	OAFUN_DEBUG("c42_f76");

	uchar type = ReadUInt8();
	string str = ReadString(m_Size - /*4*/5);

	bool lastSpell = g_LastSpellIndex;

	if (type == 0x24) //Skill
	{
		int index = atoi(str.c_str());

		if (g_World != nullptr && g_TabMacros->GetRecording())
		{
			CMacro *macro = new CMacroWithIntValue(MT_USE_SKILL, index);

			if (index == g_LastSkillIndex)
				macro->SetConvertType(MCT_LAST);

			g_TabMacros->AddAction(macro, false);
		}

		g_LastSkillIndex = index;
	}
	else if (type == 0x56) //Spell
	{
		int index = atoi(str.c_str());

		if (g_World != nullptr && g_TabMacros->GetRecording())
		{
			CMacro *macro = new CMacroWithIntValue(MT_CAST, index);

			if (index == g_LastSpellIndex)
				macro->SetConvertType(MCT_LAST);

			g_TabMacros->AddAction(macro, false);
		}

		g_LastSpellIndex = index;
	}
	else if (type == 0x27) //Spell from book
	{
		int index = atoi(str.c_str());

		if (g_World != nullptr && g_TabMacros->GetRecording())
		{
			CMacro *macro = new CMacroWithIntValue(MT_CAST_FROM_BOOK, index);

			if (index == g_LastSpellIndex)
				macro->SetConvertType(MCT_LAST);

			g_TabMacros->AddAction(macro, false);
		}

		g_LastSpellIndex = index;
	}

	if (lastSpell != g_LastSpellIndex)
		g_TabDisplay->RedrawTitle();

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(KrriosClientSpecialS)
{
	OAFUN_DEBUG("c42_f77");
	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(KrriosClientSpecialR)
{
	OAFUN_DEBUG("c42_f78");
	uchar type = ReadUInt8();

	if (type == 1)
	{
		int count = (m_Size - 4) / 9;

		IFOR(i, 0, count)
		{
			PARTY_MEMBER_INFO info;
			info.Serial = ReadUInt32BE();
			info.X = ReadUInt16BE();
			info.Y = ReadUInt16BE();
			info.Map = ReadUInt8();

			g_PartyManager.SetMemberInfo(info);
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(OrionMessagesS)
{
	OAFUN_DEBUG("c42_f79");
	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(OrionMessagesR)
{
	OAFUN_DEBUG("c42_f80");
	ushort message = ReadUInt16BE();
	ushort application = (message >> 12);
	message &= 0x0FFF;

	if (application == 0x0000)
	{
		switch (message)
		{
			case OIPMT_SKILL_LIST:
			{
				g_TabSkills->SkillsListUpdated(*this);
				break;
			}
			case OIPMT_SPELL_LIST:
			{
				int count = ReadUInt16BE();

				QStringList hotkeyActions("Last");

				IFOR(i, 0, count)
				{
					g_SpellManager.m_SpellbookSpellNames.push_back(QStringList());
					QStringList &list = g_SpellManager.m_SpellbookSpellNames.back();

					int spellsCount = ReadUInt16BE();
					int spellOffset = i * 100;

					if (i > 2)
						spellOffset += 100;

					IFOR(j, 0, spellsCount)
					{
						QString spellName = ReadString().c_str();

						g_SpellManager.Insert(spellName.toLower(), j + spellOffset + 1);

						list << spellName;
					}

					hotkeyActions << list;
				}

				g_HotkeysForm->UpdateActionList(HT_CAST, hotkeyActions);
				g_HotkeysForm->UpdateActionList(HT_CAST_SELF, hotkeyActions);
				g_HotkeysForm->UpdateActionList(HT_CAST_LAST_TARGET, hotkeyActions);
				g_HotkeysForm->UpdateActionList(HT_CAST_LAST_ATTACK, hotkeyActions);
				g_HotkeysForm->UpdateActionList(HT_CAST_ENEMY, hotkeyActions);
				g_HotkeysForm->UpdateActionList(HT_CAST_FRIEND, hotkeyActions);
				g_HotkeysForm->UpdateActionList(HT_CAST_NEW_TARGET_SYSTEM, hotkeyActions);

				break;
			}
			case OIPMT_MACRO_LIST:
			{
				int count = ReadUInt16BE();

				IFOR(i, 0, count)
				{
					g_ClientMacro.push_back(CClientMacro(ReadString().c_str()));

					CClientMacro &macro = g_ClientMacro.back();

					int actionsCount = ReadUInt16BE();

					IFOR(j, 0, actionsCount)
					{
						macro.m_Actions.push_back(ReadString().c_str());
					}
				}

				break;
			}
			case OIPMT_OPEN_MAP:
			{
				g_TabMain->OpenMap();
				break;
			}
			default:
				break;
		}
	}
	else if (application == 0x000A)
	{
		switch (message)
		{
			case OAPMT_SET_FLAGS:
			{
				g_EnabledFeature = (uint64)ReadInt64BE();
				g_EnabledCommands = (uint64)ReadInt64BE();

				g_TextCommandManager.UpdateCommands();
				g_TabMain->UpdateFeatures();

				/*QString fea[] =
				{
					"OAEF_CORPSES_AUTOOPEN", // 0x0000000000000001,
					"OAEF_STEALTH_STEP_COUNTER", // 0x0000000000000002,
					"OAEF_SOUND_FILTER", // 0x0000000000000004,
					"OAEF_LIGHT_FILTER", // 0x0000000000000008,
					"OAEF_WEATHER_FILTER", // 0x0000000000000010,
					"OAEF_SEASON_FILTER", // 0x0000000000000020,
					"OAEF_NO_DEATH_SCREEN", // 0x0000000000000040, //black screen ~5 sec
					"OAEF_AUTO_OPEN_DOORS", // 0x0000000000000080,
					"OAEF_TRACKER", // 0x0000000000000100,
					"OAEF_LOOPED_SCRIPTS", // 0x0000000000000200,
					"OAEF_EXTENDED_SCRIPTS", // 0x0000000000000400,
					"OAEF_FAST_ROTATION", // 0x0000000000000800,
					"OAEF_RECURSE_CONTAINERS_SEARCH", // 0x0000000000001000,
					"OAEF_AUTOSTART", // 0x0000000000002000,
					"OAEF_IGNORE_STAMINA_CHECK", // 0x0000000000004000,
					"OAEF_PARTY_AGENT", // 0x0000000000008000,
					"OAEF_SHOP_AGENT", // 0x0000000000010000,
					"OAEF_MINIMIZE_TO_TRAY" // 0x0000000000020000,
				};

				IFOR(i, 0, 18)
				{
					QString str = fea[i];
					ushort color = g_MessagesColor;

					if (g_EnabledFeature & (1 << i))
						str += " is enabled (option)";
					else
					{
						str += " is disabled (option)";
						color = 0x0021;
					}

					g_OrionAssistant.ClientPrint(str, color);
				}

				QString sfea[] =
				{
					"OAEC_INFORMATION", // 0x0000000000000001,
					"OAEC_TEXT_WINDOW", // 0x0000000000000002,
					"OAEC_SEARCH_CONTAINER", // 0x0000000000000004,
					"OAEC_SEARCH_GROUND", // 0x0000000000000008,
					"OAEC_MOVING", // 0x0000000000000010,
					"OAEC_RESEND", // 0x0000000000000020,
					"OAEC_JOURNAL", // 0x0000000000000040,
					"OAEC_RENAME_MOUNTS", // 0x0000000000000080,
					"OAEC_SHOP_BUY", // 0x0000000000000100,
					"OAEC_SHOP_SELL", // 0x0000000000000200,
					"OAEC_HIDE", // 0x0000000000000400,
					"OAEC_MOVE_ITEMS", // 0x0000000000000800,
					"OAEC_MORPHING", // 0x0000000000001000,
					"OAEC_EMOTE_ACTIONS", // 0x0000000000002000,
					"OAEC_FIND_TYPE_OR_IGNORE", // 0x0000000000004000,
					"OAEC_FIND_ARRAYS", // 0x0000000000008000,
					"OAEC_OPTIONS_ACCESS", // 0x0000000000010000,
					"OAEC_SEARCH_CORPSE", // 0x0000000000020000,
					"OAEC_EXTENDED_MENU", // 0x0000000000040000,
					"OAEC_BUFFS", // 0x0000000000080000,
					"OAEC_SECURE_TRADING", // 0x0000000000100000,
					"OAEC_FRIEND_LIST", // 0x0000000000200000,
					"OAEC_ENEMY_LIST", // 0x0000000000400000,
					"OAEC_FONT_COLOR" // 0x0000000000800000,
				};

				IFOR(i, 0, 24)
				{
					QString str = sfea[i];
					ushort color = g_MessagesColor;

					if (g_EnabledCommands & (1 << i))
						str += " is enabled (script)";
					else
					{
						str += " is disabled (script)";
						color = 0x0021;
					}

					g_OrionAssistant.ClientPrint(str, color);
				}*/

				break;
			}
			default:
				break;
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(DoubleClick)
{
	OAFUN_DEBUG("c42_f81");

	uint serial = ReadUInt32BE();

	if (g_World != nullptr && g_TabMacros->GetRecording())
	{
		CGameObject *obj = g_World->FindWorldObject(serial);

		CMacro *macro = NULL;

		if (obj != nullptr)
			macro = new CMacroWithObject(MT_USE, serial, obj->GetGraphic(), obj->GetColor());
		else
			macro = new CMacroWithObject(MT_USE, serial, 0, 0);

		if (serial == g_PlayerSerial)
			macro->SetConvertType(MCT_SELF);
		//else if (serial == g_LastUseObject)
		//	macro->SetConvertType(MCT_LAST);

		g_TabMacros->AddAction(macro, false);
	}

	g_LastUseObject = serial;

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(AttackRequest)
{
	OAFUN_DEBUG("c42_f82");

	if (g_World != nullptr && g_TabMacros->GetRecording())
	{
		uint serial = ReadUInt32BE();

		CGameCharacter *obj = g_World->FindWorldCharacter(serial);

		CMacro *macro = NULL;

		if (obj != nullptr)
			macro = new CMacroWithObject(MT_ATTACK, serial, obj->GetGraphic(), obj->GetColor());
		else
			macro = new CMacroWithObject(MT_ATTACK, serial, 0, 0);

		if (serial == g_LastAttackObject)
			macro->SetConvertType(MCT_LASTATTACK);
		else if (serial == g_LastTargetObject)
			macro->SetConvertType(MCT_LASTTARGET);
		else if (serial == COrionAssistant::TextToSerial("enemy"))
			macro->SetConvertType(MCT_ENEMY);

		g_TabMacros->AddAction(macro, false);
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(HelpRequest)
{
	OAFUN_DEBUG("c42_f83");

	if (g_World != nullptr && g_TabMacros->GetRecording())
		g_TabMacros->AddAction(new CMacro(MT_HELP_MENU_REQUEST), false);

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(AOSCommandsS)
{
	OAFUN_DEBUG("c42_f84");

	uint serial = ReadUInt32BE();
	Q_UNUSED(serial);
	ushort cmd = ReadUInt16BE();

	if (cmd == 0x0019) //Combat ability
	{
		if (g_World != nullptr && g_TabMacros->GetRecording())
		{
			Move(4);
			uchar ability = ReadUInt8();

			CMacro *macro = new CMacro(MT_COMBAT_ABILITY);

			if (g_Ability[0] == ability)
				macro->SetConvertType(MCT_PRIMARY);
			else
				macro->SetConvertType(MCT_SECONDARY);

			g_TabMacros->AddAction(macro, false);
		}
	}
	else if (cmd == 0x0028) //Guild
	{
		if (g_World != nullptr && g_TabMacros->GetRecording())
			g_TabMacros->AddAction(new CMacro(MT_GUILD_MENU_REQUEST), false);
	}
	else if (cmd == 0x0032) //Quests
	{
		if (g_World != nullptr && g_TabMacros->GetRecording())
			g_TabMacros->AddAction(new CMacro(MT_QUEST_MENU_REQUEST), false);
	}

	return true;
}
//----------------------------------------------------------------------------------
PACKET_HANDLER(AOSCommandsR)
{
	OAFUN_DEBUG("c42_f85");

	return true;
}
//----------------------------------------------------------------------------------
