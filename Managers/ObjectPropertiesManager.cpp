﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ObjectPropertiesManager.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "ObjectPropertiesManager.h"
//----------------------------------------------------------------------------------
CObjectPropertiesManager g_ObjectPropertiesManager;
//----------------------------------------------------------------------------------
//----------------------------------CObjectProperty---------------------------------
//----------------------------------------------------------------------------------
CObjectProperty::CObjectProperty(const uint &revision, const QString &data)
: m_Revision(revision), m_Data(data)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
//------------------------------CObjectPropertiesManager----------------------------
//----------------------------------------------------------------------------------
CObjectPropertiesManager::~CObjectPropertiesManager()
{
	OAFUN_DEBUG("");
	m_Map.clear();
}
//----------------------------------------------------------------------------------
QString CObjectPropertiesManager::GetData(const uint &serial)
{
	OAFUN_DEBUG("");
	OBJECT_PROPERTIES_MAP::iterator it = m_Map.find(serial);

	if (it == m_Map.end())
		return "";

	return it.value().GetData();
}
//----------------------------------------------------------------------------------
void CObjectPropertiesManager::Add(const uint &serial, const CObjectProperty &objectProperty)
{
	OAFUN_DEBUG("");
	m_Map.insert(serial, objectProperty);
}
//----------------------------------------------------------------------------------
