/***********************************************************************************
**
** SkillManager.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SKILLMANAGER_H
#define SKILLMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
typedef QMap<QString, int> SKILLS_MAP;
//----------------------------------------------------------------------------------
class CSkillManager
{
private:
	SKILLS_MAP m_AllSkills;
	SKILLS_MAP m_Skills;

public:
	CSkillManager();
	~CSkillManager();

	void Insert(const QString &name, const bool &usable, const int &index);

	int GetIndex(const QString &name);

	int Find(const QString &name);

	void Use(const QString &name);

	void TargetUse(const QString &name, const uint &serial);
};
//----------------------------------------------------------------------------------
extern CSkillManager g_SkillManager;
//----------------------------------------------------------------------------------
#endif // SKILLMANAGER_H
//----------------------------------------------------------------------------------
