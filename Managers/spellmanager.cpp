// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** SpellManager.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "spellmanager.h"
#include "../OrionAssistant/orionassistant.h"
#include "../OrionAssistant/Packets.h"

CSpellManager g_SpellManager;
//----------------------------------------------------------------------------------
CSpellManager::CSpellManager()
{
}
//----------------------------------------------------------------------------------
CSpellManager::~CSpellManager()
{
}
//----------------------------------------------------------------------------------
void CSpellManager::Insert(const QString &name, const int &index)
{
	OAFUN_DEBUG("c22_f1");
	m_Spells.insert(name, index);
	m_Spells.insert(QString::number(index), index);
}
//----------------------------------------------------------------------------------
int CSpellManager::Find(QString name)
{
	OAFUN_DEBUG("c22_f2");
	if (name == "last" || name == "lastspell")
		return g_LastSpellIndex;

	SPELLS_MAP::iterator it = m_Spells.find(name);

	if (it != m_Spells.end())
		return it.value();

	return -1;
}
//----------------------------------------------------------------------------------
void CSpellManager::Cast(const QString &name)
{
	OAFUN_DEBUG("c22_f3");
	int index = Find(name.toLower());

	if (index != -1)
		CPacketCastSpellRequest(index).SendClient();
	else
		g_OrionAssistant.ClientPrint("Spell '" + name + "' is not found.");
}
//----------------------------------------------------------------------------------
void CSpellManager::TargetCast(const QString &name, const uint &serial)
{
	OAFUN_DEBUG("c22_f3");
	int index = Find(name.toLower());

	if (index != -1)
		CPacketTargetCastSpell(index, serial).SendServer();
	else
		g_OrionAssistant.ClientPrint("Spell '" + name + "' is not found.");
}
//----------------------------------------------------------------------------------
