﻿/***********************************************************************************
**
** ObjectPripertiesManager.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OBJECTPROPERTIESMANAGER_H
#define OBJECTPROPERTIESMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CObjectProperty
{
	SETGET(uint, Revision, 0)
	SETGET(QString, Data, "")

public:
	CObjectProperty() {}
	CObjectProperty(const uint &revision, const QString &data);
};
//----------------------------------------------------------------------------------
typedef QMap<uint, CObjectProperty> OBJECT_PROPERTIES_MAP;
//----------------------------------------------------------------------------------
class CObjectPropertiesManager
{
private:
	OBJECT_PROPERTIES_MAP m_Map;

public:
	CObjectPropertiesManager() {}
	virtual ~CObjectPropertiesManager();

	QString GetData(const uint &serial);

	void Add(const uint &serial, const CObjectProperty &objectProperty);
};
//----------------------------------------------------------------------------------
extern CObjectPropertiesManager g_ObjectPropertiesManager;
//----------------------------------------------------------------------------------
#endif //OBJECTPROPERTIESMANAGER_H
//----------------------------------------------------------------------------------
