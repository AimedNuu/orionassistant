// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** JournalManager.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "journalmanager.h"
#include "../OrionAssistant/textdialog.h"

CJournalManager g_JournalManager;
//----------------------------------------------------------------------------------
CJournalManager::CJournalManager()
{
}
//----------------------------------------------------------------------------------
CJournalManager::~CJournalManager()
{
	m_List.clear();
}
//----------------------------------------------------------------------------------
int CJournalManager::FindLine(QString pattern, const uint &serial, const ushort &color, const uchar &type, const uint &startTime, const uint &endTime, const bool &remove)
{
	OAFUN_DEBUG("c19_f1");
	int size = m_List.size();

	if (!pattern.length() || !size)
		return -1;

	if (m_IgnoreCase)
		pattern = pattern.toLower();

	QStringList patterns = pattern.split("|");

	DFOR(i, size - 1, 0)
	{
		CJournalMessage &message = m_List[i];

		if (serial && serial != message.GetSerial())
			continue;
		else if (color != 0xFFFF && color != message.GetColor())
			continue;
		else if (type && !(type & message.GetFlags()))
			continue;
		else if (message.GetTimer() < startTime)
			continue;
		else if (endTime && message.GetTimer() > endTime)
			continue;

		QString text = message.GetText();

		if (m_IgnoreCase)
			text = text.toLower();

		int findID = 0;

		foreach (const QString &p, patterns)
		{
			if (text.contains(p))
			{
				if (remove)
				{
					m_List.remove(i);
					break;
				}
				else
				{
					message.SetFindTextID(findID);
					return i;
				}
			}

			findID++;
		}
	}

	return -1;
}
//----------------------------------------------------------------------------------
void CJournalManager::Clear(QString pattern, const uint &serial, const ushort &color, const uchar &type)
{
	OAFUN_DEBUG("c19_f2");
	int line = FindLine(pattern, serial, color, type, 0, 0, true);

	if (line < 0 && !pattern.length())
		m_List.clear();
}
//----------------------------------------------------------------------------------
CJournalMessage *CJournalManager::Find(QString pattern, const uint &serial, const ushort &color, const uchar &type, const uint &startTime, const uint &endTime)
{
	OAFUN_DEBUG("c19_f3");
	int line = FindLine(pattern, serial, color, type, startTime, endTime, false);

	if (line < 0 || line >= m_List.size())
		return nullptr;

	return &m_List[line];
}
//----------------------------------------------------------------------------------
void CJournalManager::Add(const CJournalMessage &message)
{
	OAFUN_DEBUG("c19_f4");
	m_List.push_back(message);

	if (m_List.size() >= m_MaxLines)
		m_List.pop_front();
}
//----------------------------------------------------------------------------------
CJournalMessage *CJournalManager::Get(const uint &index)
{
	OAFUN_DEBUG("c19_f5");
	if (index >= (uint)m_List.size())
		return nullptr;

	return &m_List[index];
}
//----------------------------------------------------------------------------------
void CJournalManager::Remove(const uint &index)
{
	OAFUN_DEBUG("c19_f6");
	if (index < (uint)m_List.size())
		m_List.remove(index);
}
//----------------------------------------------------------------------------------
void CJournalManager::SetMessageText(const CJournalMessage *message, const QString &newText)
{
	IFOR(i, 0, m_List.size())
	{
		if (&m_List[i] == message)
		{
			m_List[i].SetText(newText);
			break;
		}
	}
}
//----------------------------------------------------------------------------------
CJournalMessage *CJournalManager::LastMessage()
{
	OAFUN_DEBUG("c19_f7");

	if (!m_List.empty())
		return &m_List.back();

	return nullptr;
}
//----------------------------------------------------------------------------------
void CJournalManager::ShowLines(int count)
{
	OAFUN_DEBUG("c19_f8");
	g_TextDialog->ClearText();

	uint size = m_List.size();

	if (!size)
		return;

	int i = 0;
	int end = size;

	if (count > 0)
	{
		i = size - count;

		if (i < 0)
		{
			i = 0;
			end = size;
			count = size;
		}
		else
			end = i + count;
	}
	else
		count = size;

	g_TextDialog->AddText("Journal lines[" + QString::number(count) + "]:");

	for (; i < end; i++)
	{
		const CJournalMessage &item = m_List[i];
		QString text = "";
		text.sprintf("[s=0x%08X c=0x%04X]: %s", item.GetSerial(), item.GetColor(), item.GetText().toStdString().c_str());

		if (item.GetFlags() & JMT_SYSTEM)
			text = "sys " + text;
		else if (item.GetFlags() & JMT_MY)
			text = "my " + text;

		g_TextDialog->AddText(text);
	}

	if (g_TextDialog->isVisible())
		g_TextDialog->activateWindow();
	else
		g_TextDialog->show();
}
//----------------------------------------------------------------------------------
