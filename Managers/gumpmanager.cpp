// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** GumpManager.cpp
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "gumpmanager.h"
#include "../GameObjects/GameWorld.h"
#include "../OrionAssistant/textdialog.h"
#include "../OrionAssistant/Packets.h"
#include "../OrionAssistant/orionassistant.h"
#include "TextParser.h"
#include "../OrionAssistant/tabmacros.h"

CGumpManager g_GumpManager;
//----------------------------------------------------------------------------------
CGumpManager::CGumpManager()
{
}
//----------------------------------------------------------------------------------
CGumpManager::~CGumpManager()
{
}
//----------------------------------------------------------------------------------
int CGumpManager::GetGumpCount()
{
	OAFUN_DEBUG("c20_f1");
	return m_Items.size();
}
//----------------------------------------------------------------------------------
CGump *CGumpManager::GetLastGump()
{
	OAFUN_DEBUG("c20_f2");
	return m_LastGump;
}
//----------------------------------------------------------------------------------
CGump *CGumpManager::GetGump(const uint &index)
{
	OAFUN_DEBUG("c20_f3.1");
	if (index < (uint)m_Items.size())
		return m_Items[index];

	return nullptr;
}
//----------------------------------------------------------------------------------
CGump *CGumpManager::GetGump(const uint &serial, const uint &id)
{
	OAFUN_DEBUG("c20_f3");
	IFOR(i, 0, m_Items.size())
	{
		if (m_Items[i]->GetSerial() == serial && m_Items[i]->GetID() == id)
			return m_Items[i];
	}

	return nullptr;
}
//----------------------------------------------------------------------------------
void CGumpManager::RemoveLastGump()
{
	OAFUN_DEBUG("c20_f4");
	IFOR(i, 0, m_Items.size())
	{
		if (m_Items[i] == m_LastGump)
		{
			m_LastGump = nullptr;
			break;
		}
	}

	delete m_LastGump;
	m_LastGump = nullptr;
}
//----------------------------------------------------------------------------------
void CGumpManager::ClearGumps()
{
	OAFUN_DEBUG("c20_f5");
	IFOR(i, 0, m_Items.size())
		delete m_Items[i];

	m_Items.clear();
	m_LastGump = nullptr;
}
//----------------------------------------------------------------------------------
void CGumpManager::AddHook(CGumpHook *hook)
{
	OAFUN_DEBUG("c20_f6");
	m_Hooks.push_back(CGumpHook(hook));
}
//----------------------------------------------------------------------------------
void CGumpManager::ClearHooks()
{
	OAFUN_DEBUG("c20_f7");
	m_Hooks.clear();
}
//----------------------------------------------------------------------------------
bool CGumpManager::OnPacketReceived(CDataReader &reader)
{
	OAFUN_DEBUG("c20_f8");
	RemoveLastGump();

	if (g_World == nullptr)
		return true;

	uint serial = reader.ReadUInt32BE();
	uint id = reader.ReadUInt32BE();
	int x = reader.ReadInt32BE();
	int y = reader.ReadInt32BE();

	m_LastGump = new CGump(serial, id, x, y);
	m_Items.push_back(m_LastGump);

    ushort commandsLength = reader.ReadUInt16BE();
	string commands = reader.ReadString(commandsLength);
	CTextParser parser(commands, "", "", "{}");

	QStringList commandList = parser.ReadTokens();

	foreach (const QString &str, commandList)
	{
		QStringList list = str.split(" ", QString::SkipEmptyParts);

		if (!list.size())
			continue;

		QString cmd = list.at(0).trimmed().toLower();

		GUMP_ITEM_TYPE type = GIT_INVALID_TYPE;

		if (cmd == "nodispose")
		{
			m_LastGump->SetNoDispose(true);
			type = GIT_NODISPOSE;
		}
		else if (cmd == "nomove")
		{
			m_LastGump->SetNoMove(true);
			type = GIT_NOMOVE;
		}
		else if (cmd == "noclose")
		{
			m_LastGump->SetNoClose(true);
			type = GIT_NOCLOSE;
		}
		else if (cmd == "page")
			type = GIT_PAGE;
		else if (cmd == "group")
			type = GIT_GROUP;
		else if (cmd == "endgroup")
			type = GIT_GROUP;
		else if (cmd == "resizepic")
			type = GIT_RESIZEPIC;
		else if (cmd == "checkertrans")
			type = GIT_CHECKERTRANS;
		else if (cmd == "button")
			type = GIT_BUTTON;
		else if (cmd == "buttontileart")
			type = GIT_BUTTONTILEART;
		else if (cmd == "checkbox")
			type = GIT_CHECKBOX;
		else if (cmd == "radio")
			type = GIT_RADIO;
		else if (cmd == "text")
			type = GIT_TEXT;
		else if (cmd == "croppedtext")
			type = GIT_CROPPEDTEXT;
		else if (cmd == "textentry")
			type = GIT_TEXTENTRY;
		else if (cmd == "textentrylimited")
			type = GIT_TEXTENTRYLIMITED;
		else if (cmd == "tilepic")
			type = GIT_TILEPIC;
		else if (cmd == "tilepichue")
			type = GIT_TILEPICHUE;
		else if (cmd == "gumppic")
			type = GIT_GUMPPIC;
		else if (cmd == "gumppictiled")
			type = GIT_GUMPPICTILED;
		else if (cmd == "htmlgump")
			type = GIT_HTMLGUMP;
		else if (cmd == "xmfhtmlgump")
			type = GIT_XMFHTMLGUMP;
		else if (cmd == "xmfhtmlgumpcolor")
			type = GIT_XMLHTMLGUMPCOLOR;
		else if (cmd == "xmfhtmltok")
			type = GIT_XMLHTMLTOK;
		else if (cmd == "tooltip")
			type = GIT_TOOLTIP;
		else if (cmd == "mastergump")
			type = GIT_MASTERGUMP;

		if (type != GIT_INVALID_TYPE)
		{
			m_LastGump->m_Items.push_back(CGumpItem(type, str));
		}
	}

	int textLinesCount = reader.ReadInt16BE();

	IFOR(i, 0, textLinesCount)
	{
		int linelen = reader.ReadInt16BE();
		QString line = "";

		if (linelen)
			line = QString::fromStdWString(reader.ReadWString(linelen, true));

		m_LastGump->m_Text << line;
	}

	if (/*!m_Hooks.empty() &&*/ g_TabMacros->GetPlaying() && g_TabMacros->PlayWaiting(QList<MACRO_TYPE>() << MT_WAIT_FOR_GUMP))
		return false;

	if (!m_Hooks.empty())
	{
		if (SendGumpChoice(m_LastGump, m_Hooks.first()))
		{
			m_Hooks.pop_front();
			return false;
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
bool CGumpManager::OnPacketSend(CDataReader &reader)
{
	OAFUN_DEBUG("c20_f9");
	uint serial = reader.ReadUInt32BE();
	uint id = reader.ReadUInt32BE();

	/*//Invoke virture
	if (id == 0x000001CD && serial == g_PlayerSerial)
	{
	}*/

	IFOR(i, 0, m_Items.size())
	{
		CGump *gump = m_Items[i];

		if (!gump->GetReplayed() && gump->GetSerial() == serial && gump->GetID() == id)
		{
			uint button = reader.ReadUInt32BE();

			gump->SetReplayed(true);
			gump->SetReplyID(button);

			if (g_World != nullptr && g_TabMacros->GetRecording())
				g_TabMacros->AddAction(new CMacroWithGump(MT_WAIT_FOR_GUMP, serial, id, button), false);

			if (gump != m_LastGump)
				delete gump;

			m_Items.removeAt(i);

			break;
		}
	}

	return true;
}
//----------------------------------------------------------------------------------
bool CGumpManager::SendGumpChoice(CGump *gump, CGumpHook &hook)
{
	if (gump == nullptr || gump->GetReplayed())
		return false;

	int returnCode = -1;
	int hookIndex = hook.GetIndex();

	if (!hookIndex && !gump->GetNoClose())
		returnCode = 0;
	else if (!gump->m_Items.empty())
	{
		for (const CGumpItem &item : gump->m_Items)
		{
			if (item.GetType() == GIT_BUTTON || item.GetType() == GIT_BUTTONTILEART)
			{
				QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

				if (commands.size() >= 8 && commands[5].toInt())
				{
					int itemIndex = commands[7].toInt();

					if (hookIndex == itemIndex)
					{
						returnCode = itemIndex;

						g_OrionAssistant.ClientPrint("Gump choice successful.");
						break;
					}
				}
			}
		}
	}

	if (returnCode == -1)
		return false;

	if (hook.m_Checks.size() || hook.m_Entries.size())
	{
		int page = 0;
		int group = 0;

		for (CGumpItem &item : gump->m_Items)
		{
			if (item.GetType() == GIT_PAGE)
			{
				QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

				if (commands.size() >= 2)
					page = commands[1].toInt();
			}
			else if (item.GetType() == GIT_GROUP)
			{
				QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

				if (commands.size() >= 2)
					group = commands[1].toInt();
				else
					group = 0;
			}
			else if ((item.GetType() == GIT_CHECKBOX || item.GetType() == GIT_RADIO) && hook.m_Checks.size())
			{
				QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

				if (commands.size() >= 7)
				{
					for (int i = 0; i < hook.m_Checks.size(); i++)
					{
						CGumpHookCheck &checkHook = hook.m_Checks[i];

						if (commands[6].toInt() == checkHook.GetIndex())
						{
							if (item.GetType() == GIT_RADIO)
							{
								int usedPage = 0;
								int usedGroup = 0;

								for (CGumpItem &tempItem : gump->m_Items)
								{
									if (tempItem.GetType() == GIT_RADIO)
									{
										QStringList tempCommands = tempItem.GetCommand().split(" ", QString::SkipEmptyParts);

										if (tempCommands.size() >= 7 && ((usedPage <= 1 && page <= 1) || (usedPage == page)))
										{
											if (usedGroup == group)
											{
												tempCommands[5] = "0";

												tempItem.RestoreCommand(tempCommands);
											}
										}
									}
									else if (tempItem.GetType() == GIT_PAGE)
									{
										QStringList tempCommands = tempItem.GetCommand().split(" ", QString::SkipEmptyParts);

										if (tempCommands.size() >= 2)
											usedPage = tempCommands[1].toInt();
									}
									else if (tempItem.GetType() == GIT_GROUP)
									{
										QStringList tempCommands = tempItem.GetCommand().split(" ", QString::SkipEmptyParts);

										if (tempCommands.size() >= 2)
											usedGroup = tempCommands[1].toInt();
										else
											group = 0;
									}
								}
							}

							commands[5] = (checkHook.GetState() ? "1" : "0");
							item.RestoreCommand(commands);

							hook.m_Checks.removeAt(i);
							break;
						}
					}
				}
			}
			else if ((item.GetType() == GIT_TEXTENTRY || item.GetType() == GIT_TEXTENTRYLIMITED) && hook.m_Entries.size())
			{
				QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

				if (commands.size() >= 8)
				{
					int maxLength = 0;

					if (item.GetType() == GIT_TEXTENTRYLIMITED && commands.size() >= 9)
						maxLength = commands[8].toInt();

					for (int i = 0; i < hook.m_Entries.size(); i++)
					{
						CGumpHookEntry &entryHook = hook.m_Entries[i];

						if (commands[6].toInt() == entryHook.GetIndex())
						{
							uint textIndex = commands[7].toUInt();

							if (textIndex < (uint)gump->m_Text.size())
							{
								QString text = entryHook.GetText();

								if (maxLength && text.length() > maxLength)
									text.resize(maxLength);

								gump->m_Text[textIndex] = text;

								hook.m_Entries.removeAt(i);

								break;
							}
						}
					}
				}
			}
		}
	}

	CPacketGumpResponse(gump, returnCode).SendServer();
	CPacketCloseGenericGumpWithoutResponse(gump, true).SendClient();

	gump->SetReplayed(true);
	gump->SetReplyID(returnCode);

	return true;
}
//----------------------------------------------------------------------------------
void CGumpManager::InfoGump(const QString &gumpIndex)
{
	OAFUN_DEBUG("c20_f12");

	int index = -1;

	if (gumpIndex.toLower() != "lastgump" && gumpIndex.toLower() != "last")
	{
		bool ok = false;
		index = gumpIndex.toInt(&ok);

		if (!ok)
			index= -1;
	}

	CGump *gump = nullptr;

	if (index == -1)
		gump = m_LastGump;
	else if (index >= 0 && index < m_Items.size())
		gump = m_Items[index];

	if (gump == nullptr)
		return;

	g_TextDialog->ClearText();

	QString buf = "";
	buf.sprintf("Gump information:\nSerial=0x%08X\nID=0x%08X\nX=%i\nY=%i\n", gump->GetSerial(), gump->GetID(), gump->GetX(), gump->GetY());
	g_TextDialog->AddText(buf);

	if (gump->GetReplayed())
		g_TextDialog->AddText("Gump replayed with id: " + QString::number(gump->GetReplyID()) + "\n");

	g_TextDialog->AddText("Gump buttons:");
	foreach (const CGumpItem &item, gump->m_Items)
	{
		if (item.GetType() == GIT_BUTTON || item.GetType() == GIT_BUTTONTILEART)
		{
			QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

			if (commands.size() >= 8)
				g_TextDialog->AddText("[" + commands[7] + "]\t" + item.GetCommand());
		}
	}

	g_TextDialog->AddText("\nGump checks & radios:");
	foreach (const CGumpItem &item, gump->m_Items)
	{
		if (item.GetType() == GIT_CHECKBOX || item.GetType() == GIT_RADIO)
		{
			QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

			if (commands.size() >= 7)
				g_TextDialog->AddText("[" + commands[6] + "]\t" + item.GetCommand());
		}
	}

	g_TextDialog->AddText("\nGump entries:");
	foreach (const CGumpItem &item, gump->m_Items)
	{
		if (item.GetType() == GIT_TEXTENTRY || item.GetType() == GIT_TEXTENTRYLIMITED)
		{
			QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

			if (commands.size() >= 8)
				g_TextDialog->AddText("[" + commands[6] + "]\t" + item.GetCommand());
		}
	}

	g_TextDialog->AddText("\nGump all commands:");
	foreach (const CGumpItem &item, gump->m_Items)
	{
		g_TextDialog->AddText(item.GetCommand());

	}

	index = 0;
	g_TextDialog->AddText("\nGump text:");
	foreach (const QString &str, gump->m_Text)
	{
		g_TextDialog->AddText("[" + QString::number(index) + "]\t" + str);
		index++;
	}

	if (g_TextDialog->isVisible())
		g_TextDialog->activateWindow();
	else
		g_TextDialog->show();
}
//----------------------------------------------------------------------------------
