/***********************************************************************************
**
** UOMapManager.h
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef UOMAPMANAGER_H
#define UOMAPMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
enum UOAM_MESSAGES
{
	UOAMM_REGISTER					= WM_USER + 200,
	UOAMM_COUNT_RESOURCES			= WM_USER + 201,
	UOAMM_GET_COORDINATES			= WM_USER + 202,
	UOAMM_GET_SKILL					= WM_USER + 203,
	UOAMM_GET_STAT					= WM_USER + 204,
	UOAMM_SET_ACTIVE_MACRO			= WM_USER + 205,
	UOAMM_PLAY_MACRO				= WM_USER + 206,
	UOAMM_DISPLAY_TEXT				= WM_USER + 207,
	UOAMM_MULTI_INFO				= WM_USER + 208,
	UOAMM_ADD_COMMAND				= WM_USER + 209,
	UOAMM_GET_PLAYER_SERIAL			= WM_USER + 210,
	UOAMM_GET_SHARD_NAME			= WM_USER + 211,
	UOAMM_ADD_USER_TO_PARTY			= WM_USER + 212,
	UOAMM_GET_UO_WINDOW_HANDLE		= WM_USER + 213,
	UOAMM_GET_POISON_STATUS			= WM_USER + 214,
	UOAMM_SET_SKILL_LOCK			= WM_USER + 215,
	UOAMM_GET_ACCOUNT_ID			= WM_USER + 216,

	UOAMM_COUNT_RESOURCES_FINISHED	= WM_USER + 301,
	UOAMM_SPELL_WAS_ATTEMPTED		= WM_USER + 302,
	UOAMM_LOGON						= WM_USER + 303,
	UOAMM_GET_MAGERY_SKILL_VALUE	= WM_USER + 304,
	UOAMM_GET_INT_AND_MANA_VALUE	= WM_USER + 305,
	UOAMM_GET_SKILL_VALUE			= WM_USER + 306,
	UOAMM_MACRO_FINISHED			= WM_USER + 307,
	UOAMM_LOGOUT					= WM_USER + 308,
	UOAMM_GET_HP_AND_MAXHP_VALUE	= WM_USER + 309,
	UOAMM_GET_DEX_AND_STAM_VALUE	= WM_USER + 310,
	UOAMM_ADD_MULTI					= WM_USER + 311,
	UOAMM_DELETE_MULTI				= WM_USER + 312,
	UOAMM_FACET_OR_LAND_INFO		= WM_USER + 313,
	UOAMM_POWER_HOUR_ENTERED		= WM_USER + 314
};
//----------------------------------------------------------------------------------
struct UOAnotifyCommandS
{
	HWND Handle;
	UINT Message;
	string Command;
};
//----------------------------------------------------------------------------------
class CUOMapManager
{
private:
	HWND m_Handle{ 0 };
	QList<HWND> m_UOAMHandles;

	void PostMessageSend(const UINT &message, const WPARAM &wParam, const LPARAM &lParam);

public:
	CUOMapManager();
	~CUOMapManager();

	QList<UOAnotifyCommandS> UOAnotifyCommand;

	void UOAMnotifyLand();
	void UOAMnotifyHouse(const ushort &x, const ushort &y, const ushort &type, const bool &destroyed = false);
	bool UOAnotifyCmd(const char *command);
	void UOAMEnterWorld();
	void UOAMnotifyTrack(const bool &enabled, const int &x, const int &y);
	void UOAMhousing();
	void UpdateHits();
	void UpdateMana();
	void UpdateStam();

	LRESULT CALLBACK MessageQueue(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
};
//----------------------------------------------------------------------------------
extern CUOMapManager g_UOMapManager;
//----------------------------------------------------------------------------------
#endif
//----------------------------------------------------------------------------------
