/***********************************************************************************
**
** MenuManager.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef MENUMANAGER_H
#define MENUMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../Managers/DataReader.h"
#include "../CommonItems/menu.h"
#include <QVector>
//----------------------------------------------------------------------------------
class CMenuHook
{
	SETGET(QString, Name, "")
	SETGET(QString, ItemName, "")

public:
	CMenuHook(const QString &name, const QString &itemName) : m_Name(name), m_ItemName(itemName) {}
	~CMenuHook() {}
};
//----------------------------------------------------------------------------------
class CMenuManager
{
private:
	CMenu *m_LastMenu{ nullptr };

	QList<CMenuHook> m_Hooks;

	QList<CMenu*> m_Items;

	void RemoveLastMenu();

public:
	CMenuManager();
	virtual ~CMenuManager();

	int GetMenuCount();

	CMenu *GetLastMenu();

	CMenu *GetMenu(const QString &name);

	void ClearMenus();

	void SetHook(const QString &name, const QString &itemName);

	void ClearHooks();

	bool OnPacketReceived(CDataReader &reader);

	bool OnPacketSend(CDataReader &reader);

	bool SendChoiceIfMenuExists(const QString &name, const QString &itemName, const bool &sendPacket);

	bool SendMenuChoice(CMenu *menu, const QString &itemName, const bool &sendPacket);

	void InfoMenu(const QString &menuIndex);
};
//----------------------------------------------------------------------------------
extern CMenuManager g_MenuManager;
//----------------------------------------------------------------------------------
#endif // MENUMANAGER_H
//----------------------------------------------------------------------------------
