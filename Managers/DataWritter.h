//----------------------------------------------------------------------------------
#ifndef DATAWRITTER_H
#define DATAWRITTER_H
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CDataWritter : public QObject
{
	Q_OBJECT

	SETGET(bool, AutoResize, false)
	SETGET(puchar, Ptr, nullptr)

protected:
	UCHAR_LIST m_Data;

public:
	CDataWritter();
	CDataWritter(const int &size, const bool &autoResize = true);

	virtual ~CDataWritter();

	UCHAR_LIST Data() const { return m_Data; }
	int Size() { return (int)m_Data.size(); }

	void Resize(const int &newSize, const bool &resetPtr = false);
	void ResetPtr() { m_Ptr = &m_Data[0]; }

	void Move(const int &offset);

	void WriteDataBE(const puchar data, const int &size, const int offset = 0);
	void WriteDataLE(const puchar data, const int &size, const int offset = 0);

	void WriteUInt8(const uchar &val, const int &offset = 0) { WriteDataBE((puchar)&val, sizeof(uchar), offset); }

	void WriteUInt16BE(const ushort &val, const int &offset = 0) { WriteDataBE((puchar)&val, sizeof(ushort), offset); }
	void WriteUInt16LE(const ushort &val, const int &offset = 0) { WriteDataLE((puchar)&val, sizeof(ushort), offset); }

	void WriteUInt32BE(const uint &val, const int &offset = 0) { WriteDataBE((puchar)&val, sizeof(uint), offset); }
	void WriteUInt32LE(const uint &val, const int &offset = 0) { WriteDataLE((puchar)&val, sizeof(uint), offset); }

	void WriteUInt64BE(const uint64 &val, const int &offset = 0) { WriteDataBE((puchar)&val, sizeof(uint64), offset); }
	void WriteUInt64LE(const uint64 &val, const int &offset = 0) { WriteDataLE((puchar)&val, sizeof(uint64), offset); }

	void WriteInt8(const char &val, const int &offset = 0) { WriteDataBE((puchar)&val, sizeof(char), offset); }

	void WriteInt16BE(const short &val, const int &offset = 0) { WriteDataBE((puchar)&val, sizeof(short), offset); }
	void WriteInt16LE(const short &val, const int &offset = 0) { WriteDataLE((puchar)&val, sizeof(short), offset); }

	void WriteInt32BE(const int &val, const int &offset = 0) { WriteDataBE((puchar)&val, sizeof(int), offset); }
	void WriteInt32LE(const int &val, const int &offset = 0) { WriteDataLE((puchar)&val, sizeof(int), offset); }

	void WriteInt64BE(const __int64 &val, const int &offset = 0) { WriteDataBE((puchar)&val, sizeof(__int64), offset); }
	void WriteInt64LE(const __int64 &val, const int &offset = 0) { WriteDataLE((puchar)&val, sizeof(__int64), offset); }

	void WriteFloatBE(const float &val, const int &offset = 0) { WriteDataBE((puchar)&val, sizeof(float), offset); }
	void WriteFloatLE(const float &val, const int &offset = 0) { WriteDataLE((puchar)&val, sizeof(float), offset); }

	void WriteDoubleBE(const double &val, const int &offset = 0) { WriteDataBE((puchar)&val, sizeof(double), offset); }
	void WriteDoubleLE(const double &val, const int &offset = 0) { WriteDataLE((puchar)&val, sizeof(double), offset); }

	void WriteString(const string &val, int length = 0, const bool &nullTerminated = true, const int &offset = 0);
	void WriteWString(const wstring &val, int length = 0, const bool &bigEndian = true, const bool &nullTerminated = true, const int &offset = 0);
};
//----------------------------------------------------------------------------------
#endif

