// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** SkillManager.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "skillmanager.h"
#include "../OrionAssistant/orionassistant.h"
#include "../OrionAssistant/Packets.h"

CSkillManager g_SkillManager;
//----------------------------------------------------------------------------------
CSkillManager::CSkillManager()
{
}
//----------------------------------------------------------------------------------
CSkillManager::~CSkillManager()
{
}
//----------------------------------------------------------------------------------
void CSkillManager::Insert(const QString &name, const bool &usable, const int &index)
{
	OAFUN_DEBUG("c21_f1");
	m_AllSkills.insert(name, index);
	m_AllSkills.insert(QString::number(index), index);

	if (usable)
	{
		m_Skills.insert(name, index);
		m_Skills.insert(QString::number(index), index);
	}
}
//----------------------------------------------------------------------------------
int CSkillManager::GetIndex(const QString &name)
{
	OAFUN_DEBUG("c21_f2");
	SKILLS_MAP::iterator it = m_AllSkills.find(name.toLower());

	if (it != m_AllSkills.end())
		return it.value();

	return -1;
}
//----------------------------------------------------------------------------------
int CSkillManager::Find(const QString &name)
{
	OAFUN_DEBUG("c21_f3");
	if (name == "last" || name == "lastskill")
		return g_LastSkillIndex;

	SKILLS_MAP::iterator it = m_Skills.find(name);

	if (it != m_Skills.end())
		return it.value();

	return -1;
}
//----------------------------------------------------------------------------------
void CSkillManager::Use(const QString &name)
{
	OAFUN_DEBUG("c21_f4");
	int index = Find(name.toLower());

	if (index != -1)
		CPacketUseSkillRequest(index).SendClient();
	else
		g_OrionAssistant.ClientPrint("Skill '" + name + "' is not found.");
}
//----------------------------------------------------------------------------------
void CSkillManager::TargetUse(const QString &name, const uint &serial)
{
	OAFUN_DEBUG("c21_f4");
	int index = Find(name.toLower());

	if (index != -1)
		CPacketTargetUseSkill(index + 1, serial).SendServer();
	else
		g_OrionAssistant.ClientPrint("Skill '" + name + "' is not found.");
}
//----------------------------------------------------------------------------------
