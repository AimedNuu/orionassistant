/***********************************************************************************
**
** DisplayManager.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef DISPLAYMANAGER_H
#define DISPLAYMANAGER_H
//----------------------------------------------------------------------------------
#include <QVector>
#include "../orionassistant_global.h"
#include "../CommonItems/displayitem.h"
#include <QMap>
//----------------------------------------------------------------------------------
enum REDRAW_DISPLAY_MODE
{
	RDM_DISABLED = 0,
	RDM_TEXTUAL,
	RDM_COLORED_TEXTUAL,
	RDM_COLORED_GRAPHICAL
};
//----------------------------------------------------------------------------------
enum DISPLAY_KEY_IDENTIFIER
{
	DKI_NONE = 0,
	DKI_TEXT,
	DKI_ID,
	DKI_HUE,
	DKI_COLOR_MIN,
	DKI_COLOR_MID,
	DKI_COLOR,
	DKI_BACKGROUND,
	DKI_BG_COLOR_MIN,
	DKI_BG_COLOR_MID,
	DKI_BG_COLOR,
	DKI_MIN_VAULE,
	DKI_MID_VALUE,
	DKI_PERCENTS,
	DKI_COUNT
};
//----------------------------------------------------------------------------------
class CDisplayIconData
{
public:
	CDisplayIconData() {}
	~CDisplayIconData();

	HBITMAP Bitmap{nullptr};
};
//----------------------------------------------------------------------------------
struct DISPLAY_KEY_ITEM
{
	QString Name;
	DISPLAY_KEY_IDENTIFIER Identifier;
};
//----------------------------------------------------------------------------------
typedef QVector<CDisplayInfo> DISPLAY_INFO_LIST;
typedef QVector<CDisplayItem> DISPLAY_LIST;
typedef QMap<uint, CDisplayIconData*> DISPLAY_ICON_MAP;
//----------------------------------------------------------------------------------
class CDisplayManager
{
private:
	DISPLAY_INFO_LIST m_InfoCommands;

	DISPLAY_LIST m_Commands;

	DISPLAY_ICON_MAP m_Icons;

	static const CDisplayItem m_DefaultItems[26];

	static const DISPLAY_KEY_ITEM m_KeysList[DKI_COUNT];

	CDisplayIconData *GetIcon(const CDisplayItem &item);

	void LoadDisplayIcon(CDisplayIconData *item, ushort graphic, const ushort &color, const size_t &address);

	void ApplyAttributeToDisplayItem(CDisplayItem &obj, const QString &name, const QString &value);

	void CompilleData(const QString &text, const uint &textColor);

	void GetValues(const CDisplayItem &item, QString &value, QString &maxValue, uint &textColor, uint &backgroundColor, const bool &useNotoriety);

public:
	CDisplayManager();
	~CDisplayManager();

	void ClearIcons();

	const DISPLAY_INFO_LIST &GetCommands() const { return m_InfoCommands; }

	CDisplayItem GetDisplayItemByName(const QString &name);

	void Redraw(const REDRAW_DISPLAY_MODE &mode, const bool &useNotoriety, const bool &hightFont, bool highlightReagents, const bool &displayAppIcon, const uint &backgroundColor);

	void RecompilleData(const QString &text, const uint &textColor);

	bool CanRedraw(const ushort &graphic, const ushort &color);
};
//----------------------------------------------------------------------------------
extern CDisplayManager g_DisplayManager;
//----------------------------------------------------------------------------------
#endif // DISPLAYMANAGER_H
//----------------------------------------------------------------------------------
