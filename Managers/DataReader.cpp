// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
//----------------------------------------------------------------------------------
#include "DataReader.h"
//----------------------------------------------------------------------------------
CDataReader::CDataReader()
{
}
//----------------------------------------------------------------------------------
CDataReader::CDataReader(puchar start, const int &size)
: m_Start(start), m_Size(size), m_End(m_Start + size)
{
	m_Ptr = m_Start;
}
//----------------------------------------------------------------------------------
CDataReader::~CDataReader()
{
	m_Start = nullptr;
	m_Size = 0;
	m_End = nullptr;
	m_Ptr = nullptr;
}
//----------------------------------------------------------------------------------
void CDataReader::SetData(puchar start, const int &size, const int &offset)
{
	m_Start = start;
	m_Size = size;
	m_End = m_Start + size;
	m_Ptr = m_Start + offset;
}
//----------------------------------------------------------------------------------
void CDataReader::ReadDataBE(puchar data, const int &size, const int offset)
{
	if (m_Ptr != nullptr)
	{
		puchar ptr = m_Ptr + offset + size - 1;

		if (ptr >= m_Start && ptr <= m_End)
		{
			for (int i = 0; i < size; i++)
				data[i] = *(ptr - i);

			m_Ptr += size;
		}
	}
}
//----------------------------------------------------------------------------------
void CDataReader::ReadDataLE(puchar data, const int &size, const int offset)
{
	if (m_Ptr != nullptr)
	{
		puchar ptr = m_Ptr + offset;

		if (ptr >= m_Start && ptr + size <= m_End)
		{
			for (int i = 0; i < size; i++)
				data[i] = ptr[i];

			m_Ptr += size;
		}
	}
}
//----------------------------------------------------------------------------------
string CDataReader::ReadString(int size, const int &offset)
{
	puchar ptr = m_Ptr + offset;

	if (!size)
	{
		if (ptr >= m_Start && ptr <= m_End)
		{
			puchar buf = ptr;

			while (buf <= m_End && *buf)
				buf++;

			size = (buf - ptr) + 1;
		}
	}

	string result = "";

	if (ptr >= m_Start && ptr + size <= m_End)
	{
		result.resize(size, 0);
		ReadDataLE((puchar)&result[0], size, offset);
	}

	return result.c_str();
}
//----------------------------------------------------------------------------------
wstring CDataReader::ReadWString(int size, const bool &bigEndian, const int &offset)
{
	puchar ptr = m_Ptr + offset;

	if (!size)
	{
		if (ptr >= m_Start && ptr <= m_End)
		{
			puchar buf = ptr;

			while (buf <= m_End)
			{
				ushort val = (bigEndian ? ((buf[0] << 8) | buf[1]) : *(pushort)buf);

				buf += 2;

				if (!val)
					break;
			}

			size = ((buf - ptr) / 2);
		}
	}

	wstring result = L"";

	if (ptr >= m_Start && ptr + size <= m_End)
	{
		result.resize(size, 0);

		if (bigEndian)
		{
			for (int i = 0; i < size; i++)
				result[i] = ReadInt16BE(offset);
		}
		else
		{
			for (int i = 0; i < size; i++)
				result[i] = ReadInt16LE(offset);
		}
	}

	return result.c_str();
}
//----------------------------------------------------------------------------------
