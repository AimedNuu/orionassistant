﻿/***********************************************************************************
**
** ClilocManager.h
**
** Copyright (C) June 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef CLILOCMANAGER_H
#define CLILOCMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "MappedFile.h"
//----------------------------------------------------------------------------------
typedef QMap<uint, QString> CLILOC_MAP;
//----------------------------------------------------------------------------------
//!Класс менеджера клилоков
class CClilocManager
{
private:
	//!Системные клилоки (id < 1000000)
	CLILOC_MAP m_ClilocSystem;

	//!Обычные клилоки (id >= 1000000 && id < 3000000)
	CLILOC_MAP m_ClilocRegular;

	//!Клилоки для помощи (id >= 3000000)
	CLILOC_MAP m_ClilocSupport;

	QString Load(uint &id);

	inline QString CamelCaseTest(const bool &toCamelCase, QString result);

public:
	CClilocManager();
	virtual ~CClilocManager();

	CDataReader m_ClilocEnu;

	QString Get(const uint &id, const bool &toCamelCase = false, QString result = "");

	QString ParseArgumentsToClilocString(const uint &cliloc, const bool &toCamelCase, wstring args);
};
//----------------------------------------------------------------------------------
//!Ссылка на менеджер клилоков
extern CClilocManager g_ClilocManager;
//----------------------------------------------------------------------------------
#endif
//----------------------------------------------------------------------------------
