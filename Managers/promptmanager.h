/***********************************************************************************
**
** PromptManager.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef PROMPTMANAGER_H
#define PROMPTMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../CommonItems/prompt.h"
#include "../Managers/DataReader.h"
//----------------------------------------------------------------------------------
typedef QVector<CPrompt> PROMPT_LIST;
//----------------------------------------------------------------------------------
class CPromptManager
{
	SETGET(bool, IsUnicode, false)
	SETGET(bool, Exists, false)
	SETGET(uint, Serial, 0)
	SETGET(uint, ID, 0)

private:
	PROMPT_LIST m_Hooks;

public:
	CPromptManager();

	void AddHook(const CPrompt &prompt);

	void ClearHooks();

	bool OnPacketReceived(CDataReader &reader);

	bool OnPacketSend(CDataReader &reader);

	void Send(QString text);
};
//----------------------------------------------------------------------------------
extern CPromptManager g_PromptManager;
//----------------------------------------------------------------------------------
#endif // PROMPTMANAGER_H
//----------------------------------------------------------------------------------
