// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** CommandManager.cpp
**
** Copyright (C) January 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "commandmanager.h"
#include "../GameObjects/gameworld.h"
#include "../GameObjects/GamePlayer.h"
#include "../OrionAssistant/orionassistant.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../Managers/TextParser.h"
#include "journalmanager.h"
#include "../OrionAssistant/Target.h"
#include "../OrionAssistant/textdialog.h"
#include "../OrionAssistant/Packets.h"
#include "menumanager.h"
#include "spellmanager.h"
#include "skillmanager.h"
#include "../CommonItems/ignoreitem.h"
#include "../OrionAssistant/scripteditordialog.h"
#include <QDir>
#include "buffmanager.h"
#include "trademanager.h"
#include "gumpmanager.h"
#include "filemanager.h"
#include "promptmanager.h"
#include "../OrionAssistant/tabmain.h"
#include "../OrionAssistant/tabliststypes.h"
#include "../OrionAssistant/tablistsobjects.h"
#include "../OrionAssistant/tablistsfind.h"
#include "../OrionAssistant/tablistsignore.h"
#include "../OrionAssistant/tablistsfriends.h"
#include "../OrionAssistant/tablistsenemies.h"
#include "../OrionAssistant/tabscripts.h"
#include "../OrionAssistant/tabagentsdress.h"
#include "../OrionAssistant/objectinspectorform.h"
#include "../OrionAssistant/tabagentsshop.h"
#include "ObjectPropertiesManager.h"
#include "contextmenumanager.h"
#include "../OrionAssistant/tabhotkeys.h"
#include "../OrionAssistant/hotkeysform.h"

CCommandManager g_CommandManager;

#define CONNECT_SCRIPT_COMMAND_DIRECT(name, ...) \
	connect(this, SIGNAL(Command ##name ( __VA_ARGS__ )), \
			this, SLOT(OnCommand ##name ( __VA_ARGS__ )), \
			Qt::DirectConnection)
#define CONNECT_SCRIPT_COMMAND(name, ...) \
	connect(this, SIGNAL(Command ##name ( __VA_ARGS__ )), \
			this, SLOT(OnCommand ##name ( __VA_ARGS__ )))
//----------------------------------------------------------------------------------
CCommandManager::CCommandManager()
: QObject(nullptr)
{
	OAFUN_DEBUG("c26_f1");
	m_Time.start();

	qRegisterMetaType<CJournalMessage>("CJournalMessage");
	qRegisterMetaType<CJournalMessage>("CMenu");
	qRegisterMetaType<CJournalMessage>("CGumpHook");

	CONNECT_SCRIPT_COMMAND_DIRECT(Wait, QString, int*);
	CONNECT_SCRIPT_COMMAND(Info, const QString&);
	CONNECT_SCRIPT_COMMAND(InfoTile, const QString&);
	CONNECT_SCRIPT_COMMAND(InfoMenu, const QString&);
	CONNECT_SCRIPT_COMMAND(SaveConfig);
	CONNECT_SCRIPT_COMMAND_DIRECT(Click, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(UseObject, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetStatus, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Attack, const QString&);
	CONNECT_SCRIPT_COMMAND(TextWindowOpen);
	CONNECT_SCRIPT_COMMAND(TextWindowClose);
	CONNECT_SCRIPT_COMMAND(TextWindowClear);
	CONNECT_SCRIPT_COMMAND(TextWindowPrint, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetLight, const bool&, int);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetWeather, const bool&, const int&, const int&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetSeason, const bool&, const int&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Track, const bool&, const int&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Cast, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(UseSkill, const QString&);
	CONNECT_SCRIPT_COMMAND(HelpGump);
	CONNECT_SCRIPT_COMMAND(CloseUO);
	CONNECT_SCRIPT_COMMAND_DIRECT(Warmode, const uint&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Morph, ushort);
	CONNECT_SCRIPT_COMMAND(Resend);
	CONNECT_SCRIPT_COMMAND_DIRECT(Sound, const ushort&);
	CONNECT_SCRIPT_COMMAND_DIRECT(EmoteAction, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Hide, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(BlockMoving, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(UseType, const QStringList&, const QStringList&, const QString&, const bool&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(UseFromGround, const QStringList&, const QStringList&, const QString&, const QString&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(UseTypeList, const QString&, const QString&, const bool&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(UseFromGroundList, const QString&, const QString&, const QString&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(Print, const ushort&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(CharPrint, const QString&, const ushort&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Say, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(RenameMount, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Ignore, const QString&, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(IgnoreReset);
	CONNECT_SCRIPT_COMMAND_DIRECT(Drop, const QString&, const int&, const int&, const int&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(DropHere, const QString&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(MoveItem, const QString&, const int&, const QString&, const int&, const int&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ShowJournal, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClearJournal, const QString&, const QString&, const ushort&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(JournalIgnoreCase, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetDressBag, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(UnsetDressBag);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetArm, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(UnsetArm, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetDress, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(UnsetDress, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Arm, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Disarm);
	CONNECT_SCRIPT_COMMAND_DIRECT(Dress, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Undress);
	CONNECT_SCRIPT_COMMAND_DIRECT(Unequip, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Equip, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(EquipT, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Terminate, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(WaitTargetObject, const QStringList&);
	CONNECT_SCRIPT_COMMAND_DIRECT(WaitTargetTile, const QString&, const int&, const int&, const int&, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(CancelWaitTarget);
	CONNECT_SCRIPT_COMMAND_DIRECT(WaitMenu, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(CancelWaitMenu);
	CONNECT_SCRIPT_COMMAND_DIRECT(TargetObject, const QStringList&);
	CONNECT_SCRIPT_COMMAND_DIRECT(TargetTile, const QString&, const int&, const int&, const int&, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(AddType, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(RemoveType, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(AddObject, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(RemoveObject, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(LoadScript, const QString&);
	CONNECT_SCRIPT_COMMAND(Exec, const QString&, const bool&, const QStringList&);
	CONNECT_SCRIPT_COMMAND_DIRECT(SaveHotkeys, const QString&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(LoadHotkeys, const QString&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(SkillValue, const QString&, const QString&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(InJournal, const QString&, const QString&, const ushort&, const QString&, const uint&, const uint&, CJournalMessage**);
	CONNECT_SCRIPT_COMMAND_DIRECT(FindType, const QStringList&, const QStringList&, const QString&, const QString&, const QString&, const QString&, const bool&, QStringList*);
	CONNECT_SCRIPT_COMMAND_DIRECT(CountType, const QStringList&, const QStringList&, const QString&, const QString&, const bool&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(FindList, const QString&, const QString&, const QString&, const QString&, const QString&, const bool&, QStringList*);
	CONNECT_SCRIPT_COMMAND_DIRECT(HaveTarget, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ScriptRunning, const QString&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetDistance, const QString&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetDistance, const int&, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetUsedIgnoreList, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjAtLayer, const QString&, const QString&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(FindObject, const QString&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(CanWalk, uchar, int, int, char, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(Step, const int&, const bool&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(WalkTo, const int&, const int&, const int&, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(StopWalking);
	CONNECT_SCRIPT_COMMAND_DIRECT(IsWalking, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionSound, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionSound, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionSoundVolume, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionSoundVolume, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionMusic, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionMusic, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionMusicVolume, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionMusicVolume, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionUseTooltips, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionUseTooltips, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionAlwaysRun, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionAlwaysRun, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionNewTargetSystem, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionNewTargetSystem, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionObjectHandles, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionObjectHandles, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionScaleSpeech, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionScaleSpeech, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionScaleSpeechDelay, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionScaleSpeechDelay, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionIgnoreGuildMessages, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionIgnoreGuildMessages, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionIgnoreAllianceMessages, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionIgnoreAllianceMessages, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionDarkNights, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionDarkNights, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionColoredLighting, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionColoredLighting, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionCriminalActionsQuery, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionCriminalActionsQuery, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionCircleOfTransparency, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionCircleOfTransparency, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionCircleOfTransparencyValue, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionCircleOfTransparencyValue, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionLockResizingGameWindow, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionLockResizingGameWindow, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionFPSValue, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionFPSValue, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionUseScalingGameWindow, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionUseScalingGameWindow, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionDrawStatusState, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionDrawStatusState, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionDrawStumps, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionDrawStumps, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionMarkingCaves, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionMarkingCaves, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionNoVegetation, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionNoVegetation, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionNoFieldsAnimation, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionNoFieldsAnimation, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionStandardCharactesFrameRate, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionStandardCharactesFrameRate, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionStandardItemsFrameRate, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionStandardItemsFrameRate, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionLockGumpsMoving, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionLockGumpsMoving, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionEnterChat, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionEnterChat, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionHiddenCharacters, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionHiddenCharacters, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionHiddenCharactersAlpha, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionHiddenCharactersAlpha, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionHiddenCharactersModeOnlyForSelf, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionHiddenCharactersModeOnlyForSelf, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionTransparentSpellIcons, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionTransparentSpellIcons, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionSpellIconsAlpha, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionSpellIconsAlpha, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionFastRotation, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OptionFastRotation, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetGraphic, const int&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetColor, const int&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetX, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetY, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetZ, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetContainer, const int&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetMap, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetCount, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetFlags, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetName, const int&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetMobile, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetIgnored, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetFrozen, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetPoisoned,const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetFlying, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetYellowHits, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetIgnoreCharacters, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetLocked, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetWarMode, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetHidden, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetIsHuman, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetIsPlayer, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetIsCorpse, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetLayer, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetIsMulti, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetEquipLayer, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetHits, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetMaxHits, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetMana, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetMaxMana, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetStam, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetMaxStam, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetFemale, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetRace, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetDirection, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetNotoriety, const int&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetCanChangeName, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetDead, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetExists, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetStr, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetInt, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetDex, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetLockStrState, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetLockIntState, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetLockDexState, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetWeight, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetMaxWeight, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetArmor, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetGold, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetStatsCap, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetFollowers, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetMaxFollowers, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetFireResistance, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetColdResistance, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetPoisonResistance, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetEnergyResistance, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetLuck, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetMinDamage, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetMaxDamage, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetTithingPoints, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetStealthSteps, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PlayerGetParalyzed, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClientLastTarget, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClientLastTarget, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClientLastAttack,QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClientLastAttack, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(TargetSystemSerial, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(TargetSystemSerial, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetSerial, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(AddFindList, const QString&, const QString&, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClearFindList, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(AddIgnoreList, const QString&, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(AddIgnoreList, const QString&, const QString&, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClearIgnoreList, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(MenuCount, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetMenu, const QString&, CMenu**);
	CONNECT_SCRIPT_COMMAND_DIRECT(SelectMenu, const QString&, const QString&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(CloseMenu, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(BuffExists, const QString&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(TradeCount, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(TradeContainer, const QString&, const int&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(TradeOpponent, const QString&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(TradeName, const QString&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(TradeCheckState, const QString&, const int&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(TradeCheck, const QString&, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(TradeClose, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetGraphic, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetContainer, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(FindFriend, const QString&, const QString&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(FindEnemy, const QString&, const QString&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetFriendList, QStringList*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetEnemyList, QStringList*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetFriendsStatus);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetEnemiesStatus);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetFontColor, const bool&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetFontColor, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetFontColorValue, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetCharactersFontColor, const bool&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetCharactersFontColor, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetCharactersFontColorValue, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(AddFriend, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(RemoveFriend, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClearFriendList);
	CONNECT_SCRIPT_COMMAND_DIRECT(AddEnemy, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(RemoveEnemy, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClearEnemyList);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetGlobal, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetGlobal, const QString&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClearGlobals);
	CONNECT_SCRIPT_COMMAND(InfoGump, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ValidateTargetTile, const QString&, int, int, const bool&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(UseAbility, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(LastJournalMessage, CJournalMessage**);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetLastTargetPosition, int*, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetLastAttackPosition, int*, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(UseWrestlingDisarm);
	CONNECT_SCRIPT_COMMAND_DIRECT(UseWrestlingStun);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetExists, const QString&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(InvokeVirture, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(JournalCount, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(JournalLine, const int&, CJournalMessage**);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetJournalLine, const CJournalMessage*, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(WaitGump, CGumpHook*);
	CONNECT_SCRIPT_COMMAND_DIRECT(CancelWaitGump);
	CONNECT_SCRIPT_COMMAND_DIRECT(SelectGump, CGump*, CGumpHook*, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GumpCount, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetGump, const int&, CGump**);
	CONNECT_SCRIPT_COMMAND_DIRECT(GetGump, const int&, const int&, CGump**);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetProperties, const uint&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetProfileReceived, const uint&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetProfile, const uint&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(RequestProfile, const uint&);
	CONNECT_SCRIPT_COMMAND_DIRECT(WaitPrompt, const QString&, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(CancelWaitPrompt);
	CONNECT_SCRIPT_COMMAND_DIRECT(ObjectGetTitle, const uint&, QString*);
	CONNECT_SCRIPT_COMMAND_DIRECT(Shop, const QString&, const bool&, const QString&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(IsShopping, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ShowStatusbar, const QString&, const int&, const int&, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(CloseStatusbar, const QString&);
	CONNECT_SCRIPT_COMMAND(LogOut);
	CONNECT_SCRIPT_COMMAND(TextWindowSetPos, const int&, const int&);
	CONNECT_SCRIPT_COMMAND(TextWindowSetSize, const int&, const int&);
	CONNECT_SCRIPT_COMMAND(PlayClientMacro, const QStringList&, const QStringList&);
	CONNECT_SCRIPT_COMMAND_DIRECT(TimerExists, const QString&, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(SetTimer, const QString&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(Timer, const QString&, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(RemoveTimer, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClearTimers);
	CONNECT_SCRIPT_COMMAND_DIRECT(CancelTarget);
	CONNECT_SCRIPT_COMMAND_DIRECT(PromptExists, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PromptSerial, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(PromptID, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(SendPrompt, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OpenPaperdoll, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClosePaperdoll, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(MovePaperdoll, const QString&, const int&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(RequestContextMenu, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(WaitContextMenu, const QString&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(CancelContextMenu);
	CONNECT_SCRIPT_COMMAND_DIRECT(BandageTarget, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(CastTarget, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(UseSkillTarget, const QString&, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClientViewRange, int*);
	CONNECT_SCRIPT_COMMAND_DIRECT(ClientViewRange, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OpenOrionMap);
	CONNECT_SCRIPT_COMMAND_DIRECT(MoveOrionMap, const int&, const int&);
	CONNECT_SCRIPT_COMMAND_DIRECT(CloseOrionMap);
	CONNECT_SCRIPT_COMMAND_DIRECT(LoadProfile, const QString&);
	CONNECT_SCRIPT_COMMAND_DIRECT(OnOffHotkeys, bool*);
	CONNECT_SCRIPT_COMMAND_DIRECT(OnOffHotkeys, const bool&);
	CONNECT_SCRIPT_COMMAND_DIRECT(TextWindowSaveToFile, const QString&);
}
//----------------------------------------------------------------------------------
CCommandManager::~CCommandManager()
{
}
//----------------------------------------------------------------------------------
uchar CCommandManager::GetJournalFlags(const QString &text)
{
	OAFUN_DEBUG("c26_f2");
	uchar flags = 0;

	CTextParser parser(text.toStdString(), "|", "", "");

	QStringList list = parser.ReadTokens();

	foreach (const QString &str, list)
	{
		if (str == "my" || str == "self")
			flags |= JMT_MY;
		else if (str == "sys" || str == "system")
			flags |= JMT_SYSTEM;
	}

	return flags;
}
//----------------------------------------------------------------------------------
ushort CCommandManager::GetFindFlags(const QString &text)
{
	OAFUN_DEBUG("c26_f3");
	ushort flags = FTF_NORMAL;
	ushort flagsType = FTF_NORMAL;

	CTextParser parser(text.toStdString(), "|", "", "");

	QStringList list = parser.ReadTokens();

	foreach (const QString &str, list)
	{
		if (str == "fast")
			flags = FTF_FAST;
		else if (str == "near")
			flags = FTF_NEAREST;
		else if (str == "mobile")
			flagsType |= FTF_MOBILE;
		else if (str == "item")
			flagsType = FTF_ITEM;
		else if (str == "human")
			flagsType |= (FTF_HUMAN | FTF_MOBILE);
		else if (str == "live")
			flagsType |= (FTF_LIVE | FTF_MOBILE);
		else if (str == "dead")
			flagsType |= (FTF_DEAD | FTF_MOBILE);
		else if (str == "injured")
			flagsType |= (FTF_INJURED | FTF_MOBILE);
		else if (str == "next")
			flagsType |= (FTF_NEXT | FTF_FAST);
		else if (str == "ignorefriends")
			flagsType |= FTF_IGNORE_FRIENDS;
		else if (str == "ignoreenemies")
			flagsType |= FTF_IGNORE_ENEMIES;
	}

	return flags | flagsType;
}
//----------------------------------------------------------------------------------
uchar CCommandManager::GetSkillsFlags(const QString &text)
{
	OAFUN_DEBUG("c26_f4");
	uchar flags = SF_REAL;

	CTextParser parser(text.toStdString(), "|", "", "");

	QStringList list = parser.ReadTokens();

	foreach (const QString &str, list)
	{
		if (str == "base")
			flags = SF_BASE;
		else if (str == "cap")
			flags = SF_CAP;
		else if (str == "lock")
			flags = SF_LOCK;
	}

	return flags;
}
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandWait(QString text, int *delay)
{
	OAFUN_DEBUG("c26_f5");
	text = text.toLower().trimmed();

	if (!text.length())
		text = "1";

	*delay = g_OrionAssistantForm->DelayFromText(text);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandInfo(const QString &serial)
{
	OAFUN_DEBUG("c26_f6");
	if (g_World == nullptr || !EnabledCommandInformation())
		return;

	CGameObject *obj = nullptr;

	if (serial.length())
		obj = g_World->FindWorldObject(COrionAssistant::TextToSerial(serial));

	if (obj != nullptr)
		g_OrionAssistant.DumpObjectInfo(obj);
	else
	{
		g_TargetHandler = &COrionAssistant::HandleTargetInfo;

		g_OrionAssistant.RequestTarget("Select a object for obtain info");
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandInfoTile(const QString &lasttile)
{
	OAFUN_DEBUG("c26_f7");
	if (g_World == nullptr || !EnabledCommandInformation())
		return;

	if (lasttile.toLower() == "lasttile")
		g_Target.DumpLastTileInfo();
	else
	{
		g_TargetHandler = &COrionAssistant::HandleTargetInfoTile;

		g_OrionAssistant.RequestTarget("Select a tile for obtain info");
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandInfoMenu(const QString &index)
{
	OAFUN_DEBUG("c26_f8");
	if (g_World == nullptr || !EnabledCommandInformation())
		return;

	g_MenuManager.InfoMenu(index);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSaveConfig()
{
	OAFUN_DEBUG("c26_f9");
	if (g_World == nullptr)
		return;

	g_OrionAssistantForm->SaveCurrentProfile();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClick(const QString &serial)
{
	OAFUN_DEBUG("c26_f10");
	if (g_World == nullptr)
		return;

	if (serial.length())
		g_OrionAssistant.Click(COrionAssistant::TextToSerial(serial));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUseObject(const QString &serial)
{
	OAFUN_DEBUG("c26_f11");
	if (g_World == nullptr)
		return;

	if (serial.length())
		g_OrionAssistant.DoubleClick(COrionAssistant::TextToSerial(serial));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetStatus(const QString &serial)
{
	OAFUN_DEBUG("c26_f12");
	if (g_World == nullptr)
		return;

	if (serial.length())
		g_OrionAssistant.GetStatus(COrionAssistant::TextToSerial(serial));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandAttack(const QString &serial)
{
	OAFUN_DEBUG("c26_f13");
	if (g_World == nullptr)
		return;

	if (serial.length())
		g_OrionAssistant.Attack(COrionAssistant::TextToSerial(serial));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTextWindowOpen()
{
	OAFUN_DEBUG("c26_f14");
	if (!EnabledCommandTextWindow())
		return;

	if (g_TextDialog->isVisible())
		g_TextDialog->activateWindow();
	else
		g_TextDialog->show();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTextWindowClose()
{
	OAFUN_DEBUG("c26_f15");
	if (!EnabledCommandTextWindow())
		return;

	if (g_TextDialog->isVisible())
		g_TextDialog->hide();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTextWindowClear()
{
	OAFUN_DEBUG("c26_f16");
	if (!EnabledCommandTextWindow())
		return;

	g_TextDialog->ClearText();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTextWindowPrint(const QString &text)
{
	OAFUN_DEBUG("c26_f17");
	if (!EnabledCommandTextWindow())
		return;

	g_TextDialog->AddText(text);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetLight(const bool &on, int value)
{
	OAFUN_DEBUG("c26_f18");
	if (g_World == nullptr || !EnabledFeatureLightFilter())
		return;

	if (!on)
		value = 0;

	CPacketLight(value).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetWeather(const bool &on, const int &index, const int &count, const int &temperature)
{
	OAFUN_DEBUG("c26_f19");
	if (g_World == nullptr || !EnabledFeatureWeatherFilter())
		return;

	WEATHER_DATA weather = g_Weather;

	if (!on)
		weather.Type = 0xFF;
	else
	{
		weather.Type = index;
		weather.Count = count;
		weather.Temperature = temperature;
	}

	CPacketWeather(weather).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetSeason(const bool &on, const int &index, const int &music)
{
	OAFUN_DEBUG("c26_f20");
	if (g_World == nullptr || !EnabledFeatureSeasonFilter())
		return;

	SEASON_DATA season = g_Season;

	if (!on)
		season.Index = ST_SUMMER;
	else
	{
		season.Index = index;
		season.Music = music;
	}

	CPacketSeason(season).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTrack(const bool &on, const int &x, const int &y)
{
	OAFUN_DEBUG("c26_f21");
	if (g_World == nullptr)
		return;

	g_OrionAssistant.Track(on, x, y);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSaveHotkeys(const QString &fileName, bool *result)
{
	OAFUN_DEBUG("c26_f22");
	if (g_World == nullptr)
		return;

	QDir(g_DllPath).mkdir(g_DllPath + "/SavedHotkeys");
	QString path = g_DllPath + "/SavedHotkeys/" + fileName + ".xml";

	*result = g_HotkeysForm->Save(path);

	g_OrionAssistant.ClientPrint("Hotkeys saved to: " + path);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandLoadHotkeys(const QString &fileName, bool *result)
{
	OAFUN_DEBUG("c26_f23");
	if (g_World == nullptr)
		return;

	QString path = g_DllPath + "/SavedHotkeys/" + fileName + ".xml";

	*result = g_HotkeysForm->Load(path);

	g_OrionAssistant.ClientPrint("Hotkeys loaded from: " + path);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCast(const QString &name)
{
	OAFUN_DEBUG("c26_f24");
	if (g_World == nullptr)
		return;

	g_SpellManager.Cast(name);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUseSkill(const QString &name)
{
	OAFUN_DEBUG("c26_f25");
	if (g_World == nullptr)
		return;

	g_SkillManager.Use(name);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSkillValue(const QString &name, const QString &type, int *result)
{
	OAFUN_DEBUG("c26_f26");
	if (g_World == nullptr)
		return;

	int index = g_SkillManager.GetIndex(name);
	*result = 0;

	if (index >= 0 && index < g_SkillsCount)
	{
		uchar flags = GetSkillsFlags(type);

		if (flags == SF_REAL)
			*result = (int)(g_Skills[index].GetValue() * 10.0f);
		else if (flags == SF_BASE)
			*result = (int)(g_Skills[index].GetBaseValue() * 10.0f);
		else if (flags == SF_CAP)
			*result = (int)(g_Skills[index].GetCap() * 10.0f);
		else if (flags == SF_LOCK)
			*result = (int)g_Skills[index].GetStatus();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandHelpGump()
{
	OAFUN_DEBUG("c26_f27");
	if (g_World == nullptr)
		return;

	CPacketHelpRequest().SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCloseUO()
{
	OAFUN_DEBUG("c26_f28");
	SendMessage(g_ClientHandle, WM_CLOSE, 0, 0);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandWarmode(const uint &state)
{
	OAFUN_DEBUG("c26_f29");
	if (g_World == nullptr)
		return;

	uchar warmode = (uchar)!g_Player->GetWarmode();

	if (state < 2)
		warmode = (uchar)state;

	CPacketChangeWarmode(warmode).SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandMorph(ushort graphic)
{
	OAFUN_DEBUG("c26_f30");
	if (g_World == nullptr || !EnabledCommandMorphing())
		return;

	if (!graphic)
	{
		g_MorphGraphic = 0;
		graphic = g_OriginalGraphic;
	}
	else
		g_MorphGraphic = graphic;

	g_SetInt(VKI_SET_PLAYER_GRAPHIC, graphic);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandResend()
{
	OAFUN_DEBUG("c26_f31");
	if (g_World == nullptr || !EnabledCommandResend())
		return;

	static uint lastResendTime = GetTickCount();
	uint currentTime = GetTickCount();

	if (lastResendTime > currentTime)
		return;

	lastResendTime = currentTime + 10000;

	CPacketResend().SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSound(const ushort &sound)
{
	OAFUN_DEBUG("c26_f32");
	if (g_World == nullptr)
		return;

	CPacketPlaySound(sound, g_Player->GetX(), g_Player->GetY(), g_Player->GetZ()).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandEmoteAction(const QString &name)
{
	OAFUN_DEBUG("c26_f33");
	if (g_World == nullptr || !EnabledCommandEmoteActions())
		return;

	CPacketEmoteAction(name.toStdString().c_str()).SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandHide(const QString &serial)
{
	OAFUN_DEBUG("c26_f34");
	if (g_World == nullptr || !EnabledCommandHide())
		return;

	if (serial.length())
		CPacketHideObject(COrionAssistant::TextToSerial(serial)).SendClient();
	else
	{
		g_TargetHandler = &COrionAssistant::HandleTargetHide;

		g_OrionAssistant.RequestTarget("Select a object for hide");
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandBlockMoving(const bool &on)
{
	OAFUN_DEBUG("c26_f35");
	if (g_World == nullptr || !EnabledCommandMoving())
		return;

	g_SetInt(VKI_BLOCK_MOVING, on);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUseType(const QStringList &graphic, const QStringList &color, const QString &container, const bool &recurse, bool *result)
{
	OAFUN_DEBUG("c26_f36");
	if (g_World == nullptr)
		return;

	QStringList list;

	CGameObject *found = g_OrionAssistant.FindType(graphic, color, container, "usedistance", GetFindFlags("fast"), "", recurse, list);

	if (found != nullptr)
	{
		g_OrionAssistant.DoubleClick(found->GetSerial());
		*result = true;
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUseFromGround(const QStringList &graphic, const QStringList &color, const QString &distance, const QString &flags, bool *result)
{
	OAFUN_DEBUG("c26_f37");
	if (g_World == nullptr)
		return;

	QStringList list;

	CGameObject *found = g_OrionAssistant.FindType(graphic, color, "ground", distance, GetFindFlags(flags), "", false, list);

	if (found != nullptr)
	{
		g_OrionAssistant.DoubleClick(found->GetSerial());
		*result = true;
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUseTypeList(const QString &findListName, const QString &container, const bool &recurse, bool *result)
{
	OAFUN_DEBUG("c26_f38");
	if (g_World == nullptr || !EnabledCommandFindArrays())
		return;

	QStringList list;
	CGameObject *found = g_OrionAssistant.FindList(findListName, container, "usedistance", GetFindFlags("fast"), "", recurse, list);

	if (found != nullptr)
	{
		g_OrionAssistant.DoubleClick(found->GetSerial());
		*result = true;
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUseFromGroundList(const QString &findListName, const QString &distance, const QString &flags, bool *result)
{
	OAFUN_DEBUG("c26_f39");
	if (g_World == nullptr || !EnabledCommandFindArrays())
		return;

	QStringList list;
	CGameObject *found = g_OrionAssistant.FindList(findListName, "ground", distance, GetFindFlags(flags), "", false, list);

	if (found != nullptr)
	{
		g_OrionAssistant.DoubleClick(found->GetSerial());
		*result = true;
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPrint(const ushort &color, const QString &text)
{
	OAFUN_DEBUG("c26_f40");
	if (g_World == nullptr)
		return;

	g_OrionAssistant.ClientPrint(text, (color == 0xFFFF ? g_MessagesColor : color));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCharPrint(const QString &serial, const ushort &color, const QString &text)
{
	OAFUN_DEBUG("c26_f41");
	if (g_World == nullptr)
		return;

	g_OrionAssistant.ClientCharPrint(COrionAssistant::TextToSerial(serial), text, color);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSay(const QString &text)
{
	OAFUN_DEBUG("c26_f42");
	if (g_World == nullptr || !text.trimmed().length())
		return;

	ushort color = 0;
	bool changeColor = g_TabMain->GetFontColor(color);

	if (!changeColor)
		color = 0;

	CPacketUnicodeSpeechRequest(color, text.toStdWString()).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandRenameMount(const QString &serial, const QString &text)
{
	OAFUN_DEBUG("c26_f43");
	if (g_World == nullptr || !text.trimmed().length() || !EnabledCommandRenameMounts())
		return;

	CPacketRenameMountRequest(COrionAssistant::TextToSerial(serial), text.toStdString()).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandFindType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &distance, const QString &flags, const QString &notoriety, const bool &recurse, QStringList *result)
{
	OAFUN_DEBUG("c26_f44");
	if (g_World == nullptr || !EnabledCommandFindTypeOrIgnore())
		return;

	/*CGameObject *found =*/ g_OrionAssistant.FindType(graphic, color, container, distance, GetFindFlags(flags), notoriety, recurse, *result);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandIgnore(const QString &serial, const bool &on)
{
	OAFUN_DEBUG("c26_f45");
	if (g_World == nullptr || !EnabledCommandFindTypeOrIgnore())
		return;

	CGameObject *item = g_World->FindWorldObject(COrionAssistant::TextToSerial(serial));

	if (item != nullptr)
		item->SetIgnored(on);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandIgnoreReset()
{
	OAFUN_DEBUG("c26_f46");
	if (g_World == nullptr || !EnabledCommandFindTypeOrIgnore())
		return;

	g_World->IgnoreReset();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandDrop(const QString &serial, const int &count, const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c26_f47");
	if (g_World == nullptr || !EnabledCommandMoveItems())
		return;

	if (!serial.length())
	{
		g_TargetHandler = &COrionAssistant::HandleTargetDrop;

		g_OrionAssistant.RequestTarget("Select a object for drop");
	}
	else
	{
		CGameItem *item = g_World->FindWorldItem(COrionAssistant::TextToSerial(serial));

		if (item != nullptr)
		{
			g_MoveItemData.Reset();

			g_MoveItemData.Count = count;

			if (x != 0xFFFF)
				g_MoveItemData.X = x;
			else
				g_MoveItemData.X = g_Player->GetX();

			if (y != 0xFFFF)
				g_MoveItemData.Y = y;
			else
				g_MoveItemData.Y = g_Player->GetY();

			if (z != 0xFF)
				g_MoveItemData.Z = z;
			else
				g_MoveItemData.Z = g_Player->GetZ();

			g_OrionAssistant.MoveItem(item);
		}
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandDropHere(const QString &serial, const int &count)
{
	OAFUN_DEBUG("c26_f48");
	if (g_World == nullptr || !EnabledCommandMoveItems())
		return;

	if (!serial.length())
	{
		g_TargetHandler = &COrionAssistant::HandleTargetDrop;

		g_OrionAssistant.RequestTarget("Select a object for drop");
	}
	else
	{
		CGameItem *item = g_World->FindWorldItem(COrionAssistant::TextToSerial(serial));

		if (item != nullptr)
		{
			g_MoveItemData.Reset();

			g_MoveItemData.Count = count;
			g_MoveItemData.X = g_Player->GetX();
			g_MoveItemData.Y = g_Player->GetY();
			g_MoveItemData.Z = g_Player->GetZ();

			g_OrionAssistant.MoveItem(item);
		}
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandMoveItem(const QString &serial, const int &count, const QString &container, const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c26_f49");
	if (g_World == nullptr || !EnabledCommandMoveItems())
		return;

	if (!serial.length())
	{
		g_TargetHandler = &COrionAssistant::HandleTargetDrop;

		g_OrionAssistant.RequestTarget("Select a object for move item");
	}
	else
	{
		CGameItem *item = g_World->FindWorldItem(COrionAssistant::TextToSerial(serial));

		if (item != nullptr)
		{
			g_MoveItemData.Reset();

			g_MoveItemData.Count = count;
			g_MoveItemData.Container = COrionAssistant::TextToSerial(container);
			g_MoveItemData.X = x;
			g_MoveItemData.Y = y;
			g_MoveItemData.Z = z;

			g_OrionAssistant.MoveItem(item);
		}
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandShowJournal(const int &lines)
{
	OAFUN_DEBUG("c26_f50");
	if (g_World == nullptr || !EnabledCommandInformation() || !EnabledCommandJournal())
		return;

	g_JournalManager.ShowLines(lines);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClearJournal(const QString &pattern, const QString &serial, const ushort &color, const QString &flags)
{
	OAFUN_DEBUG("c26_f51");
	if (g_World == nullptr || !EnabledCommandJournal())
		return;

	g_JournalManager.Clear(pattern, COrionAssistant::TextToSerial(serial), color, GetJournalFlags(flags.toLower()));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandJournalIgnoreCase(const bool &on)
{
	OAFUN_DEBUG("c26_f52");
	if (g_World == nullptr || !EnabledCommandJournal())
		return;

	g_JournalManager.SetIgnoreCase(on);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandInJournal(const QString &pattern, const QString &serial, const ushort &color, const QString &flags, const uint &startTime, const uint &endTime, CJournalMessage **msg)
{
	OAFUN_DEBUG("c26_f53");
	if (g_World == nullptr || !EnabledCommandJournal())
		return;

	*msg = g_JournalManager.Find(pattern, COrionAssistant::TextToSerial(serial), color, GetJournalFlags(flags.toLower()), startTime, endTime);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetDressBag(const QString &serial)
{
	OAFUN_DEBUG("c26_f54");
	if (g_World == nullptr)
		return;

	if (serial.length())
		g_TabListsObjects->SetDressBag(COrionAssistant::TextToSerial(serial));
	else
	{
		g_TargetHandler = &COrionAssistant::HandleTargetDressBag;

		g_OrionAssistant.RequestTarget("Select a dress bag.");
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUnsetDressBag()
{
	OAFUN_DEBUG("c26_f55");
	if (g_World == nullptr)
		return;

	g_TabListsObjects->SetDressBag(0);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetArm(const QString &name)
{
	OAFUN_DEBUG("c26_f56");
	if (g_World == nullptr)
		return;

	CDressListItem *list = g_TabAgentsDress->GetDressSet(name.toLower());

	if (list == nullptr)
		list = g_TabAgentsDress->AddDressSet(name);

	g_TabAgentsDress->ClearDressSet(list);

	if (g_Player != nullptr)
	{
		QFOR(item, g_Player->m_Items, CGameItem*)
		{
			int layer = item->GetEquipLayer();

			if (layer && layer <= OL_2_HAND)
			{
				g_TabAgentsDress->AddDressItemInList(list, layer, item->GetSerial());
				list->m_Items[layer] = item->GetSerial();
			}
		}
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUnsetArm(const QString &name)
{
	OAFUN_DEBUG("c26_f57");
	if (g_World == nullptr)
		return;

	CDressListItem *list = g_TabAgentsDress->GetDressSet(name.toLower());

	if (list != nullptr)
		g_TabAgentsDress->ClearDressSet(list);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetDress(const QString &name)
{
	OAFUN_DEBUG("c26_f58");
	if (g_World == nullptr)
		return;

	CDressListItem *list = g_TabAgentsDress->GetDressSet(name.toLower());

	if (list == nullptr)
		list = g_TabAgentsDress->AddDressSet(name);

	g_TabAgentsDress->ClearDressSet(list);

	if (g_Player != nullptr)
	{
		QFOR(item, g_Player->m_Items, CGameItem*)
		{
			int layer = item->GetEquipLayer();

			if (layer > OL_2_HAND && layer < OL_MOUNT && layer != OL_BACKPACK && g_LayerSafe[layer])
			{
				g_TabAgentsDress->AddDressItemInList(list, layer, item->GetSerial());
				list->m_Items[layer] = item->GetSerial();
			}
		}
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUnsetDress(const QString &name)
{
	OAFUN_DEBUG("c26_f59");
	if (g_World == nullptr)
		return;

	CDressListItem *list = g_TabAgentsDress->GetDressSet(name.toLower());

	if (list != nullptr)
		g_TabAgentsDress->ClearDressSet(list);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandArm(const QString &name)
{
	OAFUN_DEBUG("c26_f60");
	if (g_World == nullptr)
		return;

	g_TabAgentsDress->DressSet(g_TabAgentsDress->GetDressSet(name.toLower()));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandDisarm()
{
	OAFUN_DEBUG("c26_f61");
	if (g_World == nullptr)
		return;

	g_TabAgentsDress->DisarmSet();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandDress(const QString &name)
{
	OAFUN_DEBUG("c26_f62");
	if (g_World == nullptr)
		return;

	g_TabAgentsDress->DressSet(g_TabAgentsDress->GetDressSet(name.toLower()));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUndress()
{
	OAFUN_DEBUG("c26_f63");
	if (g_World == nullptr)
		return;

	g_TabAgentsDress->UndressSet();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUnequip(const QString &layerName)
{
	OAFUN_DEBUG("c26_f64");
	if (g_World == nullptr)
		return;

	int layer = COrionAssistant::TextToLayer(layerName);
	CGameItem *item = g_Player->FindLayer(layer);

	if (item != nullptr && g_LayerSafe[layer])
	{
		g_MoveItemData.Reset();
		g_MoveItemData.Container = g_Backpack;

		g_OrionAssistant.MoveItem(item);
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandEquip(const QString &serial)
{
	OAFUN_DEBUG("c26_f65");
	if (g_World == nullptr)
		return;

	CGameItem *item = g_World->FindWorldItem(COrionAssistant::TextToSerial(serial));

	if (item != nullptr && item->GetEquipLayer())
	{
		int layer = item->GetEquipLayer();

		if (!g_LayerSafe[layer])
			return;

		if (g_TabAgentsDress->UndressBeforeDressing())
		{
			CGameItem *equipped = g_Player->FindLayer(layer);

			if (equipped != nullptr)
			{
				g_TabScripts->TerminateScript(g_EquipScriptName, "");

				QString script = "function " + g_EquipScriptName + "()\n{\n";

				int delay = g_TabMain->MoveItemsDelay();

				QString buf = "";

				const char *equipModeName[10] = { "Equip", "UseObject" };
				const char *equipMode = equipModeName[g_TabAgentsDress->DoubleClickForEquipItems() ? 1 : 0];

				if (delay)
					buf.sprintf("Orion.Unequip(%i);\nOrion.Wait(%i);\nOrion.%s(0x%08X);", layer, delay, equipMode, item->GetSerial());
				else
					buf.sprintf("Orion.Unequip(%i);\nOrion.%s(0x%08X);", layer, equipMode, item->GetSerial());

				script += buf + "\n}\n";

				g_TabScripts->RunScript(g_EquipScriptName, script, 0, 0, QStringList());

				return;
			}
		}

		g_OrionAssistant.MoveEquip(item->GetSerial(), layer, g_PlayerSerial);
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandEquipT(const QString &graphic, const QString &color)
{
	OAFUN_DEBUG("c26_f66");
	if (g_World == nullptr)
		return;

	CGameItem *backpack = g_Player->FindLayer(OL_BACKPACK);

	if (backpack != nullptr)
	{
		QStringList list;

		CGameItem *item = (CGameItem*)backpack->FindType(CFindItem(graphic, color), QList<CIgnoreItem>(), FTF_FAST, true, list);

		if (item != nullptr)
			OnCommandEquip(COrionAssistant::SerialToText(item->GetSerial()));
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTerminate(const QString &functionName, const QString &functionsSave)
{
	OAFUN_DEBUG("c26_f67");
	g_TabScripts->TerminateScript(functionName, functionsSave);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandWaitTargetObject(const QStringList &objects)
{
	OAFUN_DEBUG("c26_f68");
	if (g_World == nullptr)
		return;

	g_Target.SetObjectsHook(objects);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandWaitTargetTile(const QString &tile, const int &x, const int &y, const int &z, const bool &relative)
{
	OAFUN_DEBUG("c26_f69");
	if (g_World == nullptr)
		return;

	g_Target.SetTileHook(tile, x, y, z, relative);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCancelWaitTarget()
{
	OAFUN_DEBUG("c26_f70");
	if (g_World == nullptr)
		return;

	g_Target.ClearHooks();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandWaitMenu(const QString &prompt, const QString &choice)
{
	OAFUN_DEBUG("c26_f71");
	if (g_World == nullptr)
		return;

	g_MenuManager.SetHook(prompt, choice);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCancelWaitMenu()
{
	OAFUN_DEBUG("c26_f72");
	if (g_World == nullptr)
		return;

	g_MenuManager.ClearHooks();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCountType(const QStringList &graphic, const QStringList &color, const QString &container, const QString &distance, const bool &recurse, int *result)
{
	OAFUN_DEBUG("c26_f73");
	if (g_World == nullptr)
		return;

	*result = g_OrionAssistant.CountType(graphic, color, container, distance, recurse);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandFindList(const QString &findListName, const QString &container, const QString &distance, const QString &flags, const QString &notoriety, const bool &recurse, QStringList *result)
{
	OAFUN_DEBUG("c26_f74");
	if (g_World == nullptr || !EnabledCommandFindArrays())
		return;

	/*CGameObject *found =*/ g_OrionAssistant.FindList(findListName, container, distance, GetFindFlags(flags), notoriety, recurse, *result);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTargetObject(const QStringList &objects)
{
	OAFUN_DEBUG("c26_f75");
	if (g_World == nullptr)
		return;

	if (g_Target.IsTargeting())
	{
		g_Target.SetObjectsHook(objects);
		g_Target.CheckHook();
		g_Target.ClearHooks();
		CPacketCancelTarget().SendClient();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTargetTile(const QString &tile, const int &x, const int &y, const int &z, const bool &relative)
{
	OAFUN_DEBUG("c26_f76");
	if (g_World == nullptr)
		return;

	if (g_Target.IsTargeting())
	{
		g_Target.SetTileHook(tile, x, y, z, relative);
		g_Target.CheckHook();
		g_Target.ClearHooks();
		CPacketCancelTarget().SendClient();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandAddType(const QString &name, const QString &type)
{
	OAFUN_DEBUG("c26_f77");
	if (g_World == nullptr)
		return;

	if (name.length())
		g_TabListsTypes->AddType(name, type);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandRemoveType(const QString &name)
{
	OAFUN_DEBUG("c26_f78");
	if (g_World == nullptr)
		return;

	g_TabListsTypes->RemoveType(name);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandAddObject(const QString &name, const QString &object)
{
	OAFUN_DEBUG("c26_f79");
	if (g_World == nullptr)
		return;

	if (name.length())
		g_TabListsObjects->AddObject(name, object);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandRemoveObject(const QString &name)
{
	OAFUN_DEBUG("c26_f80");
	if (g_World == nullptr)
		return;

	g_TabListsObjects->RemoveObject(name);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandLoadScript(const QString &filePath)
{
	OAFUN_DEBUG("c26_f81");
	g_TabScripts->LoadScript(filePath);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandExec(const QString &name, const bool &once, const QStringList &args)
{
	OAFUN_DEBUG("c26_f82");
	if (once && g_TabScripts->ScriptRunning(name))
		return;

	g_TabScripts->RunScript(name, g_ScriptEditorDialog->GetText(), 0, 0, args);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandHaveTarget(int *result)
{
	OAFUN_DEBUG("c26_f83");
	if (g_World == nullptr)
		return;

	if (!g_Target.IsTargeting())
		*result = 0;
	else
		*result = g_Target.GetCursorType() + 1;
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandScriptRunning(const QString &name, bool *result)
{
	OAFUN_DEBUG("c26_f84");
	*result = g_TabScripts->ScriptRunning(name);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetDistance(const QString &serial, int *result)
{
	OAFUN_DEBUG("c26_f85");
	*result = 100500;

	if (g_World != nullptr && g_Player != nullptr)
	{
		CGameObject *obj = g_World->FindWorldObject(COrionAssistant::TextToSerial(serial));

		if (obj != nullptr)
			*result = ::GetDistance(g_Player->GetX(), g_Player->GetY(), obj->GetX(), obj->GetY());
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetDistance(const int &x, const int &y, int *result)
{
	OAFUN_DEBUG("c26_f86");
	*result = 100500;

	if (g_World != nullptr && g_Player != nullptr)
		*result = ::GetDistance(g_Player->GetX(), g_Player->GetY(), x, y);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetUsedIgnoreList(const QString &listName)
{
	OAFUN_DEBUG("c26_f87");
	if (g_World == nullptr || !EnabledCommandFindArrays() || !EnabledCommandFindTypeOrIgnore())
		return;

	g_OrionAssistant.SetUsedIgnoreList(listName);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjAtLayer(const QString &layer, const QString &serial, int *result)
{
	OAFUN_DEBUG("c26_f88");
	if (g_World == nullptr || !EnabledCommandFindTypeOrIgnore())
		return;

	*result = 0;
	uint objSerial = g_PlayerSerial;

	if (serial.length())
		objSerial = COrionAssistant::TextToSerial(serial);

	if (g_World != nullptr)
	{
		CGameObject *obj = g_World->FindWorldObject(objSerial);

		if (obj != nullptr)
		{
			CGameItem *item = obj->FindLayer(COrionAssistant::TextToLayer(layer));

			if (item != nullptr)
				*result = (int)item->GetSerial();
		}
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandFindObject(const QString &serial, int *result)
{
	OAFUN_DEBUG("c26_f89");
	if (g_World == nullptr)
		return;

	*result = 0;
	uint objSerial = COrionAssistant::TextToSerial(serial);

	if (g_World != nullptr && g_World->FindWorldObject(objSerial))
		*result = objSerial;
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCanWalk(uchar direction, int x, int y, char z, bool *result)
{
	OAFUN_DEBUG("c26_f90");
	if (g_World == nullptr || !EnabledCommandMoving())
		return;

	*result = g_ClientInterface->Client->PathFinder->CanWalk(direction, x, y, z);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandStep(const int &direction, const bool &run, bool *result)
{
	OAFUN_DEBUG("c26_f91");
	if (g_World == nullptr || !EnabledCommandMoving())
		return;

	*result = g_ClientInterface->Client->PathFinder->Walk(run, direction);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandWalkTo(const int &x, const int &y, const int &z, const int &distance, bool *result)
{
	OAFUN_DEBUG("c26_f92");
	if (g_World == nullptr || !EnabledCommandMoving())
		return;

	*result = g_ClientInterface->Client->PathFinder->WalkTo(x, y, z, distance);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandStopWalking()
{
	OAFUN_DEBUG("c26_f93");
	if (g_World == nullptr || !EnabledCommandMoving())
		return;

	g_ClientInterface->Client->PathFinder->StopAutowalk();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandIsWalking(bool *result)
{
	OAFUN_DEBUG("c26_f94");
	if (g_World == nullptr || !EnabledCommandMoving())
		return;

	*result = g_ClientInterface->Client->PathFinder->GetAutowalking();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionSound(bool *state)
{
	OAFUN_DEBUG("c26_f95");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_SOUND);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionSound(const bool &state)
{
	OAFUN_DEBUG("c26_f96");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_SOUND, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionSoundVolume(int *value)
{
	OAFUN_DEBUG("c26_f97");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*value = GetInt(VKI_SOUND_VALUE);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionSoundVolume(const int &value)
{
	OAFUN_DEBUG("c26_f98");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_SOUND_VALUE, value);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionMusic(bool *state)
{
	OAFUN_DEBUG("c26_f99");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_MUSIC);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionMusic(const bool &state)
{
	OAFUN_DEBUG("c26_f100");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_MUSIC, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionMusicVolume(int *value)
{
	OAFUN_DEBUG("c26_f101");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*value = GetInt(VKI_MUSIC_VALUE);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionMusicVolume(const int &value)
{
	OAFUN_DEBUG("c26_f102");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_MUSIC_VALUE, value);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionUseTooltips(bool *state)
{
	OAFUN_DEBUG("c26_f103");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_USE_TOOLTIPS);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionUseTooltips(const bool &state)
{
	OAFUN_DEBUG("c26_f104");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_USE_TOOLTIPS, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionAlwaysRun(bool *state)
{
	OAFUN_DEBUG("c26_f105");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_ALWAYS_RUN);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionAlwaysRun(const bool &state)
{
	OAFUN_DEBUG("c26_f106");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_ALWAYS_RUN, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionNewTargetSystem(bool *state)
{
	OAFUN_DEBUG("c26_f107");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_NEW_TARGET_SYSTEM);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionNewTargetSystem(const bool &state)
{
	OAFUN_DEBUG("c26_f108");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_NEW_TARGET_SYSTEM, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionObjectHandles(bool *state)
{
	OAFUN_DEBUG("c26_f109");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_OBJECT_HANDLES);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionObjectHandles(const bool &state)
{
	OAFUN_DEBUG("c26_f110");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_OBJECT_HANDLES, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionScaleSpeech(bool *state)
{
	OAFUN_DEBUG("c26_f111");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_SCALE_SPEECH_DELAY);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionScaleSpeech(const bool &state)
{
	OAFUN_DEBUG("c26_f112");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_SCALE_SPEECH_DELAY, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionScaleSpeechDelay(int *value)
{
	OAFUN_DEBUG("c26_f113");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*value = GetInt(VKI_SPEECH_DELAY);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionScaleSpeechDelay(const int &value)
{
	OAFUN_DEBUG("c26_f114");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_SPEECH_DELAY, value);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionIgnoreGuildMessages(bool *state)
{
	OAFUN_DEBUG("c26_f115");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_IGNORE_GUILD_MESSAGES);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionIgnoreGuildMessages(const bool &state)
{
	OAFUN_DEBUG("c26_f116");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_IGNORE_GUILD_MESSAGES, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionIgnoreAllianceMessages(bool *state)
{
	OAFUN_DEBUG("c26_f117");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_IGNORE_ALLIANCE_MESSAGES);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionIgnoreAllianceMessages(const bool &state)
{
	OAFUN_DEBUG("c26_f118");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_IGNORE_ALLIANCE_MESSAGES, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionDarkNights(bool *state)
{
	OAFUN_DEBUG("c26_f119");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_DARK_NIGHTS);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionDarkNights(const bool &state)
{
	OAFUN_DEBUG("c26_f120");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_DARK_NIGHTS, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionColoredLighting(bool *state)
{
	OAFUN_DEBUG("c26_f121");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_COLORED_LIGHTING);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionColoredLighting(const bool &state)
{
	OAFUN_DEBUG("c26_f122");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_COLORED_LIGHTING, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionCriminalActionsQuery(bool *state)
{
	OAFUN_DEBUG("c26_f123");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_CRIMINAL_ACTIONS_QUERY);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionCriminalActionsQuery(const bool &state)
{
	OAFUN_DEBUG("c26_f124");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_CRIMINAL_ACTIONS_QUERY, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionCircleOfTransparency(bool *state)
{
	OAFUN_DEBUG("c26_f125");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_CIRCLETRANS);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionCircleOfTransparency(const bool &state)
{
	OAFUN_DEBUG("c26_f126");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_CIRCLETRANS, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionCircleOfTransparencyValue(int *value)
{
	OAFUN_DEBUG("c26_f127");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*value = GetInt(VKI_CIRCLETRANS_VALUE);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionCircleOfTransparencyValue(const int &value)
{
	OAFUN_DEBUG("c26_f128");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_CIRCLETRANS_VALUE, value);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionLockResizingGameWindow(bool *state)
{
	OAFUN_DEBUG("c26_f129");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_LOCK_RESIZING_GAME_WINDOW);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionLockResizingGameWindow(const bool &state)
{
	OAFUN_DEBUG("c26_f130");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_LOCK_RESIZING_GAME_WINDOW, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionFPSValue(int *value)
{
	OAFUN_DEBUG("c26_f131");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*value = GetInt(VKI_CLIENT_FPS_VALUE);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionFPSValue(const int &value)
{
	OAFUN_DEBUG("c26_f132");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_CLIENT_FPS_VALUE, value);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionUseScalingGameWindow(bool *state)
{
	OAFUN_DEBUG("c26_f133");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_USE_SCALING_GAME_WINDOW);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionUseScalingGameWindow(const bool &state)
{
	OAFUN_DEBUG("c26_f134");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_USE_SCALING_GAME_WINDOW, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionDrawStatusState(int *state)
{
	OAFUN_DEBUG("c26_f135");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_DRAW_STATUS_STATE);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionDrawStatusState(const int &state)
{
	OAFUN_DEBUG("c26_f136");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_DRAW_STATUS_STATE, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionDrawStumps(bool *state)
{
	OAFUN_DEBUG("c26_f137");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_DRAW_STUMPS);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionDrawStumps(const bool &state)
{
	OAFUN_DEBUG("c26_f138");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_DRAW_STUMPS, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionMarkingCaves(bool *state)
{
	OAFUN_DEBUG("c26_f139");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_MARKING_CAVES);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionMarkingCaves(const bool &state)
{
	OAFUN_DEBUG("c26_f140");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_MARKING_CAVES, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionNoVegetation(bool *state)
{
	OAFUN_DEBUG("c26_f141");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_NO_VEGETATION);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionNoVegetation(const bool &state)
{
	OAFUN_DEBUG("c26_f142");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_NO_VEGETATION, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionNoFieldsAnimation(bool *state)
{
	OAFUN_DEBUG("c26_f143");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_NO_ANIMATE_FIELDS);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionNoFieldsAnimation(const bool &state)
{
	OAFUN_DEBUG("c26_f144");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_NO_ANIMATE_FIELDS, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionStandardCharactesFrameRate(bool *state)
{
	OAFUN_DEBUG("c26_f145");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_STANDARD_CHARACTERS_DELAY);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionStandardCharactesFrameRate(const bool &state)
{
	OAFUN_DEBUG("c26_f146");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_STANDARD_CHARACTERS_DELAY, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionStandardItemsFrameRate(bool *state)
{
	OAFUN_DEBUG("c26_f147");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_STANDARD_ITEMS_DELAY);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionStandardItemsFrameRate(const bool &state)
{
	OAFUN_DEBUG("c26_f148");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_STANDARD_ITEMS_DELAY, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionLockGumpsMoving(bool *state)
{
	OAFUN_DEBUG("c26_f149");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_LOCK_GUMPS_MOVING);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionLockGumpsMoving(const bool &state)
{
	OAFUN_DEBUG("c26_f150");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_LOCK_GUMPS_MOVING, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionEnterChat(bool *state)
{
	OAFUN_DEBUG("c26_f151");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_CONSOLE_NEED_ENTER);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionEnterChat(const bool &state)
{
	OAFUN_DEBUG("c26_f152");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_CONSOLE_NEED_ENTER, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionHiddenCharacters(int *state)
{
	OAFUN_DEBUG("c26_f153");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_HIDDEN_CHARACTERS_MODE);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionHiddenCharacters(const int &state)
{
	OAFUN_DEBUG("c26_f154");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_HIDDEN_CHARACTERS_MODE, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionHiddenCharactersAlpha(int *value)
{
	OAFUN_DEBUG("c26_f155");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*value = GetInt(VKI_HIDDEN_CHARACTERS_ALPHA);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionHiddenCharactersAlpha(const int &value)
{
	OAFUN_DEBUG("c26_f156");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_HIDDEN_CHARACTERS_ALPHA, value);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionHiddenCharactersModeOnlyForSelf(bool *state)
{
	OAFUN_DEBUG("c26_f157");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_HIDDEN_CHARACTERS_MODE_ONLY_FOR_SELF);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionHiddenCharactersModeOnlyForSelf(const bool &state)
{
	OAFUN_DEBUG("c26_f158");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_HIDDEN_CHARACTERS_MODE_ONLY_FOR_SELF, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionTransparentSpellIcons(bool *state)
{
	OAFUN_DEBUG("c26_f159");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_TRANSPARENT_SPELL_ICONS);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionTransparentSpellIcons(const bool &state)
{
	OAFUN_DEBUG("c26_f160");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_TRANSPARENT_SPELL_ICONS, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionSpellIconsAlpha(int *value)
{
	OAFUN_DEBUG("c26_f161");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*value = GetInt(VKI_SPELL_ICONS_ALPHA);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionSpellIconsAlpha(const int &value)
{
	OAFUN_DEBUG("c26_f162");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	g_SetInt(VKI_SPELL_ICONS_ALPHA, value);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionFastRotation(bool *state)
{
	OAFUN_DEBUG("c26_f163");
	if (g_World == nullptr || !EnabledCommandOptionsAccess())
		return;

	*state = GetInt(VKI_FAST_ROTATION);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOptionFastRotation(const bool &state)
{
	OAFUN_DEBUG("c26_f164");
	if (g_World == nullptr || !EnabledCommandOptionsAccess() || !EnabledFeatureFastRotation())
		return;

	g_SetInt(VKI_FAST_ROTATION, state);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetGraphic(const int &serial, QString *value)
{
	OAFUN_DEBUG("c26_f165");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = COrionAssistant::GraphicToText(obj->GetGraphic());
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetColor(const int &serial, QString *value)
{
	OAFUN_DEBUG("c26_f166");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = COrionAssistant::GraphicToText(obj->GetColor());
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetX(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f167");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = obj->GetX();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetY(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f168");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = obj->GetY();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetZ(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f169");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = obj->GetZ();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetContainer(const int &serial, QString *value)
{
	OAFUN_DEBUG("c26_f170");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = COrionAssistant::SerialToText(obj->GetContainer());
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetMap(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f171");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = obj->GetMapIndex();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetCount(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f172");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = obj->GetCount();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetFlags(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f173");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = obj->GetFlags();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetName(const int &serial, QString *value)
{
	OAFUN_DEBUG("c26_f174");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = obj->GetName();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetMobile(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f175");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = obj->GetNPC();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetIgnored(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f176");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		if (obj != nullptr)
			*value = obj->GetIgnored();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetFrozen(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f177");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->Frozen();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetPoisoned(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f178");
	if (g_World != nullptr)
	{
		if ((uint)serial == g_PlayerSerial)
			*value = g_Player->Poisoned() || g_BuffManager.Exists(0x7560);
		else
		{
			CGameCharacter *obj = g_World->FindWorldCharacter(serial);

			if (obj != nullptr)
				*value = obj->Poisoned();
		}
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetFlying(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f179");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->Flying();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetYellowHits(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f180");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->YellowHits();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetIgnoreCharacters(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f181");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->IgnoreCharacters();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetLocked(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f182");
	if (g_World != nullptr)
	{
		CGameItem *obj = g_World->FindWorldItem(serial);

		if (obj != nullptr)
			*value = obj->Locked();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetWarMode(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f183");
	if (g_World != nullptr)
	{
		if ((uint)serial == g_PlayerSerial)
			*value = g_Player->GetWarmode();
		else
		{
			CGameCharacter *obj = g_World->FindWorldCharacter(serial);

			if (obj != nullptr)
				*value = obj->InWarMode();
		}
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetHidden(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f184");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->Hidden();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetIsHuman(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f185");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->IsHuman();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetIsPlayer(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f186");
	*value = ((uint)serial == g_PlayerSerial);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetIsCorpse(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f187");
	if (g_World != nullptr)
	{
		CGameItem *obj = g_World->FindWorldItem(serial);

		if (obj != nullptr)
			*value = obj->IsCorpse();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetLayer(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f188");
	if (g_World != nullptr)
	{
		CGameItem *obj = g_World->FindWorldItem(serial);

		if (obj != nullptr)
			*value = obj->GetLayer();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetIsMulti(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f189");
	if (g_World != nullptr)
	{
		CGameItem *obj = g_World->FindWorldItem(serial);

		if (obj != nullptr)
			*value = obj->GetMultiBody();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetEquipLayer(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f190");
	if (g_World != nullptr)
	{
		CGameItem *obj = g_World->FindWorldItem(serial);

		if (obj != nullptr)
			*value = obj->GetEquipLayer();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetHits(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f191");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetHits();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetMaxHits(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f192");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetMaxHits();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetMana(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f193");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetMana();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetMaxMana(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f194");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetMaxMana();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetStam(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f195");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetStam();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetMaxStam(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f196");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetMaxStam();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetFemale(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f197");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetSex();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetRace(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f198");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetRace();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetDirection(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f199");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetDirection();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetNotoriety(const int &serial, int *value)
{
	OAFUN_DEBUG("c26_f200");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetNotoriety();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetCanChangeName(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f201");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetCanChangeName();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetDead(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f202");
	if (g_World != nullptr)
	{
		CGameCharacter *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->Dead();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetExists(const int &serial, bool *value)
{
	OAFUN_DEBUG("c26_f203");
	if (g_World != nullptr)
	{
		CGameObject *obj = (((uint)serial == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serial));

		*value = (obj != nullptr);
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetStr(int *value)
{
	OAFUN_DEBUG("c26_f204");
	if (g_Player != nullptr)
		*value = g_Player->GetStr();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetInt(int *value)
{
	OAFUN_DEBUG("c26_f205");
	if (g_Player != nullptr)
		*value = g_Player->GetInt();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetDex(int *value)
{
	OAFUN_DEBUG("c26_f206");
	if (g_Player != nullptr)
		*value = g_Player->GetDex();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetLockStrState(int *value)
{
	OAFUN_DEBUG("c26_f207");
	if (g_Player != nullptr)
		*value = g_Player->GetLockStr();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetLockIntState(int *value)
{
	OAFUN_DEBUG("c26_f208");
	if (g_Player != nullptr)
		*value = g_Player->GetLockInt();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetLockDexState(int *value)
{
	OAFUN_DEBUG("c26_f209");
	if (g_Player != nullptr)
		*value = g_Player->GetLockDex();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetWeight(int *value)
{
	OAFUN_DEBUG("c26_f210");
	if (g_Player != nullptr)
		*value = g_Player->GetWeight();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetMaxWeight(int *value)
{
	OAFUN_DEBUG("c26_f211");
	if (g_Player != nullptr)
		*value = g_Player->GetMaxWeight();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetArmor(int *value)
{
	OAFUN_DEBUG("c26_f212");
	if (g_Player != nullptr)
		*value = g_Player->GetArmor();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetGold(int *value)
{
	OAFUN_DEBUG("c26_f213");
	if (g_Player != nullptr)
		*value = g_Player->GetGold();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetStatsCap(int *value)
{
	OAFUN_DEBUG("c26_f214");
	if (g_Player != nullptr)
		*value = g_Player->GetStatsCap();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetFollowers(int *value)
{
	OAFUN_DEBUG("c26_f215");
	if (g_Player != nullptr)
		*value = g_Player->GetFollowers();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetMaxFollowers(int *value)
{
	OAFUN_DEBUG("c26_f216");
	if (g_Player != nullptr)
		*value = g_Player->GetMaxFollowers();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetFireResistance(int *value)
{
	OAFUN_DEBUG("c26_f217");
	if (g_Player != nullptr)
		*value = g_Player->GetFireResistance();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetColdResistance(int *value)
{
	OAFUN_DEBUG("c26_f218");
	if (g_Player != nullptr)
		*value = g_Player->GetColdResistance();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetPoisonResistance(int *value)
{
	OAFUN_DEBUG("c26_f219");
	if (g_Player != nullptr)
		*value = g_Player->GetPoisonResistance();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetEnergyResistance(int *value)
{
	OAFUN_DEBUG("c26_f220");
	if (g_Player != nullptr)
		*value = g_Player->GetEnergyResistance();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetLuck(int *value)
{
	OAFUN_DEBUG("c26_f221");
	if (g_Player != nullptr)
		*value = g_Player->GetLuck();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetMinDamage(int *value)
{
	OAFUN_DEBUG("c26_f222");
	if (g_Player != nullptr)
		*value = g_Player->GetMinDamage();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetMaxDamage(int *value)
{
	OAFUN_DEBUG("c26_f223");
	if (g_Player != nullptr)
		*value = g_Player->GetMaxDamage();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetTithingPoints(int *value)
{
	OAFUN_DEBUG("c26_f224");
	if (g_Player != nullptr)
		*value = g_Player->GetTithingPoints();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetStealthSteps(int *value)
{
	OAFUN_DEBUG("c26_f225");
	if (g_Player != nullptr)
		*value = g_Player->GetStealthSteps();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayerGetParalyzed(bool *value)
{
	OAFUN_DEBUG("c26_f226");
	*value = g_BuffManager.Exists(0x755B);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClientLastTarget(QString *value)
{
	OAFUN_DEBUG("c26_f227");
	if (g_World == nullptr)
		return;

	*value = COrionAssistant::SerialToText(GetInt(VKI_LAST_TARGET));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClientLastTarget(const QString &value)
{
	OAFUN_DEBUG("c26_f228");
	if (g_World == nullptr)
		return;

	uint serial = COrionAssistant::TextToSerial(value);
	g_SetInt(VKI_LAST_TARGET, (int)serial);
	g_LastTargetObject = serial;
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClientLastAttack(QString *value)
{
	OAFUN_DEBUG("c26_f229");
	if (g_World == nullptr)
		return;

	*value = COrionAssistant::SerialToText(GetInt(VKI_LAST_ATTACK));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClientLastAttack(const QString &value)
{
	OAFUN_DEBUG("c26_f230");
	if (g_World == nullptr)
		return;

	uint serial = COrionAssistant::TextToSerial(value);
	g_SetInt(VKI_LAST_ATTACK, (int)serial);
	g_LastAttackObject = serial;
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTargetSystemSerial(QString *value)
{
	OAFUN_DEBUG("c26_f231");
	if (g_World == nullptr)
		return;

	*value = COrionAssistant::SerialToText(GetInt(VKI_NEW_TARGET_SYSTEM_SERIAL));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTargetSystemSerial(const QString &value)
{
	OAFUN_DEBUG("c26_f232");
	if (g_World == nullptr)
		return;

	g_SetInt(VKI_NEW_TARGET_SYSTEM_SERIAL, COrionAssistant::TextToSerial(value));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetSerial(QString *value)
{
	OAFUN_DEBUG("c26_f233");
	if (g_World == nullptr)
		return;

	*value = COrionAssistant::SerialToText(COrionAssistant::TextToSerial(*value));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandAddFindList(const QString &listName, const QString &graphic, const QString &color, const QString &comment)
{
	OAFUN_DEBUG("c26_f234");
	if (g_World == nullptr || !EnabledCommandFindArrays())
		return;

	g_TabListsFind->AddFindList(listName, graphic, color, comment);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClearFindList(const QString &listName)
{
	OAFUN_DEBUG("c26_f235");
	if (g_World == nullptr || !EnabledCommandFindArrays() || !listName.length())
		return;

	g_TabListsFind->ClearFindList(listName);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandAddIgnoreList(const QString &listName, const QString &object, const QString &comment)
{
	OAFUN_DEBUG("c26_f236");
	if (g_World == nullptr || !EnabledCommandFindArrays() || !listName.length())
		return;

	g_TabListsIgnore->AddIgnoreList(listName, object, comment);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandAddIgnoreList(const QString &listName, const QString &graphic, const QString &color, const QString &comment)
{
	OAFUN_DEBUG("c26_f237");
	if (g_World == nullptr || !EnabledCommandFindArrays())
		return;

	g_TabListsIgnore->AddIgnoreList(listName, graphic, color, comment);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClearIgnoreList(const QString &listName)
{
	OAFUN_DEBUG("c26_f238");
	if (g_World == nullptr || !EnabledCommandFindArrays() || !listName.length())
		return;

	g_TabListsIgnore->ClearIgnoreList(listName);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandMenuCount(int *count)
{
	OAFUN_DEBUG("c26_f239");
	if (g_World == nullptr || !EnabledCommandExtendedMenu())
		return;

	*count = g_MenuManager.GetMenuCount();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetMenu(const QString &name, CMenu **menu)
{
	OAFUN_DEBUG("c26_f240");
	if (g_World == nullptr || !EnabledCommandExtendedMenu())
		return;

	if (name.trimmed().toLower() == "last" || name.trimmed().toLower() == "lastmenu")
		*menu = g_MenuManager.GetLastMenu();
	else
		*menu = g_MenuManager.GetMenu(name);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSelectMenu(const QString &name, const QString &itemName, bool *result)
{
	OAFUN_DEBUG("c26_f241");
	if (g_World == nullptr || !EnabledCommandExtendedMenu())
		return;

	*result = g_MenuManager.SendChoiceIfMenuExists(name, itemName, false);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCloseMenu(const QString &name)
{
	OAFUN_DEBUG("c26_f242");
	if (g_World == nullptr || !EnabledCommandExtendedMenu())
		return;

	g_MenuManager.SendChoiceIfMenuExists(name, "", false);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandBuffExists(const QString &name, bool *result)
{
	OAFUN_DEBUG("c26_f243");
	if (g_World == nullptr || !EnabledCommandBuffs())
		return;

	*result = g_BuffManager.Exists(name.trimmed().toLower());
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTradeCount(int *result)
{
	OAFUN_DEBUG("c26_f244");
	if (g_World == nullptr || !EnabledCommandSecureTrading())
		return;

	*result = g_TradeManager.Count();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTradeContainer(const QString &index, const int &opponent, QString *result)
{
	OAFUN_DEBUG("c26_f245");
	if (g_World == nullptr || !EnabledCommandSecureTrading())
		return;

	CTradeWindow *trade = g_TradeManager.GetTrade(index);

	if (trade == nullptr)
		return;

	if (opponent)
		*result = COrionAssistant::SerialToText(trade->GetID1());
	else
		*result = COrionAssistant::SerialToText(trade->GetID2());
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTradeOpponent(const QString &index, QString *result)
{
	OAFUN_DEBUG("c26_f246");
	if (g_World == nullptr || !EnabledCommandSecureTrading())
		return;

	CTradeWindow *trade = g_TradeManager.GetTrade(index);

	if (trade == nullptr)
		return;

	*result = COrionAssistant::SerialToText(trade->GetSerial());
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTradeName(const QString &index, QString *result)
{
	OAFUN_DEBUG("c26_f247");
	if (g_World == nullptr || !EnabledCommandSecureTrading())
		return;

	CTradeWindow *trade = g_TradeManager.GetTrade(index);

	if (trade == nullptr)
		return;

	*result = trade->GetName();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTradeCheckState(const QString &index, const int &opponent, bool *result)
{
	OAFUN_DEBUG("c26_f248");
	if (g_World == nullptr || !EnabledCommandSecureTrading())
		return;

	CTradeWindow *trade = g_TradeManager.GetTrade(index);

	if (trade == nullptr)
		return;

	if (opponent)
		*result = trade->GetCheck1();
	else
		*result = trade->GetCheck2();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTradeCheck(const QString &index, const bool &state)
{
	OAFUN_DEBUG("c26_f249");
	if (g_World == nullptr || !EnabledCommandSecureTrading())
		return;

	CTradeWindow *trade = g_TradeManager.GetTrade(index);

	if (trade != nullptr)
		CPacketSecureTradeCheck(trade->GetID1(), state).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTradeClose(const QString &index)
{
	OAFUN_DEBUG("c26_f250");
	if (g_World == nullptr || !EnabledCommandSecureTrading())
		return;

	CTradeWindow *trade = g_TradeManager.GetTrade(index);

	if (trade != nullptr)
		CPacketSecureTradeClose(trade->GetID1()).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetGraphic(QString *value)
{
	OAFUN_DEBUG("c26_f251");
	if (g_World == nullptr)
		return;

	*value = COrionAssistant::GraphicToText(COrionAssistant::TextToGraphic(*value));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetContainer(QString *value)
{
	OAFUN_DEBUG("c26_f252");
	if (g_World == nullptr)
		return;

	CGameObject *obj = g_World->FindWorldObject(COrionAssistant::TextToSerial(*value));

	if (obj != nullptr)
		*value = COrionAssistant::SerialToText(obj->GetContainer());
	else
		*value = "";
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandFindFriend(const QString &flags, const QString &distance, QString *result)
{
	OAFUN_DEBUG("c26_f253");
	if (g_World == nullptr || !EnabledCommandFriendList())
		return;

	CGameObject *found = g_OrionAssistant.FindFriend(GetFindFlags(flags), distance);

	if (found != nullptr)
	{
		*result = COrionAssistant::SerialToText(found->GetSerial());
		g_LastFriendFound = found->GetSerial();
		g_TabListsObjects->AddObject("friend", *result);
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandFindEnemy(const QString &flags, const QString &distance, QString *result)
{
	OAFUN_DEBUG("c26_f254");
	if (g_World == nullptr || !EnabledCommandEnemyList())
		return;

	CGameObject *found = g_OrionAssistant.FindEnemy(GetFindFlags(flags), distance);

	if (found != nullptr)
	{
		*result = COrionAssistant::SerialToText(found->GetSerial());
		g_LastEnemyFound = found->GetSerial();
		g_TabListsObjects->AddObject("enemy", *result);
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetFriendList(QStringList *result)
{
	OAFUN_DEBUG("c26_f255");
	if (g_World == nullptr || !EnabledCommandFriendList())
		return;

	*result = g_TabListsFriends->GetFriendList();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetEnemyList(QStringList *result)
{
	OAFUN_DEBUG("c26_f256");
	if (g_World == nullptr || !EnabledCommandEnemyList())
		return;

	*result = g_TabListsEnemies->GetEnemyList();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetFriendsStatus()
{
	OAFUN_DEBUG("c26_f257");
	if (g_World == nullptr || !EnabledCommandFriendList())
		return;

	g_OrionAssistant.GetFriendsStatus();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetEnemiesStatus()
{
	OAFUN_DEBUG("c26_f258");
	if (g_World == nullptr || !EnabledCommandEnemyList())
		return;

	g_OrionAssistant.GetEnemiesStatus();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetFontColor(const bool &state, const QString &color)
{
	OAFUN_DEBUG("c26_f259");
	if (g_World == nullptr || !EnabledCommandFontColor())
		return;

	g_TabMain->SetFontColor(state, COrionAssistant::TextToGraphic(color));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetFontColor(bool *result)
{
	OAFUN_DEBUG("c26_f260");
	if (g_World == nullptr || !EnabledCommandFontColor())
		return;

	ushort color = 0;
	*result = g_TabMain->GetFontColor(color);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetFontColorValue(QString *result)
{
	OAFUN_DEBUG("c26_f261");
	if (g_World == nullptr || !EnabledCommandFontColor())
		return;

	ushort color = 0;
	g_TabMain->GetFontColor(color);
	*result = COrionAssistant::GraphicToText(color);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetCharactersFontColor(const bool &state, const QString &color)
{
	OAFUN_DEBUG("c26_f262");
	if (g_World == nullptr || !EnabledCommandFontColor())
		return;

	g_TabMain->SetCharactersFontColor(state, COrionAssistant::TextToGraphic(color));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetCharactersFontColor(bool *result)
{
	OAFUN_DEBUG("c26_f263");
	if (g_World == nullptr || !EnabledCommandFontColor())
		return;

	ushort color = 0;
	*result = g_TabMain->GetCharactersFontColor(color);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetCharactersFontColorValue(QString *result)
{
	OAFUN_DEBUG("c26_f264");
	if (g_World == nullptr || !EnabledCommandFontColor())
		return;

	ushort color = 0;
	g_TabMain->GetCharactersFontColor(color);
	*result = COrionAssistant::GraphicToText(color);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandAddFriend(const QString &name, const QString &object)
{
	OAFUN_DEBUG("c26_f265");
	if (g_World == nullptr)
		return;

	if (name.length())
		g_TabListsFriends->AddFriend(name, object);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandRemoveFriend(const QString &name)
{
	OAFUN_DEBUG("c26_f266");
	if (g_World == nullptr)
		return;

	g_TabListsFriends->RemoveFriend(name);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClearFriendList()
{
	OAFUN_DEBUG("c26_f267");
	if (g_World == nullptr)
		return;

	g_TabListsFriends->ClearFriendList();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandAddEnemy(const QString &name, const QString &object)
{
	OAFUN_DEBUG("c26_f268");
	if (g_World == nullptr)
		return;

	if (name.length())
		g_TabListsEnemies->AddEnemy(name, object);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandRemoveEnemy(const QString &name)
{
	OAFUN_DEBUG("c26_f269");
	if (g_World == nullptr)
		return;

	g_TabListsEnemies->RemoveEnemy(name);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClearEnemyList()
{
	OAFUN_DEBUG("c26_f270");
	if (g_World == nullptr)
		return;

	g_TabListsEnemies->ClearEnemyList();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetGlobal(const QString &name, const QString &value)
{
	OAFUN_DEBUG("c26_f271");

	GLOBALS_MAP::iterator it = m_Globals.find(name);

	if (it != m_Globals.end())
		*it = value;
	else
		m_Globals.insert(name, value);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetGlobal(const QString &name, QString *value)
{
	OAFUN_DEBUG("c26_f272");

	GLOBALS_MAP::iterator it = m_Globals.find(name);

	if (it != m_Globals.end())
		*value = *it;
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClearGlobals()
{
	OAFUN_DEBUG("c26_f273");

	m_Globals.clear();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandInfoGump(const QString &index)
{
	OAFUN_DEBUG("c26_f274");

	g_GumpManager.InfoGump(index);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandValidateTargetTile(const QString &tile, int x, int y, const bool &relative, bool *result)
{
	OAFUN_DEBUG("c26_f275");

	if (g_World == nullptr)
		return;

	if (relative)
	{
		x += g_Player->GetX();
		y += g_Player->GetY();
	}
	else if (!x && !y)
	{
		x = g_Player->GetX();
		y = g_Player->GetY();
	}

	QList<CTileInfo> tileList = g_FileManager.GetTile(x, y, 0, tile);

	if (tileList.size())
		*result = (tileList[0].GetGraphic() != 0);
	else
		*result = false;
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUseAbility(const QString &name)
{
	OAFUN_DEBUG("c26_f276");

	int index = -1;

	if (name == "primary")
		CPacketUseAbilityRequest(0).SendClient();
	else if (name == "secondary")
		CPacketUseAbilityRequest(1).SendClient();
	else
	{
		IFOR(i, 0, MAX_ABILITIES_COUNT)
		{
			if (g_AbilityName[i].toLower() == name)
			{
				index = i + 1;
				break;
			}
		}

		if (index == -1)
		{
			bool ok = false;
			int i = name.toInt(&ok);

			if (ok)
				index = i + 1;
		}
	}

	if (index != -1)
		CPacketUseCombatAbility(index).SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandLastJournalMessage(CJournalMessage **result)
{
	OAFUN_DEBUG("c26_f277");

	if (g_World == nullptr || !EnabledCommandJournal())
		return;

	*result = g_JournalManager.LastMessage();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetLastTargetPosition(int *x, int *y)
{
	OAFUN_DEBUG("c26_f278");

	if (g_World == nullptr || !EnabledCommandRemovedPtrPosition())
		return;

	CGameCharacter *obj = g_World->FindWorldCharacter(g_LastTargetObject);

	if (obj != nullptr)
	{
		*x = obj->GetX();
		*y = obj->GetY();
	}
	else
	{
		*x = g_LastTargetPosition.x();
		*y = g_LastTargetPosition.y();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetLastAttackPosition(int *x, int *y)
{
	OAFUN_DEBUG("c26_f279");

	if (g_World == nullptr || !EnabledCommandRemovedPtrPosition())
		return;

	CGameCharacter *obj = g_World->FindWorldCharacter(g_LastAttackObject);

	if (obj != nullptr)
	{
		*x = obj->GetX();
		*y = obj->GetY();
	}
	else
	{
		*x = g_LastAttackPosition.x();
		*y = g_LastAttackPosition.y();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUseWrestlingDisarm()
{
	OAFUN_DEBUG("c26_f280");

	CPacketWrestlingStun().SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUseWrestlingStun()
{
	OAFUN_DEBUG("c26_f281");

	CPacketWrestlingDisarm().SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetExists(const QString &serial, bool *value)
{
	OAFUN_DEBUG("c26_f282");
	if (g_World != nullptr)
	{
		uint serialID = COrionAssistant::TextToSerial(serial);
		CGameObject *obj = (((uint)serialID == g_PlayerSerial) ? g_Player : g_World->FindWorldObject(serialID));

		*value = (obj != nullptr);
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandInvokeVirture(const QString &name)
{
	OAFUN_DEBUG("c26_f283");

	IFOR(i, 0, MAX_INVOKE_VIRTURE_COUNT)
	{
		if (name == g_InvokeVirtureName[i])
		{
			CPacketInvokeVirtureRequest(i + 31).SendServer();
			break;
		}
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandJournalCount(int *count)
{
	OAFUN_DEBUG("c26_f284");

	if (g_World == nullptr || !EnabledCommandJournal())
		return;

	*count = g_JournalManager.Count();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandJournalLine(const int &index, CJournalMessage **result)
{
	OAFUN_DEBUG("c26_f285");

	if (g_World == nullptr || !EnabledCommandJournal())
		return;

	*result = g_JournalManager.Get(index);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetJournalLine(const CJournalMessage *message, const QString &newText)
{
	OAFUN_DEBUG("c26_f286");

	if (g_World == nullptr || !EnabledCommandJournal())
		return;

	g_JournalManager.SetMessageText(message, newText);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandWaitGump(CGumpHook *hook)
{
	OAFUN_DEBUG("c26_f287");

	if (g_World == nullptr)
		return;

	g_GumpManager.AddHook(hook);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCancelWaitGump()
{
	OAFUN_DEBUG("c26_f288");

	if (g_World == nullptr)
		return;

	g_GumpManager.ClearHooks();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSelectGump(CGump *gump, CGumpHook *hook, bool *result)
{
	OAFUN_DEBUG("c26_f288");

	if (g_World == nullptr || gump == nullptr || hook == nullptr || !EnabledCommandExtendedGump())
		return;

	*result = g_GumpManager.SendGumpChoice(gump, *hook);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGumpCount(int *count)
{
	OAFUN_DEBUG("c26_f289");

	if (g_World == nullptr || !EnabledCommandExtendedGump())
		return;

	*count = g_GumpManager.GetGumpCount();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetGump(const int &index, CGump **gump)
{
	OAFUN_DEBUG("c26_f290");

	if (g_World == nullptr || !EnabledCommandExtendedGump())
		return;

	if (index == -1)
		*gump = g_GumpManager.GetLastGump();
	else
		*gump = g_GumpManager.GetGump(index);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandGetGump(const int &serial, const int &id, CGump **gump)
{
	OAFUN_DEBUG("c26_f291");

	if (g_World == nullptr || !EnabledCommandExtendedGump())
		return;

	*gump = g_GumpManager.GetGump(serial, id);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetProperties(const uint &serial, QString *value)
{
	OAFUN_DEBUG("c26_f292");

	if (EnabledCommandCharacterProperties())
		*value = g_ObjectPropertiesManager.GetData(serial);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetProfileReceived(const uint &serial, bool *value)
{
	OAFUN_DEBUG("c26_f292");

	if (g_World != nullptr && EnabledCommandCharacterProfile())
	{
		CGameCharacter *obj = ((serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetProfileReceived();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetProfile(const uint &serial, QString *value)
{
	OAFUN_DEBUG("c26_f292");

	if (g_World != nullptr && EnabledCommandCharacterProfile())
	{
		CGameCharacter *obj = ((serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetProfile();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandRequestProfile(const uint &serial)
{
	OAFUN_DEBUG("c26_f292");

	if (g_World != nullptr && EnabledCommandCharacterProfile())
	{
		CGameCharacter *obj = g_World->FindWorldCharacter(serial);

		if (obj != nullptr)
		{
			obj->SetProfileReceived(false);
			obj->SetProfile("");
			obj->SetBlockProfile(true);
		}

		CPacketProfileRequest(serial).SendServer();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandWaitPrompt(const QString &text, const QString &serialText, const QString &typeText)
{
	OAFUN_DEBUG("c26_f293");

	if (g_World != nullptr && EnabledCommandPrompts())
	{
		uint serial = COrionAssistant::TextToSerial(serialText);
		PROMPT_TYPE type = PT_NONE;

		if (typeText == "ascii")
			type = PT_ASCII;
		else if (typeText == "unicode")
			type = PT_UNICODE;

		g_PromptManager.AddHook(CPrompt(serial, text, type));
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCancelWaitPrompt()
{
	OAFUN_DEBUG("c26_f294");

	if (g_World != nullptr && EnabledCommandPrompts())
		g_PromptManager.ClearHooks();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandObjectGetTitle(const uint &serial, QString *value)
{
	OAFUN_DEBUG("c26_f292");

	if (g_World != nullptr)
	{
		CGameCharacter *obj = ((serial == g_PlayerSerial) ? g_Player : g_World->FindWorldCharacter(serial));

		if (obj != nullptr)
			*value = obj->GetTitle();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandShop(const QString &shopListName, const bool &buy, const QString &vendorName, const int &shopDelay)
{
	OAFUN_DEBUG("c26_f292");

	if (g_TabAgentsShop != nullptr)
	{
		if (buy)
		{
			if (!EnabledCommandShopBuy())
				return;
		}
		else if (!EnabledCommandShopSell())
			return;

		SHOP_LIST_WAIT_STATE state = (buy ? SLWS_BUY : SLWS_SELL);
		wstring text = vendorName.toStdWString() + (vendorName.length() ? L" " : L"") + (buy ? L"buy" : L"sell");

		g_TabAgentsShop->SendShopRequest(state, shopListName, text, shopDelay);
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandIsShopping(bool *value)
{
	OAFUN_DEBUG("c26_f293");

	if (g_TabAgentsShop != nullptr && EnabledFeatureShopAgent())
		*value = g_TabAgentsShop->IsActiveShopping();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandShowStatusbar(const QString &serialText, const int &x, const int &y, const bool &minimized)
{
	OAFUN_DEBUG("c26_f294");

	if (EnabledCommandStatusbars())
		CPacketDrawStatusbar(COrionAssistant::TextToSerial(serialText), x, y, minimized).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCloseStatusbar(const QString &serialText)
{
	OAFUN_DEBUG("c26_f295");

	if (EnabledCommandStatusbars())
		CPacketCloseStatusbar(COrionAssistant::TextToSerial(serialText)).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandLogOut()
{
	OAFUN_DEBUG("c26_f296");

	if (EnabledCommandLogOut())
		CPacketLogOut().SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTextWindowSetPos(const int &x, const int &y)
{
	OAFUN_DEBUG("c26_f297");

	if (EnabledCommandTextWindow() && g_TextDialog->isVisible())
		g_TextDialog->move(x, y);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTextWindowSetSize(const int &width, const int &height)
{
	OAFUN_DEBUG("c26_f298");

	if (EnabledCommandTextWindow() && g_TextDialog->isVisible())
		g_TextDialog->resize(width, height);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPlayClientMacro(const QStringList &action, const QStringList &subAction)
{
	OAFUN_DEBUG("c26_f299");

	if (g_World != nullptr)
	{
		QVector<string> actionList;
		QVector<string> subActionList;

		int count = action.size();

		IFOR(i, 0, count)
		{
			actionList.push_back(action[i].toStdString());
			subActionList.push_back(subAction[i].toStdString());
		}

		CPacketPlayMacro(actionList, subActionList).SendClient();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTimerExists(const QString &name, bool *value)
{
	OAFUN_DEBUG("c26_f300");

	if (EnabledCommandTimers())
		*value = (m_Timers.find(name) != m_Timers.end());
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSetTimer(const QString &name, const int &value)
{
	OAFUN_DEBUG("c26_f301");

	if (EnabledCommandTimers())
	{
		TIMERS_MAP::iterator it = m_Timers.find(name);

		if (it != m_Timers.end())
			*it = m_Time.elapsed() - value;
		else
			m_Timers.insert(name, m_Time.elapsed() - value);
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTimer(const QString &name, int *value)
{
	OAFUN_DEBUG("c26_f302");

	if (EnabledCommandTimers())
	{
		TIMERS_MAP::iterator it = m_Timers.find(name);

		if (it != m_Timers.end())
			*value = m_Time.elapsed() - it.value();
		else
			*value = -1;
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandRemoveTimer(const QString &name)
{
	OAFUN_DEBUG("c26_f303");

	if (EnabledCommandTimers())
		m_Timers.remove(name);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClearTimers()
{
	OAFUN_DEBUG("c26_f304");

	if (EnabledCommandTimers())
		m_Timers.clear();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCancelTarget()
{
	OAFUN_DEBUG("c26_f305");

	if (g_World == nullptr)
		return;

	if (g_Target.IsTargeting())
	{
		g_Target.SendCancelTarget();
		CPacketCancelTarget().SendClient();
	}
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPromptExists(bool *value)
{
	OAFUN_DEBUG("c26_f306");

	if (g_World != nullptr && EnabledCommandPrompts())
		*value = g_PromptManager.GetExists();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPromptSerial(int *value)
{
	OAFUN_DEBUG("c26_f307");

	if (g_World != nullptr && EnabledCommandPrompts())
		*value = g_PromptManager.GetSerial();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandPromptID(int *value)
{
	OAFUN_DEBUG("c26_f308");

	if (g_World != nullptr && EnabledCommandPrompts())
		*value = g_PromptManager.GetID();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandSendPrompt(const QString &text)
{
	OAFUN_DEBUG("c26_f309");

	if (g_World != nullptr && EnabledCommandPrompts())
		g_PromptManager.Send(text);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOpenPaperdoll(const QString &serial)
{
	OAFUN_DEBUG("c26_f310");
	if (g_World == nullptr)
		return;

	if (serial.length())
		CPacketDoubleClickRequest(COrionAssistant::TextToSerial(serial) | 0x80000000).SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClosePaperdoll(const QString &serial)
{
	OAFUN_DEBUG("c26_f311");
	if (g_World == nullptr)
		return;

	if (serial.length())
		CPacketClosePaperdoll(COrionAssistant::TextToSerial(serial)).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandMovePaperdoll(const QString &serial, const int &x, const int &y)
{
	OAFUN_DEBUG("c26_f312");
	if (g_World == nullptr)
		return;

	if (serial.length())
		CPacketMovePaperdoll(COrionAssistant::TextToSerial(serial), x, y).SendClient();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandRequestContextMenu(const QString &serial)
{
	OAFUN_DEBUG("c26_f313");
	if (g_World == nullptr || !EnabledCommandContextMenu())
		return;

	CPacketContextMenuRequest(COrionAssistant::TextToSerial(serial)).SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandWaitContextMenu(const QString &serial, const int &index)
{
	OAFUN_DEBUG("c26_f314");
	if (g_World == nullptr || !EnabledCommandContextMenu())
		return;

	g_ContextMenuManager.SetHook(COrionAssistant::TextToSerial(serial), index);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCancelContextMenu()
{
	OAFUN_DEBUG("c26_f315");
	if (g_World == nullptr || !EnabledCommandContextMenu())
		return;

	g_ContextMenuManager.RemoveHook();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandBandageTarget(const QString &serial)
{
	OAFUN_DEBUG("c26_f316");
	if (g_World == nullptr)
		return;

	CGameItem *bandage = g_Player->FindBandage();

	if (bandage != nullptr)
		CPacketTargetUseObject(bandage->GetSerial(), COrionAssistant::TextToSerial(serial)).SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCastTarget(const QString &name, const QString &serial)
{
	OAFUN_DEBUG("c26_f317");
	if (g_World == nullptr)
		return;

	g_SpellManager.TargetCast(name, COrionAssistant::TextToSerial(serial));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandUseSkillTarget(const QString &name, const QString &serial)
{
	OAFUN_DEBUG("c26_f318");
	if (g_World == nullptr)
		return;

	g_SkillManager.TargetUse(name, COrionAssistant::TextToSerial(serial));
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClientViewRange(int *value)
{
	OAFUN_DEBUG("c26_f319");
	if (g_World == nullptr)
		return;

	*value = GetInt(VKI_VIEW_RANGE);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandClientViewRange(const int &value)
{
	OAFUN_DEBUG("c26_f320");
	if (g_World == nullptr)
		return;

	CPacketClientViewRange(value).SendServer();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOpenOrionMap()
{
	OAFUN_DEBUG("c26_f321");
	g_TabMain->OpenMap();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandMoveOrionMap(const int &x, const int &y)
{
	OAFUN_DEBUG("c26_f322");
	g_TabMain->MoveMap(x, y);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandCloseOrionMap()
{
	OAFUN_DEBUG("c26_f323");
	g_TabMain->CloseMap();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandLoadProfile(const QString &name)
{
	OAFUN_DEBUG("c26_f324");
	g_TabMain->SetCurrentProfile(name);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOnOffHotkeys(bool *value)
{
	OAFUN_DEBUG("c26_f325");
	*value = g_TabHotkeys->OnOffHotkeys();
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandOnOffHotkeys(const bool &value)
{
	OAFUN_DEBUG("c26_f326");
	g_TabHotkeys->SetOnOffHotkeys(value);
}
//----------------------------------------------------------------------------------
void CCommandManager::OnCommandTextWindowSaveToFile(const QString &filePath)
{
	OAFUN_DEBUG("c26_f327");
	g_TextDialog->SaveToFile(filePath);
}
//----------------------------------------------------------------------------------
