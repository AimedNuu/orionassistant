/***********************************************************************************
**
** TradeManager.h
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TRADEMANAGER_H
#define TRADEMANAGER_H
//----------------------------------------------------------------------------------
#include "../CommonItems/tradewindow.h"
#include "../Managers/DataReader.h"
//----------------------------------------------------------------------------------
typedef QList<CTradeWindow> TRADE_LIST;
//----------------------------------------------------------------------------------
class CTradeManager
{
private:
	TRADE_LIST m_List;

public:
	CTradeManager();
	virtual ~CTradeManager();

	bool OnPacketSend(CDataReader &reader);

	bool OnPacketReceived(CDataReader &reader);

	void Clear();

	int Count();

	CTradeWindow *GetTrade(const QString &serial);
};
//----------------------------------------------------------------------------------
extern CTradeManager g_TradeManager;
//----------------------------------------------------------------------------------
#endif // TRADEMANAGER_H
//----------------------------------------------------------------------------------
