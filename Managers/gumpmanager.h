/***********************************************************************************
**
** GumpManager.h
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef GUMPMANAGER_H
#define GUMPMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../Managers/DataReader.h"
#include "../CommonItems/gump.h"
#include "../CommonItems/gumphook.h"
//----------------------------------------------------------------------------------
class CGumpManager
{
private:
	CGump *m_LastGump{ nullptr };

	QList<CGumpHook> m_Hooks;

	QList<CGump*> m_Items;

	void RemoveLastGump();

public:
	CGumpManager();
	virtual ~CGumpManager();

	int GetGumpCount();

	CGump *GetLastGump();

	CGump *GetGump(const uint &index);

	CGump *GetGump(const uint &serial, const uint &id);

	void ClearGumps();

	void AddHook(CGumpHook *hook);

	void ClearHooks();

	bool OnPacketReceived(CDataReader &reader);

	bool OnPacketSend(CDataReader &reader);

	bool SendGumpChoice(CGump *gump, CGumpHook &hook);

	void InfoGump(const QString &gumpIndex);
};
//----------------------------------------------------------------------------------
extern CGumpManager g_GumpManager;
//----------------------------------------------------------------------------------
#endif // GUMPMANAGER_H
//----------------------------------------------------------------------------------
