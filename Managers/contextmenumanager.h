/***********************************************************************************
**
** ContextMenuManager.h
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef CONTEXTMENUMANAGER_H
#define CONTEXTMENUMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../Managers/DataReader.h"
//----------------------------------------------------------------------------------
class CContextMenuManager
{
	SETGET(uint, Serial, 0)
	SETGET(bool, HookEnabled, false)
	SETGET(uint, HookID, 0)
	SETGET(uint, HookIndex, 0)

public:
	CContextMenuManager();

	void SetHook(const uint &id, const uint &index);

	void RemoveHook();

	bool OnPacketReceived(CDataReader &reader);

	bool OnPacketSend(CDataReader &reader);
};
//----------------------------------------------------------------------------------
extern CContextMenuManager g_ContextMenuManager;
//----------------------------------------------------------------------------------
#endif // CONTEXTMENUMANAGER_H
//----------------------------------------------------------------------------------
