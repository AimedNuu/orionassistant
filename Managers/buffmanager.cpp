// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** BuffManager.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "buffmanager.h"
#include "../OrionAssistant/orionassistant.h"

CBuffManager g_BuffManager;
//----------------------------------------------------------------------------------
CBuffManager::CBuffManager()
{
}
//----------------------------------------------------------------------------------
CBuffManager::~CBuffManager()
{
}
//----------------------------------------------------------------------------------
void CBuffManager::Clear()
{
	OAFUN_DEBUG("c16_f1");
	m_Map.clear();
}
//----------------------------------------------------------------------------------
bool CBuffManager::Exists(const ushort &graphic)
{
	OAFUN_DEBUG("c16_f2");
	return (m_Map.find(graphic) != m_Map.end());
}
//----------------------------------------------------------------------------------
bool CBuffManager::Exists(const QString &name)
{
	OAFUN_DEBUG("c16_f3");
	ushort graphic = COrionAssistant::TextToGraphic(name);

	for (BUFF_MAP::iterator i = m_Map.begin(); i != m_Map.end(); ++i)
	{
		if (i->GetGraphic() == graphic || i->GetName() == name)
			return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
void CBuffManager::Add(const ushort &graphic, const uint &timer, const QString &name)
{
	OAFUN_DEBUG("c16_f6");
	BUFF_MAP::iterator found = m_Map.find(graphic);

	if (found != m_Map.end())
		found.value().SetTimer(timer);
	else
		m_Map.insert(graphic, CBuffItem(graphic, timer, name));
}
//----------------------------------------------------------------------------------
void CBuffManager::Remove(const ushort &graphic)
{
	OAFUN_DEBUG("c16_f5");
	m_Map.remove(graphic);
}
//----------------------------------------------------------------------------------
