// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TextCommandManager.cpp
**
** Copyright (C) January 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "textcommandmanager.h"
#include "../GameObjects/gameworld.h"
#include "../OrionAssistant/orionassistant.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../OrionAssistant/Packets.h"
#include "../OrionAssistant/textdialog.h"
#include "../OrionAssistant/Target.h"
#include "../GameObjects/GamePlayer.h"
#include "../GUI/runningscriptlistitem.h"
#include "spellmanager.h"
#include "skillmanager.h"
#include "menumanager.h"
#include "journalmanager.h"
#include "buffmanager.h"
#include "commandmanager.h"
#include <QDir>
#include "../OrionAssistant/tabmain.h"
#include "../OrionAssistant/tabscripts.h"

CTextCommandManager g_TextCommandManager;
COMMANDS_MAP CTextCommandManager::m_Commands;
//----------------------------------------------------------------------------------
#define COMMAND(name, flags, description) m_Commands.insert(#name, CCommand(true, flags, &CTextCommandManager::Command ## name, description))
//----------------------------------------------------------------------------------
CTextCommandManager::CTextCommandManager()
{
}
//----------------------------------------------------------------------------------
CTextCommandManager::~CTextCommandManager()
{
}
//----------------------------------------------------------------------------------
void CTextCommandManager::InitCommands()
{
	COMMAND(info, OAEC_INFORMATION | OAEC_TEXT_WINDOW, "Usage: info [serial=targetRequest]");
	COMMAND(infotile, OAEC_INFORMATION | OAEC_TEXT_WINDOW, "Usage: infotile ['lasttile'=targetRequest]");
	COMMAND(saveconfig, 0, "Usage: saveconfig");
	COMMAND(setdressbag, 0, "Usage: setdressbag [serial=targetRequest]");
	COMMAND(unsetdressbag, 0, "Usage: unsetdressbag");
	COMMAND(getstatus, 0, "Usage: getstatus [serial=self]");
	COMMAND(click, 0, "Usage: click [serial=self]");
	COMMAND(useobject, 0, "Usage: useobject [serial=self]");
	COMMAND(attack, 0, "Usage: attack [serial=self]");
	COMMAND(usetype, OAEC_SEARCH_CONTAINER, "Usage: usetype graphic [color=0xFFFF] [container=self] [recurse=true]");
	COMMAND(usefromground, OAEC_SEARCH_GROUND, "Usage: usefromground graphic [color=0xFFFF] [distance=useObjectsDistance]");
	COMMAND(textopen, OAEC_TEXT_WINDOW, "Usage: textopen");
	COMMAND(textclose, OAEC_TEXT_WINDOW, "Usage: textclose");
	COMMAND(textclear, OAEC_TEXT_WINDOW, "Usage: textclear");
	COMMAND(textprint, OAEC_TEXT_WINDOW, "Usage: textprint 'text'");
	COMMAND(terminate, 0, "Usage: terminate scriptName");
	COMMAND(setlight, 0, "Usage: setlight on/off [value=currentLight]");
	COMMAND(setweather, 0, "Usage: setweather on/off [index=currentWeather] [effectsCount=currentCount] [temperature=currentTemperature]");
	COMMAND(setseason, 0, "Usage: setseason on/off [index=currentIndex] [musicIndex=currentMusic]");
	COMMAND(cast, 0, "Usage: cast spellIndex/spellName");
	COMMAND(useskill, 0, "Usage: useskill skillIndex/skillName");
	COMMAND(helpgump, 0, "Usage: helpgump");
	COMMAND(closeuo, 0, "Usage: closeuo");
	COMMAND(warmode, 0, "Usage: warmode [on/off=switch]");
	COMMAND(morph, OAEC_MORPHING, "Usage: morph [graphic=off]");
	COMMAND(resend, OAEC_RESEND, "Usage: resend");
	COMMAND(sound, 0, "Usage: sound index");
	COMMAND(emoteaction, OAEC_EMOTE_ACTIONS, "Usage: emoteaction actionName");
	COMMAND(hide, OAEC_HIDE, "Usage: hide [serial=target]");
	COMMAND(blockmoving, OAEC_MOVING, "Usage: blockmoving on/off");
	COMMAND(drop, OAEC_MOVE_ITEMS, "Usage: drop serial count x y z");
	COMMAND(drophere, OAEC_MOVE_ITEMS, "Usage: drophere [serial=target] [count=all]");
	COMMAND(moveitem, OAEC_MOVE_ITEMS, "Usage: moveitem serial [count=all] [container=backpack] [x y] [z]");
	COMMAND(setarm, 0, "Usage: setarm setName");
	COMMAND(unsetarm, 0, "Usage: unsetarm setName");
	COMMAND(setdress, 0, "Usage: setdress setName");
	COMMAND(unsetdress, 0, "Usage: unsetdress setName");
	COMMAND(savehotkeys, 0, "Usage: savehotkeys fileName");
	COMMAND(loadhotkeys, 0, "Usage: loadhotkeys fileName");
	COMMAND(track, 0, "Usage: track [on=off] [x=-1 y=-1]");
	COMMAND(infomenu, OAEC_INFORMATION | OAEC_TEXT_WINDOW, "Usage: infomenu");
	COMMAND(showjournal, OAEC_INFORMATION | OAEC_TEXT_WINDOW | OAEC_JOURNAL, "Usage: showjournal [linesCount=maxLines]");
	COMMAND(clearjournal, OAEC_JOURNAL, "Usage: clearjournal ['pattern'] [serial=0xFFFFFFFF] [color=0xFFFF] [type=all]");
	COMMAND(journalignorecase, OAEC_JOURNAL, "Usage: journalignorecase [on=off]");
	COMMAND(ignorereset, OAEC_FIND_TYPE_OR_IGNORE, "Usage: ignorereset");
	COMMAND(infogump, OAEC_INFORMATION | OAEC_TEXT_WINDOW, "Usage: infogump");
}
//----------------------------------------------------------------------------------
void CTextCommandManager::DoCommand(const QString &text)
{
	OAFUN_DEBUG("c25_f1");
	if (g_World == nullptr)
		return;

	CTextParser parser(text.toStdString(), " ", "#", "''\"\"()");

	while (!parser.IsEOF())
	{
		QStringList list = parser.ReadTokens();

		if (list.size() == 0)
		{
			//g_OrionAssistant.ClientPrint("Error: empty command");
			continue;
		}

		QString str = list.at(0);

		if (!str.length())
			continue;

		COMMANDS_MAP::iterator command = m_Commands.find(str.toLower());

		if (command == m_Commands.end())
		{
			g_OrionAssistant.ClientPrint(QString("Unknown command: ") + str);
			continue;
		}

		if (command->GetEnabled())
		{
			list.pop_front();
			try
			{
				(this ->*(command->Handler))(list, *command);
			}
			catch(...)
			{
				g_OrionAssistant.ClientPrint(QString("Warning! Error in command: ") + str);
			}
		}
		else
			g_OrionAssistant.ClientPrint("This command is disabled!");
	}
}
//----------------------------------------------------------------------------------
void CTextCommandManager::UpdateCommands()
{
	OAFUN_DEBUG("c25_f2");
	for (COMMANDS_MAP::iterator i = m_Commands.begin(); i != m_Commands.end(); ++i)
	{
		CCommand &command = i.value();
		uint64 flags = command.GetFlags();

		if (!flags || ((g_EnabledCommands & flags) == flags))
			command.SetEnabled(true);
		else
			command.SetEnabled(false);
	}
}
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
#define HCOMMAND(name) void CTextCommandManager::Command ##name (const COMMAND_ARGS &args, const CCommand &command)
//----------------------------------------------------------------------------------
HCOMMAND(info)
{
	OAFUN_DEBUG("c25_f3");
	Q_UNUSED(command);

	QString serial = "";

	if (args.size())
		serial = args[0];

	emit g_CommandManager.CommandInfo(serial);
}
//----------------------------------------------------------------------------------
HCOMMAND(infotile)
{
	OAFUN_DEBUG("c25_f4");
	Q_UNUSED(command);

	QString lasttile = "";

	if (args.size())
		lasttile = args[0];

	emit g_CommandManager.CommandInfoTile(lasttile);
}
//----------------------------------------------------------------------------------
HCOMMAND(saveconfig)
{
	OAFUN_DEBUG("c25_f5");
	Q_UNUSED(command);
	Q_UNUSED(args);

	emit g_CommandManager.CommandSaveConfig();
}
//----------------------------------------------------------------------------------
HCOMMAND(setdressbag)
{
	OAFUN_DEBUG("c25_f6");
	Q_UNUSED(command);

	QString serial = "";

	if (args.size())
		serial = args[0];

	emit g_CommandManager.CommandSetDressBag(serial);
}
//----------------------------------------------------------------------------------
HCOMMAND(unsetdressbag)
{
	OAFUN_DEBUG("c25_f7");
	Q_UNUSED(args);
	Q_UNUSED(command);

	emit g_CommandManager.CommandUnsetDressBag();
}
//----------------------------------------------------------------------------------
HCOMMAND(getstatus)
{
	OAFUN_DEBUG("c25_f8");
	Q_UNUSED(command);

	QString serial = "self";

	if (args.size())
		serial = args[0];

	emit g_CommandManager.CommandGetStatus(serial);
}
//----------------------------------------------------------------------------------
HCOMMAND(click)
{
	OAFUN_DEBUG("c25_f0");
	Q_UNUSED(command);

	QString serial = "self";

	if (args.size())
		serial = args[0];

	emit g_CommandManager.CommandClick(serial);
}
//----------------------------------------------------------------------------------
HCOMMAND(useobject)
{
	OAFUN_DEBUG("c25_f10");
	Q_UNUSED(command);

	QString serial = "self";

	if (args.size())
		serial = args[0];

	emit g_CommandManager.CommandUseObject(serial);
}
//----------------------------------------------------------------------------------
HCOMMAND(attack)
{
	OAFUN_DEBUG("c25_f11");
	Q_UNUSED(command);

	if (args.size())
		emit g_CommandManager.CommandAttack(args[0]);
}
//----------------------------------------------------------------------------------
HCOMMAND(usetype)
{
	OAFUN_DEBUG("c25_f12");
	int size = args.size();

	if (!size)
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	QString color = "0xFFFF";
	QString container = "self";
	bool recurseSearch = true;

	if (size > 1)
	{
		color = args[1];

		if (size > 2)
		{
			container = args[2];

			if (container == "ground")
				recurseSearch = false;
			else if (size > 3)
				recurseSearch = COrionAssistant::RawStringToBool(args[3]);
		}
	}

	bool result = false;
	emit g_CommandManager.CommandUseType(QStringList() << args[0], QStringList() << color, container, recurseSearch, &result);
}
//----------------------------------------------------------------------------------
HCOMMAND(usefromground)
{
	OAFUN_DEBUG("c25_f13");
	int size = args.size();

	if (!size)
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	QString color = "0xFFFF";
	QString distance = "usedistance";
	QString flags = "";

	if (size > 1)
	{
		color = args[1];

		if (size > 2)
			distance = args[2];
	}

	bool result = false;
	emit g_CommandManager.CommandUseFromGround(QStringList() << args[0], QStringList() << color, distance, flags, &result);
}
//----------------------------------------------------------------------------------
HCOMMAND(textopen)
{
	OAFUN_DEBUG("c25_f14");
	Q_UNUSED(command);
	Q_UNUSED(args);

	emit g_CommandManager.CommandTextWindowOpen();
}
//----------------------------------------------------------------------------------
HCOMMAND(textclose)
{
	OAFUN_DEBUG("c25_f15");
	Q_UNUSED(command);
	Q_UNUSED(args);

	emit g_CommandManager.CommandTextWindowClose();
}
//----------------------------------------------------------------------------------
HCOMMAND(textclear)
{
	OAFUN_DEBUG("c25_f16");
	Q_UNUSED(command);
	Q_UNUSED(args);

	emit g_CommandManager.CommandTextWindowClear();
}
//----------------------------------------------------------------------------------
HCOMMAND(textprint)
{
	OAFUN_DEBUG("c25_f17");
	Q_UNUSED(command);

	QString text = "";
	bool first = true;

	if (!args.empty())
	{
		for (const QString &str : args)
		{
			if (!first)
				text += " ";

			first = false;
			text += str;
		}
	}

	emit g_CommandManager.CommandTextWindowPrint(text);
}
//----------------------------------------------------------------------------------
HCOMMAND(terminate)
{
	OAFUN_DEBUG("c25_f18");
	Q_UNUSED(command);

	if (!args.size() || !args[0].length())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
	}

	QString functionsSave = "";

	if (args.size() > 1)
		functionsSave = args[1];

	g_TabScripts->TerminateScript(args[0], functionsSave);
}
//----------------------------------------------------------------------------------
HCOMMAND(setlight)
{
	OAFUN_DEBUG("c25_f19");
	if (!g_TabMain->LightFilter())
		return;

	Q_UNUSED(command);

	int size = args.size();

	if (!size)
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	bool on = COrionAssistant::RawStringToBool(args[0]);
	uchar value = 0;

	if (on)
	{
		if (size > 1)
			value = args[1].toInt();
		else
			value = g_LightLevel;
	}

	emit g_CommandManager.CommandSetLight(on, value);
}
//----------------------------------------------------------------------------------
HCOMMAND(setweather)
{
	OAFUN_DEBUG("c25_f20");
	if (!g_TabMain->WeatherFilter())
		return;

	Q_UNUSED(command);

	int size = args.size();

	if (!size)
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	WEATHER_DATA weather = g_Weather;
	bool on = COrionAssistant::RawStringToBool(args[0]);

	if (!on)
		weather.Type = 0xFF;
	else if (size > 1)
	{
		weather.Type = args[1].toInt();

		if (size > 2)
		{
			weather.Count = args[2].toInt();

			if (size > 3)
				weather.Temperature = args[3].toInt();
		}
	}

	emit g_CommandManager.CommandSetWeather(on, weather.Type, weather.Count, weather.Temperature);
}
//----------------------------------------------------------------------------------
HCOMMAND(setseason)
{
	OAFUN_DEBUG("c25_f21");
	if (!g_TabMain->SeasonFilter())
		return;

	Q_UNUSED(command);

	int size = args.size();

	if (!size)
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	SEASON_DATA season = g_Season;
	bool on = COrionAssistant::RawStringToBool(args[0]);

	if (!on)
		season.Index = ST_SUMMER;
	else if (size > 1)
	{
		season.Index = args[1].toInt();

		if (size > 2)
			season.Music = args[2].toInt();
	}

	emit g_CommandManager.CommandSetSeason(on, season.Index, season.Music);
}
//----------------------------------------------------------------------------------
HCOMMAND(cast)
{
	OAFUN_DEBUG("c25_f22");
	Q_UNUSED(command);

	if (!args.size())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	emit g_CommandManager.CommandCast(args[0]);
}
//----------------------------------------------------------------------------------
HCOMMAND(useskill)
{
	OAFUN_DEBUG("c25_f23");
	Q_UNUSED(command);

	if (!args.size())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	emit g_CommandManager.CommandUseSkill(args[0]);
}
//----------------------------------------------------------------------------------
HCOMMAND(helpgump)
{
	OAFUN_DEBUG("c25_f24");
	Q_UNUSED(args);
	Q_UNUSED(command);

	emit g_CommandManager.CommandHelpGump();
}
//----------------------------------------------------------------------------------
HCOMMAND(closeuo)
{
	OAFUN_DEBUG("c25_f25");
	Q_UNUSED(args);
	Q_UNUSED(command);

	emit g_CommandManager.CommandCloseUO();
}
//----------------------------------------------------------------------------------
HCOMMAND(warmode)
{
	OAFUN_DEBUG("c25_f26");
	Q_UNUSED(command);

	uint state = 2;

	if (args.size())
		state = (uint)COrionAssistant::RawStringToBool(args[0]);

	emit g_CommandManager.CommandWarmode(state);
}
//----------------------------------------------------------------------------------
HCOMMAND(morph)
{
	OAFUN_DEBUG("c25_f27");
	Q_UNUSED(command);

	ushort graphic = 0;

	if (args.size())
		graphic = COrionAssistant::TextToGraphic(args[0]);

	emit g_CommandManager.CommandMorph(graphic);
}
//----------------------------------------------------------------------------------
HCOMMAND(resend)
{
	OAFUN_DEBUG("c25_f28");
	Q_UNUSED(args);
	Q_UNUSED(command);

	emit g_CommandManager.CommandResend();
}
//----------------------------------------------------------------------------------
HCOMMAND(sound)
{
	OAFUN_DEBUG("c25_f29");
	Q_UNUSED(command);

	if (!args.size())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	emit g_CommandManager.CommandSound(COrionAssistant::TextToGraphic(args[0]));
}
//----------------------------------------------------------------------------------
HCOMMAND(emoteaction)
{
	OAFUN_DEBUG("c25_f30");
	Q_UNUSED(command);

	if (!args.size())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	emit g_CommandManager.CommandEmoteAction(args[0]);
}
//----------------------------------------------------------------------------------
HCOMMAND(hide)
{
	OAFUN_DEBUG("c25_f31");
	Q_UNUSED(command);

	QString serial = "";

	if (args.size())
		serial = args[0];

	emit g_CommandManager.CommandHide(serial);
}
//----------------------------------------------------------------------------------
HCOMMAND(blockmoving)
{
	OAFUN_DEBUG("c25_f32");
	Q_UNUSED(command);

	if (!args.size())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	emit g_CommandManager.CommandBlockMoving(COrionAssistant::RawStringToBool(args[0]));
}
//----------------------------------------------------------------------------------
HCOMMAND(drop)
{
	OAFUN_DEBUG("c25_f33");
	Q_UNUSED(command);

	int size = (int)args.size();

	if (!size)
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	g_MoveItemData.Reset();

	QString serial = "";

	int count = 0;
	ushort x = 0xFFFF;
	ushort y = 0xFFFF;
	char z = -1;

	serial = args[0];

	if (size > 1)
	{
		count = args[1].toInt();

		if (size > 3)
		{
			x = args[2].toShort();
			y = args[3].toShort();

			if (size > 4)
				z = (char)args[4].toShort();
		}
	}

	emit g_CommandManager.CommandDrop(serial, count, x, y, z);
}
//----------------------------------------------------------------------------------
HCOMMAND(drophere)
{
	OAFUN_DEBUG("c25_f34");
	Q_UNUSED(command);

	int size = (int)args.size();

	QString serial = "";
	int count = 0;

	if (size)
	{
		serial = args[0];

		if (size > 1)
			count = args[1].toInt();
	}

	emit g_CommandManager.CommandDropHere(serial, count);
}
//----------------------------------------------------------------------------------
HCOMMAND(moveitem)
{
	OAFUN_DEBUG("c25_f35");
	Q_UNUSED(command);

	int size = (int)args.size();

	QString serial = 0;
	QString container = "backpack";
	int count = 0;
	ushort x = 0xFFFF;
	ushort y = 0xFFFF;
	char z = -1;

	g_MoveItemData.Reset();
	g_MoveItemData.Container = g_Backpack;

	if (size)
	{
		serial = args[0];

		if (size > 1)
		{
			count = args[1].toInt();

			if (size > 2)
			{
				container = args[2];

				if (size > 4)
				{
					x = args[3].toShort();
					y = args[4].toShort();

					if (size > 5)
						z = (char)args[5].toShort();
				}
			}
		}
	}

	emit g_CommandManager.CommandMoveItem(serial, count, container, x, y, z);
}
//----------------------------------------------------------------------------------
HCOMMAND(setarm)
{
	OAFUN_DEBUG("c25_f36");
	Q_UNUSED(command);

	if (!args.size())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	emit g_CommandManager.CommandSetArm(args[0]);
}
//----------------------------------------------------------------------------------
HCOMMAND(unsetarm)
{
	OAFUN_DEBUG("c25_f37");
	Q_UNUSED(command);

	if (!args.size())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	emit g_CommandManager.CommandUnsetArm(args[0]);
}
//----------------------------------------------------------------------------------
HCOMMAND(setdress)
{
	OAFUN_DEBUG("c25_f38");
	Q_UNUSED(command);

	if (!args.size())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	emit g_CommandManager.CommandSetDress(args[0]);
}
//----------------------------------------------------------------------------------
HCOMMAND(unsetdress)
{
	OAFUN_DEBUG("c25_f39");
	Q_UNUSED(command);

	if (!args.size())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	emit g_CommandManager.CommandUnsetDress(args[0]);
}
//----------------------------------------------------------------------------------
HCOMMAND(savehotkeys)
{
	OAFUN_DEBUG("c25_f40");
	Q_UNUSED(command);

	int size = args.size();

	if (!size || !args[0].length())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	bool result = false;
	emit g_CommandManager.CommandSaveHotkeys(args[0], &result);
}
//----------------------------------------------------------------------------------
HCOMMAND(loadhotkeys)
{
	OAFUN_DEBUG("c25_f41");
	Q_UNUSED(command);

	int size = args.size();

	if (!size || !args[0].length())
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	bool result = false;
	emit g_CommandManager.CommandLoadHotkeys(args[0], &result);
}
//----------------------------------------------------------------------------------
HCOMMAND(track)
{
	OAFUN_DEBUG("c25_f42");
	Q_UNUSED(command);

	int size = args.size();

	if (!size)
	{
		g_OrionAssistant.ClientPrintDebugLevel1(command.GetDescription());
		return;
	}

	bool on = COrionAssistant::RawStringToBool(args[0]);

	short x = -1;
	short y = -1;

	if (!on)
	{
		x = 0;
		y = 0;
	}
	else if (size > 2)
	{
		x = args[1].toInt();
		y = args[2].toInt();
	}

	emit g_CommandManager.CommandTrack(on, x, y);
}
//----------------------------------------------------------------------------------
HCOMMAND(infomenu)
{
	OAFUN_DEBUG("c25_f43");
	Q_UNUSED(args);
	Q_UNUSED(command);

	emit g_CommandManager.CommandInfoMenu("lastmenu");
}
//----------------------------------------------------------------------------------
HCOMMAND(showjournal)
{
	OAFUN_DEBUG("c25_f44");
	Q_UNUSED(command);

	int lines = -1;

	if (args.size())
		lines = args[0].toInt();

	emit g_CommandManager.CommandShowJournal(lines);
}
//----------------------------------------------------------------------------------
HCOMMAND(clearjournal)
{
	OAFUN_DEBUG("c25_f45");
	Q_UNUSED(command);

	QString pattern = "";
	QString serial = "0xFFFFFFFF";
	ushort color = 0xFFFF;
	QString type = "";

	int size = args.size();

	if (size)
	{
		pattern = args[0];

		if (size > 1)
		{
			serial = args[1];

			if (size > 2)
			{
				color = COrionAssistant::TextToGraphic(args[2]);

				if (size > 3)
					type = args[3];
			}
		}
	}

	emit g_CommandManager.CommandClearJournal(pattern, serial, color, type);
}
//----------------------------------------------------------------------------------
HCOMMAND(journalignorecase)
{
	OAFUN_DEBUG("c25_f46");
	Q_UNUSED(command);

	bool ignoreCase = false;

	if (args.size())
		ignoreCase = COrionAssistant::RawStringToBool(args[0]);

	emit g_CommandManager.CommandJournalIgnoreCase(ignoreCase);
}
//----------------------------------------------------------------------------------
HCOMMAND(ignorereset)
{
	OAFUN_DEBUG("c25_f47");
	Q_UNUSED(args);
	Q_UNUSED(command);

	emit g_CommandManager.CommandIgnoreReset();
}
//----------------------------------------------------------------------------------
HCOMMAND(infogump)
{
	OAFUN_DEBUG("c25_f48");
	Q_UNUSED(args);
	Q_UNUSED(command);

	emit g_CommandManager.CommandInfoGump("lastgump");
}
//----------------------------------------------------------------------------------
