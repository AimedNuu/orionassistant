/***********************************************************************************
**
** PartyManager.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef PARTYMANAGER_H
#define PARTYMANAGER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../Managers/DataReader.h"
//----------------------------------------------------------------------------------
struct PARTY_MEMBER_INFO
{
	uint Serial{ 0 };
	int X{ 0 };
	int Y{ 0 };
	uint Map{ 0 };
	QString Name{ "" };
};
//----------------------------------------------------------------------------------
class CPartyManager
{
	//Серийник лидера пати
	SETGET(uint, Leader, 0)
	//Серийник пригласившего в пати
	SETGET(uint, Inviter, 0)
	//Может ли группа лутать труп игрока
	SETGET(bool, CanLoot, false)

public:
	CPartyManager();

	//Члены группы
	uint Member[10];
	PARTY_MEMBER_INFO m_MemberInfo[10];

	//Обработка пакетов пати
	bool OnPacketReceived(CDataReader &reader);

	//Содержит ли пати игрока с данным серийником
	bool Contains(const uint &serial);

	void SaveMemberInfo(class CGameCharacter *obj);

	void SetMemberInfo(const PARTY_MEMBER_INFO &info);

	//Очистить пати
	void Clear();
};
//----------------------------------------------------------------------------------
extern CPartyManager g_PartyManager;
//----------------------------------------------------------------------------------
#endif // PARTYMANAGER_H
//----------------------------------------------------------------------------------
