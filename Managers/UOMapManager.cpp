// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** UOMapManager.cpp
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "UOMapManager.h"
#include "../OrionAssistant/orionassistant.h"
#include "../GameObjects/GameWorld.h"
#include "../GameObjects/GamePlayer.h"

CUOMapManager g_UOMapManager;
int UOAnotifyCommandN = WM_USER + 500;

//http://tiyuki.tank.jp/uotool/uoa.html
//----------------------------------------------------------------------------------
LRESULT CALLBACK UOAMMessageQueue(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	return g_UOMapManager.MessageQueue(hwnd, message, wParam, lParam);
}
//----------------------------------------------------------------------------------
LRESULT CALLBACK CUOMapManager::MessageQueue(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	OAFUN_DEBUG("c24_f1");
	switch (message)
	{
		case WM_CREATE:
		{
			return 0;
		}
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
		case UOAMM_REGISTER:
		{
			if (m_UOAMHandles.removeOne((HWND)wParam))
			{
				LOG("UOA: unhandled hwnd 0x%08X\n", wParam);

				return 2;
			}

			m_UOAMHandles.push_back((HWND)wParam);
			LOG("UOA: handled  0x%X\n", (HWND)wParam);

			if (lParam == 1 && g_World != nullptr)
			{
				QFOR(obj, g_World->m_Items, CGameObject*)
				{
					if (obj->GetNPC() || !((CGameItem*)obj)->GetMultiBody())
						continue;

					uint coords = (((obj->GetY() << 16) & 0xFFFF0000) | (obj->GetX() & 0xFFFF));
					PostMessage((HWND)wParam, UOAMM_ADD_MULTI, coords, obj->GetGraphic());
				}
			}

			return 1;
		}
		case UOAMM_GET_COORDINATES:
		{
			if (g_Player == nullptr)
				return 0;

			return (((g_Player->GetY() << 16) & 0xFFFF0000) | (g_Player->GetX() & 0xFFFF));
		}
		case UOAMM_GET_STAT:
		{
			if (g_Player != nullptr)
			{
				switch (wParam)
				{
					case 0:
						return g_Player->GetHits();
					case 1:
						return g_Player->GetMana();
					case 2:
						return g_Player->GetStam();
					case 3:
						return g_Player->GetWeight();
					case 4:
						return g_Player->GetMaxHits();
					case 5:
						return g_Player->GetTithingPoints();
					default:
						break;
				}
			}

			return 0;
		}
		case UOAMM_DISPLAY_TEXT:
		{
			char buf[1023] = { 0 };
			GlobalGetAtomNameA((ATOM)lParam, &buf[0], 1022);
			GlobalDeleteAtom((ATOM)lParam);

			ushort color = LOWORD(wParam);

			if (HIWORD(wParam))
				g_OrionAssistant.ClientPrint(buf, color);
			else
				g_OrionAssistant.ClientCharPrint(g_PlayerSerial, buf, color);

			return 1;
		}
		case UOAMM_MULTI_INFO:
		{
			if (g_Player != nullptr)
			{
				UOAMhousing();
				return 1;
			}

			return 0;
		}
		case UOAMM_ADD_COMMAND:
		{
			char buf[1023] = { 0 };
			GlobalGetAtomNameA((ATOM)lParam, buf, 1022);
			GlobalDeleteAtom((ATOM)lParam);

			UOAnotifyCommandS cmd = { (HWND)wParam, (UINT)UOAnotifyCommandN, buf };

			UOAnotifyCommandN++;
			UOAnotifyCommand.push_back(cmd);

			LOG("UOA: registered -%s\n", buf);

			return cmd.Message;
		}
		case UOAMM_GET_PLAYER_SERIAL:
		case UOAMM_GET_ACCOUNT_ID:
			return g_PlayerSerial;
		case UOAMM_GET_SHARD_NAME:
		{
			if (g_ServerName.length())
				return GlobalAddAtom(g_ServerName.toStdWString().c_str());

			return 0;
		}
		case UOAMM_ADD_USER_TO_PARTY:
			return 1;
		case UOAMM_GET_UO_WINDOW_HANDLE:
			return (LRESULT)g_ClientHandle;
		case UOAMM_GET_POISON_STATUS:
		{
			if (g_Player != nullptr)
				return g_Player->Poisoned();

			return 0;
		}
		default:
		{
			if (message >= WM_USER)
			{
				LOG("UOA: WM_USER+%i\n", message - WM_USER);
			}

			break;
		}
	}

	return DefWindowProc(hwnd, message, wParam, lParam);
}
//----------------------------------------------------------------------------------
CUOMapManager::CUOMapManager()
{
	WNDCLASSEX wcex;
	wcex.cbSize			= sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)UOAMMessageQueue;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= NULL;
	wcex.hIcon			= NULL;
	wcex.hCursor		= NULL;
	wcex.hbrBackground	= NULL;
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= L"UOASSIST-TP-MSG-WND";
	wcex.hIconSm		= NULL;

	if (!RegisterClassEx(&wcex))
	{
		LOG("Warning!!! UOATP can not be registered!");
	}
	else
	{
		m_Handle = CreateWindow(L"UOASSIST-TP-MSG-WND", L"UOASSIST-TP-MSG-WND", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, NULL, NULL);

		if (!m_Handle)
		{
			LOG("Warning!!! UOATP can not be created!");
		}
	}
}
//----------------------------------------------------------------------------------
CUOMapManager::~CUOMapManager()
{
	if (m_Handle)
	{
		DestroyWindow(m_Handle);
		m_Handle = 0;
	}
}
//----------------------------------------------------------------------------------
void CUOMapManager::PostMessageSend(const UINT &message, const WPARAM &wParam, const LPARAM &lParam)
{
	OAFUN_DEBUG("c24_f2");

	if (!m_UOAMHandles.empty())
	{
		for (const HWND &hwnd : m_UOAMHandles)
			PostMessage(hwnd, message, wParam, lParam);
	}
}
//----------------------------------------------------------------------------------
void CUOMapManager::UOAMnotifyLand()
{
	OAFUN_DEBUG("c24_f3");
	PostMessageSend(UOAMM_FACET_OR_LAND_INFO, g_CurrentMap, 0);
}
//----------------------------------------------------------------------------------
void CUOMapManager::UOAMnotifyHouse(const ushort &x, const ushort &y, const ushort &type, const bool &destroyed)
{
	OAFUN_DEBUG("c24_f4");
	WPARAM coord = ((y << 16) & 0xFFFF0000) | (x & 0xFFFF);

	PostMessageSend((destroyed ? UOAMM_DELETE_MULTI : UOAMM_ADD_MULTI), coord, type);
}
//----------------------------------------------------------------------------------
bool CUOMapManager::UOAnotifyCmd(const char *command)
{
	OAFUN_DEBUG("c24_f5");
	LOG("seeking command [%s]: ", command);

	IFOR(n, 0, (int)UOAnotifyCommand.size())
	{
		int l = UOAnotifyCommand[n].Command.length();

		if (strncmp(UOAnotifyCommand[n].Command.c_str(), command, l))
		{
			LOG("NOT [%s] ", UOAnotifyCommand[n].Command.c_str());
			continue;
		}
		if (command[l] && command[l] != ' ')
		{
			LOG("not [%s] ", UOAnotifyCommand[n].Command.c_str());
			continue;
		}

		ATOM a = 0;
		if (command[l] == ' ')
			a = GlobalAddAtomA(command + l + 1);

		PostMessage(UOAnotifyCommand[n].Handle, UOAnotifyCommand[n].Message, a, 0);
		LOG("IS [%s]!\n", UOAnotifyCommand[n].Command.c_str());
		return true;
	}

	LOG("finished\n");

	return false;
}
//----------------------------------------------------------------------------------
void CUOMapManager::UOAMEnterWorld()
{
	OAFUN_DEBUG("c24_f6");
	PostMessageSend(UOAMM_LOGON, g_PlayerSerial, 0);
}
//----------------------------------------------------------------------------------
void CUOMapManager::UOAMnotifyTrack(const bool &enabled, const int &x, const int &y)
{
	OAFUN_DEBUG("c24_f7");
	if (!enabled)
	{
		UOAnotifyCmd("unmark");
		return;
	}

	HWND hwnd = FindWindow(L"CUOAMWindow", NULL);

	if (hwnd != NULL)
		SendMessage(hwnd, UOAMM_LOGON, 0, ((y << 16) & 0xFFFF0000) | (x & 0xFFFF));
}
//----------------------------------------------------------------------------------
void CUOMapManager::UOAMhousing()
{
	OAFUN_DEBUG("c24_f8");
	if (g_World != nullptr)
	{
		QFOR(obj, g_World->m_Items, CGameObject*)
		{
			if (obj->GetNPC() || !((CGameItem*)obj)->GetMultiBody())
				continue;

			UOAMnotifyHouse(obj->GetX(), obj->GetY(), obj->GetGraphic());
		}
	}
}
//----------------------------------------------------------------------------------
void CUOMapManager::UpdateHits()
{
	OAFUN_DEBUG("c24_f9");
	if (g_Player != nullptr)
		PostMessageSend(UOAMM_GET_HP_AND_MAXHP_VALUE, g_Player->GetMaxHits(), g_Player->GetHits());
}
//----------------------------------------------------------------------------------
void CUOMapManager::UpdateMana()
{
	OAFUN_DEBUG("c24_f10");
	if (g_Player != nullptr)
		PostMessageSend(UOAMM_GET_INT_AND_MANA_VALUE, g_Player->GetMaxMana(), g_Player->GetMana());
}
//----------------------------------------------------------------------------------
void CUOMapManager::UpdateStam()
{
	OAFUN_DEBUG("c24_f11");
	if (g_Player != nullptr)
		PostMessageSend(UOAMM_GET_DEX_AND_STAM_VALUE, g_Player->GetMaxStam(), g_Player->GetStam());
}
//----------------------------------------------------------------------------------
