// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ClientMacroManager.cpp
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "clientmacromanager.h"
//----------------------------------------------------------------------------------
QList<CClientMacro> g_ClientMacro;
//----------------------------------------------------------------------------------
CClientMacro::CClientMacro(const QString &name)
: m_Name(name)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
