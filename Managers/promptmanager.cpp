// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** PromptManager.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "promptmanager.h"
#include "../GameObjects/GameWorld.h"
#include "../OrionAssistant/Packets.h"
#include "../OrionAssistant/tabmacros.h"
//----------------------------------------------------------------------------------
CPromptManager g_PromptManager;
//----------------------------------------------------------------------------------
CPromptManager::CPromptManager()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
void CPromptManager::AddHook(const CPrompt &prompt)
{
	OAFUN_DEBUG("");
	m_Hooks.push_back(prompt);
}
//----------------------------------------------------------------------------------
void CPromptManager::ClearHooks()
{
	OAFUN_DEBUG("");
	m_Hooks.clear();
}
//----------------------------------------------------------------------------------
bool CPromptManager::OnPacketReceived(CDataReader &reader)
{
	OAFUN_DEBUG("");
	if (g_World == nullptr)
		return true;

	m_IsUnicode = (*reader.GetStart() == 0xC2);
	m_Serial = reader.ReadUInt32BE();
	m_ID = reader.ReadUInt32BE();
	m_Exists = true;

	if (/*!m_Hooks.empty() &&*/ g_TabMacros->GetPlaying() && g_TabMacros->PlayWaiting(QList<MACRO_TYPE>() << MT_WAIT_FOR_PROMPT))
		return false;

	if (!m_Hooks.empty())
	{
		const CPrompt &prompt = m_Hooks.first();

		if (prompt.GetType() != PT_NONE)
		{
			if (!m_IsUnicode && prompt.GetType() == PT_UNICODE)
				return true;
			else if (m_IsUnicode && prompt.GetType() == PT_ASCII)
				return true;
		}

		if (prompt.GetSerial() && prompt.GetSerial() != m_Serial)
			return true;

		Send(prompt.GetText());

		m_Hooks.pop_front();

		return false;
	}

	return true;
}
//----------------------------------------------------------------------------------
bool CPromptManager::OnPacketSend(CDataReader &reader)
{
	if (m_Exists)
	{
		m_Exists = false;

		if (g_World != nullptr && g_TabMacros->GetRecording())
		{
			int length = reader.GetSize() - 15;
			reader.Move(8);
			uint response = reader.ReadUInt32BE();
			QString text = "";

			if (response)
			{
				if (*reader.GetStart() == 0xC2)
				{
					reader.Move(4);
					length = (length - 4) / 2;

					text = QString::fromStdWString(reader.ReadWString(length));
				}
				else
					text = QString::fromStdString(reader.ReadString(length));
			}

			g_TabMacros->AddAction(new CMacroWithStringValue(MT_WAIT_FOR_PROMPT, text), false);
		}

		return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
void CPromptManager::Send(QString text)
{
	if (m_Exists)
	{
		bool cancel = (!text.length() || text.toLower() == "cancel");

		if (cancel)
			text = "";

		if (m_IsUnicode)
			CPacketUnicodePromptResponse(m_Serial, m_ID, text.toStdWString(), "ENU", cancel).SendServer();
		else
			CPacketASCIIPromptResponse(m_Serial, m_ID, text.toStdString(), cancel).SendServer();

		m_Exists = false;
	}
}
//----------------------------------------------------------------------------------
