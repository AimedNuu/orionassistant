﻿/***********************************************************************************
**
** OrionAssistant_Global.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef ORIONASSISTANT_GLOBAL_H
#define ORIONASSISTANT_GLOBAL_H
//----------------------------------------------------------------------------------
#include <QtCore/qglobal.h>
#include <QDebug>
#include <QThread>
#include <QMessageBox>
#include <QCoreApplication>
#include <QTime>
#include <QTimer>
#include <QMutex>
#include <QListWidget>
#include <QListWidgetItem>
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
#include <QMenu>
#include <QMainWindow>
#include <QPushButton>
#include <QTableWidget>
#include <QTableWidgetItem>

#include <string>
using std::string;
using std::wstring;

#include <vector>
using std::vector;

#define UO_ABYSS_SHARD 1
//----------------------------------------------------------------------------------
typedef unsigned __int64 uint64;
typedef uchar *puchar;
typedef ushort *pushort;
typedef uint *puint;
typedef uint64 *puint64;
typedef char *pchar;
typedef short *pshort;
typedef int *pint;
typedef __int64 *pint64;
//----------------------------------------------------------------------------------
typedef vector<uchar> UCHAR_LIST;
typedef vector<ushort> USHORT_LIST;
typedef vector<uint> UINT_LIST;
typedef vector<string> STRING_LIST;
//----------------------------------------------------------------------------------
#ifndef Q_CC_MSVC
#pragma GCC diagnostic ignored "-Wwrite-strings"
#pragma GCC diagnostic ignored "-Wmultichar"
#endif
//----------------------------------------------------------------------------------
#include "EnumList.h"
#include "CommonInterfaces.h"
#include "PluginInterface.h"
#include "MulStruct.h"
#include "orionassistantlogger.h"
//----------------------------------------------------------------------------------
extern PACKET_PROC *g_SendClient;
extern PACKET_PROC *g_SendServer;
extern FUNCDEF_GET_STATIC_FLAGS *g_GetStaticFlags;
extern FUNCDEF_GET_VALUE_INT *g_GetInt;
extern FUNCDEF_SET_VALUE_INT *g_SetInt;
extern FUNCDEF_GET_VALUE_STRING *g_GetString;
extern FUNCDEF_SET_VALUE_STRING *g_SetString;
//----------------------------------------------------------------------------------
int GetInt(const VALUE_KEY_INT &key, const int &value = -1);
QString GetString(const VALUE_KEY_STRING &key, const QString &value = "");
//----------------------------------------------------------------------------------
enum OA_ENABLED_FEATURES
{
	OAEF_CORPSES_AUTOOPEN			= 0x0000000000000001,
	OAEF_STEALTH_STEP_COUNTER		= 0x0000000000000002,
	OAEF_SOUND_FILTER				= 0x0000000000000004,
	OAEF_LIGHT_FILTER				= 0x0000000000000008,
	OAEF_WEATHER_FILTER				= 0x0000000000000010,
	OAEF_SEASON_FILTER				= 0x0000000000000020,
	OAEF_NO_DEATH_SCREEN			= 0x0000000000000040, //black screen ~5 sec
	OAEF_AUTO_OPEN_DOORS			= 0x0000000000000080,
	OAEF_TRACKER					= 0x0000000000000100,
	OAEF_LOOPED_MACRO				= 0x0000000000000200,
	OAEF_EXTENDED_SCRIPTS			= 0x0000000000000400,
	OAEF_FAST_ROTATION				= 0x0000000000000800,
	OAEF_RECURSE_CONTAINERS_SEARCH	= 0x0000000000001000,
	OAEF_AUTOSTART					= 0x0000000000002000,
	OAEF_IGNORE_STAMINA_CHECK		= 0x0000000000004000,
	OAEF_PARTY_AGENT				= 0x0000000000008000,
	OAEF_SHOP_AGENT					= 0x0000000000010000,
	OAEF_MINIMIZE_TO_TRAY			= 0x0000000000020000,
	OAEF_SOUND_ECHO					= 0x0000000000040000,
	OAEF_ANIMATION_ECHO				= 0x0000000000080000,
	OAEF_EFFECT_ECHO				= 0x0000000000100000,
	OAEF_MAP_ECHO					= 0x0000000000200000,
	OAEF_SPEECH_FILTER				= 0x0000000000400000,
	OAEF_TEXT_REPLACE				= 0x0000000000800000,
	OAEF_RECONNECTOR				= 0x0000000001000000,

	OAEF_ALL						= 0xFFFFFFFFFFFFFFFF,

	OAEF_DEFAULT					= OAEF_ALL
};
//----------------------------------------------------------------------------------
extern uint64 g_EnabledFeature;
//----------------------------------------------------------------------------------
inline bool EnabledFeatureCorpsesAutoopen() {return (g_EnabledFeature & OAEF_CORPSES_AUTOOPEN);}
inline bool EnabledFeatureStealthStepCounter() {return (g_EnabledFeature & OAEF_STEALTH_STEP_COUNTER);}
inline bool EnabledFeatureSoundFilter() {return (g_EnabledFeature & OAEF_SOUND_FILTER);}
inline bool EnabledFeatureLightFilter() {return (g_EnabledFeature & OAEF_LIGHT_FILTER);}
inline bool EnabledFeatureWeatherFilter() {return (g_EnabledFeature & OAEF_WEATHER_FILTER);}
inline bool EnabledFeatureSeasonFilter() {return (g_EnabledFeature & OAEF_SEASON_FILTER);}
inline bool EnabledFeatureNoDeathScreen() {return (g_EnabledFeature & OAEF_NO_DEATH_SCREEN);}
inline bool EnabledFeatureAutoOpenDoors() {return (g_EnabledFeature & OAEF_AUTO_OPEN_DOORS);}
inline bool EnabledFeatureTracker() {return (g_EnabledFeature & OAEF_TRACKER);}
inline bool EnabledFeatureLoopedMacro() {return (g_EnabledFeature & OAEF_LOOPED_MACRO);}
inline bool EnabledFeatureExtendedScripts() {return (g_EnabledFeature & OAEF_EXTENDED_SCRIPTS);}
inline bool EnabledFeatureFastRotation() {return (g_EnabledFeature & OAEF_FAST_ROTATION);}
inline bool EnabledFeatureRecurseContainersSearch() {return (g_EnabledFeature & OAEF_RECURSE_CONTAINERS_SEARCH);}
inline bool EnabledFeatureAutostart() {return (g_EnabledFeature & OAEF_AUTOSTART);}
inline bool EnabledFeatureIgnoreStaminaCheck() {return (g_EnabledFeature & OAEF_IGNORE_STAMINA_CHECK);}
inline bool EnabledFeaturePartyAgent() {return (g_EnabledFeature & OAEF_PARTY_AGENT);}
inline bool EnabledFeatureShopAgent() {return (g_EnabledFeature & OAEF_SHOP_AGENT);}
inline bool EnabledFeatureMinimizeToTray() {return (g_EnabledFeature & OAEF_MINIMIZE_TO_TRAY);}
inline bool EnabledFeatureSoundEcho() {return (g_EnabledFeature & OAEF_SOUND_ECHO);}
inline bool EnabledFeatureAnimationEcho() {return (g_EnabledFeature & OAEF_ANIMATION_ECHO);}
inline bool EnabledFeatureEffectEcho() {return (g_EnabledFeature & OAEF_EFFECT_ECHO);}
inline bool EnabledFeatureMapEcho() {return (g_EnabledFeature & OAEF_MAP_ECHO);}
inline bool EnabledFeatureSpeechFilter() {return (g_EnabledFeature & OAEF_SPEECH_FILTER);}
inline bool EnabledFeatureTextReplace() {return (g_EnabledFeature & OAEF_TEXT_REPLACE);}
inline bool EnabledFeatureReconnector() {return (g_EnabledFeature & OAEF_RECONNECTOR);}
//----------------------------------------------------------------------------------
enum OA_ENABLED_COMMANDS : uint64
{
	OAEC_INFORMATION			= 0x0000000000000001,
	OAEC_TEXT_WINDOW			= 0x0000000000000002,
	OAEC_SEARCH_CONTAINER		= 0x0000000000000004,
	OAEC_SEARCH_GROUND			= 0x0000000000000008,
	OAEC_MOVING					= 0x0000000000000010,
	OAEC_RESEND					= 0x0000000000000020,
	OAEC_JOURNAL				= 0x0000000000000040,
	OAEC_RENAME_MOUNTS			= 0x0000000000000080,
	OAEC_SHOP_BUY				= 0x0000000000000100,
	OAEC_SHOP_SELL				= 0x0000000000000200,
	OAEC_HIDE					= 0x0000000000000400,
	OAEC_MOVE_ITEMS				= 0x0000000000000800,
	OAEC_MORPHING				= 0x0000000000001000,
	OAEC_EMOTE_ACTIONS			= 0x0000000000002000,
	OAEC_FIND_TYPE_OR_IGNORE	= 0x0000000000004000,
	OAEC_FIND_ARRAYS			= 0x0000000000008000,
	OAEC_OPTIONS_ACCESS			= 0x0000000000010000,
	OAEC_SEARCH_CORPSE			= 0x0000000000020000,
	OAEC_EXTENDED_MENU			= 0x0000000000040000,
	OAEC_BUFFS					= 0x0000000000080000,
	OAEC_SECURE_TRADING			= 0x0000000000100000,
	OAEC_FRIEND_LIST			= 0x0000000000200000,
	OAEC_ENEMY_LIST				= 0x0000000000400000,
	OAEC_FONT_COLOR				= 0x0000000000800000,
	OAEC_EXTENDED_TARGET_TILE	= 0x0000000001000000,
	OAEC_LAUNCH					= 0x0000000002000000,
	OAEC_REMOVED_PTR_POSITION	= 0x0000000004000000,
	OAEC_EXTENDED_GUMP			= 0x0000000008000000,
	OAEC_CHARACTER_PROPERTIES	= 0x0000000010000000,
	OAEC_CHARACTER_PROFILE		= 0x0000000020000000,
	OAEC_PROMPTS				= 0x0000000040000000,
	OAEC_STATUSBARS				= 0x0000000080000000,
	OAEC_LOG_OUT				= 0x0000000100000000,
	OAEC_TIMERS					= 0x0000000200000000,
	OAEC_CONTEXT_MENU			= 0x0000000400000000,

	OAEC_ALL					= 0xFFFFFFFFFFFFFFFF,

	OAEC_DEFAULT				= OAEC_ALL
};
//----------------------------------------------------------------------------------
extern uint64 g_EnabledCommands;
//----------------------------------------------------------------------------------
inline bool EnabledCommandInformation() {return (g_EnabledCommands & OAEC_INFORMATION);}
inline bool EnabledCommandTextWindow() {return (g_EnabledCommands & OAEC_TEXT_WINDOW);}
inline bool EnabledCommandSearchContainers() {return (g_EnabledCommands & OAEC_SEARCH_CONTAINER);}
inline bool EnabledCommandSearchGround() {return (g_EnabledCommands & OAEC_SEARCH_GROUND);}
inline bool EnabledCommandMoving() {return (g_EnabledCommands & OAEC_MOVING);}
inline bool EnabledCommandResend() {return (g_EnabledCommands & OAEC_RESEND);}
inline bool EnabledCommandJournal() {return (g_EnabledCommands & OAEC_JOURNAL);}
inline bool EnabledCommandRenameMounts() {return (g_EnabledCommands & OAEC_RENAME_MOUNTS);}
inline bool EnabledCommandShopBuy() {return (g_EnabledCommands & OAEC_SHOP_BUY);}
inline bool EnabledCommandShopSell() {return (g_EnabledCommands & OAEC_SHOP_SELL);}
inline bool EnabledCommandHide() {return (g_EnabledCommands & OAEC_HIDE);}
inline bool EnabledCommandMoveItems() {return (g_EnabledCommands & OAEC_MOVE_ITEMS);}
inline bool EnabledCommandMorphing() {return (g_EnabledCommands & OAEC_MORPHING);}
inline bool EnabledCommandEmoteActions() {return (g_EnabledCommands & OAEC_EMOTE_ACTIONS);}
inline bool EnabledCommandFindTypeOrIgnore() {return (g_EnabledCommands & OAEC_FIND_TYPE_OR_IGNORE);}
inline bool EnabledCommandFindArrays() {return (g_EnabledCommands & OAEC_FIND_ARRAYS);}
inline bool EnabledCommandOptionsAccess() {return (g_EnabledCommands & OAEC_OPTIONS_ACCESS);}
inline bool EnabledCommandSearchCorpse() {return (g_EnabledCommands & OAEC_SEARCH_CORPSE);}
inline bool EnabledCommandExtendedMenu() {return (g_EnabledCommands & OAEC_EXTENDED_MENU);}
inline bool EnabledCommandBuffs() {return (g_EnabledCommands & OAEC_BUFFS);}
inline bool EnabledCommandSecureTrading() {return (g_EnabledCommands & OAEC_SECURE_TRADING);}
inline bool EnabledCommandFriendList() {return (g_EnabledCommands & OAEC_FRIEND_LIST);}
inline bool EnabledCommandEnemyList() {return (g_EnabledCommands & OAEC_ENEMY_LIST);}
inline bool EnabledCommandFontColor() {return (g_EnabledCommands & OAEC_FONT_COLOR);}
inline bool EnabledCommandExtendedTargetTile() {return (g_EnabledCommands & OAEC_EXTENDED_TARGET_TILE);}
inline bool EnabledCommandLaunch() {return (g_EnabledCommands & OAEC_LAUNCH);}
inline bool EnabledCommandRemovedPtrPosition() {return (g_EnabledCommands & OAEC_REMOVED_PTR_POSITION);}
inline bool EnabledCommandExtendedGump() {return (g_EnabledCommands & OAEC_EXTENDED_GUMP);}
inline bool EnabledCommandCharacterProperties() {return (g_EnabledCommands & OAEC_CHARACTER_PROPERTIES);}
inline bool EnabledCommandCharacterProfile() {return (g_EnabledCommands & OAEC_CHARACTER_PROFILE);}
inline bool EnabledCommandPrompts() {return (g_EnabledCommands & OAEC_PROMPTS);}
inline bool EnabledCommandStatusbars() {return (g_EnabledCommands & OAEC_STATUSBARS);}
inline bool EnabledCommandLogOut() {return (g_EnabledCommands & OAEC_LOG_OUT);}
inline bool EnabledCommandTimers() {return (g_EnabledCommands & OAEC_TIMERS);}
inline bool EnabledCommandContextMenu() {return (g_EnabledCommands & OAEC_CONTEXT_MENU);}
//----------------------------------------------------------------------------------
enum ORION_INTERNAL_PACKET_MESSAGE_TYPE
{
	OIPMT_FILES_TRANSFERED = 1,
	OIPMT_FILE_INFO = 2,
	OIPMT_FILE_INFO_LOCALIZED = 3,
	OIPMT_GRAPHIC_DATA_INFO = 4,
	OIPMT_SKILL_LIST = 100,
	OIPMT_SPELL_LIST = 101,
	OIPMT_MACRO_LIST = 102,
	OIPMT_OPEN_MAP = 103
};
//----------------------------------------------------------------------------------
enum ORION_FILE_INDEX
{
	OFI_MAP_0_MUL = 1,
	OFI_MAP_1_MUL,
	OFI_MAP_2_MUL,
	OFI_MAP_3_MUL,
	OFI_MAP_4_MUL,
	OFI_MAP_5_MUL,
	OFI_MAP_0_UOP,
	OFI_MAP_1_UOP,
	OFI_MAP_2_UOP,
	OFI_MAP_3_UOP,
	OFI_MAP_4_UOP,
	OFI_MAP_5_UOP,
	OFI_MAPX_0_UOP,
	OFI_MAPX_1_UOP,
	OFI_MAPX_2_UOP,
	OFI_MAPX_3_UOP,
	OFI_MAPX_4_UOP,
	OFI_MAPX_5_UOP,
	OFI_STAIDX_0_MUL,
	OFI_STAIDX_1_MUL,
	OFI_STAIDX_2_MUL,
	OFI_STAIDX_3_MUL,
	OFI_STAIDX_4_MUL,
	OFI_STAIDX_5_MUL,
	OFI_STATICS_0_MUL,
	OFI_STATICS_1_MUL,
	OFI_STATICS_2_MUL,
	OFI_STATICS_3_MUL,
	OFI_STATICS_4_MUL,
	OFI_STATICS_5_MUL,
	OFI_MAP_DIF_0_MUL,
	OFI_MAP_DIF_1_MUL,
	OFI_MAP_DIF_2_MUL,
	OFI_MAP_DIF_3_MUL,
	OFI_MAP_DIF_4_MUL,
	OFI_MAP_DIF_5_MUL,
	OFI_MAP_DIFL_0_MUL,
	OFI_MAP_DIFL_1_MUL,
	OFI_MAP_DIFL_2_MUL,
	OFI_MAP_DIFL_3_MUL,
	OFI_MAP_DIFL_4_MUL,
	OFI_MAP_DIFL_5_MUL,
	OFI_STA_DIF_0_MUL,
	OFI_STA_DIF_1_MUL,
	OFI_STA_DIF_2_MUL,
	OFI_STA_DIF_3_MUL,
	OFI_STA_DIF_4_MUL,
	OFI_STA_DIF_5_MUL,
	OFI_STA_DIFI_0_MUL,
	OFI_STA_DIFI_1_MUL,
	OFI_STA_DIFI_2_MUL,
	OFI_STA_DIFI_3_MUL,
	OFI_STA_DIFI_4_MUL,
	OFI_STA_DIFI_5_MUL,
	OFI_STA_DIFL_0_MUL,
	OFI_STA_DIFL_1_MUL,
	OFI_STA_DIFL_2_MUL,
	OFI_STA_DIFL_3_MUL,
	OFI_STA_DIFL_4_MUL,
	OFI_STA_DIFL_5_MUL,
	OFI_TILEDATA_MUL,
	OFI_MULTI_IDX,
	OFI_MULTI_MUL,
	OFI_MULTI_UOP,
	OFI_HUES_MUL,
	OFI_VERDATA_MUL,
	OFI_CLILOC_MUL,
	OFI_RADARCOL_MUL,
	OFI_FILES_COUNT
};
//----------------------------------------------------------------------------------
enum ORION_GRAPHIC_DATA_TYPE
{
	OGDT_STATIC_ART = 1,
	OGDT_GUMP_ART
};
//----------------------------------------------------------------------------------
enum OA_PACKET_MESSAGE_TYPE
{
	OAPMT_SET_FLAGS = 0x0001
};
//---------------------------------------------------------------------------
enum TRADE_ACTIONS
{
	TA_OPEN = 0,
	TA_CLOSE,
	TA_CHECK
};
//----------------------------------------------------------------------------------
enum THIRD_PARTY_FEATURES
{
	TPF_FILTER_WEATHER		= 1 << 0,
	TPF_FILTER_LIGHT		= 1 << 1,

	TPF_SMART_TARGET		= 1 << 2,
	TPF_RANGED_TARGET		= 1 << 3,

	TPF_AUTOOPEN_DOORS		= 1 << 4,

	TPF_UNEQUIP_ON_CAST		= 1 << 5,
	TPF_AUTO_POTION_EQUIP	= 1 << 6,

	TPF_PROTECT_HEALS		= 1 << 7,

	TPF_LOOPED_MACROS		= 1 << 8,

	TPF_USE_ONCE_AGENT		= 1 << 9,
	TPF_RESTOCK_AGENT		= 1 << 10,
	TPF_SELL_AGENT			= 1 << 11,
	TPF_BUY_AGENT			= 1 << 12,

	TPF_POTION_HOTKEYS		= 1 << 13,

	TPF_RANDOM_TARGETS		= 1 << 14,
	TPF_CLOSEST_TARGETS		= 1 << 15, // All closest target hotkeys
	TPF_OVERHEAD_HEALTH		= 1 << 16, // Health and Mana/Stam messages shown over player's heads

	TPF_AUTOLOOT_AGENT		= 1 << 17,
	TPF_BONE_CUTTER_AGENT	= 1 << 18,
	TPF_ADVANCED_MACROS		= 1 << 19,
	TPF_AUTO_REMOUNT		= 1 << 20,
	TPF_AUTO_BANDAGE		= 1 << 21,
	TPF_ENEMY_TARGET_SHARE	= 1 << 22,
	TPF_FILTER_SEASON		= 1 << 23,
	TPF_SPELL_TARGET_SHARE	= 1 << 24,

	TPF_ALL					= 0xFFFFFFFF
};
//----------------------------------------------------------------------------------
struct SEASON_DATA
{
	uchar Index;
	uchar Music;
};
//----------------------------------------------------------------------------------
struct WEATHER_DATA
{
	uchar Type;
	uchar Count;
	uchar Temperature;
};
//----------------------------------------------------------------------------------
extern HINSTANCE g_HInstance;
extern QString g_DllPath;
extern QString g_ClientPath;
extern bool g_SaveAero;
extern PPLUGIN_INTERFACE g_ClientInterface;
extern HWND g_ClientHandle;
extern uint g_LastSendTime;
extern uint g_LastPacketTime;
extern uint g_TotalSendSize;
extern uint g_TotalRecvSize;
extern uchar g_LightLevel;
extern uchar g_PersonalLightLevel;
extern SEASON_DATA g_Season;
extern WEATHER_DATA g_Weather;
extern uint g_PlayerSerial;
extern uint g_Backpack;
extern uint g_FindItem;
extern uint g_CurrentMap;
extern QString g_ServerName;
extern QString g_CharacterName;
extern bool g_UpdateDisplayOnStep;
extern bool g_LinuxOS;
extern uint g_DressBag;
extern ushort g_MessagesColor;
extern uint g_LastTargetObject;
extern uint g_LastAttackObject;
extern ushort g_MorphGraphic;
extern ushort g_OriginalGraphic;
extern uint g_LastCorpseObject;
extern uint g_LastContainerObject;
extern QTime g_LastUseObjectTimer;
extern QTime g_StartTimer;
extern bool g_Connected;
extern uint g_LastFriendFound;
extern uint g_LastEnemyFound;
extern uchar g_Ability[2];
extern QPoint g_LastTargetPosition;
extern QPoint g_LastAttackPosition;
extern uint g_LastStatusRequest;
extern uint g_LastStatusObject;
extern uint g_LastUseObject;
extern UOI_SELECTED_TILE g_SelectedClientWorldObject;
extern bool g_ClientMacroPlayed;
extern QPoint g_LastPlayerCorpsePosition;
extern uint g_LastPlayerDeadTimer;
extern int g_LastSkillIndex;
extern int g_LastSpellIndex;
extern bool g_TargetReceived;
extern bool g_MenuReceived;
extern bool g_GumpReceived;
extern bool g_PromptReceived;
extern bool g_ContextMenuReceived;
extern bool g_TradeReceived;
extern bool g_ShopReceived;
extern ushort g_SpellReagents[64][5];
extern QPoint g_RemoveRangeXY;
//----------------------------------------------------------------------------------
static const bool g_LayerSafe[30] =
{
	false,   //0
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	false,
	true,
	true,
	true,
	false,
	false,
	true,
	true,
	true,
	true,
	false, //0x15
	true,
	true,
	true,
	false,
	false,
	false,
	false,
	false
};
//----------------------------------------------------------------------------------
const int MAX_ABILITIES_COUNT = 32;

//!Размер таблицы индексов статики из арт.мул, >=CV_7090: count=0x10000, >=CV_7000: count=0x8000, <CV_7000: count=0x4000
const int MAX_STATIC_DATA_INDEX_COUNT = 0x10000;

//!Размер таблицы индексов гампов
const int MAX_GUMP_DATA_INDEX_COUNT = 0x10000;
//----------------------------------------------------------------------------------
const QString g_AbilityName[MAX_ABILITIES_COUNT] =
{
	"Armor Ignore",
	"Bleed Attack",
	"Concussion Blow",
	"Crushing Blow",
	"Disarm",
	"Dismount",
	"Double Strike",
	"Infecting",
	"Mortal Strike",
	"Moving Shot",
	"Paralyzing Blow",
	"Shadow Strike",
	"Whirlwind Attack",
	"Riding Swipe",			//CV_500a
	"Frenzied Whirlwind",
	"Block",
	"Defense Mastery",
	"Nerve Strike",
	"Talon Strike",
	"Feint",
	"Dual Wield",
	"Double Shot",
	"Armor Pierce",
	"Bladeweave",
	"Force Arrow",
	"Lightning Arrow",
	"Psychic Attack",
	"Serpent Arrow",
	"Force of Nature",
	"Infused Throw",		//CV_7000
	"Mystic Arc",
	"Disrobe"
};
//----------------------------------------------------------------------------------
const int MAX_INVOKE_VIRTURE_COUNT = 3;
//----------------------------------------------------------------------------------
const QString g_InvokeVirtureName[MAX_INVOKE_VIRTURE_COUNT] =
{
	"Honor",
	"Sacrifice",
	"Valor"
};
//----------------------------------------------------------------------------------
class COAFunDebug
{
public:
	COAFunDebug(const char *str);
	~COAFunDebug();
};
//----------------------------------------------------------------------------------
#if defined(ORIONASSISTANT_LIBRARY)
#  define ORIONASSISTANTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ORIONASSISTANTSHARED_EXPORT Q_DECL_IMPORT
#endif
//----------------------------------------------------------------------------------
#define USE_OA_DEBUG_FUNCTION_NAMES 0

#if USE_OA_DEBUG_FUNCTION_NAMES == 1
#define OAFUN_DEBUG(name) \
	static COAFunDebug oafunctiondebugname(__PRETTY_FUNCTION__); \
	(void)oafunctiondebugname
	//COAFunDebug oafunctiondebugname("w_" __PRETTY_FUNCTION__);
	//static const std::string oafunctiondebugname("oa_" name);
	//(void)oafunctiondebugname
#elif USE_OA_DEBUG_FUNCTION_NAMES == 2
	extern const char *g_OACurrentFunctionName;
#define OAFUN_DEBUG(name) \
	g_OACurrentFunctionName = __FUNCTION__
#else
#define OAFUN_DEBUG(name)
#endif
//----------------------------------------------------------------------------------
//!Incremented ordinary for
#define IFOR(var, start, stop) for (int var = start; var < stop; var ++)
//!Decremented ordinary for
#define DFOR(var, start, stop) for (int var = start; var >= stop; var --)
//----------------------------------------------------------------------------------
#define IN_RANGE(name, id1, id2) ((name) >= (id1) && (name) <= (id2))
#define OUT_RANGE(name, id1, id2) ((name) < (id1) || (name) > (id2))
//----------------------------------------------------------------------------------
#define RELEASE_POINTER(ptr) \
if (ptr != NULL) \
{ \
	delete ptr; \
	ptr = NULL; \
}
//----------------------------------------------------------------------------------
//!Set/Get ordinary class property
#define SETGET(type, name, defaultValue) \
	protected: \
	type m_##name{ defaultValue }; \
	public: \
	inline void __fastcall Set##name(type val) { m_##name = val; } \
	inline type Get##name() const { return m_##name; }
//----------------------------------------------------------------------------------
//!Set/Get ordinary class property
#define SETGETE(type, name, defaultValue, event) \
	protected: \
	type m_##name{ defaultValue }; \
	public: \
	void event(const type &val); \
	inline void __fastcall Set##name(type val) { event(val); m_##name = val; } \
	inline type Get##name() const { return m_##name; }
//----------------------------------------------------------------------------------
//!Set/Get ordinary class property for pointers
#define SETGETP(type, name, defaultValue) \
	protected: \
	type m_##name = defaultValue; \
	public: \
	inline void __fastcall Set##name(type val) { m_##name = val; } \
	inline type Get##name() const { return m_##name; }
//----------------------------------------------------------------------------------
inline bool IsKeyDown(int vKey){return ((GetAsyncKeyState(vKey) & 0x80000000) != 0);}
//----------------------------------------------------------------------------------
inline bool IsBackground(const quint64 &flags) { return (flags & 0x00000001); }
inline bool IsWeapon(const quint64 &flags) { return (flags & 0x00000002); }
inline bool IsTransparent(const quint64 &flags) { return (flags & 0x00000004); }
inline bool IsTranslucent(const quint64 &flags) { return (flags & 0x00000008); }
inline bool IsWall(const quint64 &flags) { return (flags & 0x00000010); }
inline bool IsDamaging(const quint64 &flags) { return (flags & 0x00000020); }
inline bool IsImpassable(const quint64 &flags) { return (flags & 0x00000040); }
inline bool IsWet(const quint64 &flags) { return (flags & 0x00000080); }
inline bool IsUnknown(const quint64 &flags) { return (flags & 0x00000100); }
inline bool IsSurface(const quint64 &flags) { return (flags & 0x00000200); }
inline bool IsBridge(const quint64 &flags) { return (flags & 0x00000400); }
inline bool IsStackable(const quint64 &flags) { return (flags & 0x00000800); }
inline bool IsWindow(const quint64 &flags) { return (flags & 0x00001000); }
inline bool IsNoShoot(const quint64 &flags) { return (flags & 0x00002000); }
inline bool IsPrefixA(const quint64 &flags) { return (flags & 0x00004000); }
inline bool IsPrefixAn(const quint64 &flags) { return (flags & 0x00008000); }
inline bool IsInternal(const quint64 &flags) { return (flags & 0x00010000); }
inline bool IsFoliage(const quint64 &flags) { return (flags & 0x00020000); }
inline bool IsPartialHue(const quint64 &flags) { return (flags & 0x00040000); }
inline bool IsUnknown1(const quint64 &flags) { return (flags & 0x00080000); }
inline bool IsMap(const quint64 &flags) { return (flags & 0x00100000); }
inline bool IsContainer(const quint64 &flags) { return (flags & 0x00200000); }
inline bool IsWearable(const quint64 &flags) { return (flags & 0x00400000); }
inline bool IsLightSource(const quint64 &flags) { return (flags & 0x00800000); }
inline bool IsAnimated(const quint64 &flags) { return (flags & 0x01000000); }
inline bool IsNoDiagonal(const quint64 &flags) { return (flags & 0x02000000); }
inline bool IsUnknown2(const quint64 &flags) { return (flags & 0x04000000); }
inline bool IsArmor(const quint64 &flags) { return (flags & 0x08000000); }
inline bool IsRoof(const quint64 &flags) { return (flags & 0x10000000); }
inline bool IsDoor(const quint64 &flags) { return (flags & 0x20000000); }
inline bool IsStairBack(const quint64 &flags) { return (flags & 0x40000000); }
inline bool IsStairRight(const quint64 &flags) { return (flags & 0x80000000); }
//----------------------------------------------------------------------------------
static const QString g_EquipScriptName = "equip_script";
//----------------------------------------------------------------------------------
static const QString g_LayerName[30] =
{
	"None",   //0
	"RightHand",
	"LeftHand",
	"Shoes",
	"Pants",
	"Shirt",
	"Helmet",
	"Gloves",
	"Ring",
	"Talisman",
	"Necklace",
	"Hair",
	"Waist",
	"InnerTorso",
	"Bracelet",
	"Face",
	"Beard",
	"MidTorso",
	"Earrings",
	"Arms",
	"Cloak",
	"Backpack", //0x15
	"Robe",
	"Eggs",
	"Legs",
	"Mount",
	"Buy",
	"Resale",
	"Sell",
	"Bank"
};
//----------------------------------------------------------------------------------
inline uint unpack32(puchar buf)
{
	return (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
}
//----------------------------------------------------------------------------------
inline ushort unpack16(puchar buf)
{
	return (buf[0] << 8) | buf[1];
}
//----------------------------------------------------------------------------------
inline void pack32(puchar buf, uint x)
{
	buf[0] = uchar(x >> 24);
	buf[1] = uchar((x >> 16) & 0xff);
	buf[2] = uchar((x >> 8) & 0xff);
	buf[3] = uchar(x & 0xff);
}
//----------------------------------------------------------------------------------
inline void pack16(puchar buf, ushort x)
{
	buf[0] = x >> 8;
	buf[1] = x & 0xff;
}
//----------------------------------------------------------------------------------
#endif // ORIONASSISTANT_GLOBAL_H
//----------------------------------------------------------------------------------
