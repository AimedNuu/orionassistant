﻿/***********************************************************************************
**
** EnumList.h
**
** Copyright (C) August 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef ENUMLIST_H
#define ENUMLIST_H
//----------------------------------------------------------------------------------
#define MODKEY_ALT					0x0100
#define MODKEY_CTRL					0x0200
#define MODKEY_SHIFT				0x0400
//----------------------------------------------------------------------------------
//!Версия клиента (для изменения в протоколе и прочих няшках)
enum CLIENT_VERSION
{
	CV_OLD = 0,		//Предшествующие клиенту 2.0.0, Остальные по логике, исходя из названия
	CV_200,			//Отправляется пакет с габаритами экрана
	//CV_204C,		//Использование *.def файлов
	CV_305D,		//Использование клилоков, количество слотов в списке персонажей равно количеству персонажей
	CV_306E,		//Добавлен пакет с типом клиента (0xBF subcmd 0x0F), использование mp3 вместо midi
	CV_308D,		//Добавлен параметр Maximum Stats в статусбар
	CV_308J,		//Добавлен параметр Followers в статусбар
	CV_308Z,		//Добавлены классы paladin, necromancer; custom houses, 5 resistances, изменено окно выбора профессии, убрана галочка Save Password
	CV_400B,		//Удаление тултипов
	CV_405A,		//Добавлены классы ninja, samurai
	CV_4011D,		//Изменение окна создания персонажа. Добавлена расса elves
	CV_500A,		//Кнопки папердолла заменены: Journal -> Quests; Chat -> Guild, использование Mega Cliloc, Убрана загрузка файла Verdata.mul
	CV_5020,		//Добавлен гамп бафов
	CV_5090,		//
	CV_6000,		//Добавлены цвета гильд/алли чата, игноры чатов. Добавлены опции новой таргет системы, вывод свойств предметов, Object Handles,
	CV_6013,		//
	CV_6017,		//
	CV_6040,		//Увеличилось количество слотов персонажей
	CV_6060,		//
	CV_60142,		//
	CV_60144,		//Изменение окна создания персонажа. Добавлена расса gargoyle
	CV_7000,		//
	CV_7090,		//
	CV_70130,		//
	CV_70160,		//
	CV_70180,		//
	CV_70240,		//*.mul -> *.uop
	CV_70331		//
};
//----------------------------------------------------------------------------------
//!Рассы
enum RACE_TYPE
{
	RT_HUMAN = 1,
	RT_ELF,
	RT_GARGOYLE
};
//----------------------------------------------------------------------------------
//!Состояния ClientFlag для отправки при создании персонажа
enum CLIENT_FLAG
{
	CF_T2A = 0x00,
	CF_RE = 0x01,
	CF_TD = 0x02,
	CF_LBR = 0x04,
	CF_AOS = 0x08,
	CF_SE = 0x10,
	CF_SA = 0x20,
	CF_UO3D = 0x40,
	CF_RESERVED = 0x80,
	CF_3D = 0x100
};
//----------------------------------------------------------------------------------
//!Расширения для списка персонажей
enum CHARACTER_LIST_FLAG
{
	CLF_UNKNOWN = 0x01,
	CLF_OWERWRITE_CONFIGURATION_BUTTON = 0x02,
	CLF_ONE_CHARACTER_SLOT = 0x04,
	CLF_CONTEXT_MENU = 0x08,
	CLF_LIMIT_CHARACTER_SLOTS = 0x10,
	CLF_PALADIN_NECROMANCER_TOOLTIPS = 0x20,
	CLF_6_CHARACTER_SLOT = 0x40,
	CLF_SAMURAI_NINJA = 0x80,
	CLF_ELVEN_RACE = 0x100,
	CLF_UNKNOWN_1 = 0x200,
	CLF_UO3D = 0x400,
	CLF_UNKNOWN_2 = 0x800,
	CLF_7_CHARACTER_SLOT = 0x1000,
	CLF_UNKNOWN_3 = 0x2000,
	CLF_NEW_MOVEMENT_SYSTEM = 0x4000,
	CLF_UNLOCK_FELUCCA_AREAS = 0x8000
};
//----------------------------------------------------------------------------------
//!Состояния client features
enum LOCKED_FEATURE_FLAG
{
	LFF_T2A = 0x01,
	LFF_RE = 0x02,
	LFF_TD = 0x04,
	LFF_LBR = 0x08,
	LFF_AOS = 0x10,
	LFF_6_SLOT = 0x20,
	LFF_SE = 0x40,
	LFF_ML = 0x80,
	LFF_8_AGE = 0x100,
	LFF_9_AGE = 0x200,
	LFF_10_AGE = 0x400,
	LFF_HOUSING = 0x800,
	LFF_7_SLOT = 0x1000,
	LFF_KR = 0x2000,
	LFF_TRIAL_ACC = 0x4000,
	LFF_11_AGE = 0x8000,
	LFF_SA = 0x10000,
	LFF_HSA = 0x20000,
	LFF_GOTHIC_HOUSING = 0x40000,
	LFF_RUSTIC_HOUSING = 0x80000
};
//----------------------------------------------------------------------------------
//!Типы спеллбук
enum SPELLBOOK_TYPE
{
	ST_MAGE = 0,
	ST_NECRO,
	ST_PALADIN,
	ST_BUSHIDO = 4,
	ST_NINJITSU,
	ST_SPELL_WEAVING,
	ST_MYSTICISM
};
//----------------------------------------------------------------------------------
//!Смещение для книг заклинаний
enum SPELLBOOK_OFFSET
{
	SO_MAGE = 1,
	SO_NECRO = 101,
	SO_PALADIN = 201,
	SO_BUSHIDO = 301,
	SO_NINJITSU = 401,
	SO_SPELL_WEAVING = 501,
	SO_MYSTICISM = 601
};
//----------------------------------------------------------------------------------
//!Тип плавного переключения экрана
enum SCREEN_EFFECT_TYPE
{
	SET_TO_BLACK,
	SET_TO_WHITE,
	SET_TO_WHITE_FAST,
	SET_TO_WHITE_THEN_BLACK,
	SET_TO_BLACK_VERY_FAST
};
//----------------------------------------------------------------------------------
//!Состояния плавного переключения экранов
enum WEATHER_TYPE
{
	WT_RAIN = 0,
	WT_FIERCE_STORM,
	WT_SNOW,
	WT_STORM
};
//----------------------------------------------------------------------------------
enum MAP_MESSAGE
{
	MM_ADD = 1,
	MM_INSERT,
	MM_MOVE,
	MM_REMOVE,
	MM_CLEAR,
	MM_EDIT,
	MM_EDIT_RESPONSE
};
//----------------------------------------------------------------------------------
enum UPDATE_GAME_OBJECT_TYPE
{
	UGOT_ITEM = 0,
	UGOT_NEW_ITEM = 1,
	UGOT_MULTI = 2,
};
//----------------------------------------------------------------------------------
enum ORION_COMMAND_TYPE
{
	OCT_RESERVED = 1,
	OCT_CLOSE_GENERIC_GUMP_WITHOUT_RESPONSE = 100,
	OCT_SELECT_MENU = 101,
	OCT_CAST_SPELL_REQUEST = 102,
	OCT_USE_SKILL_REQUEST = 103,
	OCT_DRAW_STATUSBAR = 104,
	OCT_CLOSE_STATUSBAR = 105,
	OCT_SECURE_TRADE_CHECK = 106,
	OCT_SECURE_TRADE_CLOSE = 107,
	OCT_UNICODE_SPEECH_REQUEST = 108,
	OCT_RENAME_MOUNT_REQUEST = 109,
	OCT_RECONNECT = 110,
	OCT_PLAY_MACRO = 111,
	OCT_MOVE_PAPERDOLL = 112,
	OCT_USE_ABILITY = 113
};
//----------------------------------------------------------------------------------
enum VALUE_KEY_INT
{
	VKI_SOUND = 0,
	VKI_SOUND_VALUE,
	VKI_MUSIC,
	VKI_MUSIC_VALUE,
	VKI_USE_TOOLTIPS,
	VKI_ALWAYS_RUN,
	VKI_NEW_TARGET_SYSTEM,
	VKI_OBJECT_HANDLES,
	VKI_SCALE_SPEECH_DELAY,
	VKI_SPEECH_DELAY,
	VKI_IGNORE_GUILD_MESSAGES,
	VKI_IGNORE_ALLIANCE_MESSAGES,
	VKI_DARK_NIGHTS,
	VKI_COLORED_LIGHTING,
	VKI_CRIMINAL_ACTIONS_QUERY,
	VKI_CIRCLETRANS,
	VKI_CIRCLETRANS_VALUE,
	VKI_LOCK_RESIZING_GAME_WINDOW,
	VKI_CLIENT_FPS_VALUE,
	VKI_USE_SCALING_GAME_WINDOW,
	VKI_DRAW_STATUS_STATE,
	VKI_DRAW_STUMPS,
	VKI_MARKING_CAVES,
	VKI_NO_VEGETATION,
	VKI_NO_ANIMATE_FIELDS,
	VKI_STANDARD_CHARACTERS_DELAY,
	VKI_STANDARD_ITEMS_DELAY,
	VKI_LOCK_GUMPS_MOVING,
	VKI_CONSOLE_NEED_ENTER,
	VKI_HIDDEN_CHARACTERS_MODE,
	VKI_HIDDEN_CHARACTERS_ALPHA,
	VKI_HIDDEN_CHARACTERS_MODE_ONLY_FOR_SELF,
	VKI_TRANSPARENT_SPELL_ICONS,
	VKI_SPELL_ICONS_ALPHA,
	VKI_SKILLS_COUNT,
	VKI_SKILL_CAN_BE_USED,
	VKI_STATIC_ART_ADDRESS,
	VKI_USED_LAYER,
	VKI_SPELLBOOK_COUNT,
	VKI_BLOCK_MOVING,
	VKI_SET_PLAYER_GRAPHIC,
	VKI_FAST_ROTATION,
	VKI_IGNORE_STAMINA_CHECK,
	VKI_LAST_TARGET,
	VKI_LAST_ATTACK,
	VKI_NEW_TARGET_SYSTEM_SERIAL,
	VKI_GET_MAP_SIZE,
	VKI_GET_MAP_BLOCK_SIZE,
	VKI_MAP_MUL_ADDRESS,
	VKI_STATIC_IDX_ADDRESS,
	VKI_STATIC_MUL_ADDRESS,
	VKI_MAP_DIFL_ADDRESS,
	VKI_MAP_DIF_ADDRESS,
	VKI_STATIC_DIFL_ADDRESS,
	VKI_STATIC_DIFI_ADDRESS,
	VKI_STATIC_DIF_ADDRESS,
	VKI_VERDATA_ADDRESS,
	VKI_MAP_MUL_SIZE,
	VKI_STATIC_IDX_SIZE,
	VKI_STATIC_MUL_SIZE,
	VKI_MAP_DIFL_SIZE,
	VKI_MAP_DIF_SIZE,
	VKI_STATIC_DIFL_SIZE,
	VKI_STATIC_DIFI_SIZE,
	VKI_STATIC_DIF_SIZE,
	VKI_VERDATA_SIZE,
	VKI_MAP_UOP_ADDRESS,
	VKI_MAP_UOP_SIZE,
	VKI_MAP_X_UOP_ADDRESS,
	VKI_MAP_X_UOP_SIZE,
	VKI_CLILOC_ENU_ADDRESS,
	VKI_CLILOC_ENU_SIZE,
	VKI_GUMP_ART_ADDRESS,
	VKI_VIEW_RANGE
};
//----------------------------------------------------------------------------------
enum VALUE_KEY_STRING
{
	VKS_SKILL_NAME = 0,
	VKS_SERVER_NAME,
	VKS_CHARACTER_NAME,
	VKS_SPELLBOOK_1_SPELL_NAME,
	VKS_SPELLBOOK_2_SPELL_NAME,
	VKS_SPELLBOOK_3_SPELL_NAME,
	VKS_SPELLBOOK_4_SPELL_NAME,
	VKS_SPELLBOOK_5_SPELL_NAME,
	VKS_SPELLBOOK_6_SPELL_NAME,
	VKS_SPELLBOOK_7_SPELL_NAME
};
//----------------------------------------------------------------------------------
//!Типы текстовых сообщений
enum OBJECT_LAYERS
{
	OL_NONE = 0,	//0
	OL_1_HAND,		//1
	OL_2_HAND,		//2
	OL_SHOES,		//3
	OL_PANTS,		//4
	OL_SHIRT,		//5
	OL_HELMET,		//6
	OL_GLOVES,		//7
	OL_RING,		//8
	OL_TALISMAN,	//9
	OL_NECKLACE,	//10
	OL_HAIR,		//11
	OL_WAIST,		//12
	OL_TORSO,		//13
	OL_BRACELET,	//14
	OL_15,			//15
	OL_BEARD,		//16
	OL_TUNIC,		//17
	OL_EARRINGS,	//18
	OL_ARMS,		//19
	OL_CLOAK,		//20
	OL_BACKPACK,	//21
	OL_ROBE,		//22
	OL_SKIRT,		//23
	OL_LEGS,		//24
	OL_MOUNT,		//25
	OL_BUY_RESTOCK,	//26
	OL_BUY,			//27
	OL_SELL,		//28
	OL_BANK			//29
};
//----------------------------------------------------------------------------------
//!Типы текстовых сообщений
enum SPEECH_TYPE
{
	ST_NORMAL = 0,
	ST_BROADCAST,				//System
	ST_EMOTE,					//Emote
	ST_SYSTEM = 0x06,			//System / Lower Corner
	ST_SYSTEM_WHITE_NAME = 0x07,//Message / Corner With Name
	ST_WHISPER = 0x08,			//Whisper
	ST_YELL = 0x09,				//Yell
	ST_SPELL = 0x0A,			//Spell
	ST_GUILD_CHAT = 0x0D,		//Guild Chat
	ST_ALLIANCE_CHAT = 0x0E,	//Alliance Chat
	ST_COMMAND_PROMT = 0x0F,	//Command Prompts
	ST_ENCODED_COMMAND = 0xC0,	//Encoded Commands
};
//----------------------------------------------------------------------------------
enum TEXT_TYPE
{
	TT_CLIENT = 0,
	TT_SYSTEM,
	TT_OBJECT
};
//----------------------------------------------------------------------------------
enum TEXT_ALIGN_TYPE
{
	TS_LEFT = 0,
	TS_CENTER,
	TS_RIGHT
};
//----------------------------------------------------------------------------------
//!Типы промптов
enum PROMPT_TYPE
{
	PT_NONE = 0,
	PT_ASCII,
	PT_UNICODE
};
//----------------------------------------------------------------------------------
enum MOUSE_WHEEL_STATE
{
	MWS_UP = 0,
	MWS_DOWN,
	MWS_DOUBLE_CLICK,
	MWS_SCROLL_UP,
	MWS_SCROLL_DOWN
};
//----------------------------------------------------------------------------------
//!Направления чара
enum DIRECTION_TYPE
{
	DT_N = 0,
	DT_NE,
	DT_E,
	DT_SE,
	DT_S,
	DT_SW,
	DT_W,
	DT_NW
};
//----------------------------------------------------------------------------------
//!Злобность персонажа
enum NOTORIETY_TYPE
{
	NT_NONE = 0,
	NT_INNOCENT,
	NT_FRIENDLY,
	NT_SOMEONE_GRAY,
	NT_CRIMINAL,
	NT_ENEMY,
	NT_MURDERER,
	NT_INVULNERABLE
};
//----------------------------------------------------------------------------------
enum SEASON_TYPE
{
	ST_SPRING = 0,
	ST_SUMMER,
	ST_FALL,
	ST_WINTER,
	ST_DESOLATION
};
//----------------------------------------------------------------------------------
enum ABILITY_TYPE
{
	AT_NONE = 0,
	AT_ARMOR_IGNORE,
	AT_BLEED_ATTACK,
	AT_CONCUSSION_BLOW,
	AT_CRUSHING_BLOW,
	AT_DISARM,
	AT_DISMOUNT,
	AT_DOUBLE_STRIKE,
	AT_INFECTING,
	AT_MORTAL_STRIKE,
	AT_MOVING_SHOT,
	AT_PARALYZING_BLOW,
	AT_SHADOW_STRIKE,
	AT_WHIRLWIND_ATTACK,
	AT_RIDING_SWIPE,
	AT_FRENZIED_WHIRLWIND,
	AT_BLOCK,
	AT_DEFENSE_MASTERY,
	AT_NERVE_STRIKE,
	AT_TALON_STRIKE,
	AT_FEINT,
	AT_DUAL_WIELD,
	AT_DOUBLE_SHOT,
	AT_ARMOR_PIERCE,
	AT_BLADEWEAVE,
	AT_FORCE_ARROW,
	AT_LIGHTNING_ARROW,
	AT_PSYCHIC_ATTACK,
	AT_SERPENT_ARROW,
	AT_FORCE_OF_NATURE,
	AT_INFUSED_THROW,
	AT_MYSTIC_ARC,
	AT_DISROBE
};
//----------------------------------------------------------------------------------
#endif
//----------------------------------------------------------------------------------
