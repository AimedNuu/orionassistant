// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OrionAssistant_Global.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "orionassistant_global.h"
//----------------------------------------------------------------------------------
#if USE_OA_DEBUG_FUNCTION_NAMES == 2
const char *g_OACurrentFunctionName = NULL;
#endif
//----------------------------------------------------------------------------------
COAFunDebug::COAFunDebug(const char *str)
{
	Q_UNUSED(str);
}
//----------------------------------------------------------------------------------
COAFunDebug::~COAFunDebug()
{
}
//----------------------------------------------------------------------------------
int GetInt(const VALUE_KEY_INT &key, const int &value)
{
	return g_GetInt(key, value);
}
//----------------------------------------------------------------------------------
QString GetString(const VALUE_KEY_STRING &key, const QString &value)
{
	std::string str = value.toStdString();
	IOrionString *data = g_GetString(key, str.c_str());
	QString result = "";

	if (data->m_Unicode)
		result.fromWCharArray(data->m_DataW);
	else
		result = data->m_DataA;

	return result;
}
//----------------------------------------------------------------------------------
uint64 g_EnabledFeature = OAEF_DEFAULT;
uint64 g_EnabledCommands = OAEC_DEFAULT;
//----------------------------------------------------------------------------------
PACKET_PROC *g_SendClient = nullptr;
PACKET_PROC *g_SendServer = nullptr;
FUNCDEF_GET_STATIC_FLAGS *g_GetStaticFlags = nullptr;
FUNCDEF_GET_VALUE_INT *g_GetInt = nullptr;
FUNCDEF_SET_VALUE_INT *g_SetInt = nullptr;
FUNCDEF_GET_VALUE_STRING *g_GetString = nullptr;
FUNCDEF_SET_VALUE_STRING *g_SetString = nullptr;
HINSTANCE g_HInstance = 0;
QString g_DllPath = "";
QString g_ClientPath = "";
bool g_SaveAero = false;
PPLUGIN_INTERFACE g_ClientInterface = nullptr;
HWND g_ClientHandle = 0;
uint g_LastSendTime = 0;
uint g_LastPacketTime = 0;
uint g_TotalSendSize = 0;
uint g_TotalRecvSize = 0;
uchar g_LightLevel = 0;
uchar g_PersonalLightLevel = 0;
SEASON_DATA g_Season = {0, 0};
WEATHER_DATA g_Weather = {0, 0, 0,};
uint g_PlayerSerial = 0;
uint g_Backpack = 0;
uint g_FindItem = 0;
uint g_CurrentMap = 0;
QString g_ServerName = "";
QString g_CharacterName = "";
bool g_UpdateDisplayOnStep = false;
bool g_LinuxOS = false;
uint g_DressBag = 0;
ushort g_MessagesColor = 1153;
uint g_LastTargetObject = 0;
uint g_LastAttackObject = 0;
ushort g_MorphGraphic = 0;
ushort g_OriginalGraphic = 0;
uint g_LastCorpseObject = 0;
uint g_LastContainerObject = 0;
QTime g_LastUseObjectTimer;
QTime g_StartTimer;
bool g_Connected = false;
uint g_LastFriendFound = 0;
uint g_LastEnemyFound = 0;
uchar g_Ability[2] = { AT_DISARM, AT_PARALYZING_BLOW };
QPoint g_LastTargetPosition(0, 0);
QPoint g_LastAttackPosition(0, 0);
uint g_LastStatusRequest = 0;
uint g_LastStatusObject = 0;
uint g_LastUseObject = 0;
UOI_SELECTED_TILE g_SelectedClientWorldObject;
bool g_ClientMacroPlayed = false;
QPoint g_LastPlayerCorpsePosition = QPoint(-1, -1);
uint g_LastPlayerDeadTimer = 0;
int g_LastSkillIndex = 1;
int g_LastSpellIndex = 1;
bool g_TargetReceived = false;
bool g_MenuReceived = false;
bool g_GumpReceived = false;
bool g_PromptReceived = false;
bool g_ContextMenuReceived = false;
bool g_TradeReceived = false;
bool g_ShopReceived = false;
QPoint g_RemoveRangeXY = QPoint();
//----------------------------------------------------------------------------------
ushort g_SpellReagents[64][5] =
{
	{2, 0x0F7B, 0x0F88, 0, 0},
	{3, 0x0F84, 0x0F85, 0x0F86, 0},
	{2, 0x0F85, 0x0F88, 0, 0},
	{3, 0x0F84, 0x0F85, 0x0F8D, 0},
	{2, 0x0F7A, 0x0F88, 0, 0},
	{2, 0x0F8C, 0x0F8D, 0, 0},
	{3, 0x0F84, 0x0F8C, 0x0F8D, 0},
	{2, 0x0F84, 0x0F88, 0, 0},
	{2, 0x0F7B, 0x0F86, 0, 0},
	{2, 0x0F86, 0x0F88, 0, 0},
	{2, 0x0F84, 0x0F85, 0, 0},
	{2, 0x0F88, 0x0F8D, 0, 0},
	{3, 0x0F84, 0x0F8C, 0x0F8D, 0},
	{2, 0x0F7B, 0x0F8C, 0, 0},
	{3, 0x0F84, 0x0F85, 0x0F8C, 0},
	{2, 0x0F86, 0x0F88, 0, 0},
	{2, 0x0F84, 0x0F86, 0, 0},
	{2, 0x0F7A, 0x0F8C, 0, 0},
	{3, 0x0F8C, 0x0F7B, 0x0F84, 0},
	{1, 0x0F88, 0, 0, 0},
	{2, 0x0F7B, 0x0F86, 0, 0},
	{2, 0x0F7B, 0x0F86, 0, 0},
	{2, 0x0F7B, 0x0F8C, 0, 0},
	{2, 0x0F7B, 0x0F84, 0, 0},
	{3, 0x0F84, 0x0F85, 0x0F86, 0},
	{4, 0x0F84, 0x0F85, 0x0F86, 0x0F8C},
	{3, 0x0F84, 0x0F88, 0x0F8C, 0},
	{3, 0x0F7A, 0x0F8D, 0x0F8C, 0},
	{4, 0x0F84, 0x0F85, 0x0F86, 0x0F8D},
	{3, 0x0F7A, 0x0F86, 0x0F8C, 0},
	{3, 0x0F7A, 0x0F86, 0x0F8D, 0},
	{3, 0x0F7A, 0x0F7B, 0x0F86, 0},
	{3, 0x0F7A, 0x0F86, 0x0F88, 0},
	{4, 0x0F84, 0x0F7A, 0x0F8D, 0x0F8C},
	{3, 0x0F7B, 0x0F84, 0x0F88, 0},
	{3, 0x0F84, 0x0F86, 0x0F8D, 0},
	{4, 0x0F7A, 0x0F86, 0x0F88, 0x0F8C},
	{3, 0x0F84, 0x0F86, 0x0F8D, 0},
	{3, 0x0F7A, 0x0F88, 0x0F8D, 0},
	{3, 0x0F7B, 0x0F86, 0x0F8D, 0},
	{3, 0x0F84, 0x0F86, 0x0F8C, 0},
	{2, 0x0F7A, 0x0F88, 0, 0},
	{3, 0x0F7A, 0x0F86, 0x0F8C, 0},
	{2, 0x0F7B, 0x0F88, 0, 0},
	{3, 0x0F7A, 0x0F7B, 0x0F86, 0},
	{4, 0x0F84, 0x0F86, 0x0F88, 0x0F8C},
	{3, 0x0F7A, 0x0F85, 0x0F8D, 0},
	{3, 0x0F7B, 0x0F8C, 0, 0},
	{4, 0x0F7A, 0x0F86, 0x0F7B, 0x0F8C},
	{4, 0x0F7A, 0x0F86, 0x0F8D, 0x0F8C},
	{2, 0x0F8D, 0x0F8C, 0, 0},
	{3, 0x0F7A, 0x0F86, 0x0F8C, 0},
	{4, 0x0F7A, 0x0F7B, 0x0F86, 0x0F8D},
	{4, 0x0F7A, 0x0F84, 0x0F86, 0x0F8C},
	{4, 0x0F7B, 0x0F8D, 0x0F86, 0x0F8C},
	{3, 0x0F7B, 0x0F86, 0x0F8D, 0},
	{4, 0x0F7B, 0x0F85, 0x0F86, 0x0F8C},
	{4, 0x0F7A, 0x0F7B, 0x0F86, 0x0F88},
	{3, 0x0F7B, 0x0F84, 0x0F85, 0},
	{3, 0x0F7B, 0x0F86, 0x0F8D, 0},
	{4, 0x0F7B, 0x0F86, 0x0F8D, 0x0F8C},
	{3, 0x0F7B, 0x0F86, 0x0F8D, 0},
	{4, 0x0F7B, 0x0F86, 0x0F8D, 0x0F8C},
	{3, 0x0F7B, 0x0F86, 0x0F8D, 0}
};
//----------------------------------------------------------------------------------
