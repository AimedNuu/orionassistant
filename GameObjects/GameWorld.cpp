﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** GameWorld.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "GameWorld.h"
#include "GamePlayer.h"
#include "../OrionAssistant/orionassistantform.h"
#include "../Managers/corpsemanager.h"
#include "../Managers/UOMapManager.h"
#include "../OrionAssistant/tabmain.h"
#include "../OrionAssistant/tabdisplay.h"
#include "../OrionAssistant/objectinspectorform.h"
#include "../OrionAssistant/orionmap.h"
#include "../Managers/partymanager.h"
//----------------------------------------------------------------------------------
CGameWorld *g_World = nullptr;
//----------------------------------------------------------------------------------
CGameWorld::CGameWorld(const uint &serial)
{
	OAFUN_DEBUG("c12_f1");
	m_Items = nullptr;
	CreatePlayer(serial);
}
//----------------------------------------------------------------------------------
CGameWorld::~CGameWorld()
{
	OAFUN_DEBUG("c12_f2");
	RemovePlayer();

	CGameObject *obj = m_Items;
	while (obj != nullptr)
	{
		CGameObject *next = (CGameObject*)obj->m_Next;
		RemoveObject(obj);
		//delete obj;
		obj = next;
	}

	m_Map.clear();

	m_Items = nullptr;
}
//---------------------------------------------------------------------------
/*!
Создать игрока
@param [__in] serial Серийник игрока
@return
*/
void CGameWorld::CreatePlayer(const uint &serial)
{
	OAFUN_DEBUG("c12_f3");
	RemovePlayer();

	g_PlayerSerial = serial;
	g_Player = new CPlayer(serial);

	m_Map[serial] = g_Player;

	if (m_Items != nullptr)
		m_Items->Add(g_Player);
	else
	{
		m_Items = g_Player;
		m_Items->m_Next = nullptr;
		m_Items->m_Prev = nullptr;
	}
}
//---------------------------------------------------------------------------
/*!
Удалить игрока
@return
*/
void CGameWorld::RemovePlayer()
{
	OAFUN_DEBUG("c12_f4");
	if (g_Player != nullptr)
	{
		RemoveFromContainer(g_Player);
		m_Map[g_Player->GetSerial()] = nullptr;
		m_Map.erase(g_Player->GetSerial());
		delete g_Player;
		g_Player = nullptr;
		g_PlayerSerial = 0;
	}
}
//---------------------------------------------------------------------------
/*!
Установить текущего чара с указанным серийником как основного
@param [__in] serial Серийник нового игрока
@return
*/
void CGameWorld::SetPlayer(const uint &serial)
{
	OAFUN_DEBUG("c12_f5");
	if (serial != g_Player->GetSerial())
		CreatePlayer(serial);
}
//----------------------------------------------------------------------------------
void CGameWorld::RemoveRangedObjects()
{
	OAFUN_DEBUG("c12_f6");
	int objectsRange = GetInt(VKI_VIEW_RANGE);

	CGameObject *go = m_Items;

	while (go != NULL)
	{
		CGameObject *next = (CGameObject*)go->m_Next;

		if (go->GetContainer() == 0xFFFFFFFF && !go->IsPlayer())
		{
			if (GetDistance(g_RemoveRangeXY.x(), g_RemoveRangeXY.y(), go->GetX(), go->GetY()) > objectsRange)
			{
				if (g_PartyManager.Contains(go->GetSerial()))
					g_PartyManager.SaveMemberInfo((CGameCharacter*)go);

				RemoveObject(go);
			}
		}

		go = next;
	}
}
//---------------------------------------------------------------------------
void CGameWorld::IgnoreReset()
{
	OAFUN_DEBUG("c12_f7");
	for (WORLD_MAP::iterator i = m_Map.begin(); i != m_Map.end(); ++i)
		i->second->SetIgnored(false);
}
//---------------------------------------------------------------------------
/*!
Создать (или взять, если уже существует) игровой предмет
@param [__in] serial Серийник предмета
@return Ссылка на предмет
*/
CGameItem *CGameWorld::GetWorldItem(const uint &serial)
{
	OAFUN_DEBUG("c12_f8");
	WORLD_MAP::iterator i = m_Map.find(serial);

	if (i == m_Map.end() || (*i).second == NULL)
	{
		CGameItem *obj = new CGameItem(serial);

		m_Map[serial] = obj;

		if (m_Items != NULL)
			m_Items->AddObject(obj);
		else
		{
			m_Items = obj;
			m_Items->m_Next = NULL;
			m_Items->m_Prev = NULL;
		}

		return obj;
	}

	return (CGameItem*)(*i).second;
}
//---------------------------------------------------------------------------
/*!
Создать (или взять, если уже существует) игрового персонажа
@param [__in] serial Серийник персонажа
@return Ссылка на персонажа
*/
CGameCharacter *CGameWorld::GetWorldCharacter(const uint &serial)
{
	OAFUN_DEBUG("c12_f9");
	WORLD_MAP::iterator i = m_Map.find(serial);

	if (i == m_Map.end() || (*i).second == NULL)
	{
		CGameCharacter *obj = new CGameCharacter(serial);

		m_Map[serial] = obj;

		if (m_Items != NULL)
			m_Items->AddObject(obj);
		else
		{
			m_Items = obj;
			m_Items->m_Next = NULL;
			m_Items->m_Prev = NULL;
		}

		return obj;
	}

	return (CGameCharacter*)i->second;
}
//---------------------------------------------------------------------------
/*!
Найти игровой объект в памяти
@param [__in] serial Серийник объекта
@return Ссылка на объект или NULL
*/
CGameObject *CGameWorld::FindWorldObject(const uint &serial)
{
	OAFUN_DEBUG("c12_f10");
	CGameObject *result = NULL;

	WORLD_MAP::iterator i = m_Map.find(serial);
	if (i != m_Map.end())
		result = (*i).second;

	return result;
}
//---------------------------------------------------------------------------
/*!
Найти игровой предмет в памяти
@param [__in] serial Серийник предмета
@return Ссылка на предмет или NULL
*/
CGameItem *CGameWorld::FindWorldItem(const uint &serial)
{
	OAFUN_DEBUG("c12_f11");
	CGameItem *result = NULL;

	WORLD_MAP::iterator i = m_Map.find(serial);
	if (i != m_Map.end() && !((*i).second)->GetNPC())
		result = (CGameItem*)(*i).second;

	return result;
}
//---------------------------------------------------------------------------
/*!
Найти игрового персонажа в памяти
@param [__in] serial Серийник персонажа
@return Ссылка а персонажа или NULL
*/
CGameCharacter *CGameWorld::FindWorldCharacter(const uint &serial)
{
	OAFUN_DEBUG("c12_f12");
	CGameCharacter *result = NULL;

	WORLD_MAP::iterator i = m_Map.find(serial);
	if (i != m_Map.end() && ((*i).second)->GetNPC())
		result = (CGameCharacter*)i->second;

	return result;
}
//---------------------------------------------------------------------------
/*!
Удалить объект из памяти
@param [__in] obj Ссылка на объект
@return
*/
void CGameWorld::RemoveObject(CGameObject *obj)
{
	OAFUN_DEBUG("c12_f13");
	RemoveFromContainer(obj);

	DWORD serial = obj->GetSerial();
	m_Map[serial] = NULL;
	m_Map.erase(serial);
	delete obj;
}
//---------------------------------------------------------------------------
/*!
Вынуть объект из контейнера
@param [__in] obj Ссылка на объект
@return
*/
void CGameWorld::RemoveFromContainer(CGameObject *obj)
{
	OAFUN_DEBUG("c12_f14");
	if (obj->GetContainer() != 0xFFFFFFFF)
	{
		CGameObject *container = FindWorldObject(obj->GetContainer());
		if (container != NULL)
			container->Reject(obj);
		else
			obj->SetContainer(0xFFFFFFFF);
	}
	else
	{
		if (m_Items != NULL)
		{
			if (m_Items->GetSerial() == obj->GetSerial())
			{
				m_Items = (CGameObject*)m_Items->m_Next;
				if (m_Items != NULL)
					m_Items->m_Prev = NULL;
			}
			else
			{
				if (obj->m_Next != NULL)
				{
					if (obj->m_Prev != NULL)
					{
						obj->m_Prev->m_Next = obj->m_Next;
						obj->m_Next->m_Prev = obj->m_Prev;
					}
					else //WTF???
						obj->m_Next->m_Prev = NULL;
				}
				else if (obj->m_Prev != NULL)
					obj->m_Prev->m_Next = NULL;
			}
		}
	}

	obj->m_Next = NULL;
	obj->m_Prev = NULL;
}
//---------------------------------------------------------------------------
/*!
Очистить указанный контейнер
@param [__in] obj Ссылка на объект (контейнер)
@return
*/
void CGameWorld::ClearContainer(CGameObject *obj)
{
	OAFUN_DEBUG("c12_f15");
	if (!obj->Empty())
		obj->Clear();
}
//---------------------------------------------------------------------------
/*!
Положить в контейнер
@param [__in] obj Ссылка на объект
@param [__in] container Ссылка на контейнер
@return
*/
void CGameWorld::PutContainer(CGameObject *obj, CGameObject *container)
{
	OAFUN_DEBUG("c12_f16");
	RemoveFromContainer(obj);
	container->AddItem(obj);
}
//---------------------------------------------------------------------------
/*!
Поднять объект вверх в очереди
@param [__in] obj Ссылка на объект
@return
*/
void CGameWorld::MoveToTop(CGameObject *obj)
{
	OAFUN_DEBUG("c12_f17");
	if (obj == NULL)
		return;

	if (obj->m_Next == NULL)
		return;

	if (obj->GetContainer() == 0xFFFFFFFF)
	{
		if (obj->m_Prev == NULL)
		{
			m_Items = (CGameObject*)obj->m_Next;
			m_Items->m_Prev = NULL;

			CGameObject *item = m_Items;

			while (item != NULL)
			{
				if (item->m_Next == NULL)
				{
					item->m_Next = obj;
					obj->m_Prev = item;
					obj->m_Next = NULL;

					break;
				}

				item = (CGameObject*)item->m_Next;
			}
		}
		else
		{
			CGameObject *item = (CGameObject*)obj->m_Next;

			obj->m_Prev->m_Next = obj->m_Next;
			obj->m_Next->m_Prev = obj->m_Prev;

			while (item != NULL)
			{
				if (item->m_Next == NULL)
				{
					item->m_Next = obj;
					obj->m_Prev = item;
					obj->m_Next = NULL;

					break;
				}

				item = (CGameObject*)item->m_Next;
			}
		}
	}
	else
	{
		CGameObject *container = FindWorldObject(obj->GetContainer());

		if (container == NULL)
			return;

		if (obj->m_Prev == NULL)
		{
			container->m_Items = obj->m_Next;
			container->m_Items->m_Prev = NULL;

			CGameObject *item = (CGameObject*)container->m_Items;

			while (item != NULL)
			{
				if (item->m_Next == NULL)
				{
					item->m_Next = obj;
					obj->m_Prev = item;
					obj->m_Next = NULL;

					break;
				}

				item = (CGameObject*)item->m_Next;
			}
		}
		else
		{
			CGameObject *item = (CGameObject*)obj->m_Next;

			obj->m_Prev->m_Next = obj->m_Next;
			obj->m_Next->m_Prev = obj->m_Prev;

			while (item != NULL)
			{
				if (item->m_Next == NULL)
				{
					item->m_Next = obj;
					obj->m_Prev = item;
					obj->m_Next = NULL;

					break;
				}

				item = (CGameObject*)item->m_Next;
			}
		}
	}
}
//----------------------------------------------------------------------------------
void CGameWorld::UpdateGameObject(const uint &serial, ushort graphic, const uchar &graphicIncrement, const int &count, const int &x, const int &y, const char &z, const uchar &direction, const ushort &color, const uchar &flags, const int &a11, const UPDATE_GAME_OBJECT_TYPE &updateType, const ushort &a13)
{
	Q_UNUSED(a11);
	Q_UNUSED(a13);

	//LOG("UpdateGameObject 0x%08lX:0x%04X 0x%04X (%i) %d:%d:%d %i\n", serial, graphic, color, count, x, y, z, direction);

	CGameCharacter *character = nullptr;
	CGameItem *item = nullptr;
	CGameObject *obj = FindWorldObject(serial);

	if (obj == nullptr)
	{
		//LOG("created ");

		if (!(serial & 0x40000000) && updateType != 3)
		{
			character = GetWorldCharacter(serial);

			if (character == nullptr)
			{
				LOG("No memory?\n");
				return;
			}

			obj = character;
			character->SetGraphic(graphic + graphicIncrement);
			//character->OnGraphicChange(1000);
			character->SetDirection(direction);
			character->SetColor(FixColor(color, (color & 0x8000)));
			character->SetX(x);
			character->SetY(y);
			character->SetZ(z);
			character->SetFlags(flags);

			if (g_OrionMap != nullptr && g_OrionMap->isVisible())
				g_OrionMap->CheckForUpdate(character->GetSerial());
		}
		else
		{
			item = GetWorldItem(serial);

			if (item == nullptr)
			{
				LOG("No memory?\n");
				return;
			}

			if (graphic == 0x2006)
			{
				g_LastCorpseObject = serial;

				if (g_TabMain->CorpsesAutoopen())
					g_CorpseManager.Add(CCorpseInfo(g_LastCorpseObject, g_StartTimer.elapsed() + g_TabMain->CorpseKeepDelay()));
			}

			obj = item;
		}
	}
	else
	{
		//LOG("updated ");

		if (obj->GetContainer() != 0xFFFFFFFF)
		{
			RemoveFromContainer(obj);
			obj->SetContainer(0xFFFFFFFF);
			m_Items->AddObject(obj);
		}

		if (obj->GetNPC())
			character = (CGameCharacter*)obj;
		else
			item = (CGameItem*)obj;
	}

	if (obj == nullptr)
		return;

	obj->SetMapIndex(g_CurrentMap);

	if (!obj->GetNPC())
	{
		if (graphic != 0x2006)
			graphic += graphicIncrement;

		if (updateType == UGOT_MULTI)
		{
			item->SetMultiBody(true);

			item->SetGraphic(graphic & 0x3FFF);

			g_UOMapManager.UOAMnotifyHouse(x, y, obj->GetGraphic());
		}
		else
		{
			item->SetMultiBody(false);

			item->SetGraphic(graphic);
			item->SetTiledataFlags(g_GetStaticFlags(graphic));
		}

		item->SetX(x);
		item->SetY(y);
		item->SetZ(z);

		if (graphic == 0x2006)
			item->SetLayer(direction);

		item->SetColor(FixColor(color, (color & 0x8000)));

		if (count < 1)
			item->SetCount(1);
		else
			item->SetCount(count);

		item->SetFlags(flags);

		//item->OnGraphicChange(direction);

		//LOG("serial:0x%08X graphic:0x%04X color:0x%04X count:%i xyz:%d,%d,%d flags:0x%02X\n", obj->GetSerial(), obj->GetGraphic(), obj->GetColor(), item->GetCount(), obj->GetX(), obj->GetY(), obj->GetZ(), obj->GetFlags());
	}
	else
	{
		graphic += graphicIncrement;

		/*bool found = false;

		if (character->m_Steps.size() != MAX_STEPS_COUNT)
		{
			//if (character->Graphic == graphic && character->Flags == flags)
			{
				if (!character->m_Steps.empty())
				{
					CWalkData &wd = character->m_Steps.back();

					if (wd.X == x && wd.Y == y && wd.Z == z && wd.Direction == direction)
					{
						found = true;
					}
				}
				else if (character->X == x && character->Y == y && character->Z == z && character->Direction == direction)
				{
					found = true;
				}
			}

			if (!found)
			{
				if (character->m_Steps.empty())
					character->LastStepTime = g_Ticks;

				character->m_Steps.push_back(CWalkData(x, y, z, direction, graphic & 0x3FFF, flags));
				found = true;
			}
		}

		if (!found)*/
		{
			character->SetGraphic(graphic & 0x3FFF);
			character->SetX(x);
			character->SetY(y);
			character->SetZ(z);
			character->SetDirection(direction);
			character->SetColor(FixColor(color, (color & 0x8000)));
			character->SetFlags(flags);

			if (g_OrionMap != nullptr && g_OrionMap->isVisible())
				g_OrionMap->CheckForUpdate(character->GetSerial());

			//character->m_Steps.clear();
		}

		//LOG("NPC serial:0x%08X graphic:0x%04X color:0x%04X xyz:%d,%d,%d flags:0x%02X direction:%d notoriety:%d\n", obj->GetSerial(), obj->GetGraphic(), obj->GetColor(), obj->GetX(), obj->GetY(), obj->GetZ(), obj->GetFlags(), character->GetDirection(), character->GetNotoriety());
	}

	g_ObjectInspectorForm->CheckUpdate(obj);

	MoveToTop(obj);
}
//----------------------------------------------------------------------------------
void CGameWorld::UpdatePlayer(const uint &serial, const ushort &graphic, const uchar &graphicIncrement, const ushort &color, const uchar &flags, const int &x, const int &y, const ushort &serverID, const uchar &direction, const char &z)
{
	Q_UNUSED(graphicIncrement);
	Q_UNUSED(serverID);

	if (serial == g_PlayerSerial)
	{
		g_Player->SetX(x);
		g_Player->SetY(y);
		g_Player->SetZ(z);

		g_Player->SetGraphic(graphic);
		//g_Player->OnGraphicChange();

		g_Player->SetDirection(direction);
		g_Player->SetColor(FixColor(color, 0));

		g_Player->SetFlags(flags);

		g_ObjectInspectorForm->CheckUpdate(g_Player);

		if (g_OrionMap != nullptr && g_OrionMap->isVisible())
			g_OrionMap->SetWantRedraw(true);

		MoveToTop(g_Player);
	}
}
//----------------------------------------------------------------------------------
void CGameWorld::UpdateContainedItem(const uint &serial, const ushort &graphic, const uchar &graphicIncrement, const ushort &count, const int &x, const int &y, const uint &containerSerial, const ushort &color)
{
	CGameObject *container = FindWorldObject(containerSerial);

	if (container == NULL)
		return;

	CGameObject *obj = FindWorldObject(serial);

	if (obj != NULL && (!container->IsCorpse() || ((CGameItem*)obj)->GetLayer() == OL_NONE))
	{
		RemoveObject(obj);
		obj = NULL;
	}

	if (obj == NULL)
	{
		if (serial & 0x40000000)
			obj = GetWorldItem(serial);
		else
			obj = GetWorldCharacter(serial);
	}

	if (obj == NULL)
	{
		LOG("No memory?\n");
		return;
	}

	obj->SetMapIndex(g_CurrentMap);

	obj->SetGraphic(graphic + graphicIncrement);

	if (!obj->GetNPC())
		((CGameItem*)obj)->SetTiledataFlags(g_GetStaticFlags(graphic + graphicIncrement));
	obj->SetColor(FixColor(color, (color & 0x8000)));

	if (count < 1)
		obj->SetCount(1);
	else
		obj->SetCount(count);

	obj->SetX(x);
	obj->SetY(y);
	PutContainer(obj, container);

	g_ObjectInspectorForm->CheckUpdate(obj);

	MoveToTop(obj);

	if (obj->GetTopObject() == g_Player)
		g_TabDisplay->CheckRedrawTitle(obj);

	//LOG("\t|0x%08X<0x%08X:%04X*%d (%d,%d) %04X\n", containerSerial, serial, graphic + graphicIncrement, count, x, y, color);
}
//----------------------------------------------------------------------------------
ushort CGameWorld::FixColor(const ushort &color, const ushort &defaultColor)
{
	ushort fixedColor = color & 0x3FFF;

	if (fixedColor)
	{
		if (fixedColor >= 0x0BB8)
			fixedColor = 1;

		fixedColor |= (color & 0xC000);
	}
	else
	{
		fixedColor = defaultColor;
	}

	return fixedColor;
}
//---------------------------------------------------------------------------
/*!
Дамп предметов, хранящихся в памяти
@param [__in] nCount Количество отступов
@param [__in_opt] serial Серийник родителя
@return
*/
void CGameWorld::Dump(uchar tCount, uint serial)
{
	OAFUN_DEBUG("c12_f18");
#if CLOGGER != 0
	LOG("World Dump:\n\n");

	CGameObject *obj = m_Items;

	if (serial != 0xFFFFFFFF)
	{
		obj = FindWorldObject(serial);
		if (obj != NULL)
			obj = (CGameObject*)obj->m_Items;
	}

	while (obj != NULL)
	{
		if (obj->GetContainer() == serial)
		{
			if (obj->GetSerial() == g_Player->GetSerial())
				LOG("---Player---\n");

			IFOR(i, 0, tCount)
				LOG("\t");

			LOG("%s%08X:%04X[%04X](%%02X)*%i\tin 0x%08X XYZ=%i,%i,%i on Map %i\n", (obj->GetNPC() ? "NPC: " : "Item: "), obj->GetSerial(), obj->GetGraphic(), obj->GetColor(), /*obj->Layer,*/ obj->GetCount(), obj->GetContainer(), obj->GetX(), obj->GetY(), obj->GetZ(), obj->GetMapIndex());

			if (obj->m_Items != NULL)
				Dump(tCount + 1, obj->GetContainer());
		}

		obj = (CGameObject*)obj->m_Next;
	}
#else
	Q_UNUSED(tCount);
	Q_UNUSED(serial);
#endif
}
//---------------------------------------------------------------------------
