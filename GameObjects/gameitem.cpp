﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** GameItem.cpp
**
** Copyright (C) August 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "GameItem.h"
//----------------------------------------------------------------------------------
CGameItem::CGameItem(const uint &serial)
: CGameObject(serial)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CGameItem::~CGameItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
int CGameItem::GetEquipLayer()
{
	OAFUN_DEBUG("c8_f1");
	int layer = m_Layer;

	if (!layer)
		layer = GetInt(VKI_USED_LAYER, m_Graphic);

	return layer;
}
//----------------------------------------------------------------------------------
CGameItem *CGameItem::FindItem(const ushort &graphic, const ushort &color)
{
	OAFUN_DEBUG("c8_f2");
	CGameItem *item = NULL;

	if (color == 0xFFFF) //Поиск по минимальному цвету
	{
		WORD minColor = 0xFFFF;

		QFOR(obj, m_Items, CGameItem*)
		{
			if (obj->GetGraphic() == graphic)
			{
				if (obj->GetColor() < minColor)
				{
					item = obj;
					minColor = obj->GetColor();
				}
			}

			if (!obj->Empty())
			{
				CGameItem *found = obj->FindItem(graphic, color);

				if (found != NULL && found->GetColor() < minColor)
				{
					item = found;
					minColor = found->GetColor();
				}
			}
		}
	}
	else //стандартный поиск
	{
		QFOR(obj, m_Items, CGameItem*)
		{
			if (obj->GetGraphic() == graphic && obj->GetColor() == color)
				item = obj;

			if (!obj->Empty())
			{
				CGameItem *found = obj->FindItem(graphic, color);

				if (found != NULL)
					item = found;
			}
		}
	}

	return item;
}
//----------------------------------------------------------------------------------
