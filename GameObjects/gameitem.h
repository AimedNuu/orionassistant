/***********************************************************************************
**
** GameItem.h
**
** Copyright (C) August 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef GAMEITEM_H
#define GAMEITEM_H
//----------------------------------------------------------------------------------
#include "GameObject.h"
//----------------------------------------------------------------------------------
//!Класс игрового предмета (или трупа)
class CGameItem : public CGameObject
{
	//!Слой, в котором расположен объект
	SETGET(uchar, Layer, 0)
	//!Это прокси-объект мульти
	SETGET(bool, MultiBody, false)

	SETGET(uint, Price, 0)
	SETGET(bool, NameFromCliloc, false)
	SETGET(quint64, TiledataFlags, 0)

public:
	CGameItem(const uint &serial = 0);
	virtual ~CGameItem();

	int GetEquipLayer();

	CGameItem *FindItem(const ushort &graphic, const ushort &color = 0xFFFF);
};
//----------------------------------------------------------------------------------
#endif
//----------------------------------------------------------------------------------
