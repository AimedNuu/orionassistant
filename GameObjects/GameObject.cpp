// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** GameObject.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "GameObject.h"
#include "GameWorld.h"
#include "../Managers/PacketManager.h"
#include "../OrionAssistant/orionassistant.h"
#include "GamePlayer.h"
//----------------------------------------------------------------------------------
CGameObject::CGameObject(const uint &serial)
: CBaseQueueItem(), m_Serial(serial)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CGameObject::~CGameObject()
{
	OAFUN_DEBUG("");
	m_Next = NULL;
	m_Prev = NULL;

	Clear();
}
//----------------------------------------------------------------------------------
void CGameObject::OnChangeFlags(const uchar &newFlags)
{
	OAFUN_DEBUG("c9_f1");
	if (IsPlayer())
	{
		if ((m_Flags & 0x80) != (newFlags & 0x80))
			g_Player->SetStealthSteps(0);
	}
}
//----------------------------------------------------------------------------------
void CGameObject::Clear()
{
	OAFUN_DEBUG("c9_f2");
	if (!Empty())
	{
		CGameObject *obj = (CGameObject*)m_Items;

		while (obj != NULL)
		{
			CGameObject *next = (CGameObject*)obj->m_Next;

			g_World->RemoveObject(obj);

			obj = next;
		}

		m_Items = NULL;
	}
}
//----------------------------------------------------------------------------------
void CGameObject::ClearUnequipped()
{
	OAFUN_DEBUG("c9_f2");
	if (!Empty())
	{
		CGameObject *newFirstItem = NULL;
		CGameObject *obj = (CGameObject*)m_Items;

		while (obj != NULL)
		{
			CGameObject *next = (CGameObject*)obj->m_Next;

			if (((CGameItem*)obj)->GetLayer() != OL_NONE)
			{
				if (newFirstItem == NULL)
					newFirstItem = obj;
			}
			else
				g_World->RemoveObject(obj);

			obj = next;
		}

		m_Items = newFirstItem;
	}
}
//----------------------------------------------------------------------------------
bool CGameObject::Poisoned()
{
	OAFUN_DEBUG("c9_f3");
	if (g_PacketManager.GetClientVersion() >= CV_7000)
		return m_SA_Poisoned;
	else
		return (m_Flags & 0x04);
}
//----------------------------------------------------------------------------------
bool CGameObject::Flying()
{
	OAFUN_DEBUG("c9_f4");
	if (g_PacketManager.GetClientVersion() >= CV_7000)
		return (m_Flags & 0x04);
	else
		return false;
}
//----------------------------------------------------------------------------------
void CGameObject::AddObject(CGameObject *obj)
{
	OAFUN_DEBUG("c9_f5");
	g_World->RemoveFromContainer(obj);

	if (m_Next == NULL)
	{
		m_Next = obj;
		m_Next->m_Prev = this;
		m_Next->m_Next = NULL;

		((CGameObject*)m_Next)->SetContainer(m_Container);
	}
	else
	{
		CGameObject *item = (CGameObject*)m_Next;

		while (item->m_Next != NULL)
			item = (CGameObject*)item->m_Next;

		item->m_Next = obj;
		obj->m_Next = NULL;
		obj->m_Prev = item;

		obj->SetContainer(m_Container);
	}
}
//----------------------------------------------------------------------------------
void CGameObject::AddItem(CGameObject *obj)
{
	OAFUN_DEBUG("c9_f6");
	if (obj->GetContainer() != 0xFFFFFFFF)
		return;

	g_World->RemoveFromContainer(obj);

	if (m_Items != NULL)
	{
		CGameObject *item = (CGameObject*)Last();

		item->m_Next = obj;
		obj->m_Next = NULL;
		obj->m_Prev = item;
	}
	else
	{
		m_Items = obj;
		m_Items->m_Next = NULL;
		m_Items->m_Prev = NULL;
	}

	obj->SetContainer(m_Serial);
}
//----------------------------------------------------------------------------------
void CGameObject::Reject(CGameObject *obj)
{
	OAFUN_DEBUG("c9_f7");
	if (obj->GetContainer() != m_Serial)
		return;

	if (m_Items != NULL)
	{
		if (((CGameObject*)m_Items)->GetSerial() == obj->GetSerial())
		{
			if (m_Items->m_Next != NULL)
			{
				m_Items = m_Items->m_Next;
				m_Items->m_Prev = NULL;
			}
			else
				m_Items = NULL;
		}
		else
		{
			if (obj->m_Next != NULL)
			{
				if (obj->m_Prev != NULL)
				{
					obj->m_Prev->m_Next = obj->m_Next;
					obj->m_Next->m_Prev = obj->m_Prev;
				}
				else //WTF???
					obj->m_Next->m_Prev = NULL;
			}
			else if (obj->m_Prev != NULL)
				obj->m_Prev->m_Next = NULL;
		}
	}

	obj->m_Next = NULL;
	obj->m_Prev = NULL;
	obj->SetContainer(0xFFFFFFFF);
}
//----------------------------------------------------------------------------------
CGameObject *CGameObject::GetTopObject()
{
	OAFUN_DEBUG("c9_f8");
	CGameObject *obj = this;

	while (obj->GetContainer() != 0xFFFFFFFF)
		obj = g_World->FindWorldObject(obj->GetContainer());

	return obj;
}
//----------------------------------------------------------------------------------
CGameItem *CGameObject::FindLayer(const int &layer)
{
	OAFUN_DEBUG("c9_f9");
	QFOR(obj, m_Items, CGameItem*)
	{
		if (obj->GetLayer() == layer)
			return obj;
	}

	return NULL;
}
//----------------------------------------------------------------------------------
int CGameObject::CountItems(const CFindItem &findListItem, const bool &recurse)
{
	OAFUN_DEBUG("c9_f10");
	int count = 0;

	QFOR(item, m_Items, CGameItem*)
	{
		if (findListItem.Found(item->GetGraphic(), item->GetColor()))
		/*if ((graphic == 0xFFFF || graphic == item->GetGraphic()) &&
			(color == 0xFFFF || color == item->GetColor()))*/
		{
			count += item->GetCount();
		}

		if (recurse && !item->Empty() && item->GetLayer() != OL_BANK)
			count += item->CountItems(findListItem, recurse);
	}

	return count;
}
//----------------------------------------------------------------------------------
int CGameObject::CountItems(const ushort &graphic, const ushort &color, const bool &recurse)
{
	OAFUN_DEBUG("c9_f11");
	int count = 0;

	QFOR(item, m_Items, CGameItem*)
	{
		if ((graphic == 0xFFFF || graphic == item->GetGraphic()) &&
			(color == 0xFFFF || color == item->GetColor()))
		{
			count += item->GetCount();
		}

		if (recurse && !item->Empty() && item->GetLayer() != OL_BANK)
			count += item->CountItems(graphic, color, recurse);
	}

	return count;
}
//----------------------------------------------------------------------------------
CGameObject *CGameObject::FindType(const CFindItem &findListItem, const QList<CIgnoreItem> &ignoreList, const uchar &flags, const bool &recurseSearch, QStringList &list)
{
	OAFUN_DEBUG("c9_f12");
	CGameObject *result = nullptr;

	QList<CGameObject*> containers;

	QFOR(item, m_Items, CGameItem*)
	{
		if (!item->GetIgnored() &&
		    findListItem.Found(item->GetGraphic(), item->GetColor()))
			/*(graphic == 0xFFFF || graphic == item->GetGraphic()) &&
			(color == 0xFFFF || color == item->GetColor()))*/
		{
			bool ignored = false;

			if (!ignoreList.empty())
			{
				for (const CIgnoreItem &ii : ignoreList)
				{
					if (ii.Found(item->GetSerial(), item->GetGraphic(), item->GetColor()))
					{
						ignored = true;
						break;
					}
				}
			}

			if (!ignored)
			{
				list << COrionAssistant::SerialToText(item->GetSerial());

				result = item;

				if (flags & FTF_FAST)
					break;
			}
		}

		if (recurseSearch && !item->Empty() && item->GetLayer() != OL_BANK)
			containers.push_back(item);
	}

	if (!(flags & FTF_FAST) || result == nullptr)
	{
		for (CGameObject *item : containers)
		{
			CGameObject *temp = item->FindType(findListItem, ignoreList, flags, recurseSearch, list);

			if (temp != nullptr)
			{
				result = temp;

				if (flags & FTF_FAST)
					break;
			}
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
int GetDistance(CGameObject *obj1, CGameObject *obj2)
{
	OAFUN_DEBUG("c10_f1");
	if (obj1 != nullptr && obj2 != nullptr)
	{
		int distx = abs(obj2->GetX() - obj1->GetX());
		int disty = abs(obj2->GetY() - obj1->GetY());

		return qMax(distx, disty);
	}

	return 100500;
}
//----------------------------------------------------------------------------------
int GetDistance(const int &x1, const int &y1, const int &x2, const int &y2)
{
	OAFUN_DEBUG("c10_f2");
	int distx = abs(x2 - x1);
	int disty = abs(y2 - y1);

	return qMax(distx, disty);
}
//----------------------------------------------------------------------------------
