// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** GameCharacter.cpp
**
** Copyright (C) August 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "GameCharacter.h"
#include "GamePlayer.h"
#include "GameItem.h"
//----------------------------------------------------------------------------------
CGameCharacter::CGameCharacter(const uint &serial)
: CGameObject(serial)
{
	OAFUN_DEBUG("");
	m_NPC = true;
}
//----------------------------------------------------------------------------------
CGameCharacter::~CGameCharacter()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
