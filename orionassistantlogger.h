﻿//----------------------------------------------------------------------------------
#ifndef ORIONASSISTANTLOGGER_H
#define ORIONASSISTANTLOGGER_H
//----------------------------------------------------------------------------------
#include "orionassistant_global.h"
//----------------------------------------------------------------------------------
#define CLOGGER 1

#if CLOGGER != 0
	#define INITLOGGER(path) g_Logger.Init(path);
	#define LOG g_Logger.Print
	#if CLOGGER == 2
		#define LOG_DUMP(...)
	#else //CLOGGER != 2
		#define LOG_DUMP g_Logger.Dump
	#endif //CLOGGER == 2
#else //CLOGGER == 0
	#define INITLOGGER(path)
#define LOG(...)
#define LOG_DUMP(...)
#endif //CLOGGER!=0
//----------------------------------------------------------------------------------
class CLogger
{
protected:
	FILE *m_File;

public:
	CLogger();
	~CLogger();

	bool Ready() const { return m_File != NULL; }

	void Init(const string &filePath);
	void Init(const wstring &filePath);

	void Print(const char *format, ...);
	void VPrint(const char *format, va_list ap);
	void Print(const wchar_t *format, ...);
	void VPrint(const wchar_t *format, va_list ap);
	void Dump(uchar *buf, int size);
};
//----------------------------------------------------------------------------------
extern CLogger g_Logger;
//----------------------------------------------------------------------------------
#endif //ORIONASSISTANTLOGGER_H
//----------------------------------------------------------------------------------
