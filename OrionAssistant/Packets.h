/***********************************************************************************
**
** Packets.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef PACKETS_H
#define PACKETS_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../Managers/DataWritter.h"
#include "../Managers/gumpmanager.h"
//----------------------------------------------------------------------------------
#define DEFINE_PACKET(name, ...) \
	class CPacket ##name : public CPacket \
	{ \
	public: \
		CPacket ##name ( __VA_ARGS__ ); \
	}
//----------------------------------------------------------------------------------
class CPacket : public CDataWritter
{
public:
	CPacket(const int &size, const bool &autoResize = false);

	void SendClient();
	void SendServer();
};
//----------------------------------------------------------------------------------
DEFINE_PACKET(Light, const uchar &light);
DEFINE_PACKET(PersonalLight, const uint &serial, const uchar &light);
DEFINE_PACKET(ToClientSystemPrint, const wstring &text, const ushort &color);
DEFINE_PACKET(ToClientCharacterPrint, const uint &serial, const wstring &text, const ushort &color);
DEFINE_PACKET(ToClientTextReplacePrintA, const string &text, const uint &serial, const ushort &graphic, const uchar &messageType, const ushort &color, const ushort &font);
DEFINE_PACKET(ToClientTextReplacePrintW, const wstring &text, const uint &serial, const ushort &graphic, const uchar &messageType, const ushort &color, const ushort &font, const uint &lang);
DEFINE_PACKET(ToClientTargetRequest);
DEFINE_PACKET(StatusRequest, const uint &serial);
DEFINE_PACKET(ClickRequest, const uint &serial);
DEFINE_PACKET(DoubleClickRequest, const uint &serial);
DEFINE_PACKET(AttackRequest, const uint &serial);
DEFINE_PACKET(Weather, const WEATHER_DATA &data);
DEFINE_PACKET(Season, const SEASON_DATA &data);
DEFINE_PACKET(HelpRequest);
DEFINE_PACKET(ChangeWarmode, const uchar &state);
DEFINE_PACKET(Resend);
DEFINE_PACKET(EmoteAction, const char *action);
DEFINE_PACKET(HideObject, const uint &serial);
DEFINE_PACKET(PlaySound, const ushort &index, const short &x, const short &y, const char &z);
DEFINE_PACKET(PickupRequest, const uint &serial, const ushort &count);
DEFINE_PACKET(DropRequestOld, const uint &serial, const ushort &x, const ushort &y, const char &z, const uint &container);
DEFINE_PACKET(DropRequestNew, const uint &serial, const ushort &x, const ushort &y, const char &z, const uchar &slot, const uint &container);
DEFINE_PACKET(EquipRequest, const uint &serial, const uchar &layer, const uint &container);
DEFINE_PACKET(QuestArrowOld, const bool &enabled, const short &x, const short &y);
DEFINE_PACKET(QuestArrowNew, const bool &enabled, const short &x, const short &y);
DEFINE_PACKET(MenuResponse, const uint &serial, const uint &id, const int &code, const ushort &graphic, const ushort &color);
DEFINE_PACKET(GrayMenuResponse, const uint &serial, const uint &id, const int &code);
DEFINE_PACKET(GraphicEffect, const ushort &graphic, const uchar &duration, const short &x, const short &y, const char &z);
DEFINE_PACKET(PartyAccept, const uint &serial);
DEFINE_PACKET(PartyDecline, const uint &serial);
DEFINE_PACKET(TradeClose, const uint &id);
DEFINE_PACKET(TradeCheckState, const uint &id, const bool &state);
DEFINE_PACKET(DyeData, const uint &serial);
DEFINE_PACKET(UseCombatAbility, const uchar &index);
DEFINE_PACKET(WrestlingStun);
DEFINE_PACKET(WrestlingDisarm);
DEFINE_PACKET(CancelTarget);
DEFINE_PACKET(InvokeVirtureRequest, const uchar &id);
DEFINE_PACKET(GumpResponse, CGump *gump, const int &code);
DEFINE_PACKET(TargetCancel, const uchar &type, const uint &targetSerial, const uchar &targetType);
DEFINE_PACKET(TargetObject, const uchar &type, const uint &targetSerial, const uchar &targetType, const uint &serial);
DEFINE_PACKET(TargetTile, const uchar &type, const uint &targetSerial, const uchar &targetType, const ushort &graphic, const ushort &x, const ushort &y, const char &z);
DEFINE_PACKET(ProfileRequest, const uint &serial);
DEFINE_PACKET(ASCIIPromptResponse, const uint &serial, const uint &id, const string &text, const bool &cancel);
DEFINE_PACKET(UnicodePromptResponse, const uint &serial, const uint &id, const wstring &text, const string &lang, const bool &cancel);
DEFINE_PACKET(BuyRequest, const uint &serial, const QVector< std::pair<uint, int> > &list);
DEFINE_PACKET(SellRequest, const uint &serial, const QVector< std::pair<uint, int> > &list);
DEFINE_PACKET(LogOut);
DEFINE_PACKET(ClosePaperdoll, const uint &serial);
DEFINE_PACKET(ContextMenuRequest, const uint &serial);
DEFINE_PACKET(ContextMenuSelection, const uint &serial, const uint &index);
DEFINE_PACKET(TargetUseObject, const uint &useObjectSerial, const uint &targetObjectSerial);
DEFINE_PACKET(TargetCastSpell, const ushort &spell, const uint &targetObjectSerial);
DEFINE_PACKET(TargetUseSkill, const ushort &skill, const uint &targetObjectSerial);
DEFINE_PACKET(ClientViewRange, uchar range);
DEFINE_PACKET(ConfirmWalk, const uchar &direction, const uchar &notoriety);
DEFINE_PACKET(MapUOPartyInfo);
DEFINE_PACKET(MapUOGuildInfo);
DEFINE_PACKET(GuildMenuRequest);
DEFINE_PACKET(QuestMenuRequest);
DEFINE_PACKET(ToggleGargoyleFlying);

//Пакеты для управления состоянием в клиенте, вместо функционального интерфейса
DEFINE_PACKET(CloseGenericGumpWithoutResponse, CGump *gump, const bool &all);
DEFINE_PACKET(SelectMenuInClient, const uint &serial, const uint &id, const int &code);
DEFINE_PACKET(CastSpellRequest, const int &id);
DEFINE_PACKET(UseSkillRequest, const int &id);
DEFINE_PACKET(DrawStatusbar, const uint &serial, const int &x, const int &y, const bool &minimized);
DEFINE_PACKET(CloseStatusbar, const uint &serial);
DEFINE_PACKET(SecureTradeCheck, const uint &id1, const bool &state);
DEFINE_PACKET(SecureTradeClose, const uint &id1);
DEFINE_PACKET(UnicodeSpeechRequest, const ushort &color, const wstring &text);
DEFINE_PACKET(RenameMountRequest, const uint &serial, const string &name);
DEFINE_PACKET(Reconnect);
DEFINE_PACKET(PlayMacro, const QVector<string> &actionList, const QVector<string> &subActionList);
DEFINE_PACKET(MovePaperdoll, const uint &serial, const int &x, const int &y);
DEFINE_PACKET(UseAbilityRequest, const int &index);
//----------------------------------------------------------------------------------
#endif
//----------------------------------------------------------------------------------
