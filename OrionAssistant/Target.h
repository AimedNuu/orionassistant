﻿/***********************************************************************************
**
** Target.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TARGET_H
#define TARGET_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../Managers/DataReader.h"
//----------------------------------------------------------------------------------
class CTargetHook
{
public:
	CTargetHook() {}
	CTargetHook(const uint &serial);
	CTargetHook(const QString &tile, const int &x, const int &y, const int &z, const bool &relative);
	~CTargetHook() {}

	bool TargetOnObject{true};
	uint Serial{0};
	QString Tile{""};
	int X{0};
	int Y{0};
	int Z{0};
	bool Relative{false};
};
//----------------------------------------------------------------------------------
//Класс для работы с таргетом
class CTarget
{
	//Тип объекта прицела
	SETGET(uchar, Type, 0)
	//Тип прицела
	SETGET(uchar, CursorType, 0)
	//Мульти на таргете
	SETGET(ushort, MultiGraphic, 0)
	//Прицел от ОА
	SETGET(bool, FromOA, false)
	//Прицел должен быть в клиенте
	SETGET(bool, ClientWantTarget, false)

	SETGET(ushort, LastTileGraphic, 0)
	SETGET(ushort, LastTileX, 0)
	SETGET(ushort, LastTileY, 0)
	SETGET(ushort, LastTileZ, 0)

private:
	//Серийник объекта, к которому привязан прицел
	uint m_CursorID{0};

	//Флаг состояния прицела
	bool m_Targeting{false};

	UCHAR_LIST m_CurrentClientTarget;

	QVector<CTargetHook> m_WaitTargets;

public:
	CTarget();
	~CTarget() {}

	bool CheckHook();

	void SetObjectsHook(const QStringList &objects);

	void SetTileHook(const QString &tile, const int &x, const int &y, const int &z, const bool &relative);

	void ClearHooks();

	void RestoreTarget();

	//Установить данные прицела
	bool SetSendData(CDataReader &reader);

	//Установить данные прицела, посланного ОА
	void SetRecvOAData(CDataReader &reader);

	//Установить данные прицела
	bool SetRecvData(CDataReader &reader);

	//Установить данные мульти-таргета
	bool SetMultiData(CDataReader &reader);

	bool IsTargeting() const { return m_Targeting; }

	void DumpLastTileInfo();

	//Очистить таргет
	void Reset();

	//Послать таргет на объект
	void SendTargetObject(const uint &Serial);

	//Послать таргет на тайл
	void SendTargetTile(const ushort &tileID, const short &x, const short &y, short z);

	//Послать таргет на последний тайл
	void SendTargetLastTile();

	//Послать отмену таргета
	void SendCancelTarget();

	//Послать таргет на последнюю цель
	void SendTargetLastTarget();
};
//----------------------------------------------------------------------------------
extern CTarget g_Target;
//----------------------------------------------------------------------------------
#endif
//----------------------------------------------------------------------------------
