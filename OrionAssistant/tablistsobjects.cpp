// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabListsObjects.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tablistsobjects.h"
#include "ui_tablistsobjects.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include "orionassistant.h"
#include "orionassistantform.h"
#include "../GUI/objectlistitem.h"
#include "../GameObjects/GameWorld.h"
//----------------------------------------------------------------------------------
TabListsObjects *g_TabListsObjects = nullptr;
//----------------------------------------------------------------------------------
TabListsObjects::TabListsObjects(QWidget *parent)
: QWidget(parent), ui(new Ui::TabListsObjects)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabListsObjects = this;

	connect(ui->lw_Objects, SIGNAL(itemDropped()), this, SLOT(OnListItemMoved()));
}
//----------------------------------------------------------------------------------
TabListsObjects::~TabListsObjects()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabListsObjects = nullptr;
}
//----------------------------------------------------------------------------------
void TabListsObjects::OnListItemMoved()
{
	OAFUN_DEBUG("");
	QString path = g_OrionAssistantForm->GetSaveConfigPath();

	if (!path.length())
		return;

	SaveObjects(path + "/Objects.xml");
}
//----------------------------------------------------------------------------------
void TabListsObjects::on_lw_Objects_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	QListWidgetItem *item = ui->lw_Objects->item(index.row());

	if (item != nullptr)
	{
		CObjectListItem *oli = (CObjectListItem*)item;
		ui->le_ObjectName->setText(oli->text());
		ui->le_ObjectSerial->setText(COrionAssistant::SerialToText(oli->GetSerial()));
	}
}
//----------------------------------------------------------------------------------
void TabListsObjects::on_pb_ObjectFromTarget_clicked()
{
	OAFUN_DEBUG("");
	g_TargetHandler = &COrionAssistant::HandleTargetObject;

	g_OrionAssistant.RequestTarget("Select a object for obtain serial");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
void TabListsObjects::on_pb_ObjectCreate_clicked()
{
	OAFUN_DEBUG("");
	QString name = ui->le_ObjectName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the object name!");
		return;
	}
	else if (!ui->le_ObjectSerial->text().length())
	{
		QMessageBox::critical(this, "Serial is empty", "Enter the object serial!");
		return;
	}

	IFOR(i, 0, ui->lw_Objects->count())
	{
		QListWidgetItem *item = ui->lw_Objects->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			QMessageBox::critical(this, "Name is already exists", "Object name is already exists!");
			return;
		}
	}

	new CObjectListItem(ui->le_ObjectName->text(), ui->le_ObjectSerial->text(), ui->lw_Objects);

	ui->lw_Objects->setCurrentRow(ui->lw_Objects->count() - 1);

	OnListItemMoved();
}
//----------------------------------------------------------------------------------
void TabListsObjects::on_pb_ObjectSave_clicked()
{
	OAFUN_DEBUG("");
	QString name = ui->le_ObjectName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the object name!");
		return;
	}
	else if (!ui->le_ObjectSerial->text().length())
	{
		QMessageBox::critical(this, "Serial is empty", "Enter the object serial!");
		return;
	}

	CObjectListItem *selected = (CObjectListItem*)ui->lw_Objects->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Object is not selected", "Object is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_Objects->count())
	{
		QListWidgetItem *item = ui->lw_Objects->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Name is already exists", "Object name is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->setText(ui->le_ObjectName->text());
	selected->SetSerial(COrionAssistant::TextToUInt(ui->le_ObjectSerial->text()));

	OnListItemMoved();
}
//----------------------------------------------------------------------------------
void TabListsObjects::on_pb_ObjectRemove_clicked()
{
	OAFUN_DEBUG("");
	QListWidgetItem *item = ui->lw_Objects->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_Objects->takeItem(ui->lw_Objects->row(item));

		if (item != nullptr)
		{
			delete item;
			OnListItemMoved();
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsObjects::on_pb_ObjectsSetDressBag_clicked()
{
	OAFUN_DEBUG("");
	CObjectListItem *selected = (CObjectListItem*)ui->lw_Objects->currentItem();

	if (selected != nullptr)
		SetDressBag(selected->GetSerial());
}
//----------------------------------------------------------------------------------
void TabListsObjects::on_pb_ObjectsUnsetDressBag_clicked()
{
	OAFUN_DEBUG("");
	SetDressBag(0);
}
//----------------------------------------------------------------------------------
void TabListsObjects::on_pb_ObjectDressBagFromTarget_clicked()
{
	OAFUN_DEBUG("");
	g_TargetHandler = &COrionAssistant::HandleTargetDressBag;

	g_OrionAssistant.RequestTarget("Select a dress bag.");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
void TabListsObjects::LoadObjects(const QString &path)
{
	OAFUN_DEBUG("");
	ui->lw_Objects->clear();

	QFile file(path);

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;
		int count = 0;

		Q_UNUSED(version);
		Q_UNUSED(count);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("size"))
						count = attributes.value("size").toInt();

					if (attributes.hasAttribute("dressbag"))
						SetDressBag(COrionAssistant::TextToSerial(attributes.value("dressbag").toString()));
				}
				else if (reader.name() == "object")
				{
					if (attributes.hasAttribute("name") && attributes.hasAttribute("id"))
					{
						new CObjectListItem(attributes.value("name").toString(), attributes.value("id").toString(), ui->lw_Objects);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsObjects::SaveObjects(const QString &path)
{
	OAFUN_DEBUG("");
	QFile file(path);

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_Objects->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");
		writter.writeAttribute("size", QString::number(count));
		writter.writeAttribute("dressbag", COrionAssistant::SerialToText(g_DressBag));

		IFOR(i, 0, count)
		{
			QListWidgetItem *item = ui->lw_Objects->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("object");

				writter.writeAttribute("name", item->text());
				writter.writeAttribute("id", COrionAssistant::SerialToText(((CObjectListItem*)item)->GetSerial()));

				writter.writeEndElement(); //object
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsObjects::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_Objects) //Objects
		on_pb_ObjectRemove_clicked();
}
//----------------------------------------------------------------------------------
uint TabListsObjects::SeekObject(const QString &text)
{
	OAFUN_DEBUG("");
	IFOR(i, 0, ui->lw_Objects->count())
	{
		QListWidgetItem *item = ui->lw_Objects->item(i);

		if (item != nullptr && item->text().toLower() == text)
			return ((CObjectListItem*)item)->GetSerial();
	}

	return 0;
}
//----------------------------------------------------------------------------------
void TabListsObjects::AddObject(QString name, const QString &object)
{
	OAFUN_DEBUG("");
	if (!object.length())
	{
		ui->le_ObjectName->setText(name);
		g_OrionAssistantForm->SetAutosaveAfterTargetingName(name);

		on_pb_ObjectFromTarget_clicked();

		return;
	}

	QString lowerName = name.toLower();

	IFOR(i, 0, ui->lw_Objects->count())
	{
		CObjectListItem *item = (CObjectListItem*)ui->lw_Objects->item(i);

		if (item != nullptr && item->text().toLower() == lowerName)
		{
			item->setText(name);
			item->SetSerial(COrionAssistant::TextToSerial(object));

			if (item == ui->lw_Objects->currentItem())
				ui->le_ObjectSerial->setText(object);

			return;
		}
	}

	new CObjectListItem(name, object, ui->lw_Objects);
}
//----------------------------------------------------------------------------------
void TabListsObjects::RemoveObject(QString name)
{
	OAFUN_DEBUG("");
	QString lowerName = name.toLower();

	IFOR(i, 0, ui->lw_Objects->count())
	{
		QListWidgetItem *item = ui->lw_Objects->item(i);

		if (item != nullptr && item->text().toLower() == lowerName)
		{
			item = ui->lw_Objects->takeItem(i);

			if (item != nullptr)
				delete item;
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsObjects::SetObjectFromTarget(const uint &serial)
{
	OAFUN_DEBUG("");
	if (serial)
	{
		ui->le_ObjectSerial->setText(COrionAssistant::SerialToText(serial));

		if (g_OrionAssistantForm->GetAutosaveAfterTargetingName().length())
			AddObject(g_OrionAssistantForm->GetAutosaveAfterTargetingName(), ui->le_ObjectSerial->text());
	}

	g_OrionAssistantForm->SetAutosaveAfterTargetingName("");
}
//----------------------------------------------------------------------------------
void TabListsObjects::SetDressBag(const uint &serial)
{
	OAFUN_DEBUG("");
	if (g_World != nullptr && serial)
	{
		CGameObject *obj = g_World->FindWorldObject(serial);

		if (obj != nullptr && !obj->GetNPC() && obj->GetGraphic())
		{
			if (!IsContainer(g_GetStaticFlags(obj->GetGraphic())))
			{
				g_OrionAssistant.ClientPrintDebugLevel1("This item is not a container!", 0x0021);
				return;
			}
		}
	}

	g_DressBag = serial;

	ui->l_DressBag->setText(COrionAssistant::SerialToText(g_DressBag));

	if (g_DressBag)
	{
		if (!g_OrionAssistantForm->GetNoExecuteGUIEvents())
			g_OrionAssistant.ClientPrintDebugLevel1("Dress bag set!", 0x0044);

		ui->l_DressBag->setStyleSheet("color: rgb(0, 170, 0);");
	}
	else
	{
		if (!g_OrionAssistantForm->GetNoExecuteGUIEvents())
			g_OrionAssistant.ClientPrintDebugLevel1("Dress bag unset!", 0x0044);

		ui->l_DressBag->setStyleSheet("color: rgb(170, 0, 0);");
	}
}
//----------------------------------------------------------------------------------
