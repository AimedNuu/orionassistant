// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabFiltersSound.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabfilterssound.h"
#include "ui_tabfilterssound.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include "tabmain.h"
#include "../GUI/filtersoundlistitem.h"
#include "tablistsfriends.h"
#include "tablistsenemies.h"
#include "Packets.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
//----------------------------------------------------------------------------------
TabFiltersSound *g_TabFiltersSound = nullptr;
//----------------------------------------------------------------------------------
TabFiltersSound::TabFiltersSound(QWidget *parent)
: QWidget(parent), ui(new Ui::TabFiltersSound)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabFiltersSound = this;

	connect(ui->lw_FiltersSoundList, SIGNAL(itemDropped()), this, SLOT(SaveSoundFilter()));
}
//----------------------------------------------------------------------------------
TabFiltersSound::~TabFiltersSound()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabFiltersSound = nullptr;
}
//----------------------------------------------------------------------------------
void TabFiltersSound::on_lw_FiltersSoundList_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f253");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	CFilterSoundListItem *item = (CFilterSoundListItem*)ui->lw_FiltersSoundList->item(index.row());

	if (item != nullptr)
	{
		ui->le_FilterSoundIndex->setText(item->GetIndex());
		ui->le_FilterSoundName->setText(item->GetName());
	}
}
//----------------------------------------------------------------------------------
void TabFiltersSound::on_pb_FilterSoundAdd_clicked()
{
	OAFUN_DEBUG("c28_f254");
	ushort index = COrionAssistant::TextToGraphic(ui->le_FilterSoundIndex->text());

	if (!index)
	{
		QMessageBox::critical(this, "Index is empty", "Enter the sound index!");
		return;
	}

	IFOR(i, 0, ui->lw_FiltersSoundList->count())
	{
		CFilterSoundListItem *item = (CFilterSoundListItem*)ui->lw_FiltersSoundList->item(i);

		if (item != nullptr && COrionAssistant::TextToGraphic(item->GetIndex()) == index)
		{
			QMessageBox::critical(this, "Index is already exists", "Sound index is already exists!");
			return;
		}
	}

	new CFilterSoundListItem(ui->le_FilterSoundIndex->text(), ui->le_FilterSoundName->text(), ui->lw_FiltersSoundList);

	ui->lw_FiltersSoundList->setCurrentRow(ui->lw_FiltersSoundList->count() - 1);
	SaveSoundFilter();
}
//----------------------------------------------------------------------------------
void TabFiltersSound::on_pb_FilterSoundSave_clicked()
{
	OAFUN_DEBUG("c28_f255");
	ushort index = COrionAssistant::TextToGraphic(ui->le_FilterSoundIndex->text());

	if (!index)
	{
		QMessageBox::critical(this, "Index is empty", "Enter the sound index!");
		return;
	}

	CFilterSoundListItem *selected = (CFilterSoundListItem*)ui->lw_FiltersSoundList->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Sound is not selected", "Sound is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_FiltersSoundList->count())
	{
		CFilterSoundListItem *item = (CFilterSoundListItem*)ui->lw_FiltersSoundList->item(i);

		if (item != nullptr && COrionAssistant::TextToGraphic(item->GetIndex()) == index)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Index is already exists", "Sound index is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->SetIndex(ui->le_FilterSoundIndex->text());
	selected->SetName(ui->le_FilterSoundName->text());
	selected->UpdateText();
	SaveSoundFilter();
}
//----------------------------------------------------------------------------------
void TabFiltersSound::on_pb_FilterSoundRemove_clicked()
{
	OAFUN_DEBUG("c28_f256");
	QListWidgetItem *item = ui->lw_FiltersSoundList->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_FiltersSoundList->takeItem(ui->lw_FiltersSoundList->row(item));

		if (item != nullptr)
		{
			delete item;
			SaveSoundFilter();
		}
	}
}
//----------------------------------------------------------------------------------
void TabFiltersSound::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_FiltersSoundList)
		on_pb_FilterSoundRemove_clicked();
}
//----------------------------------------------------------------------------------
bool TabFiltersSound::CheckSoundInFilter(const ushort &index)
{
	OAFUN_DEBUG("c28_f259");
	if (g_TabMain->SoundFilter())
	{
		IFOR(i, 0, ui->lw_FiltersSoundList->count())
		{
			CFilterSoundListItem *item = (CFilterSoundListItem*)ui->lw_FiltersSoundList->item(i);

			if (item != nullptr && COrionAssistant::TextToGraphic(item->GetIndex()) == index)
				return true;
		}
	}

	return false;
}
//----------------------------------------------------------------------------------
void TabFiltersSound::LoadSoundFilter()
{
	OAFUN_DEBUG("c28_f266");
	ui->lw_FiltersSoundList->clear();

	QFile file(g_DllPath + "/GlobalConfig/SoundFilter.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;

		Q_UNUSED(version);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();
				}
				else if (reader.name() == "sound")
				{
					if (attributes.hasAttribute("index") && attributes.hasAttribute("name"))
					{
						new CFilterSoundListItem(attributes.value("index").toString(), attributes.value("name").toString(), ui->lw_FiltersSoundList);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabFiltersSound::SaveSoundFilter()
{
	OAFUN_DEBUG("c28_f265");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/SoundFilter.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_FiltersSoundList->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");

		IFOR(i, 0, count)
		{
			CFilterSoundListItem *item = (CFilterSoundListItem*)ui->lw_FiltersSoundList->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("sound");

				writter.writeAttribute("index", item->GetIndex());
				writter.writeAttribute("name", item->GetName());

				writter.writeEndElement(); //sound
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
