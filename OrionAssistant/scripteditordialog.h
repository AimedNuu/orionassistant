/***********************************************************************************
**
** ScriptEditorDialog.h
**
** Copyright (C) January 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SCRIPTEDITORDIALOG_H
#define SCRIPTEDITORDIALOG_H
//----------------------------------------------------------------------------------
#include <QMainWindow>
#include "TextOperationDialog/findtextdialog.h"
#include "TextOperationDialog/gotolinedialog.h"
#include "TextOperationDialog/replacetextdialog.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class ScriptEditorDialog;
}
//----------------------------------------------------------------------------------
class ScriptEditorDialog : public QMainWindow
{
	Q_OBJECT

private:
	Ui::ScriptEditorDialog *ui{nullptr};

	CFindTextDialog m_FindDialog;
	CGoToLineDialog m_GoToLineDialog;
	CReplaceTextDialog m_ReplaceDialog;

	void GoToLine(const int &lineIndex);

public:
	explicit ScriptEditorDialog(QWidget *parent = 0);
	~ScriptEditorDialog();

	void ClearText();
	void SetText(const QString &text);
	void AddText(const QString &text);
	QString GetText();

	QStringList GetFunctionList();

	void LoadScript(const QString &filePath);

	void GoLineText(const QString &text);

private slots:
	void on_act_NewScript_triggered();
	void on_act_OpenScript_triggered();
	void on_act_SaveScript_triggered();
	void on_act_SaveAsScript_triggered();
	void on_act_FindText_triggered();
	void on_act_GoToLine_triggered();
	void on_act_ReplaceText_triggered();
	void on_act_CommentSelectedTextLines_triggered();

	void slot_SearchText(QString text, bool caseSensitive, bool searchDown);
	void slot_GoToLine(int lineIndex);
	void slot_ReplaceText(QString text, QString replace, bool caseSensitive, bool searchDown, bool all);
	void slot_CtrlQPressed();
};
//----------------------------------------------------------------------------------
extern ScriptEditorDialog *g_ScriptEditorDialog;
//----------------------------------------------------------------------------------
#endif // SCRIPTEDITORDIALOG_H
//----------------------------------------------------------------------------------
