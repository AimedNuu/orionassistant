// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabListsFriends.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tablistsfriends.h"
#include "ui_tablistsfriends.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include "../GUI/worldobjectlistitem.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
#include "../GameObjects/GameWorld.h"
#include "../CommonItems/objectnamereceiver.h"
#include "../Managers/partymanager.h"
//----------------------------------------------------------------------------------
TabListsFriends *g_TabListsFriends = nullptr;
//----------------------------------------------------------------------------------
TabListsFriends::TabListsFriends(QWidget *parent)
: QWidget(parent), ui(new Ui::TabListsFriends)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabListsFriends = this;

	connect(ui->lw_Friends, SIGNAL(itemDropped()), this, SLOT(OnListItemMoved()));
}
//----------------------------------------------------------------------------------
TabListsFriends::~TabListsFriends()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabListsFriends = nullptr;
}
//----------------------------------------------------------------------------------
void TabListsFriends::OnListItemMoved()
{
	OAFUN_DEBUG("");
	QString path = g_OrionAssistantForm->GetSaveConfigPath();

	if (!path.length())
		return;

	SaveFriends(path + "/Friends.xml");
}
//----------------------------------------------------------------------------------
void TabListsFriends::on_lw_Friends_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f215");
	CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Friends->item(index.row());

	if (item != nullptr)
	{
		ui->le_FriendName->setText(item->GetName());
		ui->le_FriendSerial->setText(COrionAssistant::SerialToText(item->GetSerial()));
	}
}
//----------------------------------------------------------------------------------
void TabListsFriends::on_pb_FriendFromTarget_clicked()
{
	OAFUN_DEBUG("c28_f219");
	g_TargetHandler = &COrionAssistant::HandleTargetFriendObject;

	g_OrionAssistant.RequestTarget("Select a object for obtain serial");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
void TabListsFriends::on_pb_FriendCreate_clicked()
{
	OAFUN_DEBUG("c28_f216");
	uint serial = COrionAssistant::TextToSerial(ui->le_FriendSerial->text());

	if (!serial)
	{
		QMessageBox::critical(this, "Serial is empty", "Enter the object serial!");
		return;
	}

	IFOR(i, 0, ui->lw_Friends->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Friends->item(i);

		if (item != nullptr && item->GetSerial() == serial)
		{
			QMessageBox::critical(this, "Serial is already exists", "Object serial is already exists!");
			return;
		}
	}

	new CWorldObjectListItem(serial, ui->le_FriendName->text(), true, ui->lw_Friends);

	ui->lw_Friends->setCurrentRow(ui->lw_Friends->count() - 1);

	OnListItemMoved();
}
//----------------------------------------------------------------------------------
void TabListsFriends::on_pb_FriendSave_clicked()
{
	OAFUN_DEBUG("c28_f217");
	uint serial = COrionAssistant::TextToSerial(ui->le_FriendSerial->text());

	if (!serial)
	{
		QMessageBox::critical(this, "Serial is empty", "Enter the object serial!");
		return;
	}

	CWorldObjectListItem *selected = (CWorldObjectListItem*)ui->lw_Friends->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Object is not selected", "Object is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_Friends->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Friends->item(i);

		if (item != nullptr && item->GetSerial() == serial)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Serial is already exists", "Object serial is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->SetSerial(serial);
	selected->SetName(ui->le_FriendName->text());
	selected->UpdateText();

	OnListItemMoved();
}
//----------------------------------------------------------------------------------
void TabListsFriends::on_pb_FriendRemove_clicked()
{
	OAFUN_DEBUG("c28_f218");
	QListWidgetItem *item = ui->lw_Friends->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_Friends->takeItem(ui->lw_Friends->row(item));

		if (item != nullptr)
		{
			delete item;
			OnListItemMoved();
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsFriends::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_Friends)
		on_pb_FriendRemove_clicked();
}
//----------------------------------------------------------------------------------
void TabListsFriends::LoadFriends(const QString &path)
{
	OAFUN_DEBUG("c28_f130");
	ui->lw_Friends->clear();

	QFile file(path);

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;
		int count = 0;

		Q_UNUSED(version);
		Q_UNUSED(count);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("size"))
						count = attributes.value("size").toInt();

					if (attributes.hasAttribute("feel_party_members_as_friends"))
						ui->cb_FeelPartyMembersAsFriends->setChecked(COrionAssistant::RawStringToBool(attributes.value("feel_party_members_as_friends").toString()));
				}
				else if (reader.name() == "friend")
				{
					if (attributes.hasAttribute("id") && attributes.hasAttribute("name") && attributes.hasAttribute("enabled"))
					{
						new CWorldObjectListItem(COrionAssistant::TextToSerial(attributes.value("id").toString()), attributes.value("name").toString(),
										   COrionAssistant::RawStringToBool(attributes.value("enabled").toString()), ui->lw_Friends);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsFriends::SaveFriends(const QString &path)
{
	OAFUN_DEBUG("c28_f138");
	QFile file(path);

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_Friends->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");
		writter.writeAttribute("size", QString::number(count));

		writter.writeAttribute("feel_party_members_as_friends", COrionAssistant::BoolToText(ui->cb_FeelPartyMembersAsFriends->isChecked()));

		IFOR(i, 0, count)
		{
			CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Friends->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("friend");

				writter.writeAttribute("id", COrionAssistant::SerialToText(item->GetSerial()));
				writter.writeAttribute("name", item->GetName());
				writter.writeAttribute("enabled", COrionAssistant::BoolToText(item->checkState() == Qt::Checked));

				writter.writeEndElement(); //friend
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsFriends::SetFriendObjectFromTarget(const uint &serial)
{
	OAFUN_DEBUG("c28_f220");
	if (serial)
	{
		ui->le_FriendSerial->setText(COrionAssistant::SerialToText(serial));

		if (g_World != nullptr)
		{
			CGameObject *obj = g_World->FindWorldObject(serial);

			if (obj != nullptr)
				ui->le_FriendName->setText(obj->GetName());
			else
			{
				g_OrionAssistant.Click(serial);
				new CObjectNameReceiver(serial, ui->le_FriendName);
			}
		}
	}
}
//----------------------------------------------------------------------------------
bool TabListsFriends::InFriendList(const uint &serial)
{
	OAFUN_DEBUG("c28_f230");

	if (ui->cb_FeelPartyMembersAsFriends->isChecked() && g_PartyManager.Contains(serial))
		return true;

	int count = ui->lw_Friends->count();

	IFOR(i, 0, count)
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Friends->item(i);

		if (item != nullptr && item->checkState() == Qt::Checked && item->GetSerial() == serial)
			return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
QStringList TabListsFriends::GetFriendList()
{
	OAFUN_DEBUG("c28_f231");
	int count = ui->lw_Friends->count();
	QStringList result;

	IFOR(i, 0, count)
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Friends->item(i);

		if (item != nullptr && item->checkState() == Qt::Checked)
			result << COrionAssistant::SerialToText(item->GetSerial());
	}

	return result;
}
//----------------------------------------------------------------------------------
void TabListsFriends::AddFriend(QString name, const QString &object)
{
	OAFUN_DEBUG("c28_f237");
	if (!object.length())
	{
		ui->le_FriendName->setText(name);
		g_OrionAssistantForm->SetAutosaveAfterTargetingName(name);

		on_pb_FriendFromTarget_clicked();

		return;
	}

	QString lowerName = name.toLower();

	IFOR(i, 0, ui->lw_Friends->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Friends->item(i);

		if (item != nullptr && item->text().toLower() == lowerName)
		{
			item->SetName(name);
			item->SetSerial(COrionAssistant::TextToSerial(object));
			return;
		}
	}

	new CWorldObjectListItem(COrionAssistant::TextToSerial(object), name, true, ui->lw_Friends);
}
//----------------------------------------------------------------------------------
void TabListsFriends::RemoveFriend(QString name)
{
	OAFUN_DEBUG("c28_f238");
	QString lowerName = name.toLower();

	IFOR(i, 0, ui->lw_Friends->count())
	{
		QListWidgetItem *item = ui->lw_Friends->item(i);

		if (item != nullptr && item->text().toLower() == lowerName)
		{
			item = ui->lw_Friends->takeItem(i);

			if (item != nullptr)
				delete item;
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsFriends::ClearFriendList()
{
	OAFUN_DEBUG("c28_f239");
	ui->lw_Friends->clear();
}
//----------------------------------------------------------------------------------
