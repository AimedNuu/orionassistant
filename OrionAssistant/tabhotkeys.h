/***********************************************************************************
**
** TabHotkeys.h
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABHOTKEYS_H
#define TABHOTKEYS_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabHotkeys;
}
//----------------------------------------------------------------------------------
class TabHotkeys : public QWidget
{
	Q_OBJECT

private:
	Ui::TabHotkeys *ui;

private slots:
	void on_pb_OpenHotkeysDialog_clicked();

	void on_pb_SaveHotkeys_clicked();

	void on_pb_LoadHotkeys_clicked();

	void on_pb_AnimalAndVendorControl_clicked();

	void on_pb_HouseControl_clicked();

	void on_pb_ShipControl_clicked();

	void on_cb_OnOffHotkeys_clicked();

public:
	explicit TabHotkeys(QWidget *parent = 0);
	~TabHotkeys();

	int IsCommandPrefix(QString text);

	void LoadConfig(const QXmlStreamAttributes &attributes);

	void SaveConfig(QXmlStreamWriter &writter);

	bool OnOffHotkeys();

	void SetOnOffHotkeys(const bool &state);

	bool PassHotkeys();
};
//----------------------------------------------------------------------------------
extern TabHotkeys *g_TabHotkeys;
//----------------------------------------------------------------------------------
#endif // TABHOTKEYS_H
//----------------------------------------------------------------------------------
