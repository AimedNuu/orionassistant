/***********************************************************************************
**
** TabListsTypes.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABLISTSTYPES_H
#define TABLISTSTYPES_H
//----------------------------------------------------------------------------------
#include <QWidget>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabListsTypes;
}
//----------------------------------------------------------------------------------
class TabListsTypes : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_Types_clicked(const QModelIndex &index);

	void on_pb_TypeFromTarget_clicked();

	void on_pb_TypeCreate_clicked();

	void on_pb_TypeSave_clicked();

	void on_pb_TypeRemove_clicked();

private:
	Ui::TabListsTypes *ui;

public slots:
	void SaveTypes();

public:
	explicit TabListsTypes(QWidget *parent = 0);
	~TabListsTypes();

	void LoadTypes();

	void AddType(QString name, const QString &type);

	void RemoveType(QString name);

	ushort SeekType(const QString &text);

	void SetTypeFromTarget(const ushort &graphic);

	void OnDeleteKeyClick(QWidget *widget);
};
//----------------------------------------------------------------------------------
extern TabListsTypes *g_TabListsTypes;
//----------------------------------------------------------------------------------
#endif // TABLISTSTYPES_H
//----------------------------------------------------------------------------------
