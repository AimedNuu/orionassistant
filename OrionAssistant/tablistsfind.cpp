// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabListsFind.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tablistsfind.h"
#include "ui_tablistsfind.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include "../GameObjects/GameWorld.h"
#include "../GUI/findlistitemlistitem.h"
#include "../CommonItems/objectnamereceiver.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
//----------------------------------------------------------------------------------
TabListsFind *g_TabListsFind = nullptr;
//----------------------------------------------------------------------------------
TabListsFind::TabListsFind(QWidget *parent)
: QWidget(parent), ui(new Ui::TabListsFind)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabListsFind = this;

	connect(ui->lw_Find, SIGNAL(itemDropped()), this, SLOT(SaveFindList()));
	connect(ui->lw_FindContent, SIGNAL(itemDropped()), this, SLOT(SaveFindList()));
}
//----------------------------------------------------------------------------------
TabListsFind::~TabListsFind()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabListsFind = nullptr;
}
//----------------------------------------------------------------------------------
void TabListsFind::on_lw_Find_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f172");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	CFindListItem *item = (CFindListItem*)ui->lw_Find->item(index.row());

	if (item != nullptr)
	{
		ui->le_FindName->setText(item->text());

		ui->lw_FindContent->clear();

		QList<CFindItem> &list = item->m_Items;

		if (!list.empty())
		{
			for (const CFindItem &i : list)
			{
				CFindListItemListItem *obj = new CFindListItemListItem(ui->lw_FindContent);
				obj->SetGraphic(i.GetGraphic());
				obj->SetColor(i.GetColor());
				obj->SetComment(i.GetComment());
				obj->UpdateText();
			}
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsFind::on_pb_FindCreate_clicked()
{
	OAFUN_DEBUG("c28_f173");
	QString name = ui->le_FindName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the find list name!");
		return;
	}

	IFOR(i, 0, ui->lw_Find->count())
	{
		QListWidgetItem *item = ui->lw_Find->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			QMessageBox::critical(this, "Name is already exists", "Find list name is already exists!");
			return;
		}
	}

	ui->lw_FindContent->clear();

	new CFindListItem(ui->le_FindName->text(), ui->lw_Find);

	ui->lw_Find->setCurrentRow(ui->lw_Find->count() - 1);

	SaveFindList();
}
//----------------------------------------------------------------------------------
void TabListsFind::on_pb_FindSave_clicked()
{
	OAFUN_DEBUG("c28_f174");
	QString name = ui->le_FindName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the find list name!");
		return;
	}

	CFindListItem *selected = (CFindListItem*)ui->lw_Find->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "List is not selected", "List is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_Find->count())
	{
		QListWidgetItem *item = ui->lw_Find->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Name is already exists", "Find list name is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->setText(ui->le_FindName->text());

	QList<CFindItem> &list = selected->m_Items;

	list.clear();

	for (int i = 0; i < ui->lw_FindContent->count(); i++)
	{
		CFindListItemListItem *item = (CFindListItemListItem*)ui->lw_FindContent->item(i);

		CFindItem fi(item->GetGraphic(), item->GetColor());
		fi.SetComment(item->GetComment());
		list.push_back(fi);
	}

	SaveFindList();
}
//----------------------------------------------------------------------------------
void TabListsFind::on_pb_FindRemove_clicked()
{
	OAFUN_DEBUG("c28_f175");
	QListWidgetItem *item = ui->lw_Find->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_Find->takeItem(ui->lw_Find->row(item));

		if (item != nullptr)
		{
			delete item;

			ui->lw_FindContent->clear();

			SaveFindList();
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsFind::on_lw_FindContent_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f176");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents() || ui->lw_Find->currentItem() == nullptr)
		return;

	CFindListItemListItem *item = (CFindListItemListItem*)ui->lw_FindContent->item(index.row());

	if (item != nullptr)
	{
		ui->le_FindContentGraphic->setText(item->GetGraphic());
		ui->le_FindContentColor->setText(item->GetColor());
		ui->le_FindContentComment->setText(item->GetComment());
	}
}
//----------------------------------------------------------------------------------
void TabListsFind::on_pb_FindContentFromTarget_clicked()
{
	OAFUN_DEBUG("c28_f180");
	if (ui->lw_Find->currentItem() == nullptr)
		return;

	g_TargetHandler = &COrionAssistant::HandleTargetFindList;

	g_OrionAssistant.RequestTarget("Select a object for obtain graphic and color");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
void TabListsFind::on_pb_FindContentCreate_clicked()
{
	OAFUN_DEBUG("c28_f177");
	if (ui->lw_Find->currentItem() == nullptr)
		return;

	QString graphic = ui->le_FindContentGraphic->text();

	if (!graphic.length())
		graphic = "0xFFFF";

	QString color = ui->le_FindContentColor->text();

	if (!color.length())
		color = "0xFFFF";

	CFindListItemListItem *obj = new CFindListItemListItem(ui->lw_FindContent);
	obj->SetGraphic(graphic);
	obj->SetColor(color);
	obj->SetComment(ui->le_FindContentComment->text());
	obj->UpdateText();

	ui->lw_FindContent->setCurrentRow(ui->lw_FindContent->count() - 1);

	on_pb_FindSave_clicked();
}
//----------------------------------------------------------------------------------
void TabListsFind::on_pb_FindContentSave_clicked()
{
	OAFUN_DEBUG("c28_f178");
	if (ui->lw_Find->currentItem() == nullptr)
		return;

	QString graphic = ui->le_FindContentGraphic->text();

	if (!graphic.length())
		graphic = "0xFFFF";

	QString color = ui->le_FindContentColor->text();

	if (!color.length())
		color = "0xFFFF";

	CFindListItemListItem *obj = (CFindListItemListItem*)ui->lw_FindContent->currentItem();

	if (obj != nullptr)
	{
		obj->SetGraphic(graphic);
		obj->SetColor(color);
		obj->SetComment(ui->le_FindContentComment->text());
		obj->UpdateText();

		on_pb_FindSave_clicked();
	}
	else
	{
		QMessageBox::critical(this, "Item is not selected", "Item is not selected!");
		return;
	}
}
//----------------------------------------------------------------------------------
void TabListsFind::on_pb_FindContentRemove_clicked()
{
	OAFUN_DEBUG("c28_f179");
	if (ui->lw_Find->currentItem() == nullptr)
		return;

	QListWidgetItem *item = ui->lw_FindContent->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_FindContent->takeItem(ui->lw_FindContent->row(item));

		if (item != nullptr)
		{
			delete item;

			on_pb_FindSave_clicked();
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsFind::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_Find)
		on_pb_FindRemove_clicked();
	else if (widget == ui->lw_FindContent)
		on_pb_FindContentRemove_clicked();
}
//----------------------------------------------------------------------------------
void TabListsFind::LoadFindList()
{
	OAFUN_DEBUG("c28_f126");
	ui->lw_Find->clear();

	QFile file(g_DllPath + "/GlobalConfig/FindList.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;
		int count = 0;

		Q_UNUSED(version);
		Q_UNUSED(count);

		CFindListItem *current = nullptr;

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("size"))
						count = attributes.value("size").toInt();
				}
				else if (reader.name() == "findlist")
				{
					if (attributes.hasAttribute("name"))
						current = new CFindListItem(attributes.value("name").toString(), ui->lw_Find);
				}
				else if (reader.name() == "item" && current != nullptr)
				{
					if (attributes.hasAttribute("graphic") && attributes.hasAttribute("color"))
					{
						CFindItem fi(attributes.value("graphic").toString(), attributes.value("color").toString());

						if (attributes.hasAttribute("comment"))
							fi.SetComment(attributes.value("comment").toString());

						current->m_Items.push_back(fi);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsFind::SaveFindList()
{
	OAFUN_DEBUG("c28_f135");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/FindList.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_Find->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");
		writter.writeAttribute("size", QString::number(count + 1));

		IFOR(i, 0, count)
		{
			CFindListItem *item = (CFindListItem*)ui->lw_Find->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("findlist");

				writter.writeAttribute("name", item->text());

				QList<CFindItem> &list = item->m_Items;

				if (!list.empty())
				{
					for (const CFindItem &fi : list)
					{
						writter.writeStartElement("item");

						writter.writeAttribute("graphic", fi.GetGraphic());
						writter.writeAttribute("color", fi.GetColor());
						writter.writeAttribute("comment", fi.GetComment());

						writter.writeEndElement(); //item
					}
				}

				writter.writeEndElement(); //findlist
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsFind::AddFindListItemInList(class CGameObject *obj)
{
	OAFUN_DEBUG("c28_f181");
	if (obj != nullptr)
	{
		ui->le_FindContentGraphic->setText(COrionAssistant::GraphicToText(obj->GetGraphic()));
		ui->le_FindContentColor->setText(COrionAssistant::GraphicToText(obj->GetColor()));

		if (obj->GetName().length())
			ui->le_FindContentComment->setText(obj->GetName());
		else
		{
			g_OrionAssistant.Click(obj->GetSerial());
			new CObjectNameReceiver(obj->GetSerial(), ui->le_FindContentComment);
		}
	}
}
//----------------------------------------------------------------------------------
CFindListItem *TabListsFind::GetFindList(QString name)
{
	OAFUN_DEBUG("c28_f198");
	name = name.toLower();

	IFOR(i, 0, ui->lw_Find->count())
	{
		QListWidgetItem *item = ui->lw_Find->item(i);

		if (item != nullptr && item->text().toLower() == name)
			return (CFindListItem*)item;
	}

	return nullptr;
}
//----------------------------------------------------------------------------------
void TabListsFind::AddFindList(QString listName, const QString &graphic, const QString &color, const QString &comment)
{
	OAFUN_DEBUG("c28_f182");
	if (!listName.length())
	{
		on_pb_FindContentFromTarget_clicked();
		return;
	}

	QString name = listName.toLower();

	CFindListItem *item = nullptr;

	IFOR(i, 0, ui->lw_Find->count())
	{
		item = (CFindListItem*)ui->lw_Find->item(i);

		if (item != nullptr && item->text().toLower() == name)
			break;
	}

	if (item == nullptr)
		item = new CFindListItem(listName, ui->lw_Find);

	if (!item->m_Items.empty())
	{
		for (const CFindItem &fi : item->m_Items)
		{
			if (fi.GetGraphic() == graphic && fi.GetColor() == color)
				return;
		}
	}

	CFindItem fi(graphic, color);
	fi.SetComment(comment);
	item->m_Items.push_back(fi);

	SaveFindList();
}
//----------------------------------------------------------------------------------
void TabListsFind::ClearFindList(QString listName)
{
	OAFUN_DEBUG("c28_f183");
	listName = listName.toLower();

	IFOR(i, 0, ui->lw_Find->count())
	{
		QListWidgetItem *item = ui->lw_Find->item(i);

		if (item != nullptr && item->text().toLower() == listName)
		{
			if (item == ui->lw_Find->currentItem())
				ui->lw_FindContent->clear();

			item = ui->lw_Find->takeItem(i);

			delete item;

			SaveFindList();

			break;
		}
	}
}
//----------------------------------------------------------------------------------
