// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ShopProgressForm.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "shopprogressform.h"
#include "ui_shopprogressform.h"
#include "orionassistant.h"
#include "Packets.h"
//----------------------------------------------------------------------------------
ShopProgressForm *g_ShopProgressForm = nullptr;
//----------------------------------------------------------------------------------
ShopProgressForm::ShopProgressForm(QWidget *parent)
: QDialog(parent), ui(new Ui::ShopProgressForm)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	setFixedSize(size());

	g_ShopProgressForm = this;

	connect(&m_Timer, SIGNAL(timeout()), this, SLOT(on_Timer()));
}
//----------------------------------------------------------------------------------
ShopProgressForm::~ShopProgressForm()
{
	OAFUN_DEBUG("");
	if (m_Timer.isActive())
		m_Timer.stop();

	delete ui;

	g_ShopProgressForm = nullptr;
}
//----------------------------------------------------------------------------------
void ShopProgressForm::closeEvent(QCloseEvent *e)
{
	OAFUN_DEBUG("");
	hide();
	e->accept();
}
//----------------------------------------------------------------------------------
void ShopProgressForm::on_Timer()
{
	OAFUN_DEBUG("");
	int elapsed = m_Time.elapsed();

	if (elapsed >= m_Delay)
	{
		g_OrionAssistant.ClientPrintDebugLevel1("Shopping completed!");
		ui->pb_Progress->setValue(0);
		m_Timer.stop();

		if (m_Packet != nullptr)
		{
			m_Packet->SendServer();

			delete m_Packet;
			m_Packet = nullptr;
		}

		hide();
	}
	else
		ui->pb_Progress->setValue(m_Delay - elapsed);
}
//----------------------------------------------------------------------------------
void ShopProgressForm::on_pb_Cancel_clicked()
{
	OAFUN_DEBUG("");
	g_OrionAssistant.ClientPrintDebugLevel1("Shopping cancelled by user");
	m_Timer.stop();

	if (m_Packet != nullptr)
	{
		delete m_Packet;
		m_Packet = nullptr;
	}

	hide();
}
//----------------------------------------------------------------------------------
bool ShopProgressForm::IsActiveShpooing()
{
	OAFUN_DEBUG("");
	return (m_Timer.isActive() && m_Packet != nullptr);
}
//----------------------------------------------------------------------------------
void ShopProgressForm::Start(const int &delay, CPacket *packet, const bool &showWindow)
{
	OAFUN_DEBUG("");
	if (m_Timer.isActive())
	{
		g_OrionAssistant.ClientPrintDebugLevel1("Preveous shopping cancelled by new request");
		m_Timer.stop();
	}

	if (m_Packet != nullptr)
	{
		delete m_Packet;
		m_Packet = nullptr;
	}

	m_Packet = packet;

	m_Delay = delay;

	m_Time.restart();

	ui->pb_Progress->setMaximum(delay);
	ui->pb_Progress->setValue(delay);

	m_Timer.start(50);

	if (showWindow)
	{
		if (isVisible())
			activateWindow();
		else
			show();
	}
	else
		hide();
}
//----------------------------------------------------------------------------------
