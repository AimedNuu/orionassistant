/***********************************************************************************
**
** TabAbout.h
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABABOUT_H
#define TABABOUT_H
//----------------------------------------------------------------------------------
#include <QWidget>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabAbout;
}
//----------------------------------------------------------------------------------
class TabAbout : public QWidget
{
	Q_OBJECT

public:
	explicit TabAbout(QWidget *parent = 0);
	~TabAbout();

private slots:
	void on_tb_Support_anchorClicked(const QUrl &arg1);

	void on_pb_GitHub_clicked();

	void on_pb_Forum_clicked();

	void on_pb_Discord_clicked();

	void on_pb_WIKI_clicked();

private:
	Ui::TabAbout *ui;
};
//----------------------------------------------------------------------------------
#endif // TABABOUT_H
//----------------------------------------------------------------------------------
