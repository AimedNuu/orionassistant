/***********************************************************************************
**
** OrionMap.h
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef ORIONMAP_H
#define ORIONMAP_H
//----------------------------------------------------------------------------------
#include <QMainWindow>
#include "../orionassistant_global.h"
#include "mapsettingsform.h"
#include "../GameObjects/GameCharacter.h"
#include <QPainter>
#include <QTimer>
//----------------------------------------------------------------------------------
namespace Ui
{
	class OrionMap;
}
//----------------------------------------------------------------------------------
enum ORION_MAP_CHARACTER_TYPE
{
	OMCT_PLAYER = 0,
	OMCT_PARTY_MEMBER,
	OMCT_GUILD_MEMBER,
	OMCT_FRIEND,
	OMCT_ENEMY,
	OMCT_OTHER,
	OMCT_CORPSE,
	OMCT_INVALID
};
//----------------------------------------------------------------------------------
enum ORION_MAP_ACTION
{
	OMA_FACET = 1,
	OMA_FOLLOW_CHARACTER,
	OMA_ZOOM,
	OMA_SHOW_STATICS,
	OMA_APPLY_ALTITUDE,
	OMA_ALTITUDE_ONLY,
	OMA_SMOOTH_IMAGE,
	OMA_TRANSPARENT_WINDOW,
	OMA_DISPLAY_COORDINATES,
	OMA_SHOW_ON_LOGIN,
	OMA_OPEN_SETTINGS,
	OMA_ROTATE_MAP
};
//----------------------------------------------------------------------------------
enum ORION_MAP_DRAW_OBJECT_STYLE
{
	OMDOS_NORMAL = 0,
	OMDOS_DEAD,
	OMDOS_OUT_OF_RANGE
};
//----------------------------------------------------------------------------------
class COrionMapDrawObject
{
	SETGET(ORION_MAP_CHARACTER_TYPE, Type, OMCT_INVALID)
	SETGET(int, X, 0)
	SETGET(int, Y, 0)
	SETGET(QString, Name, "")
	SETGETP(CGameCharacter*, Ptr, nullptr)

public:
	COrionMapDrawObject(const ORION_MAP_CHARACTER_TYPE &type, const int &x, const int &y, const QString &name, CGameCharacter *ptr)
		: m_Type(type), m_X(x), m_Y(y), m_Name(name), m_Ptr(ptr) {}
};
//----------------------------------------------------------------------------------
class OrionMap : public QMainWindow
{
	Q_OBJECT

	SETGET(bool, WantRedraw, true)
	SETGET(bool, WantUpdateImage, true)

private:
	Ui::OrionMap *ui;

	uint m_MapHash{ 0 };
	int m_MapWidth{ 0 };
	int m_MapHeight{ 0 };
	ushort **m_Pixels{ nullptr };
	//USHORT_LIST m_Pixels;
	QImage m_MapImage;

	int m_CacheX{ 0 };
	int m_CacheY{ 0 };
	int m_CacheWidth{ 0 };
	int m_CacheHeight{ 0 };
	uint m_CacheMap{ 0 };
	bool m_CacheRotate{ false };
	float m_CacheScale{ 0.0f };
	USHORT_LIST m_CachePixels;
	QImage m_CacheImage;
	bool m_CacheNewImage{ false };

	bool m_Painting{ false };
	float m_Scale{ 0.5f };

	QPoint m_MousePressPoint{ QPoint() };
	bool m_MousePressed{ false };

	QPoint m_PressedWorldCoordinatesOffset{ QPoint() };
	QPoint m_CurrentWorldCoordinates{ QPoint(507, 840) };

	QImage m_ImageCorpse;

	QTimer m_Timer;
	QTimer m_RequestsTimer;

	MapSettingsForm *m_SettingsForm{ nullptr };

	ORION_MAP_CHARACTER_TYPE GetCharacterType(const uint &serial);

	void UpdateScale();

	void UpdateImage();

	bool BuildImage(QImage &imag, QRect &rect, const int &cx, const int &cy, int width, int height);

	void DrawLine(const int &x, const int &y, QPainter &painter, const QPen &pen, int value, const int &maxValue);

	void DrawMode0(QPainter &painter);

	void DrawMode1(QPainter &painter);

private slots:
	void on_miw_Image_customContextMenuRequested(const QPoint &pos);

	void OnRedrawTimer();

	void OnRequestsTimer();

protected:
	virtual void closeEvent(QCloseEvent *event);

public:
	explicit OrionMap(QWidget *parent = 0);
	~OrionMap();

	void mapPaintEvent(QPainter &painter);
	void mapMousePressEvent(QMouseEvent *event);
	void mapMouseReleaseEvent(QMouseEvent *event);
	void mapMouseDoubleClickEvent(QMouseEvent *event);
	void mapMouseMoveEvent(QMouseEvent *event);
	void mapWheelEvent(QWheelEvent *event);

	void OnActionSelect(const uint &type, const uint &extra);
	void ChangeSettings(const CMapSettings &settings);
	void OnCloseSettings();

	void CheckForUpdate(const uint &serial);
};
//----------------------------------------------------------------------------------
extern OrionMap *g_OrionMap;
//----------------------------------------------------------------------------------
#endif // ORIONMAP_H
//----------------------------------------------------------------------------------
