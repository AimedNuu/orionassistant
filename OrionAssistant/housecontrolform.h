/***********************************************************************************
**
** HouseControlForm.h
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef HOUSECONTROLFORM_H
#define HOUSECONTROLFORM_H
//----------------------------------------------------------------------------------
#include <QDialog>
//----------------------------------------------------------------------------------
namespace Ui
{
	class HouseControlForm;
}
//----------------------------------------------------------------------------------
class HouseControlForm : public QDialog
{
	Q_OBJECT

private:
	Ui::HouseControlForm *ui;

private slots:
	void on_pb_LockDown_clicked();

	void on_pb_Secure_clicked();

	void on_pb_Release_clicked();

	void on_pb_Ban_clicked();

	void on_pb_Trash_clicked();

	void on_pb_Remove_clicked();

	void on_pb_Strongbox_clicked();

public:
	explicit HouseControlForm(QWidget *parent = 0);
	~HouseControlForm();
};
//----------------------------------------------------------------------------------
extern HouseControlForm *g_HouseControlForm;
//----------------------------------------------------------------------------------
#endif // HOUSECONTROLFORM_H
//----------------------------------------------------------------------------------
