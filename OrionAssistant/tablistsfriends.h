/***********************************************************************************
**
** TabListsFriends.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABLISTSFRIENDS_H
#define TABLISTSFRIENDS_H
//----------------------------------------------------------------------------------
#include <QWidget>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabListsFriends;
}
//----------------------------------------------------------------------------------
class TabListsFriends : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_Friends_clicked(const QModelIndex &index);

	void on_pb_FriendFromTarget_clicked();

	void on_pb_FriendCreate_clicked();

	void on_pb_FriendSave_clicked();

	void on_pb_FriendRemove_clicked();

private:
	Ui::TabListsFriends *ui;

public slots:
	void OnListItemMoved();

public:
	explicit TabListsFriends(QWidget *parent = 0);
	~TabListsFriends();

	void OnDeleteKeyClick(QWidget *widget);

	void LoadFriends(const QString &path);

	void SaveFriends(const QString &path);

	void SetFriendObjectFromTarget(const uint &serial);

	bool InFriendList(const uint &serial);

	QStringList GetFriendList();

	void AddFriend(QString name, const QString &object);

	void RemoveFriend(QString name);

	void ClearFriendList();
};
//----------------------------------------------------------------------------------
extern TabListsFriends *g_TabListsFriends;
//----------------------------------------------------------------------------------
#endif // TABLISTSFRIENDS_H
//----------------------------------------------------------------------------------
