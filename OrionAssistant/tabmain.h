/***********************************************************************************
**
** TabMain.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABMAIN_H
#define TABMAIN_H
//----------------------------------------------------------------------------------
#include <QWidget>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include "../CommonItems/binaryfile.h"
#include <QTimer>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabMain;
}
//----------------------------------------------------------------------------------
enum CHECKLISTBOX_IDENTIFICATOR
{
	CLBI_CORPSES_AUTOOPEN = 0,
	CLBI_STEALTH_STEP_COUNTER,
	CLBI_SOUND_FILTER,
	CLBI_FILTER_LIGHT,
	CLBI_FILTER_WEATHER,
	CLBI_FILTER_SEASON,
	CLBI_NO_DEATH_SCREEN,
	CLBI_OPEN_DOORS,
	CLBI_TRACKER,
	CLBI_STAY_ON_TOP,
	CLBI_FAST_ROTATION,
	CLBI_RECURSE_CONTAINERS_SEARCH,
	CLBI_AUTOSTART,
	CLBI_IGNORE_STAMINA_CHECK,
	CLBI_MINIMIZE_TO_TRAY,
	CLBI_SOUND_ECHO,
	CLBI_ANIMATION_ECHO,
	CLBI_EFFECT_ECHO,
	CLBI_FILTER_SPEECH,
	CLBI_TEXT_REPLACE,
	CLBI_OBJECT_INSPECTOR,
	CLBI_NO_GRAY_SCREEN,
	CLBI_SAVE_FAR_OBJECTS,
	CLBI_COUNT
};
//----------------------------------------------------------------------------------
class TabMain : public QWidget
{
	Q_OBJECT

private slots:
	void on_hs_WindowsOpacity_valueChanged(int value);

	void on_tb_SelectFontColor_clicked();

	void on_tb_SelectCharactersFontColor_clicked();

	void on_lw_Options_clicked(const QModelIndex &index);

	void on_ReconnectTimerTimer();

	void on_cb_ProfileList_currentIndexChanged(const QString &arg1);

	void on_pb_SaveProfile_clicked();

	void on_pb_OpenMap_clicked();

private:
	Ui::TabMain *ui;

	int m_ReconnectSeconds{ 0 };
	QTimer m_ReconnectTimer;

	void OptionChanged(const int &index, class QListWidgetItem *item);

public:
	explicit TabMain(QWidget *parent = 0);
	~TabMain();

	void UpdateFeatures();

	int MoveItemsDelay();

	int WaitTargetsDelay();

	int SearchItemsDistance();

	int UseItemsDistance();

	int UseItemsDelay();

	int OpenCorpsesDistance();

	int CorpseKeepDelay();

	int GetMessagesLevel();

	bool CorpsesAutoopen();

	bool StealthStepCounter();

	bool SoundFilter();

	bool LightFilter();

	bool WeatherFilter();

	bool SeasonFilter();

	bool NoDeathScreen();

	bool AutoOpenDoors();

	bool Tracker();

	bool FastRotation();

	bool RecurseContainersSearch();

	bool Autostart();

	bool IgnoreStaminaCheck();

	bool MinimizeToTray();

	bool StayOnTop();

	bool SoundEcho();

	bool AnimationEcho();

	bool EffectEcho();

	bool SpeechFilter();

	bool TextReplace();

	bool ObjectInspector();

	bool NoGrayScreen();

	bool SaveFarObjects();

	bool DoubleClickForOpenDoors();

	bool GetFontColor(ushort &value);

	void SetFontColor(const bool &state, ushort value);

	bool GetCharactersFontColor(ushort &value);

	void SetCharactersFontColor(const bool &state, ushort value);

	void LoadConfig(const QXmlStreamAttributes &attributes);

	void SaveConfig(QXmlStreamWriter &writter);

	void StartReconnect();

	void InitProfiles(const QStringList &list);

	QString ProfileName();

	void SetCurrentProfile(const QString &name);

	void OpenMap();

	void MoveMap(const int &x, const int &y);

	void CloseMap();
};
//----------------------------------------------------------------------------------
extern TabMain *g_TabMain;
//----------------------------------------------------------------------------------
#endif // TABMAIN_H
//----------------------------------------------------------------------------------
