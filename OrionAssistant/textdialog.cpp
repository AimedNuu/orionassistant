// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TextDialog.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "textdialog.h"
#include "ui_textdialog.h"
#include "../orionassistant_global.h"

TextDialog *g_TextDialog = nullptr;
//----------------------------------------------------------------------------------
TextDialog::TextDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::TextDialog)
{
	OAFUN_DEBUG("c32_f1");
	ui->setupUi(this);

	Qt::WindowFlags flags = windowFlags() | Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint;
	setWindowFlags(flags);
}
//----------------------------------------------------------------------------------
TextDialog::~TextDialog()
{
	OAFUN_DEBUG("");
	delete ui;
}
//----------------------------------------------------------------------------------
void TextDialog::ClearText()
{
	OAFUN_DEBUG("c32_f2");
	ui->pte_Text->clear();
}
//----------------------------------------------------------------------------------
void TextDialog::SetText(const QString &text)
{
	OAFUN_DEBUG("c32_f3");
	ui->pte_Text->setPlainText(text);
}
//----------------------------------------------------------------------------------
void TextDialog::AddText(const QString &text)
{
	OAFUN_DEBUG("c32_f4");
	ui->pte_Text->appendPlainText(text);
}
//----------------------------------------------------------------------------------
void TextDialog::SaveToFile(const QString &filePath)
{
	OAFUN_DEBUG("c32_f5");

	QFile file(filePath);

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream(&file) << ui->pte_Text->toPlainText();
		file.close();
	}
}
//----------------------------------------------------------------------------------
