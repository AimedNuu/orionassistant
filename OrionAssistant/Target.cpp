﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** Target.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "Target.h"
#include "Managers/PacketManager.h"
#include "../GameObjects/gameworld.h"
#include "../GameObjects/gameplayer.h"
#include "orionassistant.h"
#include "Managers/FileManager.h"
#include "OrionAssistant/Packets.h"
#include "tabmacros.h"
//----------------------------------------------------------------------------------
CTarget g_Target;
//----------------------------------------------------------------------------------
CTargetHook::CTargetHook(const uint &serial)
{
	OAFUN_DEBUG("");
	TargetOnObject = true;
	Serial = serial;
}
//----------------------------------------------------------------------------------
CTargetHook::CTargetHook(const QString &tile, const int &x, const int &y, const int &z, const bool &relative)
: Tile(tile)
{
	OAFUN_DEBUG("");
	TargetOnObject = false;
	X = x;
	Y = y;
	Z = z;
	Relative = relative;
}
//----------------------------------------------------------------------------------
CTarget::CTarget()
{
	OAFUN_DEBUG("c31_f1");
}
//----------------------------------------------------------------------------------
void CTarget::SetObjectsHook(const QStringList &objects)
{
	OAFUN_DEBUG("c31_f2");
	m_WaitTargets.clear();

	if (!objects.empty())
	{
		for (const QString &i : objects)
			m_WaitTargets.push_back(CTargetHook(COrionAssistant::TextToSerial(i)));
	}
}
//----------------------------------------------------------------------------------
void CTarget::SetTileHook(const QString &tile, const int &x, const int &y, const int &z, const bool &relative)
{
	OAFUN_DEBUG("c31_f3");
	m_WaitTargets.clear();

	m_WaitTargets.push_back(CTargetHook(tile, x, y, z, relative));
}
//----------------------------------------------------------------------------------
void CTarget::ClearHooks()
{
	OAFUN_DEBUG("c31_f4");
	m_WaitTargets.clear();
}
//----------------------------------------------------------------------------------
bool CTarget::CheckHook()
{
	OAFUN_DEBUG("c31_f5");
	if (m_WaitTargets.size())
	{
		const CTargetHook &hook = m_WaitTargets.first();

		if (hook.TargetOnObject)
			SendTargetObject(hook.Serial);
		else
		{
			QString tileName = hook.Tile.toLower();

			if (tileName == "lasttile")
				SendTargetLastTile();
			else
			{
				int x = hook.X;
				int y = hook.Y;
				int z = hook.Z;

				if (hook.Relative)
				{
					x += g_Player->GetX();
					y += g_Player->GetY();
					z += g_Player->GetZ();
				}
				else if (!x && !y)
				{
					x = g_Player->GetX();
					y = g_Player->GetY();
					z = g_Player->GetZ();
				}

				QList<CTileInfo> tileList = g_FileManager.GetTile(x, y, z, hook.Tile);
				ushort tileGraphic = 0;

				if (tileList.size())
				{
					CTileInfo &tileInfo = tileList[0];

					tileGraphic = tileInfo.GetGraphic();
					z = tileInfo.GetZ();

					if (!tileInfo.GetLand())
					{
						if (g_PacketManager.GetClientVersion() >= CV_7090 && tileGraphic < g_FileManager.m_StaticDataCount)
						{
							STATIC_TILES &st = g_FileManager.m_StaticTiledata[tileGraphic / 32].Tiles[tileGraphic % 32];

							if (IsSurface(st.Flags))
								z += st.Height;
						}
					}
				}
				else
					tileGraphic = COrionAssistant::TextToGraphic(hook.Tile);

				SendTargetTile(tileGraphic, x, y, z);
			}
		}

		m_WaitTargets.pop_front();

		return false;
	}

	return true;
}
//----------------------------------------------------------------------------------
void CTarget::Reset()
{
	OAFUN_DEBUG("c31_f6");
	//Чистимся
	m_Type = 0;
	m_CursorType = 0;
	m_CursorID = 0;
	m_Targeting = false;
	m_MultiGraphic = 0;
}
//----------------------------------------------------------------------------------
void CTarget::RestoreTarget()
{
	OAFUN_DEBUG("c31_f6.1");
	if (m_FromOA)
	{
		m_FromOA = false;

		if (m_ClientWantTarget)
		{
			CDataReader reader(m_CurrentClientTarget.data(), m_CurrentClientTarget.size());

			uchar id = reader.ReadUInt8();

			if (id == 0x6C)
				SetRecvData(reader);
			else
				SetMultiData(reader);

			emit g_OrionAssistant.signal_SendClient(m_CurrentClientTarget);
		}
	}
}
//----------------------------------------------------------------------------------
bool CTarget::SetSendData(CDataReader &reader)
{
	OAFUN_DEBUG("c31_f7");
	m_Targeting = false;
	m_ClientWantTarget = false;

	if (reader.ReadUInt8() == 1) //tile
	{
		reader.Move(9);
		m_LastTileX = reader.ReadUInt16BE();
		m_LastTileY = reader.ReadUInt16BE();
		m_LastTileZ = reader.ReadUInt16BE();
		m_LastTileGraphic = reader.ReadUInt16BE();

		if (g_World != nullptr && g_TabMacros->GetRecording())
		{
			g_TabMacros->AddAction(new CMacroWithTargetTile(MT_WAIT_FOR_TARGET_TILE, m_LastTileGraphic, m_LastTileX, m_LastTileY, m_LastTileZ, m_LastTileX - g_Player->GetX(), m_LastTileY - g_Player->GetY(), m_LastTileZ - g_Player->GetZ()), false);
		}
	}
	else
	{
		reader.Move(5);
		uint serial = reader.ReadUInt32BE();

		if (g_World != nullptr && g_TabMacros->GetRecording())
		{
			CGameObject *obj = g_World->FindWorldObject(serial);

			CMacro *macro = NULL;

			if (obj != nullptr)
				macro = new CMacroWithObject(MT_WAIT_FOR_TARGET_OBJECT, serial, obj->GetGraphic(), obj->GetColor());
			else
				macro = new CMacroWithObject(MT_WAIT_FOR_TARGET_OBJECT, serial, 0, 0);

			if (serial == g_PlayerSerial)
				macro->SetConvertType(MCT_SELF);
			else if (serial == g_LastAttackObject)
				macro->SetConvertType(MCT_LASTATTACK);
			else if (serial == g_LastTargetObject)
				macro->SetConvertType(MCT_LASTTARGET);
			else if (serial == COrionAssistant::TextToSerial("enemy"))
				macro->SetConvertType(MCT_ENEMY);
			else if (serial == COrionAssistant::TextToSerial("friend"))
				macro->SetConvertType(MCT_ENEMY);

			g_TabMacros->AddAction(macro, false);
		}

		if (serial != g_PlayerSerial)
			g_LastTargetObject = serial;
	}

	return true;
}
//----------------------------------------------------------------------------------
void CTarget::SetRecvOAData(CDataReader &reader)
{
	OAFUN_DEBUG("c31_f8.1");
	//И устанавливаем соответствующие значения
	m_Type = reader.ReadUInt8();
	m_CursorID = reader.ReadUInt32BE();
	m_CursorType = reader.ReadUInt8();
	m_Targeting = (m_CursorType < 3);
	m_MultiGraphic = 0;
}
//----------------------------------------------------------------------------------
bool CTarget::SetRecvData(CDataReader &reader)
{
	OAFUN_DEBUG("c31_f8");
	//Копируем буфер
	m_CurrentClientTarget.resize(reader.GetSize());
	memcpy(&m_CurrentClientTarget[0], reader.GetStart(), reader.GetSize());

	//И устанавливаем соответствующие значения
	m_Type = reader.ReadUInt8();
	m_CursorID = reader.ReadUInt32BE();
	m_CursorType = reader.ReadUInt8();
	m_Targeting = (m_CursorType < 3);
	m_MultiGraphic = 0;
	m_ClientWantTarget = true;

	bool result = (CheckHook() && !m_FromOA);

	if (result && g_TabMacros->GetPlaying() && g_TabMacros->PlayWaiting(QList<MACRO_TYPE>() << MT_WAIT_FOR_TARGET_OBJECT << MT_WAIT_FOR_TARGET_TILE))
		return false;

	return result;
}
//----------------------------------------------------------------------------------
bool CTarget::SetMultiData(CDataReader &reader)
{
	OAFUN_DEBUG("c31_f9");
	m_CurrentClientTarget.resize(reader.GetSize());
	memcpy(&m_CurrentClientTarget[0], reader.GetStart(), reader.GetSize());

	//Устанавливаем соответствующие значения
	m_Type = reader.ReadUInt8();
	m_CursorID = reader.ReadUInt32BE();
	m_CursorType = reader.ReadUInt8();
	m_Targeting = (m_CursorType < 3);
	m_ClientWantTarget = true;

	reader.Move(10);
	m_MultiGraphic = reader.ReadUInt16BE() + 1;

	bool result = (CheckHook() && !m_FromOA);

	if (result && g_TabMacros->GetPlaying() && g_TabMacros->PlayWaiting(QList<MACRO_TYPE>() << MT_WAIT_FOR_TARGET_OBJECT << MT_WAIT_FOR_TARGET_TILE))
		return false;

	return result;
}
//----------------------------------------------------------------------------------
void CTarget::DumpLastTileInfo()
{
	OAFUN_DEBUG("c31_f10");
	g_OrionAssistant.DumpTileInfo(m_LastTileGraphic, m_LastTileX, m_LastTileY, m_LastTileZ);
}
//----------------------------------------------------------------------------------
void CTarget::SendTargetObject(const uint &serial)
{
	OAFUN_DEBUG("c31_f11");
	if (!m_Targeting)
		return; //Если в клиенте нет таргета - выход

	CPacketTargetObject(0, m_CursorID, m_CursorType, serial).SendServer();

	m_Targeting = false;
	m_MultiGraphic = 0;
	m_ClientWantTarget = false;
}
//----------------------------------------------------------------------------------
void CTarget::SendTargetTile(const ushort &tileID, const short &x, const short &y, short z)
{
	OAFUN_DEBUG("c31_f12");
	if (!m_Targeting)
		return; //Если в клиенте нет таргета - выход

	CPacketTargetTile(1, m_CursorID, m_CursorType, tileID, x, y, z).SendServer();

	m_Targeting = false;
	m_MultiGraphic = 0;
	m_ClientWantTarget = false;
}
//----------------------------------------------------------------------------------
void CTarget::SendTargetLastTile()
{
	OAFUN_DEBUG("c31_f13");
	SendTargetTile(m_LastTileGraphic, m_LastTileX, m_LastTileY, m_LastTileZ);
}
//----------------------------------------------------------------------------------
void CTarget::SendCancelTarget()
{
	OAFUN_DEBUG("c31_f14");
	if (!m_Targeting)
		return; //Если в клиенте нет таргета - выход

	CPacketTargetCancel(m_Type, m_CursorID, m_CursorType).SendServer();

	m_Targeting = false;
	m_MultiGraphic = 0;
	m_ClientWantTarget = false;
}
//----------------------------------------------------------------------------------
void CTarget::SendTargetLastTarget()
{
	OAFUN_DEBUG("c31_f15");
	SendTargetObject(g_LastTargetObject);
}
//----------------------------------------------------------------------------------
