// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ShopListEditorForm.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "shoplisteditorform.h"
#include "ui_shoplisteditorform.h"
#include "../GUI/shopitemlistitem.h"
#include "../GUI/shoplistitem.h"
#include "orionassistant.h"
//----------------------------------------------------------------------------------
ShopListEditorForm *g_ShopListEditorForm = nullptr;
//----------------------------------------------------------------------------------
ShopListEditorForm::ShopListEditorForm(QWidget *parent)
: QMainWindow(parent), ui(new Ui::ShopListEditorForm)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);

	connect(ui->le_Graphic, SIGNAL(textChanged(QString)), this, SLOT(slot_ShopItemSave()));
	connect(ui->le_Color, SIGNAL(textChanged(QString)), this, SLOT(slot_ShopItemSave()));
	connect(ui->le_Name, SIGNAL(textChanged(QString)), this, SLOT(slot_ShopItemSave()));
	connect(ui->sb_Count, SIGNAL(valueChanged(int)), this, SLOT(slot_ShopItemSave()));
	connect(ui->sb_Price, SIGNAL(valueChanged(int)), this, SLOT(slot_ShopItemSave()));
	connect(ui->le_Comment, SIGNAL(textChanged(QString)), this, SLOT(slot_ShopItemSave()));
}
//----------------------------------------------------------------------------------
ShopListEditorForm::~ShopListEditorForm()
{
	OAFUN_DEBUG("");
	delete ui;
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::closeEvent(QCloseEvent *event)
{
	OAFUN_DEBUG("");
	on_pb_Exit_clicked();
	event->ignore();
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::keyPressEvent(QKeyEvent *event)
{
	OAFUN_DEBUG("");
	if (event->isAutoRepeat())
		return;

	if (event->key() == Qt::Key_Delete)
	{
		if (QApplication::focusWidget() == ui->lw_ShopList)
			on_pb_ShopItemRemove_clicked();
	}

	event->accept();
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::on_lw_ShopList_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("");
	CShopItemListItem *item = (CShopItemListItem*)ui->lw_ShopList->item(index.row());

	if (item != nullptr)
	{
		m_NoProcessSave = true;
		const CShopItem &shopItem = item->m_ShopItem;

		ui->le_Graphic->setText(shopItem.GetGraphic());
		ui->le_Color->setText(shopItem.GetColor());
		ui->le_Name->setText(shopItem.GetName());
		ui->sb_Count->setValue(shopItem.GetCount());
		ui->sb_Price->setValue(shopItem.GetPrice());
		ui->le_Comment->setText(shopItem.GetComment());
		m_NoProcessSave = false;
	}
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::on_lw_VendorList_doubleClicked(const QModelIndex &index)
{
	OAFUN_DEBUG("");
	Q_UNUSED(index);
	on_pb_CopyShopItem_clicked();
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::on_pb_CopyShopItem_clicked()
{
	OAFUN_DEBUG("");
	CShopItemListItem *item = (CShopItemListItem*)ui->lw_VendorList->currentItem();

	if (item != nullptr)
	{
		CShopItem shopItem = item->m_ShopItem;
		shopItem.SetCount(0);
		shopItem.SetPrice(0);
		AddNewItem(shopItem);
	}
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::on_pb_ShopItemAdd_clicked()
{
	AddNewItem(CShopItem(ui->le_Graphic->text(), ui->le_Color->text(), ui->le_Name->text(), ui->sb_Count->value(), ui->sb_Price->value(), ui->le_Comment->text()));
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::AddNewItem(const CShopItem &shopItem)
{
	OAFUN_DEBUG("");
	if (!shopItem.GetGraphic().length())
	{
		QMessageBox::critical(this, "Text is empty", "Enter the shop item graphic!");
		return;
	}
	else if (!shopItem.GetColor().length())
	{
		QMessageBox::critical(this, "Text is empty", "Enter the shop item color!");
		return;
	}

	IFOR(i, 0, ui->lw_ShopList->count())
	{
		CShopItemListItem *item = (CShopItemListItem*)ui->lw_ShopList->item(i);

		if (item != nullptr &&
				item->m_ShopItem.GetGraphic().toLower() == shopItem.GetGraphic().toLower() &&
				item->m_ShopItem.GetColor().toLower() == shopItem.GetColor().toLower() &&
				(shopItem.GetName().length() && item->m_ShopItem.GetName().toLower() == shopItem.GetName().toLower()))
		{
			QMessageBox::critical(this, "Item is already exists", "Shop item is already exists!");
			return;
		}
	}

	new CShopItemListItem(shopItem, ui->lw_ShopList);
	m_Changed = true;

	ui->lw_ShopList->setCurrentRow(ui->lw_ShopList->count() - 1);
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::slot_ShopItemSave()
{
	OAFUN_DEBUG("");
	if (m_NoProcessSave)
		return;

	CShopItemListItem *selected = (CShopItemListItem*)ui->lw_ShopList->currentItem();

	if (selected != nullptr)
	{
		selected->m_ShopItem.SetGraphic(ui->le_Graphic->text());
		selected->m_ShopItem.SetColor(ui->le_Color->text());
		selected->m_ShopItem.SetName(ui->le_Name->text());
		selected->m_ShopItem.SetCount(ui->sb_Count->value());
		selected->m_ShopItem.SetPrice(ui->sb_Price->value());
		selected->m_ShopItem.SetComment(ui->le_Comment->text());
		selected->UpdateText();
		m_Changed = true;
	}
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::on_pb_ShopItemRemove_clicked()
{
	OAFUN_DEBUG("");
	QListWidgetItem *item = ui->lw_ShopList->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_ShopList->takeItem(ui->lw_ShopList->row(item));

		if (item != nullptr)
		{
			delete item;
			m_Changed = true;
		}
	}
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::on_pb_SaveChanges_clicked()
{
	OAFUN_DEBUG("");
	if (!m_Changed || m_UsedList == nullptr)
		return;

	QVector<CShopItem> &list = m_UsedList->m_Items;
	list.clear();

	IFOR(i, 0, ui->lw_ShopList->count())
	{
		CShopItemListItem *item = (CShopItemListItem*)ui->lw_ShopList->item(i);

		if (item != nullptr)
			list.push_back(item->m_ShopItem);
	}

	m_Changed = false;

	g_OrionAssistant.ClientPrint("Shop list saved!");
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::on_pb_Exit_clicked()
{
	OAFUN_DEBUG("");
	InitList(nullptr);
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::InitList(CShopListItem *list)
{
	OAFUN_DEBUG("");
	if (m_Changed && m_UsedList != nullptr)
	{
		if (QMessageBox::question(this, "Save changes?", "You have unsaved changes in this list.\nSave changes?") == QMessageBox::Yes)
			on_pb_SaveChanges_clicked();
	}

	m_UsedList = list;
	ui->lw_ShopList->clear();
	m_Changed = false;

	if (m_UsedList != nullptr)
	{
		ui->l_MyListText->setText("List '" + m_UsedList->text() + "':");

		if (!m_UsedList->m_Items.empty())
		{
			for (const CShopItem &shopItem : m_UsedList->m_Items)
				new CShopItemListItem(shopItem, ui->lw_ShopList);
		}

		m_NoProcessSave = true;
		ui->le_Graphic->setText("");
		ui->le_Color->setText("");
		ui->le_Name->setText("");
		ui->sb_Count->setValue(0);
		ui->sb_Price->setValue(0);
		ui->le_Comment->setText("");
		m_NoProcessSave = false;

		if (isVisible())
			activateWindow();
		else
			show();
	}
	else
		hide();
}
//----------------------------------------------------------------------------------
void ShopListEditorForm::UpdateVendorList()
{
	OAFUN_DEBUG("");
	ui->lw_VendorList->clear();

	if (!isVisible() || m_VendorItems.empty())
		return;

	for (const CVendorShopItem &item : m_VendorItems)
	{
		new CShopItemListItem
		(
			CShopItem(COrionAssistant::GraphicToText(item.GetGraphic()), COrionAssistant::GraphicToText(item.GetColor()), item.GetName(), item.GetCount(), item.GetPrice(), ""),
			ui->lw_VendorList
		);
	}

	g_OrionAssistant.ClientPrintDebugLevel1("Shop list loaded.");
}
//----------------------------------------------------------------------------------
