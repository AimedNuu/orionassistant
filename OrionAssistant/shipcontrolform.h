/***********************************************************************************
**
** ShipControlForm.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SHIPCONTROLFORM_H
#define SHIPCONTROLFORM_H
//----------------------------------------------------------------------------------
#include <QDialog>
//----------------------------------------------------------------------------------
namespace Ui
{
	class ShipControlForm;
}
//----------------------------------------------------------------------------------
class ShipControlForm : public QDialog
{
	Q_OBJECT

private:
	Ui::ShipControlForm *ui;

	void Send(const QString &text);

private slots:
	void on_pb_ForwardLeft_clicked();

	void on_pb_Forward_clicked();

	void on_pb_ForwardRight_clicked();

	void on_pb_Right_clicked();

	void on_pb_BackwardRight_clicked();

	void on_pb_Backward_clicked();

	void on_pb_BackwardLeft_clicked();

	void on_pb_Left_clicked();

	void on_pb_Stop_clicked();

	void on_pb_TurnLeft_clicked();

	void on_pb_TurnAround_clicked();

	void on_pb_TurnRight_clicked();

	void on_pb_RaiseAnchor_clicked();

	void on_pb_DropAnchor_clicked();

public:
	explicit ShipControlForm(QWidget *parent = 0);
	~ShipControlForm();
};
//----------------------------------------------------------------------------------
extern ShipControlForm *g_ShipControlForm;
//----------------------------------------------------------------------------------
#endif // SHIPCONTROLFORM_H
//----------------------------------------------------------------------------------
