QT       += core
CONFIG   += c++11

SOURCES += \
    $$PWD/findtextdialog.cpp \
    $$PWD/gotolinedialog.cpp \
    $$PWD/replacetextdialog.cpp


HEADERS  += \
    $$PWD/findtextdialog.h \
    $$PWD/gotolinedialog.h \
    $$PWD/replacetextdialog.h

FORMS += \
    $$PWD/findtextdialog.ui \
    $$PWD/gotolinedialog.ui \
    $$PWD/replacetextdialog.ui
