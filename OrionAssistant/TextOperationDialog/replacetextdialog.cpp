// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
//----------------------------------------------------------------------------------
#include "replacetextdialog.h"
#include "ui_replacetextdialog.h"
//----------------------------------------------------------------------------------
CReplaceTextDialog::CReplaceTextDialog(QWidget *parent)
: QDialog(parent), ui(new Ui::CReplaceTextDialog)
{
	ui->setupUi(this);
	setFixedSize(size());
	hide();
}
//----------------------------------------------------------------------------------
CReplaceTextDialog::~CReplaceTextDialog()
{
	delete ui;
}
//----------------------------------------------------------------------------------
void CReplaceTextDialog::on_pb_SearchNext_clicked()
{
	if (ui->le_Text->text().length())
		emit signal_SearchText(ui->le_Text->text(), ui->cb_CaseSensitive->isChecked(), ui->rb_DirectionDown->isChecked());
}
//----------------------------------------------------------------------------------
void CReplaceTextDialog::on_pb_Replace_clicked()
{
	if (ui->le_Text->text().length())
		emit signal_ReplaceText(ui->le_Text->text(), ui->le_Replace->text(), ui->cb_CaseSensitive->isChecked(), ui->rb_DirectionDown->isChecked(), false);
}
//----------------------------------------------------------------------------------
void CReplaceTextDialog::on_pb_ReplaceAll_clicked()
{
	if (ui->le_Text->text().length())
		emit signal_ReplaceText(ui->le_Text->text(), ui->le_Replace->text(), ui->cb_CaseSensitive->isChecked(), ui->rb_DirectionDown->isChecked(), true);
}
//----------------------------------------------------------------------------------
void CReplaceTextDialog::on_pb_Cancel_clicked()
{
	hide();
}
//----------------------------------------------------------------------------------
