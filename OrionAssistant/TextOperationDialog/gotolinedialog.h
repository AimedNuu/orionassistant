//----------------------------------------------------------------------------------
#ifndef GOTOLINEDIALOG_H
#define GOTOLINEDIALOG_H
//----------------------------------------------------------------------------------
#include <QDialog>
//----------------------------------------------------------------------------------
namespace Ui
{
	class CGoToLineDialog;
}
//----------------------------------------------------------------------------------
class CGoToLineDialog : public QDialog
{
	Q_OBJECT

signals:
	void signal_GoToLine(int lineIndex);

public:
	explicit CGoToLineDialog(QWidget *parent = 0);
	~CGoToLineDialog();

	void SetLine(const int &lineIndex);

private slots:
	void on_pb_GoToLine_clicked();

	void on_pb_Cancel_clicked();

private:
	Ui::CGoToLineDialog *ui;
};
//----------------------------------------------------------------------------------
#endif // GOTOLINEDIALOG_H
//----------------------------------------------------------------------------------
