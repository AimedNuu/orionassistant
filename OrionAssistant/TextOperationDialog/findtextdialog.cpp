// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
//----------------------------------------------------------------------------------
#include "findtextdialog.h"
#include "ui_findtextdialog.h"
//----------------------------------------------------------------------------------
CFindTextDialog::CFindTextDialog(QWidget *parent)
: QDialog(parent), ui(new Ui::CFindTextDialog)
{
	ui->setupUi(this);
	setFixedSize(size());
	hide();
}
//----------------------------------------------------------------------------------
CFindTextDialog::~CFindTextDialog()
{
	delete ui;
}
//----------------------------------------------------------------------------------
void CFindTextDialog::on_pb_SearchNext_clicked()
{
	if (ui->le_Text->text().length())
		emit signal_SearchText(ui->le_Text->text(), ui->cb_CaseSensitive->isChecked(), ui->rb_DirectionDown->isChecked());
}
//----------------------------------------------------------------------------------
void CFindTextDialog::on_pb_Cancel_clicked()
{
	hide();
}
//----------------------------------------------------------------------------------
