//----------------------------------------------------------------------------------
#ifndef REPLACETEXTDIALOG_H
#define REPLACETEXTDIALOG_H
//----------------------------------------------------------------------------------
#include <QDialog>
//----------------------------------------------------------------------------------
namespace Ui
{
	class CReplaceTextDialog;
}
//----------------------------------------------------------------------------------
class CReplaceTextDialog : public QDialog
{
	Q_OBJECT

signals:
	void signal_SearchText(QString text, bool caseSensitive, bool searchDown);
	void signal_ReplaceText(QString text, QString replace, bool caseSensitive, bool searchDown, bool all);

public:
	explicit CReplaceTextDialog(QWidget *parent = 0);
	~CReplaceTextDialog();

private slots:
	void on_pb_SearchNext_clicked();

	void on_pb_Replace_clicked();

	void on_pb_ReplaceAll_clicked();

	void on_pb_Cancel_clicked();

private:
	Ui::CReplaceTextDialog *ui;
};
//----------------------------------------------------------------------------------
#endif // REPLACETEXTDIALOG_H
//----------------------------------------------------------------------------------
