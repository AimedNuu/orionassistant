// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
//----------------------------------------------------------------------------------
#include "gotolinedialog.h"
#include "ui_gotolinedialog.h"
//----------------------------------------------------------------------------------
CGoToLineDialog::CGoToLineDialog(QWidget *parent)
: QDialog(parent), ui(new Ui::CGoToLineDialog)
{
	ui->setupUi(this);
	ui->le_LineIndex->setValidator(new QRegExpValidator(QRegExp("^\\d{1,100}"), this));
	setFixedSize(size());
	hide();
}
//----------------------------------------------------------------------------------
CGoToLineDialog::~CGoToLineDialog()
{
	delete ui;
}
//----------------------------------------------------------------------------------
void CGoToLineDialog::SetLine(const int &lineIndex)
{
	ui->le_LineIndex->setText(QString::number(lineIndex));
}
//----------------------------------------------------------------------------------
void CGoToLineDialog::on_pb_GoToLine_clicked()
{
	if (ui->le_LineIndex->text().trimmed().length())
		emit signal_GoToLine(ui->le_LineIndex->text().trimmed().toInt());
}
//----------------------------------------------------------------------------------
void CGoToLineDialog::on_pb_Cancel_clicked()
{
	hide();
}
//----------------------------------------------------------------------------------
