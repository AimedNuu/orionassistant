//----------------------------------------------------------------------------------
#ifndef CFINDTEXTDIALOG_H
#define CFINDTEXTDIALOG_H
//----------------------------------------------------------------------------------
#include <QDialog>
//----------------------------------------------------------------------------------
namespace Ui
{
	class CFindTextDialog;
}
//----------------------------------------------------------------------------------
class CFindTextDialog : public QDialog
{
	Q_OBJECT

signals:
	void signal_SearchText(QString text, bool caseSensitive, bool searchDown);

public:
	explicit CFindTextDialog(QWidget *parent = 0);
	~CFindTextDialog();

private slots:
	void on_pb_SearchNext_clicked();

	void on_pb_Cancel_clicked();

private:
	Ui::CFindTextDialog *ui{ nullptr };

};
//----------------------------------------------------------------------------------
#endif // CFINDTEXTDIALOG_H
//----------------------------------------------------------------------------------
