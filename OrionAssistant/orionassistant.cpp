// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OrionAssistant.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "orionassistant.h"
#include "orionassistantform.h"
#include "Packets.h"
#include "../GameObjects/gameworld.h"
#include "textdialog.h"
#include "../Managers/PacketManager.h"
#include "../GameObjects/GamePlayer.h"
#include "Managers/TextParser.h"
#include "CommonItems/finditem.h"
#include "CommonItems/ignoreitem.h"
#include "Managers/UOMapManager.h"
#include "Target.h"
#include "tabmain.h"
#include "tabdisplay.h"
#include "tabliststypes.h"
#include "tablistsobjects.h"
#include "tablistsfind.h"
#include "tablistsignore.h"
#include "tablistsfriends.h"
#include "tablistsenemies.h"
#include "tabagentsdress.h"
#include "tabagentsparty.h"
#include "objectinspectorform.h"
#include "animalandvendorcontrolform.h"

COrionAssistant g_OrionAssistant;
TARGET_HANDLER g_TargetHandler = nullptr;
MOVITEM_DATA g_MoveItemData;
//----------------------------------------------------------------------------------
COrionAssistant::COrionAssistant(QObject *parent)
: QObject(parent)
{
	OAFUN_DEBUG("");
	qRegisterMetaType<UCHAR_LIST>("UCHAR_LIST");

	connect(this, SIGNAL(signal_ScriptError(const QString&, const QString&)), this, SLOT(slot_ScriptError(const QString&, const QString&)));

	connect(this, SIGNAL(signal_SendClient(UCHAR_LIST)), this, SLOT(SendClient(UCHAR_LIST)));
	connect(this, SIGNAL(signal_SendServer(UCHAR_LIST)), this, SLOT(SendServer(UCHAR_LIST)));
}
//----------------------------------------------------------------------------------
COrionAssistant::~COrionAssistant()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
void COrionAssistant::slot_ScriptError(const QString &title, const QString &text)
{
	OAFUN_DEBUG("c27_f1");
	//QMessageBox::critical(g_OrionAssistantForm, title, text);
	ClientPrint(title, 0x0021);
	ClientPrint(text, 0x0021);
}
//----------------------------------------------------------------------------------
void COrionAssistant::SendClient(const uchar *buf, int size)
{
	OAFUN_DEBUG("c27_f2");
	g_SendClient((puchar)buf, size);
}
//----------------------------------------------------------------------------------
void COrionAssistant::SendServer(const uchar *buf, int size)
{
	OAFUN_DEBUG("c27_f3");
	g_SendServer((puchar)buf, size);
}
//----------------------------------------------------------------------------------
uint COrionAssistant::RawStringToUInt(const QString &value)
{
	OAFUN_DEBUG("c27_f4");
	uint result = 0;

	if (value.length() > 1)
	{
		if (value.at(0) == '#')
		{
			QString vv = value;
			vv.remove(0, 1);
			result = ("0x" + vv).toUInt(nullptr, 16);
		}
		else
		{
			if (value == "red")
				result = 0xFF0000FF;
			else if (value == "cyan")
				result = 0x00FFFFFF;
			else if (value == "blue")
				result = 0x0000FFFF;
			else if (value == "darkblue")
				result = 0x0000A0FF;
			else if (value == "lightblue")
				result = 0xADD8E6FF;
			else if (value == "purple")
				result = 0x800080FF;
			else if (value == "yellow")
				result = 0x00FFFFFF;
			else if (value == "lime")
				result = 0x00FF00FF;
			else if (value == "magenta")
				result = 0xFF00FFFF;
			else if (value == "white")
				result = 0xFFFEFEFF;
			else if (value == "silver")
				result = 0xC0C0C0FF;
			else if (value == "gray" || value == "grey")
				result = 0x808080FF;
			else if (value == "black")
				result = 0x010101FF;
			else if (value == "orange")
				result = 0xFFA500FF;
			else if (value == "brown")
				result = 0xA52A2AFF;
			else if (value == "maroon")
				result = 0x800000FF;
			else if (value == "green")
				result = 0x008000FF;
			else if (value == "olive")
				result = 0x808000FF;
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
bool COrionAssistant::RawStringToBool(QString value)
{
	OAFUN_DEBUG("c27_f5");
	value = value.toLower();
	bool result = false;

	if (value == "true" || value == "on")
		result = true;
	else
		result = (value.toInt() != 0);

	return result;
}
//----------------------------------------------------------------------------------
uint COrionAssistant::TextToUInt(const QString &text)
{
	OAFUN_DEBUG("c27_f6");
	bool testConvert = false;

	uint result = (uint)text.toInt(&testConvert);

	if (!testConvert)
		result = text.toUInt(nullptr, 16);

	return result;
}
//----------------------------------------------------------------------------------
uint COrionAssistant::TextToSerial(QString text)
{
	OAFUN_DEBUG("c27_f6");
	text = text.toLower();

	uint result = 0;

	if (text == "self")
		result = g_PlayerSerial;
	else if (text == "lasttarget")
		result = g_LastTargetObject;
	else if (text == "lastattack")
		result = g_LastAttackObject;
	else if (text == "lastcorpse")
		result = g_LastCorpseObject;
	else if (text == "lastcontainer")
		result = g_LastContainerObject;
	else if (text == "laststatus")
		result = g_LastStatusObject;
	else if (text == "lastobject")
		result = g_LastUseObject;
	else if (text == "backpack")
		result = g_Backpack;
	else if (text == "ground")
		result = 0xFFFFFFFF;
	else
		result = g_TabListsObjects->SeekObject(text);

	if (!result)
		result = TextToUInt(text);

	return result;
}
//----------------------------------------------------------------------------------
ushort COrionAssistant::TextToGraphic(QString text)
{
	OAFUN_DEBUG("c27_f8");
	text = text.toLower();

	ushort result = g_TabListsTypes->SeekType(text);

	if (!result)
		result = (ushort)TextToUInt(text);

	return result;
}
//----------------------------------------------------------------------------------
int COrionAssistant::TextToLayer(QString text)
{
	OAFUN_DEBUG("c27_f9");
	text = text.toLower();

	IFOR(i, 0, 30)
	{
		if (text == g_LayerName[i].toLower())
			return i;
	}

	return text.toInt();
}
//----------------------------------------------------------------------------------
QString COrionAssistant::ColorToText(const uint &value)
{
	OAFUN_DEBUG("c27_f10");
	QString result = "";

	result.sprintf("#%08X", value);

	return result;
}
//----------------------------------------------------------------------------------
QString COrionAssistant::SerialToText(const uint &value)
{
	OAFUN_DEBUG("c27_f11");
	QString result = "";

	result.sprintf("0x%08X", value);

	return result;
}
//----------------------------------------------------------------------------------
QString COrionAssistant::GraphicToText(const ushort &value)
{
	OAFUN_DEBUG("c27_f12");
	QString result = "";

	result.sprintf("0x%04X", value);

	return result;
}
//----------------------------------------------------------------------------------
QString COrionAssistant::BoolToText(const bool &value)
{
	OAFUN_DEBUG("c27_f13");
	if (value)
		return "true";

	return "false";
}
//----------------------------------------------------------------------------------
void COrionAssistant::ClientPrint(const QString &text, const ushort &color)
{
	OAFUN_DEBUG("c27_f14");
	//if (g_InjectEcho) Journal.Add(text, 1, 0);

	CPacketToClientSystemPrint(text.toStdWString(), color).SendClient();
}
//----------------------------------------------------------------------------------
void COrionAssistant::ClientCharPrint(const uint &serial, const QString &text, const ushort &color)
{
	OAFUN_DEBUG("c27_f15");
	//if (g_InjectEcho) Journal.Add(text, 1, 0);

	CPacketToClientCharacterPrint(serial, text.toStdWString(), color).SendClient();
}
//----------------------------------------------------------------------------------
void COrionAssistant::ClientPrintDebugLevel1(const QString &text, const ushort &color)
{
	OAFUN_DEBUG("c27_f16");
	if (g_TabMain->GetMessagesLevel() >= 1)
		ClientPrint(text, color);
}
//----------------------------------------------------------------------------------
void COrionAssistant::ClientPrintDebugLevel2(const QString &text, const ushort &color)
{
	OAFUN_DEBUG("c27_f17");
	if (g_TabMain->GetMessagesLevel() >= 2)
		ClientPrint(text, color);
}
//----------------------------------------------------------------------------------
void COrionAssistant::RequestTarget(const QString &text)
{
	OAFUN_DEBUG("c27_f18");
	if (text.length())
		ClientPrintDebugLevel2(text);

	g_Target.SetFromOA(true);
	CPacketToClientTargetRequest().SendClient();
}
//----------------------------------------------------------------------------------
void COrionAssistant::DumpObjectInfo(CGameObject *obj)
{
	OAFUN_DEBUG("c27_f19");
	if (obj != nullptr)
	{
		if (g_TabMain->ObjectInspector())
			g_ObjectInspectorForm->Update(obj);
		else
		{
			QString text = "";
			int layer = 0;

			if (!obj->GetNPC())
				layer = ((CGameItem*)obj)->GetLayer();

			text.sprintf("Serial=0x%08X Graphic=0x%04X Name=%s\nCount: %i  Color: 0x%04X  Layer: %i\nX=%i Y=%i Z=%i C=0x%08X F=0x%02X\n",
				obj->GetSerial(), obj->GetGraphic(), obj->GetName().toStdString().c_str(), obj->GetCount(),
				obj->GetColor(), layer, obj->GetX(), obj->GetY(), obj->GetZ(), obj->GetContainer(), obj->GetFlags());

			g_TextDialog->AddText(text);

			if (g_TextDialog->isVisible())
				g_TextDialog->activateWindow();
			else
				g_TextDialog->show();
		}
	}
}
//----------------------------------------------------------------------------------
void COrionAssistant::DumpTileInfo(const ushort &graphic, const short &x, const short &y, const char &z)
{
	OAFUN_DEBUG("c27_f20");
	QString text = "";

	text.sprintf("0x%04X %i %i %i\n", graphic, x, y, z);

	g_TextDialog->AddText(text);

	if (g_TextDialog->isVisible())
		g_TextDialog->activateWindow();
	else
		g_TextDialog->show();
}
//----------------------------------------------------------------------------------
void COrionAssistant::GetStatus(const uint &serial)
{
	OAFUN_DEBUG("c27_f21");
	g_LastStatusRequest = serial;
	CPacketStatusRequest(serial).SendServer();
}
//----------------------------------------------------------------------------------
void COrionAssistant::Click(const uint &serial)
{
	OAFUN_DEBUG("c27_f22");
	CPacketClickRequest(serial).SendServer();
}
//----------------------------------------------------------------------------------
void COrionAssistant::DoubleClick(const uint &serial)
{
	OAFUN_DEBUG("c27_f23");
	g_LastUseObject = serial;
	CPacketDoubleClickRequest(serial).SendServer();
}
//----------------------------------------------------------------------------------
void COrionAssistant::Attack(const uint &serial)
{
	OAFUN_DEBUG("c27_f24");
	CPacketAttackRequest(serial).SendServer();

	if (g_World != nullptr)
	{
		CGameCharacter *obj = g_World->FindWorldCharacter(serial);

		if (obj != nullptr && !obj->GetMaxHits())
			CPacketStatusRequest(serial).SendServer();
	}
}
//----------------------------------------------------------------------------------
uchar COrionAssistant::GetNotorietyFlags(const QString &text)
{
	OAFUN_DEBUG("c27_f25");
	uchar flags = NTF_ALL;

	CTextParser parser(text.toLower().toStdString(), "| ", "", "");

	QStringList list = parser.ReadTokens();

	foreach (const QString &str, list)
	{
		if (str == "innocent" || str == "blue")
			flags |= NTF_INNOCENT;
		else if (str == "friendly" || str == "green")
			flags |= NTF_FRIENDLY;
		else if (str == "gray")
			flags |= NTF_SOMEONE_GRAY;
		else if (str == "criminal")
			flags |= NTF_CRIMINAL;
		else if (str == "enemy" || str == "orange")
			flags |= NTF_ENEMY;
		else if (str == "murderer" || str == "red")
			flags |= NTF_MURDERER;
		else if (str == "invulnerable" || str == "yellow")
			flags |= NTF_INVULNERABLE;
	}

	return flags;
}
//----------------------------------------------------------------------------------
CGameObject *COrionAssistant::FindList(const QString &findListName, const QString &containerText, const QString &distanceText, const ushort &flags, const QString &notorietyText, bool recurseSearch, QStringList &result)
{
	OAFUN_DEBUG("c27_f26");
	QList<CFindItem> findListItems;

	CTextParser findListNameParser(findListName.toStdString(), "|", "", "");

	QStringList listNames = findListNameParser.ReadTokens();

	foreach (const QString &str, listNames)
	{
		CFindListItem *list = g_TabListsFind->GetFindList(str);

		if (list != nullptr)
		{
			QList<CFindItem> &temp = list->m_Items;

			foreach (const CFindItem &i, temp)
				findListItems.push_back(i);
		}
	}

	QList<CIgnoreItem> ignoreListItems;
	CTextParser ignoreListNameParser(m_UsedIgnoreList.toStdString(), "|", "", "");

	listNames = ignoreListNameParser.ReadTokens();

	foreach (const QString &str, listNames)
	{
		CIgnoreListItem *list = g_TabListsIgnore->GetIgnoreList(str);

		if (list != nullptr)
		{
			QList<CIgnoreItem> &temp = list->m_Items;

			foreach (const CIgnoreItem &i, temp)
				ignoreListItems.push_back(i);
		}
	}

	uchar notoriety = GetNotorietyFlags(notorietyText);
	uint container = TextToSerial(containerText);
	int distance = g_OrionAssistantForm->DistanceFromText(distanceText);
	CGameObject *found = nullptr;

	if (!findListItems.empty())
	{
		for (const CFindItem &item : findListItems)
		{
			CGameObject *temp = FindType(item, ignoreListItems, container, distance, flags, notoriety, recurseSearch, result);

			if (temp != nullptr)
			{
				if (flags & FTF_FAST)
					return temp;
				else if (flags & FTF_NEAREST)
				{
					if (found == nullptr || GetDistance(g_Player, temp) < GetDistance(g_Player, found))
						found = temp;
				}
				else
					found = temp;
			}
		}
	}

	if (flags & FTF_NEAREST)
	{
		result.clear();

		if (found != nullptr)
			result << COrionAssistant::SerialToText(found->GetSerial());
	}

	return found;
}
//----------------------------------------------------------------------------------
CGameObject *COrionAssistant::FindType(const QStringList &graphic, const QStringList &color, const QString &containerText, const QString &distanceText, const ushort &flags, const QString &notorietyText, bool recurseSearch, QStringList &result)
{
	OAFUN_DEBUG("c27_f27");
	result.clear();

	if (g_World == nullptr)
		return nullptr;

	QList<CIgnoreItem> ignoreListItems;
	CTextParser ignoreListNameParser(m_UsedIgnoreList.toStdString(), "|", "", "");

	QStringList listNames = ignoreListNameParser.ReadTokens();

	foreach (const QString &str, listNames)
	{
		CIgnoreListItem *list = g_TabListsIgnore->GetIgnoreList(str);

		if (list != nullptr)
		{
			QList<CIgnoreItem> &temp = list->m_Items;

			foreach (const CIgnoreItem &i, temp)
				ignoreListItems.push_back(i);
		}
	}

	uchar notoriety = GetNotorietyFlags(notorietyText);
	uint container = TextToSerial(containerText);
	int distance = g_OrionAssistantForm->DistanceFromText(distanceText);
	int graphicSize = (int)graphic.size();
	int colorSize = (int)color.size();
	CGameObject *found = nullptr;

	IFOR(i, 0, graphicSize)
	{
		IFOR(j, 0, colorSize)
		{
			CGameObject *temp = FindType(CFindItem(graphic[i], color[j]), ignoreListItems, container, distance, flags, notoriety, recurseSearch, result);

			if (temp != nullptr)
			{
				if (flags & FTF_FAST)
					return temp;
				else if (flags & FTF_NEAREST)
				{
					if (found == nullptr || GetDistance(g_Player, temp) < GetDistance(g_Player, found))
						found = temp;
				}
				else
					found = temp;
			}
		}
	}

	if (flags & FTF_NEAREST)
	{
		result.clear();

		if (found != nullptr)
			result << COrionAssistant::SerialToText(found->GetSerial());
	}

	return found;
}
//----------------------------------------------------------------------------------
CGameObject *COrionAssistant::FindType(const CFindItem &findListItem, const QList<CIgnoreItem> &ignoreList, const uint &container, const int &distance, ushort flags, const uchar &notoriety, bool recurseSearch, QStringList &list)
{
	OAFUN_DEBUG("c27_f28");
	if (g_World == nullptr)
		return nullptr;

	CGameObject *start = nullptr;
	CGameObject *result = nullptr;
	bool checkDistance = false;
	bool mobileOnly = false;
	bool itemsOnly = false;
	bool humanOnly = false;
	bool liveOnly = false;
	bool deadOnly = false;
	bool ignoreFriends = false;
	bool ignoreEnemies = false;

	if (container == 0xFFFFFFFF)
	{
		start = g_World->m_Items;
		checkDistance = true;
		recurseSearch = false;

		mobileOnly = (flags & FTF_MOBILE);
		itemsOnly = (flags & FTF_ITEM);
		humanOnly = (flags & FTF_HUMAN);
		liveOnly = (flags & FTF_LIVE);
		deadOnly = (flags & FTF_DEAD);
		ignoreFriends = (flags & FTF_IGNORE_FRIENDS);
		ignoreEnemies = (flags & FTF_IGNORE_ENEMIES);
	}
	else
	{
		CGameObject *obj = g_World->FindWorldObject(container);

		if (obj != nullptr)
		{
			if (obj->IsCorpse() && !EnabledCommandSearchCorpse())
				start = nullptr;
			else
				start = (CGameObject*)obj->m_Items;
		}

		if (flags & FTF_NEAREST)
			flags &= ~FTF_NEAREST;
	}

	QList<CGameObject*> containers;

	QFOR(item, start, CGameObject*)
	{
		if (!item->GetIgnored() &&
			/*(graphic == 0xFFFF || graphic == item->GetGraphic()) &&
			(color == 0xFFFF || color == item->GetColor()) &&*/
			(!checkDistance || GetDistance(g_Player, item) <= distance) &&
			findListItem.Found(item->GetGraphic(), item->GetColor()))
		{
			if (mobileOnly && !item->GetNPC())
				continue;
			else if (item->GetNPC())
			{
				if (itemsOnly)
					continue;
				else if (humanOnly && !item->IsHuman())
					continue;
				else
				{
					bool dead = ((CGameCharacter*)item)->Dead();

					if (liveOnly && dead)
						continue;
					else if (deadOnly && !dead)
						continue;
					else if (ignoreFriends && g_TabListsFriends->InFriendList(item->GetSerial()))
						continue;
					else if (ignoreEnemies && g_TabListsEnemies->InEnemyList(item->GetSerial()))
						continue;
				}
			}

			bool ignored = false;

			if (!ignoreList.empty())
			{
				for (const CIgnoreItem &ii : ignoreList)
				{
					if (ii.Found(item->GetSerial(), item->GetGraphic(), item->GetColor()))
					{
						ignored = true;
						break;
					}
				}
			}

			if (!ignored && notoriety)
			{
				if (item->GetNPC())
				{
					ignored = true;
					uchar itemNoto = ((CGameCharacter*)item)->GetNotoriety();

					if ((notoriety & NTF_INNOCENT) && itemNoto == NT_INNOCENT)
						ignored = false;
					else if ((notoriety & NTF_FRIENDLY) && itemNoto == NT_FRIENDLY)
						ignored = false;
					else if ((notoriety & NTF_SOMEONE_GRAY) && itemNoto == NT_SOMEONE_GRAY)
						ignored = false;
					else if ((notoriety & NTF_CRIMINAL) && itemNoto == NT_CRIMINAL)
						ignored = false;
					else if ((notoriety & NTF_ENEMY) && itemNoto == NT_ENEMY)
						ignored = false;
					else if ((notoriety & NTF_MURDERER) && itemNoto == NT_MURDERER)
						ignored = false;
					else if ((notoriety & NTF_INVULNERABLE) && itemNoto == NT_INVULNERABLE)
						ignored = false;
				}
			}

			if (!ignored)
			{
				list << COrionAssistant::SerialToText(item->GetSerial());

				if (flags & FTF_NEAREST)
				{
					if (result == nullptr || GetDistance(g_Player, item) < GetDistance(g_Player, result))
						result = item;
				}
				else
				{
					result = item;

					if (flags & FTF_FAST)
						break;
				}
			}
		}

		if (recurseSearch && !item->Empty() && (item->GetNPC() || ((CGameItem*)item)->GetLayer() != OL_BANK))
			containers.push_back(item);
	}

	if (!(flags & FTF_FAST) || result == nullptr)
	{
		for (CGameObject *item : containers)
		{
			CGameObject *temp = item->FindType(findListItem, ignoreList, flags, recurseSearch, list);

			if (temp != nullptr)
			{
				result = temp;

				if (flags & FTF_FAST)
					break;
			}
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
int COrionAssistant::CountType(const QStringList &graphic, const QStringList &color, const QString &containerText, const QString &distanceText, bool recurseSearch)
{
	OAFUN_DEBUG("c27_f29");
	if (g_World == nullptr)
		return 0;

	uint container = TextToSerial(containerText);
	int distance = g_OrionAssistantForm->DistanceFromText(distanceText);
	int graphicSize = (int)graphic.size();
	int colorSize = (int)color.size();
	int count = 0;

	IFOR(i, 0, graphicSize)
	{
		IFOR(j, 0, colorSize)
		{
			count += CountType(CFindItem(graphic[i], color[j]), container, distance, recurseSearch);
		}
	}

	return count;
}
//----------------------------------------------------------------------------------
int COrionAssistant::CountType(const CFindItem &findListItem, const uint &container, const int &distance, bool recurseSearch)
{
	OAFUN_DEBUG("c27_f30");
	if (g_World == nullptr)
		return 0;

	CGameObject *start = nullptr;
	int count = 0;
	bool checkDistance = false;

	if (container == 0xFFFFFFFF)
	{
		start = g_World->m_Items;
		checkDistance = true;
		recurseSearch = false;
	}
	else
	{
		CGameObject *obj = g_World->FindWorldObject(container);

		if (obj != nullptr)
			start = (CGameObject*)obj->m_Items;
	}

	QFOR(item, start, CGameObject*)
	{
		if (/*!item->GetIgnored() &&*/
			findListItem.Found(item->GetGraphic(), item->GetColor()) &&
			/*(graphic == 0xFFFF || graphic == item->GetGraphic()) &&
			(color == 0xFFFF || color == item->GetColor()) &&*/
			(!checkDistance || GetDistance(g_Player, item) <= distance))
		{
			count += item->GetCount();
		}

		if (recurseSearch && !item->Empty() && (item->GetNPC() || ((CGameItem*)item)->GetLayer() != OL_BANK))
		{
			count += item->CountItems(findListItem, recurseSearch);
		}
	}

	return count;
}
//----------------------------------------------------------------------------------
void COrionAssistant::UseType(const CFindItem &findListItem, const uint &container, const int &distance, const bool &recurseSearch)
{
	OAFUN_DEBUG("c27_f31");
	QStringList list;

	CGameObject *item = FindType(findListItem, QList<CIgnoreItem>(), container, distance, FTF_NORMAL, 0, recurseSearch, list);

	if (item != nullptr)
		DoubleClick(item->GetSerial());
}
//----------------------------------------------------------------------------------
void COrionAssistant::MoveItem(CGameItem *item)
{
	OAFUN_DEBUG("c27_f32");
	if (item != nullptr)
	{
		uint serial = item->GetSerial();

		if (g_MoveItemData.Count < 1)
			g_MoveItemData.Count = item->GetCount();

		CPacketPickupRequest(serial, g_MoveItemData.Count).SendServer();

		if (g_PacketManager.GetClientVersion() >= CV_6017)
			CPacketDropRequestNew(serial, g_MoveItemData.X, g_MoveItemData.Y, g_MoveItemData.Z, 0, g_MoveItemData.Container).SendServer();
		else
			CPacketDropRequestOld(serial, g_MoveItemData.X, g_MoveItemData.Y, g_MoveItemData.Z, g_MoveItemData.Container).SendServer();
	}
}
//----------------------------------------------------------------------------------
void COrionAssistant::MoveEquip(const uint &serial, const int &layer, const uint &container)
{
	OAFUN_DEBUG("c27_f33");
	CPacketPickupRequest(serial, 1).SendServer();
	CPacketEquipRequest(serial, layer, container).SendServer();
}
//----------------------------------------------------------------------------------
void COrionAssistant::Track(const bool &state, short x, short y)
{
	OAFUN_DEBUG("c27_f34");
	if (x < 0)
		x = g_Player->GetX();

	if (y < 0)
		y = g_Player->GetY();

	if (g_PacketManager.GetClientVersion() >= CV_7090)
		CPacketQuestArrowNew(state, x, y).SendClient();
	else
		CPacketQuestArrowOld(state, x, y).SendClient();

	g_UOMapManager.UOAMnotifyTrack(state, x, y);
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetType(puchar buf)
{
	OAFUN_DEBUG("c27_f35");
	g_TabListsTypes->SetTypeFromTarget(unpack16(buf + 17));
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetObject(puchar buf)
{
	OAFUN_DEBUG("c27_f36");
	g_TabListsObjects->SetObjectFromTarget(unpack32(buf + 7));
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetDisplayItem(puchar buf)
{
	OAFUN_DEBUG("c27_f37");
	ushort color = 0;

	if (g_World != nullptr)
	{
		CGameObject *obj = g_World->FindWorldObject(unpack32(buf + 7));

		if (obj != nullptr)
			color = obj->GetColor();
	}

	g_TabDisplay->SetDisplayItemFromTarget(unpack16(buf + 17), color);
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetInfo(puchar buf)
{
	OAFUN_DEBUG("c27_f38");
	if (g_World != nullptr)
		DumpObjectInfo(g_World->FindWorldObject(unpack32(buf + 7)));
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetInfoTile(puchar buf)
{
	OAFUN_DEBUG("c27_f39");
	if (g_World != nullptr)
		DumpTileInfo(unpack16(buf + 17), unpack16(buf + 11), unpack16(buf + 13), buf[16]);
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetDressBag(puchar buf)
{
	OAFUN_DEBUG("c27_f40");
	g_TabListsObjects->SetDressBag(unpack32(buf + 7));
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetDressItem(puchar buf)
{
	OAFUN_DEBUG("c27_f41");
	if (g_World != nullptr)
	{
		CGameItem *item = g_World->FindWorldItem(unpack32(buf + 7));

		if (item != nullptr)
		{
			int layer = item->GetEquipLayer();

			if (layer && layer < OL_MOUNT && layer != OL_BACKPACK)
				g_TabAgentsDress->AddDressItemInList(item);
		}
	}
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetHide(puchar buf)
{
	OAFUN_DEBUG("c27_f42");
	CPacketHideObject(unpack32(buf + 7)).SendClient();
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetDrop(puchar buf)
{
	OAFUN_DEBUG("c27_f43");
	MoveItem(g_World->FindWorldItem(unpack32(buf + 7)));
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetFindList(puchar buf)
{
	OAFUN_DEBUG("c27_f44");
	g_TabListsFind->AddFindListItemInList(g_World->FindWorldObject(unpack32(buf + 7)));
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetIgnoreList(puchar buf)
{
	OAFUN_DEBUG("c27_f45");
	g_TabListsIgnore->AddIgnoreListItemInList(g_World->FindWorldObject(unpack32(buf + 7)));
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetPartyObject(puchar buf)
{
	OAFUN_DEBUG("c27_f46");
	g_TabAgentsParty->SetPartyObjectFromTarget(unpack32(buf + 7));
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetFriendObject(puchar buf)
{
	OAFUN_DEBUG("c27_f47");
	g_TabListsFriends->SetFriendObjectFromTarget(unpack32(buf + 7));
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetEnemyObject(puchar buf)
{
	OAFUN_DEBUG("c27_f48");
	g_TabListsEnemies->SetEnemyObjectFromTarget(unpack32(buf + 7));
}
//----------------------------------------------------------------------------------
void COrionAssistant::HandleTargetAnimalAndVendorControlName(puchar buf)
{
	OAFUN_DEBUG("c27_f48_1");

	if (g_AnimalAndVendorControlForm == nullptr)
		return;

	CGameObject *obj = g_World->FindWorldObject(unpack32(buf + 7));

	if (obj != nullptr)
	{
		g_AnimalAndVendorControlForm->SetName(obj->GetName());

		if (g_AnimalAndVendorControlForm->isVisible())
			g_AnimalAndVendorControlForm->activateWindow();
	}
}
//----------------------------------------------------------------------------------
CGameObject *COrionAssistant::FindFriend(const ushort &flags, const QString &distanceText)
{
	OAFUN_DEBUG("c27_f49");
	int distance = g_OrionAssistantForm->DistanceFromText(distanceText);
	CGameObject *found = nullptr;
	CGameObject *start = g_World->m_Items;

	if ((flags & FTF_NEXT) && g_LastFriendFound)
	{
		QFOR(item, start, CGameObject*)
		{
			if (item->GetSerial() == g_LastFriendFound)
			{
				start = (CGameObject*)item->m_Next;
				break;
			}
		}
	}

	QFOR(item, start, CGameObject*)
	{
		if (item->GetIgnored() || !item->GetNPC())
			continue;
		else if (!g_TabListsFriends->InFriendList(item->GetSerial()))
			continue;
		else if (GetDistance(g_Player, item) > distance)
			continue;

		CGameCharacter *gc = (CGameCharacter*)item;

		bool dead = gc->Dead();

		if ((flags & FTF_LIVE) && dead)
			continue;
		else if ((flags & FTF_DEAD) && !dead)
			continue;
		else if ((flags & FTF_INJURED) && (!gc->GetHits() || gc->GetHits() >= gc->GetMaxHits()))
			continue;

		if (flags & FTF_FAST)
			return item;
		else if (flags & FTF_NEAREST)
		{
			if (found == nullptr || GetDistance(g_Player, item) < GetDistance(g_Player, found))
				found = item;
		}
		else if ((flags & FTF_INJURED) && found != nullptr)
		{
			if (gc->GetHits() < ((CGameCharacter*)found)->GetHits())
				found = item;
		}
		else
			found = item;
	}

	if ((flags & FTF_NEXT) && g_LastFriendFound && found == nullptr)
		found = FindFriend(flags & (~FTF_NEXT), distanceText);

	return found;
}
//----------------------------------------------------------------------------------
CGameObject *COrionAssistant::FindEnemy(const ushort &flags, const QString &distanceText)
{
	OAFUN_DEBUG("c27_f49");
	int distance = g_OrionAssistantForm->DistanceFromText(distanceText);
	CGameObject *found = nullptr;
	CGameObject *start = g_World->m_Items;

	if ((flags & FTF_NEXT) && g_LastEnemyFound)
	{
		QFOR(item, start, CGameObject*)
		{
			if (item->GetSerial() == g_LastEnemyFound)
			{
				start = (CGameObject*)item->m_Next;
				break;
			}
		}
	}

	QFOR(item, g_World->m_Items, CGameObject*)
	{
		if (item->GetIgnored() || !item->GetNPC())
			continue;
		else if (!g_TabListsEnemies->InEnemyList(item->GetSerial()))
			continue;
		else if (GetDistance(g_Player, item) > distance)
			continue;

		CGameCharacter *gc = (CGameCharacter*)item;

		bool dead = gc->Dead();

		if ((flags & FTF_LIVE) && dead)
			continue;
		else if ((flags & FTF_DEAD) && !dead)
			continue;
		else if ((flags & FTF_INJURED) && (!gc->GetHits() || gc->GetHits() >= gc->GetMaxHits()))
			continue;

		if (flags & FTF_FAST)
			return item;
		else if (flags & FTF_NEAREST)
		{
			if (found == nullptr || GetDistance(g_Player, item) < GetDistance(g_Player, found))
				found = item;
		}
		else if ((flags & FTF_INJURED) && found != nullptr)
		{
			if (gc->GetHits() < ((CGameCharacter*)found)->GetHits())
				found = item;
		}
		else
			found = item;
	}

	if ((flags & FTF_NEXT) && g_LastEnemyFound && found == nullptr)
		found = FindEnemy(flags & (~FTF_NEXT), distanceText);

	return found;
}
//----------------------------------------------------------------------------------
void COrionAssistant::GetFriendsStatus()
{
	OAFUN_DEBUG("c27_f50");
	QFOR(item, g_World->m_Items, CGameObject*)
	{
		if (g_TabListsFriends->InFriendList(item->GetSerial()))
			GetStatus(item->GetSerial());
	}
}
//----------------------------------------------------------------------------------
void COrionAssistant::GetEnemiesStatus()
{
	OAFUN_DEBUG("c27_f51");
	QFOR(item, g_World->m_Items, CGameObject*)
	{
		if (g_TabListsEnemies->InEnemyList(item->GetSerial()))
			GetStatus(item->GetSerial());
	}
}
//---------------------------------------------------------------------------
bool COrionAssistant::IsNoDrawTile(const ushort &graphic)
{
	switch (graphic)
	{
		case 0x0001:
		case 0x21BC:
		case 0x9E4C:
		case 0x9E64:
		case 0x9E65:
		case 0x9E7D:
			return true;
		default:
			break;
	}

	if (graphic != 0x63D3)
	{
		if (graphic >= 0x2198 && graphic <= 0x21A4)
			return true;

		long long flags = g_GetStaticFlags(graphic);

		if (!::IsNoDiagonal(flags) || (::IsAnimated(flags) && g_Player != nullptr && g_Player->GetRace() == RT_GARGOYLE))
			return false;
	}

	return true;
}
//----------------------------------------------------------------------------------
