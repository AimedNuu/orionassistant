/***********************************************************************************
**
** TabListsFind.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABLISTSFIND_H
#define TABLISTSFIND_H
//----------------------------------------------------------------------------------
#include <QWidget>
#include "../GUI/findlistitem.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabListsFind;
}
//----------------------------------------------------------------------------------
class TabListsFind : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_Find_clicked(const QModelIndex &index);

	void on_pb_FindCreate_clicked();

	void on_pb_FindSave_clicked();

	void on_pb_FindRemove_clicked();

	void on_lw_FindContent_clicked(const QModelIndex &index);

	void on_pb_FindContentFromTarget_clicked();

	void on_pb_FindContentCreate_clicked();

	void on_pb_FindContentSave_clicked();

	void on_pb_FindContentRemove_clicked();

private:
	Ui::TabListsFind *ui;

public slots:
	void SaveFindList();

public:
	explicit TabListsFind(QWidget *parent = 0);
	~TabListsFind();

	void OnDeleteKeyClick(QWidget *widget);

	void LoadFindList();

	void AddFindListItemInList(class CGameObject *obj);

	CFindListItem *GetFindList(QString name);

	void AddFindList(QString listName, const QString &graphic, const QString &color, const QString &comment);

	void ClearFindList(QString listName);

};
//----------------------------------------------------------------------------------
extern TabListsFind *g_TabListsFind;
//----------------------------------------------------------------------------------
#endif // TABLISTSFIND_H
//----------------------------------------------------------------------------------
