/***********************************************************************************
**
** TabScripts.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABSCRIPTS_H
#define TABSCRIPTS_H
//----------------------------------------------------------------------------------
#include <QWidget>
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
#include "../GUI/runningscriptlistitem.h"
//----------------------------------------------------------------------------------
Q_DECLARE_METATYPE(CRunningScriptListItem)
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabScripts;
}
//----------------------------------------------------------------------------------
class TabScripts : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_RunningScriptsList_clicked(const QModelIndex &index);

	void on_pb_ScriptStart_clicked();

	void on_pb_ScriptPauseOrResume_clicked();

	void on_pb_ScriptStop_clicked();

	void on_tb_SelectScriptFilePath_clicked();

	void on_tb_ReloadScriptFile_clicked();

	void on_pb_ScriptOpenEditor_clicked();

	void on_clb_ScriptExamples_clicked();

	void on_cb_SortScripts_clicked();

private:
	Ui::TabScripts *ui;

signals:
	void signal_RemoveRunningScriptItem(CRunningScriptListItem *item);

public slots:
	void slot_RemoveRunningScriptItem(CRunningScriptListItem *item);

public:
	explicit TabScripts(QWidget *parent = 0);
	~TabScripts();

	void Init();

	void OnDeleteKeyClick(QWidget *widget);

	void LoadConfig(const QXmlStreamAttributes &attributes);

	void SaveConfig(QXmlStreamWriter &writter);

	CRunningScriptListItem *RunScript(const QString &functionName, const QString &scriptBody, const uint &key, const uchar &modifiers, const QStringList &args);

	void TerminateScript(const QString &functionName, const QString &functionsSave);

	bool ScriptRunning(const QString &functionName);

	bool ScriptRunning(const uint &key, const uchar &modifiers);

	QString OpenScriptFileDialog();

	QString SaveScriptFileDialog();

	void LoadScript(const QString &filePath);

	void SetScriptPath(const QString &path);

	bool SaveAsScript(const QString &path);

	bool SaveScript(const bool &autoloadReplace);

	void UpdateScriptsList();

	bool HotkeyExistsAndUse(const uint &wParam, const uint &lParam, uint mod);
};
//----------------------------------------------------------------------------------
extern TabScripts *g_TabScripts;
//----------------------------------------------------------------------------------
#endif // TABSCRIPTS_H
//----------------------------------------------------------------------------------
