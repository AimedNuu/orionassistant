/***********************************************************************************
**
** TabFiltersSpeech.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABFILTERSSPEECH_H
#define TABFILTERSSPEECH_H
//----------------------------------------------------------------------------------
#include <QWidget>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabFiltersSpeech;
}
//----------------------------------------------------------------------------------
class TabFiltersSpeech : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_FiltersSpeechList_clicked(const QModelIndex &index);

	void on_pb_FilterSpeechAdd_clicked();

	void on_pb_FilterSpeechSave_clicked();

	void on_pb_FilterSpeechRemove_clicked();

private:
	Ui::TabFiltersSpeech *ui;

public slots:
	void SaveSpeechFilter();

public:
	explicit TabFiltersSpeech(QWidget *parent = 0);
	~TabFiltersSpeech();

	void LoadSpeechFilter();

	void OnDeleteKeyClick(QWidget *widget);

	bool CheckSpeechInFilter(const QString &text, const ushort &color);
};
//----------------------------------------------------------------------------------
extern TabFiltersSpeech *g_TabFiltersSpeech;
//----------------------------------------------------------------------------------
#endif // TABFILTERSSPEECH_H
//----------------------------------------------------------------------------------
