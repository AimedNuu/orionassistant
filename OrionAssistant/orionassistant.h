/***********************************************************************************
**
** OrionAssistant.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef CORIONASSISTANT_H
#define CORIONASSISTANT_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../CommonItems/skill.h"
#include "../CommonItems/finditem.h"
#include "../CommonItems/ignoreitem.h"
//----------------------------------------------------------------------------------
Q_DECLARE_METATYPE(UCHAR_LIST)
//----------------------------------------------------------------------------------
class COrionAssistant;
typedef void (COrionAssistant::*TARGET_HANDLER)(puchar buf);
extern TARGET_HANDLER g_TargetHandler;
//----------------------------------------------------------------------------------
enum FIND_TYPE_FLAGS
{
	FTF_NORMAL			= 0x0000,
	FTF_NEAREST			= 0x0001,
	FTF_FAST			= 0x0002,
	FTF_MOBILE			= 0x0004,
	FTF_ITEM			= 0x0008,
	FTF_HUMAN			= 0x0010,
	FTF_LIVE			= 0x0020,
	FTF_DEAD			= 0x0040,
	FTF_INJURED			= 0x0080,
	FTF_NEXT			= 0x0100,
	FTF_IGNORE_FRIENDS	= 0x0200,
	FTF_IGNORE_ENEMIES	= 0x0400
};
//----------------------------------------------------------------------------------
enum NOTORIETY_TYPE_FLAGS
{
	NTF_ALL				= 0x00,
	NTF_INNOCENT		= 0x01,
	NTF_FRIENDLY		= 0x02,
	NTF_SOMEONE_GRAY	= 0x04,
	NTF_CRIMINAL		= 0x08,
	NTF_ENEMY			= 0x10,
	NTF_MURDERER		= 0x20,
	NTF_INVULNERABLE	= 0x40
};
//----------------------------------------------------------------------------------
struct MOVITEM_DATA
{
	int Count{0};
	uint Container{0xFFFFFFFF};
	ushort X{0xFFFF};
	ushort Y{0xFFFF};
	char Z{0};

	void Reset()
	{
		Count = 0;
		Container = 0xFFFFFFFF;
		X = 0xFFFF;
		Y = 0xFFFF;
		Z = 0;
	}
};
//----------------------------------------------------------------------------------
extern MOVITEM_DATA g_MoveItemData;
//----------------------------------------------------------------------------------
class COrionAssistant : public QObject
{
	Q_OBJECT

	SETGET(QString, UsedIgnoreList, "")

signals:
	void signal_ScriptError(const QString &title, const QString &text);

	void signal_SendClient(UCHAR_LIST buf);
	void signal_SendServer(UCHAR_LIST buf);

public slots:
	void slot_ScriptError(const QString &title, const QString &text);

	void SendClient(UCHAR_LIST buf) { SendClient(&buf[0], buf.size()); }
	void SendServer(UCHAR_LIST buf) { SendServer(&buf[0], buf.size()); }

private:
	void SendClient(const uchar *buf, int size);
	void SendServer(const uchar *buf, int size);

public:
	explicit COrionAssistant(QObject *parent = 0);
	virtual ~COrionAssistant();

	static uint RawStringToUInt(const QString &value);

	static bool RawStringToBool(QString value);

	static uint TextToUInt(const QString &text);

	static uint TextToSerial(QString text);
	static ushort TextToGraphic(QString text);
	static int TextToLayer(QString text);

	static QString ColorToText(const uint &value);
	static QString SerialToText(const uint &value);
	static QString GraphicToText(const ushort &value);
	static QString BoolToText(const bool &value);

	void ClientPrint(const QString &text, const ushort &color = g_MessagesColor);

	void ClientCharPrint(const uint &serial, const QString &text, const ushort &color = g_MessagesColor);

	void ClientPrintDebugLevel1(const QString &text, const ushort &color = g_MessagesColor);
	void ClientPrintDebugLevel2(const QString &text, const ushort &color = g_MessagesColor);

	void RequestTarget(const QString &text);

	void DumpObjectInfo(class CGameObject *obj);
	void DumpTileInfo(const ushort &graphic, const short &x, const short &y, const char &z);

	void GetStatus(const uint &serial);
	void Click(const uint &serial);
	void DoubleClick(const uint &serial);
	void Attack(const uint &serial);
	uchar GetNotorietyFlags(const QString &text);
	class CGameObject *FindList(const QString &findListName, const QString &containerText, const QString &distanceText, const ushort &flags, const QString &notorietyText, bool recurseSearch, QStringList &result);
	class CGameObject *FindType(const QStringList &graphic, const QStringList &color, const QString &containerText, const QString &distanceText, const ushort &flags, const QString &notorietyText, bool recurseSearch, QStringList &result);
	class CGameObject *FindType(const CFindItem &findListItem, const QList<CIgnoreItem> &ignoreList, const uint &container, const int &distance, ushort flags, const uchar &notoriety, bool recurseSearch, QStringList &list);
	class CGameObject *FindFriend(const ushort &flags, const QString &distanceText);
	class CGameObject *FindEnemy(const ushort &flags, const QString &distanceText);
	void GetFriendsStatus();
	void GetEnemiesStatus();
	int CountType(const QStringList &graphic, const QStringList &color, const QString &containerText, const QString &distanceText, bool recurseSearch);
	int CountType(const CFindItem &findListItem, const uint &container, const int &distance, bool recurseSearch);
	void UseType(const CFindItem &findListItem, const uint &container, const int &distance, const bool &recurseSearch);
	void MoveItem(class CGameItem *item);
	void MoveEquip(const uint &serial, const int &layer, const uint &container);
	void Track(const bool &state, short x, short y);

	void HandleTargetType(puchar buf);
	void HandleTargetObject(puchar buf);
	void HandleTargetDisplayItem(puchar buf);
	void HandleTargetInfo(puchar buf);
	void HandleTargetInfoTile(puchar buf);
	void HandleTargetDressBag(puchar buf);
	void HandleTargetDressItem(puchar buf);
	void HandleTargetHide(puchar buf);
	void HandleTargetDrop(puchar buf);
	void HandleTargetFindList(puchar buf);
	void HandleTargetIgnoreList(puchar buf);
	void HandleTargetPartyObject(puchar buf);
	void HandleTargetFriendObject(puchar buf);
	void HandleTargetEnemyObject(puchar buf);
	void HandleTargetAnimalAndVendorControlName(puchar buf);

	static bool IsNoDrawTile(const ushort &graphic);
};
//----------------------------------------------------------------------------------
extern COrionAssistant g_OrionAssistant;
//----------------------------------------------------------------------------------
#endif // CORIONASSISTANT_H
//----------------------------------------------------------------------------------
