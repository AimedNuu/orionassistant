/***********************************************************************************
**
** MainScript.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef MAINSCRIPT_H
#define MAINSCRIPT_H
//----------------------------------------------------------------------------------
#include <windows.h>

#include "orionassistant_global.h"
#include "orionassistantform.h"
#include "../Managers/PacketManager.h"
//----------------------------------------------------------------------------------
LRESULT ClientMessageQueue(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
bool __cdecl DllRecv(unsigned char *buf, const int &size);
bool __cdecl DllSend(unsigned char *buf, const int &size);
void __cdecl OnClientDisconnect();
void __cdecl OnClientWorldDraw();
void __cdecl OnClientSceneDraw();
void __cdecl OnClientWorldMapDraw();
//----------------------------------------------------------------------------------
extern "C" ORIONASSISTANTSHARED_EXPORT void Install(PPLUGIN_INTERFACE intr)
{
	qDebug() << "Install";

	static bool mode = false;

	if (intr == nullptr)
		return;

	if (!mode)
	{
		mode = true;

		intr->WindowProc = ClientMessageQueue;
		intr->OnRecv = DllRecv;
		intr->OnSend = DllSend;
		intr->OnDisconnect = OnClientDisconnect;
		intr->OnWorldDraw = OnClientWorldDraw;
		intr->OnSceneDraw = OnClientSceneDraw;
	}
	else if (g_OrionAssistantForm == nullptr)
	{
		if (intr->Client->Size != sizeof(PLUGIN_CLIENT_INTERFACE) || intr->Client->Version != 2
				|| intr->Client->GL->Version != 0 || intr->Client->UO->Version != 2
				|| intr->Client->ClilocManager->Version != 0|| intr->Client->ColorManager->Version != 0
				|| intr->Client->PathFinder->Version != 0 || intr->Client->FileManager->Version != 0)
		{
			QMessageBox::critical(nullptr, "Critical!", "Mismatched version of the OrionUO.exe and OrionAssistant.dll.\nPlease, update your files for correct work!\nProgram can be crashes!");
			ShellExecute(0, L"Open", L"https://github.com/Hotride/OrionUO", NULL, NULL, SW_SHOWNORMAL);
			ShellExecute(0, L"Open", L"http://forum.orion-client.online/index.php", NULL, NULL, SW_SHOWNORMAL);
		}

		g_ClientInterface = intr;
		g_ClientHandle = g_ClientInterface->hWnd;
		g_PacketManager.SetClientVersion((CLIENT_VERSION)g_ClientInterface->ClientVersion);

		g_SendClient = (PACKET_PROC*)g_ClientInterface->Recv;
		g_SendServer = (PACKET_PROC*)g_ClientInterface->Send;

		IUltimaOnline *uo = g_ClientInterface->Client->UO;

		g_GetStaticFlags = (FUNCDEF_GET_STATIC_FLAGS*)uo->GetStaticFlags;
		g_GetInt = (FUNCDEF_GET_VALUE_INT*)uo->GetValueInt;
		g_SetInt = (FUNCDEF_SET_VALUE_INT*)uo->SetValueInt;
		g_GetString = (FUNCDEF_GET_VALUE_STRING*)uo->GetValueString;
		g_SetString = (FUNCDEF_SET_VALUE_STRING*)uo->SetValueString;

		g_OrionAssistantForm = new OrionAssistantForm(nullptr);
		g_OrionAssistantForm->Init();
	}
}
//----------------------------------------------------------------------------------
#endif // MAINSCRIPT_H
//----------------------------------------------------------------------------------
