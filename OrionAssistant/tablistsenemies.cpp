// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabListsEnemies.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tablistsenemies.h"
#include "ui_tablistsenemies.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include "../GUI/worldobjectlistitem.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
#include "../GameObjects/GameWorld.h"
#include "../CommonItems/objectnamereceiver.h"
//----------------------------------------------------------------------------------
TabListsEnemies *g_TabListsEnemies = nullptr;
//----------------------------------------------------------------------------------
TabListsEnemies::TabListsEnemies(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::TabListsEnemies)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabListsEnemies = this;

	connect(ui->lw_Enemies, SIGNAL(itemDropped()), this, SLOT(OnListItemMoved()));
}
//----------------------------------------------------------------------------------
TabListsEnemies::~TabListsEnemies()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabListsEnemies = nullptr;
}
//----------------------------------------------------------------------------------
void TabListsEnemies::OnListItemMoved()
{
	OAFUN_DEBUG("");
	QString path = g_OrionAssistantForm->GetSaveConfigPath();

	if (!path.length())
		return;

	SaveEnemies(path + "/Enemies.xml");
}
//----------------------------------------------------------------------------------
void TabListsEnemies::on_lw_Enemies_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f221");
	CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Enemies->item(index.row());

	if (item != nullptr)
	{
		ui->le_EnemyName->setText(item->GetName());
		ui->le_EnemySerial->setText(COrionAssistant::SerialToText(item->GetSerial()));
	}
}
//----------------------------------------------------------------------------------
void TabListsEnemies::on_pb_EnemyCreate_clicked()
{
	OAFUN_DEBUG("c28_f222");
	uint serial = COrionAssistant::TextToSerial(ui->le_EnemySerial->text());

	if (!serial)
	{
		QMessageBox::critical(this, "Serial is empty", "Enter the object serial!");
		return;
	}

	IFOR(i, 0, ui->lw_Enemies->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Enemies->item(i);

		if (item != nullptr && item->GetSerial() == serial)
		{
			QMessageBox::critical(this, "Serial is already exists", "Object serial is already exists!");
			return;
		}
	}

	new CWorldObjectListItem(serial, ui->le_EnemyName->text(), true, ui->lw_Enemies);

	ui->lw_Enemies->setCurrentRow(ui->lw_Enemies->count() - 1);

	OnListItemMoved();
}
//----------------------------------------------------------------------------------
void TabListsEnemies::on_pb_EnemySave_clicked()
{
	OAFUN_DEBUG("c28_f223");
	uint serial = COrionAssistant::TextToSerial(ui->le_EnemySerial->text());

	if (!serial)
	{
		QMessageBox::critical(this, "Serial is empty", "Enter the object serial!");
		return;
	}

	CWorldObjectListItem *selected = (CWorldObjectListItem*)ui->lw_Enemies->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Object is not selected", "Object is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_Enemies->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Enemies->item(i);

		if (item != nullptr && item->GetSerial() == serial)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Serial is already exists", "Object serial is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->SetSerial(serial);
	selected->SetName(ui->le_EnemyName->text());
	selected->UpdateText();

	OnListItemMoved();
}
//----------------------------------------------------------------------------------
void TabListsEnemies::on_pb_EnemyRemove_clicked()
{
	OAFUN_DEBUG("c28_f224");
	QListWidgetItem *item = ui->lw_Enemies->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_Enemies->takeItem(ui->lw_Enemies->row(item));

		if (item != nullptr)
		{
			delete item;
			OnListItemMoved();
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsEnemies::on_pb_EnemyFromTarget_clicked()
{
	OAFUN_DEBUG("c28_f225");
	g_TargetHandler = &COrionAssistant::HandleTargetEnemyObject;

	g_OrionAssistant.RequestTarget("Select a object for obtain serial");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
void TabListsEnemies::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_Enemies)
		on_pb_EnemyRemove_clicked();
}
//----------------------------------------------------------------------------------
void TabListsEnemies::LoadEnemies(const QString &path)
{
	OAFUN_DEBUG("c28_f131");
	ui->lw_Enemies->clear();

	QFile file(path);

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;
		int count = 0;

		Q_UNUSED(version);
		Q_UNUSED(count);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("size"))
						count = attributes.value("size").toInt();
				}
				else if (reader.name() == "enemy")
				{
					if (attributes.hasAttribute("id") && attributes.hasAttribute("name") && attributes.hasAttribute("enabled"))
					{
						new CWorldObjectListItem(COrionAssistant::TextToSerial(attributes.value("id").toString()), attributes.value("name").toString(),
										   COrionAssistant::RawStringToBool(attributes.value("enabled").toString()), ui->lw_Enemies);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsEnemies::SaveEnemies(const QString &path)
{
	OAFUN_DEBUG("c28_f139");
	QFile file(path);

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_Enemies->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");
		writter.writeAttribute("size", QString::number(count));

		IFOR(i, 0, count)
		{
			CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Enemies->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("enemy");

				writter.writeAttribute("id", COrionAssistant::SerialToText(item->GetSerial()));
				writter.writeAttribute("name", item->GetName());
				writter.writeAttribute("enabled", COrionAssistant::BoolToText(item->checkState() == Qt::Checked));

				writter.writeEndElement(); //enemy
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsEnemies::SetEnemyObjectFromTarget(const uint &serial)
{
	OAFUN_DEBUG("c28_f226");
	if (serial)
	{
		ui->le_EnemySerial->setText(COrionAssistant::SerialToText(serial));

		if (g_World != nullptr)
		{
			CGameObject *obj = g_World->FindWorldObject(serial);

			if (obj != nullptr)
				ui->le_EnemyName->setText(obj->GetName());
			else
			{
				g_OrionAssistant.Click(serial);
				new CObjectNameReceiver(serial, ui->le_EnemyName);
			}
		}
	}
}
//----------------------------------------------------------------------------------
bool TabListsEnemies::InEnemyList(const uint &serial)
{
	OAFUN_DEBUG("c28_f231");
	int count = ui->lw_Enemies->count();

	IFOR(i, 0, count)
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Enemies->item(i);

		if (item != nullptr && item->checkState() == Qt::Checked && item->GetSerial() == serial)
			return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
QStringList TabListsEnemies::GetEnemyList()
{
	OAFUN_DEBUG("c28_f231");
	int count = ui->lw_Enemies->count();
	QStringList result;

	IFOR(i, 0, count)
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Enemies->item(i);

		if (item != nullptr && item->checkState() == Qt::Checked)
			result << COrionAssistant::SerialToText(item->GetSerial());
	}

	return result;
}
//----------------------------------------------------------------------------------
void TabListsEnemies::AddEnemy(QString name, const QString &object)
{
	OAFUN_DEBUG("c28_f240");
	if (!object.length())
	{
		ui->le_EnemyName->setText(name);
		g_OrionAssistantForm->SetAutosaveAfterTargetingName(name);

		on_pb_EnemyFromTarget_clicked();

		return;
	}

	QString lowerName = name.toLower();

	IFOR(i, 0, ui->lw_Enemies->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_Enemies->item(i);

		if (item != nullptr && item->text().toLower() == lowerName)
		{
			item->SetName(name);
			item->SetSerial(COrionAssistant::TextToSerial(object));
			return;
		}
	}

	new CWorldObjectListItem(COrionAssistant::TextToSerial(object), name, true, ui->lw_Enemies);
}
//----------------------------------------------------------------------------------
void TabListsEnemies::RemoveEnemy(QString name)
{
	OAFUN_DEBUG("c28_f241");
	QString lowerName = name.toLower();

	IFOR(i, 0, ui->lw_Enemies->count())
	{
		QListWidgetItem *item = ui->lw_Enemies->item(i);

		if (item != nullptr && item->text().toLower() == lowerName)
		{
			item = ui->lw_Enemies->takeItem(i);

			if (item != nullptr)
				delete item;
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsEnemies::ClearEnemyList()
{
	OAFUN_DEBUG("c28_f242");
	ui->lw_Enemies->clear();
}
//----------------------------------------------------------------------------------
