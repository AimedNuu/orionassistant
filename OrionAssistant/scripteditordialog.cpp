// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ScriptEditorDialog.cpp
**
** Copyright (C) January 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "scripteditordialog.h"
#include "ui_scripteditordialog.h"
#include "orionassistantform.h"
#include "orionassistant.h"
#include "tabscripts.h"
//----------------------------------------------------------------------------------
ScriptEditorDialog *g_ScriptEditorDialog = nullptr;
//----------------------------------------------------------------------------------
ScriptEditorDialog::ScriptEditorDialog(QWidget *parent)
: QMainWindow(parent), ui(new Ui::ScriptEditorDialog),
m_FindDialog(), m_GoToLineDialog(), m_ReplaceDialog()
{
	OAFUN_DEBUG("c29_f1");
	ui->setupUi(this);

	Qt::WindowFlags flags = windowFlags() | Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint;
	setWindowFlags(flags);

	connect(&m_FindDialog, SIGNAL(signal_SearchText(QString, bool, bool)), this, SLOT(slot_SearchText(QString, bool, bool)));
	connect(&m_GoToLineDialog, SIGNAL(signal_GoToLine(int)), this, SLOT(slot_GoToLine(int)));
	connect(&m_ReplaceDialog, SIGNAL(signal_SearchText(QString, bool, bool)), this, SLOT(slot_SearchText(QString, bool, bool)));
	connect(&m_ReplaceDialog, SIGNAL(signal_ReplaceText(QString, QString, bool, bool, bool)), this, SLOT(slot_ReplaceText(QString, QString, bool, bool, bool)));

	connect(ui->pte_Script, SIGNAL(signal_CtrlQPressed()), this, SLOT(slot_CtrlQPressed()));
}
//----------------------------------------------------------------------------------
ScriptEditorDialog::~ScriptEditorDialog()
{
	OAFUN_DEBUG("");
	delete ui;
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::slot_CtrlQPressed()
{
	OAFUN_DEBUG("");
	POINT p;
	GetCursorPos(&p);
	::ScreenToClient(g_ClientHandle, &p);

	QString str = "";
	str.sprintf("MousePos: %i,%i", (int)p.x, (int)p.y);

	ui->actionMousePos->setText(str);
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::LoadScript(const QString &filePath)
{
	OAFUN_DEBUG("c29_f2");
	QFile file(filePath);

	if (file.exists() && file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		SetText(QTextStream(&file).readAll());

		file.close();
	}
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::ClearText()
{
	OAFUN_DEBUG("c29_f3");
	ui->pte_Script->clear();
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::SetText(const QString &text)
{
	OAFUN_DEBUG("c29_f4");
	ui->pte_Script->setPlainText(text);
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::AddText(const QString &text)
{
	OAFUN_DEBUG("c29_f5");
	ui->pte_Script->appendPlainText(text);
}
//----------------------------------------------------------------------------------
QString ScriptEditorDialog::GetText()
{
	OAFUN_DEBUG("c29_f6");
	return ui->pte_Script->toPlainText();
}
//----------------------------------------------------------------------------------
QStringList ScriptEditorDialog::GetFunctionList()
{
	OAFUN_DEBUG("c29_f7");
	QStringList list;

	QTextDocument *doc = ui->pte_Script->document();

	int count = doc->lineCount();

	IFOR(i, 0, count)
	{
		QString str = doc->findBlockByLineNumber(i).text().trimmed();

		if (str.indexOf("//--#") == 0)
			list << str.remove(0, 4);
		else if (str.toLower().indexOf("function ") == 0)
		{
			str = str.remove(0, 9);

			int pos = str.indexOf("(");

			if (pos > 0 && pos == str.indexOf("()"))
			//if (pos > 0 && pos == str.indexOf("("))
			{
				str.resize(pos);

				list << str.trimmed();
			}
		}
	}

	return list;
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::GoLineText(const QString &text)
{
	OAFUN_DEBUG("c29_f8");
	QTextDocument *doc = ui->pte_Script->document();

	int count = doc->lineCount();

	IFOR(i, 0, count)
	{
		QString str = doc->findBlockByLineNumber(i).text();

		if (str.indexOf(text) == 0)
			GoToLine(i);
	}
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::GoToLine(const int &lineIndex)
{
	OAFUN_DEBUG("c29_f9");
	QTextDocument *doc = ui->pte_Script->document();

	int count = doc->lineCount();

	if (lineIndex >= 0 && lineIndex < count)
	{
		QTextCursor cur = QTextCursor(doc);

		cur.movePosition(QTextCursor::MoveOperation::Start);

		int offset = 100;

		if (lineIndex + offset >= count)
			offset = count - lineIndex - 1;

		cur.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, lineIndex + offset);
		cur.select(QTextCursor::LineUnderCursor);

		cur.movePosition(QTextCursor::StartOfLine);
		ui->pte_Script->setTextCursor(cur);

		cur.movePosition(QTextCursor::Up, QTextCursor::MoveAnchor, offset);
		cur.select(QTextCursor::LineUnderCursor);

		cur.movePosition(QTextCursor::StartOfLine);
		ui->pte_Script->setTextCursor(cur);
	}
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::on_act_NewScript_triggered()
{
	OAFUN_DEBUG("c29_f10");
	g_TabScripts->SetScriptPath("NewScript");
	ClearText();
	g_TabScripts->UpdateScriptsList();
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::on_act_OpenScript_triggered()
{
	OAFUN_DEBUG("c29_f11");
	QString path = g_TabScripts->OpenScriptFileDialog();

	if (path.length())
	{
		g_TabScripts->LoadScript(path);
		g_OrionAssistant.ClientPrint("Script loaded!");
	}
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::on_act_SaveScript_triggered()
{
	OAFUN_DEBUG("c29_f12");
	if (g_TabScripts->SaveScript(false))
		g_OrionAssistant.ClientPrint("Script saved!");
	else
		g_OrionAssistant.ClientPrint("Failed to script saved!");
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::on_act_SaveAsScript_triggered()
{
	OAFUN_DEBUG("c29_f13");
	QString path = g_TabScripts->SaveScriptFileDialog();

	if (path.length())
	{
		if (g_TabScripts->SaveAsScript(path))
			g_OrionAssistant.ClientPrint("Script saved!");
		else
			g_OrionAssistant.ClientPrint("Failed to script saved!");
	}
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::on_act_FindText_triggered()
{
	OAFUN_DEBUG("c29_f14");
	if (m_FindDialog.isVisible())
		m_FindDialog.activateWindow();
	else
		m_FindDialog.show();
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::on_act_GoToLine_triggered()
{
	OAFUN_DEBUG("c29_f15");
	if (m_GoToLineDialog.isVisible())
		m_GoToLineDialog.activateWindow();
	else
		m_GoToLineDialog.show();
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::on_act_ReplaceText_triggered()
{
	OAFUN_DEBUG("c29_f16");
	if (m_ReplaceDialog.isVisible())
		m_ReplaceDialog.activateWindow();
	else
		m_ReplaceDialog.show();
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::on_act_CommentSelectedTextLines_triggered()
{
	OAFUN_DEBUG("c29_f17");
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::slot_SearchText(QString text, bool caseSensitive, bool searchDown)
{
	OAFUN_DEBUG("c29_f18");
	uint flags = 0;

	if (caseSensitive)
		flags |= QTextDocument::FindCaseSensitively;

	if (!searchDown)
		flags |= QTextDocument::FindBackward;

	QTextCursor cur = ui->pte_Script->document()->find(text, ui->pte_Script->textCursor(), (QTextDocument::FindFlag)flags);

	if (!cur.isNull())
	{
		cur.mergeCharFormat(cur.charFormat());
		ui->pte_Script->setTextCursor(cur);
	}
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::slot_GoToLine(int lineIndex)
{
	OAFUN_DEBUG("c29_f19");
	GoToLine(lineIndex - 1);
}
//----------------------------------------------------------------------------------
void ScriptEditorDialog::slot_ReplaceText(QString text, QString replace, bool caseSensitive, bool searchDown, bool all)
{
	OAFUN_DEBUG("c29_f20");
	uint flags = 0;

	if (caseSensitive)
		flags |= QTextDocument::FindCaseSensitively;

	if (!searchDown)
		flags |= QTextDocument::FindBackward;

	QTextDocument *doc = ui->pte_Script->document();
	QTextCursor cur = doc->find(text, ui->pte_Script->textCursor(), (QTextDocument::FindFlag)flags);

	while (!cur.isNull() && !cur.atEnd())
	{
		cur.mergeCharFormat(cur.charFormat());
		cur.removeSelectedText();

		if (replace.length())
			cur.insertText(replace);

		ui->pte_Script->setTextCursor(cur);

		if (!all)
			break;

		cur = doc->find(text, cur, (QTextDocument::FindFlag)flags);
	}
}
//----------------------------------------------------------------------------------
