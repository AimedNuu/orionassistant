/***********************************************************************************
**
** TabListsIgnore.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABLISTSIGNORE_H
#define TABLISTSIGNORE_H
//----------------------------------------------------------------------------------
#include <QWidget>
#include "../GUI/ignorelistitem.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabListsIgnore;
}
//----------------------------------------------------------------------------------
class TabListsIgnore : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_Ignore_clicked(const QModelIndex &index);

	void on_pb_IgnoreCreate_clicked();

	void on_pb_IgnoreSave_clicked();

	void on_pb_IgnoreRemove_clicked();

	void on_lw_IgnoreContent_clicked(const QModelIndex &index);

	void on_pb_IgnoreContentFromTarget_clicked();

	void on_pb_IgnoreContentCreate_clicked();

	void on_pb_IgnoreContentSave_clicked();

	void on_pb_IgnoreContentRemove_clicked();

private:
	Ui::TabListsIgnore *ui;

public slots:
	void SaveIgnoreList();

public:
	explicit TabListsIgnore(QWidget *parent = 0);
	~TabListsIgnore();

	void OnDeleteKeyClick(QWidget *widget);

	void LoadIgnoreList();

	void AddIgnoreListItemInList(class CGameObject *obj);

	CIgnoreListItem *GetIgnoreList(QString name);

	void AddIgnoreList(QString listName, const QString &serial, const QString &comment);

	void AddIgnoreList(QString listName, const QString &graphic, const QString &color, const QString &comment);

	void ClearIgnoreList(QString listName);

};
//----------------------------------------------------------------------------------
extern TabListsIgnore *g_TabListsIgnore;
//----------------------------------------------------------------------------------
#endif // TABLISTSIGNORE_H
//----------------------------------------------------------------------------------
