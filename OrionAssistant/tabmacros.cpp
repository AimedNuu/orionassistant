// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabMacros.cpp
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabmacros.h"
#include "ui_tabmacros.h"
#include "../GUI/macrolistitem.h"
#include "../GUI/macroactionlistitem.h"
#include "orionassistantform.h"
#include "../GUI/macroactionmenuaction.h"
#include "../Managers/skillmanager.h"
#include "../Managers/spellmanager.h"
#include "../Managers/commandmanager.h"
#include "orionassistant.h"
#include "hotkeysform.h"
#include "Target.h"
#include "../GameObjects/GameWorld.h"
#include "../GameObjects/GamePlayer.h"
#include "../Managers/FileManager.h"
#include "Packets.h"
#include "../Managers/contextmenumanager.h"
#include "../Managers/menumanager.h"
#include "../Managers/gumpmanager.h"
#include "../Managers/promptmanager.h"
#include "tabmain.h"
#include "tabagentsdress.h"
//----------------------------------------------------------------------------------
TabMacros *g_TabMacros = nullptr;
//----------------------------------------------------------------------------------
TabMacros::TabMacros(QWidget *parent)
: QWidget(parent), ui(new Ui::TabMacros)
{
	ui->setupUi(this);
	g_TabMacros = this;

	connect(ui->lw_MacroList, SIGNAL(itemChanged(QListWidgetItem*)), ui->lw_MacroList, SLOT(on_itemChanged(QListWidgetItem*)));
	connect(ui->lw_MacroList, SIGNAL(itemRenamed(QListWidgetItem*)), this, SLOT(on_itemRenamed(QListWidgetItem*)));
	connect(ui->lw_MacroList, SIGNAL(itemDeleted(QListWidget*)), this, SLOT(OnItemDeleted(QListWidget*)));
	connect(ui->lw_MacroActions, SIGNAL(itemDeleted(QListWidget*)), this, SLOT(OnItemDeleted(QListWidget*)));

	connect(ui->lw_MacroList, SIGNAL(itemDropped()), this, SLOT(OnListItemMoved()));
	connect(ui->lw_MacroActions, SIGNAL(itemDropped()), this, SLOT(OnActionListItemMoved()));

	connect(&m_Timer, SIGNAL(timeout()), this, SLOT(ProcessMacro()));
}
//----------------------------------------------------------------------------------
TabMacros::~TabMacros()
{
	delete ui;
	g_TabMacros = nullptr;
}
//----------------------------------------------------------------------------------
void TabMacros::UpdateMacroList()
{
	QStringList list;

	int count = ui->lw_MacroList->count();

	IFOR(i, 0, count)
	{
		CMacroListItem *item = (CMacroListItem*)ui->lw_MacroList->item(i);

		if (item != nullptr)
			list << item->text();
	}

	g_HotkeysForm->UpdateActionList(HT_PLAY_MACRO, list);
}
//----------------------------------------------------------------------------------
void TabMacros::OnListItemMoved()
{
	OAFUN_DEBUG("");
	QString path = g_OrionAssistantForm->GetSaveConfigPath();

	if (!path.length())
		return;

	Save(path + "/Macros.xml");
}
//----------------------------------------------------------------------------------
void TabMacros::OnActionListItemMoved()
{
	UpdateMacroByList();
	OnListItemMoved();
}
//----------------------------------------------------------------------------------
void TabMacros::OnItemDeleted(QListWidget *widget)
{
	if (widget == ui->lw_MacroActions)
		UpdateMacroByList();
	else
	{
		OnListItemMoved();
		UpdateMacroList();
	}
}
//----------------------------------------------------------------------------------
void TabMacros::on_itemRenamed(QListWidgetItem *item)
{
	Q_UNUSED(item);
	UpdateMacroList();
}
//----------------------------------------------------------------------------------
void TabMacros::Save(const QString &path)
{
	OAFUN_DEBUG("");
	QFile file(path);

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_MacroList->count();

		writter.writeStartElement("data");

		IFOR(i, 0, count)
		{
			CMacroListItem *item = (CMacroListItem*)ui->lw_MacroList->item(i);

			if (item == nullptr)
				continue;

			writter.writeStartElement("macro");

			writter.writeAttribute("name", item->text());

			for (CMacro *macro : item->m_Items)
			{
				writter.writeStartElement("action");

				macro->Save(writter);

				writter.writeEndElement(); //action
			}

			writter.writeEndElement(); //macro
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabMacros::Load(const QString &path)
{
	OAFUN_DEBUG("");
	ui->lw_MacroList->clear();
	ui->lw_MacroActions->clear();

	QFile file(path);

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		CMacroListItem *macro = nullptr;

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "macro" && attributes.hasAttribute("name"))
				{
					macro = new CMacroListItem(attributes.value("name").toString());
					ui->lw_MacroList->addItem(macro);
				}
				else if (reader.name() == "action" && macro != nullptr)
				{
					CMacro *action = CMacro::Load(attributes);

					if (action != nullptr)
						macro->m_Items.push_back(action);
				}
			}

			reader.readNext();
		}

		file.close();
		UpdateMacroList();
	}
}
//----------------------------------------------------------------------------------
void TabMacros::AddAction(CMacro *macro, const bool &insertion)
{
	CMacroActionListItem *newItem = new CMacroActionListItem(macro);

	if (insertion && ui->lw_MacroActions->currentRow() >= 0)
		ui->lw_MacroActions->insertItem(ui->lw_MacroActions->currentRow() + 1, newItem);
	else
		ui->lw_MacroActions->addItem(newItem);

	ui->lw_MacroActions->setCurrentItem(newItem);
	delete macro;
	OnListItemMoved();
}
//----------------------------------------------------------------------------------
void TabMacros::on_lw_MacroList_clicked(const QModelIndex &index)
{
	ui->lw_MacroActions->clear();

	CMacroListItem *item = (CMacroListItem*)ui->lw_MacroList->item(index.row());

	if (item != nullptr)
	{
		for (CMacro *macro : item->m_Items)
		{
			ui->lw_MacroActions->addItem(new CMacroActionListItem(macro));
		}
	}

	if (m_Playing && item == m_PlayingMacroItem)
		ui->pb_Play->setText("Stop");
	else
		ui->pb_Play->setText("Play");
}
//----------------------------------------------------------------------------------
void TabMacros::on_pb_Create_clicked()
{
	if (m_Recording)
		return;

	CMacroListItem *macro = new CMacroListItem(ui->lw_MacroList->GetFreeName(nullptr, "macro"));
	ui->lw_MacroList->addItem(macro);
	OnListItemMoved();
	ui->lw_MacroList->setCurrentItem(macro);
	on_lw_MacroList_clicked(ui->lw_MacroList->currentIndex());
	ui->lw_MacroList->editItem(macro);
	UpdateMacroList();
}
//----------------------------------------------------------------------------------
void TabMacros::on_pb_Remove_clicked()
{
	OAFUN_DEBUG("");
	if (m_Recording)
		return;

	ui->lw_MacroList->RemoveCurrentItem();
	on_lw_MacroList_clicked(ui->lw_MacroList->currentIndex());
}
//----------------------------------------------------------------------------------
void TabMacros::on_pb_Play_clicked()
{
	if (m_Recording)
		return;

	if (m_Timer.isActive())
		m_Timer.stop();

	CMacroListItem *macroItem = (CMacroListItem*)ui->lw_MacroList->currentItem();

	if ((m_Playing && macroItem == m_PlayingMacroItem) || macroItem == nullptr)
		Stop();
	else
		Play(macroItem, false);
}
//----------------------------------------------------------------------------------
void TabMacros::UpdateMacroByList()
{
	CMacroListItem *macroItem = (CMacroListItem*)ui->lw_MacroList->currentItem();

	if (macroItem == nullptr)
		return;

	if (macroItem != nullptr)
	{
		macroItem->Clear();

		IFOR(i, 0, ui->lw_MacroActions->count())
		{
			CMacroActionListItem *action = (CMacroActionListItem*)ui->lw_MacroActions->item(i);

			if (action != nullptr)
				macroItem->m_Items.push_back(action->GetMacro()->Copy());
		}
	}

	OnListItemMoved();
}
//----------------------------------------------------------------------------------
void TabMacros::on_pb_Record_clicked()
{
	if (m_Playing)
		return;

	if (m_Recording)
	{
		m_Recording = false;
		ui->pb_Record->setText("Record");

		UpdateMacroByList();
	}
	else
	{
		ui->lw_MacroActions->clear();
		m_Recording = true;
		ui->pb_Record->setText("Stop");
	}

	bool state = !m_Recording;
	ui->lw_MacroList->setEnabled(state);
	ui->pb_Create->setEnabled(state);
	ui->pb_Remove->setEnabled(state);
	ui->pb_Play->setEnabled(state);
	ui->pb_ToScript->setEnabled(state);
}
//----------------------------------------------------------------------------------
void TabMacros::Reset()
{
	m_Recording = false;
	m_Playing = false;
	m_Looped = false;
	m_LoopDelay = 1000;
	ui->pb_Record->setText("Record");
	ui->pb_Play->setText("Play");
	ui->lw_MacroList->setEnabled(true);
	ui->pb_Create->setEnabled(true);
	ui->pb_Remove->setEnabled(true);
	ui->pb_Play->setEnabled(true);
	ui->pb_ToScript->setEnabled(true);
}
//----------------------------------------------------------------------------------
void TabMacros::on_pb_ToScript_clicked()
{
}
//----------------------------------------------------------------------------------
void TabMacros::on_lw_MacroActions_customContextMenuRequested(const QPoint &pos)
{
	OAFUN_DEBUG("");
	Q_UNUSED(pos);

	CMacroActionListItem *actionItem = (CMacroActionListItem*)ui->lw_MacroActions->currentItem();
	CMacro *action = nullptr;

	if (actionItem != nullptr)
		action = actionItem->GetMacro();

	QMenu *menu = new QMenu(this);

	if (action != nullptr && action->HaveConvertTable())
	{
		QMenu *menuEdit = menu->addMenu("Convert action to...");

		MACRO_TYPE type = action->GetType();
		MACRO_CONVERT_TYPE convertType = action->GetConvertType();

		switch (type)
		{
			case MT_USE:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original object", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));

				if (((CMacroWithObject*)action)->GetGraphic())
				{
					menuEdit->addAction(new CMacroActionMenuAction("Type", type | MEF_EDIT, MCT_BY_TYPE, (convertType == MCT_BY_TYPE), this));
					menuEdit->addAction(new CMacroActionMenuAction("Type Color", type | MEF_EDIT, MCT_BY_TYPE_COLOR, (convertType == MCT_BY_TYPE_COLOR), this));
					menuEdit->addAction(new CMacroActionMenuAction("Type Ground", type | MEF_EDIT, MCT_BY_TYPE_GROUND, (convertType == MCT_BY_TYPE_GROUND), this));
					menuEdit->addAction(new CMacroActionMenuAction("Type Color Ground", type | MEF_EDIT, MCT_BY_TYPE_COLOR_GROUND, (convertType == MCT_BY_TYPE_COLOR_GROUND), this));
				}

				menuEdit->addAction(new CMacroActionMenuAction("Last object", type | MEF_EDIT, MCT_LAST, (convertType == MCT_LAST), this));
				menuEdit->addAction(new CMacroActionMenuAction("Self", type | MEF_EDIT, MCT_SELF, (convertType == MCT_SELF), this));

				break;
			}
			case MT_ATTACK:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original object", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Last attack", type | MEF_EDIT, MCT_LASTATTACK, (convertType == MCT_LASTATTACK), this));
				menuEdit->addAction(new CMacroActionMenuAction("Last target", type | MEF_EDIT, MCT_LASTTARGET, (convertType == MCT_LASTTARGET), this));
				menuEdit->addAction(new CMacroActionMenuAction("Enemy", type | MEF_EDIT, MCT_ENEMY, (convertType == MCT_ENEMY), this));

				break;
			}
			case MT_WAIT_FOR_TARGET_OBJECT:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original object", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));

				if (((CMacroWithObject*)action)->GetGraphic())
				{
					menuEdit->addAction(new CMacroActionMenuAction("Type", type | MEF_EDIT, MCT_BY_TYPE, (convertType == MCT_BY_TYPE), this));
					menuEdit->addAction(new CMacroActionMenuAction("Type Color", type | MEF_EDIT, MCT_BY_TYPE_COLOR, (convertType == MCT_BY_TYPE_COLOR), this));
					menuEdit->addAction(new CMacroActionMenuAction("Type Ground", type | MEF_EDIT, MCT_BY_TYPE_GROUND, (convertType == MCT_BY_TYPE_GROUND), this));
					menuEdit->addAction(new CMacroActionMenuAction("Type Color Ground", type | MEF_EDIT, MCT_BY_TYPE_COLOR_GROUND, (convertType == MCT_BY_TYPE_COLOR_GROUND), this));
				}

				menuEdit->addAction(new CMacroActionMenuAction("Last attack", type | MEF_EDIT, MCT_LASTATTACK, (convertType == MCT_LASTATTACK), this));
				menuEdit->addAction(new CMacroActionMenuAction("Last target", type | MEF_EDIT, MCT_LASTTARGET, (convertType == MCT_LASTTARGET), this));
				menuEdit->addAction(new CMacroActionMenuAction("Self", type | MEF_EDIT, MCT_SELF, (convertType == MCT_SELF), this));
				menuEdit->addAction(new CMacroActionMenuAction("Enemy", type | MEF_EDIT, MCT_ENEMY, (convertType == MCT_ENEMY), this));
				menuEdit->addAction(new CMacroActionMenuAction("Friend", type | MEF_EDIT, MCT_FRIEND, (convertType == MCT_FRIEND), this));

				menuEdit->addSeparator();

				goto lab_WaitSet;


				break;
			}
			case MT_WAIT_FOR_TARGET_TILE:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original tile", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Last tile", type | MEF_EDIT, MCT_LAST, (convertType == MCT_LAST), this));
				menuEdit->addAction(new CMacroActionMenuAction("Relative tile", type | MEF_EDIT, MCT_BY_TARGET_RELATIVE, (convertType == MCT_BY_TARGET_RELATIVE), this));

				menuEdit->addSeparator();

				goto lab_WaitSet;

				break;
			}
			case MT_USE_SKILL:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original skill", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Last skill", type | MEF_EDIT, MCT_LAST, (convertType == MCT_LAST), this));
				break;
			}
			case MT_CAST:
			case MT_CAST_NEW:
			case MT_CAST_FROM_BOOK:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original spell", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Last spell", type | MEF_EDIT, MCT_LAST, (convertType == MCT_LAST), this));
				break;
			}
			case MT_SAY:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Main chat", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Yell", type | MEF_EDIT, MCT_SAY_YELL, (convertType == MCT_SAY_YELL), this));
				menuEdit->addAction(new CMacroActionMenuAction("Whisper", type | MEF_EDIT, MCT_SAY_WHISPER, (convertType == MCT_SAY_WHISPER), this));
				menuEdit->addAction(new CMacroActionMenuAction("Emote", type | MEF_EDIT, MCT_SAY_EMOTE, (convertType == MCT_SAY_EMOTE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Broadcast", type | MEF_EDIT, MCT_SAY_BROADCAST, (convertType == MCT_SAY_BROADCAST), this));
				menuEdit->addAction(new CMacroActionMenuAction("Party chat", type | MEF_EDIT, MCT_SAY_PARTY, (convertType == MCT_SAY_PARTY), this));
				menuEdit->addAction(new CMacroActionMenuAction("Guild chat", type | MEF_EDIT, MCT_SAY_GUILD, (convertType == MCT_SAY_GUILD), this));
				menuEdit->addAction(new CMacroActionMenuAction("Alliance chat", type | MEF_EDIT, MCT_SAY_ALLIANCE, (convertType == MCT_SAY_ALLIANCE), this));
				break;
			}
			case MT_PICK_UP_ITEM:
			{
				if (((CMacroWithObject*)action)->GetGraphic())
				{
					menuEdit->addAction(new CMacroActionMenuAction("Original object", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));

					menuEdit->addAction(new CMacroActionMenuAction("Type", type | MEF_EDIT, MCT_BY_TYPE, (convertType == MCT_BY_TYPE), this));
					menuEdit->addAction(new CMacroActionMenuAction("Type Color", type | MEF_EDIT, MCT_BY_TYPE_COLOR, (convertType == MCT_BY_TYPE_COLOR), this));
				}

				break;
			}
			case MT_DROP_ITEM:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original object", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("In last container", type | MEF_EDIT, MCT_LASTCONTAINER, (convertType == MCT_LASTCONTAINER), this));
				menuEdit->addAction(new CMacroActionMenuAction("In backpack", type | MEF_EDIT, MCT_BACKPACK, (convertType == MCT_BACKPACK), this));
				break;
			}
			case MT_WAIT_FOR_MENU:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original menu", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Any menu", type | MEF_EDIT, MCT_ANY, (convertType == MCT_ANY), this));

				menuEdit->addSeparator();

				goto lab_WaitSet;

				break;
			}
			case MT_WAIT_FOR_GUMP:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original gump", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Any gump", type | MEF_EDIT, MCT_ANY, (convertType == MCT_ANY), this));

				menuEdit->addSeparator();

				goto lab_WaitSet;

				break;
			}
			case MT_REQUEST_CONTEXT_MENU:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original object", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Self", type | MEF_EDIT, MCT_SELF, (convertType == MCT_SELF), this));
				menuEdit->addAction(new CMacroActionMenuAction("Last attack", type | MEF_EDIT, MCT_LASTATTACK, (convertType == MCT_LASTATTACK), this));
				menuEdit->addAction(new CMacroActionMenuAction("Last target", type | MEF_EDIT, MCT_LASTTARGET, (convertType == MCT_LASTTARGET), this));
				menuEdit->addAction(new CMacroActionMenuAction("Enemy", type | MEF_EDIT, MCT_ENEMY, (convertType == MCT_ENEMY), this));
				menuEdit->addAction(new CMacroActionMenuAction("Friend", type | MEF_EDIT, MCT_FRIEND, (convertType == MCT_FRIEND), this));
				break;
			}
			case MT_WAIT_FOR_CONTEXT_MENU:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original context menu", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Any context menu", type | MEF_EDIT, MCT_ANY, (convertType == MCT_ANY), this));

				menuEdit->addSeparator();

				goto lab_WaitSet;

				break;
			}
			case MT_SELECT_TRADE:
			{
				//result.push_back(MCT_ANY);
				break;
			}
			case MT_WAR_MODE:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Original state", type | MEF_EDIT, MCT_NONE, (convertType == MCT_NONE), this));
				menuEdit->addAction(new CMacroActionMenuAction("Toggle", type | MEF_EDIT, MCT_TOGGLE, (convertType == MCT_TOGGLE), this));
				break;
			}
			case MT_WALK:
			{
				int val = ((CMacroWithIntValue*)action)->GetValue();
				menuEdit->addAction(new CMacroActionMenuAction("Turn", type | MEF_EDIT, MCT_TURN, (val < 0), this));
				menuEdit->addAction(new CMacroActionMenuAction("Walk", type | MEF_EDIT, MCT_WALK, ((val >= 0) && !(val & 0x80)), this));
				menuEdit->addAction(new CMacroActionMenuAction("Run", type | MEF_EDIT, MCT_RUN, ((val >= 0) && (val & 0x80)), this));

				break;
			}
			case MT_COMBAT_ABILITY:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Primary", type | MEF_EDIT, MCT_PRIMARY, (convertType == MCT_PRIMARY), this));
				menuEdit->addAction(new CMacroActionMenuAction("Secondary", type | MEF_EDIT, MCT_SECONDARY, (convertType == MCT_SECONDARY), this));
				break;
			}
			case MT_WAIT:
			case MT_DISARM:
			case MT_ARM:
			case MT_UNDRESS:
			case MT_DRESS:
			lab_WaitSet:
			{
				menuEdit->addAction(new CMacroActionMenuAction("Set delay 100 ms", type | MEF_WAIT_SET | MEF_EDIT, 100, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Set delay 200 ms", type | MEF_WAIT_SET | MEF_EDIT, 200, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Set delay 300 ms", type | MEF_WAIT_SET | MEF_EDIT, 300, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Set delay 400 ms", type | MEF_WAIT_SET | MEF_EDIT, 400, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Set delay 500 ms", type | MEF_WAIT_SET | MEF_EDIT, 500, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Set delay 600 ms", type | MEF_WAIT_SET | MEF_EDIT, 600, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Set delay 700 ms", type | MEF_WAIT_SET | MEF_EDIT, 700, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Set delay 800 ms", type | MEF_WAIT_SET | MEF_EDIT, 800, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Set delay 900 ms", type | MEF_WAIT_SET | MEF_EDIT, 900, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Set delay 1000 ms", type | MEF_WAIT_SET | MEF_EDIT, 1000, false, this));
				menuEdit->addSeparator();
				menuEdit->addAction(new CMacroActionMenuAction("Increase delay by 100 ms", type | MEF_WAIT_ADD | MEF_EDIT, 100, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Increase delay by 200 ms", type | MEF_WAIT_ADD | MEF_EDIT, 200, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Increase delay by 500 ms", type | MEF_WAIT_ADD | MEF_EDIT, 500, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Increase delay by 1000 ms", type | MEF_WAIT_ADD | MEF_EDIT, 1000, false, this));
				menuEdit->addSeparator();
				menuEdit->addAction(new CMacroActionMenuAction("Decrease delay by 100 ms", type | MEF_WAIT_ADD | MEF_EDIT, -100, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Decrease delay by 200 ms", type | MEF_WAIT_ADD | MEF_EDIT, -200, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Decrease delay by 500 ms", type | MEF_WAIT_ADD | MEF_EDIT, -500, false, this));
				menuEdit->addAction(new CMacroActionMenuAction("Decrease delay by 1000 ms", type | MEF_WAIT_ADD | MEF_EDIT, -1000, false, this));
			}
			default:
				break;
		}
	}

	menu->addSeparator();

	QMenu *menuWait = menu->addMenu("Wait");

	menuWait->addAction(new CMacroActionMenuAction("Wait 100", MT_WAIT, 100, false, this));
	menuWait->addAction(new CMacroActionMenuAction("Wait 200", MT_WAIT, 200, false, this));
	menuWait->addAction(new CMacroActionMenuAction("Wait 300", MT_WAIT, 300, false, this));
	menuWait->addAction(new CMacroActionMenuAction("Wait 400", MT_WAIT, 400, false, this));
	menuWait->addAction(new CMacroActionMenuAction("Wait 500", MT_WAIT, 500, false, this));
	menuWait->addAction(new CMacroActionMenuAction("Wait 600", MT_WAIT, 600, false, this));
	menuWait->addAction(new CMacroActionMenuAction("Wait 700", MT_WAIT, 700, false, this));
	menuWait->addAction(new CMacroActionMenuAction("Wait 800", MT_WAIT, 800, false, this));
	menuWait->addAction(new CMacroActionMenuAction("Wait 900", MT_WAIT, 900, false, this));
	menuWait->addAction(new CMacroActionMenuAction("Wait 1000", MT_WAIT, 1000, false, this));

	QMenu *menuEquipment = menu->addMenu("Equipment");

	menuEquipment->addAction(new CMacroActionMenuAction("Set arm", MT_SET_ARM, g_TabMain->MoveItemsDelay(), false, this));
	menuEquipment->addAction(new CMacroActionMenuAction("Disarm", MT_DISARM, g_TabMain->MoveItemsDelay(), false, this));
	menuEquipment->addAction(new CMacroActionMenuAction("Arm", MT_ARM, g_TabMain->MoveItemsDelay(), false, this));
	menuEquipment->addSeparator();
	menuEquipment->addAction(new CMacroActionMenuAction("Set dress", MT_SET_DRESS, g_TabMain->MoveItemsDelay(), false, this));
	menuEquipment->addAction(new CMacroActionMenuAction("Undress", MT_UNDRESS, g_TabMain->MoveItemsDelay(), false, this));
	menuEquipment->addAction(new CMacroActionMenuAction("Dress", MT_DRESS, g_TabMain->MoveItemsDelay(), false, this));

	menu->popup(QCursor::pos());
}
//----------------------------------------------------------------------------------
void TabMacros::AddActionFromMenu(int command, int action)
{
	if (command & MEF_EDIT)
	{
		CMacroActionListItem *actionItem = (CMacroActionListItem*)ui->lw_MacroActions->currentItem();

		if (actionItem != nullptr)
		{
			CMacro *macro = actionItem->GetMacro();

			if (macro != nullptr)
			{
				if ((command & (MEF_WAIT_SET | MEF_WAIT_ADD)))
				{
					if (command & MEF_WAIT_SET)
						macro->SetWaitingTime(action);
					else
					{
						int time = macro->GetWaitingTime() + action;

						if (time > 0)
							macro->SetWaitingTime(time);
					}
				}
				else if ((command & (~MEF_EDIT)) == MT_WALK)
				{
					int val = ((CMacroWithIntValue*)macro)->GetValue();

					if (action == MCT_TURN)
					{
						if (val >= 0)
							val = -(val | 0x80);
					}
					else
					{
						if (val < 0)
							val = -val;

						if (action == MCT_WALK)
							val &= ~0x80;
						else if (action == MCT_RUN)
							val |= 0x80;
					}

					((CMacroWithIntValue*)macro)->SetValue(val);
				}
				else
					macro->SetConvertType((MACRO_CONVERT_TYPE)action);

				actionItem->setText(macro->GetText());
				UpdateMacroByList();
			}
		}

		return;
	}

	switch (command)
	{
		case -1:
		{
			break;
		}
		case MT_WAIT:
		case MT_SET_ARM:
		case MT_DISARM:
		case MT_ARM:
		case MT_SET_DRESS:
		case MT_UNDRESS:
		case MT_DRESS:
		{
			CMacro *macro = new CMacro((MACRO_TYPE)command);
			macro->SetWaitingTime(action);
			AddAction(macro, true);
			UpdateMacroByList();
			break;
		}
		default:
			break;
	}
}
//----------------------------------------------------------------------------------
void TabMacros::Play(CMacroListItem *macroItem, const bool &doNotAutoIterrupt)
{
	m_DoNotAutoIterrupt = doNotAutoIterrupt;
	m_PlayingMacroItem = macroItem;
	m_NextMacroTick = 0;
	m_ArmList.clear();
	m_DressList.clear();
	m_WaitingEnabled = false;
	ui->lw_MacroList->setCurrentItem(m_PlayingMacroItem);
	on_lw_MacroList_clicked(ui->lw_MacroList->currentIndex());

	for (CMacro *macro : m_PlayList)
		delete macro;

	m_PlayList.clear();
	m_PlayPosition = 0;

	for (CMacro *macro : m_PlayingMacroItem->m_Items)
		m_PlayList.push_back(macro->Copy());

	if (m_PlayList.empty())
	{
		m_Playing = false;
		ui->pb_Play->setText("Play");
		return;
	}

	m_Playing = true;

	if (ui->cb_Looped->isChecked() && EnabledFeatureLoopedMacro())
	{
		m_Looped = true;
		m_LoopDelay = ui->dsb_LoopTime->value() * 1000;
	}
	else
		m_Looped = false;

	ui->pb_Play->setText("Stop");

	ProcessMacro();

	if (m_Playing)
		m_Timer.start(10);
	else
		m_Timer.stop();
}
//----------------------------------------------------------------------------------
void TabMacros::Stop()
{
	m_Playing = false;
	m_PlayingMacroItem = nullptr;
	m_WaitingEnabled = false;
	m_NextMacroTick = 0;
	ui->pb_Play->setText("Play");
	m_ArmList.clear();
	m_DressList.clear();

	if (m_Timer.isActive())
		m_Timer.stop();
}
//----------------------------------------------------------------------------------
void TabMacros::Play(const QString &name, const bool &doNotAutoIterrupt)
{
	if (m_Playing && m_DoNotAutoIterrupt)
		return;

	DFOR(i, ui->lw_MacroList->count() - 1, 0)
	{
		CMacroListItem *macroItem = (CMacroListItem*)ui->lw_MacroList->item(i);

		if (macroItem != nullptr && macroItem->text() == name)
		{
			Play(macroItem, doNotAutoIterrupt);
			break;
		}
	}
}
//----------------------------------------------------------------------------------
bool TabMacros::PlayWaiting(const QList<MACRO_TYPE> &list)
{
	if (m_Playing && m_PlayPosition >= 1 && m_PlayPosition <= m_PlayList.size())
	{
		CMacro *macro = m_PlayList.at(m_PlayPosition - 1);

		if (macro != nullptr)
		{
			MACRO_TYPE type = macro->GetType();

			for (const MACRO_TYPE &i : list)
			{
				if (type == i)
				{
					m_PlayPosition--;
					m_NextMacroTick = 0;
					ProcessMacro();

					return true;
				}
			}
		}
	}

	return false;
}
//----------------------------------------------------------------------------------
uint TabMacros::FindObject(CMacroWithObject *macro, const MACRO_CONVERT_TYPE &convertType)
{
	ushort graphic = macro->GetGraphic();

	if (!graphic)
		return macro->GetSerial();

	ushort color = ((convertType == MCT_BY_TYPE_COLOR || convertType == MCT_BY_TYPE_COLOR_GROUND) ? macro->GetColor() : 0xFFFF);

	QStringList list;

	if (convertType == MCT_BY_TYPE_GROUND || convertType == MCT_BY_TYPE_COLOR_GROUND)
	{
		int distance = g_TabMain->SearchItemsDistance();

		if (macro->GetType() == MT_USE)
			distance = g_TabMain->UseItemsDistance();

		g_OrionAssistant.FindType(CFindItem(COrionAssistant::GraphicToText(graphic), COrionAssistant::GraphicToText(color)), QList<CIgnoreItem>(), 0xFFFFFFFF, distance, 0, FTF_NEAREST, false, list);
	}
	else if (g_Player != nullptr)
		g_Player->FindType(CFindItem(COrionAssistant::GraphicToText(graphic), COrionAssistant::GraphicToText(color)), QList<CIgnoreItem>(), FTF_FAST, true, list);

	if (!list.empty())
		return COrionAssistant::TextToSerial(list.at(0));

	return 0;
}
//----------------------------------------------------------------------------------
void TabMacros::ProcessMacro()
{
	const int dressLayersCount = 18;
	const int dressLayers[dressLayersCount] =
	{
		OL_TORSO, OL_HELMET, OL_GLOVES, OL_LEGS, OL_WAIST,
		OL_ARMS, OL_TUNIC, OL_CLOAK, OL_ROBE, OL_SKIRT,
		OL_SHIRT, OL_SHOES, OL_PANTS, OL_BRACELET, OL_EARRINGS,
		OL_RING, OL_NECKLACE, OL_TALISMAN
	};

	while (m_Playing && m_NextMacroTick < GetTickCount())
	{
		int playListSize = m_PlayList.size();

		if (m_PlayPosition >= playListSize)
		{
			m_PlayPosition = 0;
			m_NextMacroTick = GetTickCount() + m_LoopDelay;

			if (!m_Looped)
				Stop();

			break;
		}

		CMacro *macro = m_PlayList.at(m_PlayPosition);

		m_PlayPosition++;

		if (macro == nullptr || g_Player == nullptr)
			return;

		MACRO_TYPE type = macro->GetType();
		MACRO_CONVERT_TYPE convertType = macro->GetConvertType();

		switch (type)
		{
			case MT_USE:
			{
				uint serial = ((CMacroWithObject*)macro)->GetSerial();

				if (convertType == MCT_BY_TYPE || convertType == MCT_BY_TYPE_COLOR || convertType == MCT_BY_TYPE_GROUND || convertType == MCT_BY_TYPE_COLOR_GROUND)
					serial = FindObject((CMacroWithObject*)macro, convertType);
				else if (convertType == MCT_LAST)
					serial = COrionAssistant::TextToSerial("lastobject");
				else if (convertType == MCT_SELF)
					serial = g_PlayerSerial;

				if (serial)
					g_OrionAssistant.DoubleClick(serial);

				break;
			}
			case MT_ATTACK:
			{
				uint serial = ((CMacroWithObject*)macro)->GetSerial();

				if (convertType == MCT_LAST || convertType == MCT_LASTATTACK)
					serial = COrionAssistant::TextToSerial("lastattack");
				else if (convertType == MCT_LASTTARGET)
					serial = COrionAssistant::TextToSerial("lasttarget");
				//else if (convertType == MCT_FRIEND)
				//	serial = COrionAssistant::TextToSerial("friend");
				else if (convertType == MCT_ENEMY)
					serial = COrionAssistant::TextToSerial("enemy");

				if (serial)
					g_OrionAssistant.Attack(serial);

				break;
			}
			case MT_WAIT_FOR_TARGET_OBJECT:
			{
				if (!g_Target.IsTargeting())
				{
					m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
					return;
				}

				uint serial = ((CMacroWithObject*)macro)->GetSerial();

				if (convertType == MCT_BY_TYPE || convertType == MCT_BY_TYPE_COLOR || convertType == MCT_BY_TYPE_GROUND || convertType == MCT_BY_TYPE_COLOR_GROUND)
					serial = FindObject((CMacroWithObject*)macro, convertType);
				else if (convertType == MCT_LASTATTACK)
					serial = COrionAssistant::TextToSerial("lastattack");
				else if (convertType == MCT_LASTTARGET)
					serial = COrionAssistant::TextToSerial("lasttarget");
				else if (convertType == MCT_SELF)
					serial = g_PlayerSerial;
				else if (convertType == MCT_ENEMY)
					serial = COrionAssistant::TextToSerial("enemy");
				else if (convertType == MCT_FRIEND)
					serial = COrionAssistant::TextToSerial("friend");

				if (serial)
				{
					g_Target.SendTargetObject(serial);
					CPacketCancelTarget().SendClient();
				}
				else
					g_Target.SendCancelTarget();

				break;
			}
			case MT_WAIT_FOR_TARGET_TILE:
			{
				if (!g_Target.IsTargeting())
				{
					m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
					return;
				}

				if (convertType == MCT_LAST)
				{
					g_Target.SendTargetLastTile();
					CPacketCancelTarget().SendClient();
				}
				else
				{
					CMacroWithTargetTile *withTarget = (CMacroWithTargetTile*)macro;

					ushort graphic = withTarget->GetGraphic();
					int x = withTarget->GetX();
					int y = withTarget->GetY();
					int z = withTarget->GetZ();

					if (convertType == MCT_BY_TARGET_RELATIVE)
					{
						x = g_Player->GetX() + withTarget->GetRelativeX();
						y = g_Player->GetY() + withTarget->GetRelativeY();
						z = g_Player->GetZ() + withTarget->GetRelativeZ();

						QList<CTileInfo> tileList = g_FileManager.GetTile(x, y, z, "any");

						if (tileList.size())
							graphic = tileList[0].GetGraphic();
					}

					g_Target.SendTargetTile(graphic, x, y, z);
					CPacketCancelTarget().SendClient();
				}

				break;
			}
			case MT_USE_SKILL:
			{
				if (convertType == MCT_LAST)
					g_SkillManager.Use("last");
				else
					g_SkillManager.Use(QString::number(((CMacroWithIntValue*)macro)->GetValue()));

				break;
			}
			case MT_CAST:
			case MT_CAST_NEW:
			case MT_CAST_FROM_BOOK:
			{
				if (convertType == MCT_LAST)
					g_SpellManager.Cast("last");
				else
					g_SpellManager.Cast(QString::number(((CMacroWithIntValue*)macro)->GetValue()));

				break;
			}
			case MT_SAY:
			{
				QString text = ((CMacroWithStringValue*)macro)->GetValue();

				if (convertType == MCT_SAY_YELL)
					text = "! " + text;
				else if (convertType == MCT_SAY_WHISPER)
					text = "; " + text;
				else if (convertType == MCT_SAY_EMOTE)
					text = ": " + text;
				else if (convertType == MCT_SAY_BROADCAST)
					text = "? " + text;
				else if (convertType == MCT_SAY_PARTY)
					text = "/ " + text;
				else if (convertType == MCT_SAY_GUILD)
					text = "\\ " + text;
				else if (convertType == MCT_SAY_ALLIANCE)
					text = "| " + text;

				g_CommandManager.CommandSay(text);

				break;
			}
			case MT_WAIT_FOR_PROMPT:
			{
				if (!EnabledCommandPrompts())
					break;

				if (!m_WaitingEnabled)
				{
					g_PromptReceived = false;
					m_WaitingEnabled = true;
					m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
					return;
				}

				m_WaitingEnabled = false;

				if (g_PromptReceived)
					g_PromptManager.Send(((CMacroWithStringValue*)macro)->GetValue());

				break;
			}
			case MT_WAIT_FOR_MENU:
			{
				if (!m_WaitingEnabled)
				{
					g_MenuReceived = false;
					m_WaitingEnabled = true;
					m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
					return;
				}

				m_WaitingEnabled = false;

				if (!g_MenuReceived)
					break;

				CMenu *menu = g_MenuManager.GetLastMenu();

				if (menu->GetReplayed())
					break;

				if (convertType == MCT_ANY || (((CMacroWithMenu*)macro)->GetName() == menu->GetName()))
				{
					if (g_MenuManager.SendMenuChoice(menu, ((CMacroWithMenu*)macro)->GetItemName(), true))
						break;
				}

				break;
			}
			case MT_WAIT_FOR_GUMP:
			{
				if (!m_WaitingEnabled)
				{
					g_GumpReceived = false;
					m_WaitingEnabled = true;
					m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
					return;
				}

				m_WaitingEnabled = false;

				if (!g_GumpReceived)
					break;

				CGump *gump = g_GumpManager.GetLastGump();

				if (gump->GetReplayed())
					break;

				if (convertType == MCT_ANY || (((CMacroWithGump*)macro)->GetID() == gump->GetID()))
				{
					CGumpHook hook(nullptr);
					hook.SetIndex(((CMacroWithGump*)macro)->GetButton());

					if (g_GumpManager.SendGumpChoice(gump, hook))
						break;
				}

				break;
			}
			case MT_REQUEST_CONTEXT_MENU:
			{
				if (!EnabledCommandContextMenu())
					break;

				uint serial = ((CMacroWithObject*)macro)->GetSerial();

				if (convertType == MCT_SELF)
					serial = g_PlayerSerial;
				else if (convertType == MCT_LASTATTACK)
					serial = COrionAssistant::TextToSerial("lastattack");
				else if (convertType == MCT_LASTTARGET)
					serial = COrionAssistant::TextToSerial("lasttarget");
				else if (convertType == MCT_ENEMY)
					serial = COrionAssistant::TextToSerial("enemy");
				else if (convertType == MCT_FRIEND)
					serial = COrionAssistant::TextToSerial("friend");

				if (serial != 0)
					CPacketContextMenuRequest(serial).SendServer();

				break;
			}
			case MT_WAIT_FOR_CONTEXT_MENU:
			{
				if (!EnabledCommandContextMenu())
					break;

				if (!m_WaitingEnabled)
				{
					g_ContextMenuReceived = false;
					m_WaitingEnabled = true;
					m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
					return;
				}

				m_WaitingEnabled = false;

				if (!g_ContextMenuReceived)
					break;

				if (convertType == MCT_ANY || (((CMacroWithObject*)macro)->GetSerial() == g_ContextMenuManager.GetSerial()))
				{
					CPacketContextMenuSelection(g_ContextMenuManager.GetSerial(), ((CMacroWithObject*)macro)->GetGraphic()).SendServer();
					break;
				}

				break;
			}
			case MT_WAR_MODE:
			{
				if (convertType == MCT_TOGGLE)
					g_CommandManager.CommandWarmode(2);
				else
					g_CommandManager.CommandWarmode(((CMacroWithIntValue*)macro)->GetValue());

				break;
			}
			case MT_HELP_MENU_REQUEST:
			{
				CPacketHelpRequest().SendServer();
				break;
			}
			case MT_QUEST_MENU_REQUEST:
			{
				CPacketQuestMenuRequest().SendServer();
				break;
			}
			case MT_GUILD_MENU_REQUEST:
			{
				CPacketGuildMenuRequest().SendServer();
				break;
			}
			case MT_WALK:
			{
				if (!EnabledCommandMoving())
					break;

				int dir = ((CMacroWithIntValue*)macro)->GetValue();
				int walkDelay = 0;

				if (dir < 0) //Turn
				{
					dir = (-dir) & 7;

					if ((g_Player->GetDirection() & 7) != dir)
					{
						bool temp = true;
						g_CommandManager.CommandStep(dir, false, &temp);
						walkDelay = (g_TabMain->FastRotation() ? 50 : 100);
					}
				}
				else
				{
					bool temp = true;
					g_CommandManager.CommandStep(dir & 7, (dir & 0x80), &temp);

					if (g_Player->FindLayer(OL_MOUNT) != nullptr)
						walkDelay = ((dir & 0x80) ? 100 : 200);
					else
						walkDelay = ((dir & 0x80) ? 200 : 400);
				}

				if (walkDelay)
				{
					m_NextMacroTick = GetTickCount() + walkDelay + 15;
					return;
				}

				break;
			}
			case MT_WRESTLING_STUN:
			{
				CPacketWrestlingStun().SendServer();
				break;
			}
			case MT_WRESTLING_DISARM:
			{
				CPacketWrestlingDisarm().SendServer();
				break;
			}
			case MT_COMBAT_ABILITY:
			{
				CPacketUseAbilityRequest(convertType == MCT_SECONDARY).SendClient();

				break;
			}
			case MT_TOGGLE_GARGOYLE_FLYING:
			{
				CPacketToggleGargoyleFlying().SendServer();

				break;
			}
			case MT_SET_ARM:
			{
				m_ArmList.clear();

				if (g_Player == nullptr)
					break;

				IFOR(i, 1, 3)
				{
					CGameItem *obj = g_Player->FindLayer(i);

					if (obj != nullptr)
						m_ArmList.push_back(obj->GetSerial());
				}

				break;
			}
			case MT_DISARM:
			{
				if (g_Player == nullptr)
					break;

				IFOR(i, 1, 3)
				{
					CGameItem *obj = g_Player->FindLayer(i);

					if (obj != nullptr)
					{
						g_MoveItemData.Reset();
						g_MoveItemData.Container = COrionAssistant::TextToSerial("dressbag");

						if (!g_MoveItemData.Container)
							g_MoveItemData.Container = COrionAssistant::TextToSerial("backpack");

						g_OrionAssistant.MoveItem(obj);

						m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
						m_PlayPosition--;
						return;
					}
				}

				break;
			}
			case MT_ARM:
			{
				if (g_Player == nullptr || m_ArmList.empty())
					break;

				while (!m_ArmList.empty())
				{
					uint serial = m_ArmList.front();

					CGameItem *obj = g_World->FindWorldItem(serial);

					if (obj == nullptr || obj->GetLayer() == obj->GetEquipLayer())
					{
						m_ArmList.pop_front();
						continue;
					}

					CGameItem *equipped = g_Player->FindLayer(obj->GetEquipLayer());

					if (equipped != nullptr && g_TabAgentsDress->UndressBeforeDressing())
					{
						g_MoveItemData.Reset();
						g_MoveItemData.Container = COrionAssistant::TextToSerial("dressbag");

						if (!g_MoveItemData.Container)
							g_MoveItemData.Container = COrionAssistant::TextToSerial("backpack");

						g_OrionAssistant.MoveItem(equipped);
					}
					else if (g_TabAgentsDress->DoubleClickForEquipItems())
						g_OrionAssistant.DoubleClick(obj->GetSerial());
					else
						g_OrionAssistant.MoveEquip(obj->GetSerial(), obj->GetEquipLayer(), g_PlayerSerial);

					m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
					m_PlayPosition--;
					return;
				}

				break;
			}
			case MT_SET_DRESS:
			{
				m_DressList.clear();

				if (g_Player == nullptr)
					break;

				IFOR(i, 0, dressLayersCount)
				{
					CGameItem *obj = g_Player->FindLayer(dressLayers[i]);

					if (obj != nullptr)
						m_DressList.push_back(obj->GetSerial());
				}

				break;
			}
			case MT_UNDRESS:
			{
				if (g_Player == nullptr)
					break;

				IFOR(i, 0, dressLayersCount)
				{
					CGameItem *obj = g_Player->FindLayer(dressLayers[i]);

					if (obj != nullptr)
					{
						g_MoveItemData.Reset();
						g_MoveItemData.Container = COrionAssistant::TextToSerial("dressbag");

						if (!g_MoveItemData.Container)
							g_MoveItemData.Container = COrionAssistant::TextToSerial("backpack");

						g_OrionAssistant.MoveItem(obj);

						m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
						m_PlayPosition--;
						return;
					}
				}

				break;
			}
			case MT_DRESS:
			{
				if (g_Player == nullptr || m_DressList.empty())
					break;

				while (!m_DressList.empty())
				{
					uint serial = m_DressList.front();

					CGameItem *obj = g_World->FindWorldItem(serial);

					if (obj == nullptr || obj->GetLayer() == obj->GetEquipLayer())
					{
						m_DressList.pop_front();
						continue;
					}

					CGameItem *equipped = g_Player->FindLayer(obj->GetEquipLayer());

					if (equipped != nullptr && g_TabAgentsDress->UndressBeforeDressing())
					{
						g_MoveItemData.Reset();
						g_MoveItemData.Container = COrionAssistant::TextToSerial("dressbag");

						if (!g_MoveItemData.Container)
							g_MoveItemData.Container = COrionAssistant::TextToSerial("backpack");

						g_OrionAssistant.MoveItem(equipped);
					}
					else if (g_TabAgentsDress->DoubleClickForEquipItems())
						g_OrionAssistant.DoubleClick(obj->GetSerial());
					else
						g_OrionAssistant.MoveEquip(obj->GetSerial(), obj->GetEquipLayer(), g_PlayerSerial);

					m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
					m_PlayPosition--;
					return;
				}

				break;
			}
			case MT_WAIT:
			{
				m_NextMacroTick = GetTickCount() + macro->GetWaitingTime();
				return;
			}
			default:
				break;
		}

		m_WaitingEnabled = false;
	}
}
//----------------------------------------------------------------------------------
