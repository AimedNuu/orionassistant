// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ShipControlForm.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "shipcontrolform.h"
#include "ui_shipcontrolform.h"
#include "Packets.h"
//----------------------------------------------------------------------------------
ShipControlForm *g_ShipControlForm = nullptr;
//----------------------------------------------------------------------------------
ShipControlForm::ShipControlForm(QWidget *parent)
: QDialog(parent), ui(new Ui::ShipControlForm)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);

	setFixedSize(size());

	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	g_ShipControlForm = this;
}
//----------------------------------------------------------------------------------
ShipControlForm::~ShipControlForm()
{
	OAFUN_DEBUG("");
	delete ui;
	g_ShipControlForm = nullptr;
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_ForwardLeft_clicked()
{
	OAFUN_DEBUG("");
	if (ui->cb_MoveState->currentIndex())
		Send(ui->cb_MoveState->currentText() + " Forward Left");
	else
		Send("Forward Left");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_Forward_clicked()
{
	OAFUN_DEBUG("");
	if (ui->cb_MoveState->currentIndex())
		Send(ui->cb_MoveState->currentText() + " Forward");
	else
		Send("Forward");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_ForwardRight_clicked()
{
	OAFUN_DEBUG("");
	if (ui->cb_MoveState->currentIndex())
		Send(ui->cb_MoveState->currentText() + " Forward Right");
	else
		Send("Forward Right");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_Right_clicked()
{
	OAFUN_DEBUG("");
	if (ui->cb_MoveState->currentIndex())
		Send(ui->cb_MoveState->currentText() + " Right");
	else
		Send("Right");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_BackwardRight_clicked()
{
	OAFUN_DEBUG("");
	if (ui->cb_MoveState->currentIndex())
		Send(ui->cb_MoveState->currentText() + " Backward Right");
	else
		Send("Backward Right");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_Backward_clicked()
{
	OAFUN_DEBUG("");
	if (ui->cb_MoveState->currentIndex())
		Send(ui->cb_MoveState->currentText() + " Backward");
	else
		Send("Backward");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_BackwardLeft_clicked()
{
	OAFUN_DEBUG("");
	if (ui->cb_MoveState->currentIndex())
		Send(ui->cb_MoveState->currentText() + " Backward Left");
	else
		Send("Backward Left");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_Left_clicked()
{
	OAFUN_DEBUG("");
	if (ui->cb_MoveState->currentIndex())
		Send(ui->cb_MoveState->currentText() + " Left");
	else
		Send("Left");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_Stop_clicked()
{
	OAFUN_DEBUG("");
	Send("Stop");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_TurnLeft_clicked()
{
	OAFUN_DEBUG("");
	Send("Turn Left");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_TurnAround_clicked()
{
	OAFUN_DEBUG("");
	Send("Turn Around");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_TurnRight_clicked()
{
	OAFUN_DEBUG("");
	Send("Turn Right");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_RaiseAnchor_clicked()
{
	OAFUN_DEBUG("");
	Send("Raise Anchor");
}
//----------------------------------------------------------------------------------
void ShipControlForm::on_pb_DropAnchor_clicked()
{
	OAFUN_DEBUG("");
	Send("Drop Anchor");
}
//----------------------------------------------------------------------------------
void ShipControlForm::Send(const QString &text)
{
	OAFUN_DEBUG("");
	CPacketUnicodeSpeechRequest(0, text.toStdWString()).SendClient();
}
//----------------------------------------------------------------------------------
