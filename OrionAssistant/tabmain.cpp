// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabMain.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabmain.h"
#include "ui_tabmain.h"
#include "orionassistantform.h"
#include "textdialog.h"
#include "scripteditordialog.h"
#include "orionassistant.h"
#include "Packets.h"
#include "../GameObjects/GamePlayer.h"
#include "orionmap.h"
#include <QDir>
//----------------------------------------------------------------------------------
TabMain *g_TabMain = nullptr;
//----------------------------------------------------------------------------------
TabMain::TabMain(QWidget *parent)
: QWidget(parent), ui(new Ui::TabMain)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabMain = this;

	ui->tw_Options->setCurrentIndex(0);

	connect(&m_ReconnectTimer, SIGNAL(timeout()), this, SLOT(on_ReconnectTimerTimer()));
}
//----------------------------------------------------------------------------------
TabMain::~TabMain()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabMain = nullptr;
}
//----------------------------------------------------------------------------------
void TabMain::UpdateFeatures()
{
	const uint table[CLBI_COUNT] =
	{
		OAEF_CORPSES_AUTOOPEN,
		OAEF_STEALTH_STEP_COUNTER,
		OAEF_SOUND_FILTER,
		OAEF_LIGHT_FILTER,
		OAEF_WEATHER_FILTER,
		OAEF_SEASON_FILTER,
		OAEF_NO_DEATH_SCREEN,
		OAEF_AUTO_OPEN_DOORS,
		OAEF_TRACKER,
		OAEF_FAST_ROTATION,
		OAEF_RECURSE_CONTAINERS_SEARCH,
		OAEF_AUTOSTART,
		OAEF_IGNORE_STAMINA_CHECK,
		OAEF_MINIMIZE_TO_TRAY,
		OAEF_SOUND_ECHO,
		OAEF_ANIMATION_ECHO,
		OAEF_EFFECT_ECHO,
		OAEF_SPEECH_FILTER,
		OAEF_TEXT_REPLACE,
		0, //Object inspector
		OAEF_NO_DEATH_SCREEN, //No gray screen
		0, //Save far objects
	};

	IFOR(i, 0, CLBI_COUNT)
	{
		const uint &flags = table[i];

		if (!flags || (g_EnabledFeature & flags))
			continue;

		QListWidgetItem *item = ui->lw_Options->item(i);

		if (item != nullptr && item->checkState() == Qt::CheckState::Checked)
			OptionChanged(i, item);
	}
}
//----------------------------------------------------------------------------------
void TabMain::on_hs_WindowsOpacity_valueChanged(int value)
{
	OAFUN_DEBUG("");
	double newOpacity = (double)value / 100.0;

	g_OrionAssistantForm->setWindowOpacity(newOpacity);
	g_TextDialog->setWindowOpacity(newOpacity);
	g_ScriptEditorDialog->setWindowOpacity(newOpacity);
}
//----------------------------------------------------------------------------------
void TabMain::on_tb_SelectFontColor_clicked()
{
	OAFUN_DEBUG("");
	CPacketDyeData(0xFFFFFFFF).SendClient();
}
//----------------------------------------------------------------------------------
void TabMain::on_tb_SelectCharactersFontColor_clicked()
{
	OAFUN_DEBUG("");
	CPacketDyeData(0xFFFFFFFE).SendClient();
}
//----------------------------------------------------------------------------------
void TabMain::on_lw_Options_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	QListWidgetItem *item = ui->lw_Options->item(index.row());

	if (item != nullptr)
		OptionChanged(index.row(), item);

	QString path = g_OrionAssistantForm->GetSaveConfigPath();

	if (!path.length())
		return;

	g_OrionAssistantForm->SaveMainOptions(path + "/Options.xml");
}
//----------------------------------------------------------------------------------
void TabMain::OptionChanged(const int &index, QListWidgetItem *item)
{
	bool state = (item->checkState() == Qt::CheckState::Checked);

	switch (index)
	{
		case CLBI_FILTER_LIGHT:
		{
			if (!EnabledFeatureLightFilter())
			{
				if (state)
					state = false;
			}

			CPacketLight((uchar)(state ? 0 : g_LightLevel)).SendClient();

			break;
		}
		case CLBI_FILTER_WEATHER:
		{
			if (!EnabledFeatureWeatherFilter())
			{
				if (state)
					state = false;
			}

			CPacketWeather(g_Weather).SendClient();

			break;
		}
		case CLBI_FILTER_SEASON:
		{
			if (!EnabledFeatureSeasonFilter())
			{
				if (state)
					state = false;
			}

			CPacketSeason(g_Season).SendClient();

			break;
		}
		case CLBI_TRACKER:
		{
			if (!EnabledFeatureTracker())
			{
				if (state)
					state = false;
			}

			if (!state)
				g_OrionAssistant.Track(false, 0, 0);

			break;
		}
		case CLBI_STAY_ON_TOP:
		{
			Qt::WindowFlags flags = g_OrionAssistantForm->windowFlags();

			if (state)
				g_OrionAssistantForm->setWindowFlags(flags | Qt::WindowStaysOnTopHint);
			else if (flags & Qt::WindowStaysOnTopHint)
				g_OrionAssistantForm->setWindowFlags(flags & ~Qt::WindowStaysOnTopHint);

			g_OrionAssistantForm->show();

			break;
		}
		case CLBI_FAST_ROTATION:
		{
			if (!EnabledFeatureFastRotation())
			{
				if (state)
					state = false;
			}

			g_SetInt(VKI_FAST_ROTATION, state);

			break;
		}
		case CLBI_IGNORE_STAMINA_CHECK:
		{
			if (!EnabledFeatureIgnoreStaminaCheck())
			{
				if (state)
					state = false;
			}

			g_SetInt(VKI_IGNORE_STAMINA_CHECK, state);

			break;
		}
		case CLBI_NO_GRAY_SCREEN:
		{
			if (!EnabledFeatureNoDeathScreen())
			{
				if (state)
					state = false;
			}

			if (g_Player != nullptr)
			{
				ushort graphic = 0;

				if (!state)
				{
					if (g_Player->Dead() || !g_MorphGraphic)
						graphic = g_Player->GetGraphic();
					else
						graphic = g_MorphGraphic;
				}
				else
				{
					if (g_Player->Dead())
						graphic = 0x03DB;
					else if (g_MorphGraphic)
						graphic = g_MorphGraphic;
					else
						graphic = g_Player->GetGraphic();
				}

				if (graphic)
					g_SetInt(VKI_SET_PLAYER_GRAPHIC, graphic);
			}

			break;
		}
		default:
			break;
	}
}
//----------------------------------------------------------------------------------
int TabMain::MoveItemsDelay()
{
	OAFUN_DEBUG("");
	return ui->sb_MoveItemsDelay->value();
}
//----------------------------------------------------------------------------------
int TabMain::WaitTargetsDelay()
{
	OAFUN_DEBUG("");
	return ui->sb_WaitTargetsDelay->value();
}
//----------------------------------------------------------------------------------
int TabMain::SearchItemsDistance()
{
	OAFUN_DEBUG("");
	return ui->sb_SearchObjectsDistance->value();
}
//----------------------------------------------------------------------------------
int TabMain::UseItemsDistance()
{
	OAFUN_DEBUG("");
	return ui->sb_UseObjectsDistance->value();
}
//----------------------------------------------------------------------------------
int TabMain::UseItemsDelay()
{
	OAFUN_DEBUG("");
	return ui->sb_UseItemsDelay->value();
}
//----------------------------------------------------------------------------------
int TabMain::OpenCorpsesDistance()
{
	OAFUN_DEBUG("");
	return ui->sb_OpenCorpsesDistance->value();
}
//----------------------------------------------------------------------------------
int TabMain::CorpseKeepDelay()
{
	OAFUN_DEBUG("");
	return ui->sb_CorpseKeepDelay->value();
}
//----------------------------------------------------------------------------------
int TabMain::GetMessagesLevel()
{
	OAFUN_DEBUG("");
	return ui->cb_AssistMessagesLevel->currentIndex();
}
//----------------------------------------------------------------------------------
bool TabMain::CorpsesAutoopen()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureCorpsesAutoopen())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_CORPSES_AUTOOPEN);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::StealthStepCounter()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureStealthStepCounter())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_STEALTH_STEP_COUNTER);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::SoundFilter()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureSoundFilter())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_SOUND_FILTER);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::LightFilter()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureLightFilter())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_FILTER_LIGHT);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::WeatherFilter()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureWeatherFilter())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_FILTER_WEATHER);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::SeasonFilter()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureSeasonFilter())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_FILTER_SEASON);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::NoDeathScreen()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureNoDeathScreen())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_NO_DEATH_SCREEN);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::AutoOpenDoors()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureAutoOpenDoors())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_OPEN_DOORS);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::Tracker()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureTracker())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_TRACKER);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::FastRotation()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureFastRotation())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_FAST_ROTATION);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::RecurseContainersSearch()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureRecurseContainersSearch())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_RECURSE_CONTAINERS_SEARCH);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::Autostart()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureAutostart())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_AUTOSTART);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::IgnoreStaminaCheck()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureIgnoreStaminaCheck())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_IGNORE_STAMINA_CHECK);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::MinimizeToTray()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureMinimizeToTray())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_MINIMIZE_TO_TRAY);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::StayOnTop()
{
	OAFUN_DEBUG("");
	QListWidgetItem *item = ui->lw_Options->item(CLBI_STAY_ON_TOP);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::SoundEcho()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureSoundEcho())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_SOUND_ECHO);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::AnimationEcho()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureAnimationEcho())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_ANIMATION_ECHO);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::EffectEcho()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureEffectEcho())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_EFFECT_ECHO);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::SpeechFilter()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureSpeechFilter())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_FILTER_SPEECH);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::TextReplace()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureTextReplace())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_TEXT_REPLACE);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::ObjectInspector()
{
	OAFUN_DEBUG("");
	QListWidgetItem *item = ui->lw_Options->item(CLBI_OBJECT_INSPECTOR);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::NoGrayScreen()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureNoDeathScreen())
		return false;

	QListWidgetItem *item = ui->lw_Options->item(CLBI_NO_GRAY_SCREEN);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::SaveFarObjects()
{
	OAFUN_DEBUG("");
	QListWidgetItem *item = ui->lw_Options->item(CLBI_SAVE_FAR_OBJECTS);

	return (item != nullptr && item->checkState() == Qt::CheckState::Checked);
}
//----------------------------------------------------------------------------------
bool TabMain::DoubleClickForOpenDoors()
{
	OAFUN_DEBUG("");
	return ui->cb_DoubleClickForOpenDoors->isChecked();
}
//----------------------------------------------------------------------------------
bool TabMain::GetFontColor(ushort &value)
{
	OAFUN_DEBUG("");
	bool result = ui->cb_FontColor->isChecked();

	if (ui->le_FontColor->text().length())
		value = COrionAssistant::TextToGraphic(ui->le_FontColor->text());
	else
		value = 0x02B2;

	return result;
}
//----------------------------------------------------------------------------------
void TabMain::SetFontColor(const bool &state, ushort value)
{
	OAFUN_DEBUG("");

	if (value > 0x0400)
		value = 0x02B2;

	ui->cb_FontColor->setChecked(state);
	ui->le_FontColor->setText(COrionAssistant::GraphicToText(value));
}
//----------------------------------------------------------------------------------
bool TabMain::GetCharactersFontColor(ushort &value)
{
	OAFUN_DEBUG("");
	bool result = ui->cb_CharactersFontColor->isChecked();

	if (ui->le_CharactersFontColor->text().length())
		value = COrionAssistant::TextToGraphic(ui->le_CharactersFontColor->text());
	else
		value = 0x02B2;

	return result;
}
//----------------------------------------------------------------------------------
void TabMain::SetCharactersFontColor(const bool &state, ushort value)
{
	OAFUN_DEBUG("");

	if (value > 0x0400)
		value = 0x02B2;

	ui->cb_CharactersFontColor->setChecked(state);
	ui->le_CharactersFontColor->setText(COrionAssistant::GraphicToText(value));
}
//----------------------------------------------------------------------------------
void TabMain::LoadConfig(const QXmlStreamAttributes &attributes)
{
	OAFUN_DEBUG("");

	if (attributes.hasAttribute("flags"))
	{
		uint flags = attributes.value("flags").toUInt();

		IFOR(i, 0, CLBI_COUNT)
		{
			QListWidgetItem *item = ui->lw_Options->item(i);

			if (item != nullptr)
			{
				if (flags & (1 << i))
					item->setCheckState(Qt::CheckState::Checked);
				else
					item->setCheckState(Qt::CheckState::Unchecked);
			}
		}
	}

	if (attributes.hasAttribute("messageslevel"))
		ui->cb_AssistMessagesLevel->setCurrentIndex(attributes.value("messageslevel").toInt());

	if (attributes.hasAttribute("searchdistance"))
		ui->sb_SearchObjectsDistance->setValue(attributes.value("searchdistance").toInt());

	if (attributes.hasAttribute("usedistance"))
		ui->sb_UseObjectsDistance->setValue(attributes.value("usedistance").toInt());

	if (attributes.hasAttribute("opencorpsedistance"))
		ui->sb_OpenCorpsesDistance->setValue(attributes.value("opencorpsedistance").toInt());

	if (attributes.hasAttribute("corpsekeepdelay"))
		ui->sb_CorpseKeepDelay->setValue(attributes.value("corpsekeepdelay").toInt());

	if (attributes.hasAttribute("waittargetsdelay"))
		ui->sb_WaitTargetsDelay->setValue(attributes.value("waittargetsdelay").toInt());

	if (attributes.hasAttribute("moveitemsdelay"))
		ui->sb_MoveItemsDelay->setValue(attributes.value("moveitemsdelay").toInt());

	if (attributes.hasAttribute("useitemsdelay"))
		ui->sb_UseItemsDelay->setValue(attributes.value("useitemsdelay").toInt());

	if (attributes.hasAttribute("windowsopacity"))
		ui->hs_WindowsOpacity->setValue(attributes.value("windowsopacity").toInt());

	if (attributes.hasAttribute("usefontcolor"))
		ui->cb_FontColor->setChecked(COrionAssistant::RawStringToBool(attributes.value("usefontcolor").toString()));

	if (attributes.hasAttribute("fontcolor"))
		ui->le_FontColor->setText(attributes.value("fontcolor").toString());

	if (attributes.hasAttribute("usecharactersfontcolor"))
		ui->cb_CharactersFontColor->setChecked(COrionAssistant::RawStringToBool(attributes.value("usecharactersfontcolor").toString()));

	if (attributes.hasAttribute("charactersfontcolor"))
		ui->le_CharactersFontColor->setText(attributes.value("charactersfontcolor").toString());

	if (attributes.hasAttribute("autoreconnect"))
		ui->cb_AutoReconnect->setChecked(COrionAssistant::RawStringToBool(attributes.value("autoreconnect").toString()));

	if (attributes.hasAttribute("autoreconnecttimer"))
		ui->sb_AutoReconnectSeconds->setValue(attributes.value("autoreconnecttimer").toInt());

	if (attributes.hasAttribute("doubleclickforopendoors"))
		ui->cb_DoubleClickForOpenDoors->setChecked(COrionAssistant::RawStringToBool(attributes.value("doubleclickforopendoors").toString()));
}
//----------------------------------------------------------------------------------
void TabMain::SaveConfig(QXmlStreamWriter &writter)
{
	OAFUN_DEBUG("");

	writter.writeStartElement("main");

	uint flags = 0;

	IFOR(i, 0, CLBI_COUNT)
	{
		QListWidgetItem *item = ui->lw_Options->item(i);

		if (item != nullptr)
		{
			if (item->checkState() == Qt::CheckState::Checked)
				flags |= 1 << i;
		}
	}

	writter.writeAttribute("flags", QString::number(flags));
	writter.writeAttribute("messageslevel", QString::number(ui->cb_AssistMessagesLevel->currentIndex()));
	writter.writeAttribute("searchdistance", QString::number(ui->sb_SearchObjectsDistance->value()));
	writter.writeAttribute("usedistance", QString::number(ui->sb_UseObjectsDistance->value()));
	writter.writeAttribute("opencorpsedistance", QString::number(ui->sb_OpenCorpsesDistance->value()));
	writter.writeAttribute("corpsekeepdelay", QString::number(ui->sb_CorpseKeepDelay->value()));
	writter.writeAttribute("waittargetsdelay", QString::number(ui->sb_WaitTargetsDelay->value()));
	writter.writeAttribute("moveitemsdelay", QString::number(ui->sb_MoveItemsDelay->value()));
	writter.writeAttribute("useitemsdelay", QString::number(ui->sb_UseItemsDelay->value()));
	writter.writeAttribute("windowsopacity", QString::number(ui->hs_WindowsOpacity->value()));
	writter.writeAttribute("usefontcolor", (ui->cb_FontColor->isChecked() ? "true" : "false"));
	writter.writeAttribute("fontcolor", ui->le_FontColor->text());
	writter.writeAttribute("usecharactersfontcolor", (ui->cb_CharactersFontColor->isChecked() ? "true" : "false"));
	writter.writeAttribute("charactersfontcolor", ui->le_CharactersFontColor->text());
	writter.writeAttribute("autoreconnect", (ui->cb_AutoReconnect->isChecked() ? "true" : "false"));
	writter.writeAttribute("autoreconnecttimer", QString::number(ui->sb_AutoReconnectSeconds->value()));
	writter.writeAttribute("doubleclickforopendoors", (ui->cb_DoubleClickForOpenDoors->isChecked() ? "true" : "false"));

	writter.writeEndElement(); //main
}
//----------------------------------------------------------------------------------
void TabMain::StartReconnect()
{
	OAFUN_DEBUG("");
	if (!ui->cb_AutoReconnect->isChecked() || !EnabledFeatureReconnector())
		return;

	if (m_ReconnectTimer.isActive())
		m_ReconnectTimer.stop();

	m_ReconnectSeconds = ui->sb_AutoReconnectSeconds->value();

	m_ReconnectTimer.start(1000);
}
//----------------------------------------------------------------------------------
void TabMain::on_ReconnectTimerTimer()
{
	OAFUN_DEBUG("");
	if (!EnabledFeatureReconnector() || g_Connected || !ui->cb_AutoReconnect->isChecked())
	{
		m_ReconnectTimer.stop();
		ui->l_SecondsToReconnect->setText("");
	}
	else
	{
		m_ReconnectSeconds--;

		if (!m_ReconnectSeconds)
		{
			CPacketReconnect().SendClient();
			m_ReconnectSeconds = ui->sb_AutoReconnectSeconds->value();
		}

		ui->l_SecondsToReconnect->setText("(left " + QString::number(m_ReconnectSeconds) + " seconds)");
	}
}
//----------------------------------------------------------------------------------
void TabMain::on_cb_ProfileList_currentIndexChanged(const QString &arg1)
{
	OAFUN_DEBUG("");
	Q_UNUSED(arg1);
	g_OrionAssistantForm->LoadCurrentProfile();

	if (g_Connected && g_Player != nullptr)
		g_OrionAssistantForm->SaveProfileMap();
}
//----------------------------------------------------------------------------------
void TabMain::on_pb_SaveProfile_clicked()
{
	OAFUN_DEBUG("");

	QString profile = ui->cb_ProfileList->currentText();
	g_OrionAssistantForm->SaveCurrentProfile();

	IFOR(i, 0, ui->cb_ProfileList->count())
	{
		if (ui->cb_ProfileList->itemText(i) == profile)
			return;
	}

	ui->cb_ProfileList->addItem(profile);
	ui->cb_ProfileList->setCurrentIndex(ui->cb_ProfileList->count() - 1);
}
//----------------------------------------------------------------------------------
void TabMain::InitProfiles(const QStringList &list)
{
	OAFUN_DEBUG("");

	for (const QString &str : list)
		ui->cb_ProfileList->addItem(str);

	if (!ui->cb_ProfileList->count())
	{
		ui->cb_ProfileList->addItem("Default");
		ui->cb_ProfileList->setCurrentIndex(ui->cb_ProfileList->count());
		g_OrionAssistantForm->SaveCurrentProfile();
	}
}
//----------------------------------------------------------------------------------
QString TabMain::ProfileName()
{
	OAFUN_DEBUG("");
	return ui->cb_ProfileList->currentText();
}
//----------------------------------------------------------------------------------
void TabMain::SetCurrentProfile(const QString &name)
{
	OAFUN_DEBUG("");
	QString nameLower = name.toLower();

	if (ui->cb_ProfileList->currentText().toLower() != nameLower)
	{
		IFOR(i, 0, ui->cb_ProfileList->count())
		{
			if (ui->cb_ProfileList->itemText(i).toLower() == nameLower)
			{
				ui->cb_ProfileList->setCurrentIndex(i);
				return;
			}
		}
	}
}
//----------------------------------------------------------------------------------
void TabMain::on_pb_OpenMap_clicked()
{
	OAFUN_DEBUG("");
	OpenMap();
}
//----------------------------------------------------------------------------------
void TabMain::OpenMap()
{
	OAFUN_DEBUG("");

	if (g_OrionMap == nullptr)
		g_OrionMap = new OrionMap();

	if (g_OrionMap->isVisible())
		g_OrionMap->activateWindow();
	else
		g_OrionMap->show();
}
//----------------------------------------------------------------------------------
void TabMain::MoveMap(const int &x, const int &y)
{
	OAFUN_DEBUG("");
	if (g_OrionMap != nullptr)
		g_OrionMap->move(x, y);
}
//----------------------------------------------------------------------------------
void TabMain::CloseMap()
{
	OAFUN_DEBUG("");
	RELEASE_POINTER(g_OrionMap);
}
//----------------------------------------------------------------------------------
