// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** Packets.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "Packets.h"
#include "orionassistant.h"
#include "Target.h"
#include "../Managers/DataReader.h"
#include "../GameObjects/GamePlayer.h"
#include "../GameObjects/GameWorld.h"
//----------------------------------------------------------------------------------
CPacket::CPacket(const int &size, const bool &autoResize)
: CDataWritter(size, autoResize)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
void CPacket::SendClient()
{
	OAFUN_DEBUG("");
	if (m_Data.size())
		emit g_OrionAssistant.signal_SendClient(m_Data);
}
//----------------------------------------------------------------------------------
void CPacket::SendServer()
{
	OAFUN_DEBUG("");
	if (m_Data.size())
		emit g_OrionAssistant.signal_SendServer(m_Data);
}
//----------------------------------------------------------------------------------
CPacketLight::CPacketLight(const uchar &light)
: CPacket(2)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x4F);
	WriteUInt8(light);
}
//----------------------------------------------------------------------------------
CPacketPersonalLight::CPacketPersonalLight(const uint &serial, const uchar &light)
: CPacket(6)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x4E);
	WriteUInt32BE(serial);
	WriteUInt8(light);
}
//----------------------------------------------------------------------------------
CPacketToClientSystemPrint::CPacketToClientSystemPrint(const wstring &text, const ushort &color)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int size = 48 + text.length() * 2 + 2;
	Resize(size, true);

	WriteUInt8(0xAE);
	WriteUInt16BE(size);
	WriteUInt32BE(0xFFFFFFFF);			//serial
	WriteUInt16BE(0);					//graphic
	WriteUInt8(0);						//mode
	WriteUInt16BE(color);				//color
	WriteUInt16BE(3);					//font
	WriteUInt32BE(0x00000000);			//language
	WriteString("OrionAssistant", 29);		//sender name
	WriteWString(text, text.length());	//content
}
//----------------------------------------------------------------------------------
CPacketToClientCharacterPrint::CPacketToClientCharacterPrint(const uint &serial, const wstring &text, const ushort &color)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int size = 48 + text.length() * 2 + 2;
	Resize(size, true);

	WriteUInt8(0xAE);
	WriteUInt16BE(size);
	WriteUInt32BE(serial);				//serial
	WriteUInt16BE(0);					//graphic
	WriteUInt8(0);						//mode
	WriteUInt16BE(color);				//color
	WriteUInt16BE(3);					//font
	WriteUInt32BE(0x00000000);			//language
	WriteString("OrionAssistant", 29);		//sender name
	WriteWString(text, text.length());	//content
}
//----------------------------------------------------------------------------------
CPacketToClientTextReplacePrintA::CPacketToClientTextReplacePrintA(const string &text, const uint &serial, const ushort &graphic, const uchar &messageType, const ushort &color, const ushort &font)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int size = 44 + text.length() + 1;
	Resize(size, true);

	QString name = "OrionAssistant";

	if (g_World != nullptr)
	{
		CGameObject *obj = g_World->FindWorldObject(serial);

		if (obj != nullptr && obj->GetName().length())
			name = obj->GetName();
	}

	WriteUInt8(0x1C);
	WriteUInt16BE(size);
	WriteUInt32BE(serial);				//serial
	WriteUInt16BE(graphic);				//graphic
	WriteUInt8(messageType);			//mode
	WriteUInt16BE(color);				//color
	WriteUInt16BE(font);				//font
	WriteString(name.toStdString(), 29);//sender name
	WriteString(text, text.length());	//content
}
//----------------------------------------------------------------------------------
CPacketToClientTextReplacePrintW::CPacketToClientTextReplacePrintW(const wstring &text, const uint &serial, const ushort &graphic, const uchar &messageType, const ushort &color, const ushort &font, const uint &lang)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int size = 48 + text.length() * 2 + 2;
	Resize(size, true);

	QString name = "OrionAssistant";

	if (g_World != nullptr)
	{
		CGameObject *obj = g_World->FindWorldObject(serial);

		if (obj != nullptr && obj->GetName().length())
			name = obj->GetName();
	}

	WriteUInt8(0xAE);
	WriteUInt16BE(size);
	WriteUInt32BE(serial);				//serial
	WriteUInt16BE(graphic);				//graphic
	WriteUInt8(messageType);			//mode
	WriteUInt16BE(color);				//color
	WriteUInt16BE(font);				//font
	WriteUInt32BE(lang);				//language
	WriteString(name.toStdString(), 29);				//sender name
	WriteWString(text, text.length());	//content
}
//----------------------------------------------------------------------------------
CPacketToClientTargetRequest::CPacketToClientTargetRequest()
: CPacket(19)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x6C);
	Move(1);
	WriteUInt32BE(0x0000601A);
	WriteUInt32BE(0x00010000);

	UCHAR_LIST packet = Data();
	CDataReader reader(&packet[0], packet.size());
	reader.Move(1);
	g_Target.SetRecvOAData(reader);
}
//----------------------------------------------------------------------------------
CPacketStatusRequest::CPacketStatusRequest(const uint &serial)
: CPacket(10)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x34);
	WriteUInt32BE(0xEDEDEDED);
	WriteUInt8(4);
	WriteUInt32BE(serial);
}
//----------------------------------------------------------------------------------
CPacketClickRequest::CPacketClickRequest(const uint &serial)
: CPacket(5)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x09);
	WriteUInt32BE(serial);
}
//----------------------------------------------------------------------------------
CPacketDoubleClickRequest::CPacketDoubleClickRequest(const uint &serial)
: CPacket(5)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x06);
	WriteUInt32BE(serial);
}
//----------------------------------------------------------------------------------
CPacketAttackRequest::CPacketAttackRequest(const uint &serial)
: CPacket(5)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x05);
	WriteUInt32BE(serial);
}
//----------------------------------------------------------------------------------
CPacketWeather::CPacketWeather(const WEATHER_DATA &data)
: CPacket(4)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x65);
	WriteUInt8(data.Type);
	WriteUInt8(data.Count);
	WriteUInt8(data.Temperature);
}
//----------------------------------------------------------------------------------
CPacketSeason::CPacketSeason(const SEASON_DATA &data)
: CPacket(3)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xBC);
	WriteUInt8(data.Index);
	WriteUInt8(data.Music);
}
//----------------------------------------------------------------------------------
CPacketHelpRequest::CPacketHelpRequest()
: CPacket(258)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x9B);
}
//----------------------------------------------------------------------------------
CPacketChangeWarmode::CPacketChangeWarmode(const uchar &state)
: CPacket(5)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x72);
	WriteUInt8(state);
	WriteUInt16BE(0x0032);
}
//----------------------------------------------------------------------------------
CPacketResend::CPacketResend()
: CPacket(3)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x22);
}
//----------------------------------------------------------------------------------
CPacketEmoteAction::CPacketEmoteAction(const char *action)
: CPacket(5 + strlen(action))
{
	OAFUN_DEBUG("");
	WriteUInt8(0x12);
	WriteUInt16BE(5 + strlen(action));
	WriteUInt8(0xC7);
	WriteString(action);
}
//----------------------------------------------------------------------------------
CPacketHideObject::CPacketHideObject(const uint &serial)
: CPacket(5)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x1D);
	WriteUInt32BE(serial);
}
//----------------------------------------------------------------------------------
CPacketPlaySound::CPacketPlaySound(const ushort &index, const short &x, const short &y, const char &z)
: CPacket(12)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x54);
	WriteUInt8(0x01);
	WriteUInt16BE(index);
	WriteUInt16BE(0x00);
	WriteInt16BE(x);
	WriteInt16BE(y);
	WriteInt8(z);
}
//----------------------------------------------------------------------------------
CPacketPickupRequest::CPacketPickupRequest(const uint &serial, const ushort &count)
: CPacket(7)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x07);
	WriteUInt32BE(serial);
	WriteUInt16BE(count);
}
//----------------------------------------------------------------------------------
CPacketDropRequestOld::CPacketDropRequestOld(const uint &serial,const  ushort &x, const ushort &y, const char &z, const uint &container)
: CPacket(14)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x08);
	WriteUInt32BE(serial);
	WriteUInt16BE(x);
	WriteUInt16BE(y);
	WriteUInt8(z);
	WriteUInt32BE(container);
}
//----------------------------------------------------------------------------------
CPacketDropRequestNew::CPacketDropRequestNew(const uint &serial, const ushort &x, const ushort &y, const char &z, const uchar &slot, const uint &container)
: CPacket(15)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x08);
	WriteUInt32BE(serial);
	WriteUInt16BE(x);
	WriteUInt16BE(y);
	WriteUInt8(z);
	WriteUInt8(slot);
	WriteUInt32BE(container);
}
//----------------------------------------------------------------------------------
CPacketEquipRequest::CPacketEquipRequest(const uint &serial, const uchar &layer, const uint &container)
: CPacket(10)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x13);
	WriteUInt32BE(serial);
	WriteUInt8(layer);
	WriteUInt32BE(container);
}
//----------------------------------------------------------------------------------
CPacketQuestArrowOld::CPacketQuestArrowOld(const bool &enabled, const short &x, const short &y)
: CPacket(6)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xBA);
	WriteUInt8((uchar)enabled);
	WriteUInt16BE(x);
	WriteUInt16BE(y);
}
//----------------------------------------------------------------------------------
CPacketQuestArrowNew::CPacketQuestArrowNew(const bool &enabled, const short &x, const short &y)
: CPacket(10)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xBA);
	WriteUInt8((uchar)enabled);
	WriteUInt16BE(x);
	WriteUInt16BE(y);
	WriteUInt32BE(0);
}
//----------------------------------------------------------------------------------
CPacketMenuResponse::CPacketMenuResponse(const uint &serial, const uint &id, const int &code, const ushort &graphic, const ushort &color)
: CPacket(13)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x7D);
	WriteUInt32BE(serial);
	WriteUInt16BE(id);
	WriteUInt16BE(code);
	WriteUInt16BE(graphic);
	WriteUInt16BE(color);
}
//----------------------------------------------------------------------------------
CPacketGrayMenuResponse::CPacketGrayMenuResponse(const uint &serial, const uint &id, const int &code)
: CPacket(13)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x7D);
	WriteUInt32BE(serial);
	WriteUInt16BE(id);
	WriteUInt16BE(code);
}
//----------------------------------------------------------------------------------
CPacketGraphicEffect::CPacketGraphicEffect(const ushort &graphic, const uchar &duration, const short &x, const short &y, const char &z)
: CPacket(28)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x70);
	WriteUInt8(2); //EF_STAY_AT_POS
	WriteUInt32BE(0x00000000); //sourceSerial
	WriteUInt32BE(0x00000000); //destSerial
	WriteUInt16BE(graphic);
	WriteUInt16BE(x); //src pos
	WriteUInt16BE(y);
	WriteUInt8(z);
	WriteUInt16BE(0); //dest pos
	WriteUInt16BE(0);
	WriteUInt8(0);
	WriteUInt8(1); //speed
	WriteUInt8(duration); //duration
	WriteUInt16BE(0); //??
	WriteUInt8(0); //fixedDirection
	WriteUInt8(0); //explode
}
//---------------------------------------------------------------------------
CPacketPartyAccept::CPacketPartyAccept(const uint &serial)
: CPacket(10)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xBF);
	WriteUInt16BE(0x000a);
	WriteUInt16BE(0x0006);
	WriteUInt8(0x08);
	WriteUInt32BE(serial);
}
//---------------------------------------------------------------------------
CPacketPartyDecline::CPacketPartyDecline(const uint &serial)
: CPacket(10)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xBF);
	WriteUInt16BE(0x000a);
	WriteUInt16BE(0x0006);
	WriteUInt8(0x09);
	WriteUInt32BE(serial);
}
//----------------------------------------------------------------------------------
CPacketTradeClose::CPacketTradeClose(const uint &id)
: CPacket(17)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x6F);
	WriteUInt16BE(17);
	WriteUInt8(0x01);
	WriteUInt32BE(id);
}
//----------------------------------------------------------------------------------
CPacketTradeCheckState::CPacketTradeCheckState(const uint &id, const bool &state)
: CPacket(17)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x6F);
	WriteUInt16BE(17);
	WriteUInt8(0x02);
	WriteUInt32BE(id);
	WriteUInt32BE(state);
}
//----------------------------------------------------------------------------------
CPacketDyeData::CPacketDyeData(const uint &serial)
: CPacket(9)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x95);
	WriteUInt32BE(serial);
	WriteUInt16BE(0);
	WriteUInt16BE(0x0FAB);
}
//---------------------------------------------------------------------------
CPacketUseCombatAbility::CPacketUseCombatAbility(const uchar &index)
: CPacket(15)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xD7);
	WriteUInt16BE(0x000F);
	WriteUInt32BE(g_PlayerSerial);
	WriteUInt16BE(0x0019);
	WriteUInt32BE(0x00000000);
	WriteUInt8(index);
	WriteUInt8(0x0A);
}
//----------------------------------------------------------------------------------
CPacketWrestlingStun::CPacketWrestlingStun()
: CPacket(5)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xBF);
	WriteUInt16BE(0x05);
	WriteUInt16BE(0x09);
}
//----------------------------------------------------------------------------------
CPacketWrestlingDisarm::CPacketWrestlingDisarm()
: CPacket(5)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xBF);
	WriteUInt16BE(0x05);
	WriteUInt16BE(0x0A);
}
//----------------------------------------------------------------------------------
CPacketCancelTarget::CPacketCancelTarget()
: CPacket(19)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x6C);
	WriteUInt8(0x00);
	WriteUInt32BE(0x00000000);
	WriteUInt8(0x03);
}
//----------------------------------------------------------------------------------
CPacketInvokeVirtureRequest::CPacketInvokeVirtureRequest(const uchar &id)
: CPacket(6)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x12);
	WriteUInt16BE(0x0006);
	WriteUInt8(0xF4);
	WriteUInt8(id);
	WriteUInt8(0x00);
}
//----------------------------------------------------------------------------------
CPacketGumpResponse::CPacketGumpResponse(CGump *gump, const int &code)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int switchesCount = 0;
	int textLinesCount = 0;
	int textLinesLength = 0;

	for (CGumpItem &item : gump->m_Items)
	{
		switch (item.GetType())
		{
			case GIT_CHECKBOX:
			case GIT_RADIO:
			{
				QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

				if (commands.size() >= 7 && commands[5].toInt())
					switchesCount++;

				break;
			}
			case GIT_TEXTENTRY:
			case GIT_TEXTENTRYLIMITED:
			{
				QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

				if (commands.size() >= 8)
				{
					uint textIndex = commands[7].toUInt();

					if (textIndex < (uint)gump->m_Text.size())
					{
						textLinesLength += gump->m_Text[textIndex].length() * 2;
						textLinesCount++;
					}
				}

				break;
			}
			default:
				break;
		}
	}

	int size = 19 + (switchesCount * 4) + 4 + ((textLinesCount * 4) + textLinesLength);
	Resize(size, true);

	WriteUInt8(0xB1);
	WriteUInt16BE(size);
	WriteUInt32BE(gump->GetSerial());
	WriteUInt32BE(gump->GetID());
	WriteUInt32BE(code);
	WriteUInt32BE(switchesCount);

	for (CGumpItem &item : gump->m_Items)
	{
		if (item.GetType() == GIT_CHECKBOX || item.GetType() == GIT_RADIO)
		{
			QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

			if (commands.size() >= 7 && commands[5].toInt())
				WriteUInt32BE(commands[6].toInt());
		}
	}

	WriteUInt32BE(textLinesCount);

	for (CGumpItem &item : gump->m_Items)
	{
		if (item.GetType() == GIT_TEXTENTRY || item.GetType() == GIT_TEXTENTRYLIMITED)
		{
			QStringList commands = item.GetCommand().split(" ", QString::SkipEmptyParts);

			if (commands.size() >= 8)
			{
				uint textIndex = commands[7].toUInt();

				if (textIndex < (uint)gump->m_Text.size())
				{
					QString &line = gump->m_Text[textIndex];

					WriteUInt16BE(commands[6].toInt());
					WriteUInt16BE(line.length());
					WriteWString(line.toStdWString(), line.length(), true, false);
				}
			}
		}
	}
}
//----------------------------------------------------------------------------------
CPacketTargetCancel::CPacketTargetCancel(const uchar &type, const uint &targetSerial, const uchar &targetType)
: CPacket(19)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x6C);
	WriteUInt8(type);
	WriteUInt32BE(targetSerial);
	WriteUInt8(targetType);
	WriteUInt32BE(0);
	WriteUInt16BE(0xFFFF);
	WriteUInt16BE(0xFFFF);
	WriteUInt16BE(0);
	WriteUInt16BE(0);
}
//----------------------------------------------------------------------------------
CPacketTargetObject::CPacketTargetObject(const uchar &type, const uint &targetSerial, const uchar &targetType, const uint &serial)
: CPacket(19)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x6C);
	WriteUInt8(type);
	WriteUInt32BE(targetSerial);
	WriteUInt8(targetType);
	WriteUInt32BE(serial);

	CGameObject *obj = (g_World != nullptr ? g_World->FindWorldObject(serial) : nullptr);

	if (obj != nullptr)
	{
		WriteUInt16BE(obj->GetX());
		WriteUInt16BE(obj->GetY());
		WriteUInt16BE(obj->GetZ());
		WriteUInt16BE(obj->GetGraphic());
	}
}
//----------------------------------------------------------------------------------
CPacketTargetTile::CPacketTargetTile(const uchar &type, const uint &targetSerial, const uchar &targetType, const ushort &graphic, const ushort &x, const ushort &y, const char &z)
: CPacket(19)
{
	OAFUN_DEBUG("");
	WriteUInt8(0x6C);
	WriteUInt8(type);
	WriteUInt32BE(targetSerial);
	WriteUInt8(targetType);
	WriteUInt32BE(0);
	WriteUInt16BE(x);
	WriteUInt16BE(y);
	WriteUInt16BE(z);
	WriteUInt16BE(graphic);
}
//---------------------------------------------------------------------------
CPacketProfileRequest::CPacketProfileRequest(const uint &serial)
: CPacket(8)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xB8);
	WriteUInt16BE(0x0008);
	WriteUInt8(0);
	WriteUInt32BE(serial);
}
//---------------------------------------------------------------------------
CPacketASCIIPromptResponse::CPacketASCIIPromptResponse(const uint &serial, const uint &id, const string &text, const bool &cancel)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int size = 15 + text.length() + 1;
	Resize(size, true);

	WriteUInt8(0x9A);
	WriteUInt16BE(size);
	WriteUInt32BE(serial);
	WriteUInt32BE(id);
	WriteUInt32BE((int)(!cancel));
	WriteString(text);
}
//---------------------------------------------------------------------------
CPacketUnicodePromptResponse::CPacketUnicodePromptResponse(const uint &serial, const uint &id, const wstring &text, const string &lang, const bool &cancel)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int size = 19 + (text.length() * 2);
	Resize(size, true);

	WriteUInt8(0xC2);
	WriteUInt16BE(size);
	WriteUInt32BE(serial);
	WriteUInt32BE(id);
	WriteUInt32BE((int)(!cancel));
	WriteString(lang, 4, false);

	WriteWString(text, text.length(), false, false);
}
//---------------------------------------------------------------------------
CPacketBuyRequest::CPacketBuyRequest(const uint &serial, const QVector< std::pair<uint, int> > &list)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int size = 8 + (list.size() * 7);

	Resize(size, true);

	WriteUInt8(0x3B);
	WriteUInt16BE(size);
	WriteUInt32BE(serial);

	if (!list.empty())
	{
		WriteUInt8(0x02);

		for (const std::pair<uint, int> &item : list)
		{
			WriteUInt8(0x1A);
			WriteUInt32BE(item.first);
			WriteUInt16BE(item.second);
		}
	}
	else
		WriteUInt8(0x00);
}
//---------------------------------------------------------------------------
CPacketSellRequest::CPacketSellRequest(const uint &serial, const QVector< std::pair<uint, int> > &list)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int count = list.size();

	int size = 9 + (count * 6);

	Resize(size, true);

	WriteUInt8(0x9F);
	WriteUInt16BE(size);
	WriteUInt32BE(serial);
	WriteUInt16BE(count);

	if (count)
	{
		for (const std::pair<uint, int> &item : list)
		{
			WriteUInt32BE(item.first);
			WriteUInt16BE(item.second);
		}
	}
}
//---------------------------------------------------------------------------
CPacketLogOut::CPacketLogOut()
: CPacket(2)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xD1);
	WriteUInt8(0x00);
}
//---------------------------------------------------------------------------
CPacketClosePaperdoll::CPacketClosePaperdoll(const uint &serial)
: CPacket(13)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xBF);
	WriteUInt16BE(13);
	WriteUInt16BE(0x0016);
	WriteUInt32BE(1);
	WriteUInt32BE(serial);
}
//---------------------------------------------------------------------------
CPacketContextMenuRequest::CPacketContextMenuRequest(const uint &serial)
: CPacket(9)
{
	WriteUInt8(0xBF);
	WriteUInt16BE(0x0009);
	WriteUInt16BE(0x0013);
	WriteUInt32BE(serial);
}
//---------------------------------------------------------------------------
CPacketContextMenuSelection::CPacketContextMenuSelection(const uint &serial, const uint &index)
: CPacket(11)
{
	WriteUInt8(0xBF);
	WriteUInt16BE(0x000B);
	WriteUInt16BE(0x0015);
	WriteUInt32BE(serial);
	WriteUInt16BE((ushort)index);
}
//---------------------------------------------------------------------------
CPacketTargetUseObject::CPacketTargetUseObject(const uint &useObjectSerial, const uint &targetObjectSerial)
: CPacket(13)
{
	WriteUInt8(0xBF);
	WriteUInt16BE(0x000D);
	WriteUInt16BE(0x002C);
	WriteUInt32BE(useObjectSerial);
	WriteUInt32BE(targetObjectSerial);
}
//---------------------------------------------------------------------------
CPacketTargetCastSpell::CPacketTargetCastSpell(const ushort &spell, const uint &targetObjectSerial)
: CPacket(11)
{
	WriteUInt8(0xBF);
	WriteUInt16BE(0x000B);
	WriteUInt16BE(0x002D);
	WriteUInt16BE(spell);
	WriteUInt32BE(targetObjectSerial);
}
//---------------------------------------------------------------------------
CPacketTargetUseSkill::CPacketTargetUseSkill(const ushort &skill, const uint &targetObjectSerial)
: CPacket(11)
{
	WriteUInt8(0xBF);
	WriteUInt16BE(0x000B);
	WriteUInt16BE(0x002E);
	WriteUInt16BE(skill);
	WriteUInt32BE(targetObjectSerial);
}
//---------------------------------------------------------------------------
CPacketClientViewRange::CPacketClientViewRange(uchar range)
: CPacket(2)
{
	WriteUInt8(0xC8);

	if (range < 5)
		range = 5;
	else if (range > 24)
		range = 24;

	WriteUInt8(range);
}
//---------------------------------------------------------------------------
CPacketConfirmWalk::CPacketConfirmWalk(const uchar &direction, const uchar &notoriety)
: CPacket(3)
{
	WriteUInt8(0x22);
	WriteUInt8(direction);
	WriteUInt8(notoriety);
}
//---------------------------------------------------------------------------
CPacketMapUOPartyInfo::CPacketMapUOPartyInfo()
: CPacket(4)
{
	WriteUInt8(0xF0);
	WriteUInt16BE(4);
	WriteUInt8(0x00);
}
//---------------------------------------------------------------------------
CPacketMapUOGuildInfo::CPacketMapUOGuildInfo()
: CPacket(5)
{
	WriteUInt8(0xF0);
	WriteUInt16BE(5);
	WriteUInt8(0x00);
	WriteUInt8(0x00);
}
//---------------------------------------------------------------------------
CPacketGuildMenuRequest::CPacketGuildMenuRequest()
: CPacket(10)
{
	WriteUInt8(0xD7);
	WriteUInt16BE(0x000A);
	WriteUInt32BE(g_PlayerSerial);
	WriteUInt16BE(0x0028);
	WriteUInt8(0x0A);
}
//---------------------------------------------------------------------------
CPacketQuestMenuRequest::CPacketQuestMenuRequest()
: CPacket(10)
{
	WriteUInt8(0xD7);
	WriteUInt16BE(0x000A);
	WriteUInt32BE(g_PlayerSerial);
	WriteUInt16BE(0x0032);
	WriteUInt8(0x0A);
}
//---------------------------------------------------------------------------
CPacketToggleGargoyleFlying::CPacketToggleGargoyleFlying()
: CPacket(11)
{
	WriteUInt8(0xBF);
	WriteUInt16BE(0x000B);
	WriteUInt16BE(0x0032);
	WriteUInt16BE(0x0001);
	WriteUInt32BE(0x00000000);
}











//----------------------------------------------------------------------------------
CPacketCloseGenericGumpWithoutResponse::CPacketCloseGenericGumpWithoutResponse(CGump *gump, const bool &all)
: CPacket(14)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(0x000E);
	WriteUInt16BE(OCT_CLOSE_GENERIC_GUMP_WITHOUT_RESPONSE);
	WriteUInt32BE(gump->GetSerial());
	WriteUInt32BE(gump->GetID());
	WriteUInt8(all ? 1 : 0);
}
//----------------------------------------------------------------------------------
CPacketSelectMenuInClient::CPacketSelectMenuInClient(const uint &serial, const uint &id, const int &code)
: CPacket(17)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(0x0011);
	WriteUInt16BE(OCT_SELECT_MENU);
	WriteUInt32BE(serial);
	WriteUInt32BE(id);
	WriteUInt32BE(code);
}
//----------------------------------------------------------------------------------
CPacketCastSpellRequest::CPacketCastSpellRequest(const int &id)
: CPacket(9)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(0x0009);
	WriteUInt16BE(OCT_CAST_SPELL_REQUEST);
	WriteInt32BE(id);
}
//----------------------------------------------------------------------------------
CPacketUseSkillRequest::CPacketUseSkillRequest(const int &id)
: CPacket(9)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(0x0009);
	WriteUInt16BE(OCT_USE_SKILL_REQUEST);
	WriteInt32BE(id);
}
//----------------------------------------------------------------------------------
CPacketDrawStatusbar::CPacketDrawStatusbar(const uint &serial, const int &x, const int &y, const bool &minimized)
: CPacket(18)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(0x0012);
	WriteUInt16BE(OCT_DRAW_STATUSBAR);
	WriteUInt32BE(serial);
	WriteUInt32BE(x);
	WriteUInt32BE(y);
	WriteUInt8(minimized ? 1 : 0);
}
//----------------------------------------------------------------------------------
CPacketCloseStatusbar::CPacketCloseStatusbar(const uint &serial)
: CPacket(9)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(0x0009);
	WriteUInt16BE(OCT_CLOSE_STATUSBAR);
	WriteUInt32BE(serial);
}
//----------------------------------------------------------------------------------
CPacketSecureTradeCheck::CPacketSecureTradeCheck(const uint &id1, const bool &state)
: CPacket(10)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(0x000A);
	WriteUInt16BE(OCT_SECURE_TRADE_CHECK);
	WriteUInt32BE(id1);
	WriteUInt8(state ? 1 : 0);
}
//----------------------------------------------------------------------------------
CPacketSecureTradeClose::CPacketSecureTradeClose(const uint &id1)
: CPacket(9)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(0x0009);
	WriteUInt16BE(OCT_SECURE_TRADE_CLOSE);
	WriteUInt32BE(id1);
}
//----------------------------------------------------------------------------------
CPacketUnicodeSpeechRequest::CPacketUnicodeSpeechRequest(const ushort &color, const wstring &text)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int size = 7 + text.length() * 2 + 2;
	Resize(size, true);

	WriteUInt8(0xFC);
	WriteUInt16BE(size);
	WriteUInt16BE(OCT_UNICODE_SPEECH_REQUEST);
	WriteUInt16BE(color);
	WriteWString(text);
}
//----------------------------------------------------------------------------------
CPacketRenameMountRequest::CPacketRenameMountRequest(const uint &serial, const string &name)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int size = 9 + name.length() + 1;
	Resize(size, true);

	WriteUInt8(0xFC);
	WriteUInt16BE(size);
	WriteUInt16BE(OCT_RENAME_MOUNT_REQUEST);
	WriteUInt32BE(serial);
	WriteString(name);
}
//----------------------------------------------------------------------------------
CPacketReconnect::CPacketReconnect()
: CPacket(5)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(0x0005);
	WriteUInt16BE(OCT_RECONNECT);
}
//----------------------------------------------------------------------------------
CPacketPlayMacro::CPacketPlayMacro(const QVector<string> &actionList, const QVector<string> &subActionList)
: CPacket(1)
{
	OAFUN_DEBUG("");
	int size = 7;

	for (const string &s : actionList)
		size += s.length() + 1;

	for (const string &s : subActionList)
		size += s.length() + 1;

	Resize(size, true);
	int count = actionList.size();

	WriteUInt8(0xFC);
	WriteUInt16BE(size);
	WriteUInt16BE(OCT_PLAY_MACRO);
	WriteUInt16BE(count);

	IFOR(i, 0, count)
	{
		WriteString(actionList[i]);
		WriteString(subActionList[i]);
	}
}
//----------------------------------------------------------------------------------
CPacketMovePaperdoll::CPacketMovePaperdoll(const uint &serial, const int &x, const int &y)
: CPacket(14)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(14);
	WriteUInt16BE(OCT_MOVE_PAPERDOLL);
	WriteUInt32BE(serial);
	WriteUInt16BE(x);
	WriteUInt16BE(y);
	WriteUInt8(0);
}
//----------------------------------------------------------------------------------
CPacketUseAbilityRequest::CPacketUseAbilityRequest(const int &index)
: CPacket(6)
{
	OAFUN_DEBUG("");
	WriteUInt8(0xFC);
	WriteUInt16BE(6);
	WriteUInt16BE(OCT_USE_ABILITY);
	WriteUInt8(index);
}
//----------------------------------------------------------------------------------
