/***********************************************************************************
**
** TextDialog.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TEXTDIALOG_H
#define TEXTDIALOG_H
//----------------------------------------------------------------------------------
#include <QDialog>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TextDialog;
}
//----------------------------------------------------------------------------------
class TextDialog : public QDialog
{
	Q_OBJECT

private:
	Ui::TextDialog *ui;

public:
	explicit TextDialog(QWidget *parent = 0);
	~TextDialog();

	void ClearText();
	void SetText(const QString &text);
	void AddText(const QString &text);

	void SaveToFile(const QString &filePath);
};
//----------------------------------------------------------------------------------
extern TextDialog *g_TextDialog;
//----------------------------------------------------------------------------------
#endif // TEXTDIALOG_H
//----------------------------------------------------------------------------------
