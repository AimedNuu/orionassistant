// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabFiltersReplaces.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabfiltersreplaces.h"
#include "ui_tabfiltersreplaces.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include "tabmain.h"
#include "../GUI/filterreplacelistitem.h"
#include "tablistsfriends.h"
#include "tablistsenemies.h"
#include "Packets.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
//----------------------------------------------------------------------------------
TabFiltersReplaces *g_TabFiltersReplaces= nullptr;
//----------------------------------------------------------------------------------
TabFiltersReplaces::TabFiltersReplaces(QWidget *parent)
: QWidget(parent), ui(new Ui::TabFiltersReplaces)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabFiltersReplaces= this;

	connect(ui->lw_FiltersReplaceList, SIGNAL(itemDropped()), this, SLOT(SaveTextReplaces()));
}
//----------------------------------------------------------------------------------
TabFiltersReplaces::~TabFiltersReplaces()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabFiltersReplaces= nullptr;
}
//----------------------------------------------------------------------------------
void TabFiltersReplaces::on_lw_FiltersReplaceList_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f249");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	CFilterReplaceListItem *item = (CFilterReplaceListItem*)ui->lw_FiltersReplaceList->item(index.row());

	if (item != nullptr)
	{
		ui->le_FilterReplaceOriginalText->setText(item->GetOriginalText());
		ui->le_FilterReplaceOriginalColor->setText(item->GetOriginalColor());
		ui->le_FilterReplaceReplaceText->setText(item->GetReplacedText());
		ui->le_FilterReplaceReplaceColor->setText(item->GetReplacedColor());
		ui->cb_FilterReplaceType->setCurrentIndex(item->GetType());
		ui->cb_FilterReplaceMode->setCurrentIndex(item->GetMode());
		ui->cb_FilterReplaceFullMatch->setChecked(item->GetFullMatch());
	}
}
//----------------------------------------------------------------------------------
void TabFiltersReplaces::on_pb_FilterReplaceAdd_clicked()
{
	OAFUN_DEBUG("c28_f250");
	QString originalText = ui->le_FilterReplaceOriginalText->text().toLower();
	QString replacedText = ui->le_FilterReplaceReplaceText->text().toLower();

	if (!originalText.length())
	{
		QMessageBox::critical(this, "Text is empty", "Enter the original text!");
		return;
	}
	else if (!replacedText.length())
	{
		QMessageBox::critical(this, "Text is empty", "Enter the replace text!");
		return;
	}

	IFOR(i, 0, ui->lw_FiltersReplaceList->count())
	{
		CFilterReplaceListItem *item = (CFilterReplaceListItem*)ui->lw_FiltersReplaceList->item(i);

		if (item != nullptr && item->GetOriginalText().toLower() == originalText && item->GetMode() == ui->cb_FilterReplaceMode->currentIndex())
		{
			QMessageBox::critical(this, "Text is already exists", "Original text is already exists!");
			return;
		}
	}

	new CFilterReplaceListItem(ui->le_FilterReplaceOriginalText->text(), ui->le_FilterReplaceOriginalColor->text(), ui->le_FilterReplaceReplaceText->text(), ui->le_FilterReplaceReplaceColor->text(), ui->cb_FilterReplaceType->currentIndex(), ui->cb_FilterReplaceMode->currentIndex(), ui->cb_FilterReplaceFullMatch->isChecked(), ui->lw_FiltersReplaceList);

	ui->lw_FiltersReplaceList->setCurrentRow(ui->lw_FiltersReplaceList->count() - 1);
	SaveTextReplaces();
}
//----------------------------------------------------------------------------------
void TabFiltersReplaces::on_pb_FilterReplaceSave_clicked()
{
	OAFUN_DEBUG("c28_f251");
	QString originalText = ui->le_FilterReplaceOriginalText->text().toLower();
	QString replacedText = ui->le_FilterReplaceReplaceText->text().toLower();

	if (!originalText.length())
	{
		QMessageBox::critical(this, "Text is empty", "Enter the original text!");
		return;
	}
	else if (!replacedText.length())
	{
		QMessageBox::critical(this, "Text is empty", "Enter the replace text!");
		return;
	}

	CFilterReplaceListItem *selected = (CFilterReplaceListItem*)ui->lw_FiltersReplaceList->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Text is not selected", "Text is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_FiltersReplaceList->count())
	{
		CFilterReplaceListItem *item = (CFilterReplaceListItem*)ui->lw_FiltersReplaceList->item(i);

		if (item != nullptr && item->GetOriginalText().toLower() == originalText && item->GetMode() == ui->cb_FilterReplaceMode->currentIndex())
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Text is already exists", "Original text is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->SetOriginalText(ui->le_FilterReplaceOriginalText->text());
	selected->SetOriginalColor(ui->le_FilterReplaceOriginalColor->text());
	selected->SetReplacedText(ui->le_FilterReplaceReplaceText->text());
	selected->SetReplacedColor(ui->le_FilterReplaceReplaceColor->text());
	selected->SetType(ui->cb_FilterReplaceType->currentIndex());
	selected->SetMode(ui->cb_FilterReplaceMode->currentIndex());
	selected->SetFullMatch(ui->cb_FilterReplaceFullMatch->isChecked());
	selected->UpdateText();
	SaveTextReplaces();
}
//----------------------------------------------------------------------------------
void TabFiltersReplaces::on_pb_FilterReplaceRemove_clicked()
{
	OAFUN_DEBUG("c28_f252");
	QListWidgetItem *item = ui->lw_FiltersReplaceList->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_FiltersReplaceList->takeItem(ui->lw_FiltersReplaceList->row(item));

		if (item != nullptr)
		{
			delete item;
			SaveTextReplaces();
		}
	}
}
//----------------------------------------------------------------------------------
void TabFiltersReplaces::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_FiltersReplaceList)
		on_pb_FilterReplaceRemove_clicked();
}
//----------------------------------------------------------------------------------
bool TabFiltersReplaces::CheckReplaceText(const QString &text, const uint &serial, const ushort &graphic, const uchar &messageType, const ushort &color, const ushort &font, const uint &lang, const bool &unicode)
{
	OAFUN_DEBUG("c28_f258");
	Q_UNUSED(font);
	if (g_TabMain->TextReplace())
	{
		int type = RTT_ASCII;

		if (unicode)
			type = RTT_UNICODE;

		IFOR(i, 0, ui->lw_FiltersReplaceList->count())
		{
			CFilterReplaceListItem *item = (CFilterReplaceListItem*)ui->lw_FiltersReplaceList->item(i);

			if (item != nullptr && (item->GetType() == RTT_ALL || item->GetType() == type))
			{
				ushort itemColor = COrionAssistant::TextToGraphic(item->GetOriginalColor());

				if (itemColor == 0xFFFF || itemColor == color)
				{
					bool found = false;

					if (item->GetFullMatch())
						found = (item->GetOriginalText() == text);
					else
						found = text.contains(item->GetOriginalText());

					if (!found)
						continue;

					bool canBeReplaced = true;

					//Full amtch

					if (item->GetMode() == RTM_FRIENDS_AND_YOU)
						canBeReplaced = (serial == g_PlayerSerial || g_TabListsFriends->InFriendList(serial));
					else if (item->GetMode() == RTM_ENEMIES)
						canBeReplaced = g_TabListsEnemies->InEnemyList(serial);
					else if (item->GetMode() == RTM_OTHERS)
						canBeReplaced = (serial != g_PlayerSerial && !g_TabListsFriends->InFriendList(serial) && !g_TabListsEnemies->InEnemyList(serial));

					if (canBeReplaced)
					{
						ushort itemColor = COrionAssistant::TextToGraphic(item->GetReplacedColor());

						if (itemColor == 0xFFFF)
							itemColor = color;

						if (unicode)
							CPacketToClientTextReplacePrintW(item->GetReplacedText().toStdWString(), serial, graphic, messageType, itemColor, 0, lang).SendClient();
						else
							CPacketToClientTextReplacePrintA(item->GetReplacedText().toStdString(), serial, graphic, messageType, itemColor, 3).SendClient();

						return true;
					}
				}
			}
		}
	}

	return false;
}
//----------------------------------------------------------------------------------
void TabFiltersReplaces::LoadTextReplaces()
{
	OAFUN_DEBUG("c28_f264");
	ui->lw_FiltersReplaceList->clear();

	QFile file(g_DllPath + "/GlobalConfig/TextReplaces.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;

		Q_UNUSED(version);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();
				}
				else if (reader.name() == "replace")
				{
					if (attributes.hasAttribute("originaltext") && attributes.hasAttribute("originalcolor")
							&& attributes.hasAttribute("replacedtext") && attributes.hasAttribute("replacedcolor")
							&& attributes.hasAttribute("type") && attributes.hasAttribute("mode") && attributes.hasAttribute("fullmatch"))
					{
						new CFilterReplaceListItem(attributes.value("originaltext").toString(), attributes.value("originalcolor").toString(), attributes.value("replacedtext").toString(), attributes.value("replacedcolor").toString(), attributes.value("type").toInt(), attributes.value("mode").toInt(), COrionAssistant::RawStringToBool(attributes.value("fullmatch").toString()), ui->lw_FiltersReplaceList);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabFiltersReplaces::SaveTextReplaces()
{
	OAFUN_DEBUG("c28_f263");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/TextReplaces.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_FiltersReplaceList->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");

		IFOR(i, 0, count)
		{
			CFilterReplaceListItem *item = (CFilterReplaceListItem*)ui->lw_FiltersReplaceList->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("replace");

				writter.writeAttribute("originaltext", item->GetOriginalText());
				writter.writeAttribute("originalcolor", item->GetOriginalColor());
				writter.writeAttribute("replacedtext", item->GetReplacedText());
				writter.writeAttribute("replacedcolor", item->GetReplacedColor());
				writter.writeAttribute("type", QString::number(item->GetType()));
				writter.writeAttribute("mode", QString::number(item->GetMode()));
				writter.writeAttribute("fullmatch", (item->GetFullMatch() ? "true" : "false"));

				writter.writeEndElement(); //replace
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
