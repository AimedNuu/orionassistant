/***********************************************************************************
**
** TabFiltersSound.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABFILTERSSOUND_H
#define TABFILTERSSOUND_H
//----------------------------------------------------------------------------------
#include <QWidget>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabFiltersSound;
}
//----------------------------------------------------------------------------------
class TabFiltersSound : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_FiltersSoundList_clicked(const QModelIndex &index);

	void on_pb_FilterSoundAdd_clicked();

	void on_pb_FilterSoundSave_clicked();

	void on_pb_FilterSoundRemove_clicked();

private:
	Ui::TabFiltersSound *ui;

public slots:
	void SaveSoundFilter();

public:
	explicit TabFiltersSound(QWidget *parent = 0);
	~TabFiltersSound();

	void OnDeleteKeyClick(QWidget *widget);

	bool CheckSoundInFilter(const ushort &index);

	void LoadSoundFilter();
};
//----------------------------------------------------------------------------------
extern TabFiltersSound *g_TabFiltersSound;
//----------------------------------------------------------------------------------
#endif // TABFILTERSSOUND_H
//----------------------------------------------------------------------------------
