/***********************************************************************************
**
** TabListsObjects.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABLISTSOBJECTS_H
#define TABLISTSOBJECTS_H
//----------------------------------------------------------------------------------
#include <QWidget>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabListsObjects;
}
//----------------------------------------------------------------------------------
class TabListsObjects : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_Objects_clicked(const QModelIndex &index);

	void on_pb_ObjectFromTarget_clicked();

	void on_pb_ObjectCreate_clicked();

	void on_pb_ObjectSave_clicked();

	void on_pb_ObjectRemove_clicked();

	void on_pb_ObjectsSetDressBag_clicked();

	void on_pb_ObjectsUnsetDressBag_clicked();

	void on_pb_ObjectDressBagFromTarget_clicked();

private:
	Ui::TabListsObjects *ui;

public slots:
	void OnListItemMoved();

public:
	explicit TabListsObjects(QWidget *parent = 0);
	~TabListsObjects();

	void LoadObjects(const QString &path);

	void SaveObjects(const QString &path);

	void OnDeleteKeyClick(QWidget *widget);

	uint SeekObject(const QString &text);

	void AddObject(QString name, const QString &object);

	void RemoveObject(QString name);

	void SetObjectFromTarget(const uint &serial);

	void SetDressBag(const uint &serial);

};
//----------------------------------------------------------------------------------
extern TabListsObjects *g_TabListsObjects;
//----------------------------------------------------------------------------------
#endif // TABLISTSOBJECTS_H
//----------------------------------------------------------------------------------
