// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabScripts.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabscripts.h"
#include "ui_tabscripts.h"
#include "../GUI/ComboboxDelegate.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include "scripteditordialog.h"
#include "../GameObjects/GameWorld.h"
#include <QFileDialog>
#include "../Managers/TextParser.h"
#include "hotkeysform.h"
//----------------------------------------------------------------------------------
TabScripts *g_TabScripts = nullptr;
//----------------------------------------------------------------------------------
TabScripts::TabScripts(QWidget *parent)
: QWidget(parent), ui(new Ui::TabScripts)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabScripts = this;
}
//----------------------------------------------------------------------------------
TabScripts::~TabScripts()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabScripts = nullptr;
}
//----------------------------------------------------------------------------------
void TabScripts::Init()
{
	OAFUN_DEBUG("");
	connect(this, SIGNAL(signal_RemoveRunningScriptItem(CRunningScriptListItem*)), this, SLOT(slot_RemoveRunningScriptItem(CRunningScriptListItem*)));

	ui->cb_ScriptNameForStart->setItemDelegate(new CComboDelegate());

	LoadScript(g_DllPath + "/Autoload.oajs");

	UpdateScriptsList();
}
//----------------------------------------------------------------------------------
void TabScripts::on_lw_RunningScriptsList_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f164");
	CRunningScriptListItem *item = (CRunningScriptListItem*)ui->lw_RunningScriptsList->item(index.row());

	if (item != nullptr)
	{
		if (item->GetPaused())
			ui->pb_ScriptPauseOrResume->setIcon(QIcon(QPixmap(":/resource/images/macro_resume.png")));
		else
			ui->pb_ScriptPauseOrResume->setIcon(QIcon(QPixmap(":/resource/images/macro_pause.png")));
	}
}
//----------------------------------------------------------------------------------
void TabScripts::on_pb_ScriptStart_clicked()
{
	OAFUN_DEBUG("c28_f165");
	RunScript(ui->cb_ScriptNameForStart->currentText(), g_ScriptEditorDialog->GetText(), 0, 0, QStringList());
}
//----------------------------------------------------------------------------------
void TabScripts::on_pb_ScriptPauseOrResume_clicked()
{
	OAFUN_DEBUG("c28_f166");
	CRunningScriptListItem *selected = (CRunningScriptListItem*)ui->lw_RunningScriptsList->currentItem();

	if (selected != nullptr)
	{
		COAScriptHandle *engine = selected->m_ScriptHandle;

		selected->SetPaused(!engine->GetPaused());

		if (selected->GetPaused())
		{
			selected->setIcon(QIcon(QPixmap(":/resource/images/macro_pause.png")));
			ui->pb_ScriptPauseOrResume->setIcon(QIcon(QPixmap(":/resource/images/macro_resume.png")));
		}
		else
		{
			selected->setIcon(QIcon(QPixmap(":/resource/images/macro_start.png")));
			ui->pb_ScriptPauseOrResume->setIcon(QIcon(QPixmap(":/resource/images/macro_pause.png")));
		}
	}
}
//----------------------------------------------------------------------------------
void TabScripts::on_pb_ScriptStop_clicked()
{
	OAFUN_DEBUG("c28_f167");
	CRunningScriptListItem *selected = (CRunningScriptListItem*)ui->lw_RunningScriptsList->currentItem();

	if (selected != nullptr)
		selected->Terminate();
}
//----------------------------------------------------------------------------------
void TabScripts::on_tb_SelectScriptFilePath_clicked()
{
	OAFUN_DEBUG("c28_f160");
	QString path = OpenScriptFileDialog();

	if (path.length())
		LoadScript(path);
}
//----------------------------------------------------------------------------------
void TabScripts::on_tb_ReloadScriptFile_clicked()
{
	OAFUN_DEBUG("c28_f260");
	LoadScript(ui->le_ScriptFilePath->text());
}
//----------------------------------------------------------------------------------
void TabScripts::on_pb_ScriptOpenEditor_clicked()
{
	OAFUN_DEBUG("c28_f162");
	if (!g_ScriptEditorDialog->isVisible())
		g_ScriptEditorDialog->show();
	else
		g_ScriptEditorDialog->activateWindow();

	QString text = ui->cb_ScriptNameForStart->currentText();

	if (text.length())
		g_ScriptEditorDialog->GoLineText("function " + text + "()");
}
//----------------------------------------------------------------------------------
void TabScripts::on_clb_ScriptExamples_clicked()
{
	OAFUN_DEBUG("c28_f197");
	ShellExecute(0, L"Open", L"http://forum.orion-client.online/index.php?forums/skripty.41", NULL, NULL, SW_SHOWNORMAL);
}
//----------------------------------------------------------------------------------
void TabScripts::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_RunningScriptsList) //Running scripts list
		on_pb_ScriptStop_clicked();
}
//----------------------------------------------------------------------------------
void TabScripts::LoadConfig(const QXmlStreamAttributes &attributes)
{
	OAFUN_DEBUG("");
	if (attributes.hasAttribute("sortscripts"))
		ui->cb_SortScripts->setChecked(COrionAssistant::RawStringToBool(attributes.value("sortscripts").toString()));
}
//----------------------------------------------------------------------------------
void TabScripts::SaveConfig(QXmlStreamWriter &writter)
{
	OAFUN_DEBUG("");

	writter.writeStartElement("scripts");

	writter.writeAttribute("sortscripts", COrionAssistant::BoolToText(ui->cb_SortScripts->isChecked()));
	writter.writeAttribute("scriptfile", ui->le_ScriptFilePath->text());

	writter.writeEndElement(); //scripts
}
//----------------------------------------------------------------------------------
CRunningScriptListItem *TabScripts::RunScript(const QString &functionName, const QString &scriptBody, const uint &key, const uchar &modifiers, const QStringList &args)
{
	OAFUN_DEBUG("c28_f144");
	CRunningScriptListItem * script = new CRunningScriptListItem(functionName, args, key, modifiers, ui->lw_RunningScriptsList);

	ui->lw_RunningScriptsList->setCurrentRow(ui->lw_RunningScriptsList->count() - 1);

	if (script->GetPaused())
		ui->pb_ScriptPauseOrResume->setIcon(QIcon(QPixmap(":/resource/images/macro_resume.png")));
	else
		ui->pb_ScriptPauseOrResume->setIcon(QIcon(QPixmap(":/resource/images/macro_pause.png")));

	script->Start(scriptBody);

	return script;
}
//----------------------------------------------------------------------------------
void TabScripts::TerminateScript(const QString &functionName, const QString &functionsSave)
{
	OAFUN_DEBUG("c28_f145");
	if (functionName.toLower().trimmed() == "all")
	{
		CTextParser parser(functionsSave.toStdString(), "|", "", "");
		QStringList list = parser.ReadTokens();

		DFOR(i, ui->lw_RunningScriptsList->count() - 1, 0)
		{
			CRunningScriptListItem *item = (CRunningScriptListItem*)ui->lw_RunningScriptsList->item(i);

			if (item != nullptr)
			{
				QString name = item->text();
				bool canTerminate = true;

				foreach (const QString &str, list)
				{
					if (name == str)
					{
						canTerminate = false;
						break;
					}
				}

				if (canTerminate)
					item->Terminate();
			}
		}

		return;
	}

	DFOR(i, ui->lw_RunningScriptsList->count() - 1, 0)
	{
		CRunningScriptListItem *item = (CRunningScriptListItem*)ui->lw_RunningScriptsList->item(i);

		if (item != nullptr && item->text() == functionName)
			item->Terminate();
	}
}
//----------------------------------------------------------------------------------
bool TabScripts::ScriptRunning(const QString &functionName)
{
	OAFUN_DEBUG("c28_f146");
	DFOR(i, ui->lw_RunningScriptsList->count() - 1, 0)
	{
		CRunningScriptListItem *item = (CRunningScriptListItem*)ui->lw_RunningScriptsList->item(i);

		if (item != nullptr && item->text() == functionName)
			return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
bool TabScripts::ScriptRunning(const uint &key, const uchar &modifiers)
{
	OAFUN_DEBUG("c28_f146_1");
	DFOR(i, ui->lw_RunningScriptsList->count() - 1, 0)
	{
		CRunningScriptListItem *item = (CRunningScriptListItem*)ui->lw_RunningScriptsList->item(i);

		if (item != nullptr && item->GetKey() == key && item->GetModifiers() == modifiers)
			return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
void TabScripts::slot_RemoveRunningScriptItem(CRunningScriptListItem *item)
{
	OAFUN_DEBUG("c28_f163");
	if (item != nullptr)
	{
		item = (CRunningScriptListItem*)ui->lw_RunningScriptsList->takeItem(ui->lw_RunningScriptsList->row(item));

		if (item != nullptr)
			delete item;
	}
}
//----------------------------------------------------------------------------------
QString TabScripts::OpenScriptFileDialog()
{
	OAFUN_DEBUG("c28_f158");
	QString startPath = ui->le_ScriptFilePath->text();

	if (!startPath.length())
		startPath = g_DllPath;

	return QFileDialog::getOpenFileName(nullptr, tr("Select script file"), startPath, tr("Orion assistant script(*.oajs)"));
}
//----------------------------------------------------------------------------------
QString TabScripts::SaveScriptFileDialog()
{
	OAFUN_DEBUG("c28_f159");
	QString startPath = ui->le_ScriptFilePath->text();

	if (!startPath.length())
		startPath = g_DllPath;

	return QFileDialog::getSaveFileName(nullptr, tr("Select script file"), startPath, tr("Orion assistant script(*.oajs)"));
}
//----------------------------------------------------------------------------------
void TabScripts::LoadScript(const QString &filePath)
{
	OAFUN_DEBUG("c28_f161");

	if (QFile::exists(filePath))
	{
		ui->le_ScriptFilePath->setText(filePath);

		g_ScriptEditorDialog->LoadScript(filePath);
		UpdateScriptsList();
	}
}
//----------------------------------------------------------------------------------
void TabScripts::SetScriptPath(const QString &path)
{
	OAFUN_DEBUG("c28_f168");
	ui->le_ScriptFilePath->setText(path);
}
//----------------------------------------------------------------------------------
bool TabScripts::SaveAsScript(const QString &path)
{
	OAFUN_DEBUG("c28_f169");
	ui->le_ScriptFilePath->setText(path);

	return SaveScript(false);
}
//----------------------------------------------------------------------------------
bool TabScripts::SaveScript(const bool &autoloadReplace)
{
	OAFUN_DEBUG("c28_f170");
	QString path = ui->le_ScriptFilePath->text();

	if (!path.length())
		path = g_DllPath + "/Autoload.oajs";

	QFile file(path);

	if (!file.exists() && autoloadReplace)
	{
		path = g_DllPath + "/Autoload.oajs";

		file.setFileName(path);

		if (!file.exists())
		{
			QMessageBox::critical(this, "Error save script", "File not found:\n" + path);

			return false;
		}

		ui->le_ScriptFilePath->setText(path);
	}

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream(&file) << g_ScriptEditorDialog->GetText();

		file.close();
	}
	else
		return false;

	UpdateScriptsList();

	return true;
}
//----------------------------------------------------------------------------------
void TabScripts::UpdateScriptsList()
{
	OAFUN_DEBUG("c28_f171");
	QString oldSelectedName = ui->cb_ScriptNameForStart->currentText();
	int oldSelectedIndex = ui->cb_ScriptNameForStart->currentIndex();

	ui->cb_ScriptNameForStart->clear();

	QStringList list = g_ScriptEditorDialog->GetFunctionList();
	int selectionIndex = -1;
	int index = 0;

	if (ui->cb_SortScripts->isChecked())
		qSort(list.begin(), list.end());

	if (!list.empty())
	{
		for (const QString &str : list)
		{
			ui->cb_ScriptNameForStart->addItem(str);

			if (str == oldSelectedName)
				selectionIndex = index;

			index++;
		}
	}

	if (selectionIndex == -1)
	{
		if (ui->cb_ScriptNameForStart->count())
			oldSelectedIndex = 0;

		selectionIndex = oldSelectedIndex;
	}

	ui->cb_ScriptNameForStart->setCurrentIndex(selectionIndex);

	g_HotkeysForm->UpdateActionList(HT_SCRIPT, list);
}
//----------------------------------------------------------------------------------
void TabScripts::on_cb_SortScripts_clicked()
{
	OAFUN_DEBUG("");
	UpdateScriptsList();
}
//----------------------------------------------------------------------------------
