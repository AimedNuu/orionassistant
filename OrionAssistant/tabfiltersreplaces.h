/***********************************************************************************
**
** TabFiltersReplaces.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABFILTERSREPLACES_H
#define TABFILTERSREPLACES_H
//----------------------------------------------------------------------------------
#include <QWidget>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabFiltersReplaces;
}
//----------------------------------------------------------------------------------
class TabFiltersReplaces : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_FiltersReplaceList_clicked(const QModelIndex &index);

	void on_pb_FilterReplaceAdd_clicked();

	void on_pb_FilterReplaceSave_clicked();

	void on_pb_FilterReplaceRemove_clicked();

private:
	Ui::TabFiltersReplaces *ui;

public slots:
	void SaveTextReplaces();

public:
	explicit TabFiltersReplaces(QWidget *parent = 0);
	~TabFiltersReplaces();

	void OnDeleteKeyClick(QWidget *widget);

	bool CheckReplaceText(const QString &text, const uint &serial, const ushort &graphic, const uchar &messageType, const ushort &color, const ushort &font, const uint &lang, const bool &unicode);

	void LoadTextReplaces();
};
//----------------------------------------------------------------------------------
extern TabFiltersReplaces *g_TabFiltersReplaces;
//----------------------------------------------------------------------------------
#endif // TABFILTERSREPLACES_H
//----------------------------------------------------------------------------------
