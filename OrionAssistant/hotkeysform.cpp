// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** HotkeysForm.cpp
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "hotkeysform.h"
#include "ui_hotkeysform.h"
#include "../GUI/hotkeytableitem.h"
#include "../Managers/spellmanager.h"
#include "../CommonItems/skill.h"
#include "hotkeysform.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include "tabhotkeys.h"
#include "tabscripts.h"
#include "scripteditordialog.h"
#include "../GameObjects/GameWorld.h"
#include "tabmacros.h"
#include "tabmain.h"
#include "orionmap.h"
#include "Target.h"
#include "../Managers/spellmanager.h"
#include "../Managers/skillmanager.h"
#include "../GUI/ComboboxDelegate.h"
#include "Packets.h"
#include "../Managers/commandmanager.h"
//----------------------------------------------------------------------------------
HotkeysForm *g_HotkeysForm = nullptr;
//----------------------------------------------------------------------------------
HotkeysForm::HotkeysForm(QWidget *parent)
: QMainWindow(parent), ui(new Ui::HotkeysForm)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);

	g_HotkeysForm = this;

	setWindowIcon(QPixmap(":/resource/images/hotkey.png"));

	ui->cb_Action->setItemDelegate(new CComboDelegate());

	QHeaderView *headerView = ui->tw_Hotkeys->horizontalHeader();

	if (headerView != nullptr)
	{
		headerView->resizeSection(0, 197);
		headerView->resizeSection(1, 197);
	}

	m_ButtonsMap.insert(ui->pb_Esc,			VKYEBOARD_KEYS_TABLE(27, 0));
	m_ButtonsMap.insert(ui->pb_F1,			VKYEBOARD_KEYS_TABLE(112, 0));
	m_ButtonsMap.insert(ui->pb_F2,			VKYEBOARD_KEYS_TABLE(113, 0));
	m_ButtonsMap.insert(ui->pb_F3,			VKYEBOARD_KEYS_TABLE(114, 0));
	m_ButtonsMap.insert(ui->pb_F4,			VKYEBOARD_KEYS_TABLE(115, 0));
	m_ButtonsMap.insert(ui->pb_F5,			VKYEBOARD_KEYS_TABLE(116, 0));
	m_ButtonsMap.insert(ui->pb_F6,			VKYEBOARD_KEYS_TABLE(117, 0));
	m_ButtonsMap.insert(ui->pb_F7,			VKYEBOARD_KEYS_TABLE(118, 0));
	m_ButtonsMap.insert(ui->pb_F8,			VKYEBOARD_KEYS_TABLE(119, 0));
	m_ButtonsMap.insert(ui->pb_F9,			VKYEBOARD_KEYS_TABLE(120, 0));
	m_ButtonsMap.insert(ui->pb_F10,			VKYEBOARD_KEYS_TABLE(121, 0));
	m_ButtonsMap.insert(ui->pb_F11,			VKYEBOARD_KEYS_TABLE(122, 0));
	m_ButtonsMap.insert(ui->pb_F12,			VKYEBOARD_KEYS_TABLE(123, 0));
	//m_ButtonsMap.insert(ui->pb_Prt,		VKYEBOARD_KEYS_TABLE(0, 0));
	m_ButtonsMap.insert(ui->pb_SLock,		VKYEBOARD_KEYS_TABLE(145, 0));
	m_ButtonsMap.insert(ui->pb_Pause,		VKYEBOARD_KEYS_TABLE(19, 0));
	m_ButtonsMap.insert(ui->pb_Tilda,		VKYEBOARD_KEYS_TABLE(192, 0));
	m_ButtonsMap.insert(ui->pb_1,			VKYEBOARD_KEYS_TABLE(49, 0));
	m_ButtonsMap.insert(ui->pb_2,			VKYEBOARD_KEYS_TABLE(50, 0));
	m_ButtonsMap.insert(ui->pb_3,			VKYEBOARD_KEYS_TABLE(51, 0));
	m_ButtonsMap.insert(ui->pb_4,			VKYEBOARD_KEYS_TABLE(52, 0));
	m_ButtonsMap.insert(ui->pb_5,			VKYEBOARD_KEYS_TABLE(53, 0));
	m_ButtonsMap.insert(ui->pb_6,			VKYEBOARD_KEYS_TABLE(54, 0));
	m_ButtonsMap.insert(ui->pb_7,			VKYEBOARD_KEYS_TABLE(55, 0));
	m_ButtonsMap.insert(ui->pb_8,			VKYEBOARD_KEYS_TABLE(56, 0));
	m_ButtonsMap.insert(ui->pb_9,			VKYEBOARD_KEYS_TABLE(57, 0));
	m_ButtonsMap.insert(ui->pb_0,			VKYEBOARD_KEYS_TABLE(48, 0));
	m_ButtonsMap.insert(ui->pb_Sub,			VKYEBOARD_KEYS_TABLE(189, 0));
	m_ButtonsMap.insert(ui->pb_Result,		VKYEBOARD_KEYS_TABLE(187, 0));
	m_ButtonsMap.insert(ui->pb_Back,		VKYEBOARD_KEYS_TABLE(8, 0));
	m_ButtonsMap.insert(ui->pb_Ins,			VKYEBOARD_KEYS_TABLE(45, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_Home,		VKYEBOARD_KEYS_TABLE(36, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_PgUp,		VKYEBOARD_KEYS_TABLE(33, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_NumLock,		VKYEBOARD_KEYS_TABLE(144, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_NumDiv,		VKYEBOARD_KEYS_TABLE(111, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_NumMul,		VKYEBOARD_KEYS_TABLE(106, 0));
	m_ButtonsMap.insert(ui->pb_NumSub,		VKYEBOARD_KEYS_TABLE(109, 0));
	m_ButtonsMap.insert(ui->pb_Tab,			VKYEBOARD_KEYS_TABLE(9, 0));
	m_ButtonsMap.insert(ui->pb_q,			VKYEBOARD_KEYS_TABLE(81, 0));
	m_ButtonsMap.insert(ui->pb_w,			VKYEBOARD_KEYS_TABLE(87, 0));
	m_ButtonsMap.insert(ui->pb_e,			VKYEBOARD_KEYS_TABLE(69, 0));
	m_ButtonsMap.insert(ui->pb_r,			VKYEBOARD_KEYS_TABLE(82, 0));
	m_ButtonsMap.insert(ui->pb_t,			VKYEBOARD_KEYS_TABLE(84, 0));
	m_ButtonsMap.insert(ui->pb_y,			VKYEBOARD_KEYS_TABLE(89, 0));
	m_ButtonsMap.insert(ui->pb_u,			VKYEBOARD_KEYS_TABLE(85, 0));
	m_ButtonsMap.insert(ui->pb_i,			VKYEBOARD_KEYS_TABLE(73, 0));
	m_ButtonsMap.insert(ui->pb_o,			VKYEBOARD_KEYS_TABLE(79, 0));
	m_ButtonsMap.insert(ui->pb_p,			VKYEBOARD_KEYS_TABLE(80, 0));
	m_ButtonsMap.insert(ui->pb_BktL,		VKYEBOARD_KEYS_TABLE(219, 0));
	m_ButtonsMap.insert(ui->pb_BktR,		VKYEBOARD_KEYS_TABLE(221, 0));
	m_ButtonsMap.insert(ui->pb_Enter,		VKYEBOARD_KEYS_TABLE(13, 0));
	m_ButtonsMap.insert(ui->pb_Del,			VKYEBOARD_KEYS_TABLE(46, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_End,			VKYEBOARD_KEYS_TABLE(35, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_PgDn,		VKYEBOARD_KEYS_TABLE(34, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_Num7,		VKYEBOARD_KEYS_TABLE(103, 0));
	m_ButtonsMap.insert(ui->pb_Num8,		VKYEBOARD_KEYS_TABLE(104, 0));
	m_ButtonsMap.insert(ui->pb_Num9,		VKYEBOARD_KEYS_TABLE(105, 0));
	m_ButtonsMap.insert(ui->pb_NumAdd,		VKYEBOARD_KEYS_TABLE(107, 0));
	m_ButtonsMap.insert(ui->pb_Caps,		VKYEBOARD_KEYS_TABLE(20, 0));
	m_ButtonsMap.insert(ui->pb_a,			VKYEBOARD_KEYS_TABLE(65, 0));
	m_ButtonsMap.insert(ui->pb_s,			VKYEBOARD_KEYS_TABLE(83, 0));
	m_ButtonsMap.insert(ui->pb_d,			VKYEBOARD_KEYS_TABLE(68, 0));
	m_ButtonsMap.insert(ui->pb_f,			VKYEBOARD_KEYS_TABLE(70, 0));
	m_ButtonsMap.insert(ui->pb_g,			VKYEBOARD_KEYS_TABLE(71, 0));
	m_ButtonsMap.insert(ui->pb_h,			VKYEBOARD_KEYS_TABLE(72, 0));
	m_ButtonsMap.insert(ui->pb_j,			VKYEBOARD_KEYS_TABLE(74, 0));
	m_ButtonsMap.insert(ui->pb_k,			VKYEBOARD_KEYS_TABLE(75, 0));
	m_ButtonsMap.insert(ui->pb_l,			VKYEBOARD_KEYS_TABLE(76, 0));
	m_ButtonsMap.insert(ui->pb_Semicolon,	VKYEBOARD_KEYS_TABLE(186, 0));
	m_ButtonsMap.insert(ui->pb_Quote,		VKYEBOARD_KEYS_TABLE(222, 0));
	m_ButtonsMap.insert(ui->pb_Slash,		VKYEBOARD_KEYS_TABLE(220, 0));
	m_ButtonsMap.insert(ui->pb_Num4,		VKYEBOARD_KEYS_TABLE(100, 0));
	m_ButtonsMap.insert(ui->pb_Num5,		VKYEBOARD_KEYS_TABLE(101, 0));
	m_ButtonsMap.insert(ui->pb_Num6,		VKYEBOARD_KEYS_TABLE(102, 0));
	m_ButtonsMap.insert(ui->pb_z,			VKYEBOARD_KEYS_TABLE(90, 0));
	m_ButtonsMap.insert(ui->pb_x,			VKYEBOARD_KEYS_TABLE(88, 0));
	m_ButtonsMap.insert(ui->pb_c,			VKYEBOARD_KEYS_TABLE(67, 0));
	m_ButtonsMap.insert(ui->pb_v,			VKYEBOARD_KEYS_TABLE(86, 0));
	m_ButtonsMap.insert(ui->pb_b,			VKYEBOARD_KEYS_TABLE(66, 0));
	m_ButtonsMap.insert(ui->pb_n,			VKYEBOARD_KEYS_TABLE(78, 0));
	m_ButtonsMap.insert(ui->pb_m,			VKYEBOARD_KEYS_TABLE(77, 0));
	m_ButtonsMap.insert(ui->pb_Comma,		VKYEBOARD_KEYS_TABLE(188, 0));
	m_ButtonsMap.insert(ui->pb_Dot,			VKYEBOARD_KEYS_TABLE(190, 0));
	m_ButtonsMap.insert(ui->pb_Div,			VKYEBOARD_KEYS_TABLE(191, 0));
	m_ButtonsMap.insert(ui->pb_Up,			VKYEBOARD_KEYS_TABLE(38, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_Num1,		VKYEBOARD_KEYS_TABLE(97, 0));
	m_ButtonsMap.insert(ui->pb_Num2,		VKYEBOARD_KEYS_TABLE(98, 0));
	m_ButtonsMap.insert(ui->pb_Num3,		VKYEBOARD_KEYS_TABLE(99, 0));
	m_ButtonsMap.insert(ui->pb_NumEnter,	VKYEBOARD_KEYS_TABLE(13, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_WinL,		VKYEBOARD_KEYS_TABLE(91, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_Space,		VKYEBOARD_KEYS_TABLE(32, 0));
	m_ButtonsMap.insert(ui->pb_WinR,		VKYEBOARD_KEYS_TABLE(92, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_Apps,		VKYEBOARD_KEYS_TABLE(93, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_Left,		VKYEBOARD_KEYS_TABLE(37, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_Down,		VKYEBOARD_KEYS_TABLE(40, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_Right,		VKYEBOARD_KEYS_TABLE(39, HM_KEYPAD));
	m_ButtonsMap.insert(ui->pb_Num0,		VKYEBOARD_KEYS_TABLE(96, 0));
	m_ButtonsMap.insert(ui->pb_NumDot,		VKYEBOARD_KEYS_TABLE(110, 0));

	m_ButtonsMap.insert(ui->pb_MouseWheelUp,	VKYEBOARD_KEYS_TABLE(MOUSE_WHELL_SCROLL_UP, HM_MOUSE_WHELL));
	m_ButtonsMap.insert(ui->pb_MouseWheelPress,	VKYEBOARD_KEYS_TABLE(MOUSE_WHELL_PRESS, HM_MOUSE_WHELL));
	m_ButtonsMap.insert(ui->pb_MouseWheelDown,	VKYEBOARD_KEYS_TABLE(MOUSE_WHELL_SCROLL_DOWN, HM_MOUSE_WHELL));
	m_ButtonsMap.insert(ui->pb_MouseX1,			VKYEBOARD_KEYS_TABLE(MOUSE_X_UP, HM_MOUSE_X));
	m_ButtonsMap.insert(ui->pb_MouseX2,			VKYEBOARD_KEYS_TABLE(MOUSE_X_DOWN, HM_MOUSE_X));

	const QList<QColoredPushButton*> &list = m_ButtonsMap.keys();

	for (QColoredPushButton *button : list)
		connect(button, SIGNAL(clicked()), this, SLOT(on_KeyButtonClicked()));

	connect(ui->pb_ShiftL, SIGNAL(clicked()), this, SLOT(on_ModifierKeyButtonClicked()));
	connect(ui->pb_ShiftR, SIGNAL(clicked()), this, SLOT(on_ModifierKeyButtonClicked()));
	connect(ui->pb_CtrlL, SIGNAL(clicked()), this, SLOT(on_ModifierKeyButtonClicked()));
	connect(ui->pb_CtrlR, SIGNAL(clicked()), this, SLOT(on_ModifierKeyButtonClicked()));
	connect(ui->pb_Alt, SIGNAL(clicked()), this, SLOT(on_ModifierKeyButtonClicked()));
	connect(ui->pb_AltGr, SIGNAL(clicked()), this, SLOT(on_ModifierKeyButtonClicked()));

	connect(&m_Timer, SIGNAL(timeout()), this, SLOT(on_UpdateTimer()));
	m_Timer.start(300);

	IFOR(i, 0, HT_COUNT_OF_ITEMS)
			ui->cb_Type->addItem(m_HotkeyTypeName[i].Name);

	m_QuestionMessageBox.setText(tr("Hotkey with this combination already exists!\nWhat needs to be done?"));
	m_QuestionMessageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
	m_QuestionMessageBox.setDefaultButton(QMessageBox::Yes);

	QAbstractButton *abstractButton = m_QuestionMessageBox.button(QMessageBox::Yes);

	if (abstractButton != nullptr)
		abstractButton->setText(tr("Replase hotkey"));

	abstractButton = m_QuestionMessageBox.button(QMessageBox::No);

	if (abstractButton != nullptr)
		abstractButton->setText(tr("Save equal keys"));

	abstractButton = m_QuestionMessageBox.button(QMessageBox::Cancel);

	if (abstractButton != nullptr)
		abstractButton->setText(tr("Select existent hotkey"));

	on_cb_Type_currentIndexChanged(HT_PLAY_MACRO);
}
//----------------------------------------------------------------------------------
HotkeysForm::~HotkeysForm()
{
	OAFUN_DEBUG("");
	m_Timer.stop();

	const QList<QColoredPushButton*> &list = m_ButtonsMap.keys();

	for (QColoredPushButton *button : list)
		disconnect(button, SIGNAL(clicked()));

	m_ButtonsMap.clear();

	disconnect(ui->pb_ShiftL, SIGNAL(clicked()));
	disconnect(ui->pb_ShiftR, SIGNAL(clicked()));
	disconnect(ui->pb_CtrlL, SIGNAL(clicked()));
	disconnect(ui->pb_CtrlR, SIGNAL(clicked()));
	disconnect(ui->pb_Alt, SIGNAL(clicked()));
	disconnect(ui->pb_AltGr, SIGNAL(clicked()));

	delete ui;
	g_HotkeysForm = nullptr;
}
//----------------------------------------------------------------------------------
void HotkeysForm::Init()
{
	OAFUN_DEBUG("");
	connect(&m_Timer, SIGNAL(timeout()), this, SLOT(on_UpdateTimer()));
	m_Timer.start(300);

	m_HotkeyTypeName[HT_TOGGLE_HOTKEYS].Content = QStringList("Toggle") << "On" << "Off";
	m_HotkeyTypeName[HT_TOGGLE_MAP].Content = QStringList("Toggle") << "Show" << "Hide";
	m_HotkeyTypeName[HT_ATTACK].Content = QStringList("Last attack") << "Last target" << "Enemy";
	m_HotkeyTypeName[HT_USE_OBJECT].Content = QStringList("Last object") << "Left hand" << "Right hand";
	m_HotkeyTypeName[HT_ALLNAMES].Content = QStringList("All") << "Mobiles" << "Humanoids" << "Corpses";
	m_HotkeyTypeName[HT_TARGETING].Content = QStringList("Self") << "Last target" << "Last attack" << "Enemy" << "Friend" << "Last tile";
	m_HotkeyTypeName[HT_BANDAGE_HEALING].Content = QStringList("Self") << "Friend" << "Last target" << "New target system";
	m_HotkeyTypeName[HT_NEW_TARGETING_SYSTEM_SET].Content = QStringList("Self") << "Last target" << "Last attack" << "Enemy" << "Friend" << "Find next hostile" << "Find preveous hostile" << "Find nearest hostile" << "Find next mobile" << "Find preveous mobile" << "Find nearest mobile";

	on_cb_Type_currentIndexChanged(HT_PLAY_MACRO);
}
//----------------------------------------------------------------------------------
bool HotkeysForm::Load(const QString &path)
{
	OAFUN_DEBUG("");
	ui->tw_Hotkeys->setRowCount(0);

	QFile file(path);

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;
		int count = 0;

		Q_UNUSED(version);
		Q_UNUSED(count);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("size"))
						count = attributes.value("size").toInt();
				}
				else if (version == 0)
				{
					if (reader.name() == "hotkey")
					{
						if (attributes.hasAttribute("onescriptrunning") && attributes.hasAttribute("key") && attributes.hasAttribute("modifiers") && attributes.hasAttribute("macro"))
						{
							QString externalCode = "";

							if (attributes.hasAttribute("externalcode"))
								externalCode = attributes.value("externalcode").toString();

							QString name = attributes.value("macro").toString();

							uint type = HT_SCRIPT;

							if (name == "ExternalCode")
							{
								type = HT_EXTERNAL_CODE;
								name = "";
							}

							int row = ui->tw_Hotkeys->rowCount();
							ui->tw_Hotkeys->setRowCount(row + 1);

							ui->tw_Hotkeys->setItem(row, 0, new CHotkeyTableItem(attributes.value("key").toUInt(), (uchar)attributes.value("modifiers").toUInt(), type, 0, name, externalCode, COrionAssistant::RawStringToBool(attributes.value("onescriptrunning").toString())));
							ui->tw_Hotkeys->setItem(row, 1, new QTableWidgetItem(m_DefaultBlankHotkeyData));

							ui->tw_Hotkeys->setCurrentCell(row, 0);
							on_tw_Hotkeys_clicked(ui->tw_Hotkeys->currentIndex());
						}
					}
					else if (reader.name() == "toggle_hotkeys" && attributes.hasAttribute("key") && attributes.hasAttribute("modifiers"))
					{
						int row = ui->tw_Hotkeys->rowCount();
						ui->tw_Hotkeys->setRowCount(row + 1);

						ui->tw_Hotkeys->setItem(row, 0, new CHotkeyTableItem(attributes.value("key").toUInt(), (uchar)attributes.value("modifiers").toUInt(), HT_TOGGLE_HOTKEYS, 0, "", "", false));
						ui->tw_Hotkeys->setItem(row, 1, new QTableWidgetItem(m_DefaultBlankHotkeyData));

						ui->tw_Hotkeys->setCurrentCell(row, 0);
						on_tw_Hotkeys_clicked(ui->tw_Hotkeys->currentIndex());
					}
				}
				else if (version == 1)
				{
					if (reader.name() == "hotkey")
					{
						if (attributes.hasAttribute("key") && attributes.hasAttribute("modifiers") && attributes.hasAttribute("type") && attributes.hasAttribute("action"))
						{
							bool runningMode = false;

							if (attributes.hasAttribute("running_mode"))
								runningMode = COrionAssistant::RawStringToBool(attributes.value("running_mode").toString());

							QString name = "";

							if (attributes.hasAttribute("name"))
								name = attributes.value("name").toString();

							QString externalCode = "";

							if (attributes.hasAttribute("external_code"))
								externalCode = attributes.value("external_code").toString();

							int row = ui->tw_Hotkeys->rowCount();
							ui->tw_Hotkeys->setRowCount(row + 1);

							ui->tw_Hotkeys->setItem(row, 0, new CHotkeyTableItem(attributes.value("key").toUInt(), (uchar)attributes.value("modifiers").toUInt(), attributes.value("type").toUInt(), attributes.value("action").toUInt(), name, externalCode, runningMode));
							ui->tw_Hotkeys->setItem(row, 1, new QTableWidgetItem(m_DefaultBlankHotkeyData));

							ui->tw_Hotkeys->setCurrentCell(row, 0);
							on_tw_Hotkeys_clicked(ui->tw_Hotkeys->currentIndex());
						}
					}
				}
			}

			reader.readNext();
		}

		file.close();

		return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
bool HotkeysForm::Save(const QString &path)
{
	OAFUN_DEBUG("");
	QFile file(path);

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->tw_Hotkeys->rowCount();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "1");
		writter.writeAttribute("size", QString::number(count + 1));

		IFOR(i, 0, count)
		{
			CHotkeyTableItem *item = (CHotkeyTableItem*)ui->tw_Hotkeys->item(i, 0);

			if (item != nullptr)
			{
				writter.writeStartElement("hotkey");

				writter.writeAttribute("type", QString::number(item->GetType()));
				writter.writeAttribute("action", QString::number(item->GetAction()));
				writter.writeAttribute("running_mode", COrionAssistant::BoolToText(item->GetRunningMode()));
				writter.writeAttribute("key", QString::number(item->GetKey()));
				writter.writeAttribute("modifiers", QString::number(item->GetModifiers()));
				writter.writeAttribute("name", item->GetScriptOrMacroName());
				writter.writeAttribute("external_code", item->GetExternalCode());

				writter.writeEndElement(); //hotkey
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();

		return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
void HotkeysForm::on_KeyButtonClicked()
{
	OAFUN_DEBUG("");
	QMap<QColoredPushButton*, VKYEBOARD_KEYS_TABLE>::iterator it = m_ButtonsMap.find((QColoredPushButton*)QApplication::focusWidget());

	if (it != m_ButtonsMap.end())
	{
		const VKYEBOARD_KEYS_TABLE &info = it.value();

		uchar modifiers = info.Modifiers;

		if (m_AltPressed || (GetAsyncKeyState(VK_MENU) & 0x80000000))
			modifiers |= HM_ALT;

		if (m_CtrlPressed || (GetAsyncKeyState(VK_CONTROL) & 0x80000000))
			modifiers |= HM_CTRL;

		if (m_ShiftPressed || (GetAsyncKeyState(VK_SHIFT) & 0x80000000))
			modifiers |= HM_SHIFT;

		ui->le_Hotkey->CreateKey(info.Key, modifiers);
	}
}
//----------------------------------------------------------------------------------
void HotkeysForm::on_ModifierKeyButtonClicked()
{
	OAFUN_DEBUG("");
	QColoredPushButton *focused = (QColoredPushButton*)QApplication::focusWidget();

	if (focused == ui->pb_ShiftL || focused == ui->pb_ShiftR)
		m_ShiftPressed = !m_ShiftPressed;
	else if (focused == ui->pb_CtrlL || focused == ui->pb_CtrlR)
		m_CtrlPressed = !m_CtrlPressed;
	else if (focused == ui->pb_Alt)
		m_AltPressed = !m_AltPressed;
	else if (focused == ui->pb_AltGr)
		m_AltPressed = !m_AltPressed;
	else
		return;

	UpdateKeyboard();
}
//----------------------------------------------------------------------------------
void HotkeysForm::on_UpdateTimer()
{
	OAFUN_DEBUG("");
	if (m_WantToSave)
	{
		QString path = g_OrionAssistantForm->GetSaveConfigPath();

		if (path.length())
			Save(path + "/Hotkeys.xml");

		m_WantToSave = false;
	}

	if (!isVisible())
		return;

	uchar modifiers = 0;

	if (m_AltPressed || (GetAsyncKeyState(VK_MENU) & 0x80000000))
		modifiers |= HM_ALT;

	if (m_CtrlPressed || (GetAsyncKeyState(VK_CONTROL) & 0x80000000))
		modifiers |= HM_CTRL;

	if (m_ShiftPressed || (GetAsyncKeyState(VK_SHIFT) & 0x80000000))
		modifiers |= HM_SHIFT;

	if (m_LastUpdateModifiers != modifiers)
		UpdateKeyboard();
}
//----------------------------------------------------------------------------------
void HotkeysForm::UpdateKeyboard()
{
	OAFUN_DEBUG("");
	m_LastUpdateModifiers = 0;

	const QColor modifierEnabledColor(200, 200, 200);
	const QColor hotkeyIsSetColor(183, 255, 183);
	const QColor oneMoreHotkeyIsSetColor(255, 183, 183);
	const QColor currentHotkeyColor(183, 183, 255);

	if (m_AltPressed || (GetAsyncKeyState(VK_MENU) & 0x80000000))
	{
		m_LastUpdateModifiers |= HM_ALT;

		ui->pb_Alt->ChangeColor(modifierEnabledColor);
		ui->pb_AltGr->ChangeColor(modifierEnabledColor);
	}
	else
	{
		ui->pb_Alt->ResetColor();
		ui->pb_AltGr->ResetColor();
	}

	if (m_CtrlPressed || (GetAsyncKeyState(VK_CONTROL) & 0x80000000))
	{
		m_LastUpdateModifiers |= HM_CTRL;

		ui->pb_CtrlL->ChangeColor(modifierEnabledColor);
		ui->pb_CtrlR->ChangeColor(modifierEnabledColor);
	}
	else
	{
		ui->pb_CtrlL->ResetColor();
		ui->pb_CtrlR->ResetColor();
	}

	if (m_ShiftPressed || (GetAsyncKeyState(VK_SHIFT) & 0x80000000))
	{
		m_LastUpdateModifiers |= HM_SHIFT;

		ui->pb_ShiftL->ChangeColor(modifierEnabledColor);
		ui->pb_ShiftR->ChangeColor(modifierEnabledColor);
	}
	else
	{
		ui->pb_ShiftL->ResetColor();
		ui->pb_ShiftR->ResetColor();
	}

	CHotkeyTableItem *hotkeyItem = nullptr;
	int row = ui->tw_Hotkeys->currentRow();
	int count = ui->tw_Hotkeys->rowCount();

	if (row >= 0 && row < count)
		hotkeyItem = (CHotkeyTableItem*)ui->tw_Hotkeys->item(row, 0);

	QList<CHotkeyTableItem*> hotkeysTable;
	uchar addMods = HM_ALT | HM_CTRL | HM_SHIFT;

	IFOR(i, 0, count)
	{
		CHotkeyTableItem *obj = (CHotkeyTableItem*)ui->tw_Hotkeys->item(i, 0);

		if (obj != nullptr && obj->GetKey())
		{
			uchar mods = obj->GetModifiers() & addMods;

			if (mods == m_LastUpdateModifiers)
				hotkeysTable.push_back(obj);
		}
	}

	const QList<QColoredPushButton*> &list = m_ButtonsMap.keys();

	QColoredPushButton *currentButton = nullptr;

	for (QMap<QColoredPushButton*, VKYEBOARD_KEYS_TABLE>::iterator it = m_ButtonsMap.begin(); it != m_ButtonsMap.end(); ++it)
	{
		QColoredPushButton *button = it.key();
		const VKYEBOARD_KEYS_TABLE &info = it.value();
		int found = 0;

		for (CHotkeyTableItem *hotkey : hotkeysTable)
		{
			if (hotkey->GetKey() == info.Key && (!info.Modifiers || (hotkey->GetModifiers() & info.Modifiers)))
			{
				found++;

				if (hotkey == hotkeyItem)
					currentButton = button;
			}
		}

		if (found)
		{
			if (found > 1)
				button->ChangeColor(oneMoreHotkeyIsSetColor);
			else
				button->ChangeColor(hotkeyIsSetColor);
		}
		else
			button->ResetColor();
	}

	if (currentButton != nullptr)
		currentButton->ChangeColor(currentHotkeyColor);
}
//----------------------------------------------------------------------------------
void HotkeysForm::UpdateActionList(const HOTKEY_TYPES &type, const QStringList &list)
{
	OAFUN_DEBUG("");
	m_HotkeyTypeName[type].Content = list;

	if (ui->cb_Type->currentIndex() == (int)type)
		on_cb_Type_currentIndexChanged(ui->cb_Type->currentIndex());
}
//----------------------------------------------------------------------------------
bool HotkeysForm::CheckExists(const uint &key, const uchar &modifiers)
{
	OAFUN_DEBUG("");
	if (!key || m_Loading)
		return true;

	bool result = true;
	int row = ui->tw_Hotkeys->currentRow();
	int count = ui->tw_Hotkeys->rowCount();

	if (row >= 0 && row < count)
	{
		CHotkeyTableItem *hotkeyItem = (CHotkeyTableItem*)ui->tw_Hotkeys->item(row, 0);

		if (hotkeyItem != nullptr)
		{
			int boxAnswer = -1;

			IFOR(i, 0, count)
			{
				CHotkeyTableItem *obj = (CHotkeyTableItem*)ui->tw_Hotkeys->item(i, 0);

				if (obj == nullptr || obj == hotkeyItem)
					continue;

				if (obj->GetKey() == key && obj->GetModifiers() == modifiers)
				{
					boxAnswer = m_QuestionMessageBox.exec();

					if (boxAnswer == QMessageBox::Yes || boxAnswer == QMessageBox::Cancel)
					{
						IFOR(j, 0, count)
						{
							obj = (CHotkeyTableItem*)ui->tw_Hotkeys->item(j, 0);

							if (obj == nullptr || obj == hotkeyItem)
								continue;

							if (obj->GetKey() == key && obj->GetModifiers() == modifiers)
							{
								if (boxAnswer == QMessageBox::Cancel)
								{
									bool oldLoading = m_Loading;
									m_Loading = true;

									ui->tw_Hotkeys->setCurrentCell(i, 0);
									on_tw_Hotkeys_clicked(ui->tw_Hotkeys->currentIndex());

									m_Loading = oldLoading;
									result = false;

									break;
								}
								else
								{
									obj->SetKey(0);
									obj->SetModifiers(0);
									obj->setCheckState(Qt::Unchecked);
									UpdateHotkeyData(j);
								}
							}
						}
					}

					break;
				}
			}

			if (boxAnswer != QMessageBox::Cancel)
			{
				hotkeyItem->SetKey(key);
				hotkeyItem->SetModifiers(modifiers);

				if (key != 0)
					hotkeyItem->setCheckState(Qt::Checked);
				else
					hotkeyItem->setCheckState(Qt::Unchecked);

				UpdateHotkeyData(row);
				m_WantToSave = true;
			}
		}
	}

	if (result)
		UpdateKeyboard();

	return result;
}
//----------------------------------------------------------------------------------
void HotkeysForm::on_cb_Type_currentIndexChanged(int index)
{
	OAFUN_DEBUG("");
	bool oldLoading = m_Loading;
	m_Loading = true;
	ui->cb_Action->clear();

	if (index >= 0 && index < HT_COUNT_OF_ITEMS && !m_HotkeyTypeName[index].Content.empty())
		ui->cb_Action->addItems(m_HotkeyTypeName[index].Content);
	m_Loading = oldLoading;

	if (index == HT_SCRIPT || index == HT_EXTERNAL_CODE)
		ui->cb_RunMode->setText("Run one script");
	else
		ui->cb_RunMode->setText("Do not auto iterrupt\nplayed macro");

	int row = ui->tw_Hotkeys->currentRow();

	if (!m_Loading && row >= 0 && row < ui->tw_Hotkeys->rowCount())
	{
		CHotkeyTableItem *hotkeyItem = (CHotkeyTableItem*)ui->tw_Hotkeys->item(row, 0);

		if (hotkeyItem != nullptr)
		{
			int lastType = hotkeyItem->GetType();
			hotkeyItem->SetType(index);

			if ((lastType >= HT_CAST && lastType <= HT_CAST_FRIEND && index >= HT_CAST && index <= HT_CAST_FRIEND) ||
				(lastType >= HT_USE_SKILL && lastType <= HT_USE_SKILL_FREIND && index >= HT_USE_SKILL && index <= HT_USE_SKILL_FREIND))
			{
				ui->cb_Action->setCurrentIndex(hotkeyItem->GetAction());
			}
			else if (index == HT_PLAY_MACRO || index == HT_SCRIPT)
				ui->cb_Action->setCurrentIndex(ui->cb_Action->findText(hotkeyItem->GetScriptOrMacroName()));
			else
				ui->cb_Action->setCurrentIndex(0);

			on_cb_Action_currentIndexChanged(ui->cb_Action->currentIndex());
			m_WantToSave = true;
		}
	}
	else if (ui->cb_Action->count())
		ui->cb_Action->setCurrentIndex(0);
}
//----------------------------------------------------------------------------------
void HotkeysForm::on_cb_Action_currentIndexChanged(int index)
{
	OAFUN_DEBUG("");
	int row = ui->tw_Hotkeys->currentRow();

	if (!m_Loading && row >= 0 && row < ui->tw_Hotkeys->rowCount())
	{
		CHotkeyTableItem *hotkeyItem = (CHotkeyTableItem*)ui->tw_Hotkeys->item(row, 0);

		if (hotkeyItem != nullptr)
		{
			if (hotkeyItem->GetType() == HT_PLAY_MACRO || hotkeyItem->GetType() == HT_SCRIPT)
				hotkeyItem->SetScriptOrMacroName(ui->cb_Action->itemText(index));
			else
				hotkeyItem->SetAction(index);

			UpdateHotkeyData(row);
			m_WantToSave = true;
		}
	}
}
//----------------------------------------------------------------------------------
void HotkeysForm::UpdateHotkeyData(const int &row)
{
	OAFUN_DEBUG("");
	CHotkeyTableItem *hotkeyItem = (CHotkeyTableItem*)ui->tw_Hotkeys->item(row, 0);

	if (hotkeyItem == nullptr)
		return;

	hotkeyItem->setText(QHotkeyBox::GetHotkeyText(hotkeyItem->GetKey(), hotkeyItem->GetModifiers()));

	QTableWidgetItem *hotkeyData = ui->tw_Hotkeys->item(row, 1);

	if (hotkeyData == nullptr)
		return;

	QString name = "";
	int type = hotkeyItem->GetType();
	int action = hotkeyItem->GetAction();

	switch (type)
	{
		case HT_PLAY_MACRO:
		{
			if (hotkeyItem->GetScriptOrMacroName().length())
				name = "Macro: " + hotkeyItem->GetScriptOrMacroName();

			break;
		}
		case HT_STOP_MACRO:
		{
			name = "Stop macro";
			break;
		}
		case HT_SCRIPT:
		{
			if (hotkeyItem->GetScriptOrMacroName().length())
				name = "Script: " + hotkeyItem->GetScriptOrMacroName();

			break;
		}
		case HT_EXTERNAL_CODE:
		{
			QStringList sl = hotkeyItem->GetExternalCode().split('\n');

			bool first = true;

			name = "Code: ";

			for (QString str : sl)
			{
				str = str.trimmed();

				if (first && str.indexOf("//") == 0)
				{
					str = str.remove(0, 2);
					name += str;
					hotkeyItem->SetExternalName(str);
					break;
				}

				if (str.length())
					name += str + " ";

				first = false;
			}

			break;
		}
		case HT_TOGGLE_HOTKEYS:
		{
			if (action == 0)
				name = "Hotkeys: toggle";
			else if (action == 1)
				name = "Hotkeys: on";
			else
				name = "Hotkeys: off";

			break;
		}
		case HT_TOGGLE_MAP:
		{
			if (action == 0)
				name = "Orion map: toggle";
			else if (action == 1)
				name = "Orion map: show";
			else
				name = "Orion map: hide";

			break;
		}
		case HT_ATTACK:
		{
			if (action == 0)
				name = "Attack: last";
			else if (action == 1)
				name = "Attack: last target";
			else
				name = "Attack: enemy";

			break;
		}
		case HT_USE_OBJECT:
		{
			if (action == 0)
				name = "Use: last";
			else if (action == 1)
				name = "Use: left hand";
			else
				name = "Use: right hand";

			break;
		}
		case HT_ALLNAMES:
		{
			if (action == 0)
				name = "All names: all";
			else if (action == 1)
				name = "All names: mobiles";
			else if (action == 2)
				name = "All names: humanoids";
			else
				name = "All names: corpses";

			break;
		}
		case HT_CAST:
		case HT_CAST_SELF:
		case HT_CAST_LAST_TARGET:
		case HT_CAST_LAST_ATTACK:
		case HT_CAST_ENEMY:
		case HT_CAST_FRIEND:
		case HT_CAST_NEW_TARGET_SYSTEM:
		{
			switch (type)
			{
				case HT_CAST:
				{
					name = "Cast: ";
					break;
				}
				case HT_CAST_SELF:
				{
					name = "Cast self: ";
					break;
				}
				case HT_CAST_LAST_TARGET:
				{
					name = "Cast last target: ";
					break;
				}
				case HT_CAST_LAST_ATTACK:
				{
					name = "Cast last attack: ";
					break;
				}
				case HT_CAST_ENEMY:
				{
					name = "Cast enemy: ";
					break;
				}
				case HT_CAST_FRIEND:
				{
					name = "Cast friend: ";
					break;
				}
				case HT_CAST_NEW_TARGET_SYSTEM:
				{
					name = "Cast new target system: ";
					break;
				}
				default:
					break;
			}

			const QStringList &spellNames = m_HotkeyTypeName[HT_CAST].Content;

			if (action >= 0 && action < spellNames.size())
				name += spellNames.at(action);

			break;
		}
		case HT_USE_SKILL:
		case HT_USE_SKILL_SELF:
		case HT_USE_SKILL_LAST_TARGET:
		case HT_USE_SKILL_LAST_ATTACK:
		case HT_USE_SKILL_ENEMY:
		case HT_USE_SKILL_FREIND:
		case HT_USE_SKILL_NEW_TARGET_SYSTEM:
		{
			switch (type)
			{
				case HT_USE_SKILL:
				{
					name = "Use skill: ";
					break;
				}
				case HT_USE_SKILL_SELF:
				{
					name = "Use skill self: ";
					break;
				}
				case HT_USE_SKILL_LAST_TARGET:
				{
					name = "Use skill last target: ";
					break;
				}
				case HT_USE_SKILL_LAST_ATTACK:
				{
					name = "Use skill last attack: ";
					break;
				}
				case HT_USE_SKILL_ENEMY:
				{
					name = "Use skill enemy: ";
					break;
				}
				case HT_USE_SKILL_FREIND:
				{
					name = "Use skill friend: ";
					break;
				}
				case HT_USE_SKILL_NEW_TARGET_SYSTEM:
				{
					name = "Use skill new target system: ";
					break;
				}
				default:
					break;
			}

			const QStringList &skillNames = m_HotkeyTypeName[HT_USE_SKILL].Content;

			if (action >= 0 && action < skillNames.size())
				name += skillNames.at(action);

			break;
		}
		case HT_TARGETING:
		{
			if (action == 0)
				name = "Target: self";
			else if (action == 1)
				name = "Target: last target";
			else if (action == 2)
				name = "Target: last attack";
			else if (action == 3)
				name = "Target: enemy";
			else if (action == 4)
				name = "Target: friend";
			else
				name = "Target: last tile";

			break;
		}
		case HT_BANDAGE_HEALING:
		{
			if (action == 0)
				name = "BandageHealing: self";
			else if (action == 1)
				name = "BandageHealing: friend";
			else if (action == 2)
				name = "BandageHealing: last target";
			else if (action == 3)
				name = "BandageHealing: new target system";

			break;
		}
		case HT_NEW_TARGETING_SYSTEM_SET:
		{
			if (action == 0)
				name = "NewTargettingSystemSet: self";
			else if (action == 1)
				name = "NewTargettingSystemSet: last target";
			else if (action == 2)
				name = "NewTargettingSystemSet: last attack";
			else if (action == 3)
				name = "NewTargettingSystemSet: enemy";
			else if (action == 4)
				name = "NewTargettingSystemSet: friend";
			else if (action == 5)
				name = "NewTargettingSystemSet: Find next hostile";
			else if (action == 6)
				name = "NewTargettingSystemSet: Find preveous hostile";
			else if (action == 7)
				name = "NewTargettingSystemSet: Find nearest hostile";
			else if (action == 8)
				name = "NewTargettingSystemSet: Find next mobile";
			else if (action == 9)
				name = "NewTargettingSystemSet: Find preveous mobile";
			else if (action == 10)
				name = "NewTargettingSystemSet: Find nearest mobile";

			break;
		}
		default:
			break;
	}

	if (name.length())
		hotkeyData->setText(name);
	else
		hotkeyData->setText(m_DefaultBlankHotkeyData);
}
//----------------------------------------------------------------------------------
void HotkeysForm::on_pb_New_clicked()
{
	OAFUN_DEBUG("");
	int row = ui->tw_Hotkeys->rowCount();
	ui->tw_Hotkeys->setRowCount(row + 1);

	ui->tw_Hotkeys->setItem(row, 0, new CHotkeyTableItem());
	ui->tw_Hotkeys->setItem(row, 1, new QTableWidgetItem(m_DefaultBlankHotkeyData));

	ui->tw_Hotkeys->setCurrentCell(row, 0);
	on_tw_Hotkeys_clicked(ui->tw_Hotkeys->currentIndex());

	m_WantToSave = true;
}
//----------------------------------------------------------------------------------
void HotkeysForm::on_pb_Delete_clicked()
{
	OAFUN_DEBUG("");
	int row = ui->tw_Hotkeys->currentRow();

	if (row >= 0 && row < ui->tw_Hotkeys->rowCount())
	{
		ui->tw_Hotkeys->removeRow(row);
		m_WantToSave = true;
	}
}
//----------------------------------------------------------------------------------
void HotkeysForm::on_tw_Hotkeys_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("");
	CHotkeyTableItem *hotkeyItem = (CHotkeyTableItem*)ui->tw_Hotkeys->item(index.row(), 0);

	if (hotkeyItem == nullptr)
		return;

	int type = hotkeyItem->GetType();

	if (type < 0 || type >= HT_COUNT_OF_ITEMS)
		return;

	m_Loading = true;

	ui->le_Hotkey->CreateKey(hotkeyItem->GetKey(), hotkeyItem->GetModifiers());

	ui->cb_Type->setCurrentIndex(hotkeyItem->GetType());
	on_cb_Type_currentIndexChanged(hotkeyItem->GetType());

	int action = hotkeyItem->GetAction();

	if (type == HT_PLAY_MACRO || type == HT_SCRIPT)
	{
		action = -1;

		DFOR(i, ui->cb_Action->count() - 1, 0)
		{
			if (ui->cb_Action->itemText(i) == hotkeyItem->GetScriptOrMacroName())
			{
				action = i;
				break;
			}
		}
	}
	else if (action < 0 || action >= ui->cb_Action->count())
		action = -1;

	ui->cb_Action->setCurrentIndex(action);

	ui->cb_RunMode->setChecked(hotkeyItem->GetRunningMode());

	ui->pte_ExternalCode->setPlainText(hotkeyItem->GetExternalCode());

	m_Loading = false;

	UpdateHotkeyData(index.row());
	UpdateKeyboard();
}
//----------------------------------------------------------------------------------
void HotkeysForm::on_cb_RunMode_clicked()
{
	OAFUN_DEBUG("");
	int row = ui->tw_Hotkeys->currentRow();

	if (!m_Loading && row >= 0 && row < ui->tw_Hotkeys->rowCount())
	{
		CHotkeyTableItem *hotkeyItem = (CHotkeyTableItem*)ui->tw_Hotkeys->item(row, 0);

		if (hotkeyItem != nullptr)
		{
			hotkeyItem->SetRunningMode(ui->cb_RunMode->isChecked());

			m_WantToSave = true;
		}
	}
}
//----------------------------------------------------------------------------------
void HotkeysForm::on_pte_ExternalCode_textChanged()
{
	OAFUN_DEBUG("");
	int row = ui->tw_Hotkeys->currentRow();

	if (!m_Loading && row >= 0 && row < ui->tw_Hotkeys->rowCount())
	{
		CHotkeyTableItem *hotkeyItem = (CHotkeyTableItem*)ui->tw_Hotkeys->item(row, 0);

		if (hotkeyItem != nullptr)
		{
			hotkeyItem->SetExternalCode(ui->pte_ExternalCode->toPlainText());

			m_WantToSave = true;
		}
	}
}
//----------------------------------------------------------------------------------
bool HotkeysForm::HotkeyExistsAndUse(const uint &wParam, const uint &lParam, uint mod)
{
	OAFUN_DEBUG("");
	bool result = false;

	if (g_World != nullptr)
	{
		if (IsKeyDown(VK_MENU))
			mod |= HM_ALT;

		if (IsKeyDown(VK_CONTROL))
			mod |= HM_CTRL;

		if (IsKeyDown(VK_SHIFT))
			mod |= HM_SHIFT;

		if ((lParam & (1 << 24)) != 0)
			mod |= HM_KEYPAD;

		//qDebug("test: 0x%08X 0x%08X :: %08X\n", wParam, lParam, mod);

		int hotkeysCount = ui->tw_Hotkeys->rowCount();

		IFOR(i, 0, hotkeysCount)
		{
			CHotkeyTableItem *item = (CHotkeyTableItem*)ui->tw_Hotkeys->item(i, 0);

			if (item != nullptr && item->GetKey() == wParam && item->GetModifiers() == mod)
			{
				int type = item->GetType();
				int action = item->GetAction();

				if (type == HT_TOGGLE_HOTKEYS)
				{
					if (action == 0)
						g_TabHotkeys->SetOnOffHotkeys(!g_TabHotkeys->OnOffHotkeys());
					else
						g_TabHotkeys->SetOnOffHotkeys(action == 1);

					result = true;
				}
				else if (g_TabHotkeys->OnOffHotkeys())
				{
					if (type == HT_SCRIPT || type == HT_EXTERNAL_CODE)
					{
						QString scriptName = item->GetScriptOrMacroName();
						bool canRun = true;

						if (item->GetRunningMode())
						{
							if (type == HT_EXTERNAL_CODE)
								canRun = !g_TabScripts->ScriptRunning(item->GetKey(), item->GetModifiers());
							else
								canRun = !g_TabScripts->ScriptRunning(scriptName);
						}

						if (canRun)
						{
							if (type == HT_EXTERNAL_CODE)
							{
								CRunningScriptListItem *script = g_TabScripts->RunScript("ExternalCode", g_ScriptEditorDialog->GetText() + "\nfunction ExternalCode()\n{\n" + item->GetExternalCode() + "\n}", item->GetKey(), item->GetModifiers(), QStringList());

								if (item->GetExternalName().length())
									script->setText("Ext: " + item->GetExternalName());
							}
							else
								g_TabScripts->RunScript(scriptName, g_ScriptEditorDialog->GetText(), item->GetKey(), item->GetModifiers(), QStringList());
						}
					}
					else if (type == HT_PLAY_MACRO)
						g_TabMacros->Play(item->GetScriptOrMacroName(), item->GetRunningMode());
					else if (type == HT_STOP_MACRO)
						g_TabMacros->Stop();
					else
						PlayHotkeyAction(item->GetType(), item->GetAction());

					result = true;
				}

				break;
			}
		}

		if (result)
		{
			if (LOBYTE(wParam) == 0x73 && IsKeyDown(VK_MENU)) //F4 + mods (for close UO on Alt+F4)
				result = false;
			else
				result = !g_TabHotkeys->PassHotkeys();
		}
	}

	return result;
}
//----------------------------------------------------------------------------------
void HotkeysForm::PlayHotkeyAction(const int &type, int action)
{
	OAFUN_DEBUG("");
	switch (type)
	{
		case HT_TOGGLE_MAP:
		{
			if (action == 1 || (!action && (g_OrionMap == nullptr || !g_OrionMap->isVisible())))
				g_TabMain->OpenMap();
			else
				g_TabMain->CloseMap();

			break;
		}
		case HT_ATTACK:
		{
			uint serial = 0;

			if (action == 0)
				serial = COrionAssistant::TextToSerial("lastattack");
			else if (action == 1)
				serial = COrionAssistant::TextToSerial("lasttarget");
			else
				serial = COrionAssistant::TextToSerial("enemy");

			if (serial)
				g_OrionAssistant.Attack(serial);

			break;
		}
		case HT_USE_OBJECT:
		{
			uint serial = 0;

			if (action == 0)
				serial = COrionAssistant::TextToSerial("lastobject");
			else if (action == 1)
			{
				CGameItem *objAtLayer = g_Player->FindLayer(action == 1 ? OL_1_HAND : OL_2_HAND);

				if (objAtLayer != nullptr)
					serial = objAtLayer->GetSerial();
			}

			if (serial)
				g_OrionAssistant.DoubleClick(serial);

			break;
		}
		case HT_ALLNAMES:
		{
			bool showMobiles = (action == 0 || action == 1);
			bool showHumanoids = (action == 0 || action == 2);
			bool showCorpses = (action == 0 || action == 3);

			QFOR(obj, g_World->m_Items, CGameObject*)
			{
				if (obj->IsCorpse())
				{
					if (showCorpses)
						g_OrionAssistant.Click(obj->GetSerial());
				}
				else if (obj->GetNPC() && !obj->IsPlayer())
				{
					if (showMobiles || (showHumanoids && obj->IsHuman()))
						g_OrionAssistant.Click(obj->GetSerial());
				}
			}

			break;
		}
		case HT_CAST:
		case HT_CAST_SELF:
		case HT_CAST_LAST_TARGET:
		case HT_CAST_LAST_ATTACK:
		case HT_CAST_ENEMY:
		case HT_CAST_FRIEND:
		case HT_CAST_NEW_TARGET_SYSTEM:
		{
			const QStringList &spellNames = m_HotkeyTypeName[HT_CAST].Content;

			if (action >= 0 && action < spellNames.size())
			{
				if (type == HT_CAST_NEW_TARGET_SYSTEM)
					g_SpellManager.TargetCast(spellNames.at(action), GetInt(VKI_NEW_TARGET_SYSTEM_SERIAL));
				else
				{
					QStringList target;
					bool withTarget = true;

					if (type == HT_CAST_SELF)
						target << "self";
					else if (type == HT_CAST_LAST_TARGET)
						target << "lasttarget";
					else if (type == HT_CAST_LAST_ATTACK)
						target << "lastattack";
					else if (type == HT_CAST_ENEMY)
						target << "enemy";
					else if (type == HT_CAST_FRIEND)
						target << "friend";
					else
						withTarget = false;

					if (withTarget && target.empty())
						break;

					if (withTarget)
						g_Target.SetObjectsHook(target);

					g_SpellManager.Cast(spellNames.at(action));
				}
			}

			break;
		}
		case HT_USE_SKILL:
		case HT_USE_SKILL_SELF:
		case HT_USE_SKILL_LAST_TARGET:
		case HT_USE_SKILL_LAST_ATTACK:
		case HT_USE_SKILL_ENEMY:
		case HT_USE_SKILL_FREIND:
		case HT_USE_SKILL_NEW_TARGET_SYSTEM:
		{
			const QStringList &skillNames = m_HotkeyTypeName[HT_USE_SKILL].Content;

			if (action >= 0 && action < skillNames.size())
			{
				if (type == HT_USE_SKILL_NEW_TARGET_SYSTEM)
					g_SkillManager.TargetUse(skillNames.at(action), GetInt(VKI_NEW_TARGET_SYSTEM_SERIAL));
				else
				{
					QStringList target;
					bool withTarget = true;

					if (type == HT_USE_SKILL_SELF)
						target << "self";
					else if (type == HT_USE_SKILL_LAST_TARGET)
						target << "lasttarget";
					else if (type == HT_USE_SKILL_LAST_ATTACK)
						target << "lastattack";
					else if (type == HT_USE_SKILL_ENEMY)
						target << "enemy";
					else if (type == HT_USE_SKILL_FREIND)
						target << "friend";
					else
						withTarget = false;

					if (withTarget && target.empty())
						break;

					g_Target.SetObjectsHook(target);

					g_SkillManager.Use(skillNames.at(action));
				}
			}

			break;
		}
		case HT_TARGETING:
		{
			if (action == 0)
				g_Target.SendTargetObject(g_PlayerSerial);
			else if (action == 1)
				g_Target.SendTargetObject(g_LastTargetObject);
			else if (action == 2)
				g_Target.SendTargetObject(g_LastAttackObject);
			else if (action == 3)
				g_Target.SendTargetObject(COrionAssistant::TextToSerial("enemy"));
			else if (action == 4)
				g_Target.SendTargetObject(COrionAssistant::TextToSerial("friend"));
			else
				g_Target.SendTargetLastTile();

			CPacketCancelTarget().SendClient();

			break;
		}
		case HT_BANDAGE_HEALING:
		{
			CGameItem *bandage = g_Player->FindBandage();

			if (bandage != nullptr)
			{
				if (action == 3)
					CPacketTargetUseObject(bandage->GetSerial(), GetInt(VKI_NEW_TARGET_SYSTEM_SERIAL)).SendServer();
				else
				{
					if (action == 0)
						g_Target.SetObjectsHook(QStringList("self"));
					else if (action == 1)
						g_Target.SetObjectsHook(QStringList("friend"));
					else if (action == 2)
						g_Target.SetObjectsHook(QStringList("lasttarget"));

					g_OrionAssistant.DoubleClick(bandage->GetSerial());
				}
			}

			break;
		}
		case HT_NEW_TARGETING_SYSTEM_SET:
		{
			if (action == 0)
				g_SetInt(VKI_NEW_TARGET_SYSTEM_SERIAL, g_PlayerSerial);
			else if (action == 1)
				g_SetInt(VKI_NEW_TARGET_SYSTEM_SERIAL, g_LastTargetObject);
			else if (action == 2)
				g_SetInt(VKI_NEW_TARGET_SYSTEM_SERIAL, g_LastAttackObject);
			else if (action == 3)
				g_SetInt(VKI_NEW_TARGET_SYSTEM_SERIAL, COrionAssistant::TextToSerial("enemy"));
			else if (action == 4)
				g_SetInt(VKI_NEW_TARGET_SYSTEM_SERIAL, COrionAssistant::TextToSerial("friend"));
			else
			{
				QVector<string> clientMacroActions;
				QVector<string> clientMacroSubActions;

				if (action == 5)
				{
					clientMacroActions.push_back("SelectNext");
					clientMacroSubActions.push_back("Hostile");
				}
				else if (action == 6)
				{
					clientMacroActions.push_back("SelectPreveous");
					clientMacroSubActions.push_back("Hostile");
				}
				else if (action == 7)
				{
					clientMacroActions.push_back("SelectNearest");
					clientMacroSubActions.push_back("Hostile");
				}
				else if (action == 8)
				{
					clientMacroActions.push_back("SelectNext");
					clientMacroSubActions.push_back("Mobile");
				}
				else if (action == 9)
				{
					clientMacroActions.push_back("SelectPreveous");
					clientMacroSubActions.push_back("Mobile");
				}
				else if (action == 10)
				{
					clientMacroActions.push_back("SelectNearest");
					clientMacroSubActions.push_back("Mobile");
				}
				else
					break;

				CPacketPlayMacro(clientMacroActions, clientMacroSubActions).SendClient();
			}

			break;
		}
		default:
			break;
	}
}
//----------------------------------------------------------------------------------
HOTKEY_TYPE_TABLE HotkeysForm::m_HotkeyTypeName[HT_COUNT_OF_ITEMS] =
{
	{ QStringList(), "Play macro" },
	{ QStringList(), "Stop macro" },
	{ QStringList(), "Run script" },
	{ QStringList(), "External code" },
	{ QStringList(), "Toggle hotkeys" },
	{ QStringList(), "Toggle Orion map" },
	{ QStringList(), "Attack" },
	{ QStringList(), "Use object" },
	{ QStringList(), "All names" },
	{ QStringList(), "Cast spell" },
	{ QStringList(), "Cast spell on self" },
	{ QStringList(), "Cast spell on last target" },
	{ QStringList(), "Cast spell on last attack" },
	{ QStringList(), "Cast spell on enemy" },
	{ QStringList(), "Cast spell on friend" },
	{ QStringList(), "Cast spell on new target system" },
	{ QStringList(), "Use skill" },
	{ QStringList(), "Use skill on self" },
	{ QStringList(), "Use skill on last target" },
	{ QStringList(), "Use skill on last attack" },
	{ QStringList(), "Use skill on enemy" },
	{ QStringList(), "Use skill on friend" },
	{ QStringList(), "Use skill on new target system" },
	{ QStringList(), "Targeting" },
	{ QStringList(), "Bandage healing" },
	{ QStringList(), "New targeting system set" }
};
//----------------------------------------------------------------------------------
