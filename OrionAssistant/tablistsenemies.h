/***********************************************************************************
**
** TabListsEnemies.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABLISTSENEMIES_H
#define TABLISTSENEMIES_H
//----------------------------------------------------------------------------------
#include <QWidget>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabListsEnemies;
}
//----------------------------------------------------------------------------------
class TabListsEnemies : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_Enemies_clicked(const QModelIndex &index);

	void on_pb_EnemyCreate_clicked();

	void on_pb_EnemySave_clicked();

	void on_pb_EnemyRemove_clicked();

	void on_pb_EnemyFromTarget_clicked();

private:
	Ui::TabListsEnemies *ui;

public slots:
	void OnListItemMoved();

public:
	explicit TabListsEnemies(QWidget *parent = 0);
	~TabListsEnemies();

	void OnDeleteKeyClick(QWidget *widget);

	void LoadEnemies(const QString &path);

	void SaveEnemies(const QString &path);

	void SetEnemyObjectFromTarget(const uint &serial);

	bool InEnemyList(const uint &serial);

	QStringList GetEnemyList();

	void AddEnemy(QString name, const QString &object);

	void RemoveEnemy(QString name);

	void ClearEnemyList();
};
//----------------------------------------------------------------------------------
extern TabListsEnemies *g_TabListsEnemies;
//----------------------------------------------------------------------------------
#endif // TABLISTSENEMIES_H
//----------------------------------------------------------------------------------
