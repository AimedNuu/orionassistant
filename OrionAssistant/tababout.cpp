// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabAbout.cpp
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tababout.h"
#include "ui_tababout.h"
#include <QDesktopServices>
//----------------------------------------------------------------------------------
TabAbout::TabAbout(QWidget *parent)
: QWidget(parent), ui(new Ui::TabAbout)
{
	ui->setupUi(this);
}
//----------------------------------------------------------------------------------
TabAbout::~TabAbout()
{
	delete ui;
}
//----------------------------------------------------------------------------------
void TabAbout::on_tb_Support_anchorClicked(const QUrl &arg1)
{
	QDesktopServices::openUrl(arg1);
}
//----------------------------------------------------------------------------------
void TabAbout::on_pb_GitHub_clicked()
{
	QDesktopServices::openUrl(QUrl("https://github.com/Hotride/OrionUO"));
}
//----------------------------------------------------------------------------------
void TabAbout::on_pb_Forum_clicked()
{
	QDesktopServices::openUrl(QUrl("https://orion-client.online/index.php?forums/"));
}
//----------------------------------------------------------------------------------
void TabAbout::on_pb_Discord_clicked()
{
	QDesktopServices::openUrl(QUrl("https://discord.gg/UcVKWzB"));
}
//----------------------------------------------------------------------------------
void TabAbout::on_pb_WIKI_clicked()
{
	QDesktopServices::openUrl(QUrl("https://github.com/Hotride/OrionUO/wiki"));
}
//----------------------------------------------------------------------------------
