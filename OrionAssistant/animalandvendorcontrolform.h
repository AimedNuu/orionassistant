/***********************************************************************************
**
** AnimalAndVendorControlForm.h
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef ANIMALANDVENDORCONTROLFORM_H
#define ANIMALANDVENDORCONTROLFORM_H
//----------------------------------------------------------------------------------
#include <QDialog>
//----------------------------------------------------------------------------------
namespace Ui
{
	class AnimalAndVendorControlForm;
}
//----------------------------------------------------------------------------------
class AnimalAndVendorControlForm : public QDialog
{
	Q_OBJECT

private:
	Ui::AnimalAndVendorControlForm *ui;

	void Send(const QString &command);

private slots:
	void on_pb_Come_clicked();

	void on_pb_Go_clicked();

	void on_pb_Follow_clicked();

	void on_pb_Stay_clicked();

	void on_pb_Stop_clicked();

	void on_pb_Guard_clicked();

	void on_pb_Transfer_clicked();

	void on_pb_Release_clicked();

	void on_pb_Attack_clicked();

	void on_pb_Drop_clicked();

	void on_pb_Give_clicked();

	void on_pb_Kill_clicked();

	void on_pb_Buy_clicked();

	void on_pb_Bye_clicked();

	void on_pb_Stock_clicked();

	void on_pb_Sell_clicked();

	void on_pb_Hire_clicked();

	void on_pb_Price_clicked();

	void on_pb_Status_clicked();

	void on_pb_NameFromTarget_clicked();

public:
	explicit AnimalAndVendorControlForm(QWidget *parent = 0);
	~AnimalAndVendorControlForm();

	void SetName(const QString &name);
};
//----------------------------------------------------------------------------------
extern AnimalAndVendorControlForm *g_AnimalAndVendorControlForm;
//----------------------------------------------------------------------------------
#endif // ANIMALANDVENDORCONTROLFORM_H
//----------------------------------------------------------------------------------
