// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** HouseControlForm.cpp
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "housecontrolform.h"
#include "ui_housecontrolform.h"
#include "Packets.h"
//----------------------------------------------------------------------------------
HouseControlForm *g_HouseControlForm = nullptr;
//----------------------------------------------------------------------------------
HouseControlForm::HouseControlForm(QWidget *parent)
: QDialog(parent), ui(new Ui::HouseControlForm)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);

	setFixedSize(size());

	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	g_HouseControlForm = this;
}
//----------------------------------------------------------------------------------
HouseControlForm::~HouseControlForm()
{
	OAFUN_DEBUG("");
	delete ui;
	g_HouseControlForm = nullptr;
}
//----------------------------------------------------------------------------------
void HouseControlForm::on_pb_LockDown_clicked()
{
	OAFUN_DEBUG("");
	CPacketUnicodeSpeechRequest(0, L"I wish to lock this down").SendClient();
}
//----------------------------------------------------------------------------------
void HouseControlForm::on_pb_Secure_clicked()
{
	OAFUN_DEBUG("");
	CPacketUnicodeSpeechRequest(0, L"I wish to secure this").SendClient();
}
//----------------------------------------------------------------------------------
void HouseControlForm::on_pb_Release_clicked()
{
	OAFUN_DEBUG("");
	CPacketUnicodeSpeechRequest(0, L"I wish to release this").SendClient();
}
//----------------------------------------------------------------------------------
void HouseControlForm::on_pb_Ban_clicked()
{
	OAFUN_DEBUG("");
	CPacketUnicodeSpeechRequest(0, L"I ban thee").SendClient();
}
//----------------------------------------------------------------------------------
void HouseControlForm::on_pb_Trash_clicked()
{
	OAFUN_DEBUG("");
	CPacketUnicodeSpeechRequest(0, L"I wish to place a trash barrel").SendClient();
}
//----------------------------------------------------------------------------------
void HouseControlForm::on_pb_Remove_clicked()
{
	OAFUN_DEBUG("");
	CPacketUnicodeSpeechRequest(0, L"Remove thyself").SendClient();
}
//----------------------------------------------------------------------------------
void HouseControlForm::on_pb_Strongbox_clicked()
{
	OAFUN_DEBUG("");
	CPacketUnicodeSpeechRequest(0, L"I wish to place a strongbox").SendClient();
}
//----------------------------------------------------------------------------------
