/***********************************************************************************
**
** OrionAssistantForm.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef ORIONASSISTANTFORM_H
#define ORIONASSISTANTFORM_H
//----------------------------------------------------------------------------------
#include <QMainWindow>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QTableWidgetItem>
#include <QTimer>
#include <QSystemTrayIcon>
#include "../orionassistant_global.h"
#include "../GUI/coloredpushbutton.h"
#include "../CommonItems/binaryfile.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class OrionAssistantForm;
}
//----------------------------------------------------------------------------------
class OrionAssistantForm : public QMainWindow
{
	Q_OBJECT

	SETGET(bool, NoExecuteGUIEvents, false)
	SETGET(QString, AutosaveAfterTargetingName, "")
	SETGET(uint, DoorToOpen, 0)

private:
	Ui::OrionAssistantForm *ui;

	QTimer m_CorpseTimer;

	QTimer m_InformationTimer;

	QTimer m_AutoOpenDoorTimer;

	QSystemTrayIcon m_TrayIcon;

	QMap<uint, QString> m_ProfileMap;

	void SaveTiles();

	void LoadTiles();

	void LoadProfileMap();

	uint m_OldWindowAttributes{0};

private slots:
	void OnTrayIconActivated(QSystemTrayIcon::ActivationReason reason);

public slots:
	void onCorpseTimer();

	void onInformationTimer();

	void onAutostartTimer();

	void onAutoOpenDoorTimer();

protected:
	virtual void closeEvent(QCloseEvent *event);

	virtual void keyPressEvent(QKeyEvent *event);

	virtual void changeEvent(QEvent *event);

public:
	explicit OrionAssistantForm(QWidget *parent = 0);
	~OrionAssistantForm();

	void Init();

	void EndWork();

	int DistanceFromText(QString text);

	int DelayFromText(QString text);

	void LoadProfileBySerial(const uint &serial);

	void LoadCurrentProfile();

	QString GetSaveConfigPath();

	void SaveCurrentProfile();

	void SaveMainOptions(const QString &path);

	void OnNewCorpse();

	void RunAutostart();

	void UpdateAssistTitle(const QString &playerNameText);

	bool SelectButtonColor(QColoredPushButton *button, QWidget *owner = nullptr);

	void SaveProfileMap();

	void AutoOpenDoor(const int &x, const int &y, const int &z);
};
//----------------------------------------------------------------------------------
extern OrionAssistantForm *g_OrionAssistantForm;
//----------------------------------------------------------------------------------
#endif // ORIONASSISTANTFORM_H
//----------------------------------------------------------------------------------
