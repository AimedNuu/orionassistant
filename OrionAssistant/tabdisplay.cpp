// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabDisplay.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabdisplay.h"
#include "ui_tabdisplay.h"
#include "orionassistantform.h"
#include "orionassistant.h"
#include <QMenu>
#include "../Managers/displaymanager.h"
#include "../GUI/displayaction.h"
#include "../GUI/displaygroupaction.h"
#include "../GUI/displaygrouplistitem.h"
#include <QDir>
#include "../GameObjects/GameObject.h"
//----------------------------------------------------------------------------------
TabDisplay *g_TabDisplay = nullptr;
//----------------------------------------------------------------------------------
TabDisplay::TabDisplay(QWidget *parent)
: QWidget(parent), ui(new Ui::TabDisplay)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabDisplay = this;

	connect(ui->lw_DisplayItems, SIGNAL(itemDropped()), this, SLOT(OnListItemMoved()));
	connect(ui->lw_DisplayGroups, SIGNAL(itemDropped()), this, SLOT(OnListItemMoved()));
}
//----------------------------------------------------------------------------------
TabDisplay::~TabDisplay()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabDisplay = nullptr;
}
//----------------------------------------------------------------------------------
void TabDisplay::OnListItemMoved()
{
	OAFUN_DEBUG("");
	QString path = g_OrionAssistantForm->GetSaveConfigPath();

	if (!path.length())
		return;

	g_OrionAssistantForm->SaveMainOptions(path + "/Options.xml");
}
//----------------------------------------------------------------------------------
void TabDisplay::Init()
{
	OAFUN_DEBUG("");
	ui->tw_Display->setCurrentIndex(0);

	const DISPLAY_INFO_LIST &commands = g_DisplayManager.GetCommands();

	for (DISPLAY_INFO_LIST::const_iterator i = commands.begin(); i != commands.end(); ++i)
	{
		if (i->Name.length())
			ui->cb_DisplayItemType->addItem(i->Name);
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::on_rb_DisplayDisabled_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	RedrawTitle();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_rb_DisplayTextual_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	RedrawTitle();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_rb_DisplayColoredTextual_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	RedrawTitle();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_rb_DisplayColoredGraphical_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	RedrawTitle();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_cb_DisplayDoubleImage_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	g_DisplayManager.ClearIcons();
	RedrawTitle();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_cb_DisplayApplyNotorietyColorOnName_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	RedrawTitle();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_cb_DisplayHightFont_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	RedrawTitle();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_cb_HighlightReagents_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	RedrawTitle();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayTextColor_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->SelectButtonColor(ui->pb_DisplayTextColor))
	{
		if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
			return;

		g_DisplayManager.RecompilleData(ui->te_DisplayTitleData->toPlainText(), ui->pb_DisplayTextColor->GetUIntColor());

		RedrawTitle();
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayBackgroundColor_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->SelectButtonColor(ui->pb_DisplayBackgroundColor))
	{
		if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
			return;

		RedrawTitle();
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::on_te_DisplayTitleData_textChanged()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	g_DisplayManager.RecompilleData(ui->te_DisplayTitleData->toPlainText(), ui->pb_DisplayTextColor->GetUIntColor());

	RedrawTitle();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayInsertCommand_clicked()
{
	OAFUN_DEBUG("");
	emit ui->pb_DisplayInsertCommand->customContextMenuRequested(QPoint());
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayInsertCommand_customContextMenuRequested(const QPoint &pos)
{
	OAFUN_DEBUG("");
	Q_UNUSED(pos);

	QMenu *menu = new QMenu(this);

	int customCount = ui->lw_DisplayItems->count();

	if (customCount)
	{
		QMenu *custom = menu->addMenu("Custom items");

		IFOR(i, 0, customCount)
		{
			QListWidgetItem *item = ui->lw_DisplayItems->item(i);

			if (item != nullptr)
				custom->addAction(new CDisplayAction(item->text(), this));
		}

		menu->addSeparator();
	}

	const DISPLAY_INFO_LIST &commands = g_DisplayManager.GetCommands();

	for (DISPLAY_INFO_LIST::const_iterator i = commands.begin(); i != commands.end(); ++i)
	{
		if (i->Name.length())
			menu->addAction(new CDisplayAction(i->Name, this));
	}

	menu->popup(QCursor::pos());
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_SetTitleGroup_clicked()
{
	OAFUN_DEBUG("");
	emit ui->pb_SetTitleGroup->customContextMenuRequested(QPoint());
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_SetTitleGroup_customContextMenuRequested(const QPoint &pos)
{
	OAFUN_DEBUG("");
	Q_UNUSED(pos);

	QMenu *menu = new QMenu(this);

	IFOR(i, 0, ui->lw_DisplayGroups->count())
	{
		QListWidgetItem *item = ui->lw_DisplayGroups->item(i);

		if (item != nullptr)
			menu->addAction(new CDisplayGroupAction(item->text(), this));
	}

	menu->popup(QCursor::pos());
}
//----------------------------------------------------------------------------------
bool TabDisplay::DoubleImage()
{
	OAFUN_DEBUG("");
	return ui->cb_DisplayDoubleImage->isChecked();
}
//----------------------------------------------------------------------------------
bool TabDisplay::ApplyNotorietyColorOnName()
{
	OAFUN_DEBUG("");
	return ui->cb_DisplayApplyNotorietyColorOnName->isChecked();
}
//----------------------------------------------------------------------------------
bool TabDisplay::HighlightReagents()
{
	OAFUN_DEBUG("");
	return ui->cb_HighlightReagents->isChecked();
}
//----------------------------------------------------------------------------------
void TabDisplay::InsertDisplayAction(const QString &name)
{
	OAFUN_DEBUG("");
	IFOR(i, 0, ui->lw_DisplayItems->count())
	{
		QListWidgetItem *item = ui->lw_DisplayItems->item(i);

		if (item != nullptr && item->text().toLower() == name.toLower())
		{
			ui->te_DisplayTitleData->insertPlainText("{" + name + "}");
			return;
		}
	}

	const DISPLAY_INFO_LIST &commands = g_DisplayManager.GetCommands();

	for (DISPLAY_INFO_LIST::const_iterator i = commands.begin(); i != commands.end(); ++i)
	{
		if (i->Name == name)
		{
			ui->te_DisplayTitleData->insertPlainText("{" + i->Command + "}");
			break;
		}
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::InsertDisplayGroupAction(const QString &name)
{
	OAFUN_DEBUG("");
	IFOR(i, 0, ui->lw_DisplayGroups->count())
	{
		CDisplayGroupListItem *item = (CDisplayGroupListItem*)ui->lw_DisplayGroups->item(i);

		if (item != nullptr && item->text() == name)
		{
			g_TabDisplay->SetTitleData(item->GetTitleData());
			break;
		}
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::ChangeDisplayItemContent(const CDisplayItem &item, const bool fullUpdate)
{
	OAFUN_DEBUG("");
	g_OrionAssistantForm->SetNoExecuteGUIEvents(true);

	if (fullUpdate)
	{
		const DISPLAY_INFO_LIST &commands = g_DisplayManager.GetCommands();

		for (DISPLAY_INFO_LIST::const_iterator i = commands.begin(); i != commands.end(); ++i)
		{
			if (i->Type == item.GetType())
			{
				IFOR(j, 0, ui->cb_DisplayItemType->count())
				{
					if (ui->cb_DisplayItemType->itemText(j) == i->Name)
					{
						ui->cb_DisplayItemType->setCurrentIndex(j);
						break;
					}
				}

				break;
			}
		}
	}

	ui->le_DisplayItemText->setText(item.GetText());

	ui->le_DisplayItemGraphic->setText(COrionAssistant::GraphicToText(item.GetGraphic()));
	ui->le_DisplayItemHue->setText(COrionAssistant::GraphicToText(item.GetHue()));

	ui->sb_DisplayItemMidValue->setValue(item.GetMidValue());
	ui->sb_DisplayItemMinValue->setValue(item.GetMinValue());

	ui->pb_DisplayItemTextColor->ChangeColor(item.GetColor());
	ui->pb_DisplayItemMidTextColor->ChangeColor(item.GetMidColor());
	ui->pb_DisplayItemMinTextColor->ChangeColor(item.GetMinColor());

	ui->pb_DisplayItemBgColor->ChangeColor(item.GetBackgroundColor());
	ui->pb_DisplayItemMidBgColor->ChangeColor(item.GetBackgroundMidColor());
	ui->pb_DisplayItemMinBgColor->ChangeColor(item.GetBackgroundMinColor());

	ui->cb_DisplayItemBackground->setChecked(item.GetBackground());
	ui->cb_DisplayItemPercents->setChecked(item.GetPercents());

	g_OrionAssistantForm->SetNoExecuteGUIEvents(false);
}
//----------------------------------------------------------------------------------
void TabDisplay::SaveDisplayItemData(CDisplayItemListItem *item)
{
	OAFUN_DEBUG("");
	CDisplayItem &di = item->m_Item;

	QString typeText = ui->cb_DisplayItemType->currentText();
	const DISPLAY_INFO_LIST &commands = g_DisplayManager.GetCommands();

	for (DISPLAY_INFO_LIST::const_iterator i = commands.begin(); i != commands.end(); ++i)
	{
		if (i->Name == typeText)
		{
			di.SetType(i->Type);

			break;
		}
	}

	di.SetText(ui->le_DisplayItemText->text());

	di.SetGraphic(COrionAssistant::TextToGraphic(ui->le_DisplayItemGraphic->text()));
	di.SetHue(COrionAssistant::TextToGraphic(ui->le_DisplayItemHue->text()));

	di.SetMidValue(ui->sb_DisplayItemMidValue->text().toInt());
	di.SetMinValue(ui->sb_DisplayItemMinValue->text().toInt());

	di.SetColor(ui->pb_DisplayItemTextColor->GetUIntColor());
	di.SetMidColor(ui->pb_DisplayItemMidTextColor->GetUIntColor());
	di.SetMinColor(ui->pb_DisplayItemMinTextColor->GetUIntColor());

	di.SetBackgroundColor(ui->pb_DisplayItemBgColor->GetUIntColor());
	di.SetBackgroundMidColor(ui->pb_DisplayItemMidBgColor->GetUIntColor());
	di.SetBackgroundMinColor(ui->pb_DisplayItemMinBgColor->GetUIntColor());

	di.SetBackground(ui->cb_DisplayItemBackground->isChecked());
	di.SetPercents(ui->cb_DisplayItemPercents->isChecked());

	SaveDisplayItems();
	g_TabDisplay->RecompilleData();
}
//----------------------------------------------------------------------------------
CDisplayItem TabDisplay::GetCustomDisplayItemByName(const QString &name)
{
	OAFUN_DEBUG("");
	IFOR(i, 0, ui->lw_DisplayItems->count())
	{
		QListWidgetItem *item = ui->lw_DisplayItems->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			CDisplayItemListItem *dili = (CDisplayItemListItem*)item;
			return dili->m_Item;
		}
	}

	return CDisplayItem();
}
//----------------------------------------------------------------------------------
QString TabDisplay::GetGroupContent(const QString &name)
{
	OAFUN_DEBUG("");
	IFOR(i, 0, ui->lw_DisplayGroups->count())
	{
		CDisplayGroupListItem *item = (CDisplayGroupListItem*)ui->lw_DisplayGroups->item(i);

		if (item != nullptr && item->text().toLower() == name)
			return item->GetTitleData();
	}

	return "";
}
//----------------------------------------------------------------------------------
void TabDisplay::SetDisplayItemFromTarget(const ushort &graphic, const ushort &color)
{
	OAFUN_DEBUG("");
	if (graphic)
	{
		ui->le_DisplayItemGraphic->setText(COrionAssistant::GraphicToText(graphic));
		ui->le_DisplayItemHue->setText(COrionAssistant::GraphicToText(color));
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::CheckRedrawTitle(CGameObject *item)
{
	OAFUN_DEBUG("");
	if (!ui->rb_DisplayDisabled->isChecked())
	{
		if (g_DisplayManager.CanRedraw(item->GetGraphic(), item->GetColor()))
			RedrawTitle();
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::RedrawTitle()
{
	OAFUN_DEBUG("");
	REDRAW_DISPLAY_MODE mode = RDM_DISABLED;

	if (ui->rb_DisplayTextual->isChecked())
		mode = RDM_TEXTUAL;
	else if (ui->rb_DisplayColoredTextual->isChecked())
		mode = RDM_COLORED_TEXTUAL;
	else if (ui->rb_DisplayColoredGraphical->isChecked())
		mode = RDM_COLORED_GRAPHICAL;

	g_DisplayManager.Redraw(mode, ui->cb_DisplayApplyNotorietyColorOnName->isChecked(), ui->cb_DisplayHightFont->isChecked(), ui->cb_HighlightReagents->isChecked(), true, ui->pb_DisplayBackgroundColor->GetUIntColor());
}
//----------------------------------------------------------------------------------
void TabDisplay::RecompilleData()
{
	OAFUN_DEBUG("");
	g_DisplayManager.RecompilleData(ui->te_DisplayTitleData->toPlainText(), ui->pb_DisplayTextColor->GetUIntColor());
	RedrawTitle();
}
//----------------------------------------------------------------------------------
void TabDisplay::SetTitleData(const QString &text)
{
	OAFUN_DEBUG("");
	ui->te_DisplayTitleData->setPlainText(text);
}
//----------------------------------------------------------------------------------
void TabDisplay::LoadConfig(const QXmlStreamAttributes &attributes)
{
	OAFUN_DEBUG("");
	if (attributes.hasAttribute("type"))
	{
		int type = attributes.value("type").toInt();

		if (type == 0)
			ui->rb_DisplayDisabled->setChecked(true);
		else if (type == 1)
			ui->rb_DisplayTextual->setChecked(true);
		else if (type == 2)
			ui->rb_DisplayColoredTextual->setChecked(true);
		else if (type == 3)
			ui->rb_DisplayColoredGraphical->setChecked(true);
	}

	if (attributes.hasAttribute("flags"))
	{
		uint flags = attributes.value("flags").toInt();

		if (flags & 0x01)
			ui->cb_DisplayDoubleImage->setChecked(true);
		else
			ui->cb_DisplayDoubleImage->setChecked(false);

		if (flags & 0x02)
			ui->cb_DisplayApplyNotorietyColorOnName->setChecked(true);
		else
			ui->cb_DisplayApplyNotorietyColorOnName->setChecked(false);

		if (flags & 0x04)
			ui->cb_DisplayHightFont->setChecked(true);
		else
			ui->cb_DisplayHightFont->setChecked(false);

		if (flags & 0x08)
			ui->cb_HighlightReagents->setChecked(true);
		else
			ui->cb_HighlightReagents->setChecked(false);
	}

	if (attributes.hasAttribute("textcolor"))
		ui->pb_DisplayTextColor->ChangeColor(attributes.value("textcolor").toUInt());

	if (attributes.hasAttribute("backgroundcolor"))
		ui->pb_DisplayBackgroundColor->ChangeColor(attributes.value("backgroundcolor").toUInt());

	if (attributes.hasAttribute("titledata"))
		ui->te_DisplayTitleData->setPlainText(attributes.value("titledata").toString());
}
//----------------------------------------------------------------------------------
void TabDisplay::SaveConfig(QXmlStreamWriter &writter)
{
	OAFUN_DEBUG("");
	writter.writeStartElement("display");

	int displayType = 0; //rb_DisplayDisabled

	if (ui->rb_DisplayTextual->isChecked())
		displayType = 1;
	else if (ui->rb_DisplayColoredTextual->isChecked())
		displayType = 2;
	else if (ui->rb_DisplayColoredGraphical->isChecked())
		displayType = 3;

	writter.writeAttribute("type", QString::number(displayType));

	int displayFlags = 0;

	if (ui->cb_DisplayDoubleImage->isChecked())
		displayFlags |= 0x01;

	if (ui->cb_DisplayApplyNotorietyColorOnName->isChecked())
		displayFlags |= 0x02;

	if (ui->cb_DisplayHightFont->isChecked())
		displayFlags |= 0x04;

	if (ui->cb_HighlightReagents->isChecked())
		displayFlags |= 0x08;

	writter.writeAttribute("flags", QString::number(displayFlags));

	writter.writeAttribute("textcolor", QString::number(ui->pb_DisplayTextColor->GetUIntColor()));
	writter.writeAttribute("backgroundcolor", QString::number(ui->pb_DisplayBackgroundColor->GetUIntColor()));

	writter.writeAttribute("titledata", ui->te_DisplayTitleData->toPlainText());

	writter.writeEndElement(); //display
}
//----------------------------------------------------------------------------------
void TabDisplay::SaveDisplayItems()
{
	OAFUN_DEBUG("");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/DisplayItems.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_DisplayItems->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");
		writter.writeAttribute("size", QString::number(count));

		IFOR(i, 0, count)
		{
			QListWidgetItem *item = ui->lw_DisplayItems->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("item");

				CDisplayItem &di = ((CDisplayItemListItem*)item)->m_Item;

				writter.writeAttribute("name", item->text());
				writter.writeAttribute("type", QString::number(di.GetType()));
				writter.writeAttribute("text", di.GetText());
				writter.writeAttribute("id", COrionAssistant::GraphicToText(di.GetGraphic()));
				writter.writeAttribute("hue", COrionAssistant::GraphicToText(di.GetHue()));
				writter.writeAttribute("mincolor", COrionAssistant::ColorToText(di.GetMinColor()));
				writter.writeAttribute("midcolor", COrionAssistant::ColorToText(di.GetMidColor()));
				writter.writeAttribute("color", COrionAssistant::ColorToText(di.GetColor()));
				writter.writeAttribute("background", COrionAssistant::BoolToText(di.GetBackground()));
				writter.writeAttribute("bgmincolor", COrionAssistant::ColorToText(di.GetBackgroundMinColor()));
				writter.writeAttribute("bgmidcolor", COrionAssistant::ColorToText(di.GetBackgroundMidColor()));
				writter.writeAttribute("bgcolor", COrionAssistant::ColorToText(di.GetBackgroundColor()));
				writter.writeAttribute("minvalue", QString::number(di.GetMinValue()));
				writter.writeAttribute("midvalue", QString::number(di.GetMidValue()));
				writter.writeAttribute("percents", COrionAssistant::BoolToText(di.GetPercents()));

				writter.writeEndElement(); //item
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::LoadDisplayItems()
{
	OAFUN_DEBUG("");
	ui->lw_DisplayItems->clear();

	QFile file(g_DllPath + "/GlobalConfig/DisplayItems.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;
		int count = 0;

		Q_UNUSED(version);
		Q_UNUSED(count);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("size"))
						count = attributes.value("size").toInt();
				}
				else if (reader.name() == "item")
				{
					if (attributes.hasAttribute("name"))
					{
						CDisplayItemListItem *item = new CDisplayItemListItem(attributes.value("name").toString(), ui->lw_DisplayItems);

						CDisplayItem &di = item->m_Item;

						if (attributes.hasAttribute("type"))
							di.SetType((DISPLAY_ITEM_TYPE)attributes.value("type").toInt());

						if (attributes.hasAttribute("text"))
							di.SetText(attributes.value("text").toString());

						if (attributes.hasAttribute("id"))
							di.SetGraphic(attributes.value("id").toUShort(nullptr, 16));

						if (attributes.hasAttribute("hue"))
							di.SetHue(attributes.value("hue").toUShort(nullptr, 16));

						if (attributes.hasAttribute("mincolor"))
							di.SetMinColor(COrionAssistant::RawStringToUInt(attributes.value("mincolor").toString()));

						if (attributes.hasAttribute("midcolor"))
							di.SetMidColor(COrionAssistant::RawStringToUInt(attributes.value("midcolor").toString()));

						if (attributes.hasAttribute("color"))
							di.SetColor(COrionAssistant::RawStringToUInt(attributes.value("color").toString()));

						if (attributes.hasAttribute("background"))
							di.SetBackground(COrionAssistant::RawStringToBool(attributes.value("background").toString()));

						if (attributes.hasAttribute("bgmincolor"))
							di.SetBackgroundMinColor(COrionAssistant::RawStringToUInt(attributes.value("bgmincolor").toString()));

						if (attributes.hasAttribute("bgmidcolor"))
							di.SetBackgroundMidColor(COrionAssistant::RawStringToUInt(attributes.value("bgmidcolor").toString()));

						if (attributes.hasAttribute("bgcolor"))
							di.SetBackgroundColor(COrionAssistant::RawStringToUInt(attributes.value("bgcolor").toString()));

						if (attributes.hasAttribute("minvalue"))
							di.SetMinValue(attributes.value("minvalue").toInt());

						if (attributes.hasAttribute("midvalue"))
							di.SetMidValue(attributes.value("midvalue").toInt());

						if (attributes.hasAttribute("percents"))
							di.SetPercents(COrionAssistant::RawStringToBool(attributes.value("percents").toString()));
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}

	if (!ui->lw_DisplayItems->count())
	{
		const DISPLAY_INFO_LIST &commands = g_DisplayManager.GetCommands();

		for (DISPLAY_INFO_LIST::const_iterator i = commands.begin(); i != commands.end(); ++i)
		{
			if (i->Name.length())
			{
				CDisplayItem di = g_DisplayManager.GetDisplayItemByName(i->Command);

				if (di.GetType() != DIT_NONE && di.GetType() != DIT_OBJECT)
				{
					CDisplayItemListItem *item = new CDisplayItemListItem(i->Command, ui->lw_DisplayItems);
					item->m_Item = di;
				}
			}
		}

		static const QString defNames[10] = {"bm", "bp", "ga", "gs", "mr", "ns", "sa", "ss", "en", "bandage"};

		IFOR(i, 0, 10)
		{
			CDisplayItemListItem *item = new CDisplayItemListItem(defNames[i], ui->lw_DisplayItems);
			item->m_Item = CDisplayItem(DIT_OBJECT, "", COrionAssistant::TextToGraphic(defNames[i]), 0, 0x00FFFFFF, 0x00000001, 0x00000001, true,	0x0000007F, 0x0000FFFF, 0x00000000, 10, 20, false);

			if (i < 9)
				item->m_Item.SetText(defNames[i] + ":");
			else
				item->m_Item.SetText("b:");
		}

		SaveDisplayItems();
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::SaveDisplayGroups()
{
	OAFUN_DEBUG("");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/DisplayGroups.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_DisplayGroups->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");
		writter.writeAttribute("size", QString::number(count));

		IFOR(i, 0, count)
		{
			QListWidgetItem *item = ui->lw_DisplayGroups->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("group");

				writter.writeAttribute("name", item->text());
				writter.writeAttribute("text", ((CDisplayGroupListItem*)item)->GetTitleData());

				writter.writeEndElement(); //group
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::LoadDisplayGroups()
{
	OAFUN_DEBUG("");
	ui->lw_DisplayGroups->clear();

	QFile file(g_DllPath + "/GlobalConfig/DisplayGroups.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;
		int count = 0;

		Q_UNUSED(version);
		Q_UNUSED(count);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("size"))
						count = attributes.value("size").toInt();
				}
				else if (reader.name() == "group")
				{
					if (attributes.hasAttribute("name") && attributes.hasAttribute("text"))
						new CDisplayGroupListItem(attributes.value("name").toString(), attributes.value("text").toString(), ui->lw_DisplayGroups);
				}
			}

			reader.readNext();
		}

		file.close();
	}

	if (!ui->lw_DisplayGroups->count())
	{
		QString allreagDisplayGroup = "{bm} {bp} {ga} {gs} {mr} {ns} {sa} {ss}";

		new CDisplayGroupListItem("allreag", allreagDisplayGroup, ui->lw_DisplayGroups);

		QString defaultDisplayGroup = "UO - {name} ({server}) {barmiddle} {hp_maxhp} {mp_maxmp} {sp_maxsp} {armor} {weight_maxweight} {gold} {group allreag} {x} {y}";

		new CDisplayGroupListItem("default", defaultDisplayGroup, ui->lw_DisplayGroups);

		SaveDisplayGroups();
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::on_lw_DisplayItems_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	QListWidgetItem *item = ui->lw_DisplayItems->item(index.row());

	if (item != nullptr)
	{
		CDisplayItemListItem *dili = (CDisplayItemListItem*)item;

		ui->le_DisplayItemName->setText(item->text());

		g_OrionAssistantForm->SetNoExecuteGUIEvents(true);
		ChangeDisplayItemContent(dili->m_Item, true);
		g_OrionAssistantForm->SetNoExecuteGUIEvents(false);
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::on_cb_DisplayItemType_currentIndexChanged(const QString &arg1)
{
	OAFUN_DEBUG("");
	if (!g_OrionAssistantForm->GetNoExecuteGUIEvents())
		ChangeDisplayItemContent(g_DisplayManager.GetDisplayItemByName(arg1), false);
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayItemTextColor_clicked()
{
	OAFUN_DEBUG("");
	g_OrionAssistantForm->SelectButtonColor(ui->pb_DisplayItemTextColor);
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayItemMidTextColor_clicked()
{
	OAFUN_DEBUG("");
	g_OrionAssistantForm->SelectButtonColor(ui->pb_DisplayItemMidTextColor);
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayItemMinTextColor_clicked()
{
	OAFUN_DEBUG("");
	g_OrionAssistantForm->SelectButtonColor(ui->pb_DisplayItemMinTextColor);
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayItemBgColor_clicked()
{
	OAFUN_DEBUG("");
	g_OrionAssistantForm->SelectButtonColor(ui->pb_DisplayItemBgColor);
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayItemMidBgColor_clicked()
{
	OAFUN_DEBUG("");
	g_OrionAssistantForm->SelectButtonColor(ui->pb_DisplayItemMidBgColor);
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayItemMinBgColor_clicked()
{
	OAFUN_DEBUG("");
	g_OrionAssistantForm->SelectButtonColor(ui->pb_DisplayItemMinBgColor);
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayItemFromTarget_clicked()
{
	OAFUN_DEBUG("");
	g_TargetHandler = &COrionAssistant::HandleTargetDisplayItem;

	g_OrionAssistant.RequestTarget("Select a object for obtain graphic and color");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayItemCreate_clicked()
{
	OAFUN_DEBUG("");
	QString name = ui->le_DisplayItemName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the display item name!");
		return;
	}

	IFOR(i, 0, ui->lw_DisplayItems->count())
	{
		QListWidgetItem *item = ui->lw_DisplayItems->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			QMessageBox::critical(this, "Name is already exists", "Display item name is already exists!");
			return;
		}
	}

	SaveDisplayItemData(new CDisplayItemListItem(ui->le_DisplayItemName->text(), ui->lw_DisplayItems));

	ui->lw_DisplayItems->setCurrentRow(ui->lw_DisplayItems->count() - 1);
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayItemSave_clicked()
{
	OAFUN_DEBUG("");
	QString name = ui->le_DisplayItemName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the display item name!");
		return;
	}

	CDisplayItemListItem *selected = (CDisplayItemListItem*)ui->lw_DisplayItems->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Item is not selected", "Item is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_DisplayItems->count())
	{
		QListWidgetItem *item = ui->lw_DisplayItems->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Name is already exists", "Display item name is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	SaveDisplayItemData(selected);
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayItemRemove_clicked()
{
	OAFUN_DEBUG("");
	QListWidgetItem *item = ui->lw_DisplayItems->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_DisplayItems->takeItem(ui->lw_DisplayItems->row(item));

		if (item != nullptr)
		{
			delete item;
			SaveDisplayItems();
			RecompilleData();
		}
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::on_lw_DisplayGroups_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("");
	QListWidgetItem *item = ui->lw_DisplayGroups->item(index.row());

	if (item != nullptr)
	{
		CDisplayGroupListItem *dgli = (CDisplayGroupListItem*)item;
		ui->le_DisplayGroupName->setText(dgli->text());
		ui->te_DisplayGroupTitleData->setPlainText(dgli->GetTitleData());
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayGroupCreate_clicked()
{
	OAFUN_DEBUG("");
	QString name = ui->le_DisplayGroupName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the title group name!");
		return;
	}

	IFOR(i, 0, ui->lw_DisplayGroups->count())
	{
		QListWidgetItem *item = ui->lw_DisplayGroups->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			QMessageBox::critical(this, "Name is already exists", "Display group name is already exists!");
			return;
		}
	}

	new CDisplayGroupListItem(ui->le_DisplayGroupName->text(), ui->te_DisplayGroupTitleData->toPlainText(), ui->lw_DisplayGroups);

	SaveDisplayGroups();

	g_TabDisplay->RecompilleData();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayGroupSave_clicked()
{
	OAFUN_DEBUG("");
	QString name = ui->le_DisplayGroupName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the title group name!");
		return;
	}

	CDisplayGroupListItem *selected = (CDisplayGroupListItem*)ui->lw_DisplayGroups->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Group is not selected", "Group is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_DisplayGroups->count())
	{
		QListWidgetItem *item = ui->lw_DisplayGroups->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Name is already exists", "Display group name is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->setText(ui->le_DisplayGroupName->text());
	selected->SetTitleData(ui->te_DisplayGroupTitleData->toPlainText());

	SaveDisplayGroups();

	g_TabDisplay->RecompilleData();
}
//----------------------------------------------------------------------------------
void TabDisplay::on_pb_DisplayGroupRemove_clicked()
{
	OAFUN_DEBUG("");
	QListWidgetItem *item = ui->lw_DisplayGroups->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_DisplayGroups->takeItem(ui->lw_DisplayGroups->row(item));

		if (item != nullptr)
		{
			delete item;

			SaveDisplayGroups();

			g_TabDisplay->RecompilleData();
		}
	}
}
//----------------------------------------------------------------------------------
void TabDisplay::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_DisplayItems) //Items editor
		on_pb_DisplayItemRemove_clicked();
	else if (widget == ui->lw_DisplayGroups) //Groups editor
		on_pb_DisplayGroupRemove_clicked();
}
//----------------------------------------------------------------------------------
