// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabHotkeys.cpp
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabhotkeys.h"
#include "ui_tabhotkeys.h"
#include "shipcontrolform.h"
#include "housecontrolform.h"
#include "animalandvendorcontrolform.h"
#include "hotkeysform.h"
#include "../GameObjects/GameWorld.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include <QFileDialog>
//----------------------------------------------------------------------------------
TabHotkeys *g_TabHotkeys = nullptr;
//----------------------------------------------------------------------------------
TabHotkeys::TabHotkeys(QWidget *parent)
: QWidget(parent), ui(new Ui::TabHotkeys)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabHotkeys = this;
}
//----------------------------------------------------------------------------------
TabHotkeys::~TabHotkeys()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabHotkeys = nullptr;
}
//----------------------------------------------------------------------------------
void TabHotkeys::on_pb_OpenHotkeysDialog_clicked()
{
	OAFUN_DEBUG("");
	if (g_HotkeysForm != nullptr)
	{
		if (g_HotkeysForm->isVisible())
			g_HotkeysForm->activateWindow();
		else
			g_HotkeysForm->show();
	}
}
//----------------------------------------------------------------------------------
void TabHotkeys::on_pb_SaveHotkeys_clicked()
{
	OAFUN_DEBUG("");
	if (g_World == nullptr)
		return;

	QDir(g_DllPath).mkdir(g_DllPath + "/SavedHotkeys");
	QString path = QFileDialog::getSaveFileName(nullptr, tr("Select file to save hotkeys"), g_DllPath + "/SavedHotkeys", tr("xml(*.xml)"));

	if (path.length())
	{
		g_HotkeysForm->Save(path);

		g_OrionAssistant.ClientPrint("Hotkeys saved to: " + path);
	}
}
//----------------------------------------------------------------------------------
void TabHotkeys::on_pb_LoadHotkeys_clicked()
{
	OAFUN_DEBUG("");
	if (g_World == nullptr)
		return;

	QDir(g_DllPath).mkdir(g_DllPath + "/SavedHotkeys");
	QString path = QFileDialog::getOpenFileName(nullptr, tr("Select file to load hotkeys"), g_DllPath + "/SavedHotkeys", tr("xml(*.xml)"));

	if (path.length())
	{
		g_HotkeysForm->Load(path);

		g_OrionAssistant.ClientPrint("Hotkeys loaded from: " + path);
	}
}
//----------------------------------------------------------------------------------
void TabHotkeys::on_pb_AnimalAndVendorControl_clicked()
{
	OAFUN_DEBUG("");
	if (g_AnimalAndVendorControlForm == nullptr)
		g_AnimalAndVendorControlForm = new AnimalAndVendorControlForm(nullptr);

	if (!g_AnimalAndVendorControlForm->isVisible())
		g_AnimalAndVendorControlForm->show();
	else
		g_AnimalAndVendorControlForm->activateWindow();
}
//----------------------------------------------------------------------------------
void TabHotkeys::on_pb_HouseControl_clicked()
{
	OAFUN_DEBUG("");
	if (g_HouseControlForm == nullptr)
		g_HouseControlForm = new HouseControlForm(nullptr);

	if (!g_HouseControlForm->isVisible())
		g_HouseControlForm->show();
	else
		g_HouseControlForm->activateWindow();
}
//----------------------------------------------------------------------------------
void TabHotkeys::on_pb_ShipControl_clicked()
{
	OAFUN_DEBUG("");
	if (g_ShipControlForm == nullptr)
		g_ShipControlForm = new ShipControlForm(nullptr);

	if (!g_ShipControlForm->isVisible())
		g_ShipControlForm->show();
	else
		g_ShipControlForm->activateWindow();
}
//----------------------------------------------------------------------------------
int TabHotkeys::IsCommandPrefix(QString text)
{
	OAFUN_DEBUG("");
	int result = 0;
	QString prefix = ui->le_CommandPrefix->text().toLower();

	if (prefix.length())
	{
		if (text.length() > prefix.length())
			text.resize(prefix.length());

		if (text.toLower() == prefix)
			result = prefix.length();
	}

	return result;
}
//----------------------------------------------------------------------------------
void TabHotkeys::LoadConfig(const QXmlStreamAttributes &attributes)
{
	OAFUN_DEBUG("");
	if (attributes.hasAttribute("passhotkeys"))
		ui->cb_PassHotkeys->setChecked(COrionAssistant::RawStringToBool(attributes.value("passhotkeys").toString()));

	if (attributes.hasAttribute("hotkeyprefix"))
		ui->le_CommandPrefix->setText(attributes.value("commandprefix").toString());
}
//----------------------------------------------------------------------------------
void TabHotkeys::SaveConfig(QXmlStreamWriter &writter)
{
	OAFUN_DEBUG("");

	writter.writeStartElement("hotkeys");

	writter.writeAttribute("passhotkeys", COrionAssistant::BoolToText(ui->cb_PassHotkeys->isChecked()));
	writter.writeAttribute("commandprefix", ui->le_CommandPrefix->text());

	writter.writeEndElement(); //hotkeys
}
//----------------------------------------------------------------------------------
void TabHotkeys::on_cb_OnOffHotkeys_clicked()
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	g_OrionAssistant.ClientPrint("Hotkeys now is " + QString(ui->cb_OnOffHotkeys->isChecked() ? "enabled" : "disabled"));
}
//----------------------------------------------------------------------------------
bool TabHotkeys::OnOffHotkeys()
{
	OAFUN_DEBUG("");
	return ui->cb_OnOffHotkeys->isChecked();
}
//----------------------------------------------------------------------------------
void TabHotkeys::SetOnOffHotkeys(const bool &state)
{
	OAFUN_DEBUG("");
	ui->cb_OnOffHotkeys->setChecked(state);

	g_OrionAssistant.ClientPrint("Hotkeys now is " + QString(state ? "enabled" : "disabled"));
}
//----------------------------------------------------------------------------------
bool TabHotkeys::PassHotkeys()
{
	OAFUN_DEBUG("");
	return ui->cb_PassHotkeys->isChecked();
}
//----------------------------------------------------------------------------------
