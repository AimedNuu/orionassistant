// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabSkills.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabskills.h"
#include "ui_tabskills.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include "../Managers/skillmanager.h"
#include <QClipboard>
#include "../GameObjects/GamePlayer.h"
#include "hotkeysform.h"
//----------------------------------------------------------------------------------
TabSkills *g_TabSkills = nullptr;
//----------------------------------------------------------------------------------
TabSkills::TabSkills(QWidget *parent)
: QWidget(parent), ui(new Ui::TabSkills)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabSkills = this;
}
//----------------------------------------------------------------------------------
TabSkills::~TabSkills()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabSkills = nullptr;
}
//----------------------------------------------------------------------------------
void TabSkills::Init()
{
	OAFUN_DEBUG("");

	ui->tw_Skills->setColumnCount(5);

	ui->tw_Skills->setHorizontalHeaderLabels(QStringList() << "Name" << "Base" << "Real" << "Delta" << "Cap");

	QHeaderView *headerView = ui->tw_Skills->horizontalHeader();

	if (headerView != nullptr)
	{
		headerView->resizeSection(0, 160);
		headerView->resizeSection(1, 50);
		headerView->resizeSection(2, 50);
		headerView->resizeSection(3, 50);
		headerView->resizeSection(4, 50);
	}
}
//----------------------------------------------------------------------------------
void TabSkills::SkillsListUpdated(CDataReader &reader)
{
	OAFUN_DEBUG("");
	g_SkillsCount = reader.ReadUInt16BE();
	ui->tw_Skills->setRowCount(g_SkillsCount);

	QStringList hotkeyActions("Last");

	IFOR(i, 0, g_SkillsCount)
	{
		uchar button = reader.ReadUInt8();
		QString name = reader.ReadString().c_str();

		if (button)
			hotkeyActions << name;

		g_Skills[i].SetButton(button);
		g_Skills[i].SetName(name);

		g_SkillManager.Insert(name.toLower(), button, i);

		QTableWidgetItem *item = new QTableWidgetItem(name);
		item->setIcon(QIcon(QPixmap(":/resource/images/skill_wtf.bmp")));
		ui->tw_Skills->setItem(i, 0, item);
		ui->tw_Skills->setItem(i, 1, new QTableWidgetItem("0.0"));
		ui->tw_Skills->setItem(i, 2, new QTableWidgetItem("0.0"));
		item = new QTableWidgetItem("");
		item->setTextAlignment(Qt::AlignVCenter);
		ui->tw_Skills->setItem(i, 3, item);
		ui->tw_Skills->setItem(i, 4, new QTableWidgetItem("0.0"));
	}

	g_HotkeysForm->UpdateActionList(HT_USE_SKILL, hotkeyActions);
	g_HotkeysForm->UpdateActionList(HT_USE_SKILL_SELF, hotkeyActions);
	g_HotkeysForm->UpdateActionList(HT_USE_SKILL_LAST_TARGET, hotkeyActions);
	g_HotkeysForm->UpdateActionList(HT_USE_SKILL_LAST_ATTACK, hotkeyActions);
	g_HotkeysForm->UpdateActionList(HT_USE_SKILL_ENEMY, hotkeyActions);
	g_HotkeysForm->UpdateActionList(HT_USE_SKILL_FREIND, hotkeyActions);
	g_HotkeysForm->UpdateActionList(HT_USE_SKILL_NEW_TARGET_SYSTEM, hotkeyActions);
}
//----------------------------------------------------------------------------------
void TabSkills::on_bt_Reset_SkillsDelta_clicked()
{
	OAFUN_DEBUG("");
	DFOR(i, g_SkillsCount - 1, 0)
	{
		g_Skills[i].SetOldValue(g_Skills[i].GetValue());

		QTableWidgetItem *item = ui->tw_Skills->item(i, 3);

		if (item != nullptr)
			item->setText("");
	}
}
//----------------------------------------------------------------------------------
void TabSkills::on_bt_CopySelectedToClipboard_clicked()
{
	OAFUN_DEBUG("");
	QTableWidgetItem *item = ui->tw_Skills->currentItem();
	QClipboard *clipboard = qApp->clipboard();

	if (item != nullptr && clipboard != nullptr)
	{
		IFOR(i, 0, g_SkillsCount)
		{
			if (item->text() == g_Skills[i].GetName())
			{
				CSkill &skill = g_Skills[i];
				QString buf;

				if (skill.GetValue() == skill.GetOldValue())
					buf.sprintf("\t%.1f\n", skill.GetValue());
				else
					buf.sprintf("\t%.1f\t %.1f\n", skill.GetValue(), skill.GetValue() - skill.GetOldValue());

				Beep(440, 100);

				clipboard->setText(g_Skills[i].GetName() + buf);

				break;
			}
		}
	}
}
//----------------------------------------------------------------------------------
void TabSkills::on_bt_CopyAllToClipboard_clicked()
{
	OAFUN_DEBUG("");
	QClipboard *clipboard = qApp->clipboard();

	if (clipboard == nullptr)
		return;

	SYSTEMTIME st;
	GetSystemTime(&st);
	QString resultBuf;
	resultBuf.sprintf("===========OrionAssist===========\n===Skills status report, %i.%i.%i===\n", st.wDay, st.wMonth, st.wYear);

	float skillSum = 0.0f;

	IFOR(i, 0, g_SkillsCount)
	{
		QString nameBuf = g_Skills[i].GetName();

		if (nameBuf.length() < 8)
			nameBuf += "\t\t\t";
		else if (nameBuf.length() < 16)
			nameBuf += "\t\t";
		else
			nameBuf += "\t";

		resultBuf += nameBuf;

		CSkill &skill = g_Skills[i];

		skillSum += skill.GetValue();

		QString buf;

		if (skill.GetValue() == skill.GetOldValue())
			buf.sprintf("%.1f\n", skill.GetValue());
		else
			buf.sprintf("%.1f\t%.1f\n", skill.GetValue(), skill.GetValue() - skill.GetOldValue());

		resultBuf += buf;
	}

	QString buf;

	if (g_Player != NULL)
	{
		int statSum = g_Player->GetStr() + g_Player->GetInt() + g_Player->GetDex();

		buf.sprintf("Str %i Int %i Dex %i\n___Skillcap: %.1f Statcap: %i___\n", g_Player->GetStr(), g_Player->GetInt(), g_Player->GetDex(), skillSum, statSum);
	}
	else
		buf.sprintf("Str 0 Int 0 Dex 0\n___Skillcap: %.1f Statcap: 0___\n", skillSum);

	clipboard->setText(resultBuf + buf);
}
//----------------------------------------------------------------------------------
int TabSkills::GetSkillRow(const uchar &id)
{
	OAFUN_DEBUG("");
	if (id < g_SkillsCount)
	{
		IFOR(i, 0, g_SkillsCount)
		{
			QTableWidgetItem *item = ui->tw_Skills->item(i, 0);

			if (item != nullptr && item->text() == g_Skills[id].GetName())
				return i;
		}
	}

	return -1;
}
//----------------------------------------------------------------------------------
void TabSkills::UpdateSkill(const uchar &id)
{
	OAFUN_DEBUG("");
	if (id >= g_SkillsCount)
		return;

	int rowIndex = GetSkillRow(id);

	if (rowIndex != -1)
	{
		CSkill &skill = g_Skills[id];
		QString buf;

		QTableWidgetItem *name = ui->tw_Skills->item(rowIndex, 0);

		if (name != nullptr)
		{
			static const QString locks[4] = {"up", "down", "lock", "wtf"};

			uchar lockIndex = skill.GetStatus();

			if (lockIndex > 2)
				lockIndex = 3;

			name->setIcon(QIcon(QPixmap(":/resource/images/skill_" + locks[lockIndex] + ".bmp")));
		}

		QTableWidgetItem *value = ui->tw_Skills->item(rowIndex, 1);

		if (value != nullptr)
		{
			value->setData(Qt::DisplayRole, skill.GetBaseValue());

			//buf.sprintf("%3.1f", skill.Base);
			//value->setText(buf);
		}

		QTableWidgetItem *real = ui->tw_Skills->item(rowIndex, 2);

		if (real != nullptr)
		{
			real->setData(Qt::DisplayRole, skill.GetValue());

			//buf.sprintf("%3.1f", skill.Real);
			//real->setText(buf);
		}

		QTableWidgetItem *delta = ui->tw_Skills->item(rowIndex, 3);

		if (delta != nullptr)
		{
			float valueDelta = skill.GetValue() - skill.GetOldValue();

			//delta->setData(Qt::DisplayRole, valueDelta);

			if ((int)(valueDelta * 10) != 0)
				buf.sprintf("%3.1f", valueDelta);
			else
				buf = "";

			delta->setText(buf);
		}

		QTableWidgetItem *cap = ui->tw_Skills->item(rowIndex, 4);

		if (cap != nullptr)
		{
			cap->setData(Qt::DisplayRole, skill.GetCap());

			//buf.sprintf("%3.1f", skill.Cap);
			//cap->setText(buf);
		}
	}
}
//----------------------------------------------------------------------------------
void TabSkills::UpdateStatsAndSkillsInfo()
{
	OAFUN_DEBUG("");
	QString buf;
	float skillSum = 0.0f;
	int statSum = 0;

	IFOR(i, 0, g_SkillsCount)
		skillSum += g_Skills[i].GetValue();

	if (g_Player != NULL)
	{
		statSum = g_Player->GetStr() + g_Player->GetInt() + g_Player->GetDex();

		ui->l_Str->setText(QString::number(g_Player->GetStr()));
		ui->l_Int->setText(QString::number(g_Player->GetInt()));
		ui->l_Dex->setText(QString::number(g_Player->GetDex()));
	}
	else
	{
		ui->l_Str->setText("0");
		ui->l_Int->setText("0");
		ui->l_Dex->setText("0");
	}

	ui->l_Statsum->setText(QString::number(statSum));

	buf.sprintf("%.1f", skillSum);
	ui->l_Skillsum->setText(buf);
}
//----------------------------------------------------------------------------------
