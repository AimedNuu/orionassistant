/***********************************************************************************
**
** TabDisplay.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABDISPLAY_H
#define TABDISPLAY_H
//----------------------------------------------------------------------------------
#include <QWidget>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include "../CommonItems/binaryfile.h"
#include "../GUI/displayitemlistitem.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabDisplay;
}
//----------------------------------------------------------------------------------
class TabDisplay : public QWidget
{
	Q_OBJECT

private slots:
	void on_rb_DisplayDisabled_clicked();

	void on_rb_DisplayTextual_clicked();

	void on_rb_DisplayColoredTextual_clicked();

	void on_rb_DisplayColoredGraphical_clicked();

	void on_cb_DisplayDoubleImage_clicked();

	void on_cb_DisplayApplyNotorietyColorOnName_clicked();

	void on_cb_DisplayHightFont_clicked();

	void on_pb_DisplayTextColor_clicked();

	void on_pb_DisplayBackgroundColor_clicked();

	void on_te_DisplayTitleData_textChanged();

	void on_pb_DisplayInsertCommand_clicked();

	void on_pb_DisplayInsertCommand_customContextMenuRequested(const QPoint &pos);

	void on_pb_SetTitleGroup_clicked();

	void on_pb_SetTitleGroup_customContextMenuRequested(const QPoint &pos);

	void on_lw_DisplayItems_clicked(const QModelIndex &index);

	void on_cb_DisplayItemType_currentIndexChanged(const QString &arg1);

	void on_pb_DisplayItemTextColor_clicked();

	void on_pb_DisplayItemMidTextColor_clicked();

	void on_pb_DisplayItemMinTextColor_clicked();

	void on_pb_DisplayItemBgColor_clicked();

	void on_pb_DisplayItemMidBgColor_clicked();

	void on_pb_DisplayItemMinBgColor_clicked();

	void on_pb_DisplayItemFromTarget_clicked();

	void on_pb_DisplayItemCreate_clicked();

	void on_pb_DisplayItemSave_clicked();

	void on_pb_DisplayItemRemove_clicked();

	void on_lw_DisplayGroups_clicked(const QModelIndex &index);

	void on_pb_DisplayGroupCreate_clicked();

	void on_pb_DisplayGroupSave_clicked();

	void on_pb_DisplayGroupRemove_clicked();

	void on_cb_HighlightReagents_clicked();

private:
	Ui::TabDisplay *ui;

	void SaveDisplayItems();

	void SaveDisplayGroups();

	void ChangeDisplayItemContent(const CDisplayItem &item, const bool fullUpdate);

	void SaveDisplayItemData(CDisplayItemListItem *item);

public slots:
	void OnListItemMoved();

public:
	explicit TabDisplay(QWidget *parent = 0);
	~TabDisplay();

	void Init();

	bool DoubleImage();

	bool ApplyNotorietyColorOnName();

	bool HighlightReagents();

	void InsertDisplayAction(const QString &name);

	void InsertDisplayGroupAction(const QString &name);

	void CheckRedrawTitle(class CGameObject *item);

	CDisplayItem GetCustomDisplayItemByName(const QString &name);

	QString GetGroupContent(const QString &name);

	void SetDisplayItemFromTarget(const ushort &graphic, const ushort &color);

	void RedrawTitle();

	void RecompilleData();

	void SetTitleData(const QString &text);

	void LoadConfig(const QXmlStreamAttributes &attributes);

	void SaveConfig(QXmlStreamWriter &writter);

	void LoadDisplayItems();

	void LoadDisplayGroups();

	void OnDeleteKeyClick(QWidget *widget);
};
//----------------------------------------------------------------------------------
extern TabDisplay *g_TabDisplay;
//----------------------------------------------------------------------------------
#endif // TABDISPLAY_H
//----------------------------------------------------------------------------------
