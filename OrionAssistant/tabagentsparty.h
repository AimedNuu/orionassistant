/***********************************************************************************
**
** TabAgentsParty.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABAGENTSPARTY_H
#define TABAGENTSPARTY_H
//----------------------------------------------------------------------------------
#include <QWidget>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabAgentsParty;
}
//----------------------------------------------------------------------------------
class TabAgentsParty : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_PartyAutoAccept_clicked(const QModelIndex &index);

	void on_pb_PartyAutoAcceptCreate_clicked();

	void on_pb_PartyAutoAcceptSave_clicked();

	void on_pb_PartyAutoAcceptRemove_clicked();

	void on_pb_PartyFromTarget_clicked();

	void on_lw_PartyAutoDecline_clicked(const QModelIndex &index);

	void on_pb_PartyAutoDeclineCreate_clicked();

	void on_pb_PartyAutoDeclineSave_clicked();

	void on_pb_PartyAutoDeclineRemove_clicked();

private:
	Ui::TabAgentsParty *ui;

public slots:
	void OnListItemMoved();

public:
	explicit TabAgentsParty(QWidget *parent = 0);
	~TabAgentsParty();

	void SetPartyObjectFromTarget(const uint &serial);

	bool PartyInviteCanBeAutoAccepted(const uint &serial);

	bool PartyInviteCanBeAutoDeclined(const uint &serial);

	void OnDeleteKeyClick(QWidget *widget);

	void LoadParty(const QString &path);

	void SaveParty(const QString &path);
};
//----------------------------------------------------------------------------------
extern TabAgentsParty *g_TabAgentsParty;
//----------------------------------------------------------------------------------
#endif // TABAGENTSPARTY_H
//----------------------------------------------------------------------------------
