// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabAgentsDress.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabagentsdress.h"
#include "ui_tabagentsdress.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include "../GUI/dressitemlistitem.h"
#include "../GameObjects/GameItem.h"
#include "../GameObjects/GamePlayer.h"
#include "../GameObjects/GameWorld.h"
#include "tabmain.h"
#include "tabscripts.h"
//----------------------------------------------------------------------------------
TabAgentsDress *g_TabAgentsDress = nullptr;
//----------------------------------------------------------------------------------
TabAgentsDress::TabAgentsDress(QWidget *parent)
: QWidget(parent), ui(new Ui::TabAgentsDress)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabAgentsDress = this;

	connect(ui->lw_Dress, SIGNAL(itemDropped()), this, SLOT(OnListItemMoved()));
}
//----------------------------------------------------------------------------------
TabAgentsDress::~TabAgentsDress()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabAgentsDress = nullptr;
}
//----------------------------------------------------------------------------------
void TabAgentsDress::OnListItemMoved()
{
	OAFUN_DEBUG("");
	QString path = g_OrionAssistantForm->GetSaveConfigPath();

	if (!path.length())
		return;

	SaveDress(path + "/Dress.coa");
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_lw_Dress_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f109");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	CDressListItem *item = (CDressListItem*)ui->lw_Dress->item(index.row());

	if (item != nullptr)
	{
		ui->le_DressName->setText(item->text());

		ui->lw_DressItems->clear();

		IFOR(i, 0, 25)
		{
			uint serial = item->m_Items[i];

			if (serial)
			{
				CDressItemListItem *obj = new CDressItemListItem("", ui->lw_DressItems);
				obj->SetLayer(i);
				obj->SetSerial(serial);
				obj->UpdateText();
			}
		}
	}
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_pb_DressCreate_clicked()
{
	OAFUN_DEBUG("c28_f110");
	QString name = ui->le_DressName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the dress name!");
		return;
	}

	if (GetDressSet(name) != nullptr)
	{
		QMessageBox::critical(this, "Name is already exists", "Dress name is already exists!");
		return;
	}

	CDressListItem *dress = AddDressSet(ui->le_DressName->text());
	ui->lw_Dress->setCurrentItem(dress);

	IFOR(i, 0, ui->lw_DressItems->count())
	{
		CDressItemListItem *item = (CDressItemListItem*)ui->lw_DressItems->item(i);

		if (item != nullptr)
		{
			int layer = item->GetLayer();

			if (layer > 0 && layer < 25)
				dress->m_Items[layer] = item->GetSerial();
		}
	}
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_pb_DressSave_clicked()
{
	OAFUN_DEBUG("c28_f111");
	QString name = ui->le_DressName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the dress name!");
		return;
	}

	CDressListItem *selected = (CDressListItem*)ui->lw_Dress->currentItem();

	if (selected != nullptr)
	{
		QListWidgetItem *found = GetDressSet(name);

		if (found != nullptr && found != selected)
		{
			QMessageBox::critical(this, "Name is already exists", "Dress name is already exists (not this item)!");
			return;
		}
	}
	else
	{
		QMessageBox::critical(this, "Dress set is not selected", "Dress set is not selected!");
		return;
	}

	selected->setText(ui->le_DressName->text());

	memset(&selected->m_Items[0], 0, sizeof(selected->m_Items));

	IFOR(i, 0, ui->lw_DressItems->count())
	{
		CDressItemListItem *item = (CDressItemListItem*)ui->lw_DressItems->item(i);

		if (item != nullptr)
		{
			int layer = item->GetLayer();

			if (layer > 0 && layer < 25)
				selected->m_Items[layer] = item->GetSerial();
		}
	}
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_pb_DressRemove_clicked()
{
	OAFUN_DEBUG("c28_f112");
	QListWidgetItem *item = ui->lw_Dress->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_Dress->takeItem(ui->lw_Dress->row(item));

		if (item != nullptr)
		{
			delete item;

			ui->lw_DressItems->clear();
		}
	}
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_pb_DressUse_clicked()
{
	OAFUN_DEBUG("c28_f113");
	DressSet((CDressListItem*)ui->lw_Dress->currentItem());
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_pb_DressUndress_clicked()
{
	OAFUN_DEBUG("c28_f114");
	UndressSet();
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_pb_DressSetCurrent_clicked()
{
	OAFUN_DEBUG("c28_f117");
	if (g_Player != nullptr && ui->lw_Dress->currentItem() != nullptr)
	{
		QFOR(item, g_Player->m_Items, CGameItem*)
		{
			int layer = item->GetEquipLayer();

			if (layer && layer < OL_MOUNT && layer != OL_BACKPACK && g_LayerSafe[layer])
				AddDressItemInList((CDressListItem*)ui->lw_Dress->currentItem(), layer, item->GetSerial());
		}
	}
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_pb_DressApplyHands_clicked()
{
	OAFUN_DEBUG("c28_f118");
	if (g_Player != nullptr && ui->lw_Dress->currentItem() != nullptr)
	{
		QFOR(item, g_Player->m_Items, CGameItem*)
		{
			int layer = item->GetEquipLayer();

			if (layer && layer <= OL_2_HAND)
				AddDressItemInList((CDressListItem*)ui->lw_Dress->currentItem(), layer, item->GetSerial());
		}
	}
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_pb_DressApplyArmor_clicked()
{
	OAFUN_DEBUG("c28_f119");
	if (g_Player != nullptr && ui->lw_Dress->currentItem() != nullptr)
	{
		QFOR(item, g_Player->m_Items, CGameItem*)
		{
			int layer = item->GetEquipLayer();

			if (layer > OL_2_HAND && layer < OL_MOUNT && layer != OL_BACKPACK && g_LayerSafe[layer])
				AddDressItemInList((CDressListItem*)ui->lw_Dress->currentItem(), layer, item->GetSerial());
		}
	}
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_pb_DressClearSet_clicked()
{
	OAFUN_DEBUG("c28_f120");
	ui->lw_DressItems->clear();
}
//----------------------------------------------------------------------------------
void TabAgentsDress::on_pb_DressFromTarget_clicked()
{
	OAFUN_DEBUG("c28_f121");
	g_TargetHandler = &COrionAssistant::HandleTargetDressItem;

	g_OrionAssistant.RequestTarget("Select a dress item");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
void TabAgentsDress::AddDressItemInList(CGameItem *item)
{
	OAFUN_DEBUG("c28_f115");
	if (item != nullptr)
		AddDressItemInList((CDressListItem*)ui->lw_Dress->currentItem(), item->GetEquipLayer(), item->GetSerial());
}
//----------------------------------------------------------------------------------
void TabAgentsDress::AddDressItemInList(CDressListItem *list, const int &layer, const uint &serial)
{
	OAFUN_DEBUG("c28_f116");
	CDressListItem *selected = (CDressListItem*)ui->lw_Dress->currentItem();

	if (list == selected)
	{
		IFOR(i, 0, ui->lw_DressItems->count())
		{
			CDressItemListItem *listItem = (CDressItemListItem*)ui->lw_DressItems->item(i);

			if (listItem != nullptr && listItem->GetLayer() == layer)
			{
				listItem->SetSerial(serial);
				listItem->UpdateText();
				return;
			}
		}

		CDressItemListItem *listItem = new CDressItemListItem("", ui->lw_DressItems);
		listItem->SetLayer(layer);
		listItem->SetSerial(serial);
		listItem->UpdateText();
	}
	else
		list->m_Items[layer] = serial;
}
//----------------------------------------------------------------------------------
void TabAgentsDress::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_Dress) //Dress
		on_pb_DressRemove_clicked();
	else if (widget == ui->lw_DressItems) //Dress Item
	{
		QListWidgetItem *item = ui->lw_DressItems->currentItem();

		if (item != nullptr)
		{
			item = ui->lw_DressItems->takeItem(ui->lw_DressItems->row(item));

			if (item != nullptr)
				delete item;
		}
	}
}
//----------------------------------------------------------------------------------
bool TabAgentsDress::UndressBeforeDressing()
{
	OAFUN_DEBUG("c28_f10");
	return ui->cb_DressUndressBeforeDress->isChecked();
}
//----------------------------------------------------------------------------------
bool TabAgentsDress::DoubleClickForEquipItems()
{
	OAFUN_DEBUG("");
	return ui->cb_DoubleClickForEquipment->isChecked();
}
//----------------------------------------------------------------------------------
void TabAgentsDress::LoadDress(const QString &path)
{
	OAFUN_DEBUG("c28_f125");
	ui->lw_Dress->clear();

	CFileObject file(path);

	if (file.Open(QIODevice::ReadOnly))
	{
		file.ReadUInt(); //version

		int count = file.ReadInt();

		ui->cb_DressUndressBeforeDress->setChecked(file.ReadUChar());

		IFOR(i, 0, count)
		{
			QString name = file.ReadString();

			if (!name.length())
				continue;

			CDressListItem *item = new CDressListItem(name, ui->lw_Dress);

			uchar layerCount = file.ReadUChar();

			IFOR(j, 0, layerCount)
			{
				uchar layer = file.ReadUChar();
				uint serial = file.ReadUInt();

				if (layer < 25)
					item->m_Items[layer] = serial;
			}
		}

		ui->cb_DoubleClickForEquipment->setChecked(file.ReadUChar());

		file.Close();
	}
}
//----------------------------------------------------------------------------------
void TabAgentsDress::SaveDress(const QString &path)
{
	OAFUN_DEBUG("c28_f134");
	CFileObject file(path);

	if (file.Open(QIODevice::WriteOnly))
	{
		file.WriteUInt(0); //version

		int count = ui->lw_Dress->count();

		file.WriteInt(count);

		file.WriteUChar((uchar)ui->cb_DressUndressBeforeDress->isChecked());

		IFOR(i, 0, count)
		{
			CDressListItem *item = (CDressListItem*)ui->lw_Dress->item(i);

			if (item != nullptr)
			{
				file.WriteString(item->text());

				int layersCount = 0;

				IFOR(layer, 0, 25)
				{
					if (item->m_Items[layer])
						layersCount++;
				}

				file.WriteUChar(layersCount);

				IFOR(layer, 0, 25)
				{
					uint serial = item->m_Items[layer];

					if (serial != 0)
					{
						file.WriteUChar((uchar)layer);
						file.WriteUInt(serial);
					}
				}
			}
		}

		file.WriteUChar((uchar)ui->cb_DoubleClickForEquipment->isChecked());

		file.Close();
	}
}
//----------------------------------------------------------------------------------
void TabAgentsDress::ClearDressSet(CDressListItem *list)
{
	OAFUN_DEBUG("c28_f148");
	if (list == ui->lw_Dress->currentItem())
		ui->lw_DressItems->clear();

	memset(&list->m_Items[0], 0, sizeof(list->m_Items));
}
//----------------------------------------------------------------------------------
CDressListItem *TabAgentsDress::GetDressSet(const QString &name)
{
	OAFUN_DEBUG("c28_f149");
	IFOR(i, 0, ui->lw_Dress->count())
	{
		QListWidgetItem *item = ui->lw_Dress->item(i);

		if (item != nullptr && item->text().toLower() == name)
			return (CDressListItem*)item;
	}

	return nullptr;
}
//----------------------------------------------------------------------------------
CDressListItem *TabAgentsDress::AddDressSet(const QString &name)
{
	OAFUN_DEBUG("c28_f150");
	return new CDressListItem(name, ui->lw_Dress);
}
//----------------------------------------------------------------------------------
void TabAgentsDress::DressSet(CDressListItem *dressSet)
{
	OAFUN_DEBUG("c28_f151");
	if (dressSet == nullptr)
		return;

	QString script = "function " + g_EquipScriptName + "()\n{\n";
	int moveItemsDelay = g_TabMain->MoveItemsDelay();
	bool ignoreLayer1_2 = false;

	if (UndressBeforeDressing())
	{
		IFOR(layer, 1, 3)
		{
			uint serial = dressSet->m_Items[layer];

			if (serial != 0)
			{
				CGameItem *obj = g_Player->FindLayer(layer);

				if (obj != nullptr && obj->GetSerial() != serial)
				{
					ignoreLayer1_2 = true;

					QString buf = "";

					if (moveItemsDelay)
						buf.sprintf("Orion.Unequip(%i);\nOrion.Wait(%i);\n", layer, moveItemsDelay);
					else
						buf.sprintf("Orion.Unequip(%i);\n", layer);

					script += buf;
				}
			}
		}
	}

	const char *equipModeName[10] = { "Equip", "UseObject" };
	const char *equipMode = equipModeName[ui->cb_DoubleClickForEquipment->isChecked() ? 1 : 0];

	IFOR(layer, 1, 25)
	{
		uint serial = dressSet->m_Items[layer];

		if (serial != 0 && g_LayerSafe[layer])
		{
			if (g_World->FindWorldItem(serial) != nullptr)
			{
				CGameItem *obj = g_Player->FindLayer(layer);

				QString buf = "";
				bool equipped = false;

				if (obj != nullptr)
				{
					equipped = (obj->GetSerial() == serial);

					if (UndressBeforeDressing() && !equipped)
					{
						if (layer > 2 || !ignoreLayer1_2)
						{
							if (moveItemsDelay)
								buf.sprintf("Orion.Unequip(%i);\nOrion.Wait(%i);\n", layer, moveItemsDelay);
							else
								buf.sprintf("Orion.Unequip(%i);\n", layer);

							script += buf;
						}
					}
				}

				if (!equipped)
				{
					if (moveItemsDelay)
						buf.sprintf("Orion.%s(0x%08X);\nOrion.Wait(%i);\n", equipMode, serial, moveItemsDelay);
					else
						buf.sprintf("Orion.%s(0x%08X);\n", equipMode, serial);

					script += buf;
				}
			}
		}
	}

	script += "\n}\n";

	g_TabScripts->TerminateScript(g_EquipScriptName, "");
	g_TabScripts->RunScript(g_EquipScriptName, script, 0, 0, QStringList());
}
//----------------------------------------------------------------------------------
void TabAgentsDress::DisarmSet()
{
	OAFUN_DEBUG("c28_f152");
	QString script = "function " + g_EquipScriptName + "()\n{\n";
	int moveItemsDelay = g_TabMain->MoveItemsDelay();

	IFOR(layer, 1, 3)
	{
		CGameItem *item = g_Player->FindLayer(layer);

		if (item != nullptr)
		{
			QString buf = "";

			if (moveItemsDelay)
				buf.sprintf("Orion.Unequip(%i);\nOrion.Wait(%i);\n", layer, moveItemsDelay);
			else
				buf.sprintf("Orion.Unequip(%i);\n", layer);

			script += buf;
		}
	}

	script += "\n}\n";

	g_TabScripts->TerminateScript(g_EquipScriptName, "");
	g_TabScripts->RunScript(g_EquipScriptName, script, 0, 0, QStringList());
}
//----------------------------------------------------------------------------------
void TabAgentsDress::UndressSet()
{
	OAFUN_DEBUG("c28_f153");
	QString script = "function " + g_EquipScriptName + "()\n{\n";
	int moveItemsDelay = g_TabMain->MoveItemsDelay();

	IFOR(layer, 3, 25)
	{
		if (!g_LayerSafe[layer])
			continue;

		CGameItem *item = g_Player->FindLayer(layer);

		if (item != nullptr)
		{
			QString buf = "";

			if (moveItemsDelay)
				buf.sprintf("Orion.Unequip(%i);\nOrion.Wait(%i)\n", layer, moveItemsDelay);
			else
				buf.sprintf("Orion.Unequip(%i);\n", layer);

			script += buf;
		}
	}

	script += "\n}\n";

	g_TabScripts->TerminateScript(g_EquipScriptName, "");
	g_TabScripts->RunScript(g_EquipScriptName, script, 0, 0, QStringList());
}
//----------------------------------------------------------------------------------
