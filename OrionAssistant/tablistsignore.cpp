// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabListsIgnore.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tablistsignore.h"
#include "ui_tablistsignore.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
#include "../GUI/ignorelistitemlistitem.h"
#include "../CommonItems/objectnamereceiver.h"
#include "../GameObjects/GameObject.h"
//----------------------------------------------------------------------------------
TabListsIgnore *g_TabListsIgnore = nullptr;
//----------------------------------------------------------------------------------
TabListsIgnore::TabListsIgnore(QWidget *parent)
: QWidget(parent), ui(new Ui::TabListsIgnore)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabListsIgnore = this;

	connect(ui->lw_Ignore, SIGNAL(itemDropped()), this, SLOT(SaveIgnoreList()));
	connect(ui->lw_IgnoreContent, SIGNAL(itemDropped()), this, SLOT(SaveIgnoreList()));
}
//----------------------------------------------------------------------------------
TabListsIgnore::~TabListsIgnore()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabListsIgnore = nullptr;
}
//----------------------------------------------------------------------------------
void TabListsIgnore::on_lw_Ignore_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f184");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	CIgnoreListItem *item = (CIgnoreListItem*)ui->lw_Ignore->item(index.row());

	if (item != nullptr)
	{
		ui->le_IgnoreName->setText(item->text());

		ui->lw_IgnoreContent->clear();

		QList<CIgnoreItem> &list = item->m_Items;

		if (!list.empty())
		{
			for (const CIgnoreItem &i : list)
			{
				CIgnoreListItemListItem *obj = new CIgnoreListItemListItem(ui->lw_IgnoreContent);
				obj->SetSerial(i.GetSerial());
				obj->SetGraphic(i.GetGraphic());
				obj->SetColor(i.GetColor());
				obj->SetComment(i.GetComment());
				obj->UpdateText();
			}
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsIgnore::on_pb_IgnoreCreate_clicked()
{
	OAFUN_DEBUG("c28_f185");
	QString name = ui->le_IgnoreName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the ignore list name!");
		return;
	}

	IFOR(i, 0, ui->lw_Ignore->count())
	{
		QListWidgetItem *item = ui->lw_Ignore->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			QMessageBox::critical(this, "Name is already exists", "Ignore list name is already exists!");
			return;
		}
	}

	ui->lw_IgnoreContent->clear();

	new CIgnoreListItem(ui->le_IgnoreName->text(), ui->lw_Ignore);

	ui->lw_Ignore->setCurrentRow(ui->lw_Ignore->count() - 1);

	SaveIgnoreList();
}
//----------------------------------------------------------------------------------
void TabListsIgnore::on_pb_IgnoreSave_clicked()
{
	OAFUN_DEBUG("c28_f186");
	QString name = ui->le_IgnoreName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the ignore list name!");
		return;
	}

	CIgnoreListItem *selected = (CIgnoreListItem*)ui->lw_Ignore->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "List is not selected", "List is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_Ignore->count())
	{
		QListWidgetItem *item = ui->lw_Ignore->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Name is already exists", "Ignore list name is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->setText(ui->le_IgnoreName->text());

	QList<CIgnoreItem> &list = selected->m_Items;

	list.clear();

	for (int i = 0; i < ui->lw_IgnoreContent->count(); i++)
	{
		CIgnoreListItemListItem *item = (CIgnoreListItemListItem*)ui->lw_IgnoreContent->item(i);

		CIgnoreItem ii(item->GetSerial(), item->GetGraphic(), item->GetColor());
		ii.SetComment(item->GetComment());

		list.push_back(ii);
	}

	SaveIgnoreList();
}
//----------------------------------------------------------------------------------
void TabListsIgnore::on_pb_IgnoreRemove_clicked()
{
	OAFUN_DEBUG("c28_f187");
	QListWidgetItem *item = ui->lw_Ignore->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_Ignore->takeItem(ui->lw_Ignore->row(item));

		if (item != nullptr)
		{
			delete item;

			ui->lw_IgnoreContent->clear();

			SaveIgnoreList();
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsIgnore::on_lw_IgnoreContent_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f188");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents() || ui->lw_Ignore->currentItem() == nullptr)
		return;

	CIgnoreListItemListItem *item = (CIgnoreListItemListItem*)ui->lw_IgnoreContent->item(index.row());

	if (item != nullptr)
	{
		ui->le_IgnoreContentSerial->setText(item->GetSerial());
		ui->le_IgnoreContentGraphic->setText(item->GetGraphic());
		ui->le_IgnoreContentColor->setText(item->GetColor());
		ui->le_IgnoreContentComment->setText(item->GetComment());
	}
}
//----------------------------------------------------------------------------------
void TabListsIgnore::on_pb_IgnoreContentFromTarget_clicked()
{
	OAFUN_DEBUG("c28_f192");
	if (ui->lw_Ignore->currentItem() == nullptr)
		return;

	g_TargetHandler = &COrionAssistant::HandleTargetIgnoreList;

	g_OrionAssistant.RequestTarget("Select a object for obtain graphic and color");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
void TabListsIgnore::on_pb_IgnoreContentCreate_clicked()
{
	OAFUN_DEBUG("c28_f189");
	if (ui->lw_Ignore->currentItem() == nullptr)
		return;

	QString graphic = ui->le_IgnoreContentGraphic->text();

	if (!graphic.length())
		graphic = "0xFFFF";

	QString color = ui->le_IgnoreContentColor->text();

	if (!color.length())
		color = "0xFFFF";

	QString serial = ui->le_IgnoreContentSerial->text();

	if (!serial.length())
		serial = "0";

	CIgnoreListItemListItem *obj = new CIgnoreListItemListItem(ui->lw_IgnoreContent);
	obj->SetSerial(serial);
	obj->SetGraphic(graphic);
	obj->SetColor(color);
	obj->SetComment(ui->le_IgnoreContentComment->text());
	obj->UpdateText();

	ui->lw_IgnoreContent->setCurrentRow(ui->lw_IgnoreContent->count() - 1);

	on_pb_IgnoreSave_clicked();
}
//----------------------------------------------------------------------------------
void TabListsIgnore::on_pb_IgnoreContentSave_clicked()
{
	OAFUN_DEBUG("c28_f190");
	if (ui->lw_Ignore->currentItem() == nullptr)
		return;

	QString graphic = ui->le_IgnoreContentGraphic->text();

	if (!graphic.length())
		graphic = "0xFFFF";

	QString color = ui->le_IgnoreContentColor->text();

	if (!color.length())
		color = "0xFFFF";

	QString serial = ui->le_IgnoreContentSerial->text();

	if (!serial.length())
		serial = "0";

	CIgnoreListItemListItem *obj = (CIgnoreListItemListItem*)ui->lw_IgnoreContent->currentItem();

	if (obj != nullptr)
	{
		obj->SetSerial(serial);
		obj->SetGraphic(graphic);
		obj->SetColor(color);
		obj->SetComment(ui->le_IgnoreContentComment->text());
		obj->UpdateText();

		on_pb_IgnoreSave_clicked();
	}
	else
	{
		QMessageBox::critical(this, "Item is not selected", "Item is not selected!");
		return;
	}
}
//----------------------------------------------------------------------------------
void TabListsIgnore::on_pb_IgnoreContentRemove_clicked()
{
	OAFUN_DEBUG("c28_f191");
	if (ui->lw_Ignore->currentItem() == nullptr)
		return;

	QListWidgetItem *item = ui->lw_IgnoreContent->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_IgnoreContent->takeItem(ui->lw_IgnoreContent->row(item));

		if (item != nullptr)
		{
			delete item;

			on_pb_IgnoreSave_clicked();
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsIgnore::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_Ignore)
		on_pb_IgnoreRemove_clicked();
	else if (widget == ui->lw_IgnoreContent)
		on_pb_IgnoreContentRemove_clicked();
}
//----------------------------------------------------------------------------------
void TabListsIgnore::LoadIgnoreList()
{
	OAFUN_DEBUG("c28_f127");
	ui->lw_Ignore->clear();

	QFile file(g_DllPath + "/GlobalConfig/IgnoreList.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;
		int count = 0;

		Q_UNUSED(version);
		Q_UNUSED(count);

		CIgnoreListItem *current = nullptr;

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("size"))
						count = attributes.value("size").toInt();
				}
				else if (reader.name() == "ignorelist")
				{
					if (attributes.hasAttribute("name"))
						current = new CIgnoreListItem(attributes.value("name").toString(), ui->lw_Ignore);
				}
				else if (reader.name() == "item" && current != nullptr)
				{
					if (attributes.hasAttribute("serial") && attributes.hasAttribute("graphic") && attributes.hasAttribute("color"))
					{
						CIgnoreItem ii(attributes.value("serial").toString(), attributes.value("graphic").toString(), attributes.value("color").toString());

						if (attributes.hasAttribute("comment"))
							ii.SetComment(attributes.value("comment").toString());

						current->m_Items.push_back(ii);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsIgnore::SaveIgnoreList()
{
	OAFUN_DEBUG("c28_f136");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/IgnoreList.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_Ignore->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");
		writter.writeAttribute("size", QString::number(count + 1));

		IFOR(i, 0, count)
		{
			CIgnoreListItem *item = (CIgnoreListItem*)ui->lw_Ignore->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("ignorelist");

				writter.writeAttribute("name", item->text());

				QList<CIgnoreItem> &list = item->m_Items;

				if (!list.empty())
				{
					for (const CIgnoreItem &fi : list)
					{
						writter.writeStartElement("item");

						writter.writeAttribute("serial", fi.GetSerial());
						writter.writeAttribute("graphic", fi.GetGraphic());
						writter.writeAttribute("color", fi.GetColor());
						writter.writeAttribute("comment", fi.GetComment());

						writter.writeEndElement(); //item
					}
				}

				writter.writeEndElement(); //ignorelist
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsIgnore::AddIgnoreListItemInList(class CGameObject *obj)
{
	OAFUN_DEBUG("c28_f193");
	if (obj != nullptr)
	{
		ui->le_IgnoreContentGraphic->setText(COrionAssistant::GraphicToText(obj->GetGraphic()));
		ui->le_IgnoreContentColor->setText(COrionAssistant::GraphicToText(obj->GetColor()));

		if (obj->GetName().length())
			ui->le_IgnoreContentComment->setText(obj->GetName());
		else
		{
			g_OrionAssistant.Click(obj->GetSerial());
			new CObjectNameReceiver(obj->GetSerial(), ui->le_IgnoreContentComment);
		}
	}
}
//----------------------------------------------------------------------------------
CIgnoreListItem *TabListsIgnore::GetIgnoreList(QString name)
{
	OAFUN_DEBUG("c28_f199");
	name = name.toLower();

	IFOR(i, 0, ui->lw_Ignore->count())
	{
		QListWidgetItem *item = ui->lw_Ignore->item(i);

		if (item != nullptr && item->text().toLower() == name)
			return (CIgnoreListItem*)item;
	}

	return nullptr;
}
//----------------------------------------------------------------------------------
void TabListsIgnore::AddIgnoreList(QString listName, const QString &serial, const QString &comment)
{
	OAFUN_DEBUG("c28_f194");
	if (!listName.length())
	{
		on_pb_IgnoreContentFromTarget_clicked();
		return;
	}

	QString name = listName.toLower();

	CIgnoreListItem *item = nullptr;

	IFOR(i, 0, ui->lw_Ignore->count())
	{
		item = (CIgnoreListItem*)ui->lw_Ignore->item(i);

		if (item != nullptr && item->text().toLower() == name)
			break;
	}

	if (item == nullptr)
		item = new CIgnoreListItem(listName, ui->lw_Ignore);

	if (!item->m_Items.empty())
	{
		for (const CIgnoreItem &ii : item->m_Items)
		{
			if (ii.GetSerial() == serial)
				return;
		}
	}

	CIgnoreItem ii(serial, "0", "0");
	ii.SetComment(comment);
	item->m_Items.push_back(ii);

	SaveIgnoreList();
}
//----------------------------------------------------------------------------------
void TabListsIgnore::AddIgnoreList(QString listName, const QString &graphic, const QString &color, const QString &comment)
{
	OAFUN_DEBUG("c28_f195");
	QString name = listName.toLower();

	CIgnoreListItem *item = nullptr;

	IFOR(i, 0, ui->lw_Ignore->count())
	{
		item = (CIgnoreListItem*)ui->lw_Ignore->item(i);

		if (item != nullptr && item->text().toLower() == name)
			break;
	}

	if (item == nullptr)
		item = new CIgnoreListItem(listName, ui->lw_Ignore);

	if (!item->m_Items.empty())
	{
		for (const CIgnoreItem &ii : item->m_Items)
		{
			if (ii.GetGraphic() == graphic && ii.GetColor() == color)
				return;
		}
	}

	CIgnoreItem ii("0", graphic, color);
	ii.SetComment(comment);
	item->m_Items.push_back(ii);

	SaveIgnoreList();
}
//----------------------------------------------------------------------------------
void TabListsIgnore::ClearIgnoreList(QString listName)
{
	OAFUN_DEBUG("c28_f196");
	listName = listName.toLower();

	IFOR(i, 0, ui->lw_Ignore->count())
	{
		QListWidgetItem *item = ui->lw_Ignore->item(i);

		if (item != nullptr && item->text().toLower() == listName)
		{
			if (item == ui->lw_Ignore->currentItem())
				ui->lw_IgnoreContent->clear();

			item = ui->lw_Ignore->takeItem(i);

			delete item;

			SaveIgnoreList();

			break;
		}
	}
}
//----------------------------------------------------------------------------------
