// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** MainScript.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "mainscript.h"
#include <QApplication>
#include "../Managers/PacketManager.h"
#include "../Managers/displaymanager.h"
#include "../GameObjects/gameworld.h"
#include "../GameObjects/gameplayer.h"
#include "orionassistant.h"
#include "../GUI/hotkeybox.h"
#include "../Managers/trademanager.h"
#include "../Managers/menumanager.h"
#include "../Managers/commandmanager.h"
#include "Target.h"
#include "tabmain.h"
#include "tabdisplay.h"
#include "tabscripts.h"
#include "objectinspectorform.h"
#include "../Managers/TextParser.h"
#include "tabmain.h"
#include "orionmap.h"
#include "hotkeysform.h"
//----------------------------------------------------------------------------------
BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpvReserved)
{
	Q_UNUSED(hInstance);
	Q_UNUSED(lpvReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		g_HInstance = hInstance;

		char pathBuf[MAX_PATH] = {0};

		GetModuleFileNameA(g_HInstance, pathBuf, MAX_PATH);

		char *bf = strrchr(pathBuf, '\\');

		if (bf)
			*bf = 0;

		g_DllPath = pathBuf;

		if (bf)
		{
			bf = strrchr(pathBuf, '\\');

			if (bf)
				*bf = 0;

			g_ClientPath = pathBuf;
		}

		//qDebug() << "g_DllPath:" << g_DllPath;

		QFile file(g_ClientPath + "/uo_debug.cfg");

		if (file.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			CTextParser parser(file.readAll().toStdString(), "=", "#;//", "");

			while (!parser.IsEOF())
			{
				QStringList list = parser.ReadTokens();

				if (list.size() < 2)
					continue;

				if (list[0].toLower() == "custompath")
				{
					g_ClientPath = list[1];
					break;
				}
			}
		}

		//qDebug() << "g_ClientPath:" << g_ClientPath;

		int argsCount = 0;
		LPWSTR *args = CommandLineToArgvW(GetCommandLineW(), &argsCount);

		IFOR(i, 0, argsCount)
		{
			if (!args[i] || *args[i] != L'-')
				continue;

			QString str = QString::fromWCharArray(args[i] + 1);

			if (str.toLower() == "aero")
				g_SaveAero = true;
		}

		LocalFree(args);

		int argc = 0;
		const char **argv = nullptr;

		new QApplication(argc, (char**)&argv);

		INITLOGGER("OA\\oa_log.txt");
	}

	return TRUE;
}
//----------------------------------------------------------------------------------
void TraceFilePrint(FILE *file, const char *format, ...)
{
	va_list arg;
	va_start(arg, format);
	vfprintf(file, format, arg);
	va_end(arg);
	fflush(file);
}
//----------------------------------------------------------------------------------
LRESULT ClientMessageQueue(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	OAFUN_DEBUG("c_f1");
	LRESULT res = 1;
	static uint keyToEat = 0;

	switch (msg)
	{
		case WM_CLOSE:
		{
			if (g_OrionAssistantForm != nullptr)
				g_OrionAssistantForm->EndWork();

			break;
		}
		case WM_MBUTTONDOWN:
		{
			g_HotkeysForm->HotkeyExistsAndUse(MOUSE_WHELL_PRESS, 0, HM_MOUSE_WHELL);
			res = 0;

			break;
		}
		case WM_MOUSEWHEEL:
		{
			if (wParam == 0x11110000)
				g_HotkeysForm->HotkeyExistsAndUse(MOUSE_WHELL_SCROLL_UP, 0, HM_MOUSE_WHELL);
			else
				g_HotkeysForm->HotkeyExistsAndUse(MOUSE_WHELL_SCROLL_DOWN, 0, HM_MOUSE_WHELL);

			res = 0;

			break;
		}
		case WM_XBUTTONDOWN:
		{
			if (wParam == 0x11110000)
				g_HotkeysForm->HotkeyExistsAndUse(MOUSE_X_UP, 0, HM_MOUSE_X);
			else
				g_HotkeysForm->HotkeyExistsAndUse(MOUSE_X_DOWN, 0, HM_MOUSE_X);

			res = 0;

			break;
		}
		case WM_CHAR:
		{
			if (keyToEat && (uint)lParam == keyToEat)
				keyToEat = 0;
			else
				res = 0;

			break;
		}
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
		{
			res = (LRESULT)g_HotkeysForm->HotkeyExistsAndUse(wParam, lParam, HM_NONE);

			if (res)
				keyToEat = lParam;

			break;
		}
		case WM_KEYUP:
		case WM_SYSKEYUP:
		{
			res = 0;
			break;
		}
		case WM_NCPAINT:
		{
			res = DefWindowProc(hwnd, msg, wParam, lParam);
		}
		case WM_NCACTIVATE:
		{
			g_TabDisplay->RedrawTitle();

			break;
		}
		case UOMSG_SET_SERVER_NAME:
		{
			if (g_ServerName != (char*)wParam)
			{
				g_ServerName = (char*)wParam;

				g_TabDisplay->RedrawTitle();
			}

			break;
		}
		case UOMSG_SET_PLAYER_NAME:
		{
			if (g_CharacterName != (char*)wParam)
			{
				g_CharacterName = (char*)wParam;

				g_OrionAssistantForm->UpdateAssistTitle(" - " + g_CharacterName);
				g_TabDisplay->RedrawTitle();
			}

			break;
		}
		case UOMSG_UPDATE_PLAYER_XYZ:
		{
			if (g_Player != NULL)
			{
				PUOI_PLAYER_XYZ_DATA xyzData = (PUOI_PLAYER_XYZ_DATA)wParam;

				g_Player->SetX(xyzData->X);
				g_Player->SetY(xyzData->Y);
				g_Player->SetZ(xyzData->Z);

				if (g_OrionMap != nullptr && g_OrionMap->isVisible())
					g_OrionMap->SetWantRedraw(true);

				if (g_Player->Hidden() && g_TabMain->StealthStepCounter())
				{
					int counter = g_Player->GetStealthSteps() + 1;

					g_Player->SetStealthSteps(counter);

					g_OrionAssistant.ClientPrint("Stealth steps: " + QString::number(counter));
				}

				if (g_UpdateDisplayOnStep)
					g_TabDisplay->RedrawTitle();

				g_ObjectInspectorForm->CheckUpdate(g_Player);
			}

			break;
		}
		case UOMSG_UPDATE_PLAYER_DIR:
		{
			if (g_Player != NULL)
			{
				g_Player->SetDirection(wParam);

				g_ObjectInspectorForm->CheckUpdate(g_Player);
			}

			break;
		}
		case UOMSG_STATUS_REQUEST:
		{
			g_LastStatusRequest = (uint)wParam;
			break;
		}
		case UOMSG_SELECTED_TILE:
		{
			memcpy(&g_SelectedClientWorldObject, (PVOID)wParam, sizeof(g_SelectedClientWorldObject));
			break;
		}
		case UOMSG_END_MACRO_PAYING:
		{
			g_ClientMacroPlayed = false;
			break;
		}
		case UOMSG_UPDATE_REMOVE_POS:
		{
			PUOI_PLAYER_XYZ_DATA xyzData = (PUOI_PLAYER_XYZ_DATA)wParam;
			g_RemoveRangeXY.setX(xyzData->X);
			g_RemoveRangeXY.setY(xyzData->Y);
			break;
		}
		default:
			break;
	}

	return res;
}
//----------------------------------------------------------------------------------
void __cdecl OnClientDisconnect()
{
	OAFUN_DEBUG("c_f2");
	g_Target.ClearHooks();
	g_MenuManager.ClearHooks();
	g_MenuManager.ClearMenus();
	g_TradeManager.Clear();

	if (g_World != nullptr)
	{
		delete g_World;
		g_World = nullptr;

		if (g_TabMain != nullptr)
			g_TabMain->StartReconnect();
	}

	g_Connected = false;
}
//----------------------------------------------------------------------------------
void __cdecl OnClientWorldDraw()
{
	OAFUN_DEBUG("c_f3");
}
//----------------------------------------------------------------------------------
void __cdecl OnClientSceneDraw()
{
	OAFUN_DEBUG("c_f4");
}
//----------------------------------------------------------------------------------
void __cdecl OnClientWorldMapDraw()
{
	OAFUN_DEBUG("c_f5");
}
//----------------------------------------------------------------------------------
bool __cdecl DllRecv(unsigned char *buf, const int &size)
{
	OAFUN_DEBUG("c_f6");
	return g_PacketManager.PacketRecv(buf, size);
}
//----------------------------------------------------------------------------------
bool __cdecl DllSend(unsigned char *buf, const int &size)
{
	OAFUN_DEBUG("c_f7");
	return g_PacketManager.PacketSend(buf, size);
}
//----------------------------------------------------------------------------------
