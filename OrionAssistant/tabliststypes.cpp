// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabListsTypes.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabliststypes.h"
#include "ui_tabliststypes.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include "../GUI/typelistitem.h"
#include "orionassistantform.h"
#include "orionassistant.h"
#include <QDir>
//----------------------------------------------------------------------------------
TabListsTypes *g_TabListsTypes = nullptr;
//----------------------------------------------------------------------------------
TabListsTypes::TabListsTypes(QWidget *parent)
: QWidget(parent), ui(new Ui::TabListsTypes)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabListsTypes = this;

	connect(ui->lw_Types, SIGNAL(itemDropped()), this, SLOT(SaveTypes()));
}
//----------------------------------------------------------------------------------
TabListsTypes::~TabListsTypes()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabListsTypes = nullptr;
}
//----------------------------------------------------------------------------------
void TabListsTypes::on_lw_Types_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	QListWidgetItem *item = ui->lw_Types->item(index.row());

	if (item != nullptr)
	{
		CTypeListItem *oli = (CTypeListItem*)item;
		ui->le_TypeName->setText(oli->text());
		ui->le_TypeGraphic->setText(COrionAssistant::GraphicToText(oli->GetType()));
	}
}
//----------------------------------------------------------------------------------
void TabListsTypes::on_pb_TypeFromTarget_clicked()
{
	OAFUN_DEBUG("");
	g_TargetHandler = &COrionAssistant::HandleTargetType;

	g_OrionAssistant.RequestTarget("Select a object for obtain type");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
void TabListsTypes::on_pb_TypeCreate_clicked()
{
	OAFUN_DEBUG("");
	QString name = ui->le_TypeName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the type name!");
		return;
	}
	else if (!ui->le_TypeGraphic->text().length())
	{
		QMessageBox::critical(this, "Type is empty", "Enter the type graphic!");
		return;
	}

	IFOR(i, 0, ui->lw_Types->count())
	{
		QListWidgetItem *item = ui->lw_Types->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			QMessageBox::critical(this, "Name is already exists", "Type name is already exists!");
			return;
		}
	}

	new CTypeListItem(ui->le_TypeName->text(), ui->le_TypeGraphic->text(), ui->lw_Types);

	ui->lw_Types->setCurrentRow(ui->lw_Types->count() - 1);
	SaveTypes();
}
//----------------------------------------------------------------------------------
void TabListsTypes::on_pb_TypeSave_clicked()
{
	OAFUN_DEBUG("");
	QString name = ui->le_TypeName->text().toLower();

	if (!name.length())
	{
		QMessageBox::critical(this, "Name is empty", "Enter the type name!");
		return;
	}
	else if (!ui->le_TypeGraphic->text().length())
	{
		QMessageBox::critical(this, "Type is empty", "Enter the type graphic!");
		return;
	}

	CTypeListItem *selected = (CTypeListItem*)ui->lw_Types->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Type is not selected", "Type is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_Types->count())
	{
		QListWidgetItem *item = ui->lw_Types->item(i);

		if (item != nullptr && item->text().toLower() == name)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Name is already exists", "Type name is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->setText(ui->le_TypeName->text());
	selected->SetType((ushort)COrionAssistant::TextToUInt(ui->le_TypeGraphic->text()));
	SaveTypes();
}
//----------------------------------------------------------------------------------
void TabListsTypes::on_pb_TypeRemove_clicked()
{
	OAFUN_DEBUG("");
	QListWidgetItem *item = ui->lw_Types->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_Types->takeItem(ui->lw_Types->row(item));

		if (item != nullptr)
		{
			delete item;
			SaveTypes();
		}
	}
}
//----------------------------------------------------------------------------------
void TabListsTypes::SaveTypes()
{
	OAFUN_DEBUG("");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/Types.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_Types->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");
		writter.writeAttribute("size", QString::number(count));

		IFOR(i, 0, count)
		{
			QListWidgetItem *item = ui->lw_Types->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("type");

				writter.writeAttribute("name", item->text());
				writter.writeAttribute("id", COrionAssistant::GraphicToText(((CTypeListItem*)item)->GetType()));

				writter.writeEndElement(); //type
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabListsTypes::LoadTypes()
{
	OAFUN_DEBUG("");
	ui->lw_Types->clear();

	QFile file(g_DllPath + "/GlobalConfig/Types.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;
		int count = 0;

		Q_UNUSED(version);
		Q_UNUSED(count);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("size"))
						count = attributes.value("size").toInt();
				}
				else if (reader.name() == "type")
				{
					if (attributes.hasAttribute("name") && attributes.hasAttribute("id"))
					{
						new CTypeListItem(attributes.value("name").toString(), attributes.value("id").toString(), ui->lw_Types);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}

	if (!ui->lw_Types->count())
	{
		new CTypeListItem("bm", "0x0F7B", ui->lw_Types);
		new CTypeListItem("bp", "0x0F7A", ui->lw_Types);
		new CTypeListItem("ga", "0x0F84", ui->lw_Types);
		new CTypeListItem("gs", "0x0F85", ui->lw_Types);
		new CTypeListItem("mr", "0x0F86", ui->lw_Types);
		new CTypeListItem("ns", "0x0F88", ui->lw_Types);
		new CTypeListItem("sa", "0x0F8C", ui->lw_Types);
		new CTypeListItem("ss", "0x0F8D", ui->lw_Types);
		new CTypeListItem("en", "0x0F87", ui->lw_Types);
		new CTypeListItem("bandage", "0x0E21", ui->lw_Types);
		new CTypeListItem("bottle", "0x0F0E", ui->lw_Types);
		new CTypeListItem("scroll", "0x0E34", ui->lw_Types);
		new CTypeListItem("map", "0x14EB", ui->lw_Types);
		new CTypeListItem("gp", "0x0EED", ui->lw_Types);

		SaveTypes();
	}
}
//----------------------------------------------------------------------------------
void TabListsTypes::AddType(QString name, const QString &type)
{
	OAFUN_DEBUG("");
	if (!type.length())
	{
		ui->le_TypeName->setText(name);
		g_OrionAssistantForm->SetAutosaveAfterTargetingName(name);

		on_pb_TypeFromTarget_clicked();

		return;
	}

	QString lowerName = name.toLower();

	IFOR(i, 0, ui->lw_Types->count())
	{
		CTypeListItem *item = (CTypeListItem*)ui->lw_Types->item(i);

		if (item != nullptr && item->text().toLower() == lowerName)
		{
			item->setText(name);
			item->SetType(COrionAssistant::TextToGraphic(type));

			if (item == ui->lw_Types->currentItem())
				ui->le_TypeGraphic->setText(type);

			SaveTypes();
			return;
		}
	}

	new CTypeListItem(name, type, ui->lw_Types);
	SaveTypes();
}
//----------------------------------------------------------------------------------
void TabListsTypes::RemoveType(QString name)
{
	OAFUN_DEBUG("");
	QString lowerName = name.toLower();

	IFOR(i, 0, ui->lw_Types->count())
	{
		QListWidgetItem *item = ui->lw_Types->takeItem(i);

		if (item != nullptr && item->text().toLower() == lowerName)
		{
			item = ui->lw_Types->item(i);

			if (item != nullptr)
				delete item;
		}
	}
}
//----------------------------------------------------------------------------------
ushort TabListsTypes::SeekType(const QString &text)
{
	OAFUN_DEBUG("");
	IFOR(i, 0, ui->lw_Types->count())
	{
		QListWidgetItem *item = ui->lw_Types->item(i);

		if (item != nullptr && item->text().toLower() == text)
			return ((CTypeListItem*)item)->GetType();
	}

	return 0;
}
//----------------------------------------------------------------------------------
void TabListsTypes::SetTypeFromTarget(const ushort &graphic)
{
	OAFUN_DEBUG("");
	if (graphic)
	{
		ui->le_TypeGraphic->setText(COrionAssistant::GraphicToText(graphic));

		if (g_OrionAssistantForm->GetAutosaveAfterTargetingName().length())
			AddType(g_OrionAssistantForm->GetAutosaveAfterTargetingName(), ui->le_TypeGraphic->text());
	}

	g_OrionAssistantForm->SetAutosaveAfterTargetingName("");
}
//----------------------------------------------------------------------------------
void TabListsTypes::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_Types) //Types
		on_pb_TypeRemove_clicked();
}
//----------------------------------------------------------------------------------
