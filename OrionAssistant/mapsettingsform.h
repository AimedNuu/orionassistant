/***********************************************************************************
**
** MapSettingsForm.h
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef MAPSETTINGSFORM_H
#define MAPSETTINGSFORM_H
//----------------------------------------------------------------------------------
#include <QDialog>
#include <QCloseEvent>
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class MapSettingsForm;
}
//----------------------------------------------------------------------------------
class CMapFontStyle
{
	SETGET(QString, Name, "FontStyle")
	SETGET(QFont, Font, QFont())

public:
	CMapFontStyle(const QString &name, const QFont &font) : m_Name(name), m_Font(font) {}
	~CMapFontStyle() {}
};
//----------------------------------------------------------------------------------
const int MAP_FONT_STYLES_COUNT = 10;
extern CMapFontStyle g_MapFontStyle[MAP_FONT_STYLES_COUNT];
//----------------------------------------------------------------------------------
class CMapSettings
{
	SETGET(int, WindowX, 0)
	SETGET(int, WindowY, 0)
	SETGET(int, WindowWidth, 0)
	SETGET(int, WindowHeight, 0)
	SETGET(uint, Facet, 0xFFFFFFFF)
	SETGET(bool, FollowPlayer, true)
	SETGET(int, Zoom, 2)
	SETGET(bool, ShowStatics, true)
	SETGET(bool, ApplyAltitude, true)
	SETGET(bool, AltitudeOnly, false)
	SETGET(bool, SmoothImage, true)
	SETGET(int, Opacity, 0)
	SETGET(bool, DisplayCoordinates, true)
	SETGET(bool, ShowOnLogin, false)
	SETGET(bool, ShowLastCorpse, true)
	SETGET(bool, MarkDeadMobiles, true)
	SETGET(bool, RotateMap, true)

	SETGET(int, PlayerFontStyle, 0)
	SETGET(QColor, PlayerColor, QColor(0, 255, 255))
	SETGET(bool, PlayerShowHits, true)
	SETGET(bool, PlayerShowMana, true)
	SETGET(bool, PlayerShowStam, true)
	SETGET(bool, PlayerShowName, true)

	SETGET(bool, PartyEnabled, true)
	SETGET(int, PartyFontStyle, 0)
	SETGET(QColor, PartyColor, QColor(0, 255, 0))
	SETGET(bool, PartyShowHits, true)
	SETGET(bool, PartyShowName, true)

	SETGET(bool, GuildEnabled, true)
	SETGET(int, GuildFontStyle, 0)
	SETGET(QColor, GuildColor, QColor(135, 200, 0))
	SETGET(bool, GuildShowHits, true)
	SETGET(bool, GuildShowName, true)

	SETGET(bool, FriendsEnabled, true)
	SETGET(int, FriendsFontStyle, 0)
	SETGET(QColor, FriendsColor, QColor(255, 255, 0))
	SETGET(bool, FriendsShowHits, true)
	SETGET(bool, FriendsShowName, true)

	SETGET(bool, EnemiesEnabled, true)
	SETGET(int, EnemiesFontStyle, 0)
	SETGET(QColor, EnemiesColor, QColor(255, 0, 0))
	SETGET(bool, EnemiesShowHits, true)
	SETGET(bool, EnemiesShowName, true)

	SETGET(bool, OtherEnabled, false)
	SETGET(int, OtherFontStyle, 0)
	SETGET(QColor, OtherColor, QColor(192, 192, 192))
	SETGET(bool, OtherShowHits, true)
	SETGET(bool, OtherShowName, true)

public:
	CMapSettings() {}
	~CMapSettings() {}

	void Copy(const CMapSettings &settings);

	void Load();

	void Save();
};
//----------------------------------------------------------------------------------
extern CMapSettings g_OrionMapSettings;
//----------------------------------------------------------------------------------
class MapSettingsForm : public QDialog
{
	Q_OBJECT

private:
	Ui::MapSettingsForm *ui;

	bool m_Loading{ false };

	bool m_Changed{ false };

	QImage m_ImageCorpse;
	QPixmap m_PixmapMap;

	CMapSettings m_Settings;

	void OnColorSelection(class QColoredPushButton *button);

private slots:
	void OnComboBoxIndexChanged(int index);
	void OnCheckBoxChecked();

	void on_pb_PlayerColor_clicked();

	void on_pb_PartyColor_clicked();

	void on_pb_GuildColor_clicked();

	void on_pb_FriendsColor_clicked();

	void on_pb_EnemiesColor_clicked();

	void on_pb_OtherColor_clicked();

	void on_pb_ApplyChanges_clicked();

	void on_pb_Close_clicked();

protected:
	virtual void closeEvent(QCloseEvent *event);

	virtual void paintEvent(QPaintEvent *event);

public:
	explicit MapSettingsForm(QWidget *parent = 0);
	~MapSettingsForm();

	void Init(const CMapSettings &settings);
};
//----------------------------------------------------------------------------------
#endif // MAPSETTINGSFORM_H
//----------------------------------------------------------------------------------
