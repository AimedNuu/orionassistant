/***********************************************************************************
**
** TabMacros.h
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABMACROS_H
#define TABMACROS_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../Macros/macro.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabMacros;
}
//----------------------------------------------------------------------------------
class TabMacros : public QWidget
{
	Q_OBJECT

	SETGET(bool, Recording, false)
	SETGET(bool, Playing, false)
	SETGET(bool, Looped, false)
	SETGET(int, LoopDelay, 1000)
	SETGET(uint, NextMacroTick, 0)

private:
	Ui::TabMacros *ui;

	QTimer m_Timer;

	class CMacroListItem *m_PlayingMacroItem{ nullptr };

	QList<CMacro*> m_PlayList;
	int m_PlayPosition;
	bool m_WaitingEnabled{ false };
	bool m_DoNotAutoIterrupt{ false };

	QList<uint> m_ArmList;
	QList<uint> m_DressList;

	void UpdateMacroByList();

	void Play(class CMacroListItem *macroItem, const bool &doNotAutoIterrupt);

	void UpdateMacroList();

	uint FindObject(CMacroWithObject *macro, const MACRO_CONVERT_TYPE &convertType);

private slots:
	void on_pb_Create_clicked();

	void on_pb_Remove_clicked();

	void on_pb_Play_clicked();

	void on_pb_Record_clicked();

	void OnItemDeleted(QListWidget *widget);

	void on_itemRenamed(QListWidgetItem *item);

	void on_pb_ToScript_clicked();

	void on_lw_MacroList_clicked(const QModelIndex &index);

	void on_lw_MacroActions_customContextMenuRequested(const QPoint &pos);

public slots:
	void OnListItemMoved();

	void OnActionListItemMoved();

	void ProcessMacro();

public:
	explicit TabMacros(QWidget *parent = 0);
	~TabMacros();

	void AddAction(class CMacro *macro, const bool &insertion);

	void Reset();

	void Load(const QString &path);

	void Save(const QString &path);

	void AddActionFromMenu(int command, int action);

	void Play(const QString &name, const bool &doNotAutoIterrupt);

	void Stop();

	bool PlayWaiting(const QList<MACRO_TYPE> &list);
};
//----------------------------------------------------------------------------------
extern TabMacros *g_TabMacros;
//----------------------------------------------------------------------------------
#endif // TABMACROS_H
//----------------------------------------------------------------------------------
