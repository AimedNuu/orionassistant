// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ObjectInspectorForm.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "objectinspectorform.h"
#include "ui_objectinspectorform.h"
#include "tabobjectinspector.h"
#include "../GameObjects/GameObject.h"
#include "orionassistant.h"
//----------------------------------------------------------------------------------
ObjectInspectorForm *g_ObjectInspectorForm = nullptr;
//----------------------------------------------------------------------------------
ObjectInspectorForm::ObjectInspectorForm(QWidget *parent)
: QMainWindow(parent), ui(new Ui::ObjectInspectorForm)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);

	connect(ui->tw_Tabs, SIGNAL(tabCloseRequested(int)), this, SLOT(onCloseTab(int)));
}
//----------------------------------------------------------------------------------
ObjectInspectorForm::~ObjectInspectorForm()
{
	OAFUN_DEBUG("");
	delete ui;
}
//----------------------------------------------------------------------------------
TabObjectInspector *ObjectInspectorForm::FindTab(const uint &serial)
{
	OAFUN_DEBUG("");
	DFOR(i, ui->tw_Tabs->count() - 1, 0)
	{
		TabObjectInspector *tab = (TabObjectInspector*)ui->tw_Tabs->widget(i);

		if (tab != nullptr && tab->GetSerial() == serial)
			return tab;
	}

	return nullptr;
}
//----------------------------------------------------------------------------------
void ObjectInspectorForm::CheckUpdate(CGameObject *obj)
{
	OAFUN_DEBUG("");
	if (/*!isVisible() ||*/ obj == nullptr)
		return;

	TabObjectInspector *tab = FindTab(obj->GetSerial());

	if (tab != nullptr)
		tab->CheckUpdate(obj);
}
//----------------------------------------------------------------------------------
void ObjectInspectorForm::Update(CGameObject *obj)
{
	OAFUN_DEBUG("");
	if (obj == nullptr)
		return;

	if (!isVisible())
		show();
	else
		activateWindow();

	TabObjectInspector *tab = FindTab(obj->GetSerial());

	if (tab != nullptr)
	{
		ui->tw_Tabs->setCurrentWidget(tab);
		tab->CheckUpdate(obj);
		return;
	}

	bool removeFirst = false;

	if (!ui->tw_Tabs->count())
	{
		ui->tw_Tabs->addTab(new QWidget(), "");
		removeFirst = true;
	}

	tab = new TabObjectInspector(this);
	ui->tw_Tabs->addTab(tab, COrionAssistant::SerialToText(obj->GetSerial()));
	tab->Update(obj);
	ui->tw_Tabs->setCurrentWidget(tab);

	if (removeFirst)
		emit ui->tw_Tabs->tabCloseRequested(0);
}
//----------------------------------------------------------------------------------
void ObjectInspectorForm::onCloseTab(int index)
{
	OAFUN_DEBUG("");
	if (index >= 0 && index < ui->tw_Tabs->count())
		ui->tw_Tabs->removeTab(index);
}
//----------------------------------------------------------------------------------
