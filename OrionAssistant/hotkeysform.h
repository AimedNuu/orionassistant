/***********************************************************************************
**
** HotkeysForm.h
**
** Copyright (C) December 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef HOTKEYSFORM_H
#define HOTKEYSFORM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "../GUI/coloredpushbutton.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class HotkeysForm;
}
//----------------------------------------------------------------------------------
enum HOTKEY_TYPES
{
	HT_PLAY_MACRO = 0,
	HT_STOP_MACRO,
	HT_SCRIPT,
	HT_EXTERNAL_CODE,
	HT_TOGGLE_HOTKEYS,
	HT_TOGGLE_MAP,
	HT_ATTACK,
	HT_USE_OBJECT,
	HT_ALLNAMES,
	HT_CAST,
	HT_CAST_SELF,
	HT_CAST_LAST_TARGET,
	HT_CAST_LAST_ATTACK,
	HT_CAST_ENEMY,
	HT_CAST_FRIEND,
	HT_CAST_NEW_TARGET_SYSTEM,
	HT_USE_SKILL,
	HT_USE_SKILL_SELF,
	HT_USE_SKILL_LAST_TARGET,
	HT_USE_SKILL_LAST_ATTACK,
	HT_USE_SKILL_ENEMY,
	HT_USE_SKILL_FREIND,
	HT_USE_SKILL_NEW_TARGET_SYSTEM,
	HT_TARGETING,
	HT_BANDAGE_HEALING,
	HT_NEW_TARGETING_SYSTEM_SET,
	HT_COUNT_OF_ITEMS
};
//----------------------------------------------------------------------------------
struct VKYEBOARD_KEYS_TABLE
{
public:
	VKYEBOARD_KEYS_TABLE(const uint &key, const uchar &modifiers)
		: Key(key), Modifiers(modifiers) {}

	uint Key;
	uchar Modifiers;
};
//----------------------------------------------------------------------------------
struct HOTKEY_TYPE_TABLE
{
	QStringList Content;
	QString Name;
};
//----------------------------------------------------------------------------------
class HotkeysForm : public QMainWindow
{
	Q_OBJECT

	SETGET(bool, ShiftPressed, false)
	SETGET(bool, CtrlPressed, false)
	SETGET(bool, AltPressed, false)
	SETGET(bool, AltGrPressed, false)

private:
	Ui::HotkeysForm *ui;

	uchar m_LastUpdateModifiers{ 0 };

	QMap<QColoredPushButton*, VKYEBOARD_KEYS_TABLE> m_ButtonsMap;

	bool m_Loading{ false };
	bool m_WantToSave{ false };

	QString m_DefaultBlankHotkeyData{ "Blank hotkey" };

	QTimer m_Timer;

	QMessageBox m_QuestionMessageBox;

	static HOTKEY_TYPE_TABLE m_HotkeyTypeName[HT_COUNT_OF_ITEMS];

	void UpdateHotkeyData(const int &row);

	void UpdateKeyboard();

	void PlayHotkeyAction(const int &type, int action);

private slots:
	void on_KeyButtonClicked();

	void on_ModifierKeyButtonClicked();

	void on_UpdateTimer();

	void on_cb_Type_currentIndexChanged(int index);

	void on_cb_Action_currentIndexChanged(int index);

	void on_pb_New_clicked();

	void on_pb_Delete_clicked();

	void on_tw_Hotkeys_clicked(const QModelIndex &index);

	void on_cb_RunMode_clicked();

	void on_pte_ExternalCode_textChanged();

public:
	explicit HotkeysForm(QWidget *parent = 0);
	~HotkeysForm();

	void Init();

	void UpdateActionList(const HOTKEY_TYPES &type, const QStringList &list);

	bool CheckExists(const uint &key, const uchar &modifiers);

	bool Load(const QString &path);

	bool Save(const QString &path);

	bool HotkeyExistsAndUse(const uint &wParam, const uint &lParam, uint mod);
};
//----------------------------------------------------------------------------------
extern HotkeysForm *g_HotkeysForm;
//----------------------------------------------------------------------------------
#endif // HOTKEYSFORM_H
//----------------------------------------------------------------------------------
