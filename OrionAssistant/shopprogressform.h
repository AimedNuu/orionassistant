/***********************************************************************************
**
** ShopProgressForm.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SHOPPROGRESSFORM_H
#define SHOPPROGRESSFORM_H
//----------------------------------------------------------------------------------
#include <QDialog>
#include <QTime>
#include <QTimer>
#include <QCloseEvent>
//----------------------------------------------------------------------------------
namespace Ui
{
	class ShopProgressForm;
}
//----------------------------------------------------------------------------------
class ShopProgressForm : public QDialog
{
	Q_OBJECT

private:
	Ui::ShopProgressForm *ui;

	int m_Delay{ 0 };

	QTimer m_Timer;

	QTime m_Time;

	class CPacket *m_Packet{ nullptr };

private slots:
	void on_pb_Cancel_clicked();

	void on_Timer();

protected:
	virtual void closeEvent(QCloseEvent *e);

public:
	explicit ShopProgressForm(QWidget *parent = 0);
	~ShopProgressForm();

	bool IsActiveShpooing();

	void Start(const int &delay, class CPacket *packet, const bool &showWindow);
};
//----------------------------------------------------------------------------------
extern ShopProgressForm *g_ShopProgressForm;
//----------------------------------------------------------------------------------
#endif // SHOPPROGRESSFORM_H
//----------------------------------------------------------------------------------
