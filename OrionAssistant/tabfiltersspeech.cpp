// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabFiltersSpeech.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabfiltersspeech.h"
#include "ui_tabfiltersspeech.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include "tabmain.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
#include "../GUI/filterspeechlistitem.h"
//----------------------------------------------------------------------------------
TabFiltersSpeech *g_TabFiltersSpeech = nullptr;
//----------------------------------------------------------------------------------
TabFiltersSpeech::TabFiltersSpeech(QWidget *parent)
: QWidget(parent), ui(new Ui::TabFiltersSpeech)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabFiltersSpeech = this;

	connect(ui->lw_FiltersSpeechList, SIGNAL(itemDropped()), this, SLOT(SaveSpeechFilter()));
}
//----------------------------------------------------------------------------------
TabFiltersSpeech::~TabFiltersSpeech()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabFiltersSpeech = nullptr;
}
//----------------------------------------------------------------------------------
void TabFiltersSpeech::on_lw_FiltersSpeechList_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f245");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	CFilterSpeechListItem *item = (CFilterSpeechListItem*)ui->lw_FiltersSpeechList->item(index.row());

	if (item != nullptr)
	{
		ui->le_FilterSpeechText->setText(item->GetText());
		ui->le_FilterSpeechColor->setText(item->GetColor());
		ui->cb_FilterSpeechFullMatch->setChecked(item->GetFullMatch());
	}
}
//----------------------------------------------------------------------------------
void TabFiltersSpeech::on_pb_FilterSpeechAdd_clicked()
{
	OAFUN_DEBUG("c28_f246");
	QString text = ui->le_FilterSpeechText->text().toLower();

	if (!text.length())
	{
		QMessageBox::critical(this, "Text is empty", "Enter the filter text!");
		return;
	}

	IFOR(i, 0, ui->lw_FiltersSpeechList->count())
	{
		CFilterSpeechListItem *item = (CFilterSpeechListItem*)ui->lw_FiltersSpeechList->item(i);

		if (item != nullptr && item->GetText().toLower() == text)
		{
			QMessageBox::critical(this, "Text is already exists", "Filter text is already exists!");
			return;
		}
	}

	new CFilterSpeechListItem(ui->le_FilterSpeechText->text(), ui->le_FilterSpeechColor->text(), ui->cb_FilterSpeechFullMatch->isChecked(), ui->lw_FiltersSpeechList);

	ui->lw_FiltersSpeechList->setCurrentRow(ui->lw_FiltersSpeechList->count() - 1);
	SaveSpeechFilter();
}
//----------------------------------------------------------------------------------
void TabFiltersSpeech::on_pb_FilterSpeechSave_clicked()
{
	OAFUN_DEBUG("c28_f247");
	QString text = ui->le_FilterSpeechText->text().toLower();

	if (!text.length())
	{
		QMessageBox::critical(this, "Text is empty", "Enter the filter text!");
		return;
	}

	CFilterSpeechListItem *selected = (CFilterSpeechListItem*)ui->lw_FiltersSpeechList->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Text is not selected", "Text is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_FiltersSpeechList->count())
	{
		CFilterSpeechListItem *item = (CFilterSpeechListItem*)ui->lw_FiltersSpeechList->item(i);

		if (item != nullptr && item->GetText().toLower() == text)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Text is already exists", "Filter text is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->SetText(ui->le_FilterSpeechText->text());
	selected->SetColor(ui->le_FilterSpeechColor->text());
	selected->SetFullMatch(ui->cb_FilterSpeechFullMatch->isChecked());
	selected->UpdateText();
	SaveSpeechFilter();
}
//----------------------------------------------------------------------------------
void TabFiltersSpeech::on_pb_FilterSpeechRemove_clicked()
{
	OAFUN_DEBUG("c28_f248");
	QListWidgetItem *item = ui->lw_FiltersSpeechList->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_FiltersSpeechList->takeItem(ui->lw_FiltersSpeechList->row(item));

		if (item != nullptr)
		{
			delete item;
			SaveSpeechFilter();
		}
	}
}
//----------------------------------------------------------------------------------
void TabFiltersSpeech::LoadSpeechFilter()
{
	OAFUN_DEBUG("c28_f262");
	ui->lw_FiltersSpeechList->clear();

	QFile file(g_DllPath + "/GlobalConfig/SpeechFilter.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;

		Q_UNUSED(version);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();
				}
				else if (reader.name() == "speech")
				{
					if (attributes.hasAttribute("text") && attributes.hasAttribute("color") && attributes.hasAttribute("fullmatch"))
					{
						new CFilterSpeechListItem(attributes.value("text").toString(), attributes.value("color").toString(), COrionAssistant::RawStringToBool(attributes.value("fullmatch").toString()), ui->lw_FiltersSpeechList);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabFiltersSpeech::SaveSpeechFilter()
{
	OAFUN_DEBUG("c28_f261");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/SpeechFilter.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_FiltersSpeechList->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");

		IFOR(i, 0, count)
		{
			CFilterSpeechListItem *item = (CFilterSpeechListItem*)ui->lw_FiltersSpeechList->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("speech");

				writter.writeAttribute("text", item->GetText());
				writter.writeAttribute("color", item->GetColor());
				writter.writeAttribute("fullmatch", COrionAssistant::BoolToText(item->GetFullMatch()));

				writter.writeEndElement(); //speech
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabFiltersSpeech::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_FiltersSpeechList)
		on_pb_FilterSpeechRemove_clicked();
}
//----------------------------------------------------------------------------------
bool TabFiltersSpeech::CheckSpeechInFilter(const QString &text, const ushort &color)
{
	OAFUN_DEBUG("c28_f257");
	if (g_TabMain->SpeechFilter())
	{
		IFOR(i, 0, ui->lw_FiltersSpeechList->count())
		{
			CFilterSpeechListItem *item = (CFilterSpeechListItem*)ui->lw_FiltersSpeechList->item(i);

			if (item != nullptr)
			{
				ushort itemColor = COrionAssistant::TextToGraphic(item->GetColor());

				if (itemColor == 0xFFFF || itemColor == color)
				{
					bool found = false;

					if (item->GetFullMatch())
						found = (item->GetText() == text);
					else
						found = text.contains(item->GetText());

					if (found)
						return true;
				}
			}
		}
	}

	return false;
}
//----------------------------------------------------------------------------------
