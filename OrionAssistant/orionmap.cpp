// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OrionMap.cpp
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "orionmap.h"
#include "ui_orionmap.h"
#include "../Managers/filemanager.h"
#include "../OrionAssistant/orionassistant.h"
#include "../GUI/mapaction.h"
#include "../GameObjects/GamePlayer.h"
#include "../GameObjects/GameWorld.h"
#include "../Managers/partymanager.h"
#include "tablistsfriends.h"
#include "tablistsenemies.h"
#include "Packets.h"
#include <QFile>
#include <QDir>
#include <QPaintEvent>
#include <QMenu>
//----------------------------------------------------------------------------------
OrionMap *g_OrionMap = nullptr;
//----------------------------------------------------------------------------------
const QString g_FacetName[6] =
{
	"Felucca",
	"Trammel",
	"Ilshenar",
	"Malas",
	"Tokuno",
	"TerMur"
};
//----------------------------------------------------------------------------------
OrionMap::OrionMap(QWidget *parent)
: QMainWindow(parent), ui(new Ui::OrionMap)
{
	ui->setupUi(this);

	g_OrionMap = this;

	setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint);

	setWindowIcon(QPixmap(":/resource/images/map_icon.png"));

	m_ImageCorpse = QImage(":/resource/images/corpse.png").scaled(15, 15);
	QImage img = m_ImageCorpse.createAlphaMask();
	IFOR(i, 0, img.width())
	{
		IFOR(j, 0, img.height())
		{
			const QRgb &pixel = m_ImageCorpse.pixel(i, j);

			if (pixel != 0xFFFFFFFF)
				img.setPixel(i, j, 0);
		}
	}
	m_ImageCorpse.setAlphaChannel(img);

	connect(&m_Timer, SIGNAL(timeout()), this, SLOT(OnRedrawTimer()));
	m_Timer.start(100);

	connect(&m_RequestsTimer, SIGNAL(timeout()), this, SLOT(OnRequestsTimer()));
	m_RequestsTimer.start(300);

	UpdateScale();

	if (g_OrionMapSettings.GetWindowX() && g_OrionMapSettings.GetWindowY())
		move(g_OrionMapSettings.GetWindowX(), g_OrionMapSettings.GetWindowY());

	if (g_OrionMapSettings.GetWindowWidth() > 0 && g_OrionMapSettings.GetWindowHeight() > 0)
		resize(g_OrionMapSettings.GetWindowWidth(), g_OrionMapSettings.GetWindowHeight());

	setWindowOpacity((100 - (g_OrionMapSettings.GetOpacity() * 25)) / 100.0);
}
//----------------------------------------------------------------------------------
OrionMap::~OrionMap()
{
	g_OrionMapSettings.SetWindowX(x());
	g_OrionMapSettings.SetWindowY(y());
	g_OrionMapSettings.SetWindowWidth(width());
	g_OrionMapSettings.SetWindowHeight(height());

	delete ui;
	ui = nullptr;
	disconnect(&m_Timer, SIGNAL(timeout()), this, SLOT(OnRedrawTimer()));

	if (m_Pixels != nullptr)
	{
		IFOR(i, 0, m_MapWidth)
			delete []m_Pixels[i];

		delete []m_Pixels;
		m_Pixels = nullptr;
	}

	m_MapWidth = 0;
	m_MapHeight = 0;

	g_OrionMap = nullptr;

	RELEASE_POINTER(m_SettingsForm);
}
//----------------------------------------------------------------------------------
void OrionMap::OnRedrawTimer()
{
	DWORD proc = 0;
	HWND hwnd = GetForegroundWindow();
	GetWindowThreadProcessId(hwnd, &proc);

	if (GetCurrentProcessId() != proc || (m_SettingsForm != nullptr && (HWND)m_SettingsForm->winId() == hwnd))
	{
		if (isVisible())
			hide();
	}
	else
	{
		if (!isVisible())
		{
			show();
			SetForegroundWindow(hwnd);
		}

		if (m_WantUpdateImage)
		{
			try
			{
				UpdateImage();
			}
			catch (std::exception &e)
			{
				qDebug() << "e:" << e.what();
				m_MapHash = 0;
			}
		}

		if (m_WantRedraw)
		{
			m_WantRedraw = false;
			m_Painting = true;
			repaint();
			m_Painting = false;
		}
	}
}
//----------------------------------------------------------------------------------
void OrionMap::OnRequestsTimer()
{
	if (g_World == nullptr || !isVisible())
		return;

	if (g_OrionMapSettings.GetPartyEnabled() && g_PartyManager.GetLeader())
	{
		IFOR(i, 0, 10)
		{
			uint serial = g_PartyManager.Member[i];

			if (serial && !g_World->FindWorldCharacter(serial))
			{
				CPacketMapUOPartyInfo().SendServer();
				break;
			}
		}

	}

	//if (g_OrionMapSettings.GetGuildEnabled())
	//	CPacketMapUOGuildInfo().SendServer();
}
//----------------------------------------------------------------------------------
void OrionMap::closeEvent(QCloseEvent *event)
{
	event->accept();

	disconnect(&m_Timer, SIGNAL(timeout()), this, SLOT(OnRedrawTimer()));
	m_Timer.stop();

	RELEASE_POINTER(g_OrionMap);
}
//----------------------------------------------------------------------------------
ORION_MAP_CHARACTER_TYPE OrionMap::GetCharacterType(const uint &serial)
{
	if (g_World == nullptr)
		return OMCT_OTHER;
	else if (serial == g_PlayerSerial)
		return OMCT_PLAYER;

	if (g_PartyManager.Contains(serial))
		return OMCT_PARTY_MEMBER;
	else if (g_TabListsFriends->InFriendList(serial))
		return OMCT_FRIEND;
	else if (g_TabListsEnemies->InEnemyList(serial))
		return OMCT_ENEMY;

	return OMCT_OTHER;
}
//----------------------------------------------------------------------------------
void OrionMap::UpdateImage()
{
	uint map = g_CurrentMap;

	if (g_OrionMapSettings.GetFacet() != 0xFFFFFFFF)
		map = g_OrionMapSettings.GetFacet();

	if (map < 6)
	{
		uint hash = 0;

		if (g_FileManager.m_File[OFI_MAP_0_UOP + map].GetStart() == nullptr)
			hash = g_FileManager.m_File[OFI_MAP_0_MUL + map].GetHash();
		else
			hash = g_FileManager.m_File[OFI_MAP_0_UOP + map].GetHash();

		bool showStatics = g_OrionMapSettings.GetShowStatics();
		bool showAltitudeMap = g_OrionMapSettings.GetAltitudeOnly();

		if (showStatics)
		{
			hash ^= g_FileManager.m_File[OFI_STAIDX_0_MUL + map].GetHash();
			hash ^= g_FileManager.m_File[OFI_STATICS_0_MUL + map].GetHash();
		}

		if (g_FileManager.m_File[OFI_MAP_0_MUL + map].GetStart() != nullptr)
		{
			if (g_FileManager.m_PatchInfo[map].GetMapPatchesCount())
			{
				hash ^= g_FileManager.m_File[OFI_MAP_DIFL_0_MUL + map].GetHash();
				hash ^= g_FileManager.m_File[OFI_MAP_DIF_0_MUL + map].GetHash();
			}

			if (g_FileManager.m_PatchInfo[map].GetStaticPatchesCount())
			{
				hash ^= g_FileManager.m_File[OFI_STA_DIFL_0_MUL + map].GetHash();
				hash ^= g_FileManager.m_File[OFI_STA_DIFI_0_MUL + map].GetHash();
				hash ^= g_FileManager.m_File[OFI_STA_DIF_0_MUL + map].GetHash();
			}
		}

		bool applyAltitude = g_OrionMapSettings.GetApplyAltitude();

		if (showAltitudeMap)
			hash ^= 0x12345678;
		else if (applyAltitude)
			hash ^= 0x11112222;

		hash ^= ((((uchar)'V' << 24) & 0xFF) | (((uchar)'E' << 16) & 0xFF) | (((uchar)'R' << 8) & 0xFF) | (((uchar)'0') & 0xFF));

		if (m_MapHash == hash)
		{
			m_WantUpdateImage = false;
			m_WantRedraw = true;
			return;
		}

		m_MapHash = hash;
		m_CacheNewImage = true;

		QString path;
		path.sprintf("/GlobalConfig/Maps/WorldMap%08X.oam", m_MapHash);
		QFile file(g_DllPath + path);
		bool fromFile = false;

		QPoint &mapSize = g_FileManager.m_MapSize[map];
		QPoint &mapBlockSize = g_FileManager.m_MapBlockSize[map];

		if (m_Pixels != nullptr)
		{
			IFOR(i, 0, m_MapWidth)
				delete []m_Pixels[i];

			delete []m_Pixels;
			m_Pixels = nullptr;
		}

		m_MapWidth = mapSize.x();
		m_MapHeight = mapSize.y();

		m_Pixels = new ushort*[m_MapWidth];

		IFOR(i, 0, m_MapWidth)
			m_Pixels[i] = new ushort[m_MapHeight];

		if (file.exists())
		{
			if (file.open(QIODevice::ReadOnly))
			{
				fromFile = true;

				IFOR(i, 0, m_MapWidth)
					file.read((pchar)&m_Pixels[i][0], m_MapHeight * 2);

				file.close();
				LOG("World map readed from file!\n");
			}
			else
				LOG("Error open world map file: %s\n", path.toLocal8Bit().data());
		}

		if (!fromFile)
		{
			struct MAP_CELLS_32
			{
				unsigned int TileID;
				char Z;
			};

			struct MAP_BLOCK_32
			{
				MAP_CELLS_32 Cells[64];
			};

			IFOR(bx, 0, mapBlockSize.x())
			{
				int mapX = (int)bx * 8;

				IFOR(by, 0, mapBlockSize.y())
				{
					CIndexMap *indexMap = g_FileManager.GetIndex(map, (int)bx, (int)by);

					if (indexMap == nullptr || indexMap->GetMapAddress() == 0)
						continue;

					int mapY = (int)by * 8;

					MAP_BLOCK_32 info;

					PMAP_BLOCK mapBlock = (PMAP_BLOCK)indexMap->GetMapAddress();

					int pos = 0;

					IFOR(y, 0, 8)
					{
						IFOR(x, 0, 8)
						{
							MAP_CELLS &cell = mapBlock->Cells[pos];
							MAP_CELLS_32 &infoCell = info.Cells[pos];
							infoCell.TileID = cell.TileID;
							infoCell.Z = cell.Z;
							pos++;
						}
					}

					PSTATICS_BLOCK sb = (PSTATICS_BLOCK)indexMap->GetStaticAddress();

					if (sb != NULL)
					{
						int count = indexMap->GetStaticCount();

						IFOR(c, 0, count)
						{
							STATICS_BLOCK &staticBlock = sb[c];

							if (staticBlock.Color && staticBlock.Color != 0xFFFF && !COrionAssistant::IsNoDrawTile(staticBlock.Color))
							{
								pos = (staticBlock.Y * 8) + staticBlock.X;
								//if (pos > 64) continue;

								if (!showStatics && !::IsWet(g_GetStaticFlags(staticBlock.Color)))
									continue;

								MAP_CELLS_32 &infoCell = info.Cells[pos];

								if (infoCell.Z <= staticBlock.Z)
								{
									infoCell.TileID = staticBlock.Color + 0x4000;
									infoCell.Z = staticBlock.Z;
								}
							}
						}
					}

					pos = 0;

					IFOR(y, 0, 8)
					{
						IFOR(x, 0, 8)
						{
							MAP_CELLS_32 &infoCell = info.Cells[pos];

							ushort color = g_FileManager.GetRadarcolData(infoCell.TileID);

							if (showAltitudeMap)
							{
								uchar z = infoCell.Z + 127;

								color = ((((z * 32) / 256) & 0x1F) << 10) | (((z * 32) / 256) & 0x1F) << 5 | (((z * 32) / 256) & 0x1F);
							}
							else if (applyAltitude && infoCell.TileID < 0x4000)
							{
								uchar z = (infoCell.Z + 127) / 8;

								color += ((((z * 32) / 256) & 0x1F) << 10) | (((z * 32) / 256) & 0x1F) << 5 | (((z * 32) / 256) & 0x1F);
							}

							m_Pixels[mapX + (int)x][mapY + (int)y] = color;

							pos++;
						}
					}
				}
			}

			QDir(g_DllPath).mkdir("GlobalConfig");
			QDir(g_DllPath).mkdir("GlobalConfig/Maps");

			if (file.open(QIODevice::WriteOnly))
			{
				IFOR(i, 0, m_MapWidth)
					file.write((pchar)&m_Pixels[i][0], m_MapHeight * 2);

				file.close();
			}
		}

		//m_MapImage = QImage((puchar)&m_Pixels[0], mapSize.x(), mapSize.y(), QImage::Format_RGB555);
		m_WantUpdateImage = false;
		m_WantRedraw = true;
	}
}
//----------------------------------------------------------------------------------
void OrionMap::CheckForUpdate(const uint &serial)
{
	ORION_MAP_CHARACTER_TYPE type = GetCharacterType(serial);

	bool wantUpdate = false;

	switch (type)
	{
		case OMCT_PLAYER:
			wantUpdate = true;
			break;
		case OMCT_PARTY_MEMBER:
			wantUpdate = g_OrionMapSettings.GetPartyEnabled();
			break;
		case OMCT_GUILD_MEMBER:
			wantUpdate = g_OrionMapSettings.GetGuildEnabled();
			break;
		case OMCT_FRIEND:
			wantUpdate = g_OrionMapSettings.GetFriendsEnabled();
			break;
		case OMCT_ENEMY:
			wantUpdate = g_OrionMapSettings.GetEnemiesEnabled();
			break;
		case OMCT_OTHER:
			wantUpdate = g_OrionMapSettings.GetOtherEnabled();
			break;
		default:
			break;
	}

	if (wantUpdate)
		m_WantRedraw = true;
}
//----------------------------------------------------------------------------------
void OrionMap::DrawLine(const int &x, const int &y, QPainter &painter, const QPen &pen, int value, const int &maxValue)
{
	if (maxValue)
		value = (int)((value * 100) / (float)maxValue);
	else
		value = 0;

	if (value > 100)
		value = 100;

	painter.setBrush(QColor(0, 0, 0, 127));
	painter.setPen(QColor(0, 0, 0, 127));
	painter.drawRect(x - 10, y, 20, 2);

	value = (value * 18) / 100 - 9;

	if (value > -9)
	{
		painter.setPen(pen);
		painter.drawLine(x - 9, y + 1, x + value, y + 1);
	}
}
//----------------------------------------------------------------------------------
bool OrionMap::BuildImage(QImage &image, QRect &rect, const int &cx, const int &cy, int width, int height)
{
	if (width < 1 || height < 1)
		return false;

	if (width % 2)
		width++;

	if (height % 2)
		height++;

	uint map = g_CurrentMap;

	if (g_OrionMapSettings.GetFacet() != 0xFFFFFFFF)
		map = g_OrionMapSettings.GetFacet();

	bool rotated = g_OrionMapSettings.GetRotateMap();

	int drawWidth = width * m_Scale;
	int drawHeight = height * m_Scale;

	int drawX = cx - (drawWidth / 2);
	int drawY = cy - (drawHeight / 2);

	rect = QRect(drawX, drawY, drawWidth, drawHeight);

	if (m_CacheScale == m_Scale && m_CacheX == cx && m_CacheY == cy && m_CacheWidth == width && m_CacheHeight == height && m_CacheMap == map && m_CacheRotate == rotated && !m_CacheNewImage)
		return true;

	m_CachePixels.clear();
	m_CachePixels.resize(width * height, 0);

	m_CacheScale = m_Scale;
	m_CacheX = cx;
	m_CacheY = cy;
	m_CacheWidth = width;
	m_CacheHeight = height;
	m_CacheMap = map;
	m_CacheRotate = rotated;
	m_CacheNewImage = false;

	int mapWidth = g_FileManager.m_MapSize[map].x();
	int mapHeight = g_FileManager.m_MapSize[map].y();

	if (rotated)
	{
		int mulWidth = width / 2;
		int mulHeight = height / 2;

		int offsetX = mulWidth * m_Scale;
		int offsetY = mulHeight * m_Scale;

		int endX = offsetX;
		int endY = offsetY;
		int startX = -endX;
		int startY = -endY;

		int startBlockX = (cx + (startY + startX)) / 8 - 1;
		int startBlockY = (cy + (startY - endX)) / 8 - 1;
		int endBlockX = (cx + (endY + endX)) / 8 + 1;
		int endBlockY = (cy + (endY - startX)) / 8 + 1;

		if (startBlockX < 0)
			startBlockX = 0;

		if (startBlockY < 0)
			startBlockY = 0;

		if (endBlockX >= g_FileManager.m_MapBlockSize[map].x())
			endBlockX = g_FileManager.m_MapBlockSize[map].x() - 1;

		if (endBlockY >= g_FileManager.m_MapBlockSize[map].y())
			endBlockY = g_FileManager.m_MapBlockSize[map].y() - 1;

		startX = startBlockX * 8;
		startY = startBlockY * 8;
		endX = endBlockX * 8;
		endY = endBlockY * 8;

		int maxPos = width * height - 1;

		int count = (int)ceil(1.0 / m_Scale);

		if (m_Scale < 1.0)
		{
			count++;

			if (m_Scale < 0.75)
			{
				count++;

				if (m_Scale < 0.5)
					count++;
			}
		}

		IFOR(y, startY, endY)
		{
			int posImageY = (int)((y - cy) / m_Scale);

			IFOR(x, startX, endX)
			{
				int posImageX = (int)((x - cx + offsetX) / m_Scale);

				int gx = posImageX - posImageY;

				if (gx < 0 || gx >= width)
					continue;

				int gy = posImageX + posImageY;

				if (gy < 0 || gy >= height)
					continue;

				ushort &color = m_Pixels[x][y];

				IFOR(i, 0, count)
				{
					int pos = ((gy + i) * width) + gx;

					IFOR(j, 0, count + 1)
					{
						if (pos < maxPos)
							m_CachePixels[pos++] = color;
					}
				}
			}
		}
	}
	else
	{
		IFOR(y, 0, height)
		{
			int posY = drawY + (int)(y * m_Scale);

			if (posY < 0 || posY >= mapHeight)
				continue;

			int posImage = y * width;

			IFOR(x, 0, width)
			{
				int posX = drawX + (int)(x * m_Scale);

				if (posX < 0 || posX >= mapWidth)
					continue;

				m_CachePixels[posImage + x] = m_Pixels[posX][posY];
			}
		}
	}

	image = QImage((puchar)&m_CachePixels[0], width, height, QImage::Format_RGB555);

	return true;
}
//----------------------------------------------------------------------------------
void OrionMap::mapPaintEvent(QPainter &painter)
{
	if (!m_Painting)
	{
		m_WantRedraw = true;
		return;
	}

	/*if (g_OrionMapSettings.GetDrawMode())
		DrawMode1(painter);
	else*/
		DrawMode0(painter);
}
//----------------------------------------------------------------------------------
void OrionMap::DrawMode0(QPainter &painter)
{
	int currentX = m_CurrentWorldCoordinates.x();
	int currentY = m_CurrentWorldCoordinates.y();

	if (g_World != nullptr && g_OrionMapSettings.GetFollowPlayer() && g_OrionMapSettings.GetFacet() == 0xFFFFFFFF)
	{
		CGameCharacter *obj = g_Player;

		if (obj != nullptr)
		{
			currentX = obj->GetX();
			currentY = obj->GetY();

			m_CurrentWorldCoordinates.setX(currentX);
			m_CurrentWorldCoordinates.setY(currentY);
		}
	}

	int width = ui->miw_Image->width();
	int height = ui->miw_Image->height();

	QRect worldBounds;

	if (!BuildImage(m_CacheImage, worldBounds, currentX, currentY, width, height) || m_CacheImage.isNull())
		return;

	painter.drawImage(0, 0, m_CacheImage);

	uint map = g_CurrentMap;

	if (g_OrionMapSettings.GetFacet() != 0xFFFFFFFF)
		map = g_OrionMapSettings.GetFacet();

	if (g_OrionMapSettings.GetDisplayCoordinates())
	{
		painter.setPen(QPen(QColor(255, 255, 255)));
		painter.drawText(2, 14, g_FacetName[map] + QString().sprintf(", x=%i y=%i", m_CurrentWorldCoordinates.x(), m_CurrentWorldCoordinates.y()));
	}

	int screenCenterX = width / 2;
	int screenCenterY = height / 2;

	if (g_World == nullptr)
	{
		painter.translate(screenCenterX, screenCenterY);

		QPen pen(Qt::red);
		pen.setWidth(2);
		painter.setPen(pen);

		painter.drawEllipse(-3, -3, 5, 5);

		return;
	}

	struct MAP_DRAW_OBJECT
	{
		int Width;
		int Height;
		int DeadWidth;
		int DeadHeight;
		QColor Color;
		bool Enabled;
		int FontStyle;
		bool DrawName;
		bool DrawHits;
		bool DrawMana;
		bool DrawStam;
	};

	MAP_DRAW_OBJECT table[6] =
	{
		{ 6, 6,	6, 6,	g_OrionMapSettings.GetPlayerColor(),	true,									g_OrionMapSettings.GetPlayerFontStyle(),	g_OrionMapSettings.GetPlayerShowName(),		g_OrionMapSettings.GetPlayerShowHits(),		g_OrionMapSettings.GetPlayerShowMana(),	g_OrionMapSettings.GetPlayerShowStam() },
		{ 4, 4,	6, 6,	g_OrionMapSettings.GetPartyColor(),		g_OrionMapSettings.GetPartyEnabled(),	g_OrionMapSettings.GetPartyFontStyle(),		g_OrionMapSettings.GetPartyShowName(),		g_OrionMapSettings.GetPartyShowHits(),		false,	false },
		{ 4, 4,	6, 6,	g_OrionMapSettings.GetGuildColor(),		g_OrionMapSettings.GetGuildEnabled(),	g_OrionMapSettings.GetGuildFontStyle(),		g_OrionMapSettings.GetGuildShowName(),		g_OrionMapSettings.GetGuildShowHits(),		false,	false },
		{ 4, 4,	6, 6,	g_OrionMapSettings.GetFriendsColor(),	g_OrionMapSettings.GetFriendsEnabled(),	g_OrionMapSettings.GetFriendsFontStyle(),	g_OrionMapSettings.GetFriendsShowName(),	g_OrionMapSettings.GetFriendsShowHits(),	false, false },
		{ 4, 4,	6, 6,	g_OrionMapSettings.GetEnemiesColor(),	g_OrionMapSettings.GetEnemiesEnabled(),	g_OrionMapSettings.GetEnemiesFontStyle(),	g_OrionMapSettings.GetEnemiesShowName(),	g_OrionMapSettings.GetEnemiesShowHits(),	false, false },
		{ 4, 4,	6, 6,	g_OrionMapSettings.GetOtherColor(),		g_OrionMapSettings.GetOtherEnabled(),	g_OrionMapSettings.GetOtherFontStyle(),		g_OrionMapSettings.GetOtherShowName(),		g_OrionMapSettings.GetOtherShowHits(),		false, false },
	};

	QList<COrionMapDrawObject> items;

	if (g_OrionMapSettings.GetPartyEnabled() && g_PartyManager.GetLeader())
	{
		IFOR(i, 0, 10)
		{
			uint serial = g_PartyManager.Member[i];

			if (!serial)
				continue;

			CGameCharacter *obj = g_World->FindWorldCharacter(serial);

			if (obj == nullptr)
			{
				PARTY_MEMBER_INFO &info = g_PartyManager.m_MemberInfo[i];

				if (info.Map == map && info.X && info.Y && worldBounds.contains(QPoint(info.X, info.Y)))
					items.push_back(COrionMapDrawObject(OMCT_PARTY_MEMBER, info.X, info.Y, info.Name, nullptr));
			}
		}
	}

	QFOR(obj, g_World->m_Items, CGameObject*)
	{
		if (!obj->GetNPC() || !worldBounds.contains(QPoint(obj->GetX(), obj->GetY())))
			continue;

		uint serial = obj->GetSerial();

		ORION_MAP_CHARACTER_TYPE type = GetCharacterType(serial);

		if (!table[type].Enabled)
			continue;

		QString name = obj->GetName();

		if (!name.length() && g_PartyManager.GetLeader())
		{
			IFOR(i, 0, 10)
			{
				if (g_PartyManager.Member[i] == serial)
				{
					name = g_PartyManager.m_MemberInfo[i].Name;

					break;
				}
			}
		}

		items.push_back(COrionMapDrawObject(type, obj->GetX(), obj->GetY(), name, (CGameCharacter*)obj));
	}

	if (g_OrionMapSettings.GetShowLastCorpse() && g_LastPlayerDeadTimer > GetTickCount() && worldBounds.contains(g_LastPlayerCorpsePosition))
		items.push_back(COrionMapDrawObject(OMCT_CORPSE, g_LastPlayerCorpsePosition.x(), g_LastPlayerCorpsePosition.y(), "Corpse", nullptr));

	int fontStyle = -1;

	bool rotated = g_OrionMapSettings.GetRotateMap();

	for (const COrionMapDrawObject &item : items)
	{
		int x;
		int y;

		if (rotated)
		{
			x = item.GetX() - currentX;
			y = item.GetY() - currentY;

			int tempX = x - y;
			int tempY = x + y;

			x = screenCenterX + (tempX / m_Scale);
			y = screenCenterY + (tempY / m_Scale);
		}
		else
		{
			x = (item.GetX() - worldBounds.x()) / m_Scale;
			y = (item.GetY() - worldBounds.y()) / m_Scale;
		}

		painter.translate(x, y);

		if (item.GetType() == OMCT_CORPSE)
		{
			Qt::BGMode bgMode = painter.backgroundMode();
			painter.setBackgroundMode(Qt::TransparentMode);

			int offsetX = m_ImageCorpse.width() / 2;
			int offsetY = m_ImageCorpse.height() / 2;

			painter.setPen(QPen(QColor(Qt::white)));
			painter.setBrush(QBrush(QColor(Qt::white)));

			painter.drawText(-(QFontMetrics(painter.font()).width(item.GetName()) / 2), -offsetY - 3, item.GetName());

			painter.drawImage(-offsetX, -offsetY, m_ImageCorpse);

			painter.setBackgroundMode(bgMode);

			painter.translate(-x, -y);

			continue;
		}

		MAP_DRAW_OBJECT &setting = table[item.GetType()];

		int width = setting.Width;
		int height = setting.Height;

		CGameCharacter * obj = item.GetPtr();
		bool dead = (obj == nullptr ? false : obj->Dead());

		if (dead && g_OrionMapSettings.GetMarkDeadMobiles())
		{
			width = setting.DeadWidth;
			height = setting.DeadHeight;
		}

		int offsetX = width / 2;
		int offsetY = height / 2;

		QPen pen(setting.Color);
		painter.setPen(pen);
		painter.setBrush(QBrush(setting.Color));

		if (setting.DrawName)
		{
			if (setting.FontStyle != fontStyle)
			{
				fontStyle = setting.FontStyle;
				painter.setFont(g_MapFontStyle[fontStyle].GetFont());
			}

			painter.drawText(-(QFontMetrics(painter.font()).width(item.GetName()) / 2), -offsetY - 3, item.GetName());
		}

		if (dead && g_OrionMapSettings.GetMarkDeadMobiles())
		{
			painter.drawLine(-offsetX, -offsetY, offsetX, offsetY);
			painter.drawLine(-offsetX, offsetY, offsetX, -offsetY);

			painter.translate(-x, -y);

			continue;
		}

		painter.drawEllipse(-offsetX, -offsetY, width, height);

		if (!dead && setting.DrawHits && obj != nullptr)
		{
			DrawLine(0, 1 + offsetY, painter, pen, ((CGameCharacter*)obj)->GetHits(), ((CGameCharacter*)obj)->GetMaxHits());

			if (setting.DrawMana)
				DrawLine(0, offsetY + 3, painter, pen, ((CGameCharacter*)obj)->GetMana(), ((CGameCharacter*)obj)->GetMaxMana());

			if (setting.DrawStam)
				DrawLine(0, offsetY + 5, painter, pen, ((CGameCharacter*)obj)->GetStam(), ((CGameCharacter*)obj)->GetMaxStam());
		}

		painter.translate(-x, -y);
	}
}
//----------------------------------------------------------------------------------
void OrionMap::DrawMode1(QPainter &painter)
{
	if (m_MapImage.isNull())
		return;

	int x = m_CurrentWorldCoordinates.x();
	int y = m_CurrentWorldCoordinates.y();

	if (g_World != nullptr && g_OrionMapSettings.GetFollowPlayer() && g_OrionMapSettings.GetFacet() == 0xFFFFFFFF)
	{
		CGameCharacter *obj = g_Player;

		if (obj != nullptr)
		{
			x = obj->GetX();
			y = obj->GetY();

			m_CurrentWorldCoordinates.setX(x);
			m_CurrentWorldCoordinates.setY(y);
		}
	}

	Qt::TransformationMode transformationMode = (g_OrionMapSettings.GetSmoothImage() ? Qt::SmoothTransformation : Qt::FastTransformation);

	int width = ui->miw_Image->width();
	int height = ui->miw_Image->height();

	int drawWidth = width * m_Scale;
	int drawHeight = height * m_Scale;

	int drawX = x - (drawWidth / 2);
	int drawY = y - (drawHeight / 2);

	bool rotated = g_OrionMapSettings.GetRotateMap();
	int offsetX = 0;
	int offsetY = 0;

	float scaleX = 1.0f;
	float scaleY = 1.0f;

	if (rotated)
	{
		/*int mulWidth = width / 2;
		int mulHeight = height / 2;

		int endX = mulWidth * m_Scale;
		int endY = mulHeight * m_Scale;
		int startX = -endX;
		int startY = -endY;

		int startBlockX = (x + (startY + startX)) / 8 - 1;
		int startBlockY = (y + (startY - endX)) / 8 - 1;
		int endBlockX = (x + (endY + endX)) / 8 + 1;
		int endBlockY = (y + (endY - startX)) / 8 + 1;

		drawX = startBlockX * 8;
		drawY = startBlockY * 8;
		int newDrawWidth = (endBlockX * 8) - drawX;
		int newDrawHeight = (endBlockY * 8) - drawY;

		scaleX = newDrawWidth / (float)drawWidth;
		scaleY = newDrawHeight / (float)drawHeight;

		drawWidth = newDrawWidth;
		drawHeight = newDrawHeight;*/
	/*}

	if (rotated)
	{*/
		offsetX = width / 2;
		offsetY = height / 2;

		painter.translate(offsetX, offsetY);

		painter.rotate(45.0);
	}

	QImage img = m_MapImage.copy(drawX, drawY, drawWidth, drawHeight);

	painter.drawImage(-offsetX, -offsetY, img.scaled(width * scaleX, height * scaleY, Qt::IgnoreAspectRatio, transformationMode));

	uint map = g_CurrentMap;

	if (g_OrionMapSettings.GetFacet() != 0xFFFFFFFF)
		map = g_OrionMapSettings.GetFacet();

	if (g_OrionMapSettings.GetDisplayCoordinates())
	{
		if (rotated)
			painter.rotate(-45.0);

		painter.setPen(QPen(QColor(255, 255, 255)));
		painter.drawText(2 - offsetX, 14 - offsetY, g_FacetName[map] + QString().sprintf(", x=%i y=%i", m_CurrentWorldCoordinates.x(), m_CurrentWorldCoordinates.y()));

		if (rotated)
			painter.rotate(45.0);
	}

	if (g_World == nullptr)
	{
		painter.translate(width / 2, height / 2);

		QPen pen(Qt::red);
		pen.setWidth(2);
		painter.setPen(pen);

		painter.drawEllipse(-5 - offsetX, -5 - offsetY, 5, 5);

		return;
	}

	struct MAP_DRAW_OBJECT
	{
		int Width;
		int Height;
		int DeadWidth;
		int DeadHeight;
		QColor Color;
		bool Enabled;
		int FontStyle;
		bool DrawName;
		bool DrawHits;
		bool DrawMana;
		bool DrawStam;
	};

	MAP_DRAW_OBJECT table[6] =
	{
		{ 6, 6,	6, 6,	g_OrionMapSettings.GetPlayerColor(),	true,									g_OrionMapSettings.GetPlayerFontStyle(),	g_OrionMapSettings.GetPlayerShowName(),		g_OrionMapSettings.GetPlayerShowHits(),		g_OrionMapSettings.GetPlayerShowMana(),	g_OrionMapSettings.GetPlayerShowStam() },
		{ 4, 4,	6, 6,	g_OrionMapSettings.GetPartyColor(),		g_OrionMapSettings.GetPartyEnabled(),	g_OrionMapSettings.GetPartyFontStyle(),		g_OrionMapSettings.GetPartyShowName(),		g_OrionMapSettings.GetPartyShowHits(),		false,	false },
		{ 4, 4,	6, 6,	g_OrionMapSettings.GetGuildColor(),		g_OrionMapSettings.GetGuildEnabled(),	g_OrionMapSettings.GetGuildFontStyle(),		g_OrionMapSettings.GetGuildShowName(),		g_OrionMapSettings.GetGuildShowHits(),		false,	false },
		{ 4, 4,	6, 6,	g_OrionMapSettings.GetFriendsColor(),	g_OrionMapSettings.GetFriendsEnabled(),	g_OrionMapSettings.GetFriendsFontStyle(),	g_OrionMapSettings.GetFriendsShowName(),	g_OrionMapSettings.GetFriendsShowHits(),	false, false },
		{ 4, 4,	6, 6,	g_OrionMapSettings.GetEnemiesColor(),	g_OrionMapSettings.GetEnemiesEnabled(),	g_OrionMapSettings.GetEnemiesFontStyle(),	g_OrionMapSettings.GetEnemiesShowName(),	g_OrionMapSettings.GetEnemiesShowHits(),	false, false },
		{ 4, 4,	6, 6,	g_OrionMapSettings.GetOtherColor(),		g_OrionMapSettings.GetOtherEnabled(),	g_OrionMapSettings.GetOtherFontStyle(),		g_OrionMapSettings.GetOtherShowName(),		g_OrionMapSettings.GetOtherShowHits(),		false, false },
	};

	QRect drawWorldRect(drawX, drawY, drawWidth, drawHeight);

	QList<COrionMapDrawObject> items;

	if (g_OrionMapSettings.GetPartyEnabled() && g_PartyManager.GetLeader())
	{
		IFOR(i, 0, 10)
		{
			uint serial = g_PartyManager.Member[i];

			if (!serial)
				continue;

			CGameCharacter *obj = g_World->FindWorldCharacter(serial);

			if (obj == nullptr)
			{
				PARTY_MEMBER_INFO &info = g_PartyManager.m_MemberInfo[i];

				if (info.Map == map && info.X && info.Y && drawWorldRect.contains(QPoint(info.X, info.Y)))
				{
					int x = (info.X - drawX) / m_Scale;
					int y = (info.Y - drawY) / m_Scale;

					items.push_back(COrionMapDrawObject(OMCT_PARTY_MEMBER, x, y, info.Name, nullptr));
				}
			}
		}
	}

	QFOR(obj, g_World->m_Items, CGameObject*)
	{
		if (!obj->GetNPC() || !drawWorldRect.contains(QPoint(obj->GetX(), obj->GetY())))
			continue;

		uint serial = obj->GetSerial();

		ORION_MAP_CHARACTER_TYPE type = GetCharacterType(serial);

		if (!table[type].Enabled)
			continue;

		int x = (obj->GetX() - drawX) / m_Scale;
		int y = (obj->GetY() - drawY) / m_Scale;

		QString name = obj->GetName();

		if (!name.length() && g_PartyManager.GetLeader())
		{
			IFOR(i, 0, 10)
			{
				if (g_PartyManager.Member[i] == serial)
				{
					name = g_PartyManager.m_MemberInfo[i].Name;

					break;
				}
			}
		}

		items.push_back(COrionMapDrawObject(type, x, y, name, (CGameCharacter*)obj));
	}

	if (g_OrionMapSettings.GetShowLastCorpse() && g_LastPlayerDeadTimer + 300000 > GetTickCount() && drawWorldRect.contains(g_LastPlayerCorpsePosition))
	{
		int x = (g_LastPlayerCorpsePosition.x() - drawX) / m_Scale;
		int y = (g_LastPlayerCorpsePosition.y() - drawY) / m_Scale;

		items.push_back(COrionMapDrawObject(OMCT_CORPSE, x, y, "Corpse", nullptr));
	}

	int fontStyle = -1;

	for (const COrionMapDrawObject &item : items)
	{
		int x = item.GetX() - offsetX;
		int y = item.GetY() - offsetY;

		painter.translate(x, y);

		if (rotated)
			painter.rotate(-45.0);

		if (item.GetType() == OMCT_CORPSE)
		{
			Qt::BGMode bgMode = painter.backgroundMode();
			painter.setBackgroundMode(Qt::TransparentMode);

			int offsetX = m_ImageCorpse.width() / 2;
			int offsetY = m_ImageCorpse.height() / 2;

			painter.setPen(QPen(QColor(Qt::white)));
			painter.setBrush(QBrush(QColor(Qt::white)));

			painter.drawText(-(QFontMetrics(painter.font()).width(item.GetName()) / 2), -offsetY - 3, item.GetName());

			painter.drawImage(-offsetX, -offsetY, m_ImageCorpse);

			painter.setBackgroundMode(bgMode);

			if (rotated)
				painter.rotate(45.0);

			painter.translate(-x, -y);

			continue;
		}

		MAP_DRAW_OBJECT &setting = table[item.GetType()];

		int width = setting.Width;
		int height = setting.Height;

		CGameCharacter * obj = item.GetPtr();
		bool dead = (obj == nullptr ? false : obj->Dead());

		if (dead && g_OrionMapSettings.GetMarkDeadMobiles())
		{
			width = setting.DeadWidth;
			height = setting.DeadHeight;
		}

		int offsetX = width / 2;
		int offsetY = height / 2;

		QPen pen(setting.Color);
		painter.setPen(pen);
		painter.setBrush(QBrush(setting.Color));

		if (setting.DrawName)
		{
			if (setting.FontStyle != fontStyle)
			{
				fontStyle = setting.FontStyle;
				painter.setFont(g_MapFontStyle[fontStyle].GetFont());
			}

			painter.drawText(-(QFontMetrics(painter.font()).width(item.GetName()) / 2), -offsetY - 3, item.GetName());
		}

		if (dead && g_OrionMapSettings.GetMarkDeadMobiles())
		{
			painter.drawLine(-offsetX, -offsetY, offsetX, offsetY);
			painter.drawLine(-offsetX, offsetY, offsetX, -offsetY);

			if (rotated)
				painter.rotate(45.0);

			painter.translate(-x, -y);

			continue;
		}

		painter.drawEllipse(-offsetX, -offsetY, width, height);

		if (!dead && setting.DrawHits && obj != nullptr)
		{
			DrawLine(0, 1 + offsetY, painter, pen, ((CGameCharacter*)obj)->GetHits(), ((CGameCharacter*)obj)->GetMaxHits());

			if (setting.DrawMana)
				DrawLine(0, offsetY + 3, painter, pen, ((CGameCharacter*)obj)->GetMana(), ((CGameCharacter*)obj)->GetMaxMana());

			if (setting.DrawStam)
				DrawLine(0, offsetY + 5, painter, pen, ((CGameCharacter*)obj)->GetStam(), ((CGameCharacter*)obj)->GetMaxStam());
		}

		if (rotated)
			painter.rotate(45.0);

		painter.translate(-x, -y);
	}
}
//----------------------------------------------------------------------------------
void OrionMap::mapMousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && !(g_World != nullptr && g_OrionMapSettings.GetFollowPlayer() && g_OrionMapSettings.GetFacet() == 0xFFFFFFFF))
	{
		m_MousePressed = true;
		m_PressedWorldCoordinatesOffset = m_CurrentWorldCoordinates;
		m_MousePressPoint = event->pos();
	}
}
//----------------------------------------------------------------------------------
void OrionMap::mapMouseReleaseEvent(QMouseEvent *event)
{
	if (!m_MousePressed || event->button() != Qt::LeftButton || (g_World != nullptr && g_OrionMapSettings.GetFollowPlayer() && g_OrionMapSettings.GetFacet() == 0xFFFFFFFF))
		return;

	mapMouseMoveEvent(event);

	m_MousePressed = false;
}
//----------------------------------------------------------------------------------
void OrionMap::mapMouseDoubleClickEvent(QMouseEvent *event)
{
	if (event->button() != Qt::LeftButton)
		return;

	if (windowFlags() & Qt::FramelessWindowHint)
		setWindowFlags(windowFlags() & ~Qt::FramelessWindowHint);
	else
		setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	show();
}
//----------------------------------------------------------------------------------
void OrionMap::mapMouseMoveEvent(QMouseEvent *event)
{
	if (!m_MousePressed)
		return;

	int deltaX = (m_MousePressPoint.x() - event->pos().x()) * m_Scale;
	int deltaY = (m_MousePressPoint.y() - event->pos().y()) * m_Scale;

	if (g_OrionMapSettings.GetRotateMap())
	{
		//deltaX = (int)round((deltaX * cos(-3.1415926535897931 / 4.0)) - (deltaY * sin(-3.1415926535897931 / 4.0)));
		//deltaY = (int)round((deltaX * sin(-3.1415926535897931 / 4.0)) + (deltaY * cos(-3.1415926535897931 / 4.0)));

		double tempX = deltaX * 0.707107;
		double tempY = deltaY * 0.707107;

		deltaX = (int)round(tempX + tempY);
		deltaY = (int)round(tempY - tempX);
	}

	int x = m_PressedWorldCoordinatesOffset.x() + deltaX;
	int y = m_PressedWorldCoordinatesOffset.y() + deltaY;

	uint map = g_CurrentMap;

	if (g_OrionMapSettings.GetFacet() != 0xFFFFFFFF)
		map = g_OrionMapSettings.GetFacet();

	QPoint &mapSize = g_FileManager.m_MapSize[map];

	if (x > mapSize.x())
		x = mapSize.x();

	if (x < 0)
		x = 0;

	if (y > mapSize.y())
		y = mapSize.y();

	if (y < 0)
		y = 0;

	m_CurrentWorldCoordinates.setX(x);
	m_CurrentWorldCoordinates.setY(y);

	m_WantRedraw = true;
}
//----------------------------------------------------------------------------------
void OrionMap::mapWheelEvent(QWheelEvent *event)
{
	if (event->delta() > 0 && g_OrionMapSettings.GetZoom() > 1) //up
	{
		if (g_OrionMapSettings.GetZoom() == 16)
			g_OrionMapSettings.SetZoom(8);
		else
			g_OrionMapSettings.SetZoom(g_OrionMapSettings.GetZoom() - 1);

		UpdateScale();
		m_WantRedraw = true;
	}
	else if (event->delta() < 0 && g_OrionMapSettings.GetZoom() <= 8) //down
	{
		if (g_OrionMapSettings.GetZoom() == 8)
			g_OrionMapSettings.SetZoom(16);
		else
			g_OrionMapSettings.SetZoom(g_OrionMapSettings.GetZoom() + 1);

		UpdateScale();
		m_WantRedraw = true;
	}
}
//----------------------------------------------------------------------------------
void OrionMap::UpdateScale()
{
	m_Scale = g_OrionMapSettings.GetZoom() * 0.25f;

	if (m_Scale < 0.25f)
		m_Scale = 0.25f;
	else if (m_Scale > 2.0f)
		m_Scale = 4.0f;
}
//----------------------------------------------------------------------------------
void OrionMap::OnActionSelect(const uint &type, const uint &extra)
{
	bool wantRedraw = true;
	bool wantUpdateImage = false;

	switch (type)
	{
		case OMA_FACET:
		{
			if (g_OrionMapSettings.GetFacet() != extra)
			{
				g_OrionMapSettings.SetFacet(extra);
				wantUpdateImage = true;
			}

			break;
		}
		case OMA_FOLLOW_CHARACTER:
		{
			g_OrionMapSettings.SetFollowPlayer(!g_OrionMapSettings.GetFollowPlayer());
			break;
		}
		case OMA_ZOOM:
		{
			g_OrionMapSettings.SetZoom(extra);
			UpdateScale();
			break;
		}
		case OMA_SHOW_STATICS:
		{
			g_OrionMapSettings.SetShowStatics(!g_OrionMapSettings.GetShowStatics());
			wantUpdateImage = true;
			break;
		}
		case OMA_APPLY_ALTITUDE:
		{
			g_OrionMapSettings.SetApplyAltitude(!g_OrionMapSettings.GetApplyAltitude());
			wantUpdateImage = true;
			break;
		}
		case OMA_ALTITUDE_ONLY:
		{
			g_OrionMapSettings.SetAltitudeOnly(!g_OrionMapSettings.GetAltitudeOnly());
			wantUpdateImage = true;
			break;
		}
		case OMA_SMOOTH_IMAGE:
		{
			g_OrionMapSettings.SetSmoothImage(!g_OrionMapSettings.GetSmoothImage());
			break;
		}
		case OMA_TRANSPARENT_WINDOW:
		{
			g_OrionMapSettings.SetOpacity(extra);
			setWindowOpacity((100 - (extra * 25)) / 100.0);
			wantRedraw = false;
			break;
		}
		case OMA_DISPLAY_COORDINATES:
		{
			g_OrionMapSettings.SetDisplayCoordinates(!g_OrionMapSettings.GetDisplayCoordinates());
			break;
		}
		case OMA_OPEN_SETTINGS:
		{
			if (m_SettingsForm == nullptr)
				m_SettingsForm = new MapSettingsForm();

			m_SettingsForm->Init(g_OrionMapSettings);
			m_SettingsForm->show();

			return;
		}
		case OMA_ROTATE_MAP:
		{
			g_OrionMapSettings.SetRotateMap(!g_OrionMapSettings.GetRotateMap());
			break;
		}
		default:
		{
			wantRedraw = false;
			break;
		}
	}

	if (wantRedraw)
	{
		if (wantUpdateImage)
			m_WantUpdateImage = true;

		m_WantRedraw = true;
	}
}
//----------------------------------------------------------------------------------
void OrionMap::on_miw_Image_customContextMenuRequested(const QPoint &pos)
{
	Q_UNUSED(pos);

	QMenu *menu = new QMenu(this);

	QMenu *menuFacet = menu->addMenu("Facet");
	menuFacet->addAction(new CMapAction("Current", OMA_FACET, 0xFFFFFFFF, (g_OrionMapSettings.GetFacet() == 0xFFFFFFFF), this));

	/*IFOR(i, 0, 6)
	{
		if ((g_FileManager.m_File[OFI_MAP_0_MUL + i].GetStart() == 0 && g_FileManager.m_File[OFI_MAP_0_UOP + i].GetStart() == 0) || g_FileManager.m_File[OFI_STAIDX_0_MUL + i].GetStart() == 0 || g_FileManager.m_File[OFI_STATICS_0_MUL + i].GetStart() == 0)
			continue;

		menuFacet->addAction(new CMapAction(g_FacetName[i], OMA_FACET, i, (g_OrionMapSettings.GetFacet() == (uint)i), this));
	}*/

	QMenu *menuFollow = menu->addMenu("Follow (only for Current facet)");
	menuFollow->addAction(new CMapAction("Player", OMA_FOLLOW_CHARACTER, g_PlayerSerial, g_OrionMapSettings.GetFollowPlayer(), this));

	menu->addSeparator();

	QMenu *menuZoom = menu->addMenu("Zoom");
	menuZoom->addAction(new CMapAction("x0.25", OMA_ZOOM, 1, (g_OrionMapSettings.GetZoom() == 1), this));
	menuZoom->addAction(new CMapAction("x0.5", OMA_ZOOM, 2, (g_OrionMapSettings.GetZoom() == 2), this));
	menuZoom->addAction(new CMapAction("x0.75", OMA_ZOOM, 3, (g_OrionMapSettings.GetZoom() == 3), this));
	menuZoom->addAction(new CMapAction("x1.0", OMA_ZOOM, 4, (g_OrionMapSettings.GetZoom() == 4), this));
	menuZoom->addAction(new CMapAction("x1.25", OMA_ZOOM, 5, (g_OrionMapSettings.GetZoom() == 5), this));
	menuZoom->addAction(new CMapAction("x1.5", OMA_ZOOM, 6, (g_OrionMapSettings.GetZoom() == 6), this));
	menuZoom->addAction(new CMapAction("x1.75", OMA_ZOOM, 7, (g_OrionMapSettings.GetZoom() == 7), this));
	menuZoom->addAction(new CMapAction("x2.0", OMA_ZOOM, 8, (g_OrionMapSettings.GetZoom() == 8), this));
	menuZoom->addAction(new CMapAction("x4.0", OMA_ZOOM, 16, (g_OrionMapSettings.GetZoom() == 16), this));

	menu->addSeparator();

	menu->addAction(new CMapAction("Rotate map", OMA_ROTATE_MAP, 0, g_OrionMapSettings.GetRotateMap(), this));

	menu->addAction(new CMapAction("Show coordinates", OMA_DISPLAY_COORDINATES, 0, g_OrionMapSettings.GetDisplayCoordinates(), this));
	menu->addAction(new CMapAction("Show altitude map", OMA_ALTITUDE_ONLY, 0, g_OrionMapSettings.GetAltitudeOnly(), this));

	if (!g_OrionMapSettings.GetAltitudeOnly())
	{
		menu->addAction(new CMapAction("Show statics", OMA_SHOW_STATICS, 0, g_OrionMapSettings.GetShowStatics(), this));
		menu->addAction(new CMapAction("Apply altitude", OMA_APPLY_ALTITUDE, 0, g_OrionMapSettings.GetApplyAltitude(), this));
	}

	//menu->addAction(new CMapAction("Smooth image", OMA_SMOOTH_IMAGE, 0, g_OrionMapSettings.GetSmoothImage(), this));

	menu->addSeparator();

	QMenu *menuOpacity = menu->addMenu("Window opacity");
	menuOpacity->addAction(new CMapAction("Opaque", OMA_TRANSPARENT_WINDOW, 0, (g_OrionMapSettings.GetOpacity() == 0), this));
	menuOpacity->addAction(new CMapAction("25%", OMA_TRANSPARENT_WINDOW, 1, (g_OrionMapSettings.GetOpacity() == 1), this));
	menuOpacity->addAction(new CMapAction("50%", OMA_TRANSPARENT_WINDOW, 2, (g_OrionMapSettings.GetOpacity() == 2), this));
	//menuOpacity->addAction(new CMapAction("75%", OMA_TRANSPARENT_WINDOW, 3, (g_OrionMapSettings.GetOpacity() == 3), this));

	menu->addAction(new CMapAction("Open display settings", OMA_OPEN_SETTINGS, 0, false, this));

	menu->popup(QCursor::pos());
}
//----------------------------------------------------------------------------------
void OrionMap::ChangeSettings(const CMapSettings &settings)
{
	g_OrionMapSettings.Copy(settings);
	g_OrionMapSettings.Save();
	m_WantRedraw = true;
}
//----------------------------------------------------------------------------------
void OrionMap::OnCloseSettings()
{
	RELEASE_POINTER(m_SettingsForm);
}
//----------------------------------------------------------------------------------
