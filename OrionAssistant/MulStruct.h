﻿/***********************************************************************************
**
** MulStruct.h
**
** Copyright (C) August 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef MULSTRUCT_H
#define MULSTRUCT_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"

#pragma pack (push,1)

typedef struct VERDATA_HEADER
{
	unsigned int FileID;
	unsigned int BlockID;
	unsigned int Position;
	unsigned int Size;
	unsigned int GumpData;
} *PVERDATA_HEADER;
//----------------------------------------------------------------------------------
typedef struct MAP_CELLS_EX
{
	unsigned short TileID;
	unsigned short Color;
	char Z;
} *PMAP_CELLS_EX;
//----------------------------------------------------------------------------------
typedef struct MAP_CELLS
{
	unsigned short TileID;
	char Z;
} *PMAP_CELLS;
//----------------------------------------------------------------------------------
typedef struct MAP_BLOCK
{
	unsigned int Header;
	MAP_CELLS Cells[64];
} *PMAP_BLOCK;
//----------------------------------------------------------------------------------
typedef struct STAIDX_BLOCK
{
	unsigned int Position;
	unsigned int Size;
	unsigned int Unknown;
} *PSTAIDX_BLOCK;
//----------------------------------------------------------------------------------
typedef struct STATICS_BLOCK
{
	unsigned short Color;
	unsigned char X;
	unsigned char Y;
	char Z;
	unsigned short Hue;
} *PSTATICS_BLOCK;
//----------------------------------------------------------------------------------
struct LAND_TILES_OLD
{
	unsigned int Flags;
	unsigned short TexID;
	char Name[20];
};
//----------------------------------------------------------------------------------
typedef struct LAND_GROUP_OLD
{
	unsigned int Unknown;
	LAND_TILES_OLD Tiles[32];
} *PLAND_GROUP_OLD;
//----------------------------------------------------------------------------------
struct STATIC_TILES_OLD
{
	unsigned int Flags;
	unsigned char Weight;
	unsigned char Quality;
	unsigned short Unknown;
	unsigned char Unknown1;
	unsigned char Quality1;
	unsigned short AnimID;
	unsigned char Unknown2;
	unsigned char Hue;
	unsigned char SittingOffset;
	unsigned char Unknown3;
	unsigned char Height;
	char Name[20];
};
//----------------------------------------------------------------------------------
typedef struct STATIC_GROUP_OLD
{
	unsigned int Unk;
	STATIC_TILES_OLD Tiles[32];
} *PSTATIC_GROUP_OLD;
//----------------------------------------------------------------------------------
struct LAND_TILES_NEW
{
	unsigned __int64 Flags;
	unsigned short TexID;
	char Name[20];
};
//----------------------------------------------------------------------------------
typedef struct LAND_GROUP_NEW
{
	unsigned int Unknown;
	LAND_TILES_NEW Tiles[32];
} *PLAND_GROUP_NEW;
//----------------------------------------------------------------------------------
struct STATIC_TILES_NEW
{
	unsigned __int64 Flags;
	unsigned char Weight;
	unsigned char Quality;
	unsigned short Unknown;
	unsigned char Unknown1;
	unsigned char Quality1;
	unsigned short AnimID;
	unsigned char Unknown2;
	unsigned char Hue;
	unsigned char SittingOffset;
	unsigned char Unknown3;
	unsigned char Height;
	char Name[20];
};
//----------------------------------------------------------------------------------
typedef struct STATIC_GROUP_NEW
{
	unsigned int Unk;
	STATIC_TILES_NEW Tiles[32];
} *PSTATIC_GROUP_NEW;
//----------------------------------------------------------------------------------
struct LAND_TILES
{
	unsigned __int64 Flags;
	unsigned short TexID;
	QString Name;
};
//----------------------------------------------------------------------------------
typedef struct LAND_GROUP
{
	unsigned int Unknown;
	LAND_TILES Tiles[32];
} *PLAND_GROUP;
//----------------------------------------------------------------------------------
struct STATIC_TILES
{
	unsigned __int64 Flags;
	unsigned char Weight;
	unsigned char Quality;
	unsigned short Unknown;
	unsigned char Unknown1;
	unsigned char Quality1;
	unsigned short AnimID;
	unsigned char Unknown2;
	unsigned char Hue;
	unsigned char SittingOffset;
	unsigned char Unknown3;
	unsigned char Height;
	QString Name;
};
//----------------------------------------------------------------------------------
typedef struct STATIC_GROUP
{
	unsigned int Unk;
	STATIC_TILES Tiles[32];
} *PSTATIC_GROUP;

#pragma pack (pop)
//----------------------------------------------------------------------------------
#endif
//----------------------------------------------------------------------------------
