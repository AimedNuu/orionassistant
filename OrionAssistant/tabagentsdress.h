/***********************************************************************************
**
** TabAgentsDress.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABAGENTSDRESS_H
#define TABAGENTSDRESS_H
//----------------------------------------------------------------------------------
#include <QWidget>
#include "../GUI/dresslistitem.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabAgentsDress;
}
//----------------------------------------------------------------------------------
class TabAgentsDress : public QWidget
{
	Q_OBJECT

private slots:
	void on_lw_Dress_clicked(const QModelIndex &index);

	void on_pb_DressCreate_clicked();

	void on_pb_DressSave_clicked();

	void on_pb_DressRemove_clicked();

	void on_pb_DressUse_clicked();

	void on_pb_DressUndress_clicked();

	void on_pb_DressSetCurrent_clicked();

	void on_pb_DressApplyHands_clicked();

	void on_pb_DressApplyArmor_clicked();

	void on_pb_DressClearSet_clicked();

	void on_pb_DressFromTarget_clicked();

private:
	Ui::TabAgentsDress *ui;

public slots:
	void OnListItemMoved();

public:
	explicit TabAgentsDress(QWidget *parent = 0);
	~TabAgentsDress();

	void AddDressItemInList(class CGameItem *item);

	void AddDressItemInList(CDressListItem *list, const int &layer, const uint &serial);

	void OnDeleteKeyClick(QWidget *widget);

	bool UndressBeforeDressing();

	bool DoubleClickForEquipItems();

	void LoadDress(const QString &path);

	void SaveDress(const QString &path);

	void ClearDressSet(CDressListItem *list);

	CDressListItem *GetDressSet(const QString &name);

	CDressListItem *AddDressSet(const QString &name);

	void DressSet(CDressListItem *dressSet);

	void DisarmSet();

	void UndressSet();
};
//----------------------------------------------------------------------------------
extern TabAgentsDress *g_TabAgentsDress;
//----------------------------------------------------------------------------------
#endif // TABAGENTSDRESS_H
//----------------------------------------------------------------------------------
