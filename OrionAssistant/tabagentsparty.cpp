// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabAgentsParty.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabagentsparty.h"
#include "ui_tabagentsparty.h"
#include "orionassistant.h"
#include "orionassistantform.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
#include "../GUI/worldobjectlistitem.h"
#include "../CommonItems/objectnamereceiver.h"
#include "../GameObjects/GameWorld.h"
#include "tablistsfriends.h"
#include "tablistsenemies.h"
//----------------------------------------------------------------------------------
TabAgentsParty *g_TabAgentsParty = nullptr;
//----------------------------------------------------------------------------------
TabAgentsParty::TabAgentsParty(QWidget *parent)
: QWidget(parent), ui(new Ui::TabAgentsParty)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabAgentsParty = this;

	connect(ui->lw_PartyAutoAccept, SIGNAL(itemDropped()), this, SLOT(OnListItemMoved()));
	connect(ui->lw_PartyAutoDecline, SIGNAL(itemDropped()), this, SLOT(OnListItemMoved()));
}
//----------------------------------------------------------------------------------
TabAgentsParty::~TabAgentsParty()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabAgentsParty = nullptr;
}
//----------------------------------------------------------------------------------
void TabAgentsParty::OnListItemMoved()
{
	OAFUN_DEBUG("");
	QString path = g_OrionAssistantForm->GetSaveConfigPath();

	if (!path.length())
		return;

	SaveParty(path + "/Party.xml");
}
//----------------------------------------------------------------------------------
void TabAgentsParty::on_lw_PartyAutoAccept_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f203");
	CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_PartyAutoAccept->item(index.row());

	if (item != nullptr)
	{
		ui->le_PartySerial->setText(COrionAssistant::SerialToText(item->GetSerial()));
		ui->le_PartyName->setText(item->GetName());
	}
}
//----------------------------------------------------------------------------------
void TabAgentsParty::on_pb_PartyAutoAcceptCreate_clicked()
{
	OAFUN_DEBUG("c28_f204");
	uint serial = COrionAssistant::TextToSerial(ui->le_PartySerial->text());

	if (!serial)
	{
		QMessageBox::critical(this, "Serial is empty", "Enter the object serial!");
		return;
	}

	IFOR(i, 0, ui->lw_PartyAutoAccept->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_PartyAutoAccept->item(i);

		if (item != nullptr && item->GetSerial() == serial)
		{
			QMessageBox::critical(this, "Serial is already exists", "Object serial is already exists!");
			return;
		}
	}

	new CWorldObjectListItem(serial, ui->le_PartyName->text(), true, ui->lw_PartyAutoAccept);

	ui->lw_PartyAutoAccept->setCurrentRow(ui->lw_PartyAutoAccept->count() - 1);
}
//----------------------------------------------------------------------------------
void TabAgentsParty::on_pb_PartyAutoAcceptSave_clicked()
{
	OAFUN_DEBUG("c28_f205");
	uint serial = COrionAssistant::TextToSerial(ui->le_PartySerial->text());

	if (!serial)
	{
		QMessageBox::critical(this, "Serial is empty", "Enter the object serial!");
		return;
	}

	CWorldObjectListItem *selected = (CWorldObjectListItem*)ui->lw_PartyAutoAccept->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Object is not selected", "Object is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_PartyAutoAccept->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_PartyAutoAccept->item(i);

		if (item != nullptr && item->GetSerial() == serial)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Serial is already exists", "Object serial is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->SetSerial(serial);
	selected->SetName(ui->le_PartyName->text());
	selected->UpdateText();
}
//----------------------------------------------------------------------------------
void TabAgentsParty::on_pb_PartyAutoAcceptRemove_clicked()
{
	OAFUN_DEBUG("c28_f206");
	QListWidgetItem *item = ui->lw_PartyAutoAccept->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_PartyAutoAccept->takeItem(ui->lw_PartyAutoAccept->row(item));

		if (item != nullptr)
			delete item;
	}
}
//----------------------------------------------------------------------------------
void TabAgentsParty::on_pb_PartyFromTarget_clicked()
{
	OAFUN_DEBUG("c28_f207");
	g_TargetHandler = &COrionAssistant::HandleTargetPartyObject;

	g_OrionAssistant.RequestTarget("Select a object for obtain serial for party");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
void TabAgentsParty::on_lw_PartyAutoDecline_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("c28_f209");
	CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_PartyAutoDecline->item(index.row());

	if (item != nullptr)
	{
		ui->le_PartySerial->setText(COrionAssistant::SerialToText(item->GetSerial()));
		ui->le_PartyName->setText(item->GetName());
	}
}
//----------------------------------------------------------------------------------
void TabAgentsParty::on_pb_PartyAutoDeclineCreate_clicked()
{
	OAFUN_DEBUG("c28_f210");
	uint serial = COrionAssistant::TextToSerial(ui->le_PartySerial->text());

	if (!serial)
	{
		QMessageBox::critical(this, "Serial is empty", "Enter the object serial!");
		return;
	}

	IFOR(i, 0, ui->lw_PartyAutoDecline->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_PartyAutoDecline->item(i);

		if (item != nullptr && item->GetSerial() == serial)
		{
			QMessageBox::critical(this, "Serial is already exists", "Object serial is already exists!");
			return;
		}
	}

	new CWorldObjectListItem(serial, ui->le_PartyName->text(), true, ui->lw_PartyAutoDecline);

	ui->lw_PartyAutoDecline->setCurrentRow(ui->lw_PartyAutoDecline->count() - 1);
}
//----------------------------------------------------------------------------------
void TabAgentsParty::on_pb_PartyAutoDeclineSave_clicked()
{
	OAFUN_DEBUG("c28_f211");
	uint serial = COrionAssistant::TextToSerial(ui->le_PartySerial->text());

	if (!serial)
	{
		QMessageBox::critical(this, "Serial is empty", "Enter the object serial!");
		return;
	}

	CWorldObjectListItem *selected = (CWorldObjectListItem*)ui->lw_PartyAutoDecline->currentItem();

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Object is not selected", "Object is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_PartyAutoDecline->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_PartyAutoDecline->item(i);

		if (item != nullptr && item->GetSerial() == serial)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Serial is already exists", "Object serial is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->SetSerial(serial);
	selected->SetName(ui->le_PartyName->text());
	selected->UpdateText();
}
//----------------------------------------------------------------------------------
void TabAgentsParty::on_pb_PartyAutoDeclineRemove_clicked()
{
	OAFUN_DEBUG("c28_f212");
	QListWidgetItem *item = ui->lw_PartyAutoDecline->currentItem();

	if (item != nullptr)
	{
		item = ui->lw_PartyAutoDecline->takeItem(ui->lw_PartyAutoDecline->row(item));

		if (item != nullptr)
			delete item;
	}
}
//----------------------------------------------------------------------------------
void TabAgentsParty::SetPartyObjectFromTarget(const uint &serial)
{
	OAFUN_DEBUG("c28_f208");
	if (serial)
	{
		ui->le_PartySerial->setText(COrionAssistant::SerialToText(serial));

		if (g_World != nullptr)
		{
			CGameObject *obj = g_World->FindWorldObject(serial);

			if (obj != nullptr)
				ui->le_PartyName->setText(obj->GetName());
		}
	}
}
//----------------------------------------------------------------------------------
bool TabAgentsParty::PartyInviteCanBeAutoAccepted(const uint &serial)
{
	OAFUN_DEBUG("c28_f213");
	if (ui->cb_PartyAutoAcceptAll->isChecked())
		return true;
	else if (ui->cb_PartyAutoAcceptFriendInvities->isChecked())
	{
		if (g_TabListsFriends->InFriendList(serial))
			return true;
	}

	IFOR(i, 0, ui->lw_PartyAutoAccept->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_PartyAutoAccept->item(i);

		if (item != nullptr && item->GetSerial() == serial && item->checkState() == Qt::Checked)
			return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
bool TabAgentsParty::PartyInviteCanBeAutoDeclined(const uint &serial)
{
	OAFUN_DEBUG("c28_f214");
	if (ui->cb_PartyAutoDeclineAll->isChecked())
		return true;
	else if (ui->cb_PartyAutoDeclineEnemyInvities->isChecked())
	{
		if (g_TabListsEnemies->InEnemyList(serial))
			return true;
	}

	IFOR(i, 0, ui->lw_PartyAutoDecline->count())
	{
		CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_PartyAutoDecline->item(i);

		if (item != nullptr && item->GetSerial() == serial && item->checkState() == Qt::Checked)
			return true;
	}

	return false;
}
//----------------------------------------------------------------------------------
void TabAgentsParty::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_PartyAutoAccept)
		on_pb_PartyAutoAcceptRemove_clicked();
	else if (widget == ui->lw_PartyAutoDecline)
		on_pb_PartyAutoDeclineRemove_clicked();
}
//----------------------------------------------------------------------------------
void TabAgentsParty::LoadParty(const QString &path)
{
	OAFUN_DEBUG("c28_f129");
	ui->lw_PartyAutoAccept->clear();
	ui->lw_PartyAutoDecline->clear();

	QFile file(path);

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;
		int count = 0;

		Q_UNUSED(version);
		Q_UNUSED(count);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("size"))
						count = attributes.value("size").toInt();

					if (attributes.hasAttribute("autoacceptall"))
						ui->cb_PartyAutoAcceptAll->setChecked(COrionAssistant::RawStringToBool(attributes.value("autoacceptall").toString()));

					if (attributes.hasAttribute("autoacceptfriends"))
						ui->cb_PartyAutoAcceptFriendInvities->setChecked(COrionAssistant::RawStringToBool(attributes.value("autoacceptfriends").toString()));

					if (attributes.hasAttribute("autodeclineall"))
						ui->cb_PartyAutoDeclineAll->setChecked(COrionAssistant::RawStringToBool(attributes.value("autodeclineall").toString()));

					if (attributes.hasAttribute("autodeclineenemies"))
						ui->cb_PartyAutoDeclineEnemyInvities->setChecked(COrionAssistant::RawStringToBool(attributes.value("autodeclineenemies").toString()));
				}
				else if (reader.name() == "acceptobject")
				{
					if (attributes.hasAttribute("id") && attributes.hasAttribute("name") && attributes.hasAttribute("enabled"))
					{
						new CWorldObjectListItem(COrionAssistant::TextToSerial(attributes.value("id").toString()), attributes.value("name").toString(),
										   COrionAssistant::RawStringToBool(attributes.value("enabled").toString()), ui->lw_PartyAutoAccept);
					}
				}
				else if (reader.name() == "declineobject")
				{
					if (attributes.hasAttribute("id") && attributes.hasAttribute("name") && attributes.hasAttribute("enabled"))
					{
						new CWorldObjectListItem(COrionAssistant::TextToSerial(attributes.value("id").toString()), attributes.value("name").toString(),
										   COrionAssistant::RawStringToBool(attributes.value("enabled").toString()), ui->lw_PartyAutoDecline);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabAgentsParty::SaveParty(const QString &path)
{
	OAFUN_DEBUG("c28_f137");
	QFile file(path);

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int countAccept = ui->lw_PartyAutoAccept->count();
		int countDecline = ui->lw_PartyAutoDecline->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");
		writter.writeAttribute("size", QString::number(countAccept + countDecline));
		writter.writeAttribute("autoacceptall", COrionAssistant::BoolToText(ui->cb_PartyAutoAcceptAll->isChecked()));
		writter.writeAttribute("autoacceptfriends", COrionAssistant::BoolToText(ui->cb_PartyAutoAcceptFriendInvities->isChecked()));
		writter.writeAttribute("autodeclineall", COrionAssistant::BoolToText(ui->cb_PartyAutoDeclineAll->isChecked()));
		writter.writeAttribute("autodeclineenemies", COrionAssistant::BoolToText(ui->cb_PartyAutoDeclineEnemyInvities->isChecked()));

		IFOR(i, 0, countAccept)
		{
			CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_PartyAutoAccept->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("acceptobject");

				writter.writeAttribute("id", COrionAssistant::SerialToText(item->GetSerial()));
				writter.writeAttribute("name", item->GetName());
				writter.writeAttribute("enabled", COrionAssistant::BoolToText(item->checkState() == Qt::Checked));

				writter.writeEndElement(); //acceptobject
			}
		}

		IFOR(i, 0, countDecline)
		{
			CWorldObjectListItem *item = (CWorldObjectListItem*)ui->lw_PartyAutoDecline->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("declineobject");

				writter.writeAttribute("id", COrionAssistant::SerialToText(item->GetSerial()));
				writter.writeAttribute("name", item->GetName());
				writter.writeAttribute("enabled", COrionAssistant::BoolToText(item->checkState() == Qt::Checked));

				writter.writeEndElement(); //declineobject
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
