// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** MapSettingsForm.cpp
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "mapsettingsform.h"
#include "ui_mapsettingsform.h"
#include "orionmap.h"
#include "orionassistantform.h"
#include "orionassistant.h"
#include <QPainter>
#include <QBitmap>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
//----------------------------------------------------------------------------------
CMapSettings g_OrionMapSettings;
//----------------------------------------------------------------------------------
CMapFontStyle g_MapFontStyle[MAP_FONT_STYLES_COUNT] =
{
	CMapFontStyle("Small 1", QFont("Tahoma", 8)),
	CMapFontStyle("Small 2", QFont("Verdana", 8)),
	CMapFontStyle("Small 3", QFont("Segoe UI", 8)),
	CMapFontStyle("Small 4", QFont("Arial", 8)),
	CMapFontStyle("Small 5", QFont("MS Sans Serif", 8)),
	CMapFontStyle("Big 1", QFont("Tahoma", 10)),
	CMapFontStyle("Big 2", QFont("Verdana", 10)),
	CMapFontStyle("Big 3", QFont("Segoe UI", 10)),
	CMapFontStyle("Big 4", QFont("Arial", 10)),
	CMapFontStyle("Big 5", QFont("MS Sans Serif", 10))
};
//----------------------------------------------------------------------------------
void CMapSettings::Copy(const CMapSettings &settings)
{
	m_ShowStatics = settings.GetShowStatics();
	m_ApplyAltitude = settings.GetApplyAltitude();
	m_AltitudeOnly = settings.GetAltitudeOnly();
	m_AltitudeOnly = settings.GetAltitudeOnly();
	m_SmoothImage = settings.GetSmoothImage();
	m_DisplayCoordinates = settings.GetDisplayCoordinates();
	m_ShowOnLogin = settings.GetShowOnLogin();
	m_ShowLastCorpse = settings.GetShowLastCorpse();
	m_MarkDeadMobiles = settings.GetMarkDeadMobiles();
	m_RotateMap = settings.GetRotateMap();

	m_PlayerFontStyle = settings.GetPlayerFontStyle();
	m_PlayerColor = settings.GetPlayerColor();
	m_PlayerShowHits = settings.GetPlayerShowHits();
	m_PlayerShowMana = settings.GetPlayerShowMana();
	m_PlayerShowStam = settings.GetPlayerShowStam();
	m_PlayerShowName = settings.GetPlayerShowName();

	m_PartyEnabled = settings.GetPartyEnabled();
	m_PartyFontStyle = settings.GetPartyFontStyle();
	m_PartyColor = settings.GetPartyColor();
	m_PartyShowHits = settings.GetPartyShowHits();
	m_PartyShowName = settings.GetPartyShowName();

	m_GuildEnabled = settings.GetGuildEnabled();
	m_GuildFontStyle = settings.GetGuildFontStyle();
	m_GuildColor = settings.GetGuildColor();
	m_GuildShowHits = settings.GetGuildShowHits();
	m_GuildShowName = settings.GetGuildShowName();

	m_FriendsEnabled = settings.GetFriendsEnabled();
	m_FriendsFontStyle = settings.GetFriendsFontStyle();
	m_FriendsColor = settings.GetFriendsColor();
	m_FriendsShowHits = settings.GetFriendsShowHits();
	m_FriendsShowName = settings.GetFriendsShowName();

	m_EnemiesEnabled = settings.GetEnemiesEnabled();
	m_EnemiesFontStyle = settings.GetEnemiesFontStyle();
	m_EnemiesColor = settings.GetEnemiesColor();
	m_EnemiesShowHits = settings.GetEnemiesShowHits();
	m_EnemiesShowName = settings.GetEnemiesShowName();

	m_OtherEnabled = settings.GetOtherEnabled();
	m_OtherFontStyle = settings.GetOtherFontStyle();
	m_OtherColor = settings.GetOtherColor();
	m_OtherShowHits = settings.GetOtherShowHits();
	m_OtherShowName = settings.GetOtherShowName();
}
//----------------------------------------------------------------------------------
void CMapSettings::Load()
{
	QFile file(g_DllPath + "/GlobalConfig/OrionMap.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("window_x"))
						m_WindowX = attributes.value("window_x").toInt();

					if (attributes.hasAttribute("window_y"))
						m_WindowY = attributes.value("window_y").toInt();

					if (attributes.hasAttribute("window_width"))
						m_WindowWidth = attributes.value("window_width").toInt();

					if (attributes.hasAttribute("window_height"))
						m_WindowHeight = attributes.value("window_height").toInt();

					if (attributes.hasAttribute("show_statics"))
						m_ShowStatics = COrionAssistant::RawStringToBool(attributes.value("show_statics").toString());

					if (attributes.hasAttribute("apply_altitude"))
						m_ApplyAltitude = COrionAssistant::RawStringToBool(attributes.value("apply_altitude").toString());

					if (attributes.hasAttribute("altitude_only"))
						m_AltitudeOnly = COrionAssistant::RawStringToBool(attributes.value("altitude_only").toString());

					if (attributes.hasAttribute("smooth_image"))
						m_SmoothImage = COrionAssistant::RawStringToBool(attributes.value("smooth_image").toString());

					if (attributes.hasAttribute("display_coordinates"))
						m_DisplayCoordinates = COrionAssistant::RawStringToBool(attributes.value("display_coordinates").toString());

					if (attributes.hasAttribute("show_on_login"))
						m_ShowOnLogin = COrionAssistant::RawStringToBool(attributes.value("show_on_login").toString());

					if (attributes.hasAttribute("show_last_corpse"))
						m_ShowLastCorpse = COrionAssistant::RawStringToBool(attributes.value("show_last_corpse").toString());

					if (attributes.hasAttribute("mark_dead_mobiles"))
						m_MarkDeadMobiles = COrionAssistant::RawStringToBool(attributes.value("mark_dead_mobiles").toString());

					if (attributes.hasAttribute("opacity"))
						m_Opacity = attributes.value("opacity").toInt();

					if (attributes.hasAttribute("rotate_map"))
						m_RotateMap = COrionAssistant::RawStringToBool(attributes.value("rotate_map").toString());



					if (attributes.hasAttribute("player_font_style"))
						m_PlayerFontStyle = attributes.value("player_font_style").toInt();

					if (attributes.hasAttribute("player_color"))
						m_PlayerColor.setRgba((QRgb)COrionAssistant::TextToSerial(attributes.value("player_color").toString()));

					if (attributes.hasAttribute("player_show_hits"))
						m_PlayerShowHits = COrionAssistant::RawStringToBool(attributes.value("player_show_hits").toString());

					if (attributes.hasAttribute("player_show_mana"))
						m_PlayerShowMana = COrionAssistant::RawStringToBool(attributes.value("player_show_mana").toString());

					if (attributes.hasAttribute("player_show_stam"))
						m_PlayerShowStam = COrionAssistant::RawStringToBool(attributes.value("player_show_stam").toString());

					if (attributes.hasAttribute("player_show_name"))
						m_PlayerShowName = COrionAssistant::RawStringToBool(attributes.value("player_show_name").toString());



					if (attributes.hasAttribute("party_enabled"))
						m_PartyEnabled = COrionAssistant::RawStringToBool(attributes.value("party_enabled").toString());

					if (attributes.hasAttribute("party_font_style"))
						m_PartyFontStyle = attributes.value("party_font_style").toInt();

					if (attributes.hasAttribute("party_color"))
						m_PartyColor.setRgba((QRgb)COrionAssistant::TextToSerial(attributes.value("party_color").toString()));

					if (attributes.hasAttribute("party_show_hits"))
						m_PartyShowHits = COrionAssistant::RawStringToBool(attributes.value("party_show_hits").toString());

					if (attributes.hasAttribute("party_show_name"))
						m_PartyShowName = COrionAssistant::RawStringToBool(attributes.value("party_show_name").toString());



					if (attributes.hasAttribute("guild_enabled"))
						m_GuildEnabled = COrionAssistant::RawStringToBool(attributes.value("guild_enabled").toString());

					if (attributes.hasAttribute("guild_font_style"))
						m_GuildFontStyle = attributes.value("guild_font_style").toInt();

					if (attributes.hasAttribute("guild_color"))
						m_GuildColor.setRgba((QRgb)COrionAssistant::TextToSerial(attributes.value("guild_color").toString()));

					if (attributes.hasAttribute("guild_show_hits"))
						m_GuildShowHits = COrionAssistant::RawStringToBool(attributes.value("guild_show_hits").toString());

					if (attributes.hasAttribute("guild_show_name"))
						m_GuildShowName = COrionAssistant::RawStringToBool(attributes.value("guild_show_name").toString());



					if (attributes.hasAttribute("friends_enabled"))
						m_FriendsEnabled = COrionAssistant::RawStringToBool(attributes.value("friends_enabled").toString());

					if (attributes.hasAttribute("friends_font_style"))
						m_FriendsFontStyle = attributes.value("friends_font_style").toInt();

					if (attributes.hasAttribute("friends_color"))
						m_FriendsColor.setRgba((QRgb)COrionAssistant::TextToSerial(attributes.value("friends_color").toString()));

					if (attributes.hasAttribute("friends_show_hits"))
						m_FriendsShowHits = COrionAssistant::RawStringToBool(attributes.value("friends_show_hits").toString());

					if (attributes.hasAttribute("friends_show_name"))
						m_FriendsShowName = COrionAssistant::RawStringToBool(attributes.value("friends_show_name").toString());



					if (attributes.hasAttribute("enemies_enabled"))
						m_EnemiesEnabled = COrionAssistant::RawStringToBool(attributes.value("enemies_enabled").toString());

					if (attributes.hasAttribute("enemies_font_style"))
						m_EnemiesFontStyle = attributes.value("enemies_font_style").toInt();

					if (attributes.hasAttribute("enemies_color"))
						m_EnemiesColor.setRgba((QRgb)COrionAssistant::TextToSerial(attributes.value("enemies_color").toString()));

					if (attributes.hasAttribute("enemies_show_hits"))
						m_EnemiesShowHits = COrionAssistant::RawStringToBool(attributes.value("enemies_show_hits").toString());

					if (attributes.hasAttribute("enemies_show_name"))
						m_EnemiesShowName = COrionAssistant::RawStringToBool(attributes.value("enemies_show_name").toString());



					if (attributes.hasAttribute("other_enabled"))
						m_OtherEnabled = COrionAssistant::RawStringToBool(attributes.value("other_enabled").toString());

					if (attributes.hasAttribute("other_font_style"))
						m_OtherFontStyle = attributes.value("other_font_style").toInt();

					if (attributes.hasAttribute("other_color"))
						m_OtherColor.setRgba((QRgb)COrionAssistant::TextToSerial(attributes.value("other_color").toString()));

					if (attributes.hasAttribute("other_show_hits"))
						m_OtherShowHits = COrionAssistant::RawStringToBool(attributes.value("other_show_hits").toString());

					if (attributes.hasAttribute("other_sho_name"))
						m_OtherShowName = COrionAssistant::RawStringToBool(attributes.value("other_sho_name").toString());
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void CMapSettings::Save()
{
	OAFUN_DEBUG("");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/OrionMap.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		writter.writeStartElement("data");

		writter.writeAttribute("window_x", QString::number(m_WindowX));
		writter.writeAttribute("window_y", QString::number(m_WindowY));
		writter.writeAttribute("window_width", QString::number(m_WindowWidth));
		writter.writeAttribute("window_height", QString::number(m_WindowHeight));

		writter.writeAttribute("show_statics", COrionAssistant::BoolToText(m_ShowStatics));
		writter.writeAttribute("apply_altitude", COrionAssistant::BoolToText(m_ApplyAltitude));
		writter.writeAttribute("altitude_only", COrionAssistant::BoolToText(m_AltitudeOnly));
		writter.writeAttribute("smooth_image", COrionAssistant::BoolToText(m_SmoothImage));
		writter.writeAttribute("display_coordinates", COrionAssistant::BoolToText(m_DisplayCoordinates));
		writter.writeAttribute("show_on_login", COrionAssistant::BoolToText(m_ShowOnLogin));
		writter.writeAttribute("show_last_corpse", COrionAssistant::BoolToText(m_ShowLastCorpse));
		writter.writeAttribute("mark_dead_mobiles", COrionAssistant::BoolToText(m_MarkDeadMobiles));
		writter.writeAttribute("opacity", QString::number(m_Opacity));
		writter.writeAttribute("rotate_map", COrionAssistant::BoolToText(m_RotateMap));

		writter.writeAttribute("player_font_style", QString::number(m_PlayerFontStyle));
		writter.writeAttribute("player_color", COrionAssistant::SerialToText((uint)m_PlayerColor.rgba()));
		writter.writeAttribute("player_show_hits", COrionAssistant::BoolToText(m_PlayerShowHits));
		writter.writeAttribute("player_show_mana", COrionAssistant::BoolToText(m_PlayerShowMana));
		writter.writeAttribute("player_show_stam", COrionAssistant::BoolToText(m_PlayerShowStam));
		writter.writeAttribute("player_show_name", COrionAssistant::BoolToText(m_PlayerShowName));

		writter.writeAttribute("party_enabled", COrionAssistant::BoolToText(m_PartyEnabled));
		writter.writeAttribute("party_font_style", QString::number(m_PartyFontStyle));
		writter.writeAttribute("party_color", COrionAssistant::SerialToText((uint)m_PartyColor.rgba()));
		writter.writeAttribute("party_show_hits", COrionAssistant::BoolToText(m_PartyShowHits));
		writter.writeAttribute("party_show_name", COrionAssistant::BoolToText(m_PartyShowName));

		writter.writeAttribute("guild_enabled", COrionAssistant::BoolToText(m_GuildEnabled));
		writter.writeAttribute("guild_font_style", QString::number(m_GuildFontStyle));
		writter.writeAttribute("guild_color", COrionAssistant::SerialToText((uint)m_GuildColor.rgba()));
		writter.writeAttribute("guild_show_hits", COrionAssistant::BoolToText(m_GuildShowHits));
		writter.writeAttribute("guild_show_name", COrionAssistant::BoolToText(m_GuildShowName));

		writter.writeAttribute("friends_enabled", COrionAssistant::BoolToText(m_FriendsEnabled));
		writter.writeAttribute("friends_font_style", QString::number(m_FriendsFontStyle));
		writter.writeAttribute("friends_color", COrionAssistant::SerialToText((uint)m_FriendsColor.rgba()));
		writter.writeAttribute("friends_show_hits", COrionAssistant::BoolToText(m_FriendsShowHits));
		writter.writeAttribute("friends_show_name", COrionAssistant::BoolToText(m_FriendsShowName));

		writter.writeAttribute("enemies_enabled", COrionAssistant::BoolToText(m_EnemiesEnabled));
		writter.writeAttribute("enemies_font_style", QString::number(m_EnemiesFontStyle));
		writter.writeAttribute("enemies_color", COrionAssistant::SerialToText((uint)m_EnemiesColor.rgba()));
		writter.writeAttribute("enemies_show_hits", COrionAssistant::BoolToText(m_EnemiesShowHits));
		writter.writeAttribute("enemies_show_name", COrionAssistant::BoolToText(m_EnemiesShowName));

		writter.writeAttribute("other_enabled", COrionAssistant::BoolToText(m_OtherEnabled));
		writter.writeAttribute("other_font_style", QString::number(m_OtherFontStyle));
		writter.writeAttribute("other_color", COrionAssistant::SerialToText((uint)m_OtherColor.rgba()));
		writter.writeAttribute("other_show_hits", COrionAssistant::BoolToText(m_OtherShowHits));
		writter.writeAttribute("other_sho_name", COrionAssistant::BoolToText(m_OtherShowName));

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
MapSettingsForm::MapSettingsForm(QWidget *parent)
: QDialog(parent), ui(new Ui::MapSettingsForm)
{
	ui->setupUi(this);

	setWindowIcon(QPixmap(":/resource/images/map_icon.png"));

	setWindowFlags((windowFlags() /*| Qt::WindowStaysOnTopHint*/) & ~Qt::WindowContextHelpButtonHint);
	setFixedSize(size());

	m_ImageCorpse = QImage(":/resource/images/corpse.png").scaled(15, 15);
	QImage img = m_ImageCorpse.createAlphaMask();
	IFOR(i, 0, img.width())
	{
		IFOR(j, 0, img.height())
		{
			const QRgb &pixel = m_ImageCorpse.pixel(i, j);

			if (pixel != 0xFFFFFFFF)
				img.setPixel(i, j, 0);
		}
	}
	m_ImageCorpse.setAlphaChannel(img);

	m_PixmapMap = QPixmap(":/resource/images/map_example.png");

	IFOR(i, 0, MAP_FONT_STYLES_COUNT)
	{
		const CMapFontStyle &fontStyle = g_MapFontStyle[i];

		ui->cb_PlayerFontStyle->addItem(fontStyle.GetName());
		ui->cb_PartyFontStyle->addItem(fontStyle.GetName());
		ui->cb_GuildFontStyle->addItem(fontStyle.GetName());
		ui->cb_FriendsFontStyle->addItem(fontStyle.GetName());
		ui->cb_EnemiesFontStyle->addItem(fontStyle.GetName());
		ui->cb_OtherFontStyle->addItem(fontStyle.GetName());
	}

	connect(ui->cb_ShowStatics, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_ApplyAltitude, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_ShowAltitudeMap, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_SmoothImage, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_ShowCoordinates, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_ShowOnLogin, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_ShowLastCorpse, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_MarkDeadMobiles, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_RotateMap, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));

	connect(ui->cb_PlayerFontStyle, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboBoxIndexChanged(int)));
	connect(ui->cb_PartyFontStyle, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboBoxIndexChanged(int)));
	connect(ui->cb_GuildFontStyle, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboBoxIndexChanged(int)));
	connect(ui->cb_FriendsFontStyle, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboBoxIndexChanged(int)));
	connect(ui->cb_EnemiesFontStyle, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboBoxIndexChanged(int)));
	connect(ui->cb_OtherFontStyle, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboBoxIndexChanged(int)));

	connect(ui->cb_PlayerShowHits, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_PlayerShowMana, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_PlayerShowStam, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_PlayerShowName, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->gb_Party, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_PartyShowHits, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_PartyShowName, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->gb_Guild, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_GuildShowHits, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_GuildShowName, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->gb_Friends, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_FriendsShowHits, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_FriendsShowName, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->gb_Enemies, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_EnemiesShowHits, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_EnemiesShowName, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->gb_Other, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_OtherShowHits, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
	connect(ui->cb_OtherShowName, SIGNAL(clicked()), this, SLOT(OnCheckBoxChecked()));
}
//----------------------------------------------------------------------------------
MapSettingsForm::~MapSettingsForm()
{
	delete ui;
}
//----------------------------------------------------------------------------------
void MapSettingsForm::Init(const CMapSettings &settings)
{
	m_Loading = true;

	m_Settings.Copy(settings);
	m_Changed = false;

	ui->cb_ShowStatics->setChecked(m_Settings.GetShowStatics());
	ui->cb_ApplyAltitude->setChecked(m_Settings.GetApplyAltitude());
	ui->cb_ShowAltitudeMap->setChecked(m_Settings.GetAltitudeOnly());
	ui->cb_SmoothImage->setChecked(m_Settings.GetSmoothImage());
	ui->cb_ShowCoordinates->setChecked(m_Settings.GetDisplayCoordinates());
	ui->cb_ShowOnLogin->setChecked(m_Settings.GetShowOnLogin());
	ui->cb_ShowLastCorpse->setChecked(m_Settings.GetShowLastCorpse());
	ui->cb_MarkDeadMobiles->setChecked(m_Settings.GetMarkDeadMobiles());
	ui->cb_RotateMap->setChecked(m_Settings.GetRotateMap());


	ui->cb_PlayerFontStyle->setCurrentIndex(m_Settings.GetPlayerFontStyle());
	ui->pb_PlayerColor->ChangeColor(m_Settings.GetPlayerColor());
	ui->cb_PlayerShowHits->setChecked(m_Settings.GetPlayerShowHits());
	ui->cb_PlayerShowMana->setChecked(m_Settings.GetPlayerShowMana());
	ui->cb_PlayerShowStam->setChecked(m_Settings.GetPlayerShowStam());
	ui->cb_PlayerShowName->setChecked(m_Settings.GetPlayerShowName());

	ui->gb_Party->setChecked(m_Settings.GetPartyEnabled());
	ui->cb_PartyFontStyle->setCurrentIndex(m_Settings.GetPartyFontStyle());
	ui->pb_PartyColor->ChangeColor(m_Settings.GetPartyColor());
	ui->cb_PartyShowHits->setChecked(m_Settings.GetPartyShowHits());
	ui->cb_PartyShowName->setChecked(m_Settings.GetPartyShowName());

	ui->gb_Guild->setChecked(m_Settings.GetGuildEnabled());
	ui->cb_GuildFontStyle->setCurrentIndex(m_Settings.GetGuildFontStyle());
	ui->pb_GuildColor->ChangeColor(m_Settings.GetGuildColor());
	ui->cb_GuildShowHits->setChecked(m_Settings.GetGuildShowHits());
	ui->cb_GuildShowName->setChecked(m_Settings.GetGuildShowName());

	ui->gb_Friends->setChecked(m_Settings.GetFriendsEnabled());
	ui->cb_FriendsFontStyle->setCurrentIndex(m_Settings.GetFriendsFontStyle());
	ui->pb_FriendsColor->ChangeColor(m_Settings.GetFriendsColor());
	ui->cb_FriendsShowHits->setChecked(m_Settings.GetFriendsShowHits());
	ui->cb_FriendsShowName->setChecked(m_Settings.GetFriendsShowName());

	ui->gb_Enemies->setChecked(m_Settings.GetEnemiesEnabled());
	ui->cb_EnemiesFontStyle->setCurrentIndex(m_Settings.GetEnemiesFontStyle());
	ui->pb_EnemiesColor->ChangeColor(m_Settings.GetEnemiesColor());
	ui->cb_EnemiesShowHits->setChecked(m_Settings.GetEnemiesShowHits());
	ui->cb_EnemiesShowName->setChecked(m_Settings.GetEnemiesShowName());

	ui->gb_Other->setChecked(m_Settings.GetOtherEnabled());
	ui->cb_OtherFontStyle->setCurrentIndex(m_Settings.GetOtherFontStyle());
	ui->pb_OtherColor->ChangeColor(m_Settings.GetOtherColor());
	ui->cb_OtherShowHits->setChecked(m_Settings.GetOtherShowHits());
	ui->cb_OtherShowName->setChecked(m_Settings.GetOtherShowName());

	m_Loading = false;
}
//----------------------------------------------------------------------------------
void MapSettingsForm::closeEvent(QCloseEvent *event)
{
	event->accept();
	on_pb_Close_clicked();
}
//----------------------------------------------------------------------------------
void MapSettingsForm::paintEvent(QPaintEvent *event)
{
	event->accept();

	int width = ui->widget->width();
	int height = ui->widget->height();

	QPainter painter(this);
	painter.drawPixmap(ui->widget->geometry(), m_PixmapMap);

	painter.translate(ui->widget->pos());

	struct DRAW_OBJECT
	{
		QString Name;
		int X;
		int Y;
		int Width;
		int Height;
		QColor Color;
		bool Enabled;
		int FontStyle;
		bool DrawName;
		bool DrawHits;
		bool DrawMana;
		bool DrawStam;
	};

	int start = (int)!ui->cb_ShowLastCorpse->isChecked();
	int end = (ui->cb_MarkDeadMobiles->isChecked() ? 13 : 7);

	DRAW_OBJECT table[13] =
	{
		{ "Corpse",					width / 2 - 3,		height / 2 - 3 - 40,	m_ImageCorpse.width(), m_ImageCorpse.height(),	QColor(Qt::white),		true,							ui->cb_PlayerFontStyle->currentIndex(),	ui->cb_PlayerShowName->isChecked(), ui->cb_PlayerShowHits->isChecked(), ui->cb_PlayerShowMana->isChecked(), ui->cb_PlayerShowStam->isChecked() },
		{ "Player",					width / 2 - 3,		height / 2 - 3,			6, 6,	ui->pb_PlayerColor->GetColor(),		true,							ui->cb_PlayerFontStyle->currentIndex(),	ui->cb_PlayerShowName->isChecked(), ui->cb_PlayerShowHits->isChecked(), ui->cb_PlayerShowMana->isChecked(), ui->cb_PlayerShowStam->isChecked() },
		{ "Party Member",			width / 2 - 3 + 80,	height / 2 - 3 - 60,	4, 4,	ui->pb_PartyColor->GetColor(),		ui->gb_Party->isChecked(),		ui->cb_PartyFontStyle->currentIndex(),	ui->cb_PartyShowName->isChecked(), ui->cb_PartyShowHits->isChecked(), false, false },
		{ "Guild Member",			width / 2 - 3 + 80,	height / 2 - 3 + 60,	4, 4,	ui->pb_GuildColor->GetColor(),		ui->gb_Guild->isChecked(),		ui->cb_GuildFontStyle->currentIndex(),	ui->cb_GuildShowName->isChecked(), ui->cb_GuildShowHits->isChecked(), false, false },
		{ "Friend",					width / 2 - 3 - 80,	height / 2 - 3 - 60,	4, 4,	ui->pb_FriendsColor->GetColor(),	ui->gb_Friends->isChecked(),	ui->cb_FriendsFontStyle->currentIndex(),	ui->cb_FriendsShowName->isChecked(), ui->cb_FriendsShowHits->isChecked(), false, false },
		{ "Enemy",					width / 2 - 3 - 80,	height / 2 - 3 + 70,	4, 4,	ui->pb_EnemiesColor->GetColor(),	ui->gb_Enemies->isChecked(),	ui->cb_EnemiesFontStyle->currentIndex(),	ui->cb_EnemiesShowName->isChecked(), ui->cb_EnemiesShowHits->isChecked(), false, false },
		{ "Other mobile",			width / 2 - 3 - 4,	height / 2 - 3 + 110,	4, 4,	ui->pb_OtherColor->GetColor(),		ui->gb_Other->isChecked(),		ui->cb_OtherFontStyle->currentIndex(),	ui->cb_OtherShowName->isChecked(), ui->cb_OtherShowHits->isChecked(), false, false },
		{ "Dead Player",			width / 2 - 3 + 60,	height / 2 - 3 + 20,	6, 6,	ui->pb_PlayerColor->GetColor(),		true,							ui->cb_PlayerFontStyle->currentIndex(),	ui->cb_PlayerShowName->isChecked(), ui->cb_PlayerShowHits->isChecked(), ui->cb_PlayerShowMana->isChecked(), ui->cb_PlayerShowStam->isChecked() },
		{ "Dead Party Member",		width / 2 - 3 + 30,	height / 2 - 3 - 80,	6, 6,	ui->pb_PartyColor->GetColor(),		ui->gb_Party->isChecked(),		ui->cb_PartyFontStyle->currentIndex(),	ui->cb_PartyShowName->isChecked(), ui->cb_PartyShowHits->isChecked(), false, false },
		{ "Dead Guild Member",		width / 2 - 3 + 70,	height / 2 - 3 + 115,	6, 6,	ui->pb_GuildColor->GetColor(),		ui->gb_Guild->isChecked(),		ui->cb_GuildFontStyle->currentIndex(),	ui->cb_GuildShowName->isChecked(), ui->cb_GuildShowHits->isChecked(), false, false },
		{ "Dead Friend",			width / 2 - 3 - 65,	height / 2 - 3 - 90,	6, 6,	ui->pb_FriendsColor->GetColor(),	ui->gb_Friends->isChecked(),	ui->cb_FriendsFontStyle->currentIndex(),	ui->cb_FriendsShowName->isChecked(), ui->cb_FriendsShowHits->isChecked(), false, false },
		{ "Dead Enemy",				width / 2 - 3 - 95,	height / 2 - 3 + 100,	6, 6,	ui->pb_EnemiesColor->GetColor(),	ui->gb_Enemies->isChecked(),	ui->cb_EnemiesFontStyle->currentIndex(),	ui->cb_EnemiesShowName->isChecked(), ui->cb_EnemiesShowHits->isChecked(), false, false },
		{ "Dead Other mobile",		width / 2 - 3 - 4,	height / 2 - 3 - 110,	6, 6,	ui->pb_OtherColor->GetColor(),		ui->gb_Other->isChecked(),		ui->cb_OtherFontStyle->currentIndex(),	ui->cb_OtherShowName->isChecked(), ui->cb_OtherShowHits->isChecked(), false, false },
	};

	painter.setBackgroundMode(Qt::TransparentMode);

	IFOR(i, start, end)
	{
		DRAW_OBJECT &obj = table[i];

		if (obj.Enabled)
		{
			int x = obj.X;
			int y = obj.Y;

			int offsetX = obj.Width / 2;
			int offsetY = obj.Height / 2;

			QPen pen(obj.Color);
			painter.setPen(pen);
			painter.setBrush(QBrush(obj.Color));

			if (obj.DrawName)
			{
				painter.setFont(g_MapFontStyle[obj.FontStyle].GetFont());
				painter.drawText(x - (QFontMetrics(painter.font()).width(obj.Name) / 2), y - offsetY - 3, obj.Name);
			}

			if (!i)
				painter.drawImage(x - offsetX, y - offsetY, m_ImageCorpse);
			else if (i > 6)
			{
				painter.drawLine(x - offsetX, y - offsetY, x + offsetX, y + offsetY);
				painter.drawLine(x - offsetX, y + offsetY, x + offsetX, y - offsetY);
			}
			else
			{
				painter.drawEllipse(x - offsetX, y - offsetY, obj.Width, obj.Height);

				if (obj.DrawHits)
				{
					y += 1 + offsetY;

					painter.setBrush(Qt::black);
					painter.setPen(Qt::black);
					painter.drawRect(x - 10, y, 20, 2);
					painter.setPen(pen);
					painter.drawLine(x - 9, y + 1, x + 9, y + 1);

					y += 2;

					if (obj.DrawMana)
					{
						painter.setBrush(Qt::black);
						painter.setPen(Qt::black);
						painter.drawRect(x - 10, y, 20, 2);
						painter.setPen(pen);
						painter.drawLine(x - 9, y + 1, x + 9, y + 1);
					}

					y += 2;

					if (obj.DrawStam)
					{
						painter.setBrush(Qt::black);
						painter.setPen(Qt::black);
						painter.drawRect(x - 10, y, 20, 2);
						painter.setPen(pen);
						painter.drawLine(x - 9, y + 1, x + 9, y + 1);
					}
				}
			}
		}
	}
}
//----------------------------------------------------------------------------------
void MapSettingsForm::OnComboBoxIndexChanged(int index)
{
	Q_UNUSED(index);

	if (m_Loading)
		return;

	m_Changed = true;
	repaint();
}
//----------------------------------------------------------------------------------
void MapSettingsForm::OnCheckBoxChecked()
{
	if (m_Loading)
		return;

	m_Changed = true;
	repaint();
}
//----------------------------------------------------------------------------------
void MapSettingsForm::OnColorSelection(QColoredPushButton *button)
{
	if (g_OrionAssistantForm->SelectButtonColor(button, this))
		m_Changed = true;
}
//----------------------------------------------------------------------------------
void MapSettingsForm::on_pb_PlayerColor_clicked()
{
	OnColorSelection(ui->pb_PlayerColor);
}
//----------------------------------------------------------------------------------
void MapSettingsForm::on_pb_PartyColor_clicked()
{
	OnColorSelection(ui->pb_PartyColor);
}
//----------------------------------------------------------------------------------
void MapSettingsForm::on_pb_GuildColor_clicked()
{
	OnColorSelection(ui->pb_GuildColor);
}
//----------------------------------------------------------------------------------
void MapSettingsForm::on_pb_FriendsColor_clicked()
{
	OnColorSelection(ui->pb_FriendsColor);
}
//----------------------------------------------------------------------------------
void MapSettingsForm::on_pb_EnemiesColor_clicked()
{
	OnColorSelection(ui->pb_EnemiesColor);
}
//----------------------------------------------------------------------------------
void MapSettingsForm::on_pb_OtherColor_clicked()
{
	OnColorSelection(ui->pb_OtherColor);
}
//----------------------------------------------------------------------------------
void MapSettingsForm::on_pb_ApplyChanges_clicked()
{
	m_Changed = false;

	m_Settings.SetShowStatics(ui->cb_ShowStatics->isChecked());
	m_Settings.SetApplyAltitude(ui->cb_ApplyAltitude->isChecked());
	m_Settings.SetAltitudeOnly(ui->cb_ShowAltitudeMap->isChecked());
	m_Settings.SetSmoothImage(ui->cb_SmoothImage->isChecked());
	m_Settings.SetDisplayCoordinates(ui->cb_ShowCoordinates->isChecked());
	m_Settings.SetShowOnLogin(ui->cb_ShowOnLogin->isChecked());
	m_Settings.SetShowLastCorpse(ui->cb_ShowLastCorpse->isChecked());
	m_Settings.SetMarkDeadMobiles(ui->cb_MarkDeadMobiles->isChecked());
	m_Settings.SetRotateMap(ui->cb_RotateMap->isChecked());

	m_Settings.SetPlayerFontStyle(ui->cb_PlayerFontStyle->currentIndex());
	m_Settings.SetPlayerColor(ui->pb_PlayerColor->GetColor());
	m_Settings.SetPlayerShowHits(ui->cb_PlayerShowHits->isChecked());
	m_Settings.SetPlayerShowMana(ui->cb_PlayerShowMana->isChecked());
	m_Settings.SetPlayerShowStam(ui->cb_PlayerShowStam->isChecked());
	m_Settings.SetPlayerShowName(ui->cb_PlayerShowName->isChecked());

	m_Settings.SetPartyEnabled(ui->gb_Party->isChecked());
	m_Settings.SetPartyFontStyle(ui->cb_PartyFontStyle->currentIndex());
	m_Settings.SetPartyColor(ui->pb_PartyColor->GetColor());
	m_Settings.SetPartyShowHits(ui->cb_PartyShowHits->isChecked());
	m_Settings.SetPartyShowName(ui->cb_PartyShowName->isChecked());

	m_Settings.SetGuildEnabled(ui->gb_Guild->isChecked());
	m_Settings.SetGuildFontStyle(ui->cb_GuildFontStyle->currentIndex());
	m_Settings.SetGuildColor(ui->pb_GuildColor->GetColor());
	m_Settings.SetGuildShowHits(ui->cb_GuildShowHits->isChecked());
	m_Settings.SetGuildShowName(ui->cb_GuildShowName->isChecked());

	m_Settings.SetFriendsEnabled(ui->gb_Friends->isChecked());
	m_Settings.SetFriendsFontStyle(ui->cb_FriendsFontStyle->currentIndex());
	m_Settings.SetFriendsColor(ui->pb_FriendsColor->GetColor());
	m_Settings.SetFriendsShowHits(ui->cb_FriendsShowHits->isChecked());
	m_Settings.SetFriendsShowName(ui->cb_FriendsShowName->isChecked());

	m_Settings.SetEnemiesEnabled(ui->gb_Enemies->isChecked());
	m_Settings.SetEnemiesFontStyle(ui->cb_EnemiesFontStyle->currentIndex());
	m_Settings.SetEnemiesColor(ui->pb_EnemiesColor->GetColor());
	m_Settings.SetEnemiesShowHits(ui->cb_EnemiesShowHits->isChecked());
	m_Settings.SetEnemiesShowName(ui->cb_EnemiesShowName->isChecked());

	m_Settings.SetOtherEnabled(ui->gb_Other->isChecked());
	m_Settings.SetOtherFontStyle(ui->cb_OtherFontStyle->currentIndex());
	m_Settings.SetOtherColor(ui->pb_OtherColor->GetColor());
	m_Settings.SetOtherShowHits(ui->cb_OtherShowHits->isChecked());
	m_Settings.SetOtherShowName(ui->cb_OtherShowName->isChecked());

	if (g_OrionMap != nullptr)
		g_OrionMap->ChangeSettings(m_Settings);
}
//----------------------------------------------------------------------------------
void MapSettingsForm::on_pb_Close_clicked()
{
	if (m_Changed)
	{
		QMessageBox::StandardButton button = QMessageBox::question(this, "Unsaved changes!", "You have unsaved changes! Save current settings?", QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::No | QMessageBox::StandardButton::Cancel);

		if (button == QMessageBox::StandardButton::Yes)
			on_pb_ApplyChanges_clicked();
		else if (button == QMessageBox::StandardButton::Cancel)
			return;
	}

	hide();

	if (g_OrionMap != nullptr)
		g_OrionMap->OnCloseSettings();
}
//----------------------------------------------------------------------------------
