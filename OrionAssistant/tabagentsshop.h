/***********************************************************************************
**
** TabAgentsShop.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABAGENTSSHOP_H
#define TABAGENTSSHOP_H
//----------------------------------------------------------------------------------
#include <QWidget>
#include "../Managers/DataReader.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabAgentsShop;
}
//----------------------------------------------------------------------------------
enum SHOP_LIST_WAIT_STATE
{
	SLWS_NORNAL = 0,
	SLWS_BUY,
	SLWS_SELL
};
//----------------------------------------------------------------------------------
class TabAgentsShop : public QWidget
{
	Q_OBJECT

	SETGET(SHOP_LIST_WAIT_STATE, State, SLWS_NORNAL)
	SETGET(uint, WaitTimeout, 0)
	SETGET(QString, WaitShopListName, "")
	SETGET(int, WaitShopDelay, 0)

private:
	Ui::TabAgentsShop *ui;

	bool ProcessShopList();

private slots:
	void on_lw_ShopLists_clicked(const QModelIndex &index);

	void on_pb_ShopListCreate_clicked();

	void on_pb_ShopListSave_clicked();

	void on_pb_ShopListRemove_clicked();

	void on_pb_BuySelectedList_clicked();

	void on_pb_SellSelectedList_clicked();

	void on_lw_ShopLists_doubleClicked(const QModelIndex &index);

	void on_pb_EditShopListItems_clicked();

public slots:
	void SaveShopList();

public:
	explicit TabAgentsShop(QWidget *parent = 0);
	~TabAgentsShop();

	void OnDeleteKeyClick(QWidget *widget);

	void LoadShopList();

	bool IsActiveShopping();

	void SendShopRequest(const SHOP_LIST_WAIT_STATE &state, const QString &listName, const wstring &text, const int &shopDelay);

	bool OnPacketReceivedOpenContaier(const uint &serial);

	bool OnPacketReceivedBuyList(CDataReader &reader);

	bool OnPacketSendBuyReply();

	bool OnPacketReceivedBuyReply(CDataReader &reader);

	bool OnPacketReceivedSellList(CDataReader &reader);

	bool OnPacketSendSellReply();
};
//----------------------------------------------------------------------------------
extern TabAgentsShop *g_TabAgentsShop;
//----------------------------------------------------------------------------------
#endif // TABAGENTSSHOP_H
//----------------------------------------------------------------------------------
