// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabObjectInspector.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabobjectinspector.h"
#include "ui_tabobjectinspector.h"
#include "../GameObjects/GameItem.h"
#include "../GameObjects/GamePlayer.h"
#include "../GUI/objectinfotreeitem.h"
#include "orionassistant.h"
#include <QClipboard>
#include <QPainter>
#include "../Managers/filemanager.h"
//----------------------------------------------------------------------------------
TabObjectInspector::TabObjectInspector(QWidget *parent)
: QWidget(parent), ui(new Ui::TabObjectInspector)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
}
//----------------------------------------------------------------------------------
TabObjectInspector::~TabObjectInspector()
{
	OAFUN_DEBUG("");
	delete ui;
}
//----------------------------------------------------------------------------------
void TabObjectInspector::CheckUpdate(CGameObject *obj)
{
	OAFUN_DEBUG("");
	if (obj == nullptr || obj->GetSerial() != m_Serial)
		return;

	IFOR(i, 0, ui->tw_Info->topLevelItemCount())
	{
		CObjectInfoTreeItem *item = (CObjectInfoTreeItem*)ui->tw_Info->topLevelItem(i);

		if (item == nullptr)
			continue;

		if (item->GetKey() == OIK_GAME_OBJECT)
		{
			if (m_Graphic != obj->GetGraphic() || m_Color != obj->GetColor() || m_Count != obj->GetCount())
			{
				m_Graphic = obj->GetGraphic();
				m_Color = obj->GetColor();
				m_Count = obj->GetCount();

				if (!obj->GetNPC())
					UpdateImage(item);
			}

			IFOR(j, 0, item->childCount())
			{
				CObjectInfoTreeItem *subItem = (CObjectInfoTreeItem*)item->child(j);

				if (subItem == nullptr)
					continue;

				if (subItem->GetKey() == OIK_SERIAL)
					subItem->SetValue(COrionAssistant::SerialToText(obj->GetSerial()));
				else if (subItem->GetKey() == OIK_CONTAINER)
					subItem->SetValue(COrionAssistant::SerialToText(obj->GetContainer()));
				else if (subItem->GetKey() == OIK_TOP_OBJECT)
				{
					CGameObject *topObject = obj->GetTopObject();
					uint topObjectSerial = ((topObject != nullptr && topObject != obj) ? topObject->GetSerial() : 0xFFFFFFFF);

					subItem->SetValue(COrionAssistant::SerialToText(topObjectSerial));
				}
				else if (subItem->GetKey() == OIK_NAME)
					subItem->SetValue(obj->GetName());
				else if (subItem->GetKey() == OIK_GRAPHIC)
					subItem->SetValue(COrionAssistant::GraphicToText(obj->GetGraphic()));
				else if (subItem->GetKey() == OIK_COLOR)
					subItem->SetValue(COrionAssistant::GraphicToText(obj->GetColor()));
				else if (subItem->GetKey() == OIK_COUNT)
					subItem->SetValue(QString::number(obj->GetCount()));
				else if (subItem->GetKey() == OIK_X)
					subItem->SetValue(QString::number(obj->GetX()));
				else if (subItem->GetKey() == OIK_Y)
					subItem->SetValue(QString::number(obj->GetY()));
				else if (subItem->GetKey() == OIK_Z)
					subItem->SetValue(QString::number(obj->GetZ()));
				else if (subItem->GetKey() == OIK_MAP)
					subItem->SetValue(QString::number(obj->GetMapIndex()));
				else if (subItem->GetKey() == OIK_FLAGS)
				{
					subItem->SetValue("0x" + QString::number(obj->GetFlags(), 16));

					IFOR(k, 0, subItem->childCount())
					{
						CObjectInfoTreeItem *subSubItem = (CObjectInfoTreeItem*)subItem->child(k);

						if (subSubItem == nullptr)
							continue;

						if (subSubItem->GetKey() == OIK_FLAGS_FROZEN)
							subSubItem->SetValue(COrionAssistant::BoolToText(obj->Frozen()));
						else if (subSubItem->GetKey() == OIK_FLAGS_POISONED)
							subSubItem->SetValue(COrionAssistant::BoolToText(obj->Poisoned()));
						else if (subSubItem->GetKey() == OIK_FLAGS_YELLOW_HITS)
							subSubItem->SetValue(COrionAssistant::BoolToText(obj->YellowHits()));
						//else if (subSubItem->GetKey() == OIK_FLAGS_IGNORE_CHARACTERS)
						//	subSubItem->SetValue(COrionAssistant::BoolToText(obj->IgnoreCharacters()));
						else if (subSubItem->GetKey() == OIK_FLAGS_LOCKED)
							subSubItem->SetValue(COrionAssistant::BoolToText(obj->Locked()));
						else if (subSubItem->GetKey() == OIK_FLAGS_IN_WAR_MODE)
							subSubItem->SetValue(COrionAssistant::BoolToText(obj->InWarMode()));
						else if (subSubItem->GetKey() == OIK_FLAGS_HIDDEN)
							subSubItem->SetValue(COrionAssistant::BoolToText(obj->Hidden()));
						else if (subSubItem->GetKey() == OIK_FLAGS_FLYING)
							subSubItem->SetValue(COrionAssistant::BoolToText(obj->Flying()));
					}
				}
				else if (subItem->GetKey() == OIK_IGNORED)
					subItem->SetValue(COrionAssistant::BoolToText(obj->GetIgnored()));
			}
		}
		else if (item->GetKey() == OIK_GAME_ITEM && !obj->GetNPC())
		{
			IFOR(j, 0, item->childCount())
			{
				CObjectInfoTreeItem *subItem = (CObjectInfoTreeItem*)item->child(j);

				if (subItem == nullptr)
					continue;

				if (subItem->GetKey() == OIK_LAYER)
					subItem->SetValue(g_LayerName[((CGameItem*)obj)->GetLayer() % 30] + " (" +QString::number(((CGameItem*)obj)->GetLayer()) + ")");
			}
		}
		else if (item->GetKey() == OIK_GAME_CHARACTER && obj->GetNPC())
		{
			CGameCharacter *gameCharacter = (CGameCharacter*)obj;

			IFOR(j, 0, item->childCount())
			{
				CObjectInfoTreeItem *subItem = (CObjectInfoTreeItem*)item->child(j);

				if (subItem == nullptr)
					continue;

				if (subItem->GetKey() == OIK_HITS)
					subItem->SetValue(QString::number(gameCharacter->GetHits()));
				else if (subItem->GetKey() == OIK_MAX_HITS)
					subItem->SetValue(QString::number(gameCharacter->GetMaxHits()));
				else if (subItem->GetKey() == OIK_MANA)
					subItem->SetValue(QString::number(gameCharacter->GetMana()));
				else if (subItem->GetKey() == OIK_MAX_MANA)
					subItem->SetValue(QString::number(gameCharacter->GetMaxMana()));
				else if (subItem->GetKey() == OIK_STAM)
					subItem->SetValue(QString::number(gameCharacter->GetStam()));
				else if (subItem->GetKey() == OIK_MAX_STAM)
					subItem->SetValue(QString::number(gameCharacter->GetMaxStam()));
				else if (subItem->GetKey() == OIK_SEX)
					subItem->SetValue((gameCharacter->GetSex() ? "Man" : "Woman"));
				else if (subItem->GetKey() == OIK_RACE)
				{
					const QString raceNames[3] = { "Human", "Elf", "Gargoyle" };
					subItem->SetValue(raceNames[((int)gameCharacter->GetRace() - 1) % 3]);
				}
				else if (subItem->GetKey() == OIK_DIRECTION)
					subItem->SetValue(QString::number(gameCharacter->GetDirection()));
				else if (subItem->GetKey() == OIK_RUNNING)
					subItem->SetValue(COrionAssistant::BoolToText(gameCharacter->GetDirection() & 0x80));
				else if (subItem->GetKey() == OIK_NOTORIETY)
					subItem->SetValue(QString::number(gameCharacter->GetNotoriety()));
				else if (subItem->GetKey() == OIK_CAN_CHANGE_NAME)
					subItem->SetValue(COrionAssistant::BoolToText(gameCharacter->GetCanChangeName()));
			}
		}
		else if (item->GetKey() == OIK_GAME_PLAYER && obj->IsPlayer())
		{
			const QString statLockStateNames[3] = { "Up", "Down", "Lock" };

			IFOR(j, 0, item->childCount())
			{
				CObjectInfoTreeItem *subItem = (CObjectInfoTreeItem*)item->child(j);

				if (subItem == nullptr)
					continue;

				if (subItem->GetKey() == OIK_STR)
					subItem->SetValue(QString::number(g_Player->GetStr()));
				else if (subItem->GetKey() == OIK_INT)
					subItem->SetValue(QString::number(g_Player->GetInt()));
				else if (subItem->GetKey() == OIK_DEX)
					subItem->SetValue(QString::number(g_Player->GetDex()));
				else if (subItem->GetKey() == OIK_STATS_CAP)
					subItem->SetValue(QString::number(g_Player->GetStatsCap()));
				else if (subItem->GetKey() == OIK_LOCK_STR)
					subItem->SetValue(statLockStateNames[g_Player->GetLockStr() % 3]);
				else if (subItem->GetKey() == OIK_LOCK_INT)
					subItem->SetValue(statLockStateNames[g_Player->GetLockInt() % 3]);
				else if (subItem->GetKey() == OIK_LOCK_DEX)
					subItem->SetValue(statLockStateNames[g_Player->GetLockDex() % 3]);
				else if (subItem->GetKey() == OIK_WEIGHT)
					subItem->SetValue(QString::number(g_Player->GetWeight()));
				else if (subItem->GetKey() == OIK_MAX_WEIGHT)
					subItem->SetValue(QString::number(g_Player->GetMaxWeight()));
				else if (subItem->GetKey() == OIK_ARMOR)
					subItem->SetValue(QString::number(g_Player->GetArmor()));
				else if (subItem->GetKey() == OIK_GOLD)
					subItem->SetValue(QString::number(g_Player->GetGold()));
				else if (subItem->GetKey() == OIK_WARMODE)
					subItem->SetValue(COrionAssistant::BoolToText(g_Player->GetWarmode()));
				else if (subItem->GetKey() == OIK_FOLLOWERS)
					subItem->SetValue(QString::number(g_Player->GetFollowers()));
				else if (subItem->GetKey() == OIK_MAX_FOLLOWERS)
					subItem->SetValue(QString::number(g_Player->GetMaxFollowers()));
				else if (subItem->GetKey() == OIK_FIRE_RESISTANCE)
					subItem->SetValue(QString::number(g_Player->GetFireResistance()));
				else if (subItem->GetKey() == OIK_COLD_RESISTANCE)
					subItem->SetValue(QString::number(g_Player->GetColdResistance()));
				else if (subItem->GetKey() == OIK_POISON_RESISTANCE)
					subItem->SetValue(QString::number(g_Player->GetPoisonResistance()));
				else if (subItem->GetKey() == OIK_ENERGY_RESISTANCE)
					subItem->SetValue(QString::number(g_Player->GetEnergyResistance()));
				else if (subItem->GetKey() == OIK_LUCK)
					subItem->SetValue(QString::number(g_Player->GetLuck()));
				else if (subItem->GetKey() == OIK_MIN_DAMAGE)
					subItem->SetValue(QString::number(g_Player->GetMinDamage()));
				else if (subItem->GetKey() == OIK_MAX_DAMAGE)
					subItem->SetValue(QString::number(g_Player->GetMaxDamage()));
				else if (subItem->GetKey() == OIK_TITHING_POINTS)
					subItem->SetValue(QString::number(g_Player->GetTithingPoints()));
				else if (subItem->GetKey() == OIK_STEALTH_STEPS)
					subItem->SetValue(QString::number(g_Player->GetStealthSteps()));
			}
		}
	}
}
//----------------------------------------------------------------------------------
void TabObjectInspector::Update(CGameObject *obj)
{
	OAFUN_DEBUG("");
	if (obj == nullptr)
		return;

	m_Serial = obj->GetSerial();
	m_Graphic = obj->GetGraphic();
	m_Color = obj->GetColor();
	m_Count = obj->GetCount();

	ui->tw_Info->clear();

	CObjectInfoTreeItem *gameObjectItem = new CObjectInfoTreeItem(true, "Game Object", "", OIK_GAME_OBJECT);
	ui->tw_Info->addTopLevelItem(gameObjectItem);
	gameObjectItem->setExpanded(true);

	if (!obj->GetNPC())
		UpdateImage(gameObjectItem);
	else
		ui->vs_ImageSpace->changeSize(1, 1);

	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Serial: ", COrionAssistant::SerialToText(obj->GetSerial()), OIK_SERIAL) );
	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Container: ", COrionAssistant::SerialToText(obj->GetContainer()), OIK_CONTAINER) );

	CGameObject *topObject = obj->GetTopObject();
	uint topObjectSerial = ((topObject != nullptr && topObject != obj) ? topObject->GetSerial() : 0xFFFFFFFF);

	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Top Object: ", COrionAssistant::SerialToText(topObjectSerial), OIK_TOP_OBJECT) );

	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Name: ", obj->GetName(), OIK_NAME) );
	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Graphic: ", COrionAssistant::GraphicToText(obj->GetGraphic()), OIK_GRAPHIC) );
	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Color: ", COrionAssistant::GraphicToText(obj->GetColor()), OIK_COLOR) );
	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Count: ", QString::number(obj->GetCount()), OIK_COUNT) );
	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "X: ", QString::number(obj->GetX()), OIK_X) );
	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Y: ", QString::number(obj->GetY()), OIK_Y) );
	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Z: ", QString::number(obj->GetZ()), OIK_Z) );
	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Map: ", QString::number(obj->GetMapIndex()), OIK_MAP) );

	CObjectInfoTreeItem *flagsItem = new CObjectInfoTreeItem(false, "Flags: ", "0x" + QString::number(obj->GetFlags(), 16), OIK_FLAGS);
	gameObjectItem->addChild(flagsItem);
	flagsItem->setExpanded(true);

	flagsItem->addChild( new CObjectInfoTreeItem(false, "Frozen: ", COrionAssistant::BoolToText(obj->Frozen()), OIK_FLAGS_FROZEN) );
	flagsItem->addChild( new CObjectInfoTreeItem(false, "Poisoned: ", COrionAssistant::BoolToText(obj->Poisoned()), OIK_FLAGS_POISONED) );
	flagsItem->addChild( new CObjectInfoTreeItem(false, "Yellow Hits: ", COrionAssistant::BoolToText(obj->YellowHits()), OIK_FLAGS_YELLOW_HITS) );
	//flagsItem->addChild( new CObjectInfoTreeItem(false, "Ignore Characters: ", COrionAssistant::BoolToText(obj->IgnoreCharacters()), OIK_FLAGS_IGNORE_CHARACTERS) );
	flagsItem->addChild( new CObjectInfoTreeItem(false, "Locked: ", COrionAssistant::BoolToText(obj->Locked()), OIK_FLAGS_LOCKED) );
	flagsItem->addChild( new CObjectInfoTreeItem(false, "In War Mode: ", COrionAssistant::BoolToText(obj->InWarMode()), OIK_FLAGS_IN_WAR_MODE) );
	flagsItem->addChild( new CObjectInfoTreeItem(false, "Hidden: ", COrionAssistant::BoolToText(obj->Hidden()), OIK_FLAGS_HIDDEN) );
	flagsItem->addChild( new CObjectInfoTreeItem(false, "Flying: ", COrionAssistant::BoolToText(obj->Flying()), OIK_FLAGS_FLYING) );

	gameObjectItem->addChild( new CObjectInfoTreeItem(false, "Ignored: ", COrionAssistant::BoolToText(obj->GetIgnored()), OIK_IGNORED) );

	if (!obj->GetNPC())
	{
		CObjectInfoTreeItem *gameItemItem = new CObjectInfoTreeItem(true, "Game Item", (obj->IsCorpse() ? " (Corpse)" : ""), OIK_GAME_ITEM);
		ui->tw_Info->addTopLevelItem(gameItemItem);
		gameItemItem->setExpanded(true);

		gameItemItem->addChild( new CObjectInfoTreeItem(false, "Layer: ", g_LayerName[((CGameItem*)obj)->GetLayer() % 30] + " (" +QString::number(((CGameItem*)obj)->GetLayer()) + ")", OIK_LAYER) );
	}
	else
	{
		CObjectInfoTreeItem *gameCharacterItem = new CObjectInfoTreeItem(true, "Game Character", (obj->IsHuman() ? " (Humanoid)" : ""), OIK_GAME_CHARACTER);
		ui->tw_Info->addTopLevelItem(gameCharacterItem);
		gameCharacterItem->setExpanded(true);

		CGameCharacter *gameCharacter = (CGameCharacter*)obj;
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Hits: ", QString::number(gameCharacter->GetHits()), OIK_HITS) );
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Max Hits: ", QString::number(gameCharacter->GetMaxHits()), OIK_MAX_HITS) );
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Mana: ", QString::number(gameCharacter->GetMana()), OIK_MANA) );
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Max Mana: ", QString::number(gameCharacter->GetMaxMana()), OIK_MAX_MANA) );
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Stam: ", QString::number(gameCharacter->GetStam()), OIK_STAM) );
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Max Stam: ", QString::number(gameCharacter->GetMaxStam()), OIK_MAX_STAM) );
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Sex: ", (gameCharacter->GetSex() ? "Man" : "Woman"), OIK_SEX) );
		const QString raceNames[3] = { "Human", "Elf", "Gargoyle" };
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Race: ", raceNames[((int)gameCharacter->GetRace() - 1) % 3], OIK_RACE) );
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Direction: ", QString::number(gameCharacter->GetDirection()), OIK_DIRECTION) );
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Running: ", COrionAssistant::BoolToText(gameCharacter->GetDirection() & 0x80), OIK_RUNNING) );
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Notoriety: ", QString::number(gameCharacter->GetNotoriety()), OIK_NOTORIETY) );
		gameCharacterItem->addChild( new CObjectInfoTreeItem(false, "Can Change Name: ", COrionAssistant::BoolToText(gameCharacter->GetCanChangeName()), OIK_CAN_CHANGE_NAME) );

		if (obj->IsPlayer())
		{
			CObjectInfoTreeItem *gamePlayerItem = new CObjectInfoTreeItem(true, "Game Player", "", OIK_GAME_PLAYER);
			ui->tw_Info->addTopLevelItem(gamePlayerItem);
			gamePlayerItem->setExpanded(true);

			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Str: ", QString::number(g_Player->GetStr()), OIK_STR) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Int: ", QString::number(g_Player->GetInt()), OIK_INT) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Dex: ", QString::number(g_Player->GetDex()), OIK_DEX) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Stats Cap: ", QString::number(g_Player->GetStatsCap()), OIK_STATS_CAP) );
			const QString statLockStateNames[3] = { "Up", "Down", "Lock" };
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Lock State Str: ", statLockStateNames[g_Player->GetLockStr() % 3], OIK_LOCK_STR) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Lock State Int: ", statLockStateNames[g_Player->GetLockInt() % 3], OIK_LOCK_INT) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Lock State Dex: ", statLockStateNames[g_Player->GetLockDex() % 3], OIK_LOCK_DEX) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Weight: ", QString::number(g_Player->GetWeight()), OIK_WEIGHT) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Max Weight: ", QString::number(g_Player->GetMaxWeight()), OIK_MAX_WEIGHT) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Armor: ", QString::number(g_Player->GetArmor()), OIK_ARMOR) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Gold: ", QString::number(g_Player->GetGold()), OIK_GOLD) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Warmode: ", COrionAssistant::BoolToText(g_Player->GetWarmode()), OIK_WARMODE) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Followers: ", QString::number(g_Player->GetFollowers()), OIK_FOLLOWERS) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Max Followers: ", QString::number(g_Player->GetMaxFollowers()), OIK_MAX_FOLLOWERS) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Fire Resistance: ", QString::number(g_Player->GetFireResistance()), OIK_FIRE_RESISTANCE) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Cold Resistance: ", QString::number(g_Player->GetColdResistance()), OIK_COLD_RESISTANCE) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Poison Resistance: ", QString::number(g_Player->GetPoisonResistance()), OIK_POISON_RESISTANCE) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Energy Resistance: ", QString::number(g_Player->GetEnergyResistance()), OIK_ENERGY_RESISTANCE) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Luck: ", QString::number(g_Player->GetLuck()), OIK_LUCK) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Min Damage: ", QString::number(g_Player->GetMinDamage()), OIK_MIN_DAMAGE) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Max Damage: ", QString::number(g_Player->GetMaxDamage()), OIK_MAX_DAMAGE) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Tithing Points: ", QString::number(g_Player->GetTithingPoints()), OIK_TITHING_POINTS) );
			gamePlayerItem->addChild( new CObjectInfoTreeItem(false, "Stealth Steps: ", QString::number(g_Player->GetStealthSteps()), OIK_STEALTH_STEPS) );
		}
	}
}
//----------------------------------------------------------------------------------
void TabObjectInspector::UpdateImage(QTreeWidgetItem *item)
{
	OAFUN_DEBUG("");
	if (item == nullptr)
		return;

	size_t address = g_FileManager.GetStaticArtInfo(m_Graphic).GetAddress();

	if (address == 0)
		return;

	ushort graphic = m_Graphic;
	pushort sizes = (pushort)(address + 4);

	int dataWidth = *sizes;
	sizes++;

	if (!dataWidth || dataWidth >= 1024)
		return;

	int dataHeight = *sizes;
	sizes++;

	if (!dataHeight || (dataHeight * 2) > 5120)
		return;

	if (graphic == 0x0EED || graphic == 0x0EEE)
		graphic = 0x0EEF; //Fix gold display

	uint64 tileFlags = g_GetStaticFlags(graphic);

	int stackOffset = 0;
	m_Pixels.clear();

	if ((tileFlags & 0x00000800) && graphic != 0x0EEF && m_Count > 1)
	{
		stackOffset = 5;
		m_Pixels.resize((dataWidth + 5) * (dataHeight + 5), 0);
	}
	else
		m_Pixels.resize(dataWidth * dataHeight, 0);

	bool partialHue = (tileFlags & 0x00040000);

	pushort lineOffsets = sizes;
	PVOID dataStart = (PVOID)((uint)sizes + (dataHeight * 2));

	int X = 0;
	int Y = 0;
	ushort XOffs = 0;
	ushort Run = 0;

	FUNCDEF_GET_COLOR *funGetColor = (partialHue ? g_ClientInterface->Client->ColorManager->GetPartialHueColor : (FUNCDEF_GET_COLOR*)g_ClientInterface->Client->ColorManager->GetColor);
	FUNCDEF_GET_COLOR16TO32 *funColor16To32 = g_ClientInterface->Client->ColorManager->Color16To32;

	pushort ptr = (pushort)((uint)dataStart + (*lineOffsets));

	int imageWidth = dataWidth + stackOffset;
	int imageHeight = dataHeight + stackOffset;

	while (Y < dataHeight)
	{
		XOffs = *ptr++;
		Run = *ptr++;

		if (XOffs + Run >= 2048)
			return;
		else if (XOffs + Run != 0)
		{
			X += XOffs;
			int pos = (Y * imageWidth) + X;

			IFOR(j, 0, Run)
			{
				if (*ptr)
				{
					uint col = 0;

					if (m_Color)
						col = funGetColor(*ptr, m_Color);
					else
						col = funColor16To32(*ptr);

					puchar pcol = (puchar)&col;
					uchar bcol = pcol[0];
					pcol[0] = pcol[2];
					pcol[2] = bcol;

					m_Pixels[pos + j] = col;
				}

				ptr++;
			}

			X += Run;
		}
		else
		{
			X = 0;
			Y++;
			ptr = (pushort)((uint)dataStart + (lineOffsets[Y] * 2));
		}
	}

	if (stackOffset)
	{
		DFOR(j, dataHeight - 1, 0)
		{
			int srcPosY = (j * imageWidth);
			int dstPosY = ((j + stackOffset) * imageWidth);

			DFOR(i, dataWidth - 1, 0)
			{
				int srcPos = srcPosY + i;
				uint pixel = m_Pixels[srcPos];

				if (pixel)
				{
					int dstPos = dstPosY + i + stackOffset;
					m_Pixels[dstPos] = pixel;
				}
			}
		}
	}

	ui->vs_ImageSpace->changeSize(imageWidth, imageHeight);
	m_Image = QImage((puchar)&m_Pixels[0], imageWidth, imageHeight, QImage::Format_RGB32);
	repaint();
}
//----------------------------------------------------------------------------------
void TabObjectInspector::paintEvent(QPaintEvent *event)
{
	OAFUN_DEBUG("");
	event->accept();

	if (!m_Image.isNull())
		QPainter(this).drawImage(0, 0, m_Image);
}
//----------------------------------------------------------------------------------
void TabObjectInspector::on_tw_Info_itemClicked(QTreeWidgetItem *item, int column)
{
	OAFUN_DEBUG("");
	Q_UNUSED(column);

	if (item != nullptr)
	{
		CObjectInfoTreeItem *info = (CObjectInfoTreeItem*)item;

		if (!info->GetGroup())
		{
			QClipboard *clipboard = qApp->clipboard();

			if (clipboard != nullptr)
				clipboard->setText(info->GetValue());
		}
	}
}
//----------------------------------------------------------------------------------
