/***********************************************************************************
**
** ObjectInspectorForm.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OBJECTINSPECTORFORM_H
#define OBJECTINSPECTORFORM_H
//----------------------------------------------------------------------------------
#include <QMainWindow>
//----------------------------------------------------------------------------------
namespace Ui
{
	class ObjectInspectorForm;
}
//----------------------------------------------------------------------------------
class ObjectInspectorForm : public QMainWindow
{
	Q_OBJECT

private:
	Ui::ObjectInspectorForm *ui;

	class TabObjectInspector *FindTab(const uint &serial);

public slots:
	void onCloseTab(int index);

public:
	explicit ObjectInspectorForm(QWidget *parent = 0);
	~ObjectInspectorForm();

	void CheckUpdate(class CGameObject *obj);

	void Update(class CGameObject *obj);
};
//----------------------------------------------------------------------------------
extern ObjectInspectorForm *g_ObjectInspectorForm;
//----------------------------------------------------------------------------------
#endif // OBJECTINSPECTORFORM_H
//----------------------------------------------------------------------------------
