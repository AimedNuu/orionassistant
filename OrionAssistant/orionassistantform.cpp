// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** OrionAssistantForm.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "orionassistantform.h"
#include "ui_orionassistantform.h"
#include <windows.h>
#include "orionassistant.h"
#include "Packets.h"
#include "../GameObjects/gameworld.h"
#include "../GameObjects/gameplayer.h"
#include <QMenu>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
#include <QColorDialog>
#include "textdialog.h"
#include "../GameObjects/gameworld.h"
#include "../GUI/DressItemListItem.h"
#include "../Managers/textcommandmanager.h"
#include "../Managers/packetmanager.h"
#include "../Managers/spellmanager.h"
#include "../CommonItems/binaryfile.h"
#include "scripteditordialog.h"
#include "../Managers/corpsemanager.h"
#include <QFileDialog>
#include "../Managers/filemanager.h"
#include "../Managers/clilocmanager.h"
#include "../Managers/commandmanager.h"
#include "tabmain.h"
#include "tabdisplay.h"
#include "tabliststypes.h"
#include "tablistsfind.h"
#include "tablistsignore.h"
#include "tablistsfriends.h"
#include "tablistsenemies.h"
#include "tabscripts.h"
#include "tabagentsdress.h"
#include "tabagentsparty.h"
#include "tabskills.h"
#include "tabfiltersspeech.h"
#include "tabfiltersreplaces.h"
#include "tabfilterssound.h"
#include "objectinspectorform.h"
#include "shipcontrolform.h"
#include "tabagentsshop.h"
#include "shopprogressform.h"
#include "shoplisteditorform.h"
#include "housecontrolform.h"
#include "animalandvendorcontrolform.h"
#include "orionmap.h"

#include "hotkeysform.h"
//----------------------------------------------------------------------------------
OrionAssistantForm *g_OrionAssistantForm = nullptr;

//! Icons:
//! https://www.iconfinder.com/iconsets/buttons-9
//! https://www.iconfinder.com/iconsets/Hand_Drawn_Web_Icon_Set
//! https://www.iconfinder.com/iconsets/app_iconset_creative_nerds
//----------------------------------------------------------------------------------
OrionAssistantForm::OrionAssistantForm(QWidget *parent)
: QMainWindow(parent), ui(new Ui::OrionAssistantForm), m_TrayIcon(this)
{
	OAFUN_DEBUG("c28_f1");

	ui->setupUi(this);

	EnableMenuItem(GetSystemMenu((HWND)winId(), 0), SC_CLOSE, MF_GRAYED);

	qsrand(GetTickCount());

	m_AutoOpenDoorTimer.setSingleShot(true);
	m_AutoOpenDoorTimer.setTimerType(Qt::PreciseTimer);
	connect(&m_AutoOpenDoorTimer, SIGNAL(timeout()), this, SLOT(onAutoOpenDoorTimer()));
}
//----------------------------------------------------------------------------------
OrionAssistantForm::~OrionAssistantForm()
{
	delete ui;
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::UpdateAssistTitle(const QString &playerNameText)
{
	OAFUN_DEBUG("c28_f2");
	QString appTitle = "Orion assistant v";
	setWindowTitle(appTitle + APP_VERSION + playerNameText);
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::Init()
{
	OAFUN_DEBUG("c28_f3");

	connect(&m_TrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(OnTrayIconActivated(QSystemTrayIcon::ActivationReason)));

	QIcon icon(QPixmap(":/resource/images/tray.png"));
	setWindowIcon(icon);
	m_TrayIcon.setIcon(icon);
	m_TrayIcon.setVisible(false);

	g_StartTimer.start();
	g_LastUseObjectTimer.start();

	m_NoExecuteGUIEvents = true;

	CTextCommandManager::InitCommands();

	g_TextDialog = new TextDialog(nullptr);
	g_ScriptEditorDialog = new ScriptEditorDialog(nullptr);
	g_ObjectInspectorForm = new ObjectInspectorForm(nullptr);
	g_ShopProgressForm = new ShopProgressForm(nullptr);
	g_ShopListEditorForm = new ShopListEditorForm(nullptr);
	g_HotkeysForm = new HotkeysForm(nullptr);

	setFixedSize(size());

	if (GetProcAddress(GetModuleHandle(L"kernel32.dll"), "wine_get_unix_file_name"))
	{
		LOG("Started on Linux operation system\n");
		g_LinuxOS = true;
	}

	uint dwVersion = GetVersion();
	uint dwMajorVersion = (uint)(LOBYTE(LOWORD(dwVersion)));

	if (dwMajorVersion > 5 && !g_SaveAero)
	{
		HMODULE hm = GetModuleHandle(L"dwmapi.dll");

		if (hm != NULL)
		{
			typedef HRESULT dwmGetFunc(HWND, uint, PVOID, uint);
			typedef HRESULT dwmSetFunc(HWND, uint, LPCVOID, uint);

			dwmGetFunc *dwmGet = (dwmGetFunc*)GetProcAddress(hm, "DwmGetWindowAttribute");
			dwmSetFunc *dwmSet = (dwmSetFunc*)GetProcAddress(hm, "DwmSetWindowAttribute");

			if (dwmGet != NULL && dwmSet != NULL)
			{
				//if (SUCCEEDED(dwmGet(g_ClientHandle, 2, &m_OldWindowAttributes, sizeof(uint))))
				{
					//LOG("dwm: %i %i\n", dwMajorVersion, m_OldWindowAttributes);

					//if (m_OldWindowAttributes)
					{
						uint dw_ = 1;
						dwmSet(g_ClientHandle, 2, &dw_, sizeof(uint));
					}
				}
			}
		}
	}

	g_FileManager.LoadFiles();

	ui->tw_Tabs->setCurrentIndex(0);
	ui->tw_Lists->setCurrentIndex(0);
	ui->tw_Agents->setCurrentIndex(0);
	ui->tw_Filters->setCurrentIndex(0);

	//Display init
	g_TabDisplay->Init();

	//Skills init
	g_TabSkills->Init();

	m_NoExecuteGUIEvents = false;

	g_TextCommandManager.UpdateCommands();

	connect(&m_CorpseTimer, SIGNAL(timeout()), this, SLOT(onCorpseTimer()));

	connect(&m_InformationTimer, SIGNAL(timeout()), this, SLOT(onInformationTimer()));
	onInformationTimer();
	m_InformationTimer.start(1000);

	g_TabScripts->Init();
	g_HotkeysForm->Init();

	UpdateAssistTitle("");

	g_TabListsTypes->LoadTypes();
	g_TabDisplay->LoadDisplayGroups();
	g_TabDisplay->LoadDisplayItems();
	g_TabListsFind->LoadFindList();
	g_TabListsIgnore->LoadIgnoreList();
	LoadTiles();
	g_TabFiltersSpeech->LoadSpeechFilter();
	g_TabFiltersReplaces->LoadTextReplaces();
	g_TabFiltersSound->LoadSoundFilter();
	g_TabAgentsShop->LoadShopList();
	g_OrionMapSettings.Load();
	LoadProfileMap();

	QDir dir(g_DllPath + "/Config/");

	QStringList dirList = dir.entryList(QDir::AllDirs);
	QStringList profiles;

	if (!dirList.empty())
	{
		bool profileAdded = false;

		for (const QString &str : dirList)
		{
			if (str.length() && str.at(0) != '.')
			{
				profiles << str;

				if (str.startsWith("0x"))
				{
					uint serial = COrionAssistant::TextToSerial(str);

					if (serial && m_ProfileMap.find(serial) == m_ProfileMap.end())
					{
						m_ProfileMap.insert(serial, str);
						profileAdded = true;
					}
				}
			}
		}

		if (profileAdded)
			SaveProfileMap();
	}

	g_TabMain->InitProfiles(profiles);

	show();
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::closeEvent(QCloseEvent *event)
{
	OAFUN_DEBUG("c28_f4");
	event->ignore();
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::keyPressEvent(QKeyEvent *event)
{
	OAFUN_DEBUG("c28_f5");
	if (event->isAutoRepeat())
		return;

	if (event->key() == Qt::Key_Delete)
	{
		QWidget *focused = QApplication::focusWidget();

		g_TabDisplay->OnDeleteKeyClick(focused);
		g_TabListsTypes->OnDeleteKeyClick(focused);
		g_TabListsObjects->OnDeleteKeyClick(focused);
		g_TabListsFind->OnDeleteKeyClick(focused);
		g_TabListsIgnore->OnDeleteKeyClick(focused);
		g_TabListsFriends->OnDeleteKeyClick(focused);
		g_TabListsEnemies->OnDeleteKeyClick(focused);
		g_TabAgentsDress->OnDeleteKeyClick(focused);
		g_TabAgentsParty->OnDeleteKeyClick(focused);
		g_TabAgentsShop->OnDeleteKeyClick(focused);
		g_TabFiltersSpeech->OnDeleteKeyClick(focused);
		g_TabFiltersReplaces->OnDeleteKeyClick(focused);
		g_TabFiltersSound->OnDeleteKeyClick(focused);
	}

	event->accept();
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::OnTrayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
	OAFUN_DEBUG("c28_f6");
	switch (reason)
	{
		case QSystemTrayIcon::Trigger:
		case QSystemTrayIcon::DoubleClick:
		{
			m_TrayIcon.hide();
			showNormal();
			break;
		}
		default:
			break;
	}
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::changeEvent(QEvent *event)
{
	OAFUN_DEBUG("c28_f7");
	event->accept();

	if (event->type() == QEvent::WindowStateChange && windowState() == Qt::WindowMinimized && g_TabMain->MinimizeToTray())
	{
		hide();
		m_TrayIcon.setToolTip(windowTitle());
		m_TrayIcon.show();
	}
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::EndWork()
{
	OAFUN_DEBUG("c28_f8");
	g_TabAgentsShop->SaveShopList();

	SaveCurrentProfile();

	RELEASE_POINTER(g_TextDialog);
	RELEASE_POINTER(g_ScriptEditorDialog);
	RELEASE_POINTER(g_ObjectInspectorForm);
	RELEASE_POINTER(g_ShipControlForm);
	RELEASE_POINTER(g_ShopProgressForm);
	RELEASE_POINTER(g_ShopListEditorForm);
	RELEASE_POINTER(g_HotkeysForm);
	RELEASE_POINTER(g_HouseControlForm);
	RELEASE_POINTER(g_AnimalAndVendorControlForm);
	RELEASE_POINTER(g_OrionMap);

	g_OrionMapSettings.Save();
}
//----------------------------------------------------------------------------------
int OrionAssistantForm::DistanceFromText(QString text)
{
	OAFUN_DEBUG("c28_f18");
	text = text.toLower().trimmed();

	if (text == "finddistance")
		return g_TabMain->SearchItemsDistance();
	else if (text == "usedistance")
		return g_TabMain->UseItemsDistance();
	else if (text == "opencorpsedistance")
		return g_TabMain->OpenCorpsesDistance();

	int result = text.toInt();

	if (result < 0 || !text.length())
		result = g_TabMain->SearchItemsDistance();

	return result;
}
//----------------------------------------------------------------------------------
int OrionAssistantForm::DelayFromText(QString text)
{
	OAFUN_DEBUG("c28_f19");
	text = text.toLower().trimmed();

	if (text == "moveitemdelay")
		return g_TabMain->MoveItemsDelay();
	else if (text == "waittargetdelay")
		return g_TabMain->WaitTargetsDelay();
	else if (text == "useitemdelay")
		return g_TabMain->UseItemsDelay();
	else if (text == "keepcorpsedelay")
		return g_TabMain->CorpseKeepDelay();

	return text.toInt();
}
//----------------------------------------------------------------------------------
bool OrionAssistantForm::SelectButtonColor(QColoredPushButton *button, QWidget *owner)
{
	OAFUN_DEBUG("c28_f89");
	QColorDialog dialog(owner ? owner : this);
	dialog.setCurrentColor(button->GetColor());
	bool result = dialog.exec();

	if (result)
		button->ChangeColor(dialog.selectedColor());

	return result;
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::LoadProfileBySerial(const uint &serial)
{
	LoadProfileMap();
	QMap<uint, QString>::iterator it = m_ProfileMap.find(serial);

	if (it != m_ProfileMap.end())
		g_TabMain->SetCurrentProfile(it.value());
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::LoadCurrentProfile()
{
	OAFUN_DEBUG("c28_f122");
	m_NoExecuteGUIEvents = true;

	QString profile = g_TabMain->ProfileName();

	if (!profile.length())
		return;

	QString path = g_DllPath + "/Config/" + profile;

	QFile file(path + "/Options.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;

		Q_UNUSED(version);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();
				}
				else if (reader.name() == "main")
				{
					g_TabMain->LoadConfig(attributes);
				}
				else if (reader.name() == "hotkeys")
				{
					g_TabScripts->LoadConfig(attributes);
				}
				else if (reader.name() == "display")
				{
					g_TabDisplay->LoadConfig(attributes);
				}
				else if (reader.name() == "scripts")
				{
					if (attributes.hasAttribute("scriptfile"))
						g_TabScripts->LoadScript(attributes.value("scriptfile").toString());
				}
			}

			reader.readNext();
		}

		file.close();
	}

	g_TabListsObjects->LoadObjects(path + "/Objects.xml");
	g_HotkeysForm->Load(path + "/Hotkeys.xml");
	g_TabAgentsDress->LoadDress(path + "/Dress.coa");
	g_TabAgentsParty->LoadParty(path + "/Party.xml");
	g_TabListsFriends->LoadFriends(path + "/Friends.xml");
	g_TabListsEnemies->LoadEnemies(path + "/Enemies.xml");
	g_TabMacros->Load(path + "/Macros.xml");

	m_NoExecuteGUIEvents = false;

	Qt::WindowFlags flags = windowFlags();

	if (g_TabMain->StayOnTop())
		setWindowFlags(flags | Qt::WindowStaysOnTopHint);
	else if (flags & Qt::WindowStaysOnTopHint)
		setWindowFlags(flags & ~Qt::WindowStaysOnTopHint);

	show();

	if (!g_TabMain->FastRotation() || !EnabledFeatureFastRotation())
		g_SetInt(VKI_FAST_ROTATION, 0);
	else
		g_SetInt(VKI_FAST_ROTATION, 1);

	if (!g_TabMain->IgnoreStaminaCheck() || !EnabledFeatureIgnoreStaminaCheck())
		g_SetInt(VKI_IGNORE_STAMINA_CHECK, 0);
	else
		g_SetInt(VKI_IGNORE_STAMINA_CHECK, 1);

	g_TabDisplay->RecompilleData();

	g_OrionAssistant.ClientPrint("Profile '" + profile + "' loaded!");
}
//----------------------------------------------------------------------------------
QString OrionAssistantForm::GetSaveConfigPath()
{
	OAFUN_DEBUG("c28_f127_1");
	QString profile = g_TabMain->ProfileName();

	if (!profile.length())
		return "";

	QString path = g_DllPath + "/Config/" + profile;

	QDir(g_DllPath).mkdir(g_DllPath + "/Config");
	QDir(g_DllPath).mkdir(path);

	return path;
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::SaveCurrentProfile()
{
	OAFUN_DEBUG("c28_f128");
	QString path = GetSaveConfigPath();

	if (!path.length())
		return;

	SaveMainOptions(path + "/Options.xml");
	g_TabListsObjects->SaveObjects(path + "/Objects.xml");
	g_HotkeysForm->Save(path + "/Hotkeys.xml");
	g_TabAgentsDress->SaveDress(path + "/Dress.coa");
	g_TabAgentsParty->SaveParty(path + "/Party.xml");
	g_TabListsFriends->SaveFriends(path + "/Friends.xml");
	g_TabListsEnemies->SaveEnemies(path + "/Enemies.xml");

	g_OrionAssistant.ClientPrint("Configuration saved.");
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::SaveMainOptions(const QString &path)
{
	QFile file(path);

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");

			g_TabMain->SaveConfig(writter);
			g_TabDisplay->SaveConfig(writter);
			g_TabScripts->SaveConfig(writter);

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::OnNewCorpse()
{
	OAFUN_DEBUG("c28_f155");
	if (!m_CorpseTimer.isActive())
		m_CorpseTimer.start(10);
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::onCorpseTimer()
{
	OAFUN_DEBUG("c28_f156");
	g_CorpseManager.CheckCorpses();

	if (g_CorpseManager.Empty())
		m_CorpseTimer.stop();
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::onInformationTimer()
{
	OAFUN_DEBUG("c28_f157");
	QString str = "";
	int minutes = g_StartTimer.elapsed() / 60000;

	if (minutes > 60)
	{
		int hours = minutes / 60;
		minutes %= 60;

		if (hours > 24)
		{
			int days = hours / 24;
			hours %= 24;

			str.sprintf("Running time: %id %ih %im", days, hours, minutes);
		}
		else
			str.sprintf("Running time: %ih %im", hours, minutes);
	}
	else
		str.sprintf("Running time: %im", minutes);

	int itemsCount = 0;

	if (g_World != nullptr)
	{
		itemsCount = g_World->m_Map.size();

		if (!g_TabMain->SaveFarObjects())
			g_World->RemoveRangedObjects();
	}

	str += " Items: " + QString::number(itemsCount);

	ui->le_Information->setText(str + ". Developed by Hotride.");
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::onAutostartTimer()
{
	OAFUN_DEBUG("c28_f200");
	g_TabScripts->RunScript("Autostart", g_ScriptEditorDialog->GetText(), 0, 0, QStringList());
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::RunAutostart()
{
	OAFUN_DEBUG("c28_f201");
	if (g_TabMain->Autostart())
		QTimer::singleShot(1000, this, SLOT(onAutostartTimer()));
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::SaveTiles()
{
	OAFUN_DEBUG("c28_f243");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/Tiles.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");

		const ushort mineTiles[9][2]=
		{
			{616,   618},
			{1339,  1363},
			{2272,  2282},
			{4963,  4973},
			{6001,  6012},
			{13121, 13369},
			{13446, 13455},
			{13484, 13492},
			{13625, 13628}
		};

		IFOR(i, 0, 9)
		{
			writter.writeStartElement("tile");

			writter.writeAttribute("name", "mine");
			writter.writeAttribute("land", "false");
			writter.writeAttribute("start", QString::number(mineTiles[i][0]));
			writter.writeAttribute("end", QString::number(mineTiles[i][1]));

			writter.writeEndElement(); //tile
		}

		const ushort treeTiles[46]=
		{
			3274, 3275, 3277, 3280, 3283,
			3286, 3288, 3290, 3293, 3296,
			3299, 3302, 3320, 3323, 3326,
			3329, 3393, 3394, 3395, 3396,
			3415, 3416, 3417, 3418, 3419,
			3438, 3439, 3440, 3441, 3442,
			3460, 3461, 3462, 3476, 3478,
			3480, 3482, 3484, 3492, 3496,
			3276, 3289, 3291, 3292, 3294,
			3295
		};

		IFOR(i, 0, 46)
		{
			writter.writeStartElement("tile");

			writter.writeAttribute("name", "tree");
			writter.writeAttribute("land", "false");
			writter.writeAttribute("start", QString::number(treeTiles[i]));

			writter.writeEndElement(); //tile
		}

		writter.writeStartElement("tile");

		writter.writeAttribute("name", "water");
		writter.writeAttribute("land", "false");
		writter.writeAttribute("start", QString::number(6038));
		writter.writeAttribute("end", QString::number(6066));

		writter.writeEndElement(); //tile

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::LoadTiles()
{
	OAFUN_DEBUG("c28_f244");

	QString path = g_DllPath + "/GlobalConfig/Tiles.xml";

	if (!QFile::exists(path))
		SaveTiles();

	QFile file(path);

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;

		Q_UNUSED(version);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();
				}
				else if (reader.name() == "tile")
				{
					if (attributes.hasAttribute("name"))
					{
						QString name = attributes.value("name").toString().trimmed().toLower();

						ushort start = 0;
						ushort end = 0;
						bool land = false;

						if (attributes.hasAttribute("land"))
							land = COrionAssistant::RawStringToBool(attributes.value("land").toString());

						if (attributes.hasAttribute("start"))
							start = COrionAssistant::TextToGraphic(attributes.value("start").toString());

						if (attributes.hasAttribute("end"))
							end = COrionAssistant::TextToGraphic(attributes.value("end").toString());

						if (name == "mine")
							g_FileManager.m_MineTiles.push_back(CSearchTileInfo(start, end, land));
						else if (name == "tree")
							g_FileManager.m_TreeTiles.push_back(CSearchTileInfo(start, end, land));
						else if (name == "water")
							g_FileManager.m_WaterTiles.push_back(CSearchTileInfo(start, end, land));
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::SaveProfileMap()
{
	OAFUN_DEBUG("c28_f243");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/Profiles.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		if (g_Connected && g_Player != nullptr)
			m_ProfileMap.insert(g_PlayerSerial, g_TabMain->ProfileName());

		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		writter.writeStartElement("data");

		for (QMap<uint, QString>::iterator i = m_ProfileMap.begin(); i != m_ProfileMap.end(); ++i)
		{
			writter.writeStartElement("profile");

			writter.writeAttribute("serial", COrionAssistant::SerialToText(i.key()));
			writter.writeAttribute("file", i.value());

			writter.writeEndElement(); //profile
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::LoadProfileMap()
{
	OAFUN_DEBUG("c28_f244");

	QString path = g_DllPath + "/GlobalConfig/Profiles.xml";

	m_ProfileMap.clear();

	if (!QFile::exists(path))
		return;

	QFile file(path);

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;

		Q_UNUSED(version);

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "profile")
				{
					if (attributes.hasAttribute("serial") && attributes.hasAttribute("file"))
					{
						uint serial = COrionAssistant::TextToSerial(attributes.value("serial").toString());
						QString profile = attributes.value("file").toString();

						m_ProfileMap.insert(serial, profile);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::AutoOpenDoor(const int &x, const int &y, const int &z)
{
	OAFUN_DEBUG("c28_f245");

	if (m_AutoOpenDoorTimer.isActive())
		m_AutoOpenDoorTimer.stop();

	m_DoorToOpen = 0;

	if (g_World != nullptr)
	{
		int minZ = z - 10;
		int maxZ = z + 10;

		QFOR(obj, g_World->m_Items, CGameObject*)
		{
			if (obj->GetNPC() || !IsDoor(((CGameItem*)obj)->GetTiledataFlags()) || obj->GetX() != x || obj->GetY() != y)
				continue;

			int objZ = obj->GetZ();

			if (objZ < minZ || objZ > maxZ)
				continue;

			m_DoorToOpen = obj->GetSerial();
			break;
		}
	}

	if (m_DoorToOpen)
		m_AutoOpenDoorTimer.start(20);
}
//----------------------------------------------------------------------------------
void OrionAssistantForm::onAutoOpenDoorTimer()
{
	OAFUN_DEBUG("c28_f246");

	if (m_DoorToOpen)
	{
		if (g_TabMain->DoubleClickForOpenDoors())
			g_OrionAssistant.DoubleClick(m_DoorToOpen);
		else
			g_OrionAssistant.DoubleClick(m_DoorToOpen);
	}

	m_DoorToOpen = 0;
}
//----------------------------------------------------------------------------------
