// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** AnimalAndVendorControlForm.cpp
**
** Copyright (C) November 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "animalandvendorcontrolform.h"
#include "ui_animalandvendorcontrolform.h"
#include "Packets.h"
#include "orionassistant.h"
//----------------------------------------------------------------------------------
AnimalAndVendorControlForm *g_AnimalAndVendorControlForm = nullptr;
//----------------------------------------------------------------------------------
AnimalAndVendorControlForm::AnimalAndVendorControlForm(QWidget *parent)
: QDialog(parent), ui(new Ui::AnimalAndVendorControlForm)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);

	setFixedSize(size());

	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	g_AnimalAndVendorControlForm = this;
}
//----------------------------------------------------------------------------------
AnimalAndVendorControlForm::~AnimalAndVendorControlForm()
{
	OAFUN_DEBUG("");
	delete ui;
	g_AnimalAndVendorControlForm = nullptr;
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Come_clicked()
{
	OAFUN_DEBUG("");
	Send("Come");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Go_clicked()
{
	OAFUN_DEBUG("");
	Send("Go");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Follow_clicked()
{
	OAFUN_DEBUG("");
	Send("Follow");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Stay_clicked()
{
	OAFUN_DEBUG("");
	Send("Stay");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Stop_clicked()
{
	OAFUN_DEBUG("");
	Send("Stop");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Guard_clicked()
{
	OAFUN_DEBUG("");
	Send("Guard");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Transfer_clicked()
{
	OAFUN_DEBUG("");
	Send("Transfer");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Release_clicked()
{
	OAFUN_DEBUG("");
	Send("Release");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Attack_clicked()
{
	OAFUN_DEBUG("");
	Send("Attack");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Drop_clicked()
{
	OAFUN_DEBUG("");
	Send("Drop");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Give_clicked()
{
	OAFUN_DEBUG("");
	Send("Give");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Kill_clicked()
{
	OAFUN_DEBUG("");
	Send("Kill");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Buy_clicked()
{
	OAFUN_DEBUG("");
	Send("Buy");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Bye_clicked()
{
	OAFUN_DEBUG("");
	Send("Bye");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Stock_clicked()
{
	OAFUN_DEBUG("");
	Send("Stock");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Sell_clicked()
{
	OAFUN_DEBUG("");
	Send("Sell");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Hire_clicked()
{
	OAFUN_DEBUG("");
	Send("Hire");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Price_clicked()
{
	OAFUN_DEBUG("");
	Send("Price");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_Status_clicked()
{
	OAFUN_DEBUG("");
	Send("Status");
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::Send(const QString &command)
{
	OAFUN_DEBUG("");
	QString text = ui->le_Name->text() + " " + command;

	CPacketUnicodeSpeechRequest(0, text.toStdWString()).SendClient();
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::SetName(const QString &name)
{
	OAFUN_DEBUG("");
	ui->le_Name->setText(name);
}
//----------------------------------------------------------------------------------
void AnimalAndVendorControlForm::on_pb_NameFromTarget_clicked()
{
	OAFUN_DEBUG("");
	g_TargetHandler = &COrionAssistant::HandleTargetAnimalAndVendorControlName;

	g_OrionAssistant.RequestTarget("Select a object for obtain name");
	BringWindowToTop(g_ClientHandle);
}
//----------------------------------------------------------------------------------
