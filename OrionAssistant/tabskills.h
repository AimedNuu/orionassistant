/***********************************************************************************
**
** TabSkills.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABSKILLS_H
#define TABSKILLS_H
//----------------------------------------------------------------------------------
#include <QWidget>
#include"../Managers/DataReader.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabSkills;
}
//----------------------------------------------------------------------------------
class TabSkills : public QWidget
{
	Q_OBJECT

private slots:
	void on_bt_Reset_SkillsDelta_clicked();

	void on_bt_CopySelectedToClipboard_clicked();

	void on_bt_CopyAllToClipboard_clicked();

private:
	Ui::TabSkills *ui;

	int GetSkillRow(const uchar &id);

public:
	explicit TabSkills(QWidget *parent = 0);
	~TabSkills();

	void Init();

	void SkillsListUpdated(CDataReader &reader);

	void UpdateSkill(const uchar &id);

	void UpdateStatsAndSkillsInfo();
};
//----------------------------------------------------------------------------------
extern TabSkills *g_TabSkills;
//----------------------------------------------------------------------------------
#endif // TABSKILLS_H
//----------------------------------------------------------------------------------
