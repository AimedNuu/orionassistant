/***********************************************************************************
**
** TabObjectInspector.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TABOBJECTINSPECTOR_H
#define TABOBJECTINSPECTOR_H
//----------------------------------------------------------------------------------
#include <QWidget>
#include "../GameObjects/GameObject.h"
#include <QTreeWidgetItem>
#include <QPaintEvent>
//----------------------------------------------------------------------------------
namespace Ui
{
	class TabObjectInspector;
}
//----------------------------------------------------------------------------------
class TabObjectInspector : public QWidget
{
	Q_OBJECT

	SETGET(uint, Serial, 0)
	SETGET(ushort, Graphic, 0)
	SETGET(ushort, Color, 0)
	SETGET(uint, Count, 0)

private slots:
	void on_tw_Info_itemClicked(QTreeWidgetItem *item, int column);

private:
	Ui::TabObjectInspector *ui;

	void UpdateImage(QTreeWidgetItem *item);

	UINT_LIST m_Pixels;
	QImage m_Image;

protected:
	virtual void paintEvent(QPaintEvent *event);

public:
	explicit TabObjectInspector(QWidget *parent = 0);
	~TabObjectInspector();

	void CheckUpdate(class CGameObject *obj);

	void Update(class CGameObject *obj);
};
//----------------------------------------------------------------------------------
#endif // TABOBJECTINSPECTOR_H
//----------------------------------------------------------------------------------
