/***********************************************************************************
**
** ShopListEditorForm.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SHOPLISTEDITORFORM_H
#define SHOPLISTEDITORFORM_H
//----------------------------------------------------------------------------------
#include <QMainWindow>
#include "../CommonItems/shopitem.h"
#include <QCloseEvent>
#include <QKeyEvent>
#include "../CommonItems/vendorshopitem.h"
//----------------------------------------------------------------------------------
namespace Ui
{
	class ShopListEditorForm;
}
//----------------------------------------------------------------------------------
class ShopListEditorForm : public QMainWindow
{
	Q_OBJECT

	SETGET(bool, IsBuyList, false)
	SETGET(bool, ShopListReplayed, false)
	SETGET(uint, VendorSerial, 0)

private:
	Ui::ShopListEditorForm *ui;

	bool m_Changed{ false };

	bool m_NoProcessSave{ false };

	void AddNewItem(const CShopItem &shopItem);

private slots:
	void on_lw_ShopList_clicked(const QModelIndex &index);

	void on_lw_VendorList_doubleClicked(const QModelIndex &index);

	void on_pb_CopyShopItem_clicked();

	void on_pb_ShopItemAdd_clicked();

	void slot_ShopItemSave();

	void on_pb_ShopItemRemove_clicked();

	void on_pb_SaveChanges_clicked();

	void on_pb_Exit_clicked();

protected:
	virtual void closeEvent(QCloseEvent *event);

	virtual void keyPressEvent(QKeyEvent *event);

public:
	explicit ShopListEditorForm(QWidget *parent = 0);
	~ShopListEditorForm();

	class CShopListItem *m_UsedList{ nullptr };

	void InitList(class CShopListItem *list);

	void UpdateVendorList();

	QVector<CVendorShopItem> m_VendorItems;
};
//----------------------------------------------------------------------------------
extern ShopListEditorForm *g_ShopListEditorForm;
//----------------------------------------------------------------------------------
#endif // SHOPLISTEDITORFORM_H
//----------------------------------------------------------------------------------
