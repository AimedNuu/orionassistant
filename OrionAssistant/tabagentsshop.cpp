// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TabAgentsShop.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tabagentsshop.h"
#include "ui_tabagentsshop.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
#include "orionassistant.h"
#include "orionassistantform.h"
#include "../GUI/shoplistitem.h"
#include "shopprogressform.h"
#include "shoplisteditorform.h"
#include "../GameObjects/GameWorld.h"
#include "../Managers/ClilocManager.h"
#include "../CommonItems/finditem.h"
#include "Packets.h"
//----------------------------------------------------------------------------------
TabAgentsShop *g_TabAgentsShop = nullptr;
//----------------------------------------------------------------------------------
TabAgentsShop::TabAgentsShop(QWidget *parent)
: QWidget(parent), ui(new Ui::TabAgentsShop)
{
	OAFUN_DEBUG("");
	ui->setupUi(this);
	g_TabAgentsShop = this;

	connect(ui->lw_ShopLists, SIGNAL(itemDropped()), this, SLOT(SaveShopList()));
}
//----------------------------------------------------------------------------------
TabAgentsShop::~TabAgentsShop()
{
	OAFUN_DEBUG("");
	delete ui;
	g_TabAgentsShop = nullptr;
}
//----------------------------------------------------------------------------------
void TabAgentsShop::OnDeleteKeyClick(QWidget *widget)
{
	OAFUN_DEBUG("");
	if (widget == ui->lw_ShopLists)
		on_pb_ShopListRemove_clicked();
}
//----------------------------------------------------------------------------------
void TabAgentsShop::LoadShopList()
{
	OAFUN_DEBUG("c28_f262");
	ui->lw_ShopLists->clear();

	QFile file(g_DllPath + "/GlobalConfig/ShopList.xml");

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader reader(&file);

		int version = 0;

		Q_UNUSED(version);

		CShopListItem *listItem = nullptr;

		while (!reader.atEnd() && !reader.hasError())
		{
			if (reader.isStartElement())
			{
				QXmlStreamAttributes attributes = reader.attributes();

				if (reader.name() == "data")
				{
					if (attributes.hasAttribute("version"))
						version = attributes.value("version").toInt();

					if (attributes.hasAttribute("show_shopping_progress_bar"))
						ui->cb_ShowShoppingProgressBar->setChecked(COrionAssistant::RawStringToBool(attributes.value("show_shopping_progress_bar").toString()));

					if (attributes.hasAttribute("max_shop_items"))
						ui->sb_MaxShopItems->setValue(attributes.value("max_shop_items").toInt());

					if (attributes.hasAttribute("max_sell_price"))
						ui->sb_MaxSellPrice->setValue(attributes.value("max_sell_price").toInt());

					if (attributes.hasAttribute("shop_delay"))
						ui->sb_ShopDelay->setValue(attributes.value("shop_delay").toInt());

					if (attributes.hasAttribute("shop_timeout"))
						ui->sb_ShopTimeout->setValue(attributes.value("shop_timeout").toInt());
				}
				else if (reader.name() == "shoplist")
				{
					if (attributes.hasAttribute("name"))
						listItem = new CShopListItem(attributes.value("name").toString(), ui->lw_ShopLists);
				}
				else if (reader.name() == "shopitem" && listItem != nullptr)
				{
					if (attributes.hasAttribute("graphic") && attributes.hasAttribute("color") && attributes.hasAttribute("name") && attributes.hasAttribute("count") && attributes.hasAttribute("price") && attributes.hasAttribute("comment"))
					{
						listItem->m_Items.push_back
						(
							CShopItem
							(
								attributes.value("graphic").toString(),
								attributes.value("color").toString(),
								attributes.value("name").toString(),
								attributes.value("count").toInt(),
								attributes.value("price").toInt(),
								attributes.value("comment").toString()
							)
						);
					}
				}
			}

			reader.readNext();
		}

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabAgentsShop::SaveShopList()
{
	OAFUN_DEBUG("c28_f261");
	QDir(g_DllPath).mkdir("GlobalConfig");

	QFile file(g_DllPath + "/GlobalConfig/ShopList.xml");

	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QXmlStreamWriter writter(&file);

		writter.setAutoFormatting(true);

		writter.writeStartDocument();

		int count = ui->lw_ShopLists->count();

		writter.writeStartElement("data");
		writter.writeAttribute("version", "0");

		writter.writeAttribute("show_shopping_progress_bar", COrionAssistant::BoolToText(ui->cb_ShowShoppingProgressBar->isChecked()));
		writter.writeAttribute("max_shop_items", QString::number(ui->sb_MaxShopItems->value()));
		writter.writeAttribute("max_sell_price", QString::number(ui->sb_MaxSellPrice->value()));
		writter.writeAttribute("shop_delay", QString::number(ui->sb_ShopDelay->value()));
		writter.writeAttribute("shop_timeout", QString::number(ui->sb_ShopTimeout->value()));

		IFOR(i, 0, count)
		{
			CShopListItem *item = (CShopListItem*)ui->lw_ShopLists->item(i);

			if (item != nullptr)
			{
				writter.writeStartElement("shoplist");

				writter.writeAttribute("name", item->text());

				QVector<CShopItem> &shopItems = item->m_Items;

				if (!shopItems.empty())
				{
					for (const CShopItem & shopItem : shopItems)
					{
						writter.writeStartElement("shopitem");

							writter.writeAttribute("graphic", shopItem.GetGraphic());
							writter.writeAttribute("color", shopItem.GetColor());
							writter.writeAttribute("name", shopItem.GetName());
							writter.writeAttribute("count", QString::number(shopItem.GetCount()));
							writter.writeAttribute("price", QString::number(shopItem.GetPrice()));
							writter.writeAttribute("comment", shopItem.GetComment());

						writter.writeEndElement(); //shopitem
					}
				}

				writter.writeEndElement(); //shoplist
			}
		}

		writter.writeEndElement(); //data

		writter.writeEndDocument();

		file.close();
	}
}
//----------------------------------------------------------------------------------
void TabAgentsShop::on_lw_ShopLists_clicked(const QModelIndex &index)
{
	OAFUN_DEBUG("");
	if (g_OrionAssistantForm->GetNoExecuteGUIEvents())
		return;

	CShopListItem *item = (CShopListItem*)ui->lw_ShopLists->item(index.row());

	if (item != nullptr)
	{
		ui->le_ShopListName->setText(item->text());
	}
}
//----------------------------------------------------------------------------------
void TabAgentsShop::on_pb_ShopListCreate_clicked()
{
	OAFUN_DEBUG("");
	QString text = ui->le_ShopListName->text().toLower();

	if (!text.length())
	{
		QMessageBox::critical(this, "Text is empty", "Enter the shop list name!");
		return;
	}

	IFOR(i, 0, ui->lw_ShopLists->count())
	{
		CShopListItem *item = (CShopListItem*)ui->lw_ShopLists->item(i);

		if (item != nullptr && item->text().toLower() == text)
		{
			QMessageBox::critical(this, "Text is already exists", "Shop list name is already exists!");
			return;
		}
	}

	new CShopListItem(ui->le_ShopListName->text(), ui->lw_ShopLists);

	ui->lw_ShopLists->setCurrentRow(ui->lw_ShopLists->count() - 1);
	SaveShopList();
}
//----------------------------------------------------------------------------------
void TabAgentsShop::on_pb_ShopListSave_clicked()
{
	OAFUN_DEBUG("");
	QString text = ui->le_ShopListName->text().toLower();

	if (!text.length())
	{
		QMessageBox::critical(this, "Text is empty", "Enter the shop list name!");
		return;
	}

	CShopListItem *selected = (CShopListItem*)ui->lw_ShopLists->item(ui->lw_ShopLists->currentRow());

	if (selected == nullptr)
	{
		QMessageBox::critical(this, "Text is not selected", "Name is not selected!");
		return;
	}

	IFOR(i, 0, ui->lw_ShopLists->count())
	{
		CShopListItem *item = (CShopListItem*)ui->lw_ShopLists->item(i);

		if (item != nullptr && item->text().toLower() == text)
		{
			if (item != selected)
			{
				QMessageBox::critical(this, "Text is already exists", "Shop list name is already exists (not this item)!");
				return;
			}

			break;
		}
	}

	selected->setText(ui->le_ShopListName->text());
	SaveShopList();
}
//----------------------------------------------------------------------------------
void TabAgentsShop::on_pb_ShopListRemove_clicked()
{
	OAFUN_DEBUG("");
	QListWidgetItem *item = ui->lw_ShopLists->item(ui->lw_ShopLists->currentRow());

	if (item != nullptr)
	{
		item = ui->lw_ShopLists->takeItem(ui->lw_ShopLists->row(item));

		if (item != nullptr)
		{
			if (g_ShopListEditorForm != nullptr && g_ShopListEditorForm->m_UsedList == item)
				g_ShopListEditorForm->InitList(nullptr);

			delete item;
			SaveShopList();
		}
	}
}
//----------------------------------------------------------------------------------
bool TabAgentsShop::IsActiveShopping()
{
	OAFUN_DEBUG("");
	return ((m_State != SLWS_NORNAL && m_WaitTimeout >= GetTickCount()) || g_ShopProgressForm->IsActiveShpooing());
}
//----------------------------------------------------------------------------------
void TabAgentsShop::SendShopRequest(const SHOP_LIST_WAIT_STATE &state, const QString &listName, const wstring &text, const int &shopDelay)
{
	OAFUN_DEBUG("");
	if (g_World == nullptr || !EnabledFeatureShopAgent())
		return;

	if (m_State != SLWS_NORNAL && m_WaitTimeout >= GetTickCount())
	{
		g_OrionAssistant.ClientPrintDebugLevel1("Preveous waiting for shopping cancelled.");
	}

	m_WaitShopDelay = shopDelay;
	m_WaitShopListName = listName;
	m_State = state;
	m_WaitTimeout = GetTickCount() + (ui->sb_ShopTimeout->value() * 1000);
	CPacketUnicodeSpeechRequest(0, text).SendClient();
}
//----------------------------------------------------------------------------------
void TabAgentsShop::on_pb_BuySelectedList_clicked()
{
	OAFUN_DEBUG("");
	CShopListItem *shopList = (CShopListItem*)ui->lw_ShopLists->item(ui->lw_ShopLists->currentRow());

	if (shopList != nullptr)
	{
		QString text = ui->le_VendorName->text();

		if (text.length())
			text += " ";

		text += "buy";

		SendShopRequest(SLWS_BUY, shopList->text(), text.toStdWString(), 0);
	}
}
//----------------------------------------------------------------------------------
void TabAgentsShop::on_pb_SellSelectedList_clicked()
{
	OAFUN_DEBUG("");
	CShopListItem *shopList = (CShopListItem*)ui->lw_ShopLists->item(ui->lw_ShopLists->currentRow());

	if (shopList != nullptr)
	{
		QString text = ui->le_VendorName->text();

		if (text.length())
			text += " ";

		text += "sell";

		SendShopRequest(SLWS_SELL, shopList->text(), text.toStdWString(), 0);
	}
}
//----------------------------------------------------------------------------------
void TabAgentsShop::on_lw_ShopLists_doubleClicked(const QModelIndex &index)
{
	OAFUN_DEBUG("");
	Q_UNUSED(index);
	on_pb_EditShopListItems_clicked();
}
//----------------------------------------------------------------------------------
void TabAgentsShop::on_pb_EditShopListItems_clicked()
{
	OAFUN_DEBUG("");
	CShopListItem *item = (CShopListItem*)ui->lw_ShopLists->item(ui->lw_ShopLists->currentRow());

	if (g_ShopListEditorForm != nullptr)
		g_ShopListEditorForm->InitList(item);
}
//----------------------------------------------------------------------------------
bool TabAgentsShop::ProcessShopList()
{
	OAFUN_DEBUG("");
	QVector<CVendorShopItem> &vendorItems = g_ShopListEditorForm->m_VendorItems;

	if (vendorItems.empty())
		return true;

	CShopListItem *shopList = nullptr;
	QString seekListName = m_WaitShopListName.toLower();

	IFOR(i, 0, ui->lw_ShopLists->count())
	{
		shopList = (CShopListItem*)ui->lw_ShopLists->item(i);

		if (shopList != nullptr && shopList->text().toLower() == seekListName)
			break;
	}

	if (shopList == nullptr)
		return true;

	QVector<CShopItem> &shopItems = shopList->m_Items;

	if (shopItems.empty())
		return true;

	bool isBuyList = g_ShopListEditorForm->GetIsBuyList();
	int maxStacks = ui->sb_MaxShopItems->value();
	int maxSellPrice = ui->sb_MaxSellPrice->value();

	if (isBuyList)
		maxSellPrice = 0;

	int totalPrice = 0;

	QVector< std::pair<uint, int> > result;

	for (const CShopItem &shopItem : shopItems)
	{
		CFindItem findItem(shopItem.GetGraphic(), shopItem.GetColor());
		int checkPrice = shopItem.GetPrice();
		QString checkName = shopItem.GetName().toLower();
		bool exit = false;

		for (const CVendorShopItem &vendorItem : vendorItems)
		{
			int price = vendorItem.GetPrice();

			if (checkPrice)
			{
				if (isBuyList)
				{
					if (checkPrice < price)
						continue;
				}
				else if (checkPrice > price)
					continue;
			}

			if (findItem.Found(vendorItem.GetGraphic(), vendorItem.GetColor()) &&
					(!checkName.length() || checkName == vendorItem.GetName().toLower()))
			{
				int count = shopItem.GetCount();

				if (!count || count > vendorItem.GetCount())
					count = vendorItem.GetCount();

				int currentPrice = count * price;

				if (maxSellPrice)
				{
					if (totalPrice + currentPrice >= maxSellPrice)
					{
						count = (maxSellPrice - totalPrice) / price;

						exit = (count != 0);
					}
				}

				if (count)
				{
					totalPrice += currentPrice;
					result.push_back( std::pair<uint, int>(vendorItem.GetSerial(), count) );

					if (maxStacks && result.size() >= maxStacks)
						exit = true;

					if (exit)
						break;
				}
			}
		}

		if (exit)
			break;
	}

	m_State = SLWS_NORNAL;

	if (result.empty())
	{
		if (isBuyList)
			g_OrionAssistant.ClientPrintDebugLevel1("No items to bought. Waiting cancelled.");
		else
			g_OrionAssistant.ClientPrintDebugLevel1("No items to sold. Waiting cancelled.");

		return true;
	}

	CPacket *packet = nullptr;

	if (isBuyList)
	{
		g_OrionAssistant.ClientPrintDebugLevel1("Waiting for buy items...");
		packet = new CPacketBuyRequest(g_ShopListEditorForm->GetVendorSerial(), result);
	}
	else
	{
		g_OrionAssistant.ClientPrintDebugLevel1("Waiting for sell items...");
		packet = new CPacketSellRequest(g_ShopListEditorForm->GetVendorSerial(), result);
	}

	m_State = SLWS_NORNAL;
	int delay = m_WaitShopDelay;

	if (!delay)
		delay = ui->sb_ShopDelay->value() * result.size();

	g_ShopProgressForm->Start(delay, packet, ui->cb_ShowShoppingProgressBar->isChecked());

	return false;
}
//----------------------------------------------------------------------------------
bool TabAgentsShop::OnPacketReceivedOpenContaier(const uint &serial)
{
	OAFUN_DEBUG("");
	if (g_World == nullptr || !EnabledFeatureShopAgent())
		return true;

	g_ShopListEditorForm->SetShopListReplayed(false);
	g_ShopListEditorForm->SetIsBuyList(true);
	QVector<CVendorShopItem> &vendorItems = g_ShopListEditorForm->m_VendorItems;
	vendorItems.clear();

	bool result = true;

	if (serial == g_ShopListEditorForm->GetVendorSerial())
	{
		CGameCharacter *vendor = g_World->FindWorldCharacter(serial);

		if (vendor != nullptr)
		{
			IFOR(i, 0, 2)
			{
				CGameItem *container = vendor->FindLayer(i + OL_BUY_RESTOCK);

				if (container == nullptr)
					continue;

				QFOR(obj, container->m_Items, CGameItem*)
				{
					CVendorShopItem item;
					item.SetSerial(obj->GetSerial());
					item.SetGraphic(obj->GetGraphic());
					item.SetColor(obj->GetColor());
					item.SetCount(obj->GetCount());
					item.SetPrice(obj->GetPrice());
					item.SetName(obj->GetName());
					item.SetNameFromCliloc(obj->GetNameFromCliloc());

					vendorItems.push_back(item);
				}
			}

			if (ui->cb_AutoBuySelectedList->isChecked() || (m_State == SLWS_BUY && m_WaitTimeout >= GetTickCount()))
				result = ProcessShopList();
		}
		else
		{
			LOG("Error!!! Buy vendor is not found!!!\n");
		}
	}
	else
	{
		LOG("Error!!! Buy vendor serial (0x%08X) is incorrent! Wanted serial: 0x%08X\n", serial, g_ShopListEditorForm->GetVendorSerial());
	}

	g_ShopListEditorForm->UpdateVendorList();

	return result;
}
//----------------------------------------------------------------------------------
bool TabAgentsShop::OnPacketReceivedBuyList(CDataReader &reader)
{
	OAFUN_DEBUG("");
	if (g_World == nullptr || !EnabledFeatureShopAgent())
		return true;

	uint serial = reader.ReadUInt32BE();

	CGameItem *container = g_World->FindWorldItem(serial);

	if (container == nullptr)
	{
		LOG("Error!!! Buy container is not found!!!\n");
		return true;
	}

	uint vendorSerial = container->GetContainer();

	g_ShopListEditorForm->SetVendorSerial(vendorSerial);

	CGameCharacter *vendor = g_World->FindWorldCharacter(vendorSerial);

	if (vendor == nullptr)
	{
		LOG("Error!!! Buy vendor is not found!!!\n");
		return true;
	}

	if (container->GetLayer() == OL_BUY_RESTOCK || container->GetLayer() == OL_BUY)
	{
		uchar count = reader.ReadUInt8();

		CGameItem *item = (CGameItem*)container->m_Items;
		//oldp spams this packet twice in a row on NPC verndors. NULL check is needed t
		if (item == nullptr)
			return true;

		bool reverse = (item->GetX() > 1);

		if (reverse)
			item = (CGameItem*)container->Last();

		IFOR(i, 0, count)
		{
			if (item == nullptr)
			{
				LOG("Error!!! Buy item is not found!!!\n");
				break;
			}

			item->SetPrice(reader.ReadUInt32BE());

			uchar nameLen = reader.ReadUInt8();
			string name = reader.ReadString(nameLen);

			bool intParsed = false;
			int clilocNum = QString(name.c_str()).toInt(&intParsed);

			if (intParsed)
				item->SetName(g_ClilocManager.Get(clilocNum, true));
			else
				item->SetName(name.c_str());

			item->SetNameFromCliloc(false);

			if (reverse)
				item = (CGameItem*)item->m_Prev;
			else
				item = (CGameItem*)item->m_Next;
		}
	}
	else
		LOG("Unknown layer for buy container!!!\n");

	return true;
}
//----------------------------------------------------------------------------------
bool TabAgentsShop::OnPacketSendBuyReply()
{
	OAFUN_DEBUG("");
	if (EnabledFeatureShopAgent())
		g_ShopListEditorForm->SetShopListReplayed(true);

	return true;
}
//----------------------------------------------------------------------------------
bool TabAgentsShop::OnPacketReceivedBuyReply(CDataReader &reader)
{
	OAFUN_DEBUG("");
	Q_UNUSED(reader);
	//uint serial = reader.ReadUInt32BE();
	//uchar flag = reader.ReadUInt8();

	return true;
}
//----------------------------------------------------------------------------------
bool TabAgentsShop::OnPacketReceivedSellList(CDataReader &reader)
{
	OAFUN_DEBUG("");
	if (g_World == nullptr || !EnabledFeatureShopAgent())
		return true;

	g_ShopListEditorForm->SetShopListReplayed(false);
	g_ShopListEditorForm->SetIsBuyList(false);
	QVector<CVendorShopItem> &vendorItems = g_ShopListEditorForm->m_VendorItems;
	vendorItems.clear();

	uint serial = reader.ReadUInt32BE();

	CGameCharacter *vendor = g_World->FindWorldCharacter(serial);

	g_ShopListEditorForm->SetVendorSerial(serial);

	bool result = true;

	if (vendor != nullptr)
	{
		ushort itemsCount = reader.ReadUInt16BE();

		if (!itemsCount)
		{
			LOG("Warning!!! Sell list is empty!\n");
		}

		IFOR(i, 0, itemsCount)
		{
			uint itemSerial = reader.ReadUInt32BE();
			ushort graphic = reader.ReadUInt16BE();
			ushort color = reader.ReadUInt16BE();
			ushort count = reader.ReadUInt16BE();
			ushort price = reader.ReadUInt16BE();
			int nameLen = reader.ReadInt16BE();
			string name = reader.ReadString(nameLen);

			bool intParsed = false;
			int clilocNum = QString(name.c_str()).toInt(&intParsed);

			if (intParsed)
				name = g_ClilocManager.Get(clilocNum, true).toStdString();

			CVendorShopItem item;
			item.SetSerial(itemSerial);
			item.SetGraphic(graphic);
			item.SetColor(color);
			item.SetCount(count);
			item.SetPrice(price);
			item.SetName(name.c_str());
			item.SetNameFromCliloc(intParsed);

			vendorItems.push_back(item);
		}

		if (ui->cb_AutoSellSelectedList->isChecked() || (m_State == SLWS_SELL && m_WaitTimeout >= GetTickCount()))
			result = ProcessShopList();
	}
	else
	{
		LOG("Error!!! Sell vendor is not found!!!\n");
	}

	g_ShopListEditorForm->UpdateVendorList();

	return result;
}
//----------------------------------------------------------------------------------
bool TabAgentsShop::OnPacketSendSellReply()
{
	OAFUN_DEBUG("");
	if (EnabledFeatureShopAgent())
		g_ShopListEditorForm->SetShopListReplayed(true);

	return true;
}
//----------------------------------------------------------------------------------
