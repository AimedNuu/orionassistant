/***********************************************************************************
**
** GumpHook.h
**
** Copyright (C) June 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef GUMPHOOK_H
#define GUMPHOOK_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CGumpHookCheck
{
	SETGET(int, Index, 0)
	SETGET(bool, State, false)

public:
	CGumpHookCheck(const int &index, const bool &state) : m_Index(index), m_State(state) {}
	~CGumpHookCheck() {}
};
//----------------------------------------------------------------------------------
class CGumpHookEntry
{
	SETGET(int, Index, 0)
	SETGET(QString, Text, "")

public:
	CGumpHookEntry(const int &index, const QString &text) : m_Index(index), m_Text(text) {}
	~CGumpHookEntry() {}
};
//----------------------------------------------------------------------------------
class CGumpHook
{
	SETGET(int, Index, 0)

public:
	CGumpHook(CGumpHook *original);

	~CGumpHook() {}

	QList<CGumpHookEntry> m_Entries;
	QList<CGumpHookCheck> m_Checks;
};
//----------------------------------------------------------------------------------
#endif // GUMPHOOK_H
//----------------------------------------------------------------------------------
