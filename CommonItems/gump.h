/***********************************************************************************
**
** Gump.h
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef GUMP_H
#define GUMP_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "gumpitem.h"
//----------------------------------------------------------------------------------
class CGump
{
	SETGET(uint, Serial, 0)
	SETGET(uint, ID, 0)

	SETGET(int, X, 0)
	SETGET(int, Y, 0)

	SETGET(bool, Replayed, false)
	SETGET(uint, ReplyID, 0)

	SETGET(bool, NoMove, false)
	SETGET(bool, NoClose, false)
	SETGET(bool, NoDispose, false)

public:
	CGump();
	CGump(const uint &serial, const uint &id, const int &x, const int &y);
	virtual ~CGump();

	GUMP_LIST m_Items;
	QStringList m_Text;
};
//----------------------------------------------------------------------------------
#endif // GUMP_H
//----------------------------------------------------------------------------------
