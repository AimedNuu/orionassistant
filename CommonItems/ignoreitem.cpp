// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** IgnoreItem.cpp
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "ignoreitem.h"
#include "../Managers/TextParser.h"
//----------------------------------------------------------------------------------
CIgnoreItem::CIgnoreItem()
: CFindItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CIgnoreItem::CIgnoreItem(const QString &serial, const QString &graphic, const QString &color)
: CFindItem(graphic, color), m_Serial(serial)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CIgnoreItem::~CIgnoreItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
bool CIgnoreItem::Found(const uint &serial, const ushort &graphic, const ushort &color) const
{
	OAFUN_DEBUG("c6_f1");
	uint currentSerial = 0;
	uint flagsSerial = ParseToken(m_Serial, currentSerial, false);

	if (currentSerial && TestValues(serial, currentSerial, flagsSerial))
		return true;

	return CFindItem::Found(graphic, color);
}
//----------------------------------------------------------------------------------
