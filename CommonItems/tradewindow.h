/***********************************************************************************
**
** TradeWindow.h
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef TRADEWINDOW_H
#define TRADEWINDOW_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CTradeWindow
{
	SETGET(uint, Serial, 0)
	SETGET(uint, ID1, 0)
	SETGET(uint, ID2, 0)
	SETGET(QString, Name, "")
	SETGET(bool, Check1, false)
	SETGET(bool, Check2, false)

public:
	CTradeWindow();
	CTradeWindow(const uint &serial, const uint &id1, const uint &id2, const QString &name);
	virtual ~CTradeWindow();
};
//----------------------------------------------------------------------------------
#endif // TRADEWINDOW_H
//----------------------------------------------------------------------------------
