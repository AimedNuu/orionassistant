/***********************************************************************************
**
** FindItem.h
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef FINDITEM_H
#define FINDITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
enum FIND_TOKEN_STATE
{
	FTS_NORMAL		= 0x00,
	FTS_NOT_EQUAL	= 0x01,
	FTS_LOWER		= 0x02,
	FTS_HIGHTER		= 0x04
};
//----------------------------------------------------------------------------------
class CFindItem
{
	SETGETE(QString, Graphic, "", OnChangeGraphic)
	SETGETE(QString, Color, "", OnChangeColor)
	SETGET(QString, Comment, "")

protected:
	QVector<ushort> m_ListGraphic;
	QVector<ushort> m_ListGraphicFlags;
	QVector<ushort> m_IgnoreGraphics;

	QVector<ushort> m_ListColor;
	QVector<ushort> m_ListColorFlags;
	QVector<ushort> m_IgnoreColors;

	uint ParseToken(QString str, uint &value, const bool &isGraphic) const;

	bool TestValues(const uint &original, const uint &test, const uint &flags) const;

public:
	CFindItem();
	CFindItem(const QString &graphic, const QString &color);
	virtual ~CFindItem();

	bool Found(const ushort &graphic, const ushort &color) const;
};
//----------------------------------------------------------------------------------
#endif // FINDITEM_H
//----------------------------------------------------------------------------------
