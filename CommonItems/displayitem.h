/***********************************************************************************
**
** DisplayItem.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef DISPLAYITEM_H
#define DISPLAYITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
enum DISPLAY_ITEM_TYPE
{
	DIT_NONE = 0,
	DIT_NAME,
	DIT_SERVER,
	DIT_STATUSBAR_SMALL,
	DIT_STATUSBAR_MIDDLE,
	DIT_STATUSBAR_LARGE,
	DIT_HITS,
	DIT_MAX_HITS,
	DIT_HITS_MAX_HITS,
	DIT_MANA,
	DIT_MAX_MANA,
	DIT_MANA_MAX_MANA,
	DIT_STAM,
	DIT_MAX_STAM,
	DIT_STAM_MAX_STAM,
	DIT_STR,
	DIT_INT,
	DIT_DEX,
	DIT_WEIGHT,
	DIT_MAX_WEIGHT,
	DIT_WEIGHT_MAX_WEIGHT,
	DIT_ARMOR,
	DIT_GOLD,
	DIT_X,
	DIT_Y,
	DIT_Z,
	DIT_OBJECT = 1000
};
//----------------------------------------------------------------------------------
class CDisplayInfo
{
public:
	CDisplayInfo() {}

	CDisplayInfo(const QString &name, const QString &command, const DISPLAY_ITEM_TYPE &type)
	: Name(name), Command(command), Type(type)
	{}

	~CDisplayInfo() {}

	QString Name{ "" };
	QString Command{ "" };
	DISPLAY_ITEM_TYPE Type{ DIT_NONE };
};
//----------------------------------------------------------------------------------
class CDisplayItem
{
	SETGET(DISPLAY_ITEM_TYPE, Type, DIT_NONE)
	SETGET(QString, Text, "")
	SETGET(ushort, Graphic, 0)
	SETGET(ushort, Hue, 0)
	SETGET(uint, MinColor, 0)
	SETGET(uint, MidColor, 0)
	SETGET(uint, Color, 0)
	SETGET(bool, Background, false)
	SETGET(uint, BackgroundMinColor, 0x007F0000)
	SETGET(uint, BackgroundMidColor, 0x00FFFF00)
	SETGET(uint, BackgroundColor, 0)
	SETGET(int, MinValue, 0)
	SETGET(int, MidValue, 0)
	SETGET(bool, Percents, false)

public:
	CDisplayItem() {}
	CDisplayItem(const DISPLAY_ITEM_TYPE &type, const QString &text, const ushort &graphic, const ushort &hue, const uint &minColor, const uint &midColor,
				 const uint &color, const bool &background, const uint &backgroundMinColor, const uint &backgroundMidColor,
				 const uint &backgroundColor, const int &minValue, const int &midValue, const bool &percents);
	CDisplayItem(const CDisplayItem &obj);
	~CDisplayItem() {}
};
//----------------------------------------------------------------------------------
#endif // DISPLAYITEM_H
//----------------------------------------------------------------------------------
