// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** GumpItem.cpp
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "gumpitem.h"
//----------------------------------------------------------------------------------
CGumpItem::CGumpItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CGumpItem::CGumpItem(const GUMP_ITEM_TYPE &type, const QString &command)
: m_Type(type), m_Command(command)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CGumpItem::~CGumpItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
void CGumpItem::RestoreCommand(const QStringList &list)
{
	OAFUN_DEBUG("");
	m_Command = "";

	if (!list.empty())
	{
		for (const QString &str : list)
		{
			if (m_Command.length())
				m_Command += " ";

			m_Command += str;
		}
	}
}
//----------------------------------------------------------------------------------
