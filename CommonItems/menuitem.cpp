// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** MenuItem.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "menuitem.h"
//----------------------------------------------------------------------------------
CMenuItem::CMenuItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CMenuItem::CMenuItem(const uint &id, const ushort &graphic, const ushort &color, const QString &name)
: m_ID(id), m_Graphic(graphic), m_Color(color), m_Name(name)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CMenuItem::~CMenuItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
