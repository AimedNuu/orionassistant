/***********************************************************************************
**
** Menu.h
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef MENU_H
#define MENU_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "menuitem.h"
//----------------------------------------------------------------------------------
class CMenu
{
	SETGET(uint, Serial, 0)
	SETGET(ushort, ID, 0)
	SETGET(bool, GrayMenu, false)
	SETGET(QString, Name, "")

	SETGET(bool, Replayed, false)
	SETGET(uint, ReplyID, 0)

public:
	CMenu();
	CMenu(const uint &serial, const ushort &id, const QString &name, const bool &grayMenu);
	virtual ~CMenu();

	MENU_LIST m_Items;
};
//----------------------------------------------------------------------------------
#endif // MENU_H
//----------------------------------------------------------------------------------
