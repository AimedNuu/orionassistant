/***********************************************************************************
**
** IgnoreItem.h
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef IGNOREITEM_H
#define IGNOREITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include "finditem.h"
//----------------------------------------------------------------------------------
class CIgnoreItem : public CFindItem
{
	SETGET(QString, Serial, "")

public:
	CIgnoreItem();
	CIgnoreItem(const QString &serial, const QString &graphic, const QString &color);
	virtual ~CIgnoreItem();

	bool Found(const uint &serial, const ushort &graphic, const ushort &color) const;
};
//----------------------------------------------------------------------------------
#endif // IGNOREITEM_H
//----------------------------------------------------------------------------------
