// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** JournalMessage.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "journalmessage.h"
//----------------------------------------------------------------------------------
CJournalMessage::CJournalMessage()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CJournalMessage::CJournalMessage(const CJournalMessage &msg)
: m_Serial(msg.GetSerial()), m_Timer(msg.GetTimer()), m_Color(msg.GetColor()),
m_Text(msg.GetText()), m_Flags(msg.GetFlags()), m_FindTextID(msg.GetFindTextID())
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CJournalMessage::CJournalMessage(const uint &serial, const ushort &color, const QString &text)
: m_Serial(serial), m_Timer(GetTickCount()), m_Color(color), m_Text(text),
m_Flags(0)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CJournalMessage::~CJournalMessage()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
