// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** BuffItem.cpp
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "buffitem.h"
//----------------------------------------------------------------------------------
CBuffItem::CBuffItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CBuffItem::CBuffItem(const ushort &graphic, const uint &timer, const QString &name)
: m_Graphic(graphic), m_Timer(timer), m_Name(name)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CBuffItem::~CBuffItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
