/***********************************************************************************
**
** ShopItem.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SHOPITEM_H
#define SHOPITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CShopItem
{
	SETGET(QString, Graphic, "")
	SETGET(QString, Color, "")
	SETGET(QString, Name, "")
	SETGET(int, Count, 0)
	SETGET(uint, Price, 0)
	SETGET(QString, Comment, "")

public:
	CShopItem() {}
	CShopItem(const QString &graphic, const QString &color, const QString &name, const int &count, const uint &price, const QString &comment);
};
//----------------------------------------------------------------------------------
#endif // SHOPITEM_H
//----------------------------------------------------------------------------------
