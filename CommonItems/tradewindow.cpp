// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** TradeWindow.cpp
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "tradewindow.h"
//----------------------------------------------------------------------------------
CTradeWindow::CTradeWindow()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CTradeWindow::CTradeWindow(const uint &serial, const uint &id1, const uint &id2, const QString &name)
: m_Serial(serial), m_ID1(id1), m_ID2(id2), m_Name(name)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CTradeWindow::~CTradeWindow()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
