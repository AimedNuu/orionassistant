// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** Menu.cpp
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "menu.h"
//----------------------------------------------------------------------------------
CMenu::CMenu()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CMenu::CMenu(const uint &serial, const ushort &id, const QString &name, const bool &grayMenu)
: m_Serial(serial), m_ID(id), m_GrayMenu(grayMenu), m_Name(name)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CMenu::~CMenu()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
