// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** FindItem.cpp
**
** Copyright (C) February 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "finditem.h"
#include "../OrionAssistant/orionassistant.h"
#include "../Managers/TextParser.h"
//----------------------------------------------------------------------------------
CFindItem::CFindItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CFindItem::CFindItem(const QString &graphic, const QString &color)
: m_Graphic(graphic), m_Color(color)
{
	OAFUN_DEBUG("");
	OnChangeGraphic(graphic);
	OnChangeColor(color);
}
//----------------------------------------------------------------------------------
CFindItem::~CFindItem()
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
void CFindItem::OnChangeGraphic(const QString &val)
{
	OAFUN_DEBUG("");
	CTextParser graphicParser(val.toStdString(), "|", "", "");

	QStringList listGraphic = graphicParser.ReadTokens();

	m_ListGraphic.clear();
	m_ListGraphicFlags.clear();
	m_IgnoreGraphics.clear();

	if (!listGraphic.empty())
	{
		for (const QString &strGraphic : listGraphic)
		{
			uint currentGraphic = 0;
			uint flagsGraphic = ParseToken(strGraphic, currentGraphic, true);

			if (flagsGraphic & FTS_NOT_EQUAL)
				m_IgnoreGraphics.push_back(currentGraphic);
			else
			{
				m_ListGraphic.push_back(currentGraphic);
				m_ListGraphicFlags.push_back(flagsGraphic);
			}
		}

		if (m_ListGraphic.empty())
		{
			m_ListGraphic.push_back(0xFFFF);
			m_ListGraphicFlags.push_back(0);
		}
	}
}
//----------------------------------------------------------------------------------
void CFindItem::OnChangeColor(const QString &val)
{
	OAFUN_DEBUG("");
	CTextParser colorParser(val.toStdString(), "|", "", "");
	QStringList listColor = colorParser.ReadTokens();

	m_ListColor.clear();
	m_ListColorFlags.clear();
	m_IgnoreColors.clear();

	if (!listColor.empty())
	{
		for (const QString &strColor : listColor)
		{
			uint currentColor = 0;
			uint flagsColor = ParseToken(strColor, currentColor, true);

			if (flagsColor & FTS_NOT_EQUAL)
				m_IgnoreColors.push_back(currentColor);
			else
			{
				m_ListColor.push_back(currentColor);
				m_ListColorFlags.push_back(flagsColor);
			}
		}

		if (m_ListColor.empty())
		{
			m_ListColor.push_back(0xFFFF);
			m_ListColorFlags.push_back(0);
		}
	}
}
//----------------------------------------------------------------------------------
uint CFindItem::ParseToken(QString str, uint &value, const bool &isGraphic) const
{
	OAFUN_DEBUG("c5_f1");
	str = str.trimmed();
	uint result = FTS_NORMAL;

	while (str.length())
	{
		if (str.at(0) == '!')
		{
			result |= FTS_NOT_EQUAL;
			str.remove(0, 1);
		}
		else if (str.at(0) == '<')
		{
			result |= FTS_LOWER;
			str.remove(0, 1);
		}
		else if (str.at(0) == '>')
		{
			result |= FTS_HIGHTER;
			str.remove(0, 1);
		}
		else
			break;
	}

	if (isGraphic)
		value = COrionAssistant::TextToGraphic(str);
	else
		value = COrionAssistant::TextToSerial(str);

	return result;
}
//----------------------------------------------------------------------------------
bool CFindItem::TestValues(const uint &original, const uint &test, const uint &flags) const
{
	OAFUN_DEBUG("c5_f2");

	if (test != 0xFFFF)
	{
		if ((flags & FTS_NOT_EQUAL) && original == test)
			return false;
		else if ((flags & FTS_LOWER) && original >= test)
			return false;
		else if ((flags & FTS_HIGHTER) && original <= test)
			return false;
		else if (!flags && original != test)
			return false;
	}

	return true;
}
//----------------------------------------------------------------------------------
bool CFindItem::Found(const ushort &graphic, const ushort &color) const
{
	OAFUN_DEBUG("c5_f3");
	if (!m_IgnoreGraphics.empty())
	{
		for (const ushort &item : m_IgnoreGraphics)
		{
			if (item == graphic)
				return false;
		}
	}

	if (!m_IgnoreColors.empty())
	{
		for (const ushort &item : m_IgnoreColors)
		{
			if (item == color)
				return false;
		}
	}

	int graphicsCount = m_ListGraphic.size();
	int colorsCount = m_ListColor.size();

	if (!graphicsCount || !colorsCount)
		return false;

	IFOR(i, 0, graphicsCount)
	{
		if (TestValues(graphic, m_ListGraphic[i], m_ListGraphicFlags[i]))
		{
			IFOR(j, 0, colorsCount)
			{
				if (TestValues(color, m_ListColor[j], m_ListColorFlags[j]))
					return true;
			}
		}
	}

	return false;
}
//----------------------------------------------------------------------------------
