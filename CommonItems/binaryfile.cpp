// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** BinaryFile.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "binaryfile.h"
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
CFileObject::CFileObject(QString fileName)
{
	OAFUN_DEBUG("c4_f1");
	m_File.setFileName(fileName);
}
//----------------------------------------------------------------------------------
CFileObject::~CFileObject()
{
	OAFUN_DEBUG("c4_f2");
	m_File.close();
}
//----------------------------------------------------------------------------------
bool CFileObject::Open(QIODevice::OpenMode mode)
{
	OAFUN_DEBUG("c4_f3");
	return m_File.open(mode);
}
//----------------------------------------------------------------------------------
void CFileObject::Close()
{
	OAFUN_DEBUG("c4_f4");
	m_File.close();
}
//----------------------------------------------------------------------------------
qint64 CFileObject::Size()
{
	OAFUN_DEBUG("c4_f5");
	return m_File.size();
}
//----------------------------------------------------------------------------------
qint64 CFileObject::Pos()
{
	OAFUN_DEBUG("c4_f6");
	return m_File.pos();
}
//----------------------------------------------------------------------------------
void CFileObject::Seek(qint64 offset)
{
	OAFUN_DEBUG("c4_f7");
	m_File.seek(offset);
}
//----------------------------------------------------------------------------------
char CFileObject::ReadChar()
{
	OAFUN_DEBUG("c4_f8");
	if (m_File.pos() + sizeof(char) > m_File.size())
		return 0;

	QByteArray buf = m_File.read(sizeof(char));

	return *buf.data();
}
//----------------------------------------------------------------------------------
short CFileObject::ReadShort()
{
	OAFUN_DEBUG("c4_f9");
	if (m_File.pos() + sizeof(short) > m_File.size())
		return 0;

	short val = 0;
	m_File.read((char*)&val, sizeof(short));

	return val;
}
//----------------------------------------------------------------------------------
int CFileObject::ReadInt()
{
	OAFUN_DEBUG("c4_f10");
	if (m_File.pos() + sizeof(int) > m_File.size())
		return 0;

	int val = 0;
	m_File.read((char*)&val, sizeof(int));

	return val;
}
//----------------------------------------------------------------------------------
uchar CFileObject::ReadUChar()
{
	OAFUN_DEBUG("c4_f11");
	return (uchar)ReadChar();
}
//----------------------------------------------------------------------------------
ushort CFileObject::ReadUShort()
{
	OAFUN_DEBUG("c4_f12");
	return (ushort)ReadShort();
}
//----------------------------------------------------------------------------------
uint CFileObject::ReadUInt()
{
	OAFUN_DEBUG("c4_f13");
	return (uint)ReadInt();
}
//----------------------------------------------------------------------------------
QString CFileObject::ReadString()
{
	OAFUN_DEBUG("c4_f14");
	int len = ReadInt();

	if (m_File.pos() + len > m_File.size() || !len)
		return "";

	QByteArray buf = m_File.read(len);

	return QString(buf);
}
//----------------------------------------------------------------------------------
QByteArray CFileObject::ReadArray(int size)
{
	OAFUN_DEBUG("c4_f15");
	if (m_File.pos() + size > m_File.size())
		size = m_File.size() - m_File.pos();

	if (size < 1)
		return m_File.readAll();

	return m_File.read(size);
}
//----------------------------------------------------------------------------------
void CFileObject::WriteChar(char val)
{
	OAFUN_DEBUG("c4_f16");
	m_File.write((char*)&val, sizeof(char));
}
//----------------------------------------------------------------------------------
void CFileObject::WriteShort(short val)
{
	OAFUN_DEBUG("c4_f17");
	m_File.write((char*)&val, sizeof(short));
}
//----------------------------------------------------------------------------------
void CFileObject::WriteInt(int val)
{
	OAFUN_DEBUG("c4_f18");
	m_File.write((char*)&val, sizeof(int));
}
//----------------------------------------------------------------------------------
void CFileObject::WriteUChar(uchar val)
{
	OAFUN_DEBUG("c4_f19");
	m_File.write((char*)&val, sizeof(char));
}
//----------------------------------------------------------------------------------
void CFileObject::WriteUShort(ushort val)
{
	OAFUN_DEBUG("c4_f20");
	m_File.write((char*)&val, sizeof(short));
}
//----------------------------------------------------------------------------------
void CFileObject::WriteUInt(uint val)
{
	OAFUN_DEBUG("c4_f21");
	m_File.write((char*)&val, sizeof(int));
}
//----------------------------------------------------------------------------------
void CFileObject::WriteString(QString val)
{
	OAFUN_DEBUG("c4_f22");
	QByteArray buf = val.toUtf8();

	int len = buf.size() + sizeof(char);
	m_File.write((char*)&len, sizeof(int));

	if (buf.size())
		m_File.write(buf.data(), buf.size());

	uchar nullTerminate = 0;
	m_File.write((char*)&nullTerminate, sizeof(char));
}
//----------------------------------------------------------------------------------
void CFileObject::WriteArray(QByteArray val)
{
	OAFUN_DEBUG("c4_f23");
	m_File.write(val);
}
//----------------------------------------------------------------------------------
