/***********************************************************************************
**
** Skill.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef SKILL_H
#define SKILL_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CSkill
{
	SETGET(bool, Button, 0)
	SETGET(QString, Name, "")
	SETGET(float, BaseValue, 0.0f)
	SETGET(float, Value, 0.0f)
	SETGET(float, OldValue, 0.0f)
	SETGET(float, Cap, 0.0f)
	SETGET(uchar, Status, 0)

public:
	CSkill() {}
	CSkill(const bool &haveButton, const QString &name);
	~CSkill() {}
};
//----------------------------------------------------------------------------------
static const int MAX_SKILLS_COUNT = 60;
extern CSkill g_Skills[MAX_SKILLS_COUNT];
extern int g_SkillsCount;
//----------------------------------------------------------------------------------
#endif // SKILL_H
//----------------------------------------------------------------------------------
