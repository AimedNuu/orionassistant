/***********************************************************************************
**
** GumpItem.h
**
** Copyright (C) May 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef GUMPITEM_H
#define GUMPITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
enum GUMP_ITEM_TYPE
{
	GIT_INVALID_TYPE = 0,
	GIT_NODISPOSE,
	GIT_NOMOVE,
	GIT_NOCLOSE,
	GIT_PAGE,
	GIT_GROUP,
	GIT_RESIZEPIC,
	GIT_CHECKERTRANS,
	GIT_BUTTON,
	GIT_BUTTONTILEART,
	GIT_CHECKBOX,
	GIT_RADIO,
	GIT_TEXT,
	GIT_CROPPEDTEXT,
	GIT_TEXTENTRY,
	GIT_TEXTENTRYLIMITED,
	GIT_TILEPIC,
	GIT_TILEPICHUE,
	GIT_GUMPPIC,
	GIT_GUMPPICTILED,
	GIT_HTMLGUMP,
	GIT_XMFHTMLGUMP,
	GIT_XMLHTMLGUMPCOLOR,
	GIT_XMLHTMLTOK,
	GIT_TOOLTIP,
	GIT_MASTERGUMP
};
//----------------------------------------------------------------------------------
class CGumpItem
{
	SETGET(GUMP_ITEM_TYPE, Type, GIT_INVALID_TYPE)
	SETGET(QString, Command, "")

public:
	CGumpItem();
	CGumpItem(const GUMP_ITEM_TYPE &type, const QString &command);
	virtual ~CGumpItem();

	void RestoreCommand(const QStringList &list);
};
//----------------------------------------------------------------------------------
typedef QList<CGumpItem> GUMP_LIST;
//----------------------------------------------------------------------------------
#endif // GUMPITEM_H
//----------------------------------------------------------------------------------
