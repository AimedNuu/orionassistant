// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ShopItem.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "shopitem.h"
//----------------------------------------------------------------------------------
CShopItem::CShopItem(const QString &graphic, const QString &color, const QString &name, const int &count, const uint &price, const QString &comment)
: m_Graphic(graphic), m_Color(color), m_Name(name), m_Count(count), m_Price(price),
m_Comment(comment)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
