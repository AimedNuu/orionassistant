/***********************************************************************************
**
** VendorShopItem.h
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef VENDORSHOPITEM_H
#define VENDORSHOPITEM_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CVendorShopItem
{
	SETGET(uint, Serial, 0)
	SETGET(ushort, Graphic, 0)
	SETGET(ushort, Color, 0)
	SETGET(QString, Name, "")
	SETGET(int, Count, 0)
	SETGET(uint, Price, 0)
	SETGET(bool, NameFromCliloc, false)

public:
	CVendorShopItem() {}
};
//----------------------------------------------------------------------------------
#endif // VENDORSHOPITEM_H
//----------------------------------------------------------------------------------
