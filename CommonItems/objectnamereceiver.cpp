// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** ObjectNameReceiver.cpp
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "ObjectNameReceiver.h"
#include <QTimer>
#include "../GameObjects/GameWorld.h"
//----------------------------------------------------------------------------------
CObjectNameReceiver::CObjectNameReceiver(const uint &serial, QLineEdit *control)
: QObject(), m_Serial(serial), m_Control(control)
{
	OAFUN_DEBUG("c7_f1");
	connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));

	QTimer::singleShot(200, this, SLOT(process()));
}
//----------------------------------------------------------------------------------
void CObjectNameReceiver::process()
{
	OAFUN_DEBUG("c7_f2");
	if (g_World != nullptr && m_Serial != 0 && m_Control != nullptr)
	{
		CGameObject *obj = g_World->FindWorldObject(m_Serial);

		if (obj != nullptr)
			m_Control->setText(obj->GetName());
	}

	emit finished();
}
//----------------------------------------------------------------------------------
