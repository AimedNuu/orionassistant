// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** DisplayItem.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "displayitem.h"
//----------------------------------------------------------------------------------
CDisplayItem::CDisplayItem(const DISPLAY_ITEM_TYPE &type, const QString &text, const ushort &graphic, const ushort &hue, const uint &minColor,
						   const uint &midColor, const uint &color, const bool &background, const uint &backgroundMinColor,
						   const uint &backgroundMidColor, const uint &backgroundColor, const int &minValue, const int &midValue,
						   const bool &percents)
: m_Type(type), m_Text(text), m_Graphic(graphic), m_Hue(hue), m_MinColor(minColor),
m_MidColor(midColor), m_Color(color), m_Background(background),
m_BackgroundMinColor(backgroundMinColor), m_BackgroundMidColor(backgroundMidColor),
m_BackgroundColor(backgroundColor), m_MinValue(minValue), m_MidValue(midValue),
m_Percents(percents)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
CDisplayItem::CDisplayItem(const CDisplayItem &obj)
: m_Type(obj.GetType()), m_Text(obj.GetText()), m_Graphic(obj.GetGraphic()), m_Hue(obj.GetHue()),
m_MinColor(obj.GetMinColor()), m_MidColor(obj.GetMidColor()), m_Color(obj.GetColor()),
m_Background(obj.GetBackground()), m_BackgroundMinColor(obj.GetBackgroundMinColor()),
m_BackgroundMidColor(obj.GetBackgroundMidColor()), m_BackgroundColor(obj.GetBackgroundColor()),
m_MinValue(obj.GetMinValue()), m_MidValue(obj.GetMidValue()), m_Percents(obj.GetPercents())
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
