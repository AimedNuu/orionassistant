// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** Prompt.cpp
**
** Copyright (C) October 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "prompt.h"
//----------------------------------------------------------------------------------
CPrompt::CPrompt(const uint &serial, const QString &text, const PROMPT_TYPE &type)
: m_Serial(serial), m_Text(text), m_Type(type)
{
	OAFUN_DEBUG("");
}
//----------------------------------------------------------------------------------
