/***********************************************************************************
**
** ObjectNameReceiver.h
**
** Copyright (C) March 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef OBJECTNAMERECEIVER_H
#define OBJECTNAMERECEIVER_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
#include <QLineEdit>
//----------------------------------------------------------------------------------
class CObjectNameReceiver : public QObject
{
	Q_OBJECT

public slots:
	void process();

signals:
	void finished();

private:
	uint m_Serial{ 0 };
	QLineEdit *m_Control{ nullptr };

public:
	CObjectNameReceiver(const uint &serial, QLineEdit *control);
	virtual ~CObjectNameReceiver() {}
};
//----------------------------------------------------------------------------------
#endif // OBJECTNAMERECEIVER_H
//----------------------------------------------------------------------------------
