// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** Skill.cpp
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "skill.h"
//----------------------------------------------------------------------------------
CSkill g_Skills[MAX_SKILLS_COUNT];
int g_SkillsCount = MAX_SKILLS_COUNT;
//----------------------------------------------------------------------------------
CSkill::CSkill(const bool &haveButton, const QString &name)
: m_Button(haveButton)
{
	OAFUN_DEBUG("");
	if (name.length())
		m_Name = name;
	else
		m_Name = "NoNameSkill";

	//LOG("Skill loaded (button:%i): %s\n", m_Button, m_Name.c_str());
}
//----------------------------------------------------------------------------------
