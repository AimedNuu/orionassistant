/***********************************************************************************
**
** BinaryFile.h
**
** Copyright (C) October 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef BINARYFILE_H
#define BINARYFILE_H
//----------------------------------------------------------------------------------
#include <QFile>
#include <QString>
#include <QRect>
#include <QByteArray>
//----------------------------------------------------------------------------------
class CFileObject
{
private:
	QFile m_File;

public:
	CFileObject(QString fileName);

	~CFileObject();

	bool Open(QIODevice::OpenMode mode);

	void Close();

	qint64 Size();

	qint64 Pos();

	void Seek(qint64 offset);



	char ReadChar();

	short ReadShort();

	int ReadInt();

	uchar ReadUChar();

	ushort ReadUShort();

	uint ReadUInt();

	QString ReadString();

	QByteArray ReadArray(int size = 0);



	void WriteChar(char val);

	void WriteShort(short val);

	void WriteInt(int val);

	void WriteUChar(uchar val);

	void WriteUShort(ushort val);

	void WriteUInt(uint val);

	void WriteString(QString val);

	void WriteArray(QByteArray val);
};
//----------------------------------------------------------------------------------
#endif // BINARYFILE_H
