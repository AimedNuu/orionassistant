// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/***********************************************************************************
**
** GumpHook.cpp
**
** Copyright (C) June 2017 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#include "gumphook.h"
//----------------------------------------------------------------------------------
CGumpHook::CGumpHook(CGumpHook *original)
{
	OAFUN_DEBUG("");
	if (original != nullptr)
	{
		m_Index = original->GetIndex();

		if (!original->m_Entries.empty())
		{
			for (const CGumpHookEntry &entry : original->m_Entries)
				m_Entries.push_back(entry);
		}

		if (!original->m_Checks.empty())
		{
			for (const CGumpHookCheck &check : original->m_Checks)
				m_Checks.push_back(check);
		}
	}
}
//----------------------------------------------------------------------------------
