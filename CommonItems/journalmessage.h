/***********************************************************************************
**
** JournalMessage.h
**
** Copyright (C) November 2016 Hotride
**
************************************************************************************
*/
//----------------------------------------------------------------------------------
#ifndef JOURNALMESSAGE_H
#define JOURNALMESSAGE_H
//----------------------------------------------------------------------------------
#include "../orionassistant_global.h"
//----------------------------------------------------------------------------------
class CJournalMessage
{
	SETGET(uint, Serial, 0)
	SETGET(uint, Timer, GetTickCount())
	SETGET(ushort, Color, 0)
	SETGET(QString, Text, "")
	SETGET(uchar, Flags, 0)
	SETGET(int, FindTextID, 0)

public:
	CJournalMessage();
	CJournalMessage(const CJournalMessage &msg);
	CJournalMessage(const uint &serial, const ushort &color, const QString &text);
	virtual ~CJournalMessage();
};
//----------------------------------------------------------------------------------
#endif // JOURNALMESSAGE_H
//----------------------------------------------------------------------------------
